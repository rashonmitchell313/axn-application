<?php 
require_once('../../Connections/dbconn.php'); 

require_once('../../includes/common.php');

require_once('../../checklogin.php');

require_once('../../includes/portal.php');

if(isset($_REQUEST['reportfor']) AND $_REQUEST['reportfor']!="")
{
	
	if($_REQUEST['reportfor']=='SR')
	{
		$reportfor='SR';
		$reptitle="Specialty Retail";	
	}
	if($_REQUEST['reportfor']=='DF')
	{
		$reportfor='DF';
		$reptitle="Duty Free";
	}
	if($_REQUEST['reportfor']=='NG')
	{
		$reportfor='NG';
		$reptitle="News & Gifts";
	}
		if($_REQUEST['reportfor']=='FB')
	{
		$reportfor='FB';
		$reptitle="Food and Beverage";
	}
		if($_REQUEST['reportfor']=='PT')
	{
		$reportfor='PT';
		$reptitle="Passenger Traffic";
	}
}
else
{
	$reportfor='FB';
	$reptitle="Food and Beverage";	
}

function subval_sort($a,$subkey) {
	foreach($a as $k=>$v) {
		$b[$k] = strtolower($v[$subkey]);
	}
	asort($b);
	foreach($b as $key=>$val) {
		$c[] = $a[$key];
	}
	return $c;
}
$airport = read_table("","SELECT DISTINCT(`IATA`),aid as aid,`acity` FROM `airports`");
$users = array();
$TCurrentSqFt=0;
	$TGrossSale=0;
	$TSalesEp=0;
	$TRentRev=0;
foreach ($airport as $AP) 
{
	$sql = "select * from terminals left join terminalsannual on terminalsannual.tid=terminals.tid 
 where aid='".$AP['aid']."' ORDER BY terminals.tid";	
	$airportname = "SELECT * FROM airports  where aid=".$AP['aid']."";
	$airportinfo=read_table("",$airportname);	
	$tl=read_table("",$sql);	
	$fbgrosssalestotal	= 0;
	$srgrosssalestotal	= 0;
	$nggrosssalestotal	= 0;
	
	$fbrentrevtotal	= 0;
	$srrentrevtotal	= 0;
	$ngrentrevtotal	= 0;
	$indi_terms_list="";
	if($tl != "")
	{
		
		foreach ($tl as $_) 
		{				
			if($reportfor=='PT')
			{
				$CurrentSqFt= $_["dfcurrsqft"];
				$GrossSale=$_["dfgrosssales"];
				$SalesEp=$_["tpasstraffic"];
				$RentRev=$_["dfrentrev"];				
			}	
			if($reportfor=='DF')
			{
				$CurrentSqFt= $_["dfcurrsqft"];
				$GrossSale=$_["dfgrosssales"];
				$SalesEp=$_["dfsalesep"];
				$RentRev=$_["dfrentrev"];
			}
			if($reportfor=='SR')
			{
				$CurrentSqFt= $_["srcurrsqft"];
				$GrossSale=$_["srgrosssales"];
				$SalesEp=$_["srsalesep"];
				$RentRev=$_["srrentrev"];
			}
			if($reportfor=='FB')
			{
				$CurrentSqFt= $_["fbcurrsqft"];
				$GrossSale=$_["fbgrosssales"];
				$SalesEp=$_["fbsalesep"];
				$RentRev=$_["fbrentrev"];
			}
			if($reportfor=='NG')
			{
				$CurrentSqFt= $_["ngcurrsqft"];
				$GrossSale=$_["nggrosssales"];
				$SalesEp=$_["ngsalesep"];
				$RentRev=$_["ngrentrev"];
			}			
			$TCurrentSqFt+=$CurrentSqFt;	
			$TGrossSale+=$GrossSale;
			$TSalesEp+=$SalesEp;
			$TRentRev+=$RentRev;	 
			$users[] = array('IATA_ID' => $airportinfo[0]['IATA'],'Terminal_Abbr' => $AP["acity"],
			'Terminal_Name'=>$_["terminalname"],
			'CurrentSqFt'=>$CurrentSqFt,'GrossSales'=>$GrossSale,
			'SalesEP'=>$SalesEp,'GrossRentals'=>$RentRev);	
		}
	}
} // end foreach ($airport as $AP) 
//print_r($users);
$songs = subval_sort($users,'SalesEP');


// Set document properties
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Los_Angeles');
if (PHP_SAPI == 'cli')
	die('This report should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';
 


$val='Top 50 TERMNLS '.$reptitle;
$ref = date("Y"); 

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle($val)
							 ->setSubject($val)
							 ->setDescription($val)
							 ->setKeywords($val)
							 ->setCategory($val);
//$val='Top 50 Airports '.$ref;
// Style for the Main heading
$BGstyleArray = array(
	'fill'  => array(
			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
			'argb' => 'F2F2F2'
			)
	 	)
	);
$styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ));
$style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );
// Style for the column titles
$heading = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    )
	);
$titlestyle = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    ),
	'borders' => array(
	'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    ),
    'top' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    )
  )
);


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);
$objPHPExcel->getActiveSheet()->setCellValue('A1','2014 Top 50 Terminals by '.$reptitle);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Top50_TRMNL_'.str_replace(" ","_",$reptitle));


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$aenplaning    = '';
$afbgrosssales = '';
$asrgrosssales = '';
$anggrosssales = '';
$avg_sp 	   = '';
$aconcessiongrosssales = '';

		$airterm ="SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airports.IATA!='YYJ' AND airports.IATA!='YVR'  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC LIMIT 0,50";
		$airtermQuery=mysql_query($airterm);
		$ap_count=mysql_num_rows($airtermQuery);

		$count 		= 1;
		$headcell	= 2;
		$titlecell	= 3;
		$cell 		= 4;
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headcell.':H'.$headcell)->applyFromArray($heading);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$headcell.':H'.$headcell);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headcell,' ');
			
		$objPHPExcel->getActiveSheet()->getStyle('A'.$titlecell.':H'.$titlecell)->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$titlecell,'IATA ID');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$titlecell, 'City');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$titlecell, 'Terminal Name');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$titlecell, 'Current Sq.Ft.');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$titlecell, 'Gross Sales');
		if($reportfor!='PT')
		{
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$titlecell, 'Sales/EP');
		} else 
		{
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$titlecell, 'Passenger Traffic');
		}
		
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$titlecell, 'Gross Rentals');
		//$objPHPExcel->getActiveSheet()->setCellValue('H'.$titlecell, 'Sales E/P');
		$counter=0;
 		 for($i=count($songs)-1;$i>=0;$i--)
		{
			//$airportlist=mysql_fetch_assoc($airtermQuery); 
			//$all=$airportlist["afbgrosssales"]+$airportlist["asrgrosssales"]+$airportlist["anggrosssales"];
			if($counter%2 == 0){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':G'.$cell)->applyFromArray($BGstyleArray);
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell, stripslashes($songs[$i]["IATA_ID"]));	
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$cell, $songs[$i]["Terminal_Abbr"]);	
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$cell, $songs[$i]["Terminal_Name"]);	
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$cell, $songs[$i]["CurrentSqFt"]);	
			//$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$cell, "$ ".$songs[$i]["GrossSales"]);
			if($reportfor!='PT')
			{
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell, "$ ".$songs[$i]["SalesEP"]);
			} else 
			{
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell,$songs[$i]["SalesEP"]);
			}
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$cell, "$ ".$songs[$i]["GrossRentals"]);
			//$objPHPExcel->getActiveSheet()->setCellValue('H'.$cell, '$'.number_format($airportlist["asalesep"], 2, '.', ''));
			if($counter==51)
			{
				break;	
			}
			$counter++;
			$cell ++;
		}
		$cell=$cell+2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell, "Total");
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$cell, "");
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$cell, "");
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$cell, $TCurrentSqFt);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$cell, $TGrossSale);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell, $TSalesEp);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$cell, $TRentRev);
		//Collect Total
		/*$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':B'.$cell)->applyFromArray($heading);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$cell.':B'.$cell);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell,'Totals');
		//Collect Total Enplanements Numbers
		if($aenplaning)
			{$tot_aenplaning=number_format($aenplaning);}
			else{$tot_aenplaning='';}
		$objPHPExcel->getActiveSheet()->getStyle('C'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$cell,$tot_aenplaning);
		//Collect Total F&B Total Sales
		if($afbgrosssales)
			{$fb_afbgrosssales='$'.number_format($afbgrosssales);}
			else{$fb_afbgrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$cell,$fb_afbgrosssales);
		//Collect Specialty Total Sales
		if($asrgrosssales)
			{$sr_asrgrosssales='$'.number_format($asrgrosssales);}
			else{$sr_asrgrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('E'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$cell,$sr_asrgrosssales);
		//Collect NG Total Sales
		if($anggrosssales)
			{$ng_anggrosssales='$'.number_format($anggrosssales);}
			else{$ng_anggrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('F'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell,$ng_anggrosssales);
		//Collect Total Sales (Excluding DF)
		if($aconcessiongrosssales)
			{$to_aconcessiongrosssales='$'.number_format($aconcessiongrosssales);}
			else{$to_aconcessiongrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('G'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$cell,$to_aconcessiongrosssales);
		//Collect Total Sales E/P
		if($avg_sp)
			{$to_avg_sp='$'.number_format($avg_sp, 2, '.', '');}
			else{$to_avg_sp='';}
		$objPHPExcel->getActiveSheet()->getStyle('H'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$cell,'AVG='.$to_avg_sp);
		//Add Date In the Footer
		$cell = $cell+5;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cell)->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell,date("Y-m-d H:i:s"));*/
//===================== Unset All Variables =========================================
unset($aenplaning);
unset($afbgrosssales);
unset($asrgrosssales);
unset($anggrosssales);
unset($aconcessiongrosssales);
unset($avg_sp);
//==============================================================

//==============================================================

$sheetName = '2014_Top_50_Terminals_by_'.str_replace(" ","_",$reptitle).'.xlsx';
// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$sheetName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
$objWriter->save('php://output');


exit;



//==============================================================

//==============================================================

//==============================================================





?>



