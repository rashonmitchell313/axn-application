<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class reset_password extends CI_Controller {

    public $outputData = "";

    function __construct() {
        parent::__construct();
        //error_reporting(1);
        $this->outputData['menu'] = 'frontend/include/menu';
        $this->outputData['footer'] = 'frontend/include/footer';
        $this->outputData['header'] = 'frontend/include/header';
    }

    /**
     * 
     * START Reset Password Link {Check Email in Database Send Reset Password link in email} (Required Email)
     */
    public function index() {
        $this->load->model('commonmodel');
        $email = $this->input->post('email');
        $action = $this->input->post('action');
        if ($action == 'email_check') {
            $user_id = $this->session->userdata('id');
            if ($this->commonmodel->check_username_email_availability($email) > 0) {
                $this->commonmodel->set_message(0, "Email Already Exist. Re-try with different email.");
                $this->commonmodel->get_message();
            } else {
                //If email does not exist then send the success flag
                echo 1;
            }
        }
        if ($action == 'send_password_rest_link') {
            if ($this->commonmodel->get_total_count('users', array('email' => $email)) > 0) {

                $where = array('email' => $email);
                $user_encrypt_key = md5($email . "-" . time());
                $this->db->where('email', $email);
                $this->db->update('users', array('user_encrypt_key' => $user_encrypt_key));
                $result = $this->commonmodel->get_single_row_from_table_where('users', $where);

                $link = 'password_reset_link';
                $where1 = array('email_temp_name' => $link);
                $email_template = $this->commonmodel->get_single_row_from_table_where('tbl_email_temp', $where1);

                $search_array = array('#first_name', '#last_namename', '#reset_key', '#faq_url');

                $resetkey = '<a href=' . site_url('reset_password/reset_pass/' . $result['user_encrypt_key']) . '>this link</a>';
                $faq_url = '<a href='. site_url('faq-view') .'>AXN Factbook FAQ</a>';
                $replace_array = array($result['first_name'], $result['last_name'], $resetkey, $faq_url);

                $message1 = $email_template['email_temp_text'];

                $message = str_replace($search_array, $replace_array, $message1);

                //send out email 
                $this->load->library('email');

                $config['smtp_host'] = 'axnfactbook.com';
                $config['smtp_user'] = 'support@axnfactbook.com';
                $config['smtp_pass'] = 'Urbanexpo1';
                $config['smtp_port'] = '587';
                $config['smtp_crypto'] = 'tls';
                $config['charset'] = 'iso-8859-1';
                $config['mailtype'] = 'html';
                $config['wordwrap'] = TRUE;
                $this->email->initialize($config);

                $this->email->from('support@axnfactbook.com', 'Axnfactbook Support');
                $this->email->to($email);
                $this->email->subject('Notification: Reset your AXNFactbook Password');
                
                $data = array(
                    'message' => $message
                );

                $this->email->message($this->load->view('frontend/email_temp', $data, true));
                
//                $this->email->send();

                if ($this->email->send()) {
                    echo "<div class='alert alert-success'>Password reset link has been sent to your email.</div>";
                    return false;
                } else {
                    echo "<div class='alert alert-danger'>There was an error while sending email. Please try again later.</div>";
                    return true;
                }
            } else {
                //If email does not exist then send the success flag
                echo "<div class='alert alert-danger'>This email does not exist in our record. Please enter the correct email address.</div>";
            }
        }
        if ($action == "update_password") {
            $user_encrypt_key = $this->input->post('user_encrypt_key');
            $user_password = md5($this->input->post('pass'));

            if ($this->commonmodel->get_total_count('users', array('user_encrypt_key' => $user_encrypt_key)) > 0) {

                $this->commonmodel->update_record('users', array('user_encrypt_key' => $user_encrypt_key), array('user_encrypt_key' => '', 'password' => $user_password));
                echo 1;
            } else {
                echo 0;
            }
        }
    }
    /**
     * 
     * END Reset Password Link
     */

    /**
     * 
     * START Reset Password (Required recived Email link)
     */
    public function reset_pass() {

        $this->load->model('commonmodel');
        $enc_type_key = $this->uri->segment(3);
        $data_array = array('user_encrypt_key' => $enc_type_key);
        $user_info = $this->commonmodel->get_all_row_from_table_where('users', $data_array);
        if (count($user_info) > 0) {

            $expiry_time = $this->common->GetSingleRowWithColumn('users', array('user_encrypt_key' => $enc_type_key), 'user_id,first_name,last_name,email,lastmodified,(TIME_TO_SEC(NOW()) -TIME_TO_SEC(lastmodified))/60 AS MinuteDiff');

            if($expiry_time['MinuteDiff'] < 4320){

                $this->load->view('frontend/reset_password', $this->outputData);
            } else {
                $this->common->setmessage('Failed ! </strong>This Reset Password link has been <b>Expired</b>. Please reset password again.', -1);
                redirect('frontend');
                exit();

            }
        } else {
            $this->common->setmessage('Failed ! </strong>Your email verification link has been expired.Please reset password again</a>', -1);
            redirect('frontend');
            exit();
        }
    }
    /**
     * 
     * END Reset Password
     */

}
