﻿<?php 
require_once('../../Connections/dbconn.php'); 

require_once('../../includes/common.php');

require_once('../../checklogin.php');

require_once('../../includes/portal.php');


$ref=$_POST['areportyear'];
// Set document properties
date_default_timezone_set('America/Los_Angeles');
if (PHP_SAPI == 'cli')
	die('This report should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle("Leases Due to Expire by Year-End".$ref)
							 ->setSubject("Leases Due to Expire by Year-End".$ref)
							 ->setDescription("Leases Due to Expire by Year-End".$ref)
							 ->setKeywords("Leases Due to Expire by Year-End".$ref)
							 ->setCategory("Leases Due to Expire by Year-End".$ref);
$val='Leases Due to Expire by Year-End '.$ref;
// Style for the Main heading
$styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ));
 $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );
// Style for the column titles
$heading = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    )
	);
$titlestyle = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    ),
	'borders' => array(
	'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    ),
    'top' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    )
	)
	);




$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);

$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
$objPHPExcel->getActiveSheet()->setCellValue('A1','Leases Due to Expire by Year-End '.$ref);




/*$objPHPExcel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($heading);
$objPHPExcel->getActiveSheet()->setCellValue('A3','Outlet Name/Description(Company)');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Airport Location');
$objPHPExcel->getActiveSheet()->setCellValue('C3', '#');
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Sq. Ft.');
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Expires');
$objPHPExcel->getActiveSheet()->setTitle('Leases_Due_Expire_'.$ref);*/


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Leases_Due_Expire_'.$ref);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$tbl = '';

$tbl2 = '';

$tbl_header='';

$tbl_header2='';



function check_date($date) {

    if(strlen($date) == 10) {

        $pattern = '/\.|\/|-/i';    // . or / or -

        preg_match($pattern, $date, $char);

       

        $array = preg_split($pattern, $date, -1, PREG_SPLIT_NO_EMPTY);

       

        if(strlen($array[2]) == 4) {

            // dd.mm.yyyy || dd-mm-yyyy

            if($char[0] == "/"|| $char[0]!= "-") {

                $month = $array[1];

                $day = $array[0];

                $year = $array[2];

            }

			}

		if(strlen($array[0]) == 4) {

            // yyyy/mm/dd    # Common U.S. writing

            if($char[0] == "/") {

                $year = $array[0];

                $month = $array[1];

                $day = $array[2];

            }

        }

        // yyyy-mm-dd    # iso 8601

        if(strlen($array[0]) == 4 && $char[0] == "-") {

            $month = $array[1];

            $day = $array[2];

            $year = $array[0];

        }

	 if(strlen($array[0]) == 4) {

		 if($char[0] == "-") {

		

            $month = $array[1];

            $day = $array[2];

            $year = $array[0];

        }

		}

        if(checkdate($month, $day, $year)) {    //Validate Gregorian date

            return TRUE;

       

        } else {

            return FALSE;

        }

    }else {

        return FALSE;    // more or less 10 chars

    }

}


   $listsql = "select distinct category from lkpcategory";

   $listsql2="SELECT count(oid) FROM `outlets` ";

   $list=mysql_query($listsql);

   $count=mysql_query($listsql2);

   $nr_of_users = mysql_fetch_array($count);
   
   $headcell	= 2;
   $titlecell	= 3;
   $cell 		= 4;
   	for($l=0;$l<mysql_num_rows($list);$l++)
	{
	$cmpnyterms = mysql_fetch_assoc($list);
	 $catid=$cmpnyterms['category'];
//echo $headcell."----".$catid."<br />";
//echo $titlecell."----Outlet Name/Description(Company)<br />";
         $ref=trim($_POST['areportyear']);

         $products = read_table("","select DATE_FORMAT(exp,'%m-%d-%y') as expf, outlets.* from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='$catid' and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and outlets.exp LIKE '%$ref%' order by DATE_FORMAT(exp,'%m-%d-%y') ASC ");

			$objPHPExcel->getActiveSheet()->getStyle('A'.$headcell.':E'.$headcell)->applyFromArray($heading);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$headcell.':F'.$headcell);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$headcell,$catid);

		   	$datetime	= date("Y/m/d H:i:s");
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$headcell,$datetime);
			 $objPHPExcel->getActiveSheet()->getStyle('D'.$headcell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$titlecell.':F'.$titlecell)->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$titlecell, 'IATA');
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$titlecell, 'Terminal Location');
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$titlecell,'Outlet Name/Description(Company)');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$titlecell, 'Airport Location');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$titlecell, 'Sq. Ft.');
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$titlecell, 'Expires');

 if(!empty($products))

 
$resourcearray = array();
$indexarray = array();
   	foreach($products as $prod)

	{

	$aiddd=$prod['aid'];

	$citystrSQL = "SELECT acity,IATA FROM airports WHERE aid='$aiddd'";

    $cityobjQuery =mysql_query($citystrSQL);

	$acity_row=mysql_fetch_assoc($cityobjQuery);

$iata=$acity_row['IATA'];

 if(!check_date($prod['exp']))

        {

        $date_val=date('m/d/Y', strtotime($prod['exp']));

        }

        elseif(strtotime($prod['exp']) =='')

        {

        $date_val="00/00/0000";

        }

        elseif(is_null($prod['exp']))

        {

        $date_val="00/00/0000";

        }

        elseif($prod['exp']=='0000-00-00')

        {

        $date_val="00/00/0000";

        }

	else

	{

	$date_val=date('m/d/Y', strtotime($prod['exp'])); 

	}

	if (empty($prod['companyname'])) 

	{

	$compny='';

	}

	else

	{

	$compny=' ('.$prod['companyname'].')';

	}

		




    //Get character information
	$date = $date_val;
    $outletname = stripslashes($prod['outletname']).$compny;
	$acity_row	= $acity_row['acity'];
	$termlocation=$prod['termlocation'];
	$numlocations	= $prod['numlocations'];
	$sqft			= number_format($prod['sqft']);
    //and anything else you want to add goes here, of course
   	array_push($resourcearray, array('outletname' => $outletname,'iata'=>$iata,'termlocation'=>$termlocation,'acity' =>$acity_row,'numlocations'=>$numlocations,'sqft'=>$sqft,'date' => $date,));
    array_push($indexarray, $date);
	//array_push($indexarray, $numlocations);

}
array_multisort($indexarray, $resourcearray);
foreach($resourcearray as $resource) {

	//echo stripslashes($resource['outletname'])."==".$cell."<br/>";
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell, stripslashes($resource['iata']));	
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$cell, stripslashes($resource['termlocation']));	
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$cell, $resource['outletname']);	
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$cell, $resource['acity']);		
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$cell, $resource['sqft']);		
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell, $resource['date']);	

$cell++; 
}

 $headcell = $cell;
 $titlecell = $cell+1;
 $cell = $cell+2;
 }


//==============================================================

//==============================================================

//==============================================================


$sheetName = 'Leases_Due_to_Expire_by_Year_End_'.$ref.'.xlsx';
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$sheetName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
$objWriter->save('php://output');


exit;



//==============================================================

//==============================================================

//==============================================================





?>



