<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Pdf extends CI_Controller {

public $dataarray;
public $style;
public $mpdf;
public $limit;

function __construct() {

parent::__construct();

$this->common->ThisSecureArea('user');

include_once APPPATH . 'mpdf/mpdf.php';

$this->common->pdfstyleing();

error_reporting(0);

$this->load->library('typography');
}

/**
* 
* Start function for Goto Required Custom Report (Required Year, Type)
*/
public function customreport() {

if ($this->uri->segment(4) >= date("Y")) {
$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));

exit;
}

$ayear = 2010;

$this->limit = 50;

$listofcolumns = $this->input->post('arr');

$reportid = $this->input->post('format');

$reportinfo = $this->common->GetSingleRowFromTableWhere('reports', array('report_id' => $this->input->post('reportid')));

$segment = $reportinfo['url'];

$counter = 0;

$cols = "";

foreach ($listofcolumns as $key => $value):

if ($counter == 0) {

$cols.="`" . $key . "`,";
} else if (count($listofcolumns) == ($counter + 1)) {

$cols.="`" . $key . "`";
} else {

$cols.="`" . $key . "`,";
}

$counter++;

endforeach;

switch ($segment) {

case "top-int-airports2":

$this->top50terminals2('FB', $ayear);

break;

case "all-airports":

$this->allairport($ayear);

break;

case "ratio-report":

$this->ratioreport($ayear);

break;

case "advertising":

$this->top50terminals('AD', $ayear);

break;

case "food-beverage":

$this->top50terminals('FB', $ayear);

break;

case "specialty-retail":

$this->top50terminals('SR', $ayear);

break;

case "news-gifts":

$this->top50terminals('NG', $ayear);

break;

case "duty-free":

$this->top50terminals('DF', $ayear);

break;

case "passenger-services":

$this->top50terminals('PT', $ayear);

break;

case "top-50-airports":

$ayear = $this->uri->segment(4);

$this->top50airport($ayear, 'T50A');

break;

case "top-int-airports":

$ayear = $this->uri->segment(4);

if (is_numeric($ayear)) {

$this->topIntairport($ayear, 'TIA');
} else {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));
}

break;

case "lease-expire2":

$ayear = $this->uri->segment(4);

if (is_numeric($ayear)) {

$this->leaseexpire2($ayear, 'TIA');
} else {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));
}

break;

case "lease-expire":

$ayear = $this->uri->segment(4);

if (is_numeric($ayear)) {

$this->leaseexpire($ayear, 'TIA');
} else {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));
}

break;

default:

$this->common->setmessage('Invalid URL', -1);

$this->common->redirect(site_url('reports'));

exit;
}
}

/**
* 
* End function for Goto Required Custom Report
*/

/**
* 
* Start Default function for Goto Required Report (Required Year, Type)
*/
public function report() {

//        if ($_SERVER['REMOTE_ADDR'] == "202.141.226.196") {
//                    echo 'testing ';
//                    exit;
//                }
//        if ($this->uri->segment(4)>= date("Y") || $this->uri->segment(4)< 2010 || $this->uri->segment(4)< 2011)

if ($this->uri->segment(3) != '') {

$segment = strtolower($this->uri->segment(3));
$ayear = $this->uri->segment(4);
//            switch ('total-tenants') {
switch ($segment) {
//                case "total-tenants":
//
//                    $this->total_tenants();
//
//                    break;
case "lease-expire":

$this->leaseexpire($ayear, 'TIA');

break;
}
}

//        if ($this->uri->segment(4) >= date("Y") || $this->uri->segment(4) < 2010 || $this->uri->segment(4) == 2011 || $this->uri->segment(4) == 2012) {
else if ($this->uri->segment(4) > date("Y") || $this->uri->segment(4) < 2010 || $this->uri->segment(4) == 2011 || $this->uri->segment(4) == 2012) {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));

exit;
}

$ayear = $this->uri->segment(4);


if ($this->common->GetSessionKey('accesslevel') == 2 && $this->uri->segment(5) != "" && is_numeric($this->uri->segment(5)) && $this->uri->segment(5) > 0 && $this->uri->segment(5) != 50) {

$this->limit = $this->uri->segment(5);
} else {

$this->limit = 50;
}

if (!is_numeric($ayear)) {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));
}

$segment = strtolower($this->uri->segment(3));

switch ($segment) {

case "top-int-airports2":

$this->top50terminals2('FB', $ayear);

break;

case "all-airports":

$this->allairport($ayear);

break;

case "ratio-report":

$this->ratioreport($ayear);

break;

case "advertising":

$this->top50terminals('AD', $ayear);

break;

case "food-beverage":

$this->top50terminals('FB', $ayear);

break;

case "specialty-retail":

$this->top50terminals('SR', $ayear);

break;

case "news-gifts":

$this->top50terminals('NG', $ayear);

break;

case "duty-free":

$this->top50terminals('DF', $ayear);

break;

case "passenger-services":

$this->top50terminals('PT', $ayear);

break;

case "top-50-airports":

$ayear = $this->uri->segment(4);

$this->top50airport($ayear, 'T50A');

break;

case "top10carrental":

$ayear = $this->uri->segment(4);

$this->top10carrental($ayear, 'T10A');

break;

case "top-int-airports":

$ayear = $this->uri->segment(4);

if (is_numeric($ayear)) {

$this->topIntairport($ayear, 'TIA');
} else {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));
}

break;

case "lease-expire2":

$ayear = $this->uri->segment(4);

if (is_numeric($ayear)) {

$this->leaseexpire2($ayear, 'TIA');
} else {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));
}

break;

case "lease-expire":

$ayear = $this->uri->segment(4);

if (is_numeric($ayear)) {

$this->leaseexpire($ayear, 'TIA');
} else {

$this->common->setmessage('Invalid URL Year.', -1);

$this->common->redirect(site_url('reports'));
}

break;

case "total-airports-passenger":

$ayear = $this->uri->segment(4);

$this->totalairportpassenger($ayear, 'TAP');

break;

case "total-tenants":

//                $ayear = $this->uri->segment(4);

$this->total_tenants();

break;

default:

$this->common->setmessage('Invalid URL', -1);

$this->common->redirect(site_url('reports'));

exit;
}
}

/**
* 
* End Default function
*/

/**
* 
* Start Lease Expiration 2 (Required Year) 
*/
public function leaseexpire2($year, $type) {

$mpdf = new mPDF('utf-8', 'Letter');

$resourcearray = array();

$indexarray = array();

$tbl_header = "";

$rcounter = 0;

$class = "";

$val = 'Leases Due to Expire by Year-End ' . $year;

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:10pt;font-weight:bold;">  ' . $val . '</div>');

$tbl_header.='<table width="100%" border="0" style="text-align:left; border-collapse: collapse;" cellspacing="0" cellpadding="0" >';

//        $listsql = "select distinct category from lkpcategory";
//
//        $listsql2 = "SELECT count(oid) FROM `outlets` ";
//        $list = $this->common->CustomQueryALL($listsql);

$list = $this->common->Selectdistinct('lkpcategory', 'category');

//        $listcount = $this->common->CustomCountQuery($listsql);
//        $nr_of_users = $this->common->CustomQueryALL($listsql2);

$nr_of_users = $this->common->GetRecordCount('outlets');

foreach ($list as $cmpnyterms):

$catid = $cmpnyterms['category'];

$ref = trim($year);

$products = $this->common->CustomQueryALL("select DATE_FORMAT(exp,'%m-%d-%y') as expf, outlets.* from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category =" . $this->db->escape($catid) . " and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and outlets.exp LIKE '%" . $this->db->escape_like_str($ref) . "%' order by DATE_FORMAT(exp,'%m-%d-%y') ASC ");

$tbl_header.='<tr style="text-align:right;">

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $catid . '</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">&nbsp;</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">&nbsp;</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">&nbsp;</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-family:Vectora LH 95 Black,arial;">' . date("Y-m-d H:i:s") . '</td>

</tr>

<tr style="text-align:right;">

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">IATA</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Outlet Name/Description(Company)</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">#Location</td>                

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Sq. Ft.</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Expires</td>

</tr>';

foreach ($products as $prod):

$aiddd = $prod['aid'];

$citystrSQL = "SELECT IATA,acity FROM airports WHERE aid=" . $this->db->escape($aiddd) . "";

$acity_row = $this->common->CustomQueryROw($citystrSQL);

if (!$this->common->pdf_check_date($prod['exp'])) {

$date_val = date('m/d/Y', strtotime($prod['exp']));
} elseif (strtotime($prod['exp']) == '') {

$date_val = "00/00/0000";
} elseif (is_null($prod['exp'])) {

$date_val = "00/00/0000";
} elseif ($prod['exp'] == '0000-00-00') {

$date_val = "00/00/0000";
} else {

$date_val = date('m/d/Y', strtotime($prod['exp']));
}

if (empty($prod['companyname'])) {

$compny = '';
} else {

$compny = ' (' . $prod['companyname'] . ')';
}

$date = $date_val;

$outletname = stripslashes($prod['outletname']) . $compny;

$acity = $acity_row['acity'];

$numlocations = $prod['numlocations'];

$sqft = number_format($prod['sqft']);

array_push($resourcearray, array('outletname' => $outletname, 'iata' => $acity_row['IATA'], 'acity' => $acity, 'numlocations' => $numlocations, 'sqft' => $sqft, 'date' => $date,));

array_push($indexarray, $date);

endforeach; // $prod

foreach ($resourcearray as $resource) {

$tbl_header.='<tr style="text-align:right;">

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resource['iata'] . '</td>  

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . stripslashes($resource['outletname']) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resource['acity'] . '</td>         

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . stripslashes($resource['sqft']) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . stripslashes($resource['date']) . '</td>

</tr>';
}

endforeach;

$tbl_header.='</table>';

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Lease Expiration 2
*/

/**
* 
* Start Top 50 Terminals 2 by Category (Required Category, Year)
* Category {Food/Beverage, Specialty Retail, News/Gifts, Duty Free & Passenger Services} 
*/
public function top50terminals2($reportfor, $year) {

$mpdf = new mPDF('utf-8', 'Letter');

if ($reportfor == 'SR') {

$reportfor = 'SR';

$reptitle = "Specialty Retail";
}

if ($reportfor == 'DF') {

$reportfor = 'DF';

$reptitle = "Duty Free";
}

if ($reportfor == 'NG') {

$reportfor = 'NG';

$reptitle = "News & Gifts";
}

if ($reportfor == 'FB') {

$reportfor = 'FB';

$reptitle = "Food and Beverage";
}

if ($reportfor == 'PT') {

$reportfor = 'PT';

$reptitle = "Passenger Traffic";
}

if ($reportfor == "AD") {

$reptitle = 'Advertising';
}

$airport = $this->common->CustomQueryALL('SELECT DISTINCT(`IATA`),`aid` as aid,`acity` FROM `airports` WHERE airport_is_active = 1');

$val = $year . ' Top ' . $this->limit . ' Terminals by ' . $reptitle;

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Arial, Helvetica, sans-serif;font-size:13pt;font-weight:bold;">  ' . $val . '</div>');

$tbl = '';

$tbl2 = '';

$tbl_header = '';

$tbl_header2 = '';

$users = array();

$TCurrentSqFt = 0;

$TGrossSale = 0;

$TSalesEp = 0;

$TRentRev = 0;

foreach ($airport as $AP) {

/*             * *************************************************************** */

$airportinfo = $this->common->GetSingleRowFromTableWhere('airports', array('aid' => $AP['aid'], 'airport_is_active' => 1));

$tl = $this->common->JoinTable('*', 'terminals', 'terminalsannual', 'terminalsannual.tid=terminals.tid', 'LEFT', "aid = '" . $AP['aid'] . "' AND terminalsannual.tyear='" . $year . "' ORDER BY terminals.tid");

$fbgrosssalestotal = 0;

$srgrosssalestotal = 0;

$nggrosssalestotal = 0;

$fbrentrevtotal = 0;

$srrentrevtotal = 0;

$ngrentrevtotal = 0;

$indi_terms_list = "";

if (count($tl) > 0) {

foreach ($tl as $_) {

if ($reportfor == 'AD') {

$CurrentSqFt = $_["dfcurrsqft"];

$GrossSale = $_["dfgrosssales"];

$SalesEp = $_["tpasstraffic"];

$RentRev = $_["dfrentrev"];
}

if ($reportfor == 'PT') {

$CurrentSqFt = $_["dfcurrsqft"];

$GrossSale = $_["dfgrosssales"];

$SalesEp = $_["tpasstraffic"];

$RentRev = $_["dfrentrev"];
}

if ($reportfor == 'DF') {

$CurrentSqFt = $_["dfcurrsqft"];

$GrossSale = $_["dfgrosssales"];

$SalesEp = $_["dfsalesep"];

$RentRev = $_["dfrentrev"];
}

if ($reportfor == 'SR') {

$CurrentSqFt = $_["srcurrsqft"];

$GrossSale = $_["srgrosssales"];

$SalesEp = $_["srsalesep"];

$RentRev = $_["srrentrev"];
}

if ($reportfor == 'FB') {

$CurrentSqFt = $_["fbcurrsqft"];

$GrossSale = $_["fbgrosssales"];

$SalesEp = $_["fbsalesep"];

$RentRev = $_["fbrentrev"];
}

if ($reportfor == 'NG') {

$CurrentSqFt = $_["ngcurrsqft"];

$GrossSale = $_["nggrosssales"];

$SalesEp = $_["ngsalesep"];

$RentRev = $_["ngrentrev"];
}

$TCurrentSqFt+=$CurrentSqFt;

$TGrossSale+=$GrossSale;

$TSalesEp+=$SalesEp;

$TRentRev+=$RentRev;

$users[] = array('IATA_ID' => $airportinfo['IATA'], 'Terminal_Abbr' => $AP["acity"],
'Terminal_Name' => $_["terminalname"],
'CurrentSqFt' => $CurrentSqFt, 'GrossSales' => $GrossSale,
'SalesEP' => $SalesEp, 'GrossRentals' => $RentRev);
} // END foreach ($tl as $_)    
}// END IF count($tl)>0

/*             * **************************************************************** */
} // END foreach($airport as $AP)

$songs = $this->common->subval_sort($users, 'SalesEP');

$tbl_header.='  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:11px; border-collapse: collapse;">

<tr  style="border-top:1px solid !important;"><td colspan="7"  style="border-top:1px solid !important;">&nbsp;</td>

<tr style="border-bottom:1px solid !important;" >



<th width="7%">IATA ID</th>

<th width="10%">City</th>

<th width="10%">Terminal Name</th>

<th width="10%">EPAX</th>

<th width="10%">Intl. EPAX</th>

<th width="10%">Current Sq. Ft.</th>

<th width="10%">Gross Sales</th>

<th width="10%">Current Sq. Ft.</th>

<th width="10%">Gross Sales</th>';



$tbl_header.='<th  width="17%">Sales/EP</th>';



$tbl_header.='



</tr><tr  style="border-bottom:1px solid !important;"><td colspan="7"  style="border-bottom:1px solid !important;">&nbsp;</td></tr>';

$counter = 0;

for ($i = count($songs) - 1; $i >= 0; $i--) {

$counter++;

if ($i % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

$tbl_header.=$class;

$tbl_header.='<td align="center">' . $songs[$i]["IATA_ID"] . '</td>



<td align="center">' . $songs[$i]["Terminal_Abbr"] . '</td>



<td align="center">' . $songs[$i]["Terminal_Name"] . '</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">' . number_format($songs[$i]["CurrentSqFt"], 2) . '</td>



<td align="center">$' . number_format($songs[$i]["GrossSales"], 2) . '</td>';

if ($reportfor == 'PT') {

$tbl_header.='<td align="center">$' . number_format($songs[$i]["SalesEP"]) . '</td>';
} else {

$tbl_header.='<td align="center">$' . $songs[$i]["SalesEP"] . '</td>';
}

$tbl_header.='</tr>';

if ($counter == $this->limit) {

break;
}
}

$tbl_header.='<tr><td colspan="7">&nbsp;</td></tr>';

$tbl_header.=$class . '<td align="left" width="7%"><strong>Total</strong></td><td width="13%">&nbsp;</td><td width="20%">&nbsp;</td><td width="17%" align="center"><strong>' . number_format($TCurrentSqFt, 2) . '</strong></td><td align="center"><strong>$' . number_format($TGrossSale, 2) . '</strong></td><td align="center"><strong>' . number_format($TSalesEp, 2) . '</strong></td><td align="center"><strong>$' . number_format($TRentRev, 2) . '</strong></td></tr>';

$tbl_header.='</table>';

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

//        $mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; 2014 Airport Revenue News, A Division of Urban Expositions</div>');

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');

exit;
}

/**
* 
* End Top 50 Terminals 2 by Category
*/

/**
* 
* Start All Airports (Required Year)
*/
public function allairport($year) {

$mpdf = new mPDF('utf-8', 'Letter');

$tbl_header = "";

$rcounter = 0;

$class = "";

$sql = "SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airports.airport_is_active = 1  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC";

$val = $year . ' All Airports';

$tbl_header = '<style type="text/css">tr.border_bottom td {

border-bottom:1pt solid black;

}</style>';



$ap_count = $this->common->CustomCountQuery($sql);

$res = $this->common->CustomQueryALL($sql);

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold;">  ' . $val . '</div>');

if ($ap_count > 0) {


$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"></table>';


$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">

<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Airports</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">IATA</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Enplanements</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">F&B Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Specialty Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">NG Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Total Sales (Excluding DF)</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Sales E/P</td>

</tr>';

foreach ($res as $airportlist):

$all = $airportlist["afbgrosssales"] + $airportlist["asrgrosssales"] + $airportlist["anggrosssales"];

if ($rcounter % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

//                number_format($airportlist["asalesep"], 2, '.', '')

$salesep = $all / $airportlist["aenplaning"];

$tbl_header.=$class;

$tbl_header.='<td style="width:25%;text-align:left;padding:3px 20px 3px 0;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $airportlist["acity"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist["IATA"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["aenplaning"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["afbgrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["asrgrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["anggrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($all) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($salesep, 2, '.', '') . '</td>

</tr>';

$rcounter++;


endforeach;

$tbl_header.='</table>';
} else {

$tbl_header = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

$tbl_header.='<tr><td style="font-family:verdana;color:red;">No Data Update...</td></tr></table>';
}  // END if(count($ap_count)>0)            

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End All Airports
*/

/**
* 
* Start Ratio Report (Required Year)
*/
public function ratioreport($year) {

$mpdf = new mPDF('utf-8', 'Letter');

$airport = $this->common->SelectdistinctWhere('airports', '(`IATA`),aid as aid,acity', array('airport_is_active' => 1));

$val = $year . ' Ratios Report';

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Arial, Helvetica, sans-serif;font-size:13pt;font-weight:bold;">  ' . $val . '</div>');

$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="font-size:11px;">';

$tbl_header.='<tr><td colspan="5" style="border-bottom:1px solid black;"></td></tr>';

$tbl_header.='<tr>

<td>&nbsp;</td>

<tr style="border-bottom:1px solid !important;">        

<td width="10%"><b>IATA ID</b></td>     

<td width="30%"><b>City</b></td>        

<td width="20%"><b>Business to Leisure<br/>Ratio</b></td>       

<td width="20%"><b>Pre/Post Security<br/>Ratio</b></td>     

<td  width="20%"><b>Avg Dwell Time<br/>(Minutes)</b></td></tr>';

$tbl_header.='<tr><td colspan="5" style="border-bottom:1px solid black;"></td></tr>';

$rcounter = 0;

foreach ($airport as $AP):

$join_array2 = array(
array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT')
);

$select = "airports.IATA,airports.acity,airportsannual.presecurity,airportsannual.postsecurity,airportsannual.ratiobusleisure,airportsannual.avgdwelltime";

$where = array('airports.aid' => $AP['aid'], 'airportsannual.ayear' => $year, 'airports.airport_is_active' => 1);

$info = $this->common->JoinTables($select, 'airports', $join_array2, $where, "airports.aid");


foreach ($info as $in) {

if ($rcounter % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

if ($in["ratiobusleisure"] != '' && $in["ratiobusleisure"] != '0') {
$ratiobusleisure = $in["ratiobusleisure"];
} else {
$ratiobusleisure = '0/0';
}

if ($in["presecurity"] != '') {
$presecurity = $in["presecurity"];
} else {
$presecurity = '0';
}

if ($in["postsecurity"] != '') {
$postsecurity = $in["postsecurity"];
} else {
$postsecurity = '0';
}

$tbl_header.=$class . '

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;">' . $in['IATA'] . '</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;">' . $in['acity'] . '</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;">' . $ratiobusleisure . '</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;">' . $presecurity . '/' . $postsecurity . '</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;">' . $in['avgdwelltime'] . '</td>

</tr>';

$rcounter++;
}

endforeach;

$tbl_header.='</table>';

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Ratio Report
*/

/**
* 
* Start Top 50 Terminals by Category (Required Category, Year)
* Category {Food/Beverage, Specialty Retail, News/Gifts, Duty Free & Passenger Services} 
*/
public function top50terminals($reportfor, $year) {

$mpdf = new mPDF('utf-8', 'Letter');

if ($reportfor == 'SR') {

$reportfor = 'SR';

$reptitle = "Specialty Retail";
}

if ($reportfor == 'DF') {

$reportfor = 'DF';

$reptitle = "Duty Free";
}

if ($reportfor == 'NG') {

$reportfor = 'NG';

$reptitle = "News & Gifts";
}

if ($reportfor == 'FB') {

$reportfor = 'FB';

$reptitle = "Food and Beverage";
}

if ($reportfor == 'PT') {

$reportfor = 'PT';

$reptitle = "Passenger Traffic";
}

if ($reportfor == "AD") {

$reptitle = 'Advertising';
}

$airport = $this->common->SelectdistinctWhere('airports', '(`IATA`),aid as aid,acity', array('airport_is_active' => 1));

$val = $year . ' Top ' . $this->limit . ' Terminals by ' . $reptitle;

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Arial, Helvetica, sans-serif;font-size:13pt;font-weight:bold;">  ' . $val . '</div>');

$tbl = '';

$tbl2 = '';

$tbl_header = '';

$tbl_header2 = '';

$users = array();

$TCurrentSqFt = 0;

$TGrossSale = 0;

$TSalesEp = 0;

$TRentRev = 0;

foreach ($airport as $AP) {

/*             * *************************************************************** */

//              $airportinfo=$this->common->GetSingleRowFromTableWhere('airports',array('aid'=>$AP['aid'], 'airport_is_active'=>1));    

$airportinfo = $this->common->GetSingleRowFromTableWhere('airports', array('aid' => $AP['aid']));

$tl = $this->common->JoinTable('*', 'terminals', 'terminalsannual', 'terminalsannual.tid=terminals.tid', 'LEFT', "aid = '" . $AP['aid'] . "' AND terminalsannual.tyear='" . $year . "' ORDER BY terminals.tid");

$fbgrosssalestotal = 0;

$srgrosssalestotal = 0;

$nggrosssalestotal = 0;

$fbrentrevtotal = 0;

$srrentrevtotal = 0;

$ngrentrevtotal = 0;


$indi_terms_list = "";

if (count($tl) > 0) {

foreach ($tl as $_) {

if ($reportfor == 'AD') {

$CurrentSqFt = $_["dfcurrsqft"];

$GrossSale = $_["dfgrosssales"];

$SalesEp = $_["tpasstraffic"];

$RentRev = $_["dfrentrev"];
}

if ($reportfor == 'PT') {

$CurrentSqFt = $_["dfcurrsqft"];

$GrossSale = $_["dfgrosssales"];

$SalesEp = $_["tpasstraffic"];

$RentRev = $_["dfrentrev"];
}

if ($reportfor == 'DF') {

$CurrentSqFt = $_["dfcurrsqft"];

$GrossSale = $_["dfgrosssales"];

$SalesEp = $_["dfsalesep"];

$RentRev = $_["dfrentrev"];
}

if ($reportfor == 'SR') {

$CurrentSqFt = $_["srcurrsqft"];

$GrossSale = $_["srgrosssales"];

$SalesEp = $_["srsalesep"];

$RentRev = $_["srrentrev"];
}

if ($reportfor == 'FB') {

$CurrentSqFt = $_["fbcurrsqft"];

$GrossSale = $_["fbgrosssales"];

$SalesEp = $_["fbsalesep"];

$RentRev = $_["fbrentrev"];
}

if ($reportfor == 'NG') {

$CurrentSqFt = $_["ngcurrsqft"];

$GrossSale = $_["nggrosssales"];

$SalesEp = $_["ngsalesep"];

$RentRev = $_["ngrentrev"];
}

$TCurrentSqFt+=$CurrentSqFt;

$TGrossSale+=$GrossSale;

$TSalesEp+=$SalesEp;

$TRentRev+=$RentRev;

$users[] = array('IATA_ID' => $airportinfo['IATA'], 'Terminal_Abbr' => $AP["acity"],
'Terminal_Name' => $_["terminalname"],
'CurrentSqFt' => $CurrentSqFt, 'GrossSales' => $GrossSale,
'SalesEP' => $SalesEp, 'GrossRentals' => $RentRev);
} // END foreach ($tl as $_)    
}// END IF count($tl)>0

/*             * **************************************************************** */
} // END foreach($airport as $AP)

$songs = $this->common->subval_sort($users, 'SalesEP');

$tbl_header.='  <table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="font-size:11px;">

<tr  style="border-top:1px solid !important;"><td colspan="7"  style="border-top:1px solid !important;">&nbsp;</td>

<tr style="border-bottom:1px solid !important;" >



<th width="7%">IATA ID</th>



<th width="13%">City</th>



<th width="20%">Terminal Name</th>



<th width="17%">Current Sq. Ft.</th>



<th  width="17%">Gross Sales</th>';

if ($reportfor == 'PT') {

$tbl_header.='<th  width="17%">Passenger Traffic</th>';
} else {

$tbl_header.='<th  width="17%">Sales/EP</th>';
}

$tbl_header.='

<th  width="17%">Gross Rentals</th>



</tr><tr  style="border-bottom:1px solid !important;"><td colspan="7"  style="border-bottom:1px solid !important;">&nbsp;</td></tr>';

$counter = 0;

$nomiCurrentSqFt = 0;

for ($i = count($songs) - 1; $i >= 0; $i--) {

$counter++;

if ($i % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

$tbl_header.=$class;

$tbl_header.='<td align="center">' . $songs[$i]["IATA_ID"] . '</td>



<td align="center">' . $songs[$i]["Terminal_Abbr"] . '</td>



<td align="center">' . $songs[$i]["Terminal_Name"] . '</td>



<td align="center">' . number_format($songs[$i]["CurrentSqFt"], 2) . '</td>



<td align="center">$' . number_format($songs[$i]["GrossSales"], 2) . '</td>';

if ($reportfor == 'PT') {

$tbl_header.='<td align="center">' . number_format($songs[$i]["SalesEP"]) . '</td>';
} else {

$tbl_header.='<td align="center">$' . number_format($songs[$i]["SalesEP"], 2) . '</td>';
}

$tbl_header.='<td align="center">$' . number_format($songs[$i]["GrossRentals"], 2) . '</td>

</tr>';

$nomiCurrentSqFt+=$songs[$i]["CurrentSqFt"];

$nomiGrossSales+=$songs[$i]["GrossSales"];

$nomiSalesEP+=$songs[$i]["SalesEP"];

$nomiGrossRentals+=$songs[$i]["GrossRentals"];

if ($counter == $this->limit) {

break;
}
}

if ($songs[$i]["SalesEP"] == "") {

$tbl_header.='<tr><td align="left" colspan="5" style="color:red;font-size:24px;font-family:verdana;font-weight:normal;"><h1>No Result Updated.</h1></td></tr>';
}

$tbl_header.='<tr><td colspan="7">&nbsp;</td></tr>';

$tbl_header.=$class . '<td align="left" width="7%"><strong>Total</strong></td><td width="13%">&nbsp;</td><td width="20%">&nbsp;</td><td width="17%" align="center"><strong>' . number_format($nomiCurrentSqFt, 2) . '</strong></td><td align="center"><strong>$' . number_format($nomiGrossSales, 2) . '</strong></td><td align="center"><strong>' . number_format($nomiSalesEP, 2) . '</strong></td><td align="center"><strong>$' . number_format($nomiGrossRentals, 2) . '</strong></td></tr>';

$tbl_header.='</table>';

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');

exit;
}

/**
* 
* End Top 50 Terminals by Category
*/

/**
* 
* Start Lease Expiration (Required Year) 
*/
public function leaseexpire($year, $type) {

$mpdf = new mPDF('utf-8', 'Letter');

$tbl_header = "";

$rcounter = 0;

$class = "";

$count = 0;

$val = 'Leases Due to Expire by Year-End ' . $year;

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:10pt;font-weight:bold;">  ' . $val . '</div>');

$tbl_header.='<table width="100%" border="0" style="text-align:left; border-collapse: collapse;" cellspacing="0" cellpadding="0" >';

$list = $this->common->Selectdistinct('lkpcategory', 'category');

$nr_of_users = $this->common->GetRecordCount('outlets');


foreach ($list as $cmpnyterms) {

$catid = "";

$catid = $cmpnyterms['category'];

$ref = trim($year);

$products = $this->common->CustomQueryALL("select outlets.companyname, outlets.aid, outlets.oid, outlets.numlocations, outlets.sqft, outlets.outletname, airports.acity, "
. "
case when length(date(str_to_date(outlets.exp,'%Y-%m-%d'))) is not NULL then str_to_date(outlets.exp,'%Y-%m-%d')
when length(date(str_to_date(outlets.exp,'%b %d %Y'))) is not NULL then str_to_date(outlets.exp,'%b %d %Y')
when length(date(str_to_date(outlets.exp,'%m/%d/%Y'))) is not NULL then str_to_date(outlets.exp,'%m/%d/%Y')
when length(date(str_to_date(outlets.exp,'%Y/%m/%d'))) is not NULL then str_to_date(outlets.exp,'%Y/%m/%d')
end as exp"
. " from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid 
left join   airports ON outlets.aid =   airports.aid 
where lkpcategory.category =" . $this->db->escape($catid) . " and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and airports.airport_is_active = 1 and outlets.exp LIKE '%" . $this->db->escape_like_str($ref) . "%' order by exp, acity ASC ");


$tbl_header.='<tr style="text-align:right;">

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $catid . '</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">&nbsp;</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">&nbsp;</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">&nbsp;</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-family:Vectora LH 95 Black,arial;">' . date("Y-m-d H:i:s") . '</td>

</tr>

<tr style="text-align:right;">

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Outlet Name/Description(Company)</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Airport Location</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;"># </td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Sq. Ft.</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Expires</td>

</tr>';


//            foreach ($products as $prod):

if (count($products) > 0)
$resourcearray = array();

$indexarray = array();


foreach ($products as $prod) {


$aiddd = $prod['aid'];

$acity_row = $prod['acity'];


$date_val = date('m/d/Y', strtotime($prod['exp']));


if (empty($prod['companyname'])) {

$compny = '';
} else {

$compny = ' (' . $prod['companyname'] . ')';
}

$date = $date_val;

$outletname = stripslashes($prod['outletname']) . $compny;

//                $acity_row = $acity_row['acity'];

$numlocations = $prod['numlocations'];

$sqft = number_format($prod['sqft']);


$tbl_header.='<tr style="text-align:right;">

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . stripslashes($outletname) . '</td>          

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $acity_row . '</td>         

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . strtoupper(substr($numlocations, 0, 1)) . '</td>            

<td style="text-align:right;padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $sqft . '</td>

<td style="text-align:right;padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $date . '</td>

</tr>';


$count = $count + 1;
}
}

$tbl_header.='</table>';

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$tbl_header = '';

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Lease Expiration
*/

/**
* 
* Start Top 50 Airports (Required Year) 
*/
public function top50airport($year, $type) {

$mpdf = new mPDF('utf-8', 'Letter');

$tbl_header = "";

$rcounter = 0;

$class = "";

$sql = "SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airporttotals.ayear=" . $this->db->escape($year) . " AND airports.IATA!='YYJ' AND airports.IATA!='YVR' AND airports.airport_is_active = 1  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC LIMIT 0,50";

$val = $year . ' Top ' . $this->limit . ' Performing North American Airports';

$ap_count = $this->common->CustomCountQuery($sql);

$res = $this->common->CustomQueryALL($sql);

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold;">  ' . $val . '</div>');

if ($ap_count > 0) {

$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"></table>';


$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">

<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Top 50 Airports</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">IATA</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Enplanements</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">F&B Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Specialty Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">NG Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Total Sales (Excluding DF)</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Sales E/P</td>

</tr>';

foreach ($res as $airportlist):

$all = $airportlist["afbgrosssales"] + $airportlist["asrgrosssales"] + $airportlist["anggrosssales"];

if ($rcounter % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

//                number_format($airportlist["asalesep"], 2, '.', '')

$salesep = $all / $airportlist["aenplaning"];

$tbl_header.=$class;

$tbl_header.='<td style="width:25%;text-align:left;padding:3px 20px 3px 0;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $airportlist["acity"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist["IATA"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["aenplaning"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["afbgrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["asrgrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["anggrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($all) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($salesep, 2, '.', '') . '</td>

</tr>';

$aenplaning+=$airportlist["aenplaning"];

$afbgrosssales+=$airportlist["afbgrosssales"];

$asrgrosssales+=$airportlist["asrgrosssales"];

$anggrosssales+=$airportlist["anggrosssales"];

$aconcessiongrosssales+=$all;

$avg_sp+=$airportlist["asalesep"] / $ap_count;

$rcounter++;

if ($rcounter == $this->limit) {

break;
}

endforeach;

$tbl_header.='<tr>

<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>';

if ($aenplaning) {

$tot_aenplaning = number_format($aenplaning);
} else {

$tot_aenplaning = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:right;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tot_aenplaning . '</td>';

if ($afbgrosssales) {

$fb_afbgrosssales = '$' . number_format($afbgrosssales);
} else {

$fb_afbgrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_afbgrosssales . '</td>';

if ($asrgrosssales) {

$sr_asrgrosssales = '$' . number_format($asrgrosssales);
} else {

$sr_asrgrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_asrgrosssales . '</td>';

if ($anggrosssales) {

$ng_anggrosssales = '$' . number_format($anggrosssales);
} else {

$ng_anggrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $ng_anggrosssales . '</td>';

if ($aconcessiongrosssales) {

$to_aconcessiongrosssales = '$' . number_format($aconcessiongrosssales);
} else {

$to_aconcessiongrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $to_aconcessiongrosssales . '</td>';

if ($avg_sp) {

$to_avg_sp = '$' . number_format($avg_sp, 2, '.', '');
} else {

$to_avg_sp = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">AVG=' . $to_avg_sp . '</td></tr></table>';

unset($aenplaning);

unset($afbgrosssales);

unset($asrgrosssales);

unset($anggrosssales);

unset($aconcessiongrosssales);

unset($avg_sp);
} else {

$tbl_header = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

$tbl_header.='<tr><td style="font-family:verdana;color:red;">No Data Update...</td></tr></table>';
}  // END if(count($ap_count)>0)            

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Top 50 Airports
*/

/**
* 
* Start Top International Airports (Required Year) 
*/
public function topIntairport($year, $type) {

$mpdf = new mPDF('utf-8', 'Letter');

$tbl_header = "";

$rcounter = 0;

$class = "";

$sql = "SELECT *,((afbgrosssales+asrgrosssales+anggrosssales+adfgrosssales)/aenplaning) myvar

FROM airporttotals

INNER JOIN airports ON airporttotals.aid = airports.aid

WHERE airporttotals.adfgrosssales!=0 OR airports.IATA='YVR' OR airports.IATA='YYJ' 

ORDER BY CAST(myvar AS DECIMAL( 28, 4 ) ) DESC LIMIT 0,50";

$val = $year . ' International Airports by Performance';

$ap_count = $this->common->CustomCountQuery($sql);

$res = $this->common->CustomQueryALL($sql);

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold;">  ' . $val . '</div>');

if ($ap_count > 0) {

$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>

<td style="text-align:center;padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">(Includes duty free sales)</td>

</tr></table>';

$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">

<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">IATA</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Enplanements</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">F&B Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Specialty Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">NG Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">DF Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Total Sales</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Sales E/P</td>

</tr>';

foreach ($res as $airportlist):

$all = $airportlist["afbgrosssales"] + $airportlist["asrgrosssales"] + $airportlist["anggrosssales"] + $airportlist["adfgrosssales"];

if ($rcounter % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

//                number_format($airportlist["asalesep"], 2, '.', '')

$salesep = $all / $airportlist["aenplaning"];

$tbl_header.=$class;

$tbl_header.='<td style="width:25%;text-align:left;padding:3px 20px 3px 0;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $airportlist["acity"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist["IATA"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["aenplaning"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["afbgrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["asrgrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["anggrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["adfgrosssales"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($all) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($salesep, 2, '.', '') . '</td>

</tr>';

$aenplaning+=$airportlist["aenplaning"];

$afbgrosssales+=$airportlist["afbgrosssales"];

$asrgrosssales+=$airportlist["asrgrosssales"];

$anggrosssales+=$airportlist["anggrosssales"];

$adfgrosssales+=$airportlist["adfgrosssales"];

$aconcessiongrosssales+=$all;

$avg_sp+=$salesep / $ap_count;

$rcounter++;

if ($rcounter == $this->limit) {

break;
}

endforeach;

$tbl_header.='<tr>

<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>';

if ($aenplaning) {

$tot_aenplaning = number_format($aenplaning);
} else {

$tot_aenplaning = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tot_aenplaning . '</td>';

if ($afbgrosssales) {

$fb_afbgrosssales = '$' . number_format($afbgrosssales);
} else {

$fb_afbgrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_afbgrosssales . '</td>';

if ($asrgrosssales) {

$sr_asrgrosssales = '$' . number_format($asrgrosssales);
} else {

$sr_asrgrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_asrgrosssales . '</td>';

if ($anggrosssales) {

$ng_anggrosssales = '$' . number_format($anggrosssales);
} else {

$ng_anggrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $ng_anggrosssales . '</td>';


if ($adfgrosssales) {

$df_adfgrosssales = '$' . number_format($adfgrosssales);
} else {

$df_adfgrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $df_adfgrosssales . '</td>';


if ($aconcessiongrosssales) {

$to_aconcessiongrosssales = '$' . number_format($aconcessiongrosssales);
} else {

$to_aconcessiongrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $to_aconcessiongrosssales . '</td>';

if ($avg_sp) {

$to_avg_sp = '$' . number_format($avg_sp, 2, '.', '');
} else {

$to_avg_sp = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">AVG=' . $to_avg_sp . '</td></tr></table>';

unset($aenplaning);

unset($afbgrosssales);

unset($asrgrosssales);

unset($anggrosssales);

unset($adfgrosssales);

unset($aconcessiongrosssales);

unset($avg_sp);
} else {

$tbl_header = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

$tbl_header.='<tr><td style="font-family:verdana;color:red;">No Data Update...</td></tr></table>';
}  // END if(count($ap_count)>0)            

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Top International Airports
*/

/**
* 
* Start Top 10 Car Rental Airports (Required Year) 
*/
public function top10carrental($year, $type) {

$mpdf = new mPDF('utf-8', 'Letter');

$tbl_header = "";

$rcounter = 0;

$class = "";

$val = $year . ' Top ' . $this->limit . ' Airports Based on Car Rental Revenue';

$sql = "SELECT *, (carrentalrevonsite) myvar

FROM airportsannual

INNER JOIN airports ON airportsannual.aid = airports.aid

WHERE airportsannual.carrentalrevonsite!=0  AND airports.airport_is_active = 1 


ORDER BY CAST(myvar AS DECIMAL( 28, 4 ) ) DESC LIMIT 0,10";


$val = $year . ' Top 10 Airports Based on Car Rental Revenue';


$ap_count = $this->common->CustomCountQuery($sql);

$res = $this->common->CustomQueryALL($sql);

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold;">  ' . $val . '</div>');

if ($ap_count > 0) {

if ($type == "TIA") {

$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>

<td style="text-align:center;padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">(Includes duty free sales)</td>

</tr></table>';
} else {

$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"></table>';
}

$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">

<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Top 10 Airports</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">IATA</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Car Rental On Site</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Gross Revenue On Site</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Car Rental Off Site</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Gross Revenue Off Site</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Total Car Rental Revenue</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Car Rental Sq. Ft</td>

</tr>';

foreach ($res as $airportlist):

$all = $airportlist["carrentalrevonsite"] + $airportlist["carrentalrevoffsite"];

if ($rcounter % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

$tbl_header.=$class;

$tbl_header.='<td style="width:25%;text-align:left;padding:3px 20px 3px 0;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $airportlist["acity"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist["IATA"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["carrentalagenciesonsite"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["carrentalrevonsite"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["carrentalagenciesoffsite"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($airportlist["carrentalrevoffsite"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">$' . number_format($all) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["carrentalsqft"], 2, '.', '') . '</td>

</tr>';

$aenplaning+=$airportlist["carrentalagenciesonsite"];

$afbgrosssales+=$airportlist["carrentalrevonsite"];

$asrgrosssales+=$airportlist["carrentalagenciesoffsite"];

$anggrosssales+=$airportlist["carrentalrevoffsite"];

$aconcessiongrosssales+=$all;

$avg_sp+=$airportlist["carrentalsqft"] / $ap_count;

$rcounter++;

if ($rcounter == $this->limit) {

break;
}

endforeach;

$tbl_header.='<tr>

<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>';

if ($aenplaning) {

$tot_aenplaning = number_format($aenplaning);
} else {

$tot_aenplaning = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 18px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">$' . $tot_aenplaning . '</td>';

if ($afbgrosssales) {

$fb_afbgrosssales = '$' . number_format($afbgrosssales);
} else {

$fb_afbgrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_afbgrosssales . '</td>';

if ($asrgrosssales) {

$sr_asrgrosssales = '$' . number_format($asrgrosssales);
} else {

$sr_asrgrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 18px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_asrgrosssales . '</td>';

if ($anggrosssales) {

$ng_anggrosssales = '$' . number_format($anggrosssales);
} else {

$ng_anggrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $ng_anggrosssales . '</td>';

if ($aconcessiongrosssales) {

$to_aconcessiongrosssales = '$' . number_format($aconcessiongrosssales);
} else {

$to_aconcessiongrosssales = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $to_aconcessiongrosssales . '</td>';

if ($avg_sp) {

$to_avg_sp = '$' . number_format($avg_sp, 2, '.', '');
} else {

$to_avg_sp = '';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">AVG=' . $to_avg_sp . '</td></tr></table>';

unset($aenplaning);

unset($afbgrosssales);

unset($asrgrosssales);

unset($anggrosssales);

unset($aconcessiongrosssales);

unset($avg_sp);
} else {

$tbl_header = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

$tbl_header.='<tr><td style="font-family:verdana;color:red;">No Data Update...</td></tr></table>';
}  // END if(count($ap_count)>0)            

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Top 10 Car Rental Airports
*/

/**
* 
* Start Total Passengers of All Airports (Required Year) 
*/
public function totalairportpassenger($year, $type) {

$mpdf = new mPDF('utf-8', 'Letter');

$tbl_header = "";

$rcounter = 0;

$class = "";


$sql = "SELECT * FROM airports LEFT JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.apasstraffic!=0 AND airporttotals.ayear=" . $this->db->escape($year) . " AND airports.airport_is_active = 1  ORDER BY cast(airporttotals.apasstraffic AS decimal( 38, 10 )) DESC";


$val = $year . ' Airports Passenger Traffic';

$ap_count = $this->common->CustomCountQuery($sql);

$res = $this->common->CustomQueryALL($sql);

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold;">  ' . $val . '</div>');


if ($ap_count > 0) {


$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">

<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Airports</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">IATA</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Passenger Traffic</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Enplanements</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Deplaning</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">EP Domestic</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">EP Int`l</td>

</tr>';

foreach ($res as $airportlist):

if ($rcounter % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

$tbl_header.=$class;

$tbl_header.='<td style="width:25%;text-align:left;padding:3px 20px 3px 0;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $airportlist["acity"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist["IATA"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["apasstraffic"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["aenplaning"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["adeplaning"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["aepdomestic"]) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist["aepintl"]) . '</td>

</tr>';

$apasstraffic+=$airportlist["apasstraffic"];

$aenplaning+=$airportlist["aenplaning"];

$adeplaning+=$airportlist["adeplaning"];

$aepdomestic+=$airportlist["aepdomestic"];

$aepintl+=$airportlist["aepintl"];


$rcounter++;


endforeach;

$tbl_header.='<tr>

<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>';

if ($apasstraffic) {

$tot_apasstraffic = number_format($apasstraffic);
} else {

$tot_apasstraffic = '0';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tot_apasstraffic . '</td>';

if ($aenplaning) {

$tot_aenplaning = number_format($aenplaning);
} else {

$tot_aenplaning = '0';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tot_aenplaning . '</td>';

if ($adeplaning) {

$tot_adeplaning = number_format($adeplaning);
} else {

$tot_adeplaning = '0';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tot_adeplaning . '</td>';

if ($aepdomestic) {

$tot_aepdomestic = number_format($aepdomestic);
} else {

$tot_aepdomestic = '0';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tot_aepdomestic . '</td>';

if ($aepintl) {

$tot_aepintl = number_format($aepintl);
} else {

$tot_aepintl = '0';
}

$tbl_header.='<td style="padding:3px 20px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tot_aepintl . '</td>';


$tbl_header.='</tr></table>';

unset($apasstraffic);

unset($aenplaning);

unset($adeplaning);

unset($aepdomestic);

unset($aepintl);
} else {

$tbl_header = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

$tbl_header.='<tr><td style="font-family:verdana;color:red;">No Data Update...</td></tr></table>';
}  // END if(count($ap_count)>0)            

$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);

$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Total Passengers of All Airports
*/

/**
* 
* Start Total Tenants
*/
public function total_tenants() {

$p = $this->input->post('numofloop');
//        $list = $this->input->post('total');

$tbl_header1 = file_get_contents("./uploads/test.txt");
$h = '<table width="100%" border="0" cellspacing="0" cellpadding="0" border-collapse: collapse; style="text-align:right "> <tr class="trs"> <td class="tds">IATA Code</td> <td class="tds">Terminal Location</td> <td class="tds">Product Category</td> <td class="tds">Tenant Name</td> <td class="tds">Product Description</td> <td class="tds">Company Name</td> <td class="tds">#Locations</td> <td class="tds">Square Footage</td> <td class="tds">Lease Expires</td> </tr>';
$tbl_header1 = str_replace(array($h, '</table>'), '', $tbl_header1);

$list = explode('</tr>', $tbl_header1);

$mpdf = new mPDF('c');
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($tbl_header1, 1);
$line = 2;
$start = ($p - 1) * 1000;
$end = $start + 1000;
$heading = "";
for ($j = $start; $j < $end; $j++) {
if ($heading == "") {
$list[$j] = '<table width="100%" border="1" cellspacing="0" cellpadding="0" border-collapse: collapse; style="text-align:right;font-family:verdana;"><tr class="trs"> <th class="tds">IATA Code</th> <th class="tds">Terminal Location</th><th class="tds">Product Category</th><th class="tds">Tenant Name</th><th class="tds">Product Description</th><th class="tds">Company Name</th><th class="tds">#Locations</th><th class="tds">Square Footage</th> <th class="tds">Lease Expires</th></tr>' . $list[$j];
$heading = 1;
}

$var = $list[$j] . "</tr>";


$mpdf->WriteHTML($var, $line);
$line++;
}

$var = "";
$mpdf->WriteHTML("</table>", $start + 2);
//  $mpdf->Output('pdf/'.$p.'.pdf','F');
$mpdf->Output('./uploads/' . $p . '.pdf', 'F');


exit;









/*

$mpdf = new mPDF('utf-8', 'Letter');

$tbl_header = "";

$rcounter = 0;

$class = "";


//    $sql = "SELECT * FROM airports LEFT JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.apasstraffic!=0 AND airporttotals.ayear='" . $year . "' AND airports.airport_is_active = 1  ORDER BY cast(airporttotals.apasstraffic AS decimal( 38, 10 )) DESC";

$sql = "select * ,lkpcategory.category as category from outlets

left join lkpcategory on lkpcategory.categoryid=outlets.categoryid

left join airports on airports.aid = outlets.aid WHERE airports.airport_is_active = 1";
//                        left join airports on airports.aid = outlets.aid WHERE airports.airport_is_active = 1 LIMIT 2000,1600";
//                        left join airports on airports.aid = outlets.aid WHERE airports.airport_is_active = 1 ORDER BY {$tblcolumnsarray[$columnsnumber]} {$ordertype} " . $LIMIT;
//        $sql = "SELECT * FROM airports LEFT JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.apasstraffic!=0 AND airporttotals.ayear=2010 AND airports.airport_is_active = 1  ORDER BY cast(airporttotals.apasstraffic AS decimal( 38, 10 )) DESC";
//echo $sql; exit;
$val = 'ARN FactBook all Tenants Record';

$ap_count = $this->common->CustomCountQuery($sql);

$this->outputData['r_loop'] = $ap_count/1000;

//        $r_loop = $ap_count/1000;

$res = $this->common->CustomQueryALL($sql);

$mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold;">  ' . $val . '</div>');

if ($ap_count > 0) {


$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">

<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">IATA Code</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Terminal Location</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Product Category</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Tenant Name</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Product Description</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Company Name</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">#Locations</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Square Footage</td>

<td style="padding:3px 20px 3px 0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">Lease Expires</td>

</tr>';

foreach ($res as $tenant_list):

if ($tenant_list['exp'] != "" && (!preg_match("/^[a-z\s\-\/]+$/i", $tenant_list['exp']))) {

$expiredate = $this->common->get_formatted_datetime($tenant_list['exp']);
} else {

$expiredate = $tenant_list['exp'];
}

if ($rcounter % 2 == 0) {

$class = '<tr>';
} else {

$class = '<tr style="background-color:#EEEEEE;">';
}

$tbl_header.=$class;


$tbl_header.='<td style="width:25%;text-align:left;padding:3px 20px 3px 0;font-size:8pt;font-weight:bold;font-family:Vectora LH 95 Black,arial;">' . $tenant_list["IATA"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $tenant_list["termlocation"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $tenant_list["category"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $tenant_list["outletname"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $tenant_list["productdescription"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $tenant_list["companyname"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $tenant_list["numlocations"] . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($tenant_list["sqft"], 2) . '</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $expiredate . '</td>

</tr>';

$rcounter++;


endforeach;

$tbl_header.='</table>';
} else {

$tbl_header = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

$tbl_header.='<tr><td style="font-family:verdana;color:red;">No Data Update...</td></tr></table>';
}  // END if(count($ap_count)>0)
//        echo json_encode(1);


$str = preg_replace('/\s\s+/', ' ', $tbl_header);

$mpdf->WriteHTML($str);
//        if ($_SERVER['REMOTE_ADDR'] == "202.141.226.196") {
//                    echo 'test ';
//                    exit;
//                }
$mpdf->defaultfooterline = 0;

$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '</div>');

$mpdf->Output($val . '.pdf', 'D');
//        $mpdf->Output($val . '.pdf', 'F');
//        $mpdf->Output($path, "F");
//        $mpdf->Output('uploads/' . $val . '.pdf', "F");
//        echo 1;

exit */
}

/**
* 
* End Total Tenants
*/
/*
public function exporttodoc() {

$compnaytype = $this->common->GetCurrentUserInfo('cc');

$listofcompanies = $this->common->TableGetAllOrderByWhere('companies', array('companytype' => $compnaytype), 'companyname', 'ASC');

header("Content-type: application/vnd.ms-word");

header("Content-Disposition: attachment;Filename=document_name.doc");

echo "<html>";

echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";

echo "<body>";

foreach ($listofcompanies as $mycom):

//print_r($mycom);
//"SELECT * FROM companycontacts left join companies on companycontacts.cid=companies.cid where companycontacts.cid = '$_SESSION[cid]' AND companycontacts.printed='Yes'";

$cont = $this->common->JoinTable('*', 'companycontacts', 'companies', 'companycontacts.cid=companies.cid', 'LEFT', array('companycontacts.cid' => $mycom['cid'], 'companycontacts.printed' => 'Yes'));

$comp = $this->common->CustomQueryALL("SELECT companylocations.cid,companylocations.aid,companylocations.tid,companylocations.IATA,airports.aname,airports.acity,airports.astate,airports.acountry FROM companies

left join companylocations on companylocations.cid = companies.cid

left join airports on airports.aid = companylocations.aid

left join terminals on terminals.tid = companylocations.tid

where companies.cid=" . $mycom['cid'] . " order by airports.acity");

$comp_ = $this->common->CustomQueryALL("SELECT lkpairportlist.aid,lkpairportlist.IATA,lkpairportlist.acity,lkpairportlist.astate,lkpairportlist.aname,lkpairportlist.acountry,companylocations.cid,companylocations.aid,companylocations.tid,companylocations.IATA FROM companies

left join companylocations on companylocations.cid = companies.cid

left join lkpairportlist on lkpairportlist.aid = companylocations.aid

where companies.cid='" . $mycom['cid'] . "' order by lkpairportlist.acity");

$whoopee = array_merge($comp, $comp_);

$this->common->array_sort_by_column($whoopee, 'acity');

echo $cont[0]['companyname'];

echo "<br>";

foreach ($cont as $result) {

echo $result['cfname'] . " " . $result['clname'];

echo "<br>";

echo $result['ctitle'];

echo "<br>";

echo $result['caddress1'];

echo "<br>";

if ($result['caddress2']) {

echo $result['caddress2'];

echo "<br>";
}

echo $result['ccity'] . ", " . $result['cstate'] . " " . $result['czip'] . " " . $result['ccountry'];

echo "<br>";

if ($result['cphone']) {

echo "Phone: " . $result['cphone'];

echo "<br>";
}

if ($result['cfax']) {

echo "Fax: " . $result['cfax'];

echo "<br>";
}

if ($result['cemail']) {

echo "Email: <a href='mailto:" . $result['cemail'] . "'>" . $result['cemail'] . "</a>";

echo "<br>";
}

if ($result['cwebsite']) {

echo "Website: <a href='" . $result['cwebsite'] . "'>" . $result['cwebsite'] . "</a>";
}

echo "<br><br><br>";
}

echo "<div style='display:block;margin:0;padding:0;width:100%;'><pre style='font-family:times new roman;font-size:17px;'>" . iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $result['companyinfo']) . "</pre></div>";

echo "<br>";

foreach ($whoopee as $loc) {

if ($loc['acity'] != '' && $loc['astate'] != '' && $loc['aname'] != '') {

echo $loc['acity'] . ', ' . $loc['astate'] . ' (' . $loc['aname'] . ')';

echo "<br>";
}
}



echo "</div>";



echo "<br style='page-break-before: always;' clear='all' />";

endforeach;

echo "</body>";

echo "</html>";
}
*/

/**
* 
* Start Companies by category
*/
public function exporttodoc() {

$val = 'Companies Report by Category';

$compnaytype = $this->common->GetCurrentUserInfo('cc');

$objQuery = $this->common->TableGetAllOrderByWhere('companies', array('companytype' => $compnaytype, 'comp_is_active' => 1), 'companyname', 'ASC');

header("Content-type: application/vnd.ms-word");

header("Content-Disposition: attachment;Filename=" . $val . ".doc");

$tbl_header.= "<html>";

//        $tbl_header.= '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\"> '
//                . '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
$tbl_header.= '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">'
. '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">';
//        $tbl_header.= "<meta charset='utf-8' />";

$tbl_header.= "<body>";

foreach ($objQuery as $resultData):

$ccid = $resultData["cid"];

$tbl_header.= '<div style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $val . '</div>';

$tbl_header.= '<table width="100%" border="0">

<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["companyname"] . '</td>

</tr>
<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["companytype"] . '</td>

</tr>

</table>

<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;">

<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Contact(s)</td>

</tr>

</table>';

$tbl_header.= '<table width="100%" border="0">';


$where = "companycontacts.cid = '" . $ccid . "' AND companycontacts.is_active=1 ORDER BY display_order";
$dataQuery = $this->outputData['ccontact'] = $this->common->JoinTable('*', 'companycontacts', 'companies', 'companycontacts.cid=companies.cid', 'LEFT', $where);

$count = 0;

foreach ($dataQuery as $resultData3):

if ($count % 2 == 0)
$tbl_header.='<tr width="40%" >';

$tbl_header.= '

<td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">' . $resultData3["cfname"] . ' ' . $resultData3["clname"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Contact</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cfname"] . ' ' . $resultData3["clname"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Title</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["ctitle"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Address</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["caddress1"] . '</td>


</tr>';



if ($resultData3["caddress2"]) {


$tbl_header.= '<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["caddress2"] . '</td>


</tr>';
}



$tbl_header.= '<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["ccity"] . ', ' . $resultData3["cstate"] . ' ' . $resultData3["czip"] . ' ' . $resultData3["ccountry"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Phone</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cphone"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Fax</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cfax"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Email</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . mailto($resultData3["cemail"]) . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Website</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"><a href="http://' . $resultData3["cwebsite"] . '" >' . $resultData3["cwebsite"] . '</a></td>



</tr>


</table></td>';

if ($count % 2 == 0) {
$tbl_header.= '</tr>';
}
$count++;

endforeach;

if (count($dataQuery) == 0) {
$tbl_header.= '<tr><td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">No Contact Found</td>


</tr></table></td></tr>';
}

$tbl_header.= '</table>';

/* End Result Data 3__________________________________________________  */

$tbl_header.= '<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Company Info</td>

</tr>

</table>';


$rows = '';

$nword = str_word_count($resultData["companyinfo"]);

$rows = $nword / 10;

$companyinfo = $this->common->get_formatted_string_company($this->common->get_formatted_string_company1($this->common->htmlallentities($resultData["companyinfo"])));




$tbl_header.= '

<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:justify;border-collapse: collapse;">

<tr>

<td style="vertical-align:top;padding:0;border-bottom:1px solid #000;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">


<p  style="width: 100% ! important; background-color: #FFFFFFFF ! important; vertical-align:top ! important;padding:0 ! important;font-size:8pt ! important;font-family:Vectora LH 55 Roman,arial ! important;" rows="' . $rows . '" >

' . $this->common->normalize($companyinfo) . '</p>';
//                ' . $this->common->get_formatted_string_company($this->common->get_formatted_string_company1($this->common->htmlallentities($resultData["companyinfo"]))) . '</p>';
//                ' . $this->common->get_formatted_string_company($resultData["companyinfo"]) . '</p>';

$tbl_header.= '</td></tr>

</table>';

$tbl_header.= '


</table>      
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">


<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">


<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Company Location</td>


</tr>


</table>';


$tbl_header.= ' <table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:left; border-collapse: collapse;">



<tr style="font-family:arial;font-size:9pt;font-weight:bold;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:9pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">No.</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:9pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Location</td>



</tr>';


$cloc = $this->common->CustomQueryALL("SELECT st1.*, st2.* FROM (SELECT `aid`, `acity`, `astate`,`aname` FROM `airports` 
    UNION SELECT `aid`, `acity`, `astate`,`aname` FROM `lkpairportlist`) st1 
    LEFT JOIN `companylocations` st2 ON `st2`.`aid` = `st1`.`aid`
    LEFT JOIN `companies` ON `companies`.`cid` = `st2`.`cid` 
    WHERE `companies`.`cid` = " . $this->db->escape($ccid) . " ORDER BY `st1`.`acity`, `st1`.`astate`");


$c = 1;

foreach ($cloc as $loc):

if ($loc['acity'] != "") {


$tbl_header.= '<tr>



<td style="width:900px; padding:3px 20px 3px 3px;font-size:9pt;font-family:Vectora LH 55 Roman,arial;">' . $loc['acity'] . ",&nbsp;" . $loc['astate'] . "&nbsp;(" . $loc['aname'] . ")" . ' </td> </tr>';
}

$c++;
endforeach;

$tbl_header.= '</table>';

$tbl_header.= '</table></td></tr></table><br><br>';

endforeach;


$ftext = "AXN's " . date('Y') . " Fact Book";

$tbl_header.= '<p style="text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-top:1px solid #000;">' . $ftext . '</p>';

$tbl_header.= "</body>";

$tbl_header.= "</html>";

echo $tbl_header;
}

/**
* 
* End Companies by category
*/
public function index() {
//$this->session->unset_userdata('list');
echo 'test';
$laid = $this->session->userdata('list');

print_r($laid);
}


/**
* 
* Start Multiple Airports PDF Report (Required ID)
*/
public function GenPdf() {

$val = 'Multi Airports Report';

// $mpdf = new mPDF('utf-8', 'Letter');
//$mpdf = new mPDF('utf-8', 'A4-L');
// $mpdf = new mPDF('utf-8', array(250,260));
// $mpdf = new mPDF('utf-8','A4','','','0','0','20',''); 
$mpdf = new mPDF('utf-8',    // mode - default ''
array(236.5,300.5),    // format - A4, for example, default ''
'',     // font size - default 0
'',    // default font family
'0',    // margin_left
'0',    // margin right
'25',     // margin top
'25',    // margin bottom
'0',     // margin header
'0',     // margin footer
'P');

 $laid = $this->session->userdata('list');



$objQuery = $this->common->GetAllWhereIn("aid,IATA,aname,acity,astate,mgtstructure", 'airports', 'aid', $laid);


//echo $this->db->last_query();
//var_dump($objQuery); exit();
foreach ($objQuery as $resultData):

$aaid = $resultData["aid"];

$tablestyle = 'style="margin-left:76;margin-right:76;"';
$tablestyle2 = 'style="margin-bottom:20px; margin-left:76;margin-right:76;"';
$tablestyle3 = 'style=" margin-left:76;margin-right:76; line-height:14px !important; font-size:12px !important;"';
$mpdf->SetHTMLHeader('
<table cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;">
<tr>
<td width="8%" >
<img src="'. base_url().'assets/images/frame-left.png" width="38" height="38" />

</td>
<td valign="top" width="40%">
<p style="margin:10, 10, 10, 10 !important; padding-left:0; color:#000; font-size:10px; font-family:Arial, Helvetica, sans-serif;"></p></td>
<td align="left" valign="top" width="5%">
<img src="'. base_url().'assets/images/bullet-header.png" />

</td>
<td width="37%">&nbsp;</td>
<td width="46" width="10%" align="right">

<img src="'. base_url().'assets/images/frame-right.png" width="38" height="38" /> 
</td>
</tr>
<tr>
<td colspan="5" align="center" style="padding-top:35px;">
<h1 style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000;  line-height:15px;">'. $resultData["acity"] . ', ' . $resultData["astate"] .'</h1>
</td>
</tr></br></br>

</table>

');


$tbl_header.='
<table width="100%" border="0" '.$tablestyle.'>

<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold; line-height:15pt;">' . $resultData["aname"] . '</td>

</tr>

<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt; line-height:15pt; font-weight:bold;">' . $resultData["IATA"] . '</td>

</tr>

</table>

<table width="100%" align:center; border="0" cellpadding="2" '.$tablestyle.'>

<tr bgcolor="#999999"  style="color:#fff;">

<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Contact(s)</td>

</tr>

</table>';

$tbl_header.= '<table width="100%" border="0" '.$tablestyle.' cellpadding="0" cellspacing="0">';


$join_array = array(
array('lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT')
);

$dataQuery = $this->common->JoinTables("*", "airportcontacts", $join_array, array('aid' => $aaid), "airportcontacts.priority");

//            $count = 0;
$count = 2;

foreach ($dataQuery as $resultData3):

if ($count % 2 == 0) {
$tbl_header.= '<tr>';
}


$tbl_header.= '


<td valign="top">
<table border="0" style="width:100% !important;" width="100%" cellspacing="0" >
<tr>
<td class="category" style="padding:8px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; line-height:10pt;" colspan="2">' . $resultData3["mgtresponsibilityname"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">Contact</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["afname"] . ' ' . $resultData3["alname"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">Title</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["atitle"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">Company</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["acompany"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Address</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["aaddress1"] . '</td>
</tr>';
if ($resultData3["aaddress2"]) {
$tbl_header.= '<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;"></td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["aaddress2"] . '</td>
</tr>';
}
$tbl_header.= '<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;"></td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["accity"] . ', ' . $resultData3["acstate"] . ' ' . $resultData3["aczip"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Phone</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["aphone"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Fax</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["afax"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Email</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["aemail"] . '</td>
</tr>
<tr>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Website</td>
<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt; line-height:10pt;">' . $resultData3["awebsite"] . '</td>
</tr>
</table>
</td>';

if ($count % 2 != 0) {
$tbl_header.= '</tr>';
}
$count++;

endforeach;

if (count($dataQuery) == 0) {
$tbl_header.= '<tr><td><table width="100%" border="0"  >

<tr>


<td  class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">No Contact Found</td>


</tr></table></td></tr>';
}

$tbl_header.= '</table>';

/* End Result Data 3__________________________________________________  */

$tbl_header.= '<table width="100%" border="0" cellpadding="2" '.$tablestyle.'>
<tr>
<td  height="20"></td>
</tr>
<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Airport Info</td>

</tr>

</table>';



 $airterm = "SELECT * FROM `airports` LEFT JOIN airportsannual ON airports.aid=airportsannual.aid 

INNER JOIN terminals ON airports.aid=terminals.aid INNER JOIN terminalsannual ON terminals.tid=terminalsannual.tid 

INNER JOIN airporttotals ON airports.aid=airporttotals.aid 

WHERE airports.aid=" . $this->db->escape($aaid) . " ORDER BY airports.aid, terminals.terminalabbr ASC";

$resultDatao = $this->common->CustomQueryROw($airterm);



$ap_count = $this->common->CustomCountQuery($airterm);

$exp_countall = $resultDatao["texpansionplanned"];

$exp_count = explode("#", $exp_countall);

$addsqft1 = $resultDatao["addsqft"];

$addsqft = str_replace(",", "", $addsqft1);

$addsq_count = explode("#", $addsqft);

$completedexpdate = $resultDatao["completedexpdate"];

$completed_count = explode("#", $completedexpdate);

$add_sq = "Add'l Sq. Ft.";

// $info = array();
$info2 = array();
$info = array("Airport Configuration","Concessions Mgt. Type");

$info2=array ($resultDatao["configuration"],$resultDatao["mgtstructure"]);
$resultData2 = $this->common->CustomQuery($airterm);

//$info2 = array();
//var_dump($addsq_count[0]); exit();
for ($kk = 0; $kk < count($exp_count); $kk++) {

array_push($info,"Expansion Planned" ,"Add'l Sq. Ft.","Complete Date");

array_push($info2,$exp_count[$kk],$addsq_count[$kk],$completed_count[$kk]);
}
// var_dump($info); exit();
$tbl_header.='

<table width="100%" border="0" '.$tablestyle2.' cellspacing="0" cellpadding="0">


<tr>
<td width="50%" style="vertical-align:top;padding:0;" valign="top"> 
<table width="100%" border="0"  cellpadding="0" cellspacing="0">
<tr bgcolor="" >

<td width="50%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Airport Info</td>

<td width="200px" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>


<td width="40%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #00000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminals/Conc.</td>
<td width="25%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Abbr.</td>
<td width="35%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Dominant Airline</td>

</tr>';

count($info2);
count($resultData2);
if(count($info2) > count($resultData2)){
$x = count($info2);
}
else
{
$x = count($resultData2);
}
for ($kk = 0; $kk < $x; $kk++) {
$tbl_header.='
<tr>
<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">'. $info[$kk] .'</td>
<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; line-height:11pt; border-bottom:1px solid #ddd;">' . $info2[$kk] . '</td>
<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">' . $resultData2[$kk]["terminalname"] . '</td>
<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">' . $resultData2[$kk]["terminalabbr"] . '</td>
<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">' . $resultData2[$kk]["tdominantair"] . '</td>
</tr>';
}
$tbl_header.='</table>
</td> 
';

$tbl_header.='</tr></table>';
if ($resultData["IATA"] != 'ORF') {
if ($resultData["IATA"] != 'AVL') {
if ($resultData["IATA"] != 'COS1') {
if ($resultData["IATA"] != 'HRL') {
if ($resultData["IATA"] != 'MEM1') {
if ($resultData["IATA"] != 'YYZ1') {
if ($resultData["IATA"] != 'OKC') {
if ($resultData["IATA"] != 'MSY1') {
$tbl_header.='
<table width="100%" border="0" '.$tablestyle.' cellpadding="0" cellspacing="0">
<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">
    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF; ">Passenger Traffic</td>
</tr>
</table>';
$tbl_header.='  <table width="100%" border="0"  '.$tablestyle2.' cellpadding="0" cellspacing="0">
<tr style="font-family:arial;font-size:9pt;font-weight:bold;">
    <td width="15%" style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total</td>
    <td align="right" width="10%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">+ / - %</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Deplaning</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Enplaning</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">EP Domestic</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:14.3%">EP Int l</td>
</tr>';
$airportlist = $this->common->CustomQuery($airterm);
//echo $this->db->last_query();
 $tot_tpasstraffic=0;
  $tot_tdeplanin=0;
  $tot_tenplaning=0;
 $tot_tepdomestic=0;
 $tot_tepintl=0;
for ($s = 0; $s < $ap_count; $s++) {
$tot_tpasstraffic+=$airportlist[$s]["tpasstraffic"];
$tot_tdeplanin+=$airportlist[$s]["tdeplaning"];
 $tot_tenplaning+=$airportlist[$s]["tenplaning"];
 $tot_tepdomestic+=$airportlist[$s]["tepdomestic"];
 $tot_tepintl+=$airportlist[$s]["tepintl"];

$tbl_header.='<tr >
    <td width="15%" style="padding:3px 20px 3px 3px;text-align:left !important;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $airportlist[$s]["terminalabbr"] . '</td>
    <td align="center" width="15%" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">' . number_format($airportlist[$s]["tpasstraffic"]) . '</td>
    <td align="right" width="10%" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $airportlist[$s]["tpasstrafficcompare"] . '</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($airportlist[$s]["tdeplaning"]) . '</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($airportlist[$s]["tenplaning"]) . '</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($airportlist[$s]["tepdomestic"]) . '</td>
    <td align="right" width="15%" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($airportlist[$s]["tepintl"]) . '</td>
</tr>';
}

/*echo "<br>".$tot_tpasstraffic;
echo "<br>".$tot_tdeplanin;

echo "<br>".$tot_tenplaning;
echo "<br>".$tot_tepdomestic;
echo "<br>".$tot_tepintl;*/


/*$tbl_header.='

<tr width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">
<td  width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>
<td align="center" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">' . number_format($resultDatao["apasstraffic"]) . '</td>
<td align="right" width="10%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["apasstrafficcompare"] . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["adeplaning"]) . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aenplaning"]) . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aepdomestic"]) . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aepintl"]) . '</td>
</tr>

</table>';*/
$tbl_header.='

<tr width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">
<td  width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>
<td align="center" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">' . number_format($tot_tpasstraffic) . '</td>
<td align="right" width="10%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["apasstrafficcompare"] . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($tot_tdeplanin) . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($tot_tenplaning) . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($tot_tepdomestic) . '</td>
<td align="right" width="15%" style="padding:3px 20px 3px 3px;border-top:1px solid #000;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($tot_tepintl) . '</td>
</tr>

</table>';

// entered daata in airport table above fields.mannualy 
/*echo "<br>".$tbl_header;

exit;*/

if ($resultDatao["apasscomment"] != '') {
$tbl_header.='<p  style="padding:-10px 10px 3px 57px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; margin:0 20px;">' . $resultDatao["apasscomment"] . '</p>';
}
if ($resultDatao["avgdwelltime"] != '') {
$avg_time = $resultDatao["avgdwelltime"] . ' minutes';
}
if ($resultData["IATA"] != 'VPS') {
$tbl_header.=' 
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;" '.$tablestyle.'>
<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">
    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF;"> Airport Percentages</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center; border-collapse: collapse;" '.$tablestyle2.'>
<tr style="font-weight:bold;">
    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Pre/Post Security</td>
    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Business to Leisure Ratio</td>
    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">OD Transfer</td>
    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Average Dwell Time</td>
</tr>
<tr>
    <td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $resultDatao["presecurity"] . '/' . $resultDatao["postsecurity"] . '</td>
    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $resultDatao["ratiobusleisure"] . '</td>
    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $resultDatao["ondtransfer"] . '</td>
    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $avg_time . '</td>
</tr>
</table>';
$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="border-collapse: collapse;" '.$tablestyle.'>
<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">
<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF"> Airportwide Info</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center; border-collapse: collapse;" '.$tablestyle.'>
<tr >
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Parking</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" >Short</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Long</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Economy</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Valet</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Car Rentals</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Agencies</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Rev</td>
<td style="text-align:right;padding:3px 3px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="85">Gross Rentals</td>
</tr>';
$tbl_header.='<tr>
<td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">Hourly</td>
<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';
if ($resultDatao["hourlyshort"]) {
    $tbl_header.='$' . number_format($resultDatao["hourlyshort"], 2, '.', '') . '</td>';
} else {
    $tbl_header.='$0.00';
}
$tbl_header.='<td  align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["hourlylong"]) {



    $tbl_header.='$' . number_format($resultDatao["hourlylong"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["hourlyeconomy"]) {



    $tbl_header.='$' . number_format($resultDatao["hourlyeconomy"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["hourlyvalet"]) {



    $tbl_header.='$' . number_format($resultDatao["hourlyvalet"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">Car Rental On Site</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $resultDatao["carrentalagenciesonsite"] . '</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["carrentalrevonsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevonsite"])) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td  style="padding:3px 3px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">';



if ($resultDatao["carrentalrevtoaironsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevtoaironsite"])) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr>




<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">Daily</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["dailyshort"]) {



    $tbl_header.='$' . number_format($resultDatao["dailyshort"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right"  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["dailylong"]) {



    $tbl_header.='$' . number_format($resultDatao["dailylong"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["dailyeconomy"]) {



    $tbl_header.='$' . number_format($resultDatao["dailyeconomy"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["dailyvalet"]) {



    $tbl_header.='$' . number_format($resultDatao["dailyvalet"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">Car Rental Off Site:</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $resultDatao["carrentalagenciesoffsite"] . '</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["carrentalrevoffsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevoffsite"])) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 3px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">';



if ($resultDatao["carrentalrevtoairoffsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevtoairoffsite"])) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr>




<tr>



<td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;"># Spaces</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($resultDatao["spacesshort"]) . '</td>



<td  align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($resultDatao["spaceslong"]) . '</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($resultDatao["spaceseconomy"]) . '</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $resultDatao["spacesvalet"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">Total Cars Rented</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($resultDatao["totalcarsrented"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 3px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">&nbsp;</td>



</tr>




<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">Car Rental Sq. Ft.</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($resultDatao["carrentalsqft"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" colspan="2">&nbsp;</td>



</tr>



</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center; border-collapse: collapse; table-layout:fixed;" '.$tablestyle2.'>



<tr>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="30%">Parking Revenue: ';



if ($resultDatao["parkingrev"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao['parkingrev'])).' ';
} else {



    $tbl_header.='$0.00';
}

$tbl_header.='</td>';

$tbl_header.='<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total Spaces: ';





if ($resultDatao["parkingspaces"]) {



    $tbl_header.=number_format($resultDatao['parkingspaces']) . '';
}else {



    $tbl_header.='0';
}



$tbl_header.='</td>

</tr>



</table>';



$tbl_header.='



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center; border-collapse: collapse;" '.$tablestyle2.'>



<tr style="font-weight:bold;">



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="150px">&nbsp;</td>



<td align="right"style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Revenue</td>



<td align="right"php style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Rev. to Airport</td>
</tr>



<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;border-bottom:1px solid #ddd;">Passenger Services</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["passservicesrev"]) {



    $tbl_header.='$' . number_format($resultDatao["passservicesrev"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["passservicesrevtoair"]) {



    $tbl_header.='$' . number_format($resultDatao["passservicesrevtoair"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}







$tbl_header.='</tr>



<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;border-bottom:1px solid #ddd;">Advertising</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["advertisingrev"]) {



    $tbl_header.='$' . number_format($resultDatao["advertisingrev"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



if ($resultDatao["advertisingrevtoair"]) {



    $tbl_header.='$' . number_format($resultDatao["advertisingrevtoair"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr>



<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Currency Exchange</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["currencyexrev"]) {



    $tbl_header.='$' . number_format($resultDatao["currencyexrev"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["currencyexrevtoair"]) {



    $tbl_header.='$' . number_format($resultDatao["currencyexrevtoair"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr></table>';



if ($resultDatao["awrevcomment"] != '') {



$tbl_header.='<p  
style="padding:3px 40px 3px 40px;
font-size:8pt;
font-family:Vectora LH 55 Roman,arial;
width:90%;
margin:0 auto;
color:red !important;
">' . $resultDatao["awrevcomme/nt"] . '</p>';
}

if ($resultData["IATA"] != 'MSN') {



//                                                    if ($resultData["IATA"] != 'RNO1') {



    if ($resultData["IATA"] != 'RIC') {



//                                                            if ($resultData["IATA"] != 'SHV') {
//                                                                if ($resultData["IATA"] != 'FWA') {



        $tbl_header.='<table width="100%" '.$tablestyle.'>



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;" >



<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF;">Concession Totals - Terminal Breakdowns <span style="font-size:9px;">(Food/Beverage, Specialty



Retail, News/Gifts Only)</span></td>



</tr>



</table>



<table width="100%" border="0" style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0"  '.$tablestyle2.' >



<tr style="font-weight:bold;">



<td style="text-align:left;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">Sales EP</td>



<td align="right" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:25%;">Total Rent To Airport</td>



<td align="right" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Rent EP</td>



<td style="text-align:right;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>







</tr>';



        $airtermQu3 = $this->common->CustomQuery($airterm);

        foreach ($airtermQu3 as $totairportList) {


            $totgr = $totairportList["fbgrosssales"] + $totairportList["srgrosssales"] + $totairportList["nggrosssales"] . "";



            $totsalesep = $totgr / $totairportList["tenplaning"];



            $totrentrev = $totairportList["fbrentrev"] + $totairportList["srrentrev"] + $totairportList["ngrentrev"];



            //$totrentep=$totairportList["fbrentep"]+$totairportList["srrentep"]+$totairportList["ngrentep"];



            $totrentep = $totrentrev / $totairportList["tenplaning"];



            $totcurrsqft = $totairportList["fbcurrsqft"] + $totairportList["srcurrsqft"] + $totairportList["ngcurrsqft"];



            $tbl_header.='



<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $totairportList["terminalabbr"] . '</td>



<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            if ($totgr) {



                $tbl_header.='$' . number_format($totgr) . '</td>';
            } else {



                $tbl_header.='</td>';
            }



            $tbl_header.='<td align="right"  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            if ($totsalesep) {



                $tbl_header.='$' . number_format($totsalesep, 2, '.', '') . '</td>';
            } else {



                $tbl_header.='$0.00</td>';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            if ($totrentrev) {



                $tbl_header.='$' . number_format($totrentrev) . '</td>';
            } else {



                $tbl_header.='</td>';
            }



            $tbl_header.='<td align="right"  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            if ($totrentep) {



                $tbl_header.='$' . number_format($totrentep, 2, '.', '') . '</td>';
            } else {



                $tbl_header.='</td>';
            }



            $tbl_header.='<td align="right"  style="text-align:right;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($totcurrsqft) . '</td>



</tr>';



            $totgrall += $totgr;



            $totsalesepall+= $totgr / $totairportList["aenplaning"];
//                                                                        $totsalesepall+= $totsalesep;



            $totrentrevall += $totrentrev;



            $totrentepall += $totrentrev / $totairportList["aenplaning"];
//                                                                        $totrentepall += $totrentep;



            $totcurrsqftall += $totcurrsqft;
        }
//                                                                    }



        $tbl_header.='<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td align="right" style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totgrall) {



            $tbl_header.='$' . number_format($totgrall) . '</td>';
        } else {



            $tbl_header.='</td>';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totsalesepall) {

//$tbl_header.= 'if';

            $tbl_header.='$' . number_format($totsalesepall, 2, '.', '') . '</td>';
        } else {



            $tbl_header.='$0.00</td>';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totrentrevall) {



            $tbl_header.='$' . number_format($totrentrevall) . '</td>';
        } else {



            $tbl_header.='</td>';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totrentepall) {



            $tbl_header.='$' . number_format($totrentepall, 2, '.', '') . '</td>';
        } else {



            $tbl_header.='</td>';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">' . number_format($totcurrsqftall) . '</td>



</tr>';



        unset($totgrall);



        unset($totsalesepall);



        unset($totrentrevall);



        unset($totrentepall);



        unset($totcurrsqftall);



        $tbl_header.='</table>';



        if ($resultDatao["grosscomment"] != '') {



            $tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; margin:0 74px; ">' . $resultDatao["grosscomment"] . '</p>';
        }



        /* $tbl_header.= '<table width="100%" border="0">



          <tr>



          <td style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH Black;font-size:14pt;">'



          .$resultData["acity"].','.$resultData["astate"].'</td>



          </tr>



          </table>'; */



        $sym = 'Food/Beverage';



        $tbl_header.='<table width="100%" border="0" cellpadding="2" '.$tablestyle.' >



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Concession Program Details - Category Breakdowns</td>



</tr>



</table>
<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" '.$tablestyle.' >
<tr style="font-weight:bold;color:#a20000;">
<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Food<strong>/</strong>Beverage</span></td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:10%;">Terminal</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>
<td align="right" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>
<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right" width="120">Current Sq. Ft</td>
</tr>';




        $airtermQu4 = $this->common->CustomQuery($airterm);


        foreach ($airtermQu4 as $fbairportList) {





            $all_fb = $fbairportList["fbsalesep"];



            $tbl_header.='



<tr>



<td style="border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fbairportList["terminalabbr"] . '</td>';



            if ($fbairportList["fbgrosssales"] && $fbairportList["fbgrosssales"] !== '0') {



                $fb_gr_value = '$' . number_format($fbairportList["fbgrosssales"]);
            } else {



                $fb_gr_value = '';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_gr_value . '</td>';



            $fb_salesep = $fbairportList["fbgrosssales"] / $fbairportList["tenplaning"];



            if ($fb_salesep) {



                $fb_sp_value = '$' . number_format($fb_salesep, 2, '.', '');
            } else {



                $fb_sp_value = '$0.00';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_sp_value . '</td>';



            if ($fbairportList["fbrentrev"]) {







                $fb_rv_value = '$' . number_format($fbairportList["fbrentrev"]);
            } else {



                $fb_rv_value = '';
            }




            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_rv_value . '</td>';



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">' . number_format($fbairportList["fbcurrsqft"]) . '</td></tr>';

      
            $allfbsales+=$fbairportList["fbgrosssales"];

         // echo "<br>".$fbairportList["fbgrosssales"].'/'.$fbairportList["aenplaning"];
             

            $allfbsalesep+=$fbairportList["fbgrosssales"] / $fbairportList["aenplaning"];



            $allfbrentrev+=$fbairportList["fbrentrev"];



            //$allfbrentep+=$fbairportList["fbrentep"];



            $allfbcurrsqft+=$fbairportList["fbcurrsqft"];
        }





        $tbl_header.='<tr><td style="text-align:left;font-size:8pt;border-top:2px solid #000;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>';



        if ($allfbsales) {



            $fb_gr_totvalue = '$' . number_format($allfbsales);
        } else {



            $fb_gr_totvalue = '';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;border-top:2px solid #000;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_gr_totvalue . '</td>';



        if ($allfbsalesep) {



            $fb_sp_totvalue = '$' . number_format($allfbsalesep, 2, '.', '');
        } else {



            $fb_sp_totvalue.='$0.00';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;border-top:2px solid #000;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_sp_totvalue . '</td>';

/*echo"<br>".$tbl_header;
exit;*/
        if ($allfbrentrev) {



            $fb_rv_totvalue = '$' . number_format($allfbrentrev);
        } else {



            $fb_rv_totvalue = '';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;border-top:2px solid #000;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_rv_totvalue . '</td>';




        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;border-top:2px solid #000;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">' . number_format($allfbcurrsqft) . '</td></tr></table>



<div style="height:10px"></div>



';



        unset($allfbsales);



        unset($allfbsalesep);



        unset($allfbrentrev);



        //unset($allfbrentep);



        unset($allfbcurrsqft);



        $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" '.$tablestyle.'>



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;">Specialty Retail</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td align="right" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 0px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right" width="120">Current Sq. Ft</td>







</tr>';




        $airtermQu5 = $this->common->CustomQuery($airterm);



        foreach ($airtermQu5 as $srairportList) {




            $tbl_header.='



<tr>



<td style="border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $srairportList["terminalabbr"] . '</td>';




            if ($srairportList["srgrosssales"]) {



                $sr_gr_value = '$' . number_format($srairportList["srgrosssales"]);
            } else {



                $sr_gr_value = '';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $sr_gr_value . '</td>';



            $sr_salesep = $srairportList["srgrosssales"] / $srairportList["tenplaning"];



            if ($sr_salesep) {



                $sr_sp_value = '$' . number_format($sr_salesep, 2, '.', '');
            } else {



                $sr_sp_value = '$0.00';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $sr_sp_value . '</td>';



            if ($srairportList["srrentrev"]) {



                $sr_rv_value = '$' . number_format($srairportList["srrentrev"]);
            } else {



                $sr_rv_value = '';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $sr_rv_value . '</td>';



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">' . number_format($srairportList["srcurrsqft"]) . '</td>







</tr>';



            $allsrsales+=$srairportList["srgrosssales"];



            $allsrsalesep+=$srairportList["srgrosssales"] / $srairportList["aenplaning"];



            $allsrrentrev+=$srairportList["srrentrev"];



            //$allsrrentep+=$srairportList["srrentep"];



            $allsrcurrsqft+=$srairportList["srcurrsqft"];
        }







        $tbl_header.='<tr>



<td style="text-align:left;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>';



        if ($allsrsales) {



            $sr_gr_totvalue = '$' . number_format($allsrsales);
        } else {



            $sr_gr_totvalue = '';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_gr_totvalue . '</td>';



        if ($allsrsalesep) {



            $sr_sp_totvalue = '$' . number_format($allsrsalesep, 2, '.', '');
        } else {



            $sr_sp_totvalue = '$0.00';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_sp_totvalue . '</td>';



        if ($allsrrentrev) {



            $sr_rv_totvalue.='$' . number_format($allsrrentrev);
        } else {



            $sr_rv_totvalue = '';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_rv_totvalue . '</td>';



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">' . number_format($allsrcurrsqft) . '</td></tr>



</table>



<div style="height:10px"></div>



';



        unset($allsrsales);



        unset($allsrsalesep);



        unset($allsrrentrev);



        //unset($allsrrentep);



        unset($allsrcurrsqft);



        $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" '.$tablestyle.'>



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>News<strong>/</strong>Gifts Retail</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td align="right" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right" width="120">Current Sq. Ft</td>







</tr>';




        $airtermQu6 = $this->common->CustomQuery($airterm);



        foreach ($airtermQu6 as $ngairportList) {




            $tbl_header.='



<tr>



<td style="border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $ngairportList["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">';



            if ($ngairportList["nggrosssales"]) {



                $tbl_header.='$' . number_format($ngairportList["nggrosssales"]) . '</td>';
            } else {



                $tbl_header.='';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            $ng_salesep = $ngairportList["nggrosssales"] / $ngairportList["tenplaning"];



            if ($ng_salesep) {



                $tbl_header.='$' . number_format($ng_salesep, 2, '.', '') . '</td>';
            } else {



                $tbl_header.='$0.00';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            if ($ngairportList["ngrentrev"]) {



                $tbl_header.='$' . number_format($ngairportList["ngrentrev"]) . '</td>';
            } else {



                $tbl_header.='';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">' . number_format($ngairportList["ngcurrsqft"]) . '</td>  



</tr>';


            $allngsales+=$ngairportList["nggrosssales"];



            $allngsalesep+=$ngairportList["nggrosssales"] / $ngairportList["aenplaning"];



            $allngrentrev+=$ngairportList["ngrentrev"];



            //$allngrentep+=$ngairportList["ngrentep"];



            $allngcurrsqft+=$ngairportList["ngcurrsqft"];
        }



        $tbl_header.='<tr><td style="text-align:left;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">';



        if ($allngsales) {



            $tbl_header.='$' . number_format($allngsales) . '</td>';
        } else {



            $tbl_header.='';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($allngsalesep) {



            $tbl_header.='$' . number_format($allngsalesep, 2, '.', '') . '</td>';
        } else {



            $tbl_header.='$0.00';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($allngrentrev) {



            $tbl_header.='$' . number_format($allngrentrev) . '</td>';
        } else {



            $tbl_header.='';
        }







        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">' . number_format($allngcurrsqft) . '</td></tr></table>



';



        unset($allngsales);



        unset($allngsalesep);



        unset($allngrentrev);



        unset($allngrentep);



        unset($allngcurrsqft);



        $tbl_header.='

<div style="height:10px"></div>

<table width="100%" border="0" style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" '.$tablestyle2.'>


<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Duty Free Retail</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td align="right" style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right"  width="120">Current Sq. Ft</td>







</tr>';




        $airtermQu7 = $this->common->CustomQuery($airterm);


        foreach ($airtermQu7 as $dfairportList) {



            $tbl_header.='



<tr>



<td style="border-bottom:1px solid #ddd;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $dfairportList["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            if ($dfairportList["dfgrosssales"]) {



                $tbl_header.='$' . number_format($dfairportList["dfgrosssales"]) . '</td>';
            } else {



                $tbl_header.='';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            $df_salesep = $dfairportList["dfgrosssales"] / $dfairportList["tepintl"];



            if ($df_salesep) {



                $tbl_header.='$' . number_format($df_salesep, 2, '.', '') . '</td>';
            } else {



                $tbl_header.='$0.00';
            }



            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



            if ($dfairportList["dfrentrev"]) {



                $tbl_header.='$' . number_format($dfairportList["dfrentrev"]) . '</td>';
            } else {



                $tbl_header.='';
            }







            $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;" align="right">' . number_format($dfairportList["dfcurrsqft"]) . '</td>  



</tr>';



            $alldfsales+=$dfairportList["dfgrosssales"];



            $alldfsalesep+=$dfairportList["dfgrosssales"] / $dfairportList["aepintl"];



            $alldfrentrev+=$dfairportList["dfrentrev"];



            //$allngrentep+=$ngairportList["ngrentep"];



            $alldfcurrsqft+=$dfairportList["dfcurrsqft"];
        }





        $tbl_header.='<tr><td style="text-align:left;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($alldfsales) {



            $tbl_header.='$' . number_format($alldfsales) . '</td>';
        } else {



            $tbl_header.='';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($alldfsalesep) {



            $tbl_header.='$' . number_format($alldfsalesep, 2, '.', '') . '</td>';
        } else {



            $tbl_header.='$0.00';
        }



        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($alldfrentrev) {



            $tbl_header.='$' . number_format($alldfrentrev) . '</td>';
        } else {



            $tbl_header.='';
        }




        $tbl_header.='<td align="right" style="padding:3px 20px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" align="right">' . number_format($alldfcurrsqft) . '</td></tr></table>



';


        unset($alldfsales);



        unset($alldfsalesep);



        unset($alldfrentrev);



        //unset($alldfrentep);



        unset($alldfcurrsqft);
    }
//                                                            }
//                                                        }
//                                                        if ($resultData["IATA"] != 'SHV') {
//                                                            if ($resultData["IATA"] != 'FWA') {



    $tbl_header.= '<table width="100%" border="0" cellpadding="2" '.$tablestyle.'>



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF" >Concession Tenant Details</td>



</tr>



</table>';


    $tbl_header.='<table width="100%" border="0" style="text-align:center; border-collapse: collapse;" cellspacing="0" cellpadding="0"  '.$tablestyle2.'>';



    $listsql = "select distinct category from lkpcategory";



    $list = $this->common->Selectdistinct('lkpcategory', 'category');


    $l = 0;


    foreach ($list as $l => $cmpnyterms) {


        $catid = $cmpnyterms['category'];


        $products = $this->common->GetAllRowjoinOrderBy('outlets', 'lkpcategory', 'outlets.categoryid = lkpcategory.categoryid', 'left', 'outletname', 'ASC', array('lkpcategory.category' => $catid, 'outlets.aid' => $aaid));

// echo "<br>".$this->db->last_query();

        $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $catid . '(Company)</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Product Description</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal </td>



<td style="white-space: nowrap;padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"># locations</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Sq. Ft.</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="70" align="right">Expires</td>




</tr> 

'
;



        if (!empty($products))
            foreach ($products as $prod) {

                if ($prod['exp_reason'] != '') {

                    // $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'TBD', $prod['exp_reason']);

                 //$reasons = $prod['exp_reason'];
                  $reasons = str_replace(array('Permanently Closed', 'Temporarily Closed'), 'Closed', $prod['exp_reason']);
                    if ($prod['exp_date'] != '0000-00-00 00:00:00' && $prod['exp_date'] != '' & $prod['exp_reason'] != 'M-T-M') {
                        $date_val = $reasons . '<br>' . date("M d,Y", strtotime($prod['exp_date']));
                    } else if (($prod['exp_date'] == '0000-00-00 00:00:00' || $prod['exp_date'] == '') && $prod['exp_reason'] != 'M-T-M') {
                        $date_val = $reasons;
                    } else {
                        $date_val = $reasons;
                    }
                } else {

                    if ($prod['exp'] != "" && (!preg_match("/^[a-z\s\-\/]+$/i", $prod['exp']))) {
                        $date_val = $this->common->get_formatted_datetime($prod['exp']);
                    } else {
                        $date_val = $prod['exp'];
                    }
                }



                /*
                  if ($prod['exp'] != "" && (!preg_match("/^[a-z\s\-\/]+$/i", $prod['exp']))) {

                  $date_val = $this->common->get_formatted_datetime($prod['exp']);
                  } else {

                  $expiredate = $prod['exp'];
                  } */
                /*   if ($prod['exp'] !== '') {



                  if (!check_date($prod['exp'])) {



                  $date_val = $prod['exp'];
                  } elseif (strtotime($prod['exp']) == '') {



                  $date_val = "00/00/0000";
                  } elseif (is_null($prod['exp'])) {



                  $date_val = "00/00/0000";
                  } elseif ($prod['exp'] == '0000-00-00') {



                  $date_val = "00/00/0000";
                  } else {



                  $date_val = date('m/d/Y', strtotime($prod['exp']));
                  }
                  } */


                if (empty($prod['companyname'])) {



                    $compny = '';
                } else {



                    $compny = ' (' . $prod['companyname'] . ')';
                }



                $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . stripslashes($prod['outletname']) . $compny . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $prod['productdescription'] . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;text-align:center;">' . $prod['termlocation'] . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;text-align:center;">' . $prod['numlocations'] . '</td>



<td align="right" style="padding:3px 5px 3px 3px;text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">';



                if ($prod['sqft']) {



                    $tbl_header.=number_format($prod['sqft']) . '</td>';
                } else {



                    $tbl_header.='';
                }



                $tbl_header.='<td style="text-align:right;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $date_val . '</td>











</tr>';

                $tottermlocation += $prod['numlocations'];



                $totsqft += $prod['sqft'];
            }



        $tbl_header.='
        <tr>



<td style="width:45%;padding:3px 10px 3px 3px;text-align:left;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $catid . ' Totals</td>



<td style="text-align:center;padding:3px 5px 3px 3px;border-top:2px solid #000;">&nbsp;</td>



<td style="text-align:center;padding:3px 5px 3px 3px;border-top:2px solid #000;">&nbsp;</td>



<td style="text-align:center;padding:3px 5px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tottermlocation . '</td>



<td align="right" style="text-align:center;padding:3px 5px 3px 3px;border-top:2px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totsqft) {



            $tbl_header.=number_format($totsqft) . '</td>';
        } else {



            $tbl_header.='';
        }



        $tbl_header.='<td style="text-align:center;padding:3px 5px 3px 3px;border-top:2px solid #000;">&nbsp;</td>



</tr>
<tr>
<td height="20"></td>
</tr>
';




        unset($tottermlocation);



        unset($totsqft);

//                                                                    $l++;
    }





    $tbl_header.='<tr><td colspan="6">&nbsp;<br><br></td></tr><tr></tr></table>';
}
//                                                        }
//                                                    }
//                                                }
}
}
}
}
}
}
}
}
}



//            $tbl_header.= '</table></td></tr></table>';



endforeach;



$this->session->unset_userdata('list');

$ftext = "AXN's " . date('Y') . " Fact Book";

$mpdf->SetHTMLFooter('
<table cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;">
<tr>
<td colspan="3" align="center" style="font-size:10px; padding-top:10px; padding-bottom:35px;">' . $ftext . '<br/></td>
</tr>
<tr>
<td width="33.333%" >
<img src="'. base_url().'assets/images/frame-b-left.png" width="38" height="38" />
</td>
<td align="center" valign="middle" width="33.333%">
<img src="'. base_url().'assets/images/bullet-header.png" style="padding-left:5px;" />
</td>
<td width="46" width="33.333%" align="right">
<img src="'. base_url().'assets/images/frame-b-right.png" width="38" height="38" /> 
</td>
</tr>
</table>
');


// var_dump($tbl_header); exit();
$mpdf->WriteHTML($tbl_header);

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Multiple Airports PDF Report
*/

/**
* 
* Start Multiple Airports DOC Report (Required ID)
*/
public function GenDoc() {


$val = 'Multi Airports Report';

$laid = $this->session->userdata('list');

$objQuery = $this->common->GetAllWhereIn("aid,IATA,aname,acity,astate,mgtstructure", 'airports', 'aid', $laid);

header('Content-Type: application/octet-stream');

header("Content-Disposition: attachment;Filename=" . $val . ".doc");

$tbl_header.= "<html>";

$tbl_header.= '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">'
. '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">';

$tbl_header.= "<body>";

foreach ($objQuery as $resultData):

$aaid = $resultData["aid"];

$tbl_header.='<div style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["acity"] . ', ' . $resultData["astate"] . '</div>';

$tbl_header.='<table width="100%" border="0" '.$tablestyle.'>

<tr>

<td style="color:#000;display:block;width:100%;text-align:left;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:9pt;font-weight:bold;">' . $resultData["aname"] . '</td>

</tr>

<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["IATA"] . '</td>

</tr>

</table>

<table width="100%" border="0" cellpadding="2" '.$tablestyle.'>

<tr bgcolor="#999999"  style="color:#fff;">

<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Contact(s)</td>

</tr>

</table>';

$tbl_header.= '<table width="100%" border="0" '.$tablestyle.'>';


$join_array = array(
array('lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT')
);

$dataQuery = $this->common->JoinTables("*", "airportcontacts", $join_array, array('aid' => $aaid), "airportcontacts.priority");

$count = 0;

foreach ($dataQuery as $resultData3):

if ($count % 2 == 0)
$tbl_header.= '<tr width="40%" >';

$tbl_header.= '

<td><table width="40%" border="0" '.$tablestyle.'>

<tr>



<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">' . $resultData3["mgtresponsibilityname"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Contact</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["afname"] . ' ' . $resultData3["alname"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Title</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["atitle"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Company</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["acompany"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Address</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["aaddress1"] . '</td>



</tr>';




if ($resultData3["aaddress2"]) {


$tbl_header.= '<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["aaddress2"] . '</td>



</tr>';
}


$tbl_header.= '<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["accity"] . ', ' . $resultData3["acstate"] . ' ' . $resultData3["aczip"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Phone</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["aphone"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Fax</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["afax"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Email</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . mailto($resultData3["aemail"]) . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Website</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"><a href="http://' . $resultData3["awebsite"] . '">' . $resultData3["awebsite"] . '</a></td>



</tr>

</table></td>';

if ($count % 2 != 0) {
$tbl_header.= '</tr>';
}
$count++;

endforeach;

if (count($dataQuery) == 0) {
$tbl_header.= '<tr><td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">No Contact Found</td>


</tr></table></td></tr>';
}

$tbl_header.= '</table>';

/* End Result Data 3__________________________________________________  */

$tbl_header.= '<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;background-color:red;">Airport Info</td>

</tr>

</table>';


$airterm = "SELECT * FROM `airports` LEFT JOIN airportsannual ON airports.aid=airportsannual.aid 

INNER JOIN terminals ON airports.aid=terminals.aid INNER JOIN terminalsannual ON terminals.tid=terminalsannual.tid 

INNER JOIN airporttotals ON airports.aid=airporttotals.aid 

WHERE airports.aid=" . $this->db->escape($aaid) . " ORDER BY airports.aid, terminals.terminalabbr ASC";


$resultDatao = $this->common->CustomQueryROw($airterm);

$ap_count = $this->common->CustomCountQuery($airterm);

$exp_countall = $resultDatao["texpansionplanned"];

$exp_count = explode("#", $exp_countall);

$addsqft1 = $resultDatao["addsqft"];

$addsqft = str_replace(",", "", $addsqft1);

$addsq_count = explode("#", $addsqft);

$completedexpdate = $resultDatao["completedexpdate"];

$completed_count = explode("#", $completedexpdate);

$add_sq = "Add'l Sq. Ft.";

$tbl_header.='


<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:left;border-collapse: collapse;">

<tr>

<td width="50%" style="vertical-align:top;padding:0;" valign="top"> 

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0 0 0 5px;">

<tr >

<td width="50%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Airport Info </td>

<td width="200px" style="padding:0;padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

 </tr>

<tr>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Airport Configuration</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["configuration"] . '</td>

</tr>

<tr>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Concessions Mgt. Type</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["mgtstructure"] . '</td>




</tr>';


for ($kk = 0; $kk < count($exp_count); $kk++) {

$tbl_header.='
<tr >



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Expansion Planned</td>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $exp_count[$kk] . '</td>



</tr>



<tr>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $add_sq . '</td>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format((float) $addsq_count[$kk]) . '</td>



</tr>



<tr>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Complete Date</td>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $completed_count[$kk] . '</td>



</tr>';
}


$tbl_header.='</table>


</td> 

</td>


<td width="50%" valign="top" style="vertical-align:top;padding:0;">



<table width="100%" border="0" cellpadding="0" cellspacing="0" >



<tr>



<td width="35%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminals/Conc.</td>



<td width="30%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Abbr.</td>



<td width="35%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Dominant Airline</td>



</tr>';

$resultData2 = $this->common->CustomQuery($airterm);



for ($r = 0; $r < $ap_count; $r++) {


$tbl_header.= '<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultData2[$r]["terminalname"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultData2[$r]["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultData2[$r]["tdominantair"] . '</td>



</tr> ';
}

$tbl_header.='</table> </td></tr></table>';

if ($resultData["IATA"] != 'ORF') {


if ($resultData["IATA"] != 'AVL') {


if ($resultData["IATA"] != 'COS1') {


if ($resultData["IATA"] != 'HRL') {


if ($resultData["IATA"] != 'MEM1') {


if ($resultData["IATA"] != 'YYZ1') {


if ($resultData["IATA"] != 'OKC') {


if ($resultData["IATA"] != 'MSY1') {


$tbl_header.='


<table width="100%" border="0" cellspacing="0" cellpadding="0">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Passenger Traffic</td>



</tr>



</table>';


$tbl_header.='  <table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:right; border-collapse: collapse;">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">+ / - %</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Deplaning</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Enplaning</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">EP Domestic</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:14.3%">EP Int l</td>



</tr>';


$airportlist = $this->common->CustomQuery($airterm);



for ($s = 0; $s < $ap_count; $s++) {




$tbl_header.='<tr>



<td style="padding:3px 20px 3px 3px;text-align:left ! important; font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist[$s]["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tpasstraffic"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist[$s]["tpasstrafficcompare"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tdeplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tenplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tepdomestic"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tepintl"]) . '</td>



</tr>';
}



$tbl_header.='<tr><td style="text-align:letf;padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["apasstraffic"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["apasstrafficcompare"] . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["adeplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aenplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aepdomestic"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aepintl"]) . '</td></tr></table>';



if ($resultDatao["apasscomment"] != '') {



$tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["apasscomment"] . '</p>';
}



if ($resultDatao["avgdwelltime"] != '') {



$avg_time = $resultDatao["avgdwelltime"] . ' minutes';
}



if ($resultData["IATA"] != 'VPS') {



$tbl_header.=' 





<table width="100%" border="0" cellspacing="0" cellpadding="0"  style ="border-collapse: collapse;">




<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold; "> Airport Percentages</td>


</tr>


</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:left; border-collapse: collapse;">



<tr style="font-weight:bold;">



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Pre/Post Security</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Business to Leisure Ratio</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">OD Transfer</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Average Dwell Time</td>



</tr>



<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["presecurity"] . '/' . $resultDatao["postsecurity"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["ratiobusleisure"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["ondtransfer"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $avg_time . '</td>



</tr></table>';



$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style ="border-collapse: collapse;">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;"> Airportwide Info</td>



</tr>



</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:right; border-collapse: collapse;">



<tr >



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Parking</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" >Short</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Long</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Economy</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Valet</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Car Rentals</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Agencies</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Rev</td>



<td style="text-align:right;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Rentals</td>



</tr>';



$tbl_header.='<tr>



<td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Hourly</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["hourlyshort"]) {



    $tbl_header.='$' . number_format($resultDatao["hourlyshort"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["hourlylong"]) {



    $tbl_header.='$' . number_format($resultDatao["hourlylong"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["hourlyeconomy"]) {



    $tbl_header.='$' . number_format($resultDatao["hourlyeconomy"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["hourlyvalet"]) {



    $tbl_header.='$' . number_format($resultDatao["hourlyvalet"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental On Site</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["carrentalagenciesonsite"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["carrentalrevonsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevonsite"]), 2) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["carrentalrevtoaironsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevtoaironsite"]), 2) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr>




<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Daily</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["dailyshort"]) {



    $tbl_header.='$' . number_format($resultDatao["dailyshort"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["dailylong"]) {



    $tbl_header.='$' . number_format($resultDatao["dailylong"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["dailyeconomy"]) {



    $tbl_header.='$' . number_format($resultDatao["dailyeconomy"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["dailyvalet"]) {



    $tbl_header.='$' . number_format($resultDatao["dailyvalet"], 2, '.', '') . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental Off Site:</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["carrentalagenciesoffsite"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["carrentalrevoffsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevoffsite"]), 2) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["carrentalrevtoairoffsite"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevtoairoffsite"]), 2) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr>



<tr>



<td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;"># Spaces</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["spacesshort"]) . '</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["spaceslong"]) . '</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["spaceseconomy"]) . '</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["spacesvalet"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Total Cars Rented</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["totalcarsrented"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>



</tr>




<tr>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental Sq. Ft.</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["carrentalsqft"]) . '</td>



<td>&nbsp;</td>



</tr>



</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:left; border-collapse: collapse;">



<tr>



<td style="width:15%;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Parking Revenue</td>



<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["parkingrev"]) {



    $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao['parkingrev']), 2) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Total Spaces</td>



<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["parkingspaces"]) {



    $tbl_header.=number_format($resultDatao['parkingspaces']) . '</td>';
}



$tbl_header.='<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td><td>&nbsp;</td>



</tr>



</table>';



$tbl_header.='



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:right; border-collapse: collapse;">



<tr style="font-weight:bold;">



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;width:160px" width="150px">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;font-weight:bold;border-bottom:1px solid #000;">Revenue</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;font-weight:bold;border-bottom:1px solid #000;">Rev. to Airport</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



</tr>


<tr>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Passenger Services</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["passservicesrev"]) {



    $tbl_header.='$' . number_format($resultDatao["passservicesrev"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["passservicesrevtoair"]) {


    $tbl_header.='$' . number_format($resultDatao["passservicesrevtoair"]) . '</td>';
} else {


    $tbl_header.='$0.00';
}


$tbl_header.='</tr>



<tr>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Advertising</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["advertisingrev"]) {



    $tbl_header.='$' . number_format($resultDatao["advertisingrev"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["advertisingrevtoair"]) {



    $tbl_header.='$' . number_format($resultDatao["advertisingrevtoair"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr>



<tr>

<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Currency Exchange</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["currencyexrev"]) {



    $tbl_header.='$' . number_format($resultDatao["currencyexrev"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



if ($resultDatao["currencyexrevtoair"]) {



    $tbl_header.='$' . number_format($resultDatao["currencyexrevtoair"]) . '</td>';
} else {



    $tbl_header.='$0.00';
}



$tbl_header.='</tr></table>';



if ($resultDatao["awrevcomment"] != '') {



    $tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["awrevcomment"] . '</p>';
}

if ($resultData["IATA"] != 'MSN') {



//                                                    if ($resultData["IATA"] != 'RNO1') {



    if ($resultData["IATA"] != 'RIC') {



//                                                            if ($resultData["IATA"] != 'SHV') {
//                                                                if ($resultData["IATA"] != 'FWA') {



        $tbl_header.='<table width="100%">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;" >



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Concession Totals - Terminal Breakdowns <span style="font-size:9px;">(Food/Beverage, Specialty



Retail, News/Gifts Only)</span></td>



</tr>



</table>


<table width="100%" border="0" style="text-align:right;  border-collapse: collapse; margin-bottom:100px;" border="0" cellspacing="0" cellpadding="0"   >



<tr style="font-weight:bold;">



<td style="text-align:left;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Sales EP</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Rent EP</td>



<td style="text-align:right;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>


</tr>';


        $airtermQu3 = $this->common->CustomQuery($airterm);


        foreach ($airtermQu3 as $totairportList) {



            $totgr = $totairportList["fbgrosssales"] + $totairportList["srgrosssales"] + $totairportList["nggrosssales"] . "";



            $totsalesep = $totgr / $totairportList["tenplaning"];



            $totrentrev = $totairportList["fbrentrev"] + $totairportList["srrentrev"] + $totairportList["ngrentrev"];



            $totrentep = $totrentrev / $totairportList["tenplaning"];



            $totcurrsqft = $totairportList["fbcurrsqft"] + $totairportList["srcurrsqft"] + $totairportList["ngcurrsqft"];



            $tbl_header.='<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">' . $totairportList["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



            if ($totgr) {



                $tbl_header.='$' . number_format($totgr) . '</td>';
            } else {



                $tbl_header.='';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



            if ($totsalesep) {



                $tbl_header.='$' . number_format($totsalesep, 2, '.', '') . '</td>';
            } else {



                $tbl_header.='$0.00';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



            if ($totrentrev) {



                $tbl_header.='$' . number_format($totrentrev) . '</td>';
            } else {



                $tbl_header.='';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



            if ($totrentep) {



                $tbl_header.='$' . number_format($totrentep, 2, '.', '') . '</td>';
            } else {



                $tbl_header.='';
            }



            $tbl_header.='<td style="text-align:right;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">' . number_format($totcurrsqft) . '</td>



                        </tr>';



            $totgrall += $totgr;



            $totsalesepall+= $totgr / $totairportList["aenplaning"];



            $totrentrevall += $totrentrev;



            $totrentepall += $totrentrev / $totairportList["aenplaning"];



            $totcurrsqftall += $totcurrsqft;
        }


        $tbl_header.='<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totgrall) {



            $tbl_header.='$' . number_format($totgrall) . '</td>';
        } else {



            $tbl_header.='';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totsalesepall) {



            $tbl_header.='$' . number_format($totsalesepall, 2, '.', '') . '</td>';
        } else {



            $tbl_header.='$0.00';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totrentrevall) {



            $tbl_header.='$' . number_format($totrentrevall) . '</td>';
        } else {



            $tbl_header.='';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totrentepall) {



            $tbl_header.='$' . number_format($totrentepall, 2, '.', '') . '</td>';
        } else {



            $tbl_header.='';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($totcurrsqftall) . '</td>



                            </tr>';


        unset($totgrall);



        unset($totsalesepall);



        unset($totrentrevall);



        unset($totrentepall);



        unset($totcurrsqftall);



        $tbl_header.='</table>';



        if ($resultDatao["grosscomment"] != '') {



            $tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["grosscomment"] . '</p>';
        }



        $sym = 'Food/Beverage';



        $tbl_header.='<table width="100%" border="0" cellpadding="2">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">


<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Concession Program Details - Category Breakdowns</td>


</tr>


</table>



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse; margin-bottom:10px;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Food<strong>/</strong>Beverage</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:10%">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>



</tr>';



        $airtermQu4 = $this->common->CustomQuery($airterm);


        foreach ($airtermQu4 as $fbairportList) {


            $all_fb = $fbairportList["fbsalesep"];



            $tbl_header.='<tr>


<td style="border-bottom:1px solid #ddd;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fbairportList["terminalabbr"] . '</td>';



            if ($fbairportList["fbgrosssales"] && $fbairportList["fbgrosssales"] !== '0') {



                $fb_gr_value = '$' . number_format($fbairportList["fbgrosssales"]);
            } else {



                $fb_gr_value = '';
            }


            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_gr_value . '</td>';


            $fb_salesep = $fbairportList["fbgrosssales"] / $fbairportList["tenplaning"];



            if ($fb_salesep) {


                $fb_sp_value = '$' . number_format($fb_salesep, 2, '.', '');
            } else {


                $fb_sp_value = '$0.00';
            }


            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_sp_value . '</td>';



            if ($fbairportList["fbrentrev"]) {


                $fb_rv_value = '$' . number_format($fbairportList["fbrentrev"]);
            } else {


                $fb_rv_value = '';
            }




            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_rv_value . '</td>';



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($fbairportList["fbcurrsqft"]) . '</td></tr>';



            $allfbsales+=$fbairportList["fbgrosssales"];



            $allfbsalesep+=$fbairportList["fbgrosssales"] / $fbairportList["aenplaning"];



            $allfbrentrev+=$fbairportList["fbrentrev"];



            $allfbcurrsqft+=$fbairportList["fbcurrsqft"];
        }







        $tbl_header.='<tr><td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



                                <td style="border-top:1px solid #000 ;">&nbsp;</td>';



        if ($allfbsales) {



            $fb_gr_totvalue = '$' . number_format($allfbsales);
        } else {



            $fb_gr_totvalue = '';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_gr_totvalue . '</td>';



        if ($allfbsalesep) {



            $fb_sp_totvalue = '$' . number_format($allfbsalesep, 2, '.', '');
        } else {



            $fb_sp_totvalue.='$0.00';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_sp_totvalue . '</td>';



        if ($allfbrentrev) {



            $fb_rv_totvalue = '$' . number_format($allfbrentrev);
        } else {



            $fb_rv_totvalue = '';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_rv_totvalue . '</td>';




        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($allfbcurrsqft) . '</td></tr></table>



                        ';



        unset($allfbsales);



        unset($allfbsalesep);



        unset($allfbrentrev);



        unset($allfbcurrsqft);



        $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;">Specialty Retail</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>



</tr>';



        $airtermQu5 = $this->common->CustomQuery($airterm);


        foreach ($airtermQu5 as $srairportList) {



            $tbl_header.='


<tr>



<td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $srairportList["terminalabbr"] . '</td>';



            if ($srairportList["srgrosssales"]) {



                $sr_gr_value = '$' . number_format($srairportList["srgrosssales"]);
            } else {



                $sr_gr_value = '';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $sr_gr_value . '</td>';



            $sr_salesep = $srairportList["srgrosssales"] / $srairportList["tenplaning"];



            if ($sr_salesep) {



                $sr_sp_value = '$' . number_format($sr_salesep, 2, '.', '');
            } else {



                $sr_sp_value = '$0.00';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $sr_sp_value . '</td>';



            if ($srairportList["srrentrev"]) {



                $sr_rv_value = '$' . number_format($srairportList["srrentrev"]);
            } else {



                $sr_rv_value = '';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $sr_rv_value . '</td>';



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . number_format($srairportList["srcurrsqft"]) . '</td>




                        </tr>';



            $allsrsales+=$srairportList["srgrosssales"];



            $allsrsalesep+=$srairportList["srgrosssales"] / $srairportList["aenplaning"];



            $allsrrentrev+=$srairportList["srrentrev"];



            $allsrcurrsqft+=$srairportList["srcurrsqft"];
        }



        $tbl_header.='<tr>


<td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>


<td style="border-top:1px solid #000 ;">&nbsp;</td>';



        if ($allsrsales) {



            $sr_gr_totvalue = '$' . number_format($allsrsales);
        } else {



            $sr_gr_totvalue = '';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_gr_totvalue . '</td>';



        if ($allsrsalesep) {



            $sr_sp_totvalue = '$' . number_format($allsrsalesep, 2, '.', '');
        } else {



            $sr_sp_totvalue = '$0.00';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_sp_totvalue . '</td>';



        if ($allsrrentrev) {



            $sr_rv_totvalue.='$' . number_format($allsrrentrev);
        } else {



            $sr_rv_totvalue = '';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_rv_totvalue . '</td>';



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($allsrcurrsqft) . '</td></tr>


</table>


<div style="height:10px"></div>';



        unset($allsrsales);



        unset($allsrsalesep);



        unset($allsrrentrev);



        //unset($allsrrentep);



        unset($allsrcurrsqft);



        $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>News<strong>/</strong>Gifts Retail</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>


</tr>';



        $airtermQu6 = $this->common->CustomQuery($airterm);



        foreach ($airtermQu6 as $ngairportList) {



            $tbl_header.='



<tr>



<td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $ngairportList["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';



            if ($ngairportList["nggrosssales"]) {


                $tbl_header.='$' . number_format($ngairportList["nggrosssales"]) . '</td>';
            } else {


                $tbl_header.='';
            }


            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


            $ng_salesep = $ngairportList["nggrosssales"] / $ngairportList["tenplaning"];


            if ($ng_salesep) {


                $tbl_header.='$' . number_format($ng_salesep, 2, '.', '') . '</td>';
            } else {


                $tbl_header.='$0.00';
            }


            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';



            if ($ngairportList["ngrentrev"]) {


                $tbl_header.='$' . number_format($ngairportList["ngrentrev"]) . '</td>';
            } else {


                $tbl_header.='';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . number_format($ngairportList["ngcurrsqft"]) . '</td>  



                                  </tr>';



            $allngsales+=$ngairportList["nggrosssales"];



            $allngsalesep+=$ngairportList["nggrosssales"] / $ngairportList["aenplaning"];



            $allngrentrev+=$ngairportList["ngrentrev"];



            $allngcurrsqft+=$ngairportList["ngcurrsqft"];
        }




        $tbl_header.='<tr><td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>


<td style="border-top:1px solid #000 ;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($allngsales) {

            $tbl_header.='$' . number_format($allngsales) . '</td>';
        } else {

            $tbl_header.='';
        }


        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


        if ($allngsalesep) {

            $tbl_header.='$' . number_format($allngsalesep, 2, '.', '') . '</td>';
        } else {

            $tbl_header.='$0.00';
        }


        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


        if ($allngrentrev) {

            $tbl_header.='$' . number_format($allngrentrev) . '</td>';
        } else {

            $tbl_header.='';
        }



        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($allngcurrsqft) . '</td></tr></table>';


        unset($allngsales);



        unset($allngsalesep);



        unset($allngrentrev);



        unset($allngrentep);



        unset($allngcurrsqft);



        $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Duty Free Retail</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>



</tr>';


        $airtermQu7 = $this->common->CustomQuery($airterm);


        foreach ($airtermQu7 as $dfairportList) {


            $tbl_header.='


<tr>


<td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $dfairportList["terminalabbr"] . '</td>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


            if ($dfairportList["dfgrosssales"]) {


                $tbl_header.='$' . number_format($dfairportList["dfgrosssales"]) . '</td>';
            } else {


                $tbl_header.='';
            }


            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


            $df_salesep = $dfairportList["dfgrosssales"] / $dfairportList["tepintl"];


            if ($df_salesep) {


                $tbl_header.='$' . number_format($df_salesep, 2, '.', '') . '</td>';
            } else {


                $tbl_header.='$0.00';
            }


            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


            if ($dfairportList["dfrentrev"]) {


                $tbl_header.='$' . number_format($dfairportList["dfrentrev"]) . '</td>';
            } else {


                $tbl_header.='';
            }



            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . number_format($dfairportList["dfcurrsqft"]) . '</td>  



                        </tr>';



            $alldfsales+=$dfairportList["dfgrosssales"];



            $alldfsalesep+=$dfairportList["dfgrosssales"] / $dfairportList["aepintl"];



            $alldfrentrev+=$dfairportList["dfrentrev"];



            $alldfcurrsqft+=$dfairportList["dfcurrsqft"];
        }



        $tbl_header.='<tr><td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>


<td style="border-top:1px solid #000 ;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


        if ($alldfsales) {

            $tbl_header.='$' . number_format($alldfsales) . '</td>';
        } else {

            $tbl_header.='';
        }


        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


        if ($alldfsalesep) {

            $tbl_header.='$' . number_format($alldfsalesep, 2, '.', '') . '</td>';
        } else {

            $tbl_header.='$0.00';
        }


        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


        if ($alldfrentrev) {

            $tbl_header.='$' . number_format($alldfrentrev) . '</td>';
        } else {

            $tbl_header.='';
        }


        $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($alldfcurrsqft) . '</td></tr></table>';

        unset($alldfsales);


        unset($alldfsalesep);


        unset($alldfrentrev);


        unset($alldfcurrsqft);
    }
//                                                            }
//                                                        }
//                                                        if ($resultData["IATA"] != 'SHV') {
//                                                            if ($resultData["IATA"] != 'FWA') {

    $tbl_header.= '<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Concession Tenant Details</td>

</tr>

</table>';


    $tbl_header.='<table width="100%" border="0" style="text-align:left; border-collapse: collapse;" cellspacing="0" cellpadding="0" >';


    $listsql = "select distinct category from lkpcategory";



    $list = $this->common->Selectdistinct('lkpcategory', 'category');

    $l = 0;

    foreach ($list as $l => $cmpnyterms) {


        $catid = $cmpnyterms['category'];

//                                                        $products = $this->common->CustomQuery("select * from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='$catid' and outlets.aid = '$aaid' ORDER BY outletname");

        $products = $this->common->GetAllRowjoinOrderBy('outlets', 'lkpcategory', 'outlets.categoryid = lkpcategory.categoryid', 'left', 'outletname', 'ASC', array('lkpcategory.category' => $catid, 'outlets.aid' => $aaid));

        $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $catid . '(Company)</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Product Description</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal </td>



<td style="white-space: nowrap;padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"># locations</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Sq. Ft.</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Expires</td>



</tr>';


        if (!empty($products))
            foreach ($products as $prod) {


                if ($prod['exp_reason'] != '') {

                    $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $prod['exp_reason']);

                    if ($prod['exp_date'] != '0000-00-00 00:00:00' && $prod['exp_date'] != '' & $prod['exp_reason'] != 'M-T-M') {
                        $date_val = $reasons . '<br>' . date("M d,Y H:i:s", strtotime($prod['exp_date']));
                    } else if (($prod['exp_date'] == '0000-00-00 00:00:00' || $prod['exp_date'] == '') && $prod['exp_reason'] != 'M-T-M') {
                        $date_val = $reasons;
                    } else {
                        $date_val = $reasons;
                    }
                } else {

                    if ($prod['exp'] != "" && (!preg_match("/^[a-z\s\-\/]+$/i", $prod['exp']))) {
                        $date_val = $this->common->get_formatted_datetime($prod['exp']);
                    } else {
                        $date_val = $prod['exp'];
                    }
                }



                if (empty($prod['companyname'])) {

                    $compny = '';
                } else {

                    $compny = ' (' . $prod['companyname'] . ')';
                }

                $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . stripslashes($prod['outletname']) . $compny . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $prod['productdescription'] . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;text-align:center;">' . $prod['termlocation'] . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;text-align:center;">' . $prod['numlocations'] . '</td>



<td style="padding:3px 5px 3px 3px;text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';



                if ($prod['sqft']) {



                    $tbl_header.=number_format($prod['sqft']) . '</td>';
                } else {



                    $tbl_header.='';
                }



                $tbl_header.='<td style="text-align:right;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $date_val . '</td>



                                 </tr>';



                $tottermlocation += $prod['numlocations'];



                $totsqft += $prod['sqft'];
            }


        $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $catid . ' Totals</td>



<td style="border-top:1px solid #000 ;">&nbsp;</td>



<td style="border-top:1px solid #000 ;">&nbsp;</td>



<td style="text-align:center;padding:3px 5px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tottermlocation . '</td>



<td style="text-align:center;padding:3px 5px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



        if ($totsqft) {



            $tbl_header.=number_format($totsqft) . '</td>';
        } else {



            $tbl_header.='</td>';
        }



        $tbl_header.='<td style="border-top:1px solid #000 ;">&nbsp;</td>



                        </tr>';



        unset($tottermlocation);



        unset($totsqft);

//                                                                    $l++;
    }




    $tbl_header.='<tr><td colspan="6">&nbsp;<br><br></td></tr><tr></tr></table>';
}
//                                                        }
//                                                    }
//                                                }
}
}
}
}
}
}
}
}
}



$tbl_header.= '</table></td></tr></table>';

endforeach;

$tbl_header.= '</table>';

$this->session->unset_userdata('list');

$ftext = "AXN's " . date('Y') . " Fact Book";

$$tbl_header.='<p style="text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-top:1px solid #000;">' . $ftext . '</p>';

$tbl_header.= "</body>";

$tbl_header.= "</html>";
//var_dump($tbl_header); exit();

echo $tbl_header;
}

/**
* 
* End Multiple Airports DOC Report
*/


/**
 *
 * Start Multiple Airports Excel Report (Required ID)
 */
public function GenExcel() {


    $val = 'Multi Airports Report';

    $laid = $this->session->userdata('list');
 
    $objQuery = $this->common->GetAllWhereIn("aid,IATA,aname,acity,astate,mgtstructure", 'airports', 'aid', $laid);
    
    header('Content-Type: application/octet-stream');

    header("Content-Disposition: attachment;Filename=" . $val . ".xls");

    $tbl_header.= "<html>";

    $tbl_header.= '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">'
        . '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">';

    $tbl_header.= "<body style='border: 1px solid #ccc'>";

    foreach ($objQuery as $resultData):

        $aaid = $resultData["aid"];

        $tbl_header.='<div style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["acity"] . ', ' . $resultData["astate"] . '</div>';

        $tbl_header.='<table width="100%" border="0" '.$tablestyle.'>

<tr>

<td style="color:#000;display:block;width:100%;text-align:left;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:9pt;font-weight:bold;">' . $resultData["aname"] . '</td>

</tr>

<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["IATA"] . '</td>

</tr>

</table>

<table width="100%" border="0" cellpadding="2" '.$tablestyle.'>

<tr bgcolor="#999999"  style="color:#fff;">

<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt !important; line-height:14px !important;font-weight:bold;color:#FFFFFF">Contact(s)</td>

</tr>

</table>';

        $tbl_header.= '<table width="100%" border="0" '.$tablestyle.'>';


        $join_array = array(
            array('lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT')
        );
        
        $dataQuery = $this->common->JoinTables("*", "airportcontacts", $join_array, array('aid' => $aaid), "airportcontacts.priority");

        $count = 0;

        foreach ($dataQuery as $resultData3):

            if ($count % 2 == 0)
                $tbl_header.= '<tr width="40%" >';

            $tbl_header.= '

<td><table width="40%" border="0" '.$tablestyle.'>

<tr>



<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">' . $resultData3["mgtresponsibilityname"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Contact</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["afname"] . ' ' . $resultData3["alname"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Title</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["atitle"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Company</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["acompany"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Address</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["aaddress1"] . '</td>



</tr>';



        

            if ($resultData3["aaddress2"]) {


                $tbl_header.= '<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["aaddress2"] . '</td>



</tr>';
            }


            $tbl_header.= '<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["accity"] . ', ' . $resultData3["acstate"] . ' ' . $resultData3["aczip"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Phone</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["aphone"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Fax</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["afax"] . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Email</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . mailto($resultData3["aemail"]) . '</td>



</tr>



<tr>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Website</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"><a href="http://' . $resultData3["awebsite"] . '">' . $resultData3["awebsite"] . '</a></td>



</tr>

</table></td>';


            if ($count % 2 != 0) {
                $tbl_header.= '</tr>';
            }
            $count++;

        endforeach;

        if (count($dataQuery) == 0) {
            $tbl_header.= '<tr><td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">No Contact Found</td>


</tr></table></td></tr>';
        }

        $tbl_header.= '</table>';

        /* End Result Data 3__________________________________________________  */

        $tbl_header.= '<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;background-color:red;">Airport Info</td>

</tr>

</table>';


        $airterm = "SELECT * FROM `airports` LEFT JOIN airportsannual ON airports.aid=airportsannual.aid 

INNER JOIN terminals ON airports.aid=terminals.aid INNER JOIN terminalsannual ON terminals.tid=terminalsannual.tid 

INNER JOIN airporttotals ON airports.aid=airporttotals.aid 

WHERE airports.aid=" . $this->db->escape($aaid) . " ORDER BY airports.aid, terminals.terminalabbr ASC";


        $resultDatao = $this->common->CustomQueryROw($airterm);

        $ap_count = $this->common->CustomCountQuery($airterm);

        $exp_countall = $resultDatao["texpansionplanned"];

        $exp_count = explode("#", $exp_countall);

        $addsqft1 = $resultDatao["addsqft"];

        $addsqft = str_replace(",", "", $addsqft1);

        $addsq_count = explode("#", $addsqft);

        $completedexpdate = $resultDatao["completedexpdate"];

        $completed_count = explode("#", $completedexpdate);

        $add_sq = "Add'l Sq. Ft.";

        $tbl_header.='


<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:left;border-collapse: collapse;">

<tr>

<td width="50%" style="vertical-align:top;padding:0;" valign="top"> 

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0 0 0 5px;">

<tr >

<td width="50%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Airport Info </td>

<td width="200px" style="padding:0;padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

</tr>

<tr>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Airport Configuration</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["configuration"] . '</td>

</tr>

<tr>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Concessions Mgt. Type</td>

<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["mgtstructure"] . '</td>




</tr>';


        for ($kk = 0; $kk < count($exp_count); $kk++) {

            $tbl_header.='
<tr >



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Expansion Planned</td>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $exp_count[$kk] . '</td>



</tr>



<tr>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $add_sq . '</td>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format((float) $addsq_count[$kk]) . '</td>



</tr>



<tr>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Complete Date</td>



<td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $completed_count[$kk] . '</td>



</tr>';
        }


        $tbl_header.='</table>


</td> 

</td>


<td width="50%" valign="top" style="vertical-align:top;padding:0;">



<table width="100%" border="0" cellpadding="0" cellspacing="0" >



<tr>



<td width="35%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminals/Conc.</td>



<td width="30%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Abbr.</td>



<td width="35%" style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Dominant Airline</td>



</tr>';

        $resultData2 = $this->common->CustomQuery($airterm);



        for ($r = 0; $r < $ap_count; $r++) {


            $tbl_header.= '<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultData2[$r]["terminalname"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultData2[$r]["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultData2[$r]["tdominantair"] . '</td>



</tr> ';
        }

        $tbl_header.='</table> </td></tr></table>';

        if ($resultData["IATA"] != 'ORF') {


            if ($resultData["IATA"] != 'AVL') {


                if ($resultData["IATA"] != 'COS1') {


                    if ($resultData["IATA"] != 'HRL') {


                        if ($resultData["IATA"] != 'MEM1') {


                            if ($resultData["IATA"] != 'YYZ1') {


                                if ($resultData["IATA"] != 'OKC') {


                                    if ($resultData["IATA"] != 'MSY1') {


                                        $tbl_header.='


<table width="100%" border="0" cellspacing="0" cellpadding="0">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Passenger Traffic</td>



</tr>



</table>';


                                        $tbl_header.='  <table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:right; border-collapse: collapse;">

<tr style="font-family:arial;font-size:9pt;font-weight:bold;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">+ / - %</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Deplaning</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Enplaning</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">EP Domestic</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:14.3%">EP Int l</td>



</tr>';


                                        $airportlist = $this->common->CustomQuery($airterm);



                                        for ($s = 0; $s < $ap_count; $s++) {




                                            $tbl_header.='<tr>



<td style="padding:3px 20px 3px 3px;text-align:left ! important; font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist[$s]["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tpasstraffic"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $airportlist[$s]["tpasstrafficcompare"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tdeplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tenplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tepdomestic"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($airportlist[$s]["tepintl"]) . '</td>



</tr>';
                                        }



                                        $tbl_header.='<tr><td style="text-align:letf;padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["apasstraffic"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["apasstrafficcompare"] . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["adeplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aenplaning"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aepdomestic"]) . '</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["aepintl"]) . '</td></tr></table>';



                                        if ($resultDatao["apasscomment"] != '') {



                                            $tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["apasscomment"] . '</p>';
                                        }



                                        if ($resultDatao["avgdwelltime"] != '') {



                                            $avg_time = $resultDatao["avgdwelltime"] . ' minutes';
                                        }



                                        if ($resultData["IATA"] != 'VPS') {



                                            $tbl_header.=' 





<table width="100%" border="0" cellspacing="0" cellpadding="0"  style ="border-collapse: collapse;">




<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold; "> Airport Percentages</td>


</tr>


</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:left; border-collapse: collapse;">



<tr style="font-weight:bold;">



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Pre/Post Security</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Business to Leisure Ratio</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">OD Transfer</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Average Dwell Time</td>



</tr>



<tr>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["presecurity"] . '/' . $resultDatao["postsecurity"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["ratiobusleisure"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["ondtransfer"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $avg_time . '</td>



</tr></table>';



                                            $tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style ="border-collapse: collapse;">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;"> Airportwide Info</td>



</tr>



</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:right; border-collapse: collapse;">



<tr >



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Parking</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" >Short</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Long</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Economy</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Valet</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Car Rentals</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Agencies</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Rev</td>



<td style="text-align:right;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Rentals</td>



</tr>';



                                            $tbl_header.='<tr>



<td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Hourly</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["hourlyshort"]) {



                                                $tbl_header.='$' . number_format($resultDatao["hourlyshort"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["hourlylong"]) {



                                                $tbl_header.='$' . number_format($resultDatao["hourlylong"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["hourlyeconomy"]) {



                                                $tbl_header.='$' . number_format($resultDatao["hourlyeconomy"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["hourlyvalet"]) {



                                                $tbl_header.='$' . number_format($resultDatao["hourlyvalet"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental On Site</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["carrentalagenciesonsite"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["carrentalrevonsite"]) {



                                                $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevonsite"]), 2) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["carrentalrevtoaironsite"]) {



                                                $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevtoaironsite"]), 2) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='</tr>




<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Daily</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["dailyshort"]) {



                                                $tbl_header.='$' . number_format($resultDatao["dailyshort"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["dailylong"]) {



                                                $tbl_header.='$' . number_format($resultDatao["dailylong"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["dailyeconomy"]) {



                                                $tbl_header.='$' . number_format($resultDatao["dailyeconomy"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["dailyvalet"]) {



                                                $tbl_header.='$' . number_format($resultDatao["dailyvalet"], 2, '.', '') . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental Off Site:</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["carrentalagenciesoffsite"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["carrentalrevoffsite"]) {



                                                $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevoffsite"]), 2) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["carrentalrevtoairoffsite"]) {



                                                $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao["carrentalrevtoairoffsite"]), 2) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='</tr>



<tr>



<td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;"># Spaces</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["spacesshort"]) . '</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["spaceslong"]) . '</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["spaceseconomy"]) . '</td>



<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["spacesvalet"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Total Cars Rented</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["totalcarsrented"]) . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>



</tr>




<tr>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental Sq. Ft.</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . number_format($resultDatao["carrentalsqft"]) . '</td>



<td>&nbsp;</td>



</tr>



</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:left; border-collapse: collapse;">



<tr>



<td style="width:15%;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Parking Revenue</td>



<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["parkingrev"]) {



                                                $tbl_header.='$' . number_format($this->common->parseNumber($resultDatao['parkingrev']), 2) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Total Spaces</td>



<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["parkingspaces"]) {



                                                $tbl_header.=number_format($resultDatao['parkingspaces']) . '</td>';
                                            }



                                            $tbl_header.='<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td>



<td>&nbsp;</td><td>&nbsp;</td>



</tr>



</table>';



                                            $tbl_header.='



<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:right; border-collapse: collapse;">



<tr style="font-weight:bold;">



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;width:160px" width="150px">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;font-weight:bold;border-bottom:1px solid #000;">Revenue</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;font-weight:bold;border-bottom:1px solid #000;">Rev. to Airport</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



<td style="border-bottom:1px solid #000;">&nbsp;</td>



</tr>


<tr>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Passenger Services</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["passservicesrev"]) {



                                                $tbl_header.='$' . number_format($resultDatao["passservicesrev"]) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["passservicesrevtoair"]) {


                                                $tbl_header.='$' . number_format($resultDatao["passservicesrevtoair"]) . '</td>';
                                            } else {


                                                $tbl_header.='$0.00';
                                            }


                                            $tbl_header.='</tr>



<tr>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Advertising</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["advertisingrev"]) {



                                                $tbl_header.='$' . number_format($resultDatao["advertisingrev"]) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["advertisingrevtoair"]) {



                                                $tbl_header.='$' . number_format($resultDatao["advertisingrevtoair"]) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='</tr>



<tr>

<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Currency Exchange</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["currencyexrev"]) {



                                                $tbl_header.='$' . number_format($resultDatao["currencyexrev"]) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';



                                            if ($resultDatao["currencyexrevtoair"]) {



                                                $tbl_header.='$' . number_format($resultDatao["currencyexrevtoair"]) . '</td>';
                                            } else {



                                                $tbl_header.='$0.00';
                                            }



                                            $tbl_header.='</tr></table>';



                                            if ($resultDatao["awrevcomment"] != '') {



                                                $tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["awrevcomment"] . '</p>';
                                            }

                                            if ($resultData["IATA"] != 'MSN') {



//                                                    if ($resultData["IATA"] != 'RNO1') {



                                                if ($resultData["IATA"] != 'RIC') {



//                                                            if ($resultData["IATA"] != 'SHV') {
//                                                                if ($resultData["IATA"] != 'FWA') {



                                                    $tbl_header.='<table width="100%">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;" >



<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Concession Totals - Terminal Breakdowns <span style="font-size:9px;">(Food/Beverage, Specialty



Retail, News/Gifts Only)</span></td>



</tr>



</table>


<table width="100%" border="0" style="text-align:right;  border-collapse: collapse; margin-bottom:100px;" border="0" cellspacing="0" cellpadding="0"   >



<tr style="font-weight:bold;">



<td style="text-align:left;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Sales EP</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Rent EP</td>



<td style="text-align:right;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>


</tr>';


                                                    $airtermQu3 = $this->common->CustomQuery($airterm);


                                                    foreach ($airtermQu3 as $totairportList) {



                                                        $totgr = $totairportList["fbgrosssales"] + $totairportList["srgrosssales"] + $totairportList["nggrosssales"] . "";



                                                        $totsalesep = $totgr / $totairportList["tenplaning"];



                                                        $totrentrev = $totairportList["fbrentrev"] + $totairportList["srrentrev"] + $totairportList["ngrentrev"];



                                                        $totrentep = $totrentrev / $totairportList["tenplaning"];



                                                        $totcurrsqft = $totairportList["fbcurrsqft"] + $totairportList["srcurrsqft"] + $totairportList["ngcurrsqft"];



                                                        $tbl_header.='<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">' . $totairportList["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



                                                        if ($totgr) {



                                                            $tbl_header.='$' . number_format($totgr) . '</td>';
                                                        } else {



                                                            $tbl_header.='';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



                                                        if ($totsalesep) {



                                                            $tbl_header.='$' . number_format($totsalesep, 2, '.', '') . '</td>';
                                                        } else {



                                                            $tbl_header.='$0.00';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



                                                        if ($totrentrev) {



                                                            $tbl_header.='$' . number_format($totrentrev) . '</td>';
                                                        } else {



                                                            $tbl_header.='';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">';



                                                        if ($totrentep) {



                                                            $tbl_header.='$' . number_format($totrentep, 2, '.', '') . '</td>';
                                                        } else {



                                                            $tbl_header.='';
                                                        }



                                                        $tbl_header.='<td style="text-align:right;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border-bottom:1px solid #ddd;">' . number_format($totcurrsqft) . '</td>



                    </tr>';



                                                        $totgrall += $totgr;



                                                        $totsalesepall+= $totgr / $totairportList["aenplaning"];



                                                        $totrentrevall += $totrentrev;



                                                        $totrentepall += $totrentrev / $totairportList["aenplaning"];



                                                        $totcurrsqftall += $totcurrsqft;
                                                    }


                                                    $tbl_header.='<tr>



<td style="text-align:left;padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



                                                    if ($totgrall) {



                                                        $tbl_header.='$' . number_format($totgrall) . '</td>';
                                                    } else {



                                                        $tbl_header.='';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



                                                    if ($totsalesepall) {



                                                        $tbl_header.='$' . number_format($totsalesepall, 2, '.', '') . '</td>';
                                                    } else {



                                                        $tbl_header.='$0.00';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



                                                    if ($totrentrevall) {



                                                        $tbl_header.='$' . number_format($totrentrevall) . '</td>';
                                                    } else {



                                                        $tbl_header.='';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



                                                    if ($totrentepall) {



                                                        $tbl_header.='$' . number_format($totrentepall, 2, '.', '') . '</td>';
                                                    } else {



                                                        $tbl_header.='';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($totcurrsqftall) . '</td>



                        </tr>';


                                                    unset($totgrall);



                                                    unset($totsalesepall);



                                                    unset($totrentrevall);



                                                    unset($totrentepall);



                                                    unset($totcurrsqftall);



                                                    $tbl_header.='</table>';



                                                    if ($resultDatao["grosscomment"] != '') {



                                                        $tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">' . $resultDatao["grosscomment"] . '</p>';
                                                    }



                                                    $sym = 'Food/Beverage';



                                                    $tbl_header.='<table width="100%" border="0" cellpadding="2">



<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">


<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Concession Program Details - Category Breakdowns</td>


</tr>


</table>



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse; margin-bottom:10px;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Food<strong>/</strong>Beverage</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:10%">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>



</tr>';



                                                    $airtermQu4 = $this->common->CustomQuery($airterm);


                                                    foreach ($airtermQu4 as $fbairportList) {


                                                        $all_fb = $fbairportList["fbsalesep"];



                                                        $tbl_header.='<tr>


<td style="border-bottom:1px solid #ddd;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fbairportList["terminalabbr"] . '</td>';



                                                        if ($fbairportList["fbgrosssales"] && $fbairportList["fbgrosssales"] !== '0') {



                                                            $fb_gr_value = '$' . number_format($fbairportList["fbgrosssales"]);
                                                        } else {



                                                            $fb_gr_value = '';
                                                        }


                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_gr_value . '</td>';


                                                        $fb_salesep = $fbairportList["fbgrosssales"] / $fbairportList["tenplaning"];



                                                        if ($fb_salesep) {


                                                            $fb_sp_value = '$' . number_format($fb_salesep, 2, '.', '');
                                                        } else {


                                                            $fb_sp_value = '$0.00';
                                                        }


                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_sp_value . '</td>';



                                                        if ($fbairportList["fbrentrev"]) {


                                                            $fb_rv_value = '$' . number_format($fbairportList["fbrentrev"]);
                                                        } else {


                                                            $fb_rv_value = '';
                                                        }




                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . $fb_rv_value . '</td>';



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #ddd;">' . number_format($fbairportList["fbcurrsqft"]) . '</td></tr>';



                                                        $allfbsales+=$fbairportList["fbgrosssales"];



                                                        $allfbsalesep+=$fbairportList["fbgrosssales"] / $fbairportList["aenplaning"];



                                                        $allfbrentrev+=$fbairportList["fbrentrev"];



                                                        $allfbcurrsqft+=$fbairportList["fbcurrsqft"];
                                                    }







                                                    $tbl_header.='<tr><td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>



                            <td style="border-top:1px solid #000 ;">&nbsp;</td>';



                                                    if ($allfbsales) {



                                                        $fb_gr_totvalue = '$' . number_format($allfbsales);
                                                    } else {



                                                        $fb_gr_totvalue = '';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_gr_totvalue . '</td>';



                                                    if ($allfbsalesep) {



                                                        $fb_sp_totvalue = '$' . number_format($allfbsalesep, 2, '.', '');
                                                    } else {



                                                        $fb_sp_totvalue.='$0.00';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_sp_totvalue . '</td>';



                                                    if ($allfbrentrev) {



                                                        $fb_rv_totvalue = '$' . number_format($allfbrentrev);
                                                    } else {



                                                        $fb_rv_totvalue = '';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $fb_rv_totvalue . '</td>';




                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($allfbcurrsqft) . '</td></tr></table>



                    ';



                                                    unset($allfbsales);



                                                    unset($allfbsalesep);



                                                    unset($allfbrentrev);



                                                    unset($allfbcurrsqft);



                                                    $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;">Specialty Retail</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>



</tr>';



                                                    $airtermQu5 = $this->common->CustomQuery($airterm);


                                                    foreach ($airtermQu5 as $srairportList) {



                                                        $tbl_header.='


<tr>



<td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $srairportList["terminalabbr"] . '</td>';



                                                        if ($srairportList["srgrosssales"]) {



                                                            $sr_gr_value = '$' . number_format($srairportList["srgrosssales"]);
                                                        } else {



                                                            $sr_gr_value = '';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $sr_gr_value . '</td>';



                                                        $sr_salesep = $srairportList["srgrosssales"] / $srairportList["tenplaning"];



                                                        if ($sr_salesep) {



                                                            $sr_sp_value = '$' . number_format($sr_salesep, 2, '.', '');
                                                        } else {



                                                            $sr_sp_value = '$0.00';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $sr_sp_value . '</td>';



                                                        if ($srairportList["srrentrev"]) {



                                                            $sr_rv_value = '$' . number_format($srairportList["srrentrev"]);
                                                        } else {



                                                            $sr_rv_value = '';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $sr_rv_value . '</td>';



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . number_format($srairportList["srcurrsqft"]) . '</td>




                    </tr>';



                                                        $allsrsales+=$srairportList["srgrosssales"];



                                                        $allsrsalesep+=$srairportList["srgrosssales"] / $srairportList["aenplaning"];



                                                        $allsrrentrev+=$srairportList["srrentrev"];



                                                        $allsrcurrsqft+=$srairportList["srcurrsqft"];
                                                    }



                                                    $tbl_header.='<tr>


<td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>


<td style="border-top:1px solid #000 ;">&nbsp;</td>';



                                                    if ($allsrsales) {



                                                        $sr_gr_totvalue = '$' . number_format($allsrsales);
                                                    } else {



                                                        $sr_gr_totvalue = '';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_gr_totvalue . '</td>';



                                                    if ($allsrsalesep) {



                                                        $sr_sp_totvalue = '$' . number_format($allsrsalesep, 2, '.', '');
                                                    } else {



                                                        $sr_sp_totvalue = '$0.00';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_sp_totvalue . '</td>';



                                                    if ($allsrrentrev) {



                                                        $sr_rv_totvalue.='$' . number_format($allsrrentrev);
                                                    } else {



                                                        $sr_rv_totvalue = '';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $sr_rv_totvalue . '</td>';



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($allsrcurrsqft) . '</td></tr>


</table>


<div style="height:10px"></div>';



                                                    unset($allsrsales);



                                                    unset($allsrsalesep);



                                                    unset($allsrrentrev);



                                                    //unset($allsrrentep);



                                                    unset($allsrcurrsqft);



                                                    $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>News<strong>/</strong>Gifts Retail</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>


</tr>';



                                                    $airtermQu6 = $this->common->CustomQuery($airterm);



                                                    foreach ($airtermQu6 as $ngairportList) {



                                                        $tbl_header.='



<tr>



<td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $ngairportList["terminalabbr"] . '</td>



<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';



                                                        if ($ngairportList["nggrosssales"]) {


                                                            $tbl_header.='$' . number_format($ngairportList["nggrosssales"]) . '</td>';
                                                        } else {


                                                            $tbl_header.='';
                                                        }


                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


                                                        $ng_salesep = $ngairportList["nggrosssales"] / $ngairportList["tenplaning"];


                                                        if ($ng_salesep) {


                                                            $tbl_header.='$' . number_format($ng_salesep, 2, '.', '') . '</td>';
                                                        } else {


                                                            $tbl_header.='$0.00';
                                                        }


                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';



                                                        if ($ngairportList["ngrentrev"]) {


                                                            $tbl_header.='$' . number_format($ngairportList["ngrentrev"]) . '</td>';
                                                        } else {


                                                            $tbl_header.='';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . number_format($ngairportList["ngcurrsqft"]) . '</td>  



                              </tr>';



                                                        $allngsales+=$ngairportList["nggrosssales"];



                                                        $allngsalesep+=$ngairportList["nggrosssales"] / $ngairportList["aenplaning"];



                                                        $allngrentrev+=$ngairportList["ngrentrev"];



                                                        $allngcurrsqft+=$ngairportList["ngcurrsqft"];
                                                    }




                                                    $tbl_header.='<tr><td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>


<td style="border-top:1px solid #000 ;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



                                                    if ($allngsales) {

                                                        $tbl_header.='$' . number_format($allngsales) . '</td>';
                                                    } else {

                                                        $tbl_header.='';
                                                    }


                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


                                                    if ($allngsalesep) {

                                                        $tbl_header.='$' . number_format($allngsalesep, 2, '.', '') . '</td>';
                                                    } else {

                                                        $tbl_header.='$0.00';
                                                    }


                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


                                                    if ($allngrentrev) {

                                                        $tbl_header.='$' . number_format($allngrentrev) . '</td>';
                                                    } else {

                                                        $tbl_header.='';
                                                    }



                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($allngcurrsqft) . '</td></tr></table>';


                                                    unset($allngsales);



                                                    unset($allngsalesep);



                                                    unset($allngrentrev);



                                                    unset($allngrentep);



                                                    unset($allngcurrsqft);



                                                    $tbl_header.='



<table width="100%" border="0"  style="text-align:right; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" >



<tr style="font-weight:bold;color:#a20000;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Duty Free Retail</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:10%;">Terminal</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100" align="right">Gross Sales</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="100"><span>Sales<strong>/</strong>EP</span></td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; width:25%;">Total Rent To Airport</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" width="120">Current Sq. Ft</td>



</tr>';


                                                    $airtermQu7 = $this->common->CustomQuery($airterm);


                                                    foreach ($airtermQu7 as $dfairportList) {


                                                        $tbl_header.='


<tr>


<td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $dfairportList["terminalabbr"] . '</td>


<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


                                                        if ($dfairportList["dfgrosssales"]) {


                                                            $tbl_header.='$' . number_format($dfairportList["dfgrosssales"]) . '</td>';
                                                        } else {


                                                            $tbl_header.='';
                                                        }


                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


                                                        $df_salesep = $dfairportList["dfgrosssales"] / $dfairportList["tepintl"];


                                                        if ($df_salesep) {


                                                            $tbl_header.='$' . number_format($df_salesep, 2, '.', '') . '</td>';
                                                        } else {


                                                            $tbl_header.='$0.00';
                                                        }


                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';


                                                        if ($dfairportList["dfrentrev"]) {


                                                            $tbl_header.='$' . number_format($dfairportList["dfrentrev"]) . '</td>';
                                                        } else {


                                                            $tbl_header.='';
                                                        }



                                                        $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . number_format($dfairportList["dfcurrsqft"]) . '</td>  



                    </tr>';



                                                        $alldfsales+=$dfairportList["dfgrosssales"];



                                                        $alldfsalesep+=$dfairportList["dfgrosssales"] / $dfairportList["aepintl"];



                                                        $alldfrentrev+=$dfairportList["dfrentrev"];



                                                        $alldfcurrsqft+=$dfairportList["dfcurrsqft"];
                                                    }



                                                    $tbl_header.='<tr><td style="text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>


<td style="border-top:1px solid #000 ;">&nbsp;</td>


<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


                                                    if ($alldfsales) {

                                                        $tbl_header.='$' . number_format($alldfsales) . '</td>';
                                                    } else {

                                                        $tbl_header.='';
                                                    }


                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


                                                    if ($alldfsalesep) {

                                                        $tbl_header.='$' . number_format($alldfsalesep, 2, '.', '') . '</td>';
                                                    } else {

                                                        $tbl_header.='$0.00';
                                                    }


                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';


                                                    if ($alldfrentrev) {

                                                        $tbl_header.='$' . number_format($alldfrentrev) . '</td>';
                                                    } else {

                                                        $tbl_header.='';
                                                    }


                                                    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . number_format($alldfcurrsqft) . '</td></tr></table>';

                                                    unset($alldfsales);


                                                    unset($alldfsalesep);


                                                    unset($alldfrentrev);


                                                    unset($alldfcurrsqft);
                                                }
//                                                            }
//                                                        }
//                                                        if ($resultData["IATA"] != 'SHV') {
//                                                            if ($resultData["IATA"] != 'FWA') {

                                                $tbl_header.= '<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Concession Tenant Details</td>

</tr>

</table>';


                                                $tbl_header.='<table width="100%" border="0" style="text-align:left; border-collapse: collapse;" cellspacing="0" cellpadding="0" >';


                                                $listsql = "select distinct category from lkpcategory";



                                                $list = $this->common->Selectdistinct('lkpcategory', 'category');

                                                $l = 0;

                                                foreach ($list as $l => $cmpnyterms) {


                                                    $catid = $cmpnyterms['category'];

//                                                        $products = $this->common->CustomQuery("select * from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='$catid' and outlets.aid = '$aaid' ORDER BY outletname");

                                                    $products = $this->common->GetAllRowjoinOrderBy('outlets', 'lkpcategory', 'outlets.categoryid = lkpcategory.categoryid', 'left', 'outletname', 'ASC', array('lkpcategory.category' => $catid, 'outlets.aid' => $aaid));

                                                    $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $catid . '(Company)</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Product Description</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal </td>



<td style="white-space: nowrap;padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"># locations</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Sq. Ft.</td>



<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Expires</td>



</tr>';


                                                    if (!empty($products))
                                                        foreach ($products as $prod) {


                                                            if ($prod['exp_reason'] != '') {

                                                                $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $prod['exp_reason']);

                                                                if ($prod['exp_date'] != '0000-00-00 00:00:00' && $prod['exp_date'] != '' & $prod['exp_reason'] != 'M-T-M') {
                                                                    $date_val = $reasons . '<br>' . date("M d,Y H:i:s", strtotime($prod['exp_date']));
                                                                } else if (($prod['exp_date'] == '0000-00-00 00:00:00' || $prod['exp_date'] == '') && $prod['exp_reason'] != 'M-T-M') {
                                                                    $date_val = $reasons;
                                                                } else {
                                                                    $date_val = $reasons;
                                                                }
                                                            } else {

                                                                if ($prod['exp'] != "" && (!preg_match("/^[a-z\s\-\/]+$/i", $prod['exp']))) {
                                                                    $date_val = $this->common->get_formatted_datetime($prod['exp']);
                                                                } else {
                                                                    $date_val = $prod['exp'];
                                                                }
                                                            }



                                                            if (empty($prod['companyname'])) {

                                                                $compny = '';
                                                            } else {

                                                                $compny = ' (' . $prod['companyname'] . ')';
                                                            }

                                                            $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . stripslashes($prod['outletname']) . $compny . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $prod['productdescription'] . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;text-align:center;">' . $prod['termlocation'] . '</td>



<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;text-align:center;">' . $prod['numlocations'] . '</td>



<td style="padding:3px 5px 3px 3px;text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';



                                                            if ($prod['sqft']) {



                                                                $tbl_header.=number_format($prod['sqft']) . '</td>';
                                                            } else {



                                                                $tbl_header.='';
                                                            }



                                                            $tbl_header.='<td style="text-align:right;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">' . $date_val . '</td>



                             </tr>';



                                                            $tottermlocation += $prod['numlocations'];



                                                            $totsqft += $prod['sqft'];
                                                        }


                                                    $tbl_header.='<tr>



<td style="width:45%;padding:3px 10px 3px 3px;text-align:left;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $catid . ' Totals</td>



<td style="border-top:1px solid #000 ;">&nbsp;</td>



<td style="border-top:1px solid #000 ;">&nbsp;</td>



<td style="text-align:center;padding:3px 5px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">' . $tottermlocation . '</td>



<td style="text-align:center;padding:3px 5px 3px 3px;border-top:1px solid #000 ;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';



                                                    if ($totsqft) {



                                                        $tbl_header.=number_format($totsqft) . '</td>';
                                                    } else {



                                                        $tbl_header.='</td>';
                                                    }



                                                    $tbl_header.='<td style="border-top:1px solid #000 ;">&nbsp;</td>



                    </tr>';



                                                    unset($tottermlocation);



                                                    unset($totsqft);

//                                                                    $l++;
                                                }




                                                $tbl_header.='<tr><td colspan="6">&nbsp;<br><br></td></tr><tr></tr></table>';
                                            }
//                                                        }
//                                                    }
//                                                }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        $tbl_header.= '</table></td></tr></table>';

    endforeach;

    $tbl_header.= '</table>';

    $this->session->unset_userdata('list');

    $ftext = "AXN's " . date('Y') . " Fact Book";

    $$tbl_header.='<p style="text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-top:1px solid #000;">' . $ftext . '</p>';

    $tbl_header.= "</body>";

    $tbl_header.= "</html>";
//var_dump($tbl_header); exit();

    echo $tbl_header;
}

/**
 *
 * End Multiple Airports Excel Report
 */


/**
* 
* Start Multiple Companies PDF Report (Required ID)
*/
public function GenPdfCompany() {


$val = 'Multi Companies Report';

$mpdf = new mPDF('utf-8', 'Letter');

$lcid = $this->session->userdata('listofcomp');


$objQuery = $this->common->GetAllWhereIn("cid,companyname,companytype,companyinfo", 'companies', 'cid', $lcid);
$count1 = 0;

foreach ($objQuery as $resultData):

$ccid = $resultData["cid"];

$mpdf->SetHTMLHeader('<div style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $val . '</div>');

$tbl_header.='<table width="100%" border="0">

<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["companyname"] . '</td>

</tr>
<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["companytype"] . '</td>

</tr>

</table>

<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Contact(s)</td>

</tr>

</table>';

$tbl_header.= '<table width="100%" border="0">';


$where = "companycontacts.cid = '" . $ccid . "' AND companycontacts.is_active=1 ORDER BY display_order";
$dataQuery = $this->outputData['ccontact'] = $this->common->JoinTable('*', 'companycontacts', 'companies', 'companycontacts.cid=companies.cid', 'LEFT', $where);

$count = 0;

foreach ($dataQuery as $resultData3):

if ($count % 2 == 0)
$tbl_header.= '<tr width="40%" >';

$tbl_header.= '

<td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">' . $resultData3["cfname"] . ' ' . $resultData3["clname"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Contact</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cfname"] . ' ' . $resultData3["clname"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Title</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["ctitle"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Address</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["caddress1"] . '</td>


</tr>';


if ($resultData3["caddress2"]) {


$tbl_header.= '<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["caddress2"] . '</td>


</tr>';
}


$tbl_header.= '<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["ccity"] . ', ' . $resultData3["cstate"] . ' ' . $resultData3["czip"] . ' ' . $resultData3["ccountry"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Phone</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cphone"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Fax</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cfax"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Email</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cemail"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Website</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cwebsite"] . '</td>



</tr>


</table></td>';

if ($count % 2 == 0) {
$tbl_header.= '</tr>';
}
$count++;

endforeach;

if (count($dataQuery) == 0) {
$tbl_header.= '<tr><td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">No Contact Found</td>


</tr></table></td></tr>';
}

$tbl_header.= '</table>';

/* End Result Data 3__________________________________________________  */

$tbl_header.= '<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Company Info</td>

</tr>

</table>';


$rows = 1;

$nword = str_word_count($resultData["companyinfo"]);

$count1++;

$rows = $nword / 10;

$tbl_header.='

<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:left;border-collapse: collapse;">

<tr>

<td style="vertical-align:top;padding:0;border-bottom:1px solid #000;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">


<textarea style="width: 100% ! important; background-color: #FFFFFFFF ! important; vertical-align:top ! important;padding:0 ! important;font-size:8pt ! important;font-family:Vectora LH 55 Roman,arial ! important;" rows="' . $rows . '" >

' . $this->common->htmlallentities($resultData["companyinfo"]) . '</textarea>';

$tbl_header.= '</td></tr>

</table>';

$tbl_header.='


<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">


<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">


<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Company Location</td>


</tr>


</table>';


$tbl_header.='  <table width="100%" border="0" cellspacing="0" cellpadding="0"  style="text-align:left; border-collapse: collapse;">



<tr style="font-family:arial;font-size:9pt;font-weight:bold;">



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:9pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Location</td>



</tr>';


$cloc = $this->common->CustomQueryALL("SELECT st1.*, st2.* FROM (SELECT `aid`, `acity`, `astate`,`aname` FROM `airports` 
    UNION SELECT `aid`, `acity`, `astate`,`aname` FROM `lkpairportlist`) st1 
    LEFT JOIN `companylocations` st2 ON `st2`.`aid` = `st1`.`aid`
    LEFT JOIN `companies` ON `companies`.`cid` = `st2`.`cid` 
    WHERE `companies`.`cid` = " . $this->db->escape($ccid) . " ORDER BY `st1`.`acity`, `st1`.`astate`");

//var_dump($loc['aname']);exit();

$c = 1;

foreach ($cloc as $loc):

if ($loc['acity'] != "") {


$tbl_header.='<tr>



<td style="width:900px; padding:3px 20px 3px 3px;font-size:9pt;font-family:Vectora LH 55 Roman,arial;">' . $loc['acity'] . ",&nbsp;" . $loc['astate'] . "&nbsp;(" . $loc['aname'] . ")" . ' </td> </tr>';
}

$c++;
endforeach;

$tbl_header.='</table>';

endforeach;

$this->session->unset_userdata('listofcomp');

$ftext = "AXN's " . date('Y') . " Fact Book";

$mpdf->SetHTMLFooter('<p style="text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-top:1px solid #000;">' . $ftext . '</p>');

$mpdf->WriteHTML($tbl_header);

$mpdf->Output($val . '.pdf', 'D');
}

/**
* 
* End Multiple Companies PDF Report
*/

/**
* 
* Start Multiple Companies DOC Report (Required ID)
*/
public function Companyexporttodoc() {

$val = 'Multi Companies Report';

$lcid = $this->session->userdata('listofcomp');


$objQuery = $this->common->GetAllWhereIn("cid,companyname,companytype,companyinfo", 'companies', 'cid', $lcid);

header("Content-type: application/vnd.ms-word");

header("Content-Disposition: attachment;Filename=" . $val . ".doc");

$tbl_header.= "<html>";

$tbl_header.= '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">'
. '<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">';

$tbl_header.= "<meta charset='utf-8' />";

$tbl_header.= "<body>";

foreach ($objQuery as $resultData):

$ccid = $resultData["cid"];

$tbl_header.= '<div style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $val . '</div>';

$tbl_header.= '<table width="100%" border="0">

<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["companyname"] . '</td>

</tr>
<tr>

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">' . $resultData["companytype"] . '</td>

</tr>

</table>

<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Contact(s)</td>

</tr>

</table>';

$tbl_header.= '<table width="100%" border="0">';


$where = "companycontacts.cid = '" . $ccid . "' AND companycontacts.is_active=1 ORDER BY display_order";
$dataQuery = $this->outputData['ccontact'] = $this->common->JoinTable('*', 'companycontacts', 'companies', 'companycontacts.cid=companies.cid', 'LEFT', $where);

$count = 0;

foreach ($dataQuery as $resultData3):

if ($count % 2 == 0)
$tbl_header.='<tr width="40%" >';

$tbl_header.= '

<td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">' . $resultData3["cfname"] . ' ' . $resultData3["clname"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Contact</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cfname"] . ' ' . $resultData3["clname"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Title</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["ctitle"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Address</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["caddress1"] . '</td>


</tr>';



if ($resultData3["caddress2"]) {


$tbl_header.= '<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["caddress2"] . '</td>


</tr>';
}


$tbl_header.= '<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["ccity"] . ', ' . $resultData3["cstate"] . ' ' . $resultData3["czip"] . ' ' . $resultData3["ccountry"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Phone</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cphone"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Fax</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . $resultData3["cfax"] . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Email</td>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">' . mailto($resultData3["cemail"]) . '</td>


</tr>


<tr>


<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Website</td>



<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"><a href="http://' . $resultData3["cwebsite"] . '" >' . $resultData3["cwebsite"] . '</a></td>



</tr>


</table></td>';

if ($count % 2 == 0) {
$tbl_header.= '</tr>';
}
$count++;

endforeach;

if (count($dataQuery) == 0) {
$tbl_header.= '<tr><td><table width="40%" border="0" >

<tr>


<td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">No Contact Found</td>


</tr></table></td></tr>';
}

$tbl_header.= '</table>';

/* End Result Data 3__________________________________________________  */

$tbl_header.= '<table width="100%" border="0" cellpadding="2">

<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Company Info</td>

</tr>

</table>';


$rows = '';

$nword = str_word_count($resultData["companyinfo"]);

$rows = $nword / 10;

$companyinfo = $this->common->htmlallentities($resultData["companyinfo"]);

$tbl_header.= '

<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:justify;border-collapse: collapse;">

<tr>

<td style="vertical-align:top;padding:0;border-bottom:1px solid #000;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">


<p  style="width: 100% ! important; background-color: #FFFFFFFF ! important; vertical-align:top ! important;padding:0 ! important;font-size:8pt ! important;font-family:Vectora LH 55 Roman,arial ! important;" rows="' . $rows . '" >

' . $this->common->normalize($this->common->get_formatted_string_company($companyinfo)) . '</p>';

$tbl_header.= '</td></tr>

</table>';

$tbl_header.= '


</table>      
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">


<tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">


<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">Company Location</td>


</tr>


</table>';


$tbl_header.= ' <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:left; border-collapse: collapse;">



<tr style="font-family:arial;font-size:9pt;font-weight:bold;">



<td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:9pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">No.</td>



<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:9pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Location</td>



</tr>';


$cloc = $this->common->CustomQueryALL("SELECT st1.*, st2.* FROM (SELECT `aid`, `acity`, `astate`,`aname` FROM `airports` 
    UNION SELECT `aid`, `acity`, `astate`,`aname` FROM `lkpairportlist`) st1 
    LEFT JOIN `companylocations` st2 ON `st2`.`aid` = `st1`.`aid`
    LEFT JOIN `companies` ON `companies`.`cid` = `st2`.`cid` 
    WHERE `companies`.`cid` = " . $this->db->escape($ccid) . " ORDER BY `st1`.`acity`, `st1`.`astate`");


$c = 1;

foreach ($cloc as $loc):

if ($loc['acity'] != "") {


$tbl_header.= '<tr>

<td style="width:1000px; padding:3px 20px 3px 3px;font-size:9pt;font-family:Vectora LH 55 Roman,arial;">' . $loc['acity'] . ",&nbsp;" . $loc['astate'] . "&nbsp;(" . $loc['aname'] . ")" . ' </td> </tr>';
}

$c++;
endforeach;


$tbl_header.= '</table>';

$tbl_header.= '</table></td></tr></table><br><br>';

endforeach;

$this->session->unset_userdata('listofcomp');

$ftext = "AXN's " . date('Y') . " Fact Book";

$tbl_header.= '<p style="text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-top:1px solid #000;">' . $ftext . '</p>';

$tbl_header.= "</body>";

$tbl_header.= "</html>";


echo $tbl_header;
}

/**
* 
* End Multiple Companies DOC Report
*/
}

// ENd PDF Class