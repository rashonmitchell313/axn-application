<?php
class adds extends CI_controller
{
	public $outputData="";		
	function __construct()	
	{			
		parent::__construct();
		$this->common->ThisSecureArea('admin');
		$this->outputData['topnav']='admin/include/topnav';
		$this->outputData['leftnav']='admin/include/leftnav';
		$this->outputData['breadcrumb']='admin/include/breadcrumb';
		$this->outputData['footer']='admin/include/footer';
		$this->outputData['header']='admin/include/header';
		$this->outputData['searchaction']='';
		$this->outputData['rsStates']=$this->common->GetAllRowOrderBy("states","state_code","asc");
		$this->outputData['row_rsCountries']=$this->common->GetAllRowOrderBy("countries","countries_name","asc");		
	}	
	public function index()
	{	
		if($this->input->post('aadd')!="")
		{
			$folder="";
			$cid="";
			$mid="";
			$subfolder="";
			$addid="";
			$aid="";
			//$subfolder=$this->input->post('aid');
			if($this->input->post('aid')!="")
			{
				$aid=$this->input->post('aid');
				$subfolder=$aid;
				$folder="aid";
				$addid=$aid;
			}elseif($this->input->post('cid')!="")
			{
				$cid=$this->input->post('cid');
				$subfolder=$cid;
				$folder="cid";
				$addid=$cid;
			}elseif($this->input->post('mid')!="")
			{
				$mid=$this->input->post('mid');
				$subfolder=$mid;
				$folder="mid";
				$addid=$mid;
			}
			//$aid=$this->input->post('aid');
			$filename=time().".jpg";
			$modifiedby=$this->common->GetCurrentUserInfo('user_id');
			$lastmodified=date("Y-m-d");
			$adlocation=$this->input->post('adlocation');
			$addfor=$this->input->post('addfor');
			$adlink=$this->input->post('adlink');
			$addfile=$this->input->post('addfile');	
			$lastmodifie=date("Y-m-d");								
			$imgpath="./ads/".$folder."/".$subfolder."/";			
			$newfile=0;			
			if($_FILES['addfile']['tmp_name']!="") 
			{						
				if(!is_dir('./ads/'.$folder.'/'.$subfolder))
				{
					mkdir('./ads/'.$folder.'/'.$subfolder,0777);
				} 
				$config['upload_path'] = $imgpath;
				$config['file_name']=	$filename;
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '10000';
				$config['max_width']  = '10240';
				$config['max_height']  = '7680';					
				$this->load->library('upload', $config);
				if(!$this->upload->do_upload('addfile'))
				{
					$error = array('error' => $this->upload->display_errors());					
					$this->common->setmessage($error['error'],"-1");													
					$newfile=0;											
				} else 
				{
					$newfile=1;	
				}
			}
			$checkold=array($folder=>$addid,'adlocation'=>$adlocation);			
			if($this->common->GetTotalFromTableWhere('advertising',$checkold)>0)
			{ 
				$oldinfo=$this->common->GetSingleRowFromTableWhere('advertising',$checkold);				
				$dataarray=array($folder=>$addid,'adlocation'=>$adlocation);								
				if($newfile==1){				
						$dataarray=array('adlink'=>$adlink,'imglink'=>$imgpath.$filename,'lastmodified'=>$lastmodifie,'modifiedby'=>$modifiedby);												
						$this->common->UpdateRecord('advertising',$checkold,$dataarray);			
					} else {
				$this->common->UpdateRecord('advertising',$checkold,
				array('adlink'=>$adlink,'lastmodified'=>$lastmodifie,'modifiedby'=>$modifiedby));				
					}
			} else 
			{
				$dataarray=array('cid'=>$cid,'aid'=>$aid,'mid'=>$mid,'imglink'=>$imgpath.$filename,'adlink'=>$adlink,'submittedby'=>$modifiedby,'adlocation'=>$adlocation);									
				$this->common->InsertInDb('advertising',$dataarray);				
				$this->common->setmessage('Ads Information has been update',1);
			}
			
		}	
		if($this->input->post('grab')!="")
		{			
			$this->outputData['thisairporinfo']=$this->common->GetSingleRowFromTableWhere('airports',array('aid'=>$this->input->post('grab')));
			$this->outputData['thisairportadd']=$this->common->JoinTable('*','advertising_mid','advertising','advertising.mid=advertising_mid.mid','LEFT',array('advertising.mid'=>$this->input->post('grab')));
		}
		if($this->input->post('cid')!="")
		{			
			$this->outputData['thiscompanyinfo']=$this->common->GetSingleRowFromTableWhere('companies',array('cid'=>$this->input->post('cid')));			
		}
		if($this->input->post('mid')!="")
		{			
			$this->outputData['thismiscinfo']=$this->common->GetSingleRowFromTableWhere('advertising_mid',array('admid'=>$this->input->post('mid')));			
		}
		$this->outputData['addloclist']=$this->common->GetAllRowFromTabl('adds_locations_type');
		$this->outputData['advertising_mid']=$this->common->GetAllRowFromTabl('advertising_mid');
		$this->outputData['airportlist']=$this->common->GetAllRowOrderBy('airports','IATA','ASC');		
		$this->outputData['misclist']=$this->common->GetAllRowOrderBy('advertising_mid','pagename','ASC');
		$this->outputData['companylist']=$this->common->GetAllRowOrderBy('companies','companyname','ASC');
		$this->outputData['misc']=$this->common->GetAllRowOrderBy('advertising_mid','mid','ASC');
		$this->load->view("admin/adds/listofadds",$this->outputData);
	}

public function delete_add($aid,$location){
    $result=$this->common->GetSingleRowFromTableWhere('advertising',array('aid'=>$aid,'adlocation'=>$location));			
    $img_link=$result['imglink'];
    $str = substr($img_link, 2);
     
     if($img_link!=""){
       if(file_exists($img_link)){
       unlink($str);
       }  
       }
     
    $this->db->query('DELETE FROM `advertising` WHERE `aid`='.$aid.' and `adlocation`="'.$location.'" ');
     $this->common->setmessage('Image has been deleted successfully.',1); 
     redirect('admin/adds');
    }


       public function company_add_delete($cid,$location){
   	$result=$this->common->GetSingleRowFromTableWhere('advertising',array('cid'=>$cid,'adlocation'=>$location));
   	$img_link=$result['imglink'];
   	$str = substr($img_link, 2);
   	  if($img_link!=""){
      if(file_exists($img_link)){
      unlink($str);
      }  
      }
     $this->db->query('DELETE FROM `advertising` WHERE `cid`='.$cid.' and `adlocation`="'.$location.'" ');
     $this->common->setmessage('Company ad has been deleted successfully.',1); 
     redirect('admin/adds');
   }


public function misc_add_delete($mid,$location){
   	$result=$this->common->GetSingleRowFromTableWhere('advertising',array('mid'=>$mid,'adlocation'=>$location));
   	$img_link=$result['imglink'];
   	$str = substr($img_link, 2);

   	  if($img_link!=""){
      if(file_exists($str)){
      unlink($str);
      }  
      }
     $this->db->query('DELETE FROM `advertising` WHERE `mid`='.$mid.' and `adlocation`="'.$location.'" ');
     $this->common->setmessage('Misc ad has been deleted successfully.',1); 
     redirect('admin/adds');
   }
   

}

?>