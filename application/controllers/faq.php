<?php

class faq extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->common->ThisSecureArea('admin');
        $this->outputData['topnav'] = 'admin/include/topnav';
        $this->outputData['leftnav'] = 'admin/include/leftnav';
        $this->outputData['breadcrumb'] = 'admin/include/breadcrumb';
        $this->outputData['footer'] = 'admin/include/footer';
        $this->outputData['header'] = 'admin/include/header';
        $this->lang->load('config', 'english');
        //error_reporting(E_ERROR);		
    }

    /**
     * 
     * Start Create FAQ Admin
     */
    public function create_faq() {
        $question = htmlentities($this->input->post('question'), ENT_DISALLOWED, 'UTF-8');
        $answer = htmlentities($this->input->post('answer'), ENT_DISALLOWED, 'UTF-8');
        $faq_id = $this->input->post('faq_id');
        $category = $this->input->post('category');
        $scategory = $this->input->post('scategory');
        $check = $this->common->GetAllRowWithColumn('tbl_faq_category', 'category_name', 'category_id > 0', 'category_name', 'ASC');
        $faq_is_active = ($this->input->post('is_active') == "on") ? 1 : 0;
        $cr_time = $this->common->get_created_time();
        if ($faq_id != NULL) {
            foreach ($check as $c)
                if ($category == $c['category_name']) {
                    $this->common->setmessage("Category already exist", -1);
                    redirect(site_url('admin/view-faq'));
                    exit();
                }
            $data_array = array('faq_question' => $question, 'faq_answer' => $answer, 'category_id' => $scategory, 'faq_is_active' => $faq_is_active);
            $this->common->update_record('tbl_faq', array('faq_id' => $faq_id), $data_array);
            $this->common->getactivity('edit_faq','');
            $this->common->setmessage("FAQ has been Updated successfully", 1);
            redirect(site_url('admin/view-faq'));
            exit();
        }
        if ($question != NULL && $answer != NULL) {
            if ($category != "") {
                foreach ($check as $c)
                    if ($category == $c['category_name']) {
                        $this->common->setmessage("Category already exist", -1);
                        redirect(site_url('admin/view-faq'));
                        exit();
                    }

                $newarray = array('category_name' => $category, 'is_active' => 1, 'created_date' => $cr_time);
                $insert = $this->common->InsertInDb('tbl_faq_category', $newarray);
                $this->common->getactivity('insert_faq_category','');
                $scategory = $insert;
            }
            $data_array = array('category_id' => $scategory, 'faq_question' => $question, 'faq_answer' => $answer, 'faq_is_active' => $faq_is_active, 'faq_created_date' => $cr_time);
            $insert_id = $this->common->insert_in_db_with_exist_return("tbl_faq", $data_array, 'faq_id', 'faq_created_date');
            $this->common->getactivity('insert_faq','');
            $this->common->setmessage("FAQ has been Added successfully", 1);
            redirect(site_url('admin/view-faq'));
            exit();
        }
        $this->outputData['faqcategory'] = $this->common->get_all_row_from_table('tbl_faq_category', 'is_active = 1');
        $this->load->view('admin/faq/create_faq', $this->outputData);
    }
    /**
     * 
     * End Create FAQ Admin
     */

    /**
     * 
     * Start View FAQs Admin
     */
    public function view_faq() {
        $this->outputData['faqs'] = $this->common->get_all_row_from_table('tbl_faq');
        $this->load->view('admin/faq/view_faq', $this->outputData);
    }
    /**
     * 
     * End View FAQs Admin
     */

    /**
     * 
     * Start Edit FAQ Admin
     */
    public function edit_faq() {
        $faq_id = (int) $this->uri->segment(3);
        $this->outputData['edit_faq'] = $this->common->JoinTable('tbl_faq.*, tbl_faq_category.category_name, tbl_faq_category.category_id', 'tbl_faq', 'tbl_faq_category', 'tbl_faq.category_id=tbl_faq_category.category_id', 'LEFT', array('faq_id' => $faq_id));
        $this->outputData['faqcategory'] = $this->common->get_all_row_from_table('tbl_faq_category');
        $this->load->view('admin/faq/create_faq', $this->outputData);
    }
    /**
     * 
     * End Edit FAQ Admin
     */

}

?>