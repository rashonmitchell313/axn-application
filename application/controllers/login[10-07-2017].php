<?php

class login extends CI_Controller {

    public $outputData = "";

    function __construct() {
        parent::__construct();
        $this->outputData['menu'] = 'frontend/include/menu';
        $this->outputData['footer'] = 'frontend/include/footer';
        $this->outputData['header'] = 'frontend/include/header';
        $this->outputData['searchaction'] = '';
    }

    /**
     * 
     * START User Logout (Required Login)
     */
    function logout() {


        $oldurl = $this->input->post('last_url');
        $str12 = explode("/", $oldurl);
        $url12 = array_unique($str12);
        $last_url1 = implode("/", $url12);

        $userid = $this->common->GetTotalCount('tbl_user_last_state', array('user_id' => $this->session->userdata('user_id')));

        if ($userid != 0) {
            $this->common->UpdateRecord('tbl_user_last_state', array('user_id' => $this->session->userdata('user_id')), array('last_state_url' => $last_url1));
        } else {
            $this->common->InsertInDb('tbl_user_last_state', array('user_id' => $this->session->userdata('user_id'), 'last_state_url' => $last_url1));
        }

        $this->common->UpdateRecord('logging', array('user_id' => $this->session->userdata('user_id'), 'lid' => $this->session->userdata('lid')), array('logout' => $this->common->GetCreatedTime()));
        $this->session->unset_userdata(array('lid' => '', 'user_id' => '', 'is_admin' => '', 'first_name' => '', 'last_name' => '', 'email' => '', 'year' => '', 'user_status' => 'away', 'accesslevel' => '', 'affiliated' => ''));
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
        exit();
    }

    /**
     * 
     * END User Logout
     */

    /**
     * 
     * START User Login (Required Email & Password)
     */
    function index() {
        $oldurl = $this->input->post('return_url');
        $str12 = explode("/", $oldurl);
        $url12 = array_unique($str12);
        $return_url1 = implode("/", $url12);

        $return_url = $return_url1;

        if (!empty($_POST)) {
            if ($this->input->post('username') != "" && md5($this->input->post('userpassword')) != "") {

                $userinfo = $this->common->
                        GetSingleRowFromTableWhere('users', array('email' => $this->input->post('username'), 'password' => md5($this->input->post('userpassword')),
                    'is_active' => 1));

                if (count($userinfo) > 0) {
                    $this->common->EmptySessionIndex('user_status');
                    $this->common->EmptySessionIndex('login_time_out');

                    $lid = $this->common->InsertInDb('logging', array('user_id' => $userinfo['user_id'], 'login' => $this->common->GetCreatedTime()));
                    $affiliated = $this->common->JoinTable("*", "users", "affiliation", "users.user_id = affiliation.user_id", "INNER", array('users.user_id' => $userinfo['user_id']));

                    $re_logintime = $this->common->GetSingleRowWithColumn('tbl_setting', 'setting_id = 1    ', 're_login_time');
                    $this->common->SetSessionKey(array('login_time_out' => $re_logintime['re_login_time']));

                    // SET ACCESS.

                    $accesslevel = $userinfo['accesslevel'];

                    $join_array = array(
                        array('tbl_access_config', 'tbl_access_config.id = tbl_access_level_config.config_id', 'LEFT'),
                        array('accesslevels', 'accesslevels.levelid = tbl_access_level_config.levelid', 'LEFT')
                    );

                    $access_config = $this->common->JoinTables('tbl_access_config.access_name', 'tbl_access_level_config', $join_array, array('accesslevels.levelid' => $accesslevel, 'tbl_access_level_config.is_active' => 1));

                    $access_list = array();
                    foreach ($access_config as $acc) {
                        $access_list[] = $acc['access_name'];
                    }
                    $access_list = implode(", ", $access_list);

                    $this->common->SetSessionKey(array('user_access' => $access_list));

//                    GetSingleColumnFromTableWhere($table, $where = "", $column)
                    $user_state = '';
                    $user_state = $this->common->GetSingleColumnFromTableWhere('tbl_user_last_state', array('user_id' => $userinfo['user_id']), 'last_state_url');

//                    print_r($user_state);
//                    echo $user_state;
//                    exit;
                    if (count($affiliated) > 0) {
                        $affiliated = 1;
                    } else {
                        $affiliated = 0;
                    }

                    if ($userinfo['year'] != "") {
                        $year = explode(",", $userinfo['year']);
                        $SesseionArray = array('affiliated' => $affiliated, 'user_id' => $userinfo['user_id'], 'accesslevel' => $userinfo['accesslevel'], 'first_name' => $userinfo['first_name'], 'last_name' => $userinfo['last_name'], 'email' => $userinfo['email'], 'lid' => $lid, 'year' => $year);
                    } else if ($userinfo['year'] == "") {
                        $SesseionArray = array('affiliated' => $affiliated, 'user_id' => $userinfo['user_id'], 'accesslevel' => $userinfo['accesslevel'], 'first_name' => $userinfo['first_name'], 'last_name' => $userinfo['last_name'], 'email' => $userinfo['email'], 'lid' => $lid);
                    }

                    if (isset($userinfo['accesslevel'])) {

//                        $this->common->SetSessionKey(array('login_time_out' => $re_logintime));
                        $this->session->set_userdata($SesseionArray);
                        if (($userinfo['accesslevel']) == 4) {
                            if ($return_url != '') {
                                redirect($return_url);
                            } else {

                                if ($user_state == '' || $user_state == '-1') {
                                    redirect(base_url());
                                    exit();
                                } else {
                                    redirect($user_state);
                                }
                            }
                        } else {

                            if ($user_state == '' || $user_state == '-1') {
                                redirect(base_url());
                                exit();
                            } else {
                                redirect($user_state);
                            }
                        }
                    } else if (!isset($userinfo['accesslevel'])) {

                        $this->outputData['userinfo'] = $this->common->GetSingleRowFromTableWhere('users', array('email' => $this->input->post('username'), 'password' => md5($this->input->post('userpassword')), 'is_active' => 1));
                        $this->outputData['email'] = $this->input->post('username');
                        if ($return_url != '') {
                            redirect($return_url);
                        } else {
                            $this->load->view("frontend/unsubsribeuser", $this->outputData);
                        }
                    }
                } else {
                    $this->common->setmessage('Username or Password invalid.', -1);
                }
            } else {
                $this->common->setmessage('Username or Password invalid.', -1);
            }
        }
        $this->outputData['content'] = $this->common->GetContents('login_content', 'text_home');
        if ($return_url != '') {
            redirect($return_url);
        } else {
            $this->load->view("frontend/login", $this->outputData);
        }
    }

    /**
     * 
     * END User Login
     */

    /**
     * 
     * START Set Session (Required Id)
     */
    public function user_away() {
        $this->common->SetSessionKey(array('user_status' => 'away'));

        if ($this->common->GetSessionKey('user_id') > 0) {

            echo 1;
        } else {
            echo 0;
        }
//        if ($this->common->GetSessionKey('user_status') == "away") {
//            $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
//            $this->output->set_header("Pragma: no-cache");
////            $this->cache->clean();
////            $this->output->clear_page_cache($this->common->GetSessionKey('last_url'));
//        }
        //         if ( $this->common->GetSessionKey('user_status') == "away") {
//             $this->common->EmptySessionIndex('user_status');
//                         echo 'hello';exit();
//         print_r($this->session->userdata(); exit;
//            print_r($this->session->userdata('user_status')); 
//            echo 'Abc...';
    }

    /**
     * 
     * END Set Session
     */
    public function user_test() {

        echo current_url();
        exit;
    }

    /**
     * 
     * START Un-Sub-Scriber User (Required Email)
     */
    public function unsubsribeuser() {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        $des = $this->input->post('detail');

        $link = 'email_unsubsribeuser';
        $where1 = array('email_temp_name' => $link);

        $email_template = $this->common->GetSingleRowFromTableWhere('tbl_email_temp', $where1);

        $search_array = array('#first_name', '#last_name', '#email', '#descrip', '#siteurl');

        $replace_array = array($fname, $lname, $email, $des);

        $message1 = $email_template['email_temp_text'];

        $message = str_replace($search_array, $replace_array, $message1);

        $this->load->library('email');

        $config['smtp_host'] = 'mail.arnfactbook.com';
        $config['smtp_user'] = 'support@arnfactbook.com';
        $config['smtp_pass'] = 'Urbanexpo1';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        $this->email->from('support@arnfactbook.com', 'Arnfactbook Support');
        $this->email->to('shawn@airportrevenuenews.com');
        $this->email->subject('Request of Subscribtion| Arnfactbook');

        $data = array(
            'message' => $message
        );

        $messagee = $this->email->message($this->load->view('frontend/email_temp', $data, true));

        $this->email->send();

        if ($this->email->send()) {
            echo "<div class='alert alert-success'>Request sent to administrator.</div>";
        } else {
            echo "<div class='alert alert-danger'>There was an error while sending email. Please try again later.</div>";
        }

        $this->common->setmessage($result, 1);
        redirect('login');
    }

    /**
     * 
     * END Un-Sub-Scriber User
     */
}
