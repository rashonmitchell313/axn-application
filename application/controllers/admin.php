<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin extends CI_Controller {

    public $outputData = "";

    function __construct() {

        parent::__construct();

        $this->common->ThisSecureArea('admin');

        $this->outputData['topnav'] = 'admin/include/topnav';

        $this->outputData['leftnav'] = 'admin/include/leftnav';

        $this->outputData['breadcrumb'] = 'admin/include/breadcrumb';

        $this->outputData['footer'] = 'admin/include/footer';

        $this->outputData['header'] = 'admin/include/header';

        $this->outputData['searchaction'] = '';

        $this->outputData['rsStates'] = $this->common->GetAllRowOrderBy("states", "state_code", "asc");

        $this->outputData['row_rsCountries'] = $this->common->GetAllRowOrderBy("countries", "countries_name", "asc");

        $this->outputData['AccessLevel'] = $this->common->GetAllRowOrderBy("accesslevels", "id", "asc");

        $this->lang->load('config', 'english');
    }

    /**
     * 
     * START Error Log Setting (Required Id)
     */
    public function error_log_setting() {

        if ($this->input->post('settingupdate') != "") {
            if ($this->input->post('isactive') != "") {

                $active = 1;
            } else {

                $active = 0;
            }

            $dataarray = array('error_log_setting_is_active' => $active, 'error_receive_email_subject' => $this->input->post('emailsubject'), 'error_attention_to' => $this->input->post('sendto'), 'error_attention_to_cc' => $this->input->post('sendtocc'), 'error_attention_to_bcc' => $this->input->post('sendtobcc'));

            $this->common->UpdateRecord('tbl_error_log_setting', array('error_log_setting_id' => $this->input->post('settingid')), $dataarray);

            $this->common->setmessage('Information has been update successfully', 1);
        }

        $this->outputData['logsetting'] = $this->common->GetAllRowOrderBy('tbl_error_log_setting', 'error_log_setting_id', 'ASC');

        $this->load->view('admin/error_log_setting', $this->outputData);
    }

    /**
     * 
     * END Error Log Setting
     */

    /**
     * 
     * START List of Errors Log (Required Id)
     */
    public function errors_log() {

        if ($this->input->post('changestatus') == 'changestatus' && $this->input->post('error_log_id') > 0) {

            $this->common->UpdateRecord('tbl_error_log_history', array('error_log_id' => $this->input->post('error_log_id')), array('error_log_status' => $this->input->post('errorlogstatus')));

            $this->common->setmessage('Status Update successfully.', 1);
        }

        $this->outputData['listoflogs'] = $this->common->GetAllRowjoinOrderBy('tbl_error_log_history', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'inner', 'tbl_error_log_history.error_log_id', 'ASC');

        $this->outputData['listoflogtype'] = $this->common->GetAllRowOrderBy('support_ticket_type', 'support_ticket_type_id', 'DESC');

        if ($this->uri->segment(3) > 0) {

            $this->outputData['listoflogs'] = $this->common->GetSingleRowFromTableJoinWhere("tbl_error_log_history", 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'LEFT', array('error_log_id' => $this->uri->segment(3)));

            if (count($this->outputData['listoflogs']) > 0) {

                $this->load->view('admin/error_log', $this->outputData);
            } else {

                $this->common->redirect(site_url('admin/errors_log'));
            }
        } else {

            $this->load->view('admin/error_log', $this->outputData);
        }
    }

    /**
     * 
     * END List of Errors Log
     */

    /**
     * 
     * START List of Deleted Errors Log (Required Id)
     */
    public function del_errors_log() {

        if ($this->input->post('changestatus') == 'changestatus' && $this->input->post('error_log_id') > 0) {

            $this->common->UpdateRecord('tbl_error_log_history', array('error_log_id' => $this->input->post('error_log_id')), array('error_log_status' => $this->input->post('errorlogstatus')));

            $this->common->setmessage('Status Update successfully.', 1);
        }

        $this->outputData['listoflogs'] = $this->common->GetAllRowjoinOrderBy('tbl_error_log_history', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'inner', 'tbl_error_log_history.error_log_id', 'ASC');

        $this->outputData['listoflogtype'] = $this->common->GetAllRowOrderBy('support_ticket_type', 'support_ticket_type_id', 'DESC');

        if ($this->uri->segment(3) > 0) {

            $this->outputData['listoflogs'] = $this->common->GetSingleRowFromTableJoinWhere("tbl_error_log_history", 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'LEFT', array('error_log_id' => $this->uri->segment(3)));

            if (count($this->outputData['listoflogs']) > 0) {

                $this->load->view('admin/del_error_log', $this->outputData);
            } else {

                $this->common->redirect(site_url('admin/del_errors_log'));
            }
        } else {

            $this->load->view('admin/del_error_log', $this->outputData);
        }
    }

    /**
     * 
     * END List of Deleted Errors Log
     */

    /**
     * 
     * START Multi Language (Required Id)
     */
    public function multi_language() {

        $this->outputData['text_multilang'] = $this->common->JoinTable("*", 'text_multilang', 'text_language', 'text_multilang.language=text_language.text_language_code', 'LEFT', array());

        $this->outputData['text_language'] = $this->common->GetAllRowOrderBy('text_language', 'text_language_id', 'ASC');

        $this->load->view('admin/multilanguage', $this->outputData);
    }

    /**
     * 
     * END Multi Language
     */

    /**
     * 
     * START View Content (Required Id)
     */
    public function edit_content() {

        $this->outputData['page_content'] = $this->common->GetAllRowFromTabl('text_home');

        $this->outputData['text_language'] = $this->common->GetAllRowOrderBy('text_language', 'text_language_id', 'ASC');

        $this->load->view('admin/editcontent', $this->outputData);
    }

    /**
     * 
     * END View Content
     */

    /**
     * 
     * START Edit Content (Required Id)
     */
    public function edit_content_text() {

        $home_content = $this->input->post('home_content');

        $home_content2 = $this->input->post('home_content2');

        $login_content = $this->input->post('login_content');

        if ($home_content != "" && $home_content2 != "" && $login_content != "") {

            $newdataarray = array('login_content' => $login_content, 'home_content' => $home_content, 'home_content2' => $home_content2);

            $this->common->UpdateRecord('text_home', array('text_home_id >' => 0), $newdataarray);

            $this->common->setmessage('Content has been convert & save Successfully.', 1);
        }

        $this->outputData['page_content'] = $this->common->GetAllRowFromTabl('text_home');

        $this->load->view('admin/edit_content_text', $this->outputData);
    }

    /**
     * 
     * END Edit Content
     */

    /**
     * 
     * START View Setting
     */
    public function setting() {

        $this->outputData['AccessLevel'] = $this->common->GetAllRowOrderBy("accesslevels", "id", "asc");

        $this->outputData['access_config'] = $this->common->GetAllRowFromTableWhere('tbl_access_config', array('is_active' => 1));

        $this->load->view('admin/setting', $this->outputData);
    }

    /**
     * 
     * END View Setting
     */

    /**
     * 
     * START List of User's All Logged In Report (Required Id)
     */
    public function logged_in_Report() {

        $this->outputData['searchaction'] = site_url('admin/logged_in_report');

        $select = "logging.*,users.first_name,users.last_name";

        $from = "logging";

        $jointbl = "users";

        $jointype = "LEFT";

        $joinon = "logging.user_id=users.user_id";

        $orderby = "last_name ASC";

        $limit = $this->uri->segment(3);

        $url = explode("/", current_url());

        if (in_array('searchclear', $url)) {

            $this->common->EmptySessionIndex('loggedinsearch');
        }

        if (isset($_REQUEST['search']) && $_REQUEST['search'] != "" && $_REQUEST['search'] == 'clear') {

            $this->common->EmptySessionIndex('loggedinsearch');
        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('loggedinsearch' => $this->input->post('search')));
        }

        if ($this->common->GetSessionKey('loggedinsearch') != "") {

            $SearchKey = $this->common->GetSessionKey('loggedinsearch');

            $this->common->SetSessionKey(array('loggedinsearch' => $SearchKey));

            $like = array('users.last_name' => $SearchKey);

            $orlike = array('users.first_name' => $SearchKey);

            $this->outputData['total'] = $this->common->JoinTableQueryLikeCount($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);

            $this->outputData['logging'] = $this->common->JoinTableQueryLike($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCount('logging');

            $this->outputData['logging'] = $this->common->JoinTableQueryOrderBy("logging.*,users.first_name,users.last_name", "logging", "users", "logging.user_id=users.user_id", "lid", "DESC", $this->uri->segment(3), "");
        }

        $this->load->view("admin/users/logging", $this->outputData);
    }

    /**
     * 
     * END List of User's All Logged In Report
     */

    /**
     * 
     * START List of User's Delted Logged In Report (Required Id)
     */
    public function delete_logged_Report() {

        $this->outputData['searchaction'] = site_url('admin/delete_logged_report');

        $select = "logging.*,users.first_name,users.last_name";

        $from = "logging";

        $jointbl = "users";

        $jointype = "LEFT";

        $joinon = "logging.user_id=users.user_id";

        $orderby = "last_name ASC";

        $limit = $this->uri->segment(3);

        $url = explode("/", current_url());

        if (in_array('searchclear', $url)) {

            $this->common->EmptySessionIndex('loggedinsearch');
        }

        if (isset($_REQUEST['search']) && $_REQUEST['search'] != "" && $_REQUEST['search'] == 'clear') {

            $this->common->EmptySessionIndex('loggedinsearch');
        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('loggedinsearch' => $this->input->post('search')));
        }

        if ($this->common->GetSessionKey('loggedinsearch') != "") {

            $SearchKey = $this->common->GetSessionKey('loggedinsearch');

            $this->common->SetSessionKey(array('loggedinsearch' => $SearchKey));

            $like = array('users.last_name' => $SearchKey);

            $orlike = array('users.first_name' => $SearchKey);

            $this->outputData['total'] = $this->common->JoinTableQueryLikeCount($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);

            $this->outputData['logging'] = $this->common->JoinTableQueryLike($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCount('logging');

            $this->outputData['logging'] = $this->common->JoinTableQueryOrderBy("logging.*,users.first_name,users.last_name", "logging", "users", "logging.user_id=users.user_id", "lid", "DESC", $this->uri->segment(3), "");
        }

        $this->load->view("admin/users/del_logging", $this->outputData);
    }

    /**
     * 
     * END List of User's Delted Logged In Report
     */

    /**
     * 
     * START Admin's Dashboard
     */
    public function index() {
          if ($_SERVER['REMOTE_ADDR'] == '202.141.226.196') {

                    ?>
                    <pre>
                        <?php
                        // print_r($this->outputData);
                        // echo "NOIM";
                        // exit;
                        ?>
                    </pre>
                    <?PHP
                }
        $sql = "SELECT count(*) as `total_number_of_login`,CONCAT(`users`.`first_name`,' ',`users`.`last_name`) AS `Username` FROM `logging` LEFT JOIN users ON `logging`.`user_id`=`users`.`user_id` GROUP BY `logging`.`user_id` ORDER BY `total_number_of_login` DESC limit 10";

        $this->outputData['LoggingHistory'] = $this->common->CustomQueryALL($sql);

        $this->outputData['TotalUsers'] = $this->common->GetRecordCount('users');

        $this->outputData['TotalUsersAdmin'] = $this->common->GetTotalCount('users', array('accesslevel' => 4));

        $this->outputData['TotalUsersReadOnly'] = $this->common->GetTotalCount('users', 'accesslevel = 1');

        $this->outputData['TotalUsersModifyCompany'] = $this->common->GetTotalCount('users', 'accesslevel = 0');

        $this->outputData['TotalUsersModifyAirport'] = $this->common->GetTotalCount('users', 'accesslevel = 0');

        $this->outputData['Subscribers'] = $this->common->GetTotalCount('users', 'fbosubscription = 1	');

        $totalAirport = $this->common->GetRecordCount('airports');

        $totalpublished = $this->common->GetTotalCount('airports', 'published > "2016-03-17 00:00:00" ORDER BY IATA');

        $totalapproved = $this->common->GetTotalCount('airports', 'approved > "2016-03-17 00:00:00" ORDER BY IATA');

        $totalmodified = $this->common->GetTotalCount('airports', 'lastmodified > "2016-03-17 00:00:00" ORDER BY IATA');

        $companycompersion = $this->common->CustomQueryALL('SELECT `companytype`,count(*) as `total` FROM `companies` group by `companytype`');

        $this->outputData['companycompersion'] = $companycompersion;

        $this->outputData['total_airport'] = $totalAirport;

        $this->outputData['totalpublished'] = $totalpublished;

        $this->outputData['totalapproved'] = $totalapproved;

        $this->outputData['totalmodified'] = $totalmodified;

        $this->outputData['num_rowpub'] = number_format(($totalpublished / $totalAirport) * 100, 2);

        $this->outputData['num_rowapp'] = number_format(($totalapproved / $totalAirport) * 100, 2) + 1;

        $this->outputData['num_rowmod'] = number_format(($totalmodified / $totalAirport) * 100, 2);

        $this->outputData['total_companies'] = $this->common->GetRecordCount('companies');

        $this->outputData['companies_pub'] = $this->common->GetTotalCount('companies', 'published > "2016-03-17 00:00:00"');

        $this->outputData['companies_app'] = $this->common->GetTotalCount('companies', 'approved > "2016-03-17 00:00:00"');

        $this->outputData['companies_mod'] = $this->common->GetTotalCount('companies', 'lastmodified > "2016-03-17 00:00:00"');

        $this->load->view("admin/home", $this->outputData);
    }

    /**
     * 
     * END Admin's Dashboard
     */

    /**
     * 
     * START User's Subscription Status (Required Id)
     */
    public function list_of_subscribers() {

        $this->outputData['listofuser'] = $this->common->JoinTableQuery("users.*,states.state_name", "users", "states", "states.states_id=users.state", "last_name asc", $this->uri->segment(3), "");

        $this->outputData['total'] = $this->common->JoinTableQuery("users.*,states.state_name", "users", "states", "states.states_id=users.state", "last_name asc", "", true);

        $this->load->view("admin/users/listofusers", $this->outputData);
    }

    /**
     * 
     * END User's Subscription Status
     */

    /**
     * 
     * START List of All Users (Required Id)
     */
    public function list_of_users() {

        if (isset($_REQUEST['search']) && $_REQUEST['search'] != "" && $_REQUEST['search'] == 'clear') {

            $this->common->EmptySessionIndex('usersearch');
        }

        $this->outputData['searchaction'] = site_url('admin/list_of_users');

        $select = "users.*,states.state_name";

        $from = "users";

        $jointbl = "states";

        $jointype = "LEFT";

        $joinon = "states.states_id=users.state";

        $orderby = "last_name ASC";

        $limit = $this->uri->segment(3);

        $url = explode("/", current_url());

        if (in_array('searchclear', $url)) {

            $this->common->EmptySessionIndex('usersearch');
        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('usersearch' => $this->input->post('search')));

            $like = array('last_name' => $this->common->GetSessionKey('usersearch'));

            $orlike = array('first_name' => $this->common->GetSessionKey('usersearch'));

            $this->outputData['listofuser'] = $this->common->
                    JoinTableQueryLike($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);

            $this->outputData['total'] = $this->common->
                    JoinTableQueryLikeCount($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);
        } elseif ($this->common->GetSessionKey('usersearch') != "") {

            $like = array('last_name' => $this->common->GetSessionKey('usersearch'));

            $orlike = array('first_name' => $this->common->GetSessionKey('usersearch'));

            $this->outputData['listofuser'] = $this->common->JoinTableQueryLike($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);

            $this->outputData['total'] = $this->common->JoinTableQueryLikeCount($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);
        } else {

            $this->outputData['listofuser'] = $this->common->JoinTableQuery($select, $from, $jointbl, $joinon, $orderby, $limit, '');

            $this->outputData['total'] = $this->common->JoinTableQuery("users.*,states.state_name", "users", "states", "states.states_id=users.state", "last_name asc", "", true);
        }

        $this->load->view("admin/users/listofusers", $this->outputData);
    }

    /**
     * 
     * END List of All Users
     */

    /**
     * 
     * START List of All Users (Required Id)
     */
    public function list_of_user_activities() {

        $this->load->view("admin/users/listofusers_activities", $this->outputData);
    }

    /**
     * 
     * END List of All Users
     */

    /**
     * 
     * START List of Deleted Users (Required Id)
     */
    public function list_of_del_users() {

        if (isset($_REQUEST['search']) && $_REQUEST['search'] != "" && $_REQUEST['search'] == 'clear') {

            $this->common->EmptySessionIndex('usersearch');
        }

        $this->outputData['searchaction'] = site_url('admin/list_of_del_users');

        $select = "users.*,states.state_name";

        $from = "users";

        $jointbl = "states";

        $jointype = "LEFT";

        $joinon = "states.states_id=users.state";

        $orderby = "last_name ASC";

        $limit = $this->uri->segment(3);

        $url = explode("/", current_url());

        if (in_array('searchclear', $url)) {

            $this->common->EmptySessionIndex('usersearch');
        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('usersearch' => $this->input->post('search')));

            $like = array('last_name' => $this->common->GetSessionKey('usersearch'));

            $orlike = array('first_name' => $this->common->GetSessionKey('usersearch'));

            $this->outputData['listofuser'] = $this->common->
                    JoinTableQueryLike($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);

            $this->outputData['total'] = $this->common->
                    JoinTableQueryLikeCount($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);
        } elseif ($this->common->GetSessionKey('usersearch') != "") {

            $like = array('last_name' => $this->common->GetSessionKey('usersearch'));

            $orlike = array('first_name' => $this->common->GetSessionKey('usersearch'));

            $this->outputData['listofuser'] = $this->common->JoinTableQueryLike($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);

            $this->outputData['total'] = $this->common->JoinTableQueryLikeCount($select, $from, $jointbl, $joinon, $jointype, $orderby, $limit, $like, $orlike);
        } else {

            $this->outputData['listofuser'] = $this->common->JoinTableQuery($select, $from, $jointbl, $joinon, $orderby, $limit, '');

            $this->outputData['total'] = $this->common->JoinTableQuery("users.*,states.state_name", "users", "states", "states.states_id=users.state", "last_name asc", "", true);
        }

        $this->load->view("admin/users/deleted_users", $this->outputData);
    }

    /**
     * 
     * END List of Deleted Users
     */

    /**
     * 
     * START Create User (Required User's info)
     */
    public function create_user() {

        if ($this->input->post('MM_update') != "") {

            if ($this->input->post('is_admin') != "") {

                $is_admin = 1;
            } else {

                $is_admin = 0;
            }

            if ($this->common->GetTotalCount('users', array('email' => $this->input->post('email'))) <= 0) {

                $first_name = $this->input->post('first_name');

                $year = $this->input->post('year');

                $last_name = $this->input->post('last_name');

                $company = $this->input->post('company');

                $address1 = $this->input->post('address1');

                $address2 = $this->input->post('address2');

                $city = $this->input->post('city');

                $state = $this->input->post('state');

                $country = $this->input->post('country');

                $zip = $this->input->post('zip');

                $work_phone = $this->input->post('work_phone');

                $home_phone = $this->input->post('home_phone');

                $cell_phone = $this->input->post('cell_phone');

                $fax = $this->input->post('fax');

                $email = $this->input->post('email');

                $published_email = $this->input->post('published_email');

                $name_on_badge = $this->input->post('name_on_badge');

                $golf_handicap = $this->input->post('golf_handicap');

                $job_title = $this->input->post('job_title');

                $job_category = $this->input->post('job_category');

                $emergency_name = $this->input->post('emergency_name');

                $emergency_cell = $this->input->post('emergency_cell');

                $emergency_phone = $this->input->post('emergency_phone');

                $emergency_email = $this->input->post('emergency_email');

                $password = md5($this->input->post('password'));

                $access_level = $this->input->post('access_level');

                $dataarray = array('is_admin' => $is_admin,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'company' => $company,
                    'address1' => $address1,
                    'address2' => $address2,
                    'city' => $city,
                    'state' => $state,
                    'country' => $country,
                    'zip' => $zip,
                    'work_phone' => $work_phone,
                    'work_phone_ext' => $this->input->post('work_phone_ext'),
                    'home_phone' => $home_phone,
                    'cell_phone' => $cell_phone,
                    'fax' => $fax,
                    'email' => $email,
                    'published_email' => $published_email,
                    'name_on_badge' => $name_on_badge,
                    'golf_handicap' => $golf_handicap,
                    'job_title' => $job_title,
                    'job_category' => $job_category,
                    'emergency_name' => $emergency_name,
                    'emergency_phone' => $emergency_phone,
                    'emergency_cell' => $emergency_cell,
                    'emergency_email' => $emergency_email,
                    'password' => $password,
                    'year' => $year,
                    'is_active' => $this->input->post('is_active'),
                    'accesslevel' => $access_level,
                    'created_date' => $this->common->GetCreatedTime());

                $this->load->library('form_validation');

                $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[2]|max_length[30]|xss_clean');

                $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[2]|max_length[30]|xss_clean');

                $this->form_validation->set_rules('company', 'Company', 'trim|min_length[2]|max_length[50]|xss_clean');

                $this->form_validation->set_rules('city', 'City', "trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]");

                $this->form_validation->set_rules('state', 'State', 'trim|max_length[50]|xss_clean|alpha_numeric');

                $this->form_validation->set_rules('zip', 'Zip', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numeric');

                $this->form_validation->set_rules('work_phone', 'Work Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

                $this->form_validation->set_rules('home_phone', 'Home Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

                $this->form_validation->set_rules('cell_phone', 'Cell Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

                $this->form_validation->set_rules('fax', 'Fax', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

                $this->form_validation->set_rules('address1', 'Address', 'trim|min_length[2]|xss_clean');

                $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|xss_clean|valid_email');

                $this->form_validation->set_rules('name_on_badge', 'Name On Badge', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numeric');

                $this->form_validation->set_rules('job_title', 'Job Title', 'trim|min_length[2]|max_length[50]|xss_clean');

                $this->form_validation->set_rules('emergency_name', 'Emergency Name', 'trim|min_length[2]|max_length[30]|xss_clean|alpha_numeric');

                $this->form_validation->set_rules('emergency_cell', 'Emergency Cell', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

                $this->form_validation->set_rules('emergency_phone', 'Emergency Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

                $this->form_validation->set_rules('emergency_email', 'Emergency Email', 'trim|max_length[100]|xss_clean|valid_email');

                if ($this->form_validation->run() == FALSE) {

                    $this->common->setmessage("Please enter valid data." . validation_errors(), "-1");

                    redirect(current_url());

                    exit();
                }

                $this->common->InsertInDb('users', $dataarray);

                $this->common->getactivity('insert_user', '');

                $this->common->setmessage("New User has been added successfully", 1);

                $this->common->redirect(site_url('admin/list_of_users'));
            } else {

                if ($this->common->GetTotalCount('users', array('email' => $this->input->post('email'), 'is_active' => '1')) > 0) {

                    $email_post = $this->input->post('email');
                    $this->common->setmessage("<b>" . $email_post . "</b> Email Already Exist in Active Users List. Please try with different email Address.", -1);
                } elseif ($this->common->GetTotalCount('users', array('email' => $this->input->post('email'), 'is_active' => '0')) > 0) {
                    $email_post = $this->input->post('email');
                    $this->common->setmessage("<b>" . $email_post . "</b> Email Already Exist in Deleted users list. Please try with different email Address.", -1);
                }
            }
        }



        $this->load->view('admin/users/createuser', $this->outputData);
    }

    /**
     * 
     * END Create User
     */

    /**
     * 
     * START Edit User (Required Id)
     */
    public function edit_user() {

        $id = $this->uri->segment(3);

        if ($this->input->post('MM_update') != "") {

            if ($this->input->post('is_admin') != "") {

                $is_admin = 1;
            } else {

                $is_admin = 0;
            }

            $first_name = $this->input->post('first_name');

            $year = $this->input->post('year');

            $last_name = $this->input->post('last_name');

            $company = $this->input->post('company');

            $address1 = $this->input->post('address1');

            $address2 = $this->input->post('address2');

            $city = $this->input->post('city');

            $state = $this->input->post('state');

            $country = $this->input->post('country');

            $zip = $this->input->post('zip');

            $work_phone = $this->input->post('work_phone');

            $home_phone = $this->input->post('home_phone');

            $cell_phone = $this->input->post('cell_phone');

            $fax = $this->input->post('fax');

            $email = $this->input->post('email');

            $published_email = $this->input->post('published_email');

            $name_on_badge = $this->input->post('name_on_badge');

            $golf_handicap = $this->input->post('golf_handicap');

            $job_title = $this->input->post('job_title');

            $job_category = $this->input->post('job_category');

            $emergency_name = $this->input->post('emergency_name');

            $emergency_cell = $this->input->post('emergency_cell');

            $emergency_phone = $this->input->post('emergency_phone');

            $emergency_email = $this->input->post('emergency_email');

            $access_level = $this->input->post('access_level');

            if (str_replace(" ", "", $this->input->post('password')) != "") {
                $password = md5($this->input->post('password'));
            } else {
                $password = '';
            }

            $dataarray = array('is_admin' => $is_admin,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $company,
                'address1' => $address1,
                'address2' => $address2,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'zip' => $zip,
                'work_phone' => $work_phone,
                'work_phone_ext' => $this->input->post('work_phone_ext'),
                'home_phone' => $home_phone,
                'cell_phone' => $cell_phone,
                'fax' => $fax,
                'email' => $email,
                'year' => $year,
                'published_email' => $published_email,
                'name_on_badge' => $name_on_badge,
                'golf_handicap' => $golf_handicap,
                'job_title' => $job_title,
                'job_category' => $job_category,
                'emergency_name' => $emergency_name,
                'emergency_phone' => $emergency_phone,
                'emergency_cell' => $emergency_cell,
                'emergency_email' => $emergency_email,
                'accesslevel' => $access_level,
                'is_active' => $this->input->post('is_active'));

            $this->load->library('form_validation');

            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[2]|max_length[30]|xss_clean');

            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[2]|max_length[30]|xss_clean');

            $this->form_validation->set_rules('company', 'Company', 'trim|min_length[2]|max_length[50]|xss_clean');

            $this->form_validation->set_rules('city', 'City', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('state', 'State', 'trim|max_length[50]|xss_clean|alpha_numeric');

            $this->form_validation->set_rules('zip', 'Zip', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numeric');

            $this->form_validation->set_rules('work_phone', 'Work Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('home_phone', 'Home Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('cell_phone', 'Cell Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('fax', 'Fax', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('address1', 'Address', 'trim|min_length[2]|xss_clean');

            $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|xss_clean|valid_email');

            $this->form_validation->set_rules('name_on_badge', 'Name On Badge', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numeric');

            $this->form_validation->set_rules('job_title', 'Job Title', 'trim|min_length[2]|max_length[50]|xss_clean');

            $this->form_validation->set_rules('emergency_name', 'Emergency Name', 'trim|min_length[2]|max_length[30]|xss_clean|alpha_numeric');

            $this->form_validation->set_rules('emergency_cell', 'Emergency Cell', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('emergency_phone', 'Emergency Phone', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('emergency_email', 'Emergency Email', 'trim|max_length[100]|xss_clean|valid_email');

            if ($this->form_validation->run() == FALSE) {

                $this->common->setmessage("Please enter valid data." . validation_errors(), "-1");

                redirect(current_url());

                exit();
            }

            $this->common->UpdateRecord('users', array('user_id' => $this->input->post('user_id')), $dataarray);

            if ($password != "") {
                $this->common->UpdateRecord('users', array('user_id' => $this->input->post('user_id')), array('password' => $password));
            }

            $effected = $this->db->affected_rows();
            if ($effected > 0) {

                $this->common->getactivity('edit_user', '');

                $this->common->UpdateRecord('users', array('user_id' => $this->input->post('user_id')), array('modifiedby' => $this->common->GetSessionKey('email')));
            }

            $this->common->setmessage("User information has been update successfully", 1);
        }

        $this->outputData['row_rsUser'] = $this->common->GetSingleRowFromTableWhere("users", array('user_id' => $this->uri->segment(3)));

        $colname_rsRegistrations = $this->uri->segment(3);

        $query_rsRegistrations = "SELECT r.*, c.name, c.start_date FROM registration r LEFT JOIN conference c ON c.conference_id = r.conference_id WHERE user_id = " . $this->db->escape($this->uri->segment(3)) . " ORDER BY c.start_date DESC";

        $this->outputData['query_rsRegistrations'] = $this->common->CustomQuery($query_rsRegistrations);

        $this->load->view("admin/users/edituser", $this->outputData);
    }

    /**
     * 
     * END Edit User
     */

    /**
     * 
     * START Edit FAQ's Category (Required Id)
     */
    public function edit_category() {

        $id = $this->uri->segment(3);

        $check = $this->common->GetAllRowWithColumn('tbl_faq_category', 'category_name,category_id', 'category_id > 0', 'category_name', 'ASC');

        $checkStatus = $this->common->GetSingleRowFromTableWhere('tbl_faq_category', array('category_id' => $id));

        if ($this->input->post('MM_update') != "") {

            $category_name = $this->input->post('category_name');

            foreach ($check as $c)
                if ($category_name == $c['category_name'] && $id != $c['category_id']) {

                    $this->common->setmessage("Category already exist", -1);

                    redirect(site_url('admin/all_faq_categories'));

                    exit();
                }

            $dataarray = array('category_name' => $category_name,
                'is_active' => $this->input->post('is_active'));

            $this->common->UpdateRecord('tbl_faq_category', array('category_id' => $this->input->post('category_id')), $dataarray);

            if ($this->input->post('is_active') == 1 && $checkStatus['is_active'] == 0) {
                $this->common->getactivity('restore_faq_category', '');
            } elseif ($this->input->post('is_active') == 0 && $checkStatus['is_active'] == 1) {
                $this->common->getactivity('inactive_faq_category', '');
            }
            $effected = $this->db->affected_rows();
            if ($effected > 0) {
                $this->common->getactivity('edit_faq_category', '');
            }

            $this->common->setmessage("Category information has been update successfully", 1);

            redirect(site_url('admin/all_faq_categories'));

            exit;
        }

        $this->outputData['row_rsUser'] = $this->common->GetSingleRowFromTableWhere("tbl_faq_category", array('category_id' => $this->uri->segment(3)));

        $this->load->view("admin/faq/edit_faq_category", $this->outputData);
    }

    /**
     * 
     * END Edit FAQ's Category
     */

    /**
     * 
     * START List of Support Ticket (Required Id)
     */
    public function view_tickets() {

        $url = explode("/", current_url());

        if (in_array('searchclear', $url)) {

            $this->common->EmptySessionIndex('ticketsearch');
        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('ticketsearch' => $this->input->post('search')));
        }

        if ($this->common->GetSessionKey('ticketsearch') != "") {

            $like = array('support_id' => $this->common->GetSessionKey('ticketsearch'));

            $orlike = array('first_name' => $this->common->GetSessionKey('ticketsearch'));

            $this->outputData['total'] = $this->common->GetRecordCountLikeWhere2('support', 'users', 'support.user_id = users.user_id', 'left', $like, $orlike, 'support.user_id > 0');

            $this->outputData['listoftickets'] = $this->common->
                    GetAllRowFromTableLimitOrderByLikeWhere1('*', 'support', 'users', 'support.user_id = users.user_id', 'left', $this->uri->segment(2), 'support_id', 'ASC', $like, $orlike, 'support.user_id > 0');
        } else {

            $this->outputData['total'] = $this->common->GetRecordCount('support');

            $this->outputData['listoftickets'] = $this->common->JoinTables('support.support_id, support.status, subject, ticket_created_date, support.is_active, first_name, last_name, email', 'support', array(
                array('users', 'support.user_id = users.user_id', 'inner')
                    ), array('support.user_id >' => 0, 'support.is_active' => '1'));
        }

        $this->load->model('commonmodel');

        $this->load->view("admin/support/viewtickets", $this->outputData);
    }

    /**
     * 
     * END List of Support Ticket
     */

    /**
     * 
     * START List of Deleted Support Ticket (Required Id)
     */
    public function view_del_tickets() {

        $url = explode("/", current_url());

        if (in_array('searchclear', $url)) {

            $this->common->EmptySessionIndex('ticketsearch');
        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('ticketsearch' => $this->input->post('search')));
        }

        if ($this->common->GetSessionKey('ticketsearch') != "") {

            $like = array('support_id' => $this->common->GetSessionKey('ticketsearch'));

            $orlike = array('first_name' => $this->common->GetSessionKey('ticketsearch'));

            $this->outputData['total'] = $this->common->GetRecordCountLikeWhere2('support', 'users', 'support.user_id = users.user_id', 'left', $like, $orlike, 'support.user_id > 0 AND support.is_active = 0');

            $this->outputData['listoftickets'] = $this->common->
                    GetAllRowFromTableLimitOrderByLikeWhere1('*', 'support', 'users', 'support.user_id = users.user_id', 'left', $this->uri->segment(2), 'support_id', 'ASC', $like, $orlike, 'support.user_id > 0 AND support.is_active = 0');
        } else {

            $this->outputData['total'] = $this->common->GetRecordCount('support');

            $this->outputData['listoftickets'] = $this->common->JoinTables('support.support_id, support.status, subject, ticket_created_date, support.is_active, first_name, last_name, email', 'support', array(
                array('users', 'support.user_id = users.user_id', 'inner')
                    ), array('support.user_id >' => 0, 'support.is_active' => '0'));
        }

        $this->load->model('commonmodel');

        $this->load->view("admin/support/viewdeltickets", $this->outputData);
    }

    /**
     * 
     * END List of Deleted Support Ticket
     */

    /**
     * 
     * START Edit Support Ticket (Required Id)
     */
    public function edit_tickets() {

        $id = $this->uri->segment(3);

        if ($this->input->post('MM_update') != "") {

            $dataarray = array(
                'subject' => $this->input->post('subject'),
                'is_active' => $this->input->post('is_active'));

            $this->common->UpdateRecord('support', array('support_id' => $this->uri->segment(3)), $dataarray);

            $this->common->setmessage("Ticket information has been update successfully", 1);
        }

        $this->outputData['row_rsTickets'] = $this->common->GetSingleRowFromTableWhere("support", array('support_id' => $this->uri->segment(3)));

        $this->load->view("admin/support/edittickets", $this->outputData);
    }

    /**
     * 
     * END Edit Support Ticket
     */

    /**
     * 
     * START View Single Support Ticket (Required Id)
     */
    public function view_single_ticket() {
        $id = $this->uri->segment(3);

        $check = $this->input->post('submitted');

        $cr_time = $this->common->get_created_time();

        $message = $this->input->post('message');

        $by = $this->session->userdata('user_id');

        if ($check != "") {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('message', 'Message', 'trim|required|min_length[1]|xss_clean');

            if ($this->form_validation->run() == FALSE) {

                $this->common->setmessage("Please enter a message first...." . validation_errors(), -1);

                redirect(current_url());

                exit();
            }

            $cr_time = $this->common->get_created_time();

            $message = $this->input->post('message');

            $newarray = array('support_id' => $id, 'sender_bit' => 0, 'fr_ticket_conversion_message' => $message, 'fr_ticket_is_active' => 1, 'fr_ticket_created_on' => $cr_time, 'fr_ticket_modified_by' => $by);

            $insert = $this->common->InsertInDb('support_ticket_conversion_frontend', $newarray);
        }

        $this->outputData['receivemessage'] = $this->common->JoinTables('support.support_id, subject, ticket_created_date, support.is_active, sender_bit, fr_ticket_conversion_message, fr_ticket_created_on, first_name, last_name', 'support', array(
            array('support_ticket_conversion_frontend sf', "support.support_id = sf.support_id", 'left'),
            array('users', 'support.user_id = users.user_id', 'inner')
                ), array('support.support_id' => $id));

        $this->load->model('commonmodel');

        $this->load->view("admin/support/viewsingleticket", $this->outputData);
    }

    /**
     * 
     * END View Single Support Ticket
     */

    /**
     * 
     * START Edit Error Log (Required Id)
     */
    public function edit_error_log() {

        $id = $this->uri->segment(3);

        if ($this->input->post('MM_update') != "") {

            $dataarray = array(
                'error_log_subject' => $this->input->post('error_log_subject'),
                'error_log_message' => $this->input->post('error_log_message'),
                'error_log_status' => $this->input->post('error_log_status'));

            $this->common->UpdateRecord('tbl_error_log_history', array('error_log_id' => $id), $dataarray);

            $this->common->setmessage("Error Log Information has been updated successfully", 1);
        }

        $this->outputData['log'] = $this->common->GetSingleRowFromTableWhere("tbl_error_log_history", array('error_log_id' => $this->uri->segment(3)));

        $this->load->view("admin/edit_error_log", $this->outputData);
    }

    /**
     * 
     * END Edit Error Log
     */

    /**
     * 
     * START Login as User From Error Log (Required Id)
     */
    public function login_to_user() {

        $user_id = $this->uri->segment(3);

        $userinfo = $this->common->GetSingleRowFromTableWhere('users', array('user_id' => $user_id));

        $affiliated = $this->common->GetSingleRowFromTableWhere('affiliation', array('user_id' => $user_id));

        $affiliated = (count($affiliated) > 0) ? 1 : 0;

        $this->common->getactivity('login_as_user', ' (' . $userinfo['first_name'] . ' ' . $userinfo['last_name'] . ')');
        
        $lid = $this->common->InsertInDb('logging', array('user_id' => $userinfo['user_id'], 'login' => $this->common->GetCreatedTime()));

        $SesseionArray = array('affiliated' => $affiliated, 'user_id' => $userinfo['user_id'], 'accesslevel' => $userinfo['accesslevel'], 'first_name' => $userinfo['first_name'], 'last_name' => $userinfo['last_name'], 'email' => $userinfo['email'], 'lid' => $lid);

        $this->session->unset_userdata(array('lid' => '', 'user_id' => '', 'is_admin' => '', 'first_name' => '', 'last_name' => '', 'email' => '', 'affiliated' => '', 'accesslevel' => ''));

        $this->session->set_userdata($SesseionArray); //var_dump($this->session->all_userdata());exit;

        redirect(base_url(), 'refresh');

        exit();
    }

    /**
     * 
     * END Login as User From Error Log
     */

    /**
     * 
     * START View Error Log (Required Id)
     */
    public function error_log_view() {

        if ($this->uri->segment(3) > 0) {

            $this->outputData['listoflogs'] = $this->common->GetSingleRowFromTableJoinWhere("tbl_error_log_history", 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'LEFT', array('error_log_id' => $this->uri->segment(3)));

            if (count($this->outputData['listoflogs']) > 0) {

                $this->load->view('admin/error_log', $this->outputData);
            } else {

                $this->common->redirect(site_url('admin/errors_log'));
            }
        } else {

            $this->load->view('admin/error_log', $this->outputData);
        }
    }

    /**
     * 
     * END View Error Log
     */

    /**
     * 
     * START View Deleted Error Log (Required Id)
     */
    public function del_error_log_view() {

        if ($this->uri->segment(3) > 0) {

            $this->outputData['listoflogs'] = $this->common->GetSingleRowFromTableJoinWhere("tbl_error_log_history", 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'LEFT', array('error_log_id' => $this->uri->segment(3)));

            if (count($this->outputData['listoflogs']) > 0) {

                $this->load->view('admin/del_errors_log', $this->outputData);
            } else {

                $this->common->redirect(site_url('admin/del_errors_log'));
            }
        } else {

            $this->load->view('admin/del_errors_log', $this->outputData);
        }
    }

    /**
     * 
     * END View Deleted Error Log
     */

    /**
     * 
     * START View FAQ's Feedback (Required Id)
     */
    public function view_faq_feedback() {

        $this->load->view("admin/faq/faq_feedback", $this->outputData);
    }

    /**
     * 
     * END View FAQ's Feedback
     */

    /**
     * 
     * START View Deleted FAQ's Feedback (Required Id)
     */
    public function view_deleted_faq_feedback() {

        $this->load->view("admin/faq/deleted_faq_feedback", $this->outputData);
    }

    /**
     * 
     * END View Deleted FAQ's Feedback
     */

    /**
     * 
     * START View FAQ's Categories (Required Id)
     */
    public function all_faq_categories() {

        $this->load->view("admin/faq/delete_faqs", $this->outputData);
    }

    /**
     * 
     * END View FAQ's Categories
     */

    /**
     * 
     * START Delete Multiple Users (Required Id)
     */
    public function deleteallusers() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_user = count($splitted);
        if ($number_user > 1) {
            $number_users = 's (' . $number_user . ')';
        } else {
            $number_users = '';
        }

        $this->common->UpdateRecord('users', 'user_id IN (' . $list . ')', array('is_active' => '0'));

        $this->common->getactivity('inactive_user', $number_users);

        echo 1;
    }

    /**
     * 
     * END Delete Multiple Users
     */

    /**
     * 
     * START Restore Multiple Users (Required Id)
     */
    public function restoreallusers() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);
        $splitted = explode(",", $list);
        $number_user = count($splitted);
        if ($number_user > 1) {
            $number_users = 's (' . $number_user . ')';
        } else {
            $number_users = '';
        }

        $this->common->UpdateRecord('users', 'user_id IN (' . $list . ')', array('is_active' => '1'));

        $this->common->getactivity('restor_user', $number_users);

        echo 1;
    }

    /**
     * 
     * END Restore Multiple Users
     */

    /**
     * 
     * START Delete Multiple Airports (Required Id)
     */
    public function deleteallairports() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_a = count($splitted);
        if ($number_a > 1) {
            $numbers_a = 's (' . $number_a . ')';
        } else {
            $numbers_a = '';
        }

        $this->common->UpdateRecord('airports', 'aid IN (' . $list . ')', array('airport_is_active' => '0'));

        $this->common->getactivity('inactive_airport', $numbers_a);

        echo 1;
    }

    /**
     * 
     * END Delete Multiple Airports
     */

    /**
     * 
     * START Delete Not Published Airports (Required Id)
     */
    public function deleteallnotpublishairports() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_a = count($splitted);
        if ($number_a > 1) {
            $numbers_a = 's (' . $number_a . ')';
        } else {
            $numbers_a = '';
        }

        $this->common->UpdateRecord('airports', 'aid IN (' . $list . ')', array('airport_is_active' => '-1'));

        $this->common->getactivity('inactive_airport', $numbers_a);

        echo 1;
    }

    /**
     * 
     * END Delete Not Published Airports
     */

    /**
     * 
     * START Restore Published & Not Published Airports (Required Id)
     */
    public function restoreallairports() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_a = count($splitted);
        if ($number_a > 1) {
            $numbers_a = 's (' . $number_a . ')';
        } else {
            $numbers_a = '';
        }

        $this->db->query("UPDATE `airports` SET airport_is_active= airport_is_active+1 WHERE airport_is_active != 1 AND aid IN ($list)");

        $this->common->getactivity('restor_airport', $numbers_a);

        echo 1;
    }

    /**
     * 
     * END Restore Published & Not Published Airports
     */

    /**
     * 
     * START Delete Multiple Companies (Required Id)
     */
    public function deleteallcompanies() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_c = count($splitted);
        if ($number_c > 1) {
            $numbers_c = 's (' . $number_c . ')';
        } else {
            $numbers_c = '';
        }

        $this->common->UpdateRecord('companies', 'cid IN (' . $list . ')', array('comp_is_active' => 0));

        $this->common->getactivity('inactive_company', $numbers_c);

        echo 1;
    }

    /**
     * 
     * END Delete Multiple Companies
     */

    /**
     * 
     * START Restore Published & Not Published Companies (Required Id)
     */
    public function restoreallcompanies() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_c = count($splitted);
        if ($number_c > 1) {
            $numbers_c = 's (' . $number_c . ')';
        } else {
            $numbers_c = '';
        }

        $this->db->query("UPDATE `companies` SET comp_is_active= comp_is_active+1 WHERE comp_is_active != 1 AND cid IN ($list)");

        $this->common->getactivity('restor_company', $numbers_c);

        echo 1;
    }

    /**
     * 
     * END Restore Published & Not Published Companies
     */

    /**
     * 
     * START Delete Not Published Companies (Required Id)
     */
    public function deleteallnotpublishcompany() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_c = count($splitted);
        if ($number_c > 1) {
            $numbers_c = 's (' . $number_c . ')';
        } else {
            $numbers_c = '';
        }

        $this->common->UpdateRecord('companies', 'cid IN (' . $list . ')', array('comp_is_active' => '-1'));

        $this->common->getactivity('inactive_company', $numbers_c);

        echo 1;
    }

    /**
     * 
     * END Delete Not Published Companies
     */

    /**
     * 
     * START Delete Error Logs (Required Id)
     */
    public function deleteallerrorlog() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_e = count($splitted);
        if ($number_e > 1) {
            $numbers_e = 's (' . $number_e . ')';
        } else {
            $numbers_e = '';
        }

        $this->common->UpdateRecord('tbl_error_log_history', 'error_log_id IN (' . $list . ')', array('error_log_active' => 0));

        $this->common->getactivity('inactive_error_log', $numbers_e);

        echo 1;
    }

    /**
     * 
     * END Delete Error Logs
     */

    /**
     * 
     * START Restore Error Logs (Required Id)
     */
    public function restoreallerrorlog() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_e = count($splitted);
        if ($number_e > 1) {
            $numbers_e = 's (' . $number_e . ')';
        } else {
            $numbers_e = '';
        }

        $this->common->UpdateRecord('tbl_error_log_history', 'error_log_id IN (' . $list . ')', array('error_log_active' => 1));

        $this->common->getactivity('restor_error_log', $numbers_e);

        echo 1;
    }

    /**
     * 
     * END Restore Error Logs
     */

    /**
     * 
     * START Delete Company's Locations (Required Id)
     */
    public function deleteallcompanieslocation() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_cl = count($splitted);
        if ($number_cl > 1) {
            $numbers_cl = 's (' . $number_cl . ')';
        } else {
            $numbers_cl = '';
        }

        $this->common->UpdateRecord('lkpairportlist', 'aid IN (' . $list . ')', array('flag' => 0));

        $this->common->getactivity('delete_airport_lookup', $numbers_cl);

        echo 1;
    }

    /**
     * 
     * END Delete Company's Locations
     */

    /**
     * 
     * START Delete User's Login Report (Required Id)
     */
    public function deleteallloginreport() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_l = count($splitted);
        if ($number_l > 1) {
            $numbers_l = 's (' . $number_l . ')';
        } else {
            $numbers_l = '';
        }

        $this->common->UpdateRecord('logging', 'lid IN (' . $list . ')', array('log_is_active' => 0));

        $this->common->getactivity('inactive_user_login', $numbers_l);

        echo 1;
    }

    /**
     * 
     * END Delete User's Login Report
     */

    /**
     * 
     * START Restore User's Login Report (Required Id)
     */
    public function restoreallloginreport() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_l = count($splitted);
        if ($number_l > 1) {
            $numbers_l = 's (' . $number_l . ')';
        } else {
            $numbers_l = '';
        }

        $this->common->UpdateRecord('logging', 'lid IN (' . $list . ')', array('log_is_active' => 1));

        $this->common->getactivity('restor_user_login', $numbers_l);

        echo 1;
    }

    /**
     * 
     * END Restore User's Login Report
     */

    /**
     * 
     * START Delete Support Questions (Required Id)
     */
    public function deleteallsupportques() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_s = count($splitted);
        if ($number_s > 1) {
            $numbers_s = 's (' . $number_s . ')';
        } else {
            $numbers_s = '';
        }

        $this->common->UpdateRecord('support', 'support_id IN (' . $list . ')', array('is_active' => 0));

        $this->common->getactivity('inactive_support_q', $numbers_s);

        echo 1;
    }

    /**
     * 
     * END Delete Support Questions
     */

    /**
     * 
     * START Restore Support Questions (Required Id)
     */
    public function restoreallsupportques() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_s = count($splitted);
        if ($number_s > 1) {
            $numbers_s = 's (' . $number_s . ')';
        } else {
            $numbers_s = '';
        }

        $this->common->UpdateRecord('support', 'support_id IN (' . $list . ')', array('is_active' => 1));

        $this->common->getactivity('restor_support_q', $numbers_s);

        echo 1;
    }

    /**
     * 
     * END Restore Support Questions
     */

    /**
     * 
     * START Delete FAQ's Feedback Temporary (Required Id)
     */
    public function deletefaqsfeedback() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_fb = count($splitted);
        if ($number_fb > 1) {
            $numbers_fb = 's (' . $number_fb . ')';
        } else {
            $numbers_fb = '';
        }

        $this->common->UpdateRecord('tbl_faq_feedback', 'feedback_id IN (' . $list . ')', array('is_active' => 0));

        $this->common->getactivity('inactive_faq_fb', $numbers_fb);

        echo 1;
    }

    /**
     * 
     * END Delete FAQ's Feedback Temporary
     */

    /**
     * 
     * START Delete FAQ's Feedback Permanently (Required Id)
     */
    public function deletefaqsfeedbackP() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_fb = count($splitted);
        if ($number_fb > 1) {
            $numbers_fb = 's (' . $number_fb . ')';
        } else {
            $numbers_fb = '';
        }

        $this->common->DeleteRecord('tbl_faq_feedback', 'feedback_id IN (' . $list . ')');

        $this->common->getactivity('delete_faq_fb', $numbers_fb);

        echo 1;
    }

    /**
     * 
     * END Delete FAQ's Feedback Permanently
     */

    /**
     * 
     * START Restore FAQ's Feedback (Required Id)
     */
    public function restorefaqsfeedback() {

        $myCheckboxes = $this->input->post('myCheckboxes');

        $list = str_replace('on,', '', $myCheckboxes);

        $splitted = explode(",", $list);
        $number_fb = count($splitted);
        if ($number_fb > 1) {
            $numbers_fb = 's (' . $number_fb . ')';
        } else {
            $numbers_fb = '';
        }

        $this->common->UpdateRecord('tbl_faq_feedback', 'feedback_id IN (' . $list . ')', array('is_active' => 1));

        $this->common->getactivity('restor_faq_fb', $numbers_fb);

        echo 1;
    }

    /**
     * 
     * END Restore FAQ's Feedback
     */

    /**
     * 
     * START Configer User's Access (Required Id)
     */
    public function access_config() {

        $access_name = $this->input->post('access_name');

        $access_level = $this->input->post('access_level');

        $is_active_access = $this->input->post('is_active_access');

        $this->outputData['total'] = $this->common->GetSingleRowFromTableWhere('tbl_access_level_config', array('config_id' => $access_name, 'levelid' => $access_level));

        $alc = $this->outputData['total'];


        if ($access_name != '' && $access_level != '') {

            if ($this->input->post('is_active_access') != "") {

                $is_active_access = 1;
            } else {

                $is_active_access = 0;
            }

            if (count($alc) > 0) {

                $this->common->UpdateRecord("tbl_access_level_config", array('config_id' => $access_name, 'levelid' => $access_level), array('is_active' => $is_active_access));

                $this->common->setmessage('User Access has been updated Successfully.', 1);
            } else {

                $dataarray = array('config_id' => $access_name, 'levelid' => $access_level, 'is_active' => 1);

                $this->common->InsertInDb("tbl_access_level_config", $dataarray);

                $this->common->setmessage('User Access has been updated Successfully.', 1);
            }
        }
//        else {
//
//            $this->common->setmessage("Please select the both (User's Access & Access Level).", -1);
//
//        }



        $from = 'tbl_access_level_config';

//        $where = "tbl_access_level_config.levelid = '" . $access_level . "' AND tbl_access_level_config.config_id  = '" . $access_name . "'";

        $select = "tbl_access_config.access_name,accesslevels.level,tbl_access_level_config.is_active";

        $join_array2 = array(
            array('tbl_access_config', 'tbl_access_config.id = tbl_access_level_config.config_id', 'LEFT'),
            array('accesslevels', 'accesslevels.levelid = tbl_access_level_config.levelid', 'LEFT')
        );

        $this->outputData['access_config_level'] = $this->common->JoinTables($select, $from, $join_array2);

        $access_config = $this->common->GetAllRowFromTableWhere('tbl_access_config', 'id > 0');

        $this->outputData['access_config'] = $access_config;

        $id = $this->uri->segment(3);

        if ($this->input->post('message') != '') {

            $this->common->UpdateRecord("tbl_footer_content", array('id' => $id), array('content' => $this->input->post('message')));

            $this->common->setmessage("Content has been saved successfully.", 1);
        }

        $FooterContent = $this->common->get_all_row_from_table('tbl_footer_content', 'id != ""', 'id,content_name,n_remove_words,content', '', 'id');

        $this->outputData['FooterContent'] = $FooterContent;

        if ($id != '') {
            $this->outputData['FooterContent_t'] = $this->common->GetSingleRowFromTableWhere("tbl_footer_content", array('id' => $id));
        }

        $this->load->view("admin/setting", $this->outputData);
    }

    /**
     * 
     * END Configer User's Access
     */
    /* public function faq_feedback_chart()

      {

      $this->outputData['question'] = $this->common->GetAllRowWithColumn('tbl_faq','*','faq_is_active = 1','faq_id','ASC');

      $this->outputData['status'] = $this->common->get_all_row_from_table_where('tbl_faq_feedback', array('is_active' => 1));

      //$this->outputData['alldata']=$this->common->JoinTable("*",'tbl_faq','tbl_faq_feedback','tbl_faq_feedback.faq_id = tbl_faq.faq_id','LEFT',array());

      $this->load->view("admin/faq/view_chart",$this->outputData);

      } */

    /**
     * 
     * START View FAQ's Activity Log (Required Id)
     */
    public function view_activity_log() {

        $this->load->view("admin/view_activity_log", $this->outputData);
    }

    public function reset_records(){

        $this->load->view('admin/reset_record/record', $this->outputData);
    }
     public function reset_data(){
  
        $sql1="UPDATE `airporttotals` SET `apasstraffic`=0,`apasstrafficcompare`=0,`aenplaning`=0,`adeplaning`=0,`aepdomestic`=0,`aepintl`=0,`aconcessiongrosssales`=0,
        `aconcessiongrosssales-df`=0,`asalesep`=0,`asalesep-df`=0,`arentrev`=0,`arentrev-df`=0,`arentep`=0,`arentep-df`=0,`afbgrosssales`=0,
        `afbsalesep`=0,`afbrentrev`=0,`afbrentep`=0,`asrgrosssales`=0,`asrsalesep`=0,`asrrentrev`=0,`asrrentep`=0,`anggrosssales`=0,
        `angsalesep`=0,`angrentrev`=0,`angrentep`=0,`adfgrosssales`=0,`adfsalesep`=0,`adfrentrev`=0,`adfrentep`=0,`apsgrosssales`=0,
        `apssalesep`=0,`apsrentrev`=0,`apsrentep`=0,`aadgrosssales`=0,`aadsalesep`=0,`aadrentrev`=0,`acegrosssales`=0,`acesalesep`=0,
        `acerentrev`=0,`acerentep`=0,`aconcessiongrosssales_incl_df`=0,`asalesep_incl_df`=0,`arentrev_incl_df`=0,`arentep_incl_df`=0 WHERE 1";
        $query1=$this->db->query($sql1);

       $sql2="UPDATE `terminalsannual` SET `tpasstraffic`='0',`tpasstrafficcompare`='0',`tdeplaning`='0',`tenplaning`='0',`tepdomestic`='0',`tepintl`='0',`fbgrosssales`='0',`fbsalesep`='0',`fbrentrev`='0',`fbrentep`='0',`srgrosssales`='0',`srsalesep`='0',`srrentrev`='0',`srrentep`='0',`nggrosssales`='0',`ngsalesep`='0',`ngrentrev`='0',`ngrentep`='0',`dfgrosssales`='0',`dfsalesep`='0',`dfrentrev`='0',`dfrentep`='0'";
       $query2=$this->db->query($sql2);
      
      $year=date('Y')+1;
      $sql3="UPDATE `airports` SET `a_year`='$year',`lastmodified`='0000-00-00',`lmodified`='0000-00-00 00:00:00',`modifiedby`='',`published`='',`approved`='0' WHERE 1";
      $query3=$this->db->query($sql3);

     $sql4="UPDATE `companies` SET `c_year`='$year',`lastmodified`='0000-00-00',`lmodified`='0000-00-00 00:00:00',`modifiedby`='',`published`='',`approved`='0' WHERE 1";
     $query4=$this->db->query($sql4);
     echo 1;
    }

    /**
     * 
     * END View FAQ's Activity Log
     */

    /**
     * 
     * START View Errors Activity Log (Required Id)
     */
    public function view_error_activity_log() {

        $this->load->view("admin/view_activity_log", $this->outputData);
    }

    /**
     * 
     * END View Errors Activity Log
     */

    /**
     * 
     * START View Airports Activity Log (Required Id)
     */
    public function view_aiorport_activity_log() {

        $this->load->view("admin/view_activity_log", $this->outputData);
    }

    /**
     * 
     * END View Airports Activity Log
     */

    /**
     * 
     * START View Companies Activity Log (Required Id)
     */
    public function view_company_activity_log() {

        $this->load->view("admin/view_activity_log", $this->outputData);
    }

    /**
     * 
     * END View Companies Activity Log
     */

    /**
     * 
     * START View Users Activity Log (Required Id)
     */
    public function view_user_activity_log() {

        $this->load->view("admin/view_activity_log", $this->outputData);
    }

    /**
     * 
     * END View Users Activity Log
     */

    /**
     * 
     * START View Compose Email
     */
    public function compose() {

        $this->outputData['users'] = $this->common->get_all_row_from_table('users', 'email != ""', 'first_name, last_name, email, user_id', '', 'first_name');

        $this->load->view('admin/email/send_email', $this->outputData);
    }

    /**
     * 
     * END View Compose Email
     */

    /**
     * 
     * START Send Email To Multiple Users (Required Id)
     */
    public function send_message() {

        $answer = htmlentities($this->input->post('message'), ENT_DISALLOWED, 'UTF-8');

        $receivers = $this->input->post('rec_id');
        $subject = $this->input->post('subject');
//        $fname = $this->input->post('fname');
//        $lname = $this->input->post('lname');
        $message_text = $this->input->post('message');

        $this->load->library('email');

        $config['smtp_host'] = 'mail.arnfactbook.com';
        $config['smtp_user'] = 'support@arnfactbook.com';
        $config['smtp_pass'] = 'Urbanexpo1';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        $link = 'send_email';
        $where1 = array('email_temp_name' => $link);

        $email_template = $this->common->GetSingleRowFromTableWhere('tbl_email_temp', $where1);

        $search_array = array('#message_text ');

        $replace_array = array($message_text);

        $message1 = $email_template['email_temp_text'];

        $message = str_replace($search_array, $replace_array, $message1);

        $data = array(
            'message' => $message
        );

        foreach ($receivers as $receiver_id):
            $this->email->from('support@arnfactbook.com', 'Arnfactbook Support');
            $this->email->to($receiver_id);
            $this->email->subject($subject);
            $this->email->message($this->load->view('frontend/email_temp', $data, true));
            $this->email->send();
        endforeach;
        $this->common->getactivity('send_email_custome_msg', '');
        $this->common->setmessage("Message has been sent successfully.", 1);
        redirect(site_url('admin/compose'));
    }

    /**
     * 
     * END Send Email To Multiple Users
     */

    /**
     * 
     * START Edit Email Content (Required Id)
     */
    public function email_content() {

        $id = $this->uri->segment(3);

        if ($this->input->post('message')) {

            $this->common->UpdateRecord("tbl_email_temp", array('id' => $id), array('email_temp_text' => $this->input->post('message')));

            $this->common->getactivity('edit_email_content', '');
            
            $this->common->setmessage("Message has been saved successfully.", 1);
        }

        $email_temp = $this->common->get_all_row_from_table('tbl_email_temp', 'id != ""', 'email_temp_name, email_temp_text, id', '', 'email_temp_name');

        foreach ($email_temp as $e_temp) {

            $str = $e_temp['id'] . ',' . $e_temp['email_temp_name'];

            $search_array = array('changepassword', 'feedbacksuggestion', 'unsubsribeuser', '_');

            $replace_array = array('change password', 'feedback suggestion', 'unsubsribe user', ' ');

            $lstr = strtolower(str_replace($search_array, $replace_array, $str));

            $a = explode(",", $lstr);

            $aa = array_values($a);

            $e_con[] = $aa;
        }

        $this->outputData['email_temp'] = $e_con;

        if ($id != '') {
            $this->outputData['email_temp_t'] = $this->common->GetSingleRowFromTableWhere("tbl_email_temp", array('id' => $id));
        }

        $this->load->view('admin/email/email_content', $this->outputData);
    }

    /**
     * 
     * END Edit Email Content
     */
}
