<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class webreport extends CI_Controller 
{
		public $dataarray;
		public $style;	
		public $mpdf;
		public $limit;
		public $outputData="";	
		function __construct()	
		{			
			parent::__construct();
			$this->common->ThisSecureArea('user');
			include_once APPPATH.'mpdf/mpdf.php';
			$this->common->pdfstyleing();
			$this->load->model('reportmodel');			
			$this->outputData['searchaction']='';
			$this->outputData['header']='frontend/include/header';
			$this->outputData['menu']='frontend/include/menu';		
			$this->outputData['footer']='frontend/include/footer';	
			error_reporting(E_ERROR);		
		}		
		public function index()
		{		
			
			$ayear = $this->input->post('years');	
			if(!(is_numeric($ayear) && $ayear>2009 && $ayear<=date("Y")) && !is_array($ayear))
				$ayear = date("Y");			
			$this->limit=$this->input->post('limit');
			$listofcolumns=$this->input->post('arr');
			$format=$this->input->post('format');						
			$reportinfo=$this->common->GetSingleRowFromTableWhere('reports',array('report_id'=>$this->input->post('reportid')));			
			$segment=$reportinfo['url'];
			switch($segment)
			{
				case "top-int-airports2":
					$this->outputData['output']=$this->top50terminals2('FB',$ayear);
					$this->load->view("frontend/reports/web-reports",$this->outputData);
				break;
				case "all-airports":
					$this->outputData['output']=$this->allairport($ayear,$listofcolumns);				
					$this->load->view("frontend/reports/web-reports",$this->outputData);
				break;
				case "ratio-report":
					$this->outputData['output']=$this->ratioreport($ayear,$listofcolumns);		 
					$this->load->view("frontend/reports/web-reports",$this->outputData);	
				break;
				case "advertising":					
					$this->outputData['output']=$this->top50terminals('AD',$ayear,$listofcolumns);
					$this->load->view("frontend/reports/web-reports",$this->outputData);					
				break;
				case "food-beverage":					
					$this->outputData['output']=$this->top50terminals('FB',$ayear,$listofcolumns);	
					$this->load->view("frontend/reports/web-reports",$this->outputData);				
				break;
				case "specialty-retail":					
					$this->outputData['output']=$this->top50terminals('SR',$ayear,$listofcolumns);
					$this->load->view("frontend/reports/web-reports",$this->outputData);		
				break;
				case "news-gifts":					
					$this->outputData['output']=$this->top50terminals('NG',$ayear,$listofcolumns);
					$this->load->view("frontend/reports/web-reports",$this->outputData);		
				break;
				case "duty-free":					
					$this->outputData['output']=$this->top50terminals('DF',$ayear,$listofcolumns);
					$this->load->view("frontend/reports/web-reports",$this->outputData);		
				break;
				case "passenger-services":					
					$this->outputData['output']=$this->top50terminals('PT',$ayear,$listofcolumns);
					$this->load->view("frontend/reports/web-reports",$this->outputData);		
				break;
				case "top-50-airports":					
					 $this->outputData['output']=$this->top50airport($ayear,'T50A',$listofcolumns);				 
					 $this->load->view("frontend/reports/web-reports",$this->outputData);							
				break;
				case "top-int-airports":										
					$this->outputData['output']=$this->top50airport($ayear,'TIA',$listofcolumns);	
					$this->load->view("frontend/reports/web-reports",$this->outputData);				
				break;
				case "lease-expire2":
					$ayear=$this->uri->segment(4);
					if(is_numeric($ayear))
					{
						$this->outputData['output']=$this->leaseexpire2($ayear,'TIA');
						$this->load->view("frontend/reports/web-reports",$this->outputData);
						
					} else
					{
						$this->common->setmessage('Invalid URL Year.',-1);
						$this->common->redirect(site_url('reports'));		
					}
				break;
				case "lease-expire":
					$this->outputData['output']=$this->leaseexpire($ayear,'TIA',$listofcolumns);		 
					$this->load->view("frontend/reports/web-reports",$this->outputData);					
				break;
				case "tenant-listing":
					$this->outputData['output']=$this->tenant_listing($ayear, $listofcolumns);			 
					$this->load->view("frontend/reports/web-reports",$this->outputData);
				break;
				case "airport-reports":
					$this->outputData['output']=$this->allairport($ayear, $listofcolumns, $this->input->post('apid'));
					$this->load->view("frontend/reports/web-reports",$this->outputData);
				break;
				default:
					$this->common->setmessage('Invalid URL',-1);
					$this->common->redirect(site_url('reports'));	
				exit;
			}			
				
		}
		public function report()
		{
			$ayear=$this->uri->segment(4);
			if($this->common->GetSessionKey('accesslevel')==2 && $this->uri->segment(5)!="" && is_numeric($this->uri->segment(5)) && $this->uri->segment(5)>0 && $this->uri->segment(5)!=50)
				{
					$this->limit=$this->uri->segment(5);
				} else 
				{
					$this->limit=50;		
				}	
			if(!is_numeric($ayear))
			{
				$this->common->setmessage('Invalid URL Year.',-1);
				$this->common->redirect(site_url('reports'));		
			}
			$segment=strtolower($this->uri->segment(3));			
			switch($segment)
			{
				case "top-int-airports2":
					$this->top50terminals2('FB',$ayear);
				break;
				case "all-airports":
						$this->allairport($ayear);
				break;
				case "ratio-report":
					$this->ratioreport($ayear);
				break;
				case "advertising":					
					$this->top50terminals('AD',$ayear);					
				break;
				case "food-beverage":					
					$this->top50terminals('FB',$ayear);					
				break;
				case "specialty-retail":					
					$this->top50terminals('SR',$ayear);		
				break;
				case "news-gifts":					
					$this->top50terminals('NG',$ayear);		
				break;
				case "duty-free":					
					$this->top50terminals('DF',$ayear);		
				break;
				case "passenger-services":					
					$this->top50terminals('PT',$ayear);		
				break;
				case "top-50-airports":
					$this->top50airport($ayear,'T50A');					
				break;
				case "top-int-airports":
					if(is_numeric($ayear))
					{
						$this->top50airport($ayear,'TIA');
					} else
					{
						$this->common->setmessage('Invalid URL Year.',-1);
						$this->common->redirect(site_url('reports'));		
					}
				break;
				case "lease-expire2":
					if(is_numeric($ayear))
					{
						$this->leaseexpire2($ayear,'TIA');
					} else
					{
						$this->common->setmessage('Invalid URL Year.',-1);
						$this->common->redirect(site_url('reports'));		
					}
				break;
				case "lease-expire":
					if(is_numeric($ayear))
					{
						$this->leaseexpire($ayear,'TIA');
					} else
					{
						$this->common->setmessage('Invalid URL Year.',-1);
						$this->common->redirect(site_url('reports'));		
					}
				break;
				default:
					$this->common->setmessage('Invalid URL',-1);
					$this->common->redirect(site_url('reports'));	
				exit;
			}			 								
		}
		public function tenant_listing($ayear, $col)
		{
			$numlocations=array();
			$sqft=array();
			
			$cols = $this->reportmodel->getcols($col);
			$cols = str_replace('category', 'lkpcategory`.`category', $cols);
			$tenant_list = $this->common->CustomQuery("SELECT $cols FROM outlets 
							LEFT JOIN lkpcategory ON lkpcategory.categoryid = outlets.categoryid
							LEFT JOIN airports ON airports.aid = outlets.aid LIMIT 0, ".$this->limit);
			$list="Tenant Listing";
			$tbl_header .= '<div class="alert alert-info"><h3>'.$list.'</div></h3>';
			$tbl_header .= '<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
			$tbl_header .= '<tr class="info">';
			$n = (count($col) != 0)?100/count($col):100;
			foreach($col as $i=>$coll):
				$column = $this->reportmodel->coltitle($i);
				$tbl_header .= '<th width="'.$n.'%">'.$column.'</th>';
			endforeach;
			$tbl_header .= '</tr>';
			
			$color = '#EEEEEE';
			
			$alldata=array();
			$numlocations[]=array('outletname', 'Num Locations');
			$sqft[]=array('outletname', 'Sqft');
			
			
			foreach($tenant_list as $tenant):	
			
				$numlocations[]=array($tenant['outletname'],intval($tenant['numlocations']));	
				$sqft[]=array($tenant['outletname'],intval($tenant['sqft']));
				
				$color = ($color == '#EEEEEE')?'#FFFFFF':'#EEEEEE';
				$tbl_header .= '<tr>';
				foreach($col as $i=>$coll):
					$tbl_header .= '<td>'.$tenant[$i].'</td>';
				endforeach;
				$tbl_header .= '</tr>';
			endforeach;
			
			$alldata[]=$numlocations;
			$alldata[]=$sqft;
			$this->outputData['alldata']=$alldata;
			
			$tbl_header .= '</table></div>';
			//echo $tbl_header;
			$str = preg_replace('/\s\s+/', ' ', $tbl_header);
			//$mpdf->WriteHTML($str);	
			//$mpdf->Output();
			return $str;
		}
		public function leaseexpire2($year,$type)
		{
			
			$resourcearray = array();
			$indexarray = array();
			$tbl_header="";
			$rcounter=0;
			$class="";
			$val='Leases Due to Expire by Year-End '.$year;
			
			$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
		    $listsql = "select distinct category from lkpcategory";
		    $listsql2="SELECT count(oid) FROM `outlets` ";
			$list=$this->common->CustomQueryALL($listsql);
			$listcount=$this->common->CustomCountQuery($listsql);
			$nr_of_users=$this->common->CustomQueryALL($listsql2);			
			foreach($list as $cmpnyterms):
				$catid=$cmpnyterms['category'];
				$ref=trim($year);				
         		$products = $this->common->CustomQueryALL("select DATE_FORMAT(exp,'%m-%d-%y') as expf, outlets.* from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='".$catid."' and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and outlets.exp LIKE '%".$ref."%' order by DATE_FORMAT(exp,'%m-%d-%y') ASC ");
				$tbl_header.='<tr>
				<td>'.$catid.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>'.date("Y-m-d H:i:s").'</td>
				</tr>
				<tr>
				<td>IATA</td>
				<td>Outlet Name/Description(Company)</td>
				<td>#Location</td>				
				<td>Sq. Ft.</td>
				<td>Expires</td>
				</tr>';
				foreach($products as $prod):
					$aiddd=$prod['aid'];			
					$citystrSQL = "SELECT IATA,acity FROM airports WHERE aid='".$aiddd."'";							
					$acity_row=$this->common->CustomQueryROw($citystrSQL);					
					 if(!$this->common->pdf_check_date($prod['exp']))
					 {  
					 	$date_val=date('m/d/Y', strtotime($prod['exp']));
				     } elseif(strtotime($prod['exp']) =='')
					 {			
						$date_val="00/00/0000";			
					 }elseif(is_null($prod['exp']))
					 {
        				$date_val="00/00/0000";
			        }elseif($prod['exp']=='0000-00-00')
        			{
				        $date_val="00/00/0000";
			        }
					else
					{
						$date_val=date('m/d/Y', strtotime($prod['exp'])); 
					}
					if (empty($prod['companyname'])) 
					{				
						$compny='';				
					}else				
					{				
						$compny=' ('.$prod['companyname'].')';				
					}
					$date = $date_val;
					$outletname = stripslashes($prod['outletname']).$compny;
					$acity	= $acity_row['acity'];
					$numlocations	= $prod['numlocations'];
					$sqft			= number_format($prod['sqft']);	
					array_push($resourcearray, array('outletname' => $outletname,'iata'=>$acity_row['IATA'],'acity' =>$acity,'numlocations'=>$numlocations,'sqft'=>$sqft,'date' => $date,));				    array_push($indexarray, $date);							
				endforeach;// $prod
				foreach($resourcearray as $resource) {
				$tbl_header.='<tr>
					<td>'.$resource['iata'].'</td>	
					<td>'.stripslashes($resource['outletname']).'</td>
					<td>'.$resource['acity'].'</td>			
					<td>'.stripslashes($resource['sqft']).'</td>
					<td>'.stripslashes($resource['date']).'</td>
					</tr>';			
			}
			endforeach;			
			$tbl_header.='</table></div>';
			$str = preg_replace('/\s\s+/', ' ', $tbl_header);
			return $str;
		}
		public function top50terminals2($reportfor,$year)
		{
			
			if($reportfor=='SR')
			{
				$reportfor='SR';
				$reptitle="Specialty Retail";	
			}
			if($reportfor=='DF')
			{
				$reportfor='DF';
				$reptitle="Duty Free";
			}
			if($reportfor=='NG')
			{
				$reportfor='NG';
				$reptitle="News & Gifts";
			}
				if($reportfor=='FB')
			{
				$reportfor='FB';
				$reptitle="Food and Beverage";
			}
			if($reportfor=='PT')
			{
				$reportfor='PT';
				$reptitle="Passenger Traffic";
			}
			if($reportfor=="AD")
			{
				$reptitle='Advertising';
			}
			$airport=$this->common->CustomQueryALL('SELECT DISTINCT(`IATA`),`aid` as aid,`acity` FROM `airports`');
			$val=$year.' Top '.$this->limit.' Terminals by '.$reptitle;
			$tbl = '';
			$tbl2 = '';
			$tbl_header='';
			$tbl_header2='';
			$users = array();
			$TCurrentSqFt=0;
			$TGrossSale=0;
			$TSalesEp=0;
			$TRentRev=0;
			foreach ($airport as $AP)
			{
				/******************************************************************/				
				$airportinfo=$this->common->GetSingleRowFromTableWhere('airports',array('aid'=>$AP['aid']));	
				$tl=$this->common->JoinTable('*','terminals','terminalsannual',
				'terminalsannual.tid=terminals.tid','LEFT',"aid = '".$AP['aid']."' AND terminalsannual.tyear='".$year."' ORDER BY terminals.tid");
				$fbgrosssalestotal	= 0;
				$srgrosssalestotal	= 0;
				$nggrosssalestotal	= 0;	
				$fbrentrevtotal	= 0;
				$srrentrevtotal	= 0;
				$ngrentrevtotal	= 0;
				$indi_terms_list="";
				if(count($tl)>0)
				{
					foreach ($tl as $_) 
					{				
						if($reportfor=='AD')
						{
							$CurrentSqFt= $_["dfcurrsqft"];
							$GrossSale=$_["dfgrosssales"];
							$SalesEp=$_["tpasstraffic"];
							$RentRev=$_["dfrentrev"];				
						}	
						if($reportfor=='PT')
						{
							$CurrentSqFt= $_["dfcurrsqft"];
							$GrossSale=$_["dfgrosssales"];
							$SalesEp=$_["tpasstraffic"];
							$RentRev=$_["dfrentrev"];				
						}	
						if($reportfor=='DF')
						{
							$CurrentSqFt= $_["dfcurrsqft"];
							$GrossSale=$_["dfgrosssales"];
							$SalesEp=$_["dfsalesep"];
							$RentRev=$_["dfrentrev"];
						}
						if($reportfor=='SR')
						{
							$CurrentSqFt= $_["srcurrsqft"];
							$GrossSale=$_["srgrosssales"];
							$SalesEp=$_["srsalesep"];
							$RentRev=$_["srrentrev"];
						}
						if($reportfor=='FB')
						{
							$CurrentSqFt= $_["fbcurrsqft"];
							$GrossSale=$_["fbgrosssales"];
							$SalesEp=$_["fbsalesep"];
							$RentRev=$_["fbrentrev"];
						}
						if($reportfor=='NG')
						{
							$CurrentSqFt= $_["ngcurrsqft"];
							$GrossSale=$_["nggrosssales"];
							$SalesEp=$_["ngsalesep"];
							$RentRev=$_["ngrentrev"];
						}	
						$TCurrentSqFt+=$CurrentSqFt;	
						$TGrossSale+=$GrossSale;
						$TSalesEp+=$SalesEp;
						$TRentRev+=$RentRev;				 
						$users[] = array('IATA_ID' => $airportinfo['IATA'],'Terminal_Abbr' => $AP["acity"],
						'Terminal_Name'=>$_["terminalname"],
						'CurrentSqFt'=>$CurrentSqFt,'GrossSales'=>$GrossSale,
						'SalesEP'=>$SalesEp,'GrossRentals'=>$RentRev);			
					} // END foreach ($tl as $_) 	
				}// END IF count($tl)>0
			/*******************************************************************/
			} // END foreach($airport as $AP)
			$songs = $this->common->subval_sort($users,'SalesEP'); 			 
			$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">
	<tr><td>&nbsp;</td>
    <tr>
    <th>IATA ID</th>
   	<th>City</th>
   	<th>Terminal Name</th>
	<th>EPAX</th>
	<th>Intl. EPAX</th>
    <th>Current Sq. Ft.</th>
    <th>Gross Sales</th>
	<th>Current Sq. Ft.</th>
    <th>Gross Sales</th>';		
	
	$tbl_header.='<th>Sales/EP</th>';
	
$tbl_header.='

  </tr><tr>&nbsp;</td></tr>';
  $counter=0;
	  for($i=count($songs)-1;$i>=0;$i--)
	  {
		$counter++; 
		  if($i%2 == 0)
		  {
			$class= '<tr>';
		  } else
		  {
			$class = '<tr>';
		  }
		  $tbl_header.=$class;
		   $tbl_header.='<td>'.$songs[$i]["IATA_ID"].'</td>
	
			<td>'.$songs[$i]["Terminal_Abbr"].'</td>
		
			<td>'.$songs[$i]["Terminal_Name"].'</td>
			<td>0</td>
			<td>0</td>
			<td>'.number_format($songs[$i]["CurrentSqFt"],2).'</td>
		
			<td>$'.number_format($songs[$i]["GrossSales"],2).'</td>';
			if($reportfor=='PT')
			{
				$tbl_header.='<td>$'.number_format($songs[$i]["SalesEP"]).'</td>';
			} else 
			{
				$tbl_header.='<td>$'.$songs[$i]["SalesEP"].'</td>';
			}
			$tbl_header.='</tr>';	
			if($counter==$this->limit)
			{
				break;
			}
	  }	  	
  		$tbl_header.='<tr><td>&nbsp;</td></tr>';
		$tbl_header.=$class.'<td><strong>Total</strong></td><td>&nbsp;</td><td>&nbsp;</td><td><strong>'.number_format($TCurrentSqFt,2).'</strong></td><td><strong>$'.number_format($TGrossSale,2).'</strong></td><td><strong>'.number_format($TSalesEp,2).'</strong></td><td><strong>$'.number_format($TRentRev,2).'</strong></td></tr>';
     	$tbl_header.='</table></div>';
		$str = preg_replace('/\s\s+/', ' ', $tbl_header);
		//$mpdf->WriteHTML($str);
		//$mpdf->defaultfooterline=0;
		//$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">'.date("Y-m-d H:i:s").'&nbsp;&nbsp;&copy; 2014 Airport Revenue News, A Division of Urban Expositions</div>');
		//$mpdf->Output();
		return $str;
		}
		public function allairport($ayear, $col, $aid = "")
		{
			$alldata=array();
			$cols = ($col == "")?array():explode(",",$this->reportmodel->getcols($col));
			if($aid != "")
			$aid = " WHERE airports.aid IN (".implode(",", $aid).")";
			$limit = ($this->limit != "")?" LIMIT 0,".$this->limit:"";
			$test="All Airports";
			$ay = array();
			if(!is_array($ayear))
				$ay[] = $ayear;
			else
				$ay = $ayear;
			foreach($ay as $year):
			$airportlist=$this->common->CustomQuery("SELECT * FROM airports $aid $limit");
			$airport_array = array();
			$outlets = array();
			$contacts = array();
			$terminals = array();	
			
			$tbl_header='<div class="alert alert-info"><h3>'.$test.'</h3></div>';
		
			foreach($airportlist as $airport):
				$terminals=$this->common->JoinTable('*','terminals','terminalsannual','terminalsannual.tid = terminals.tid','LEFT',array('aid'=>$airport['aid']));
				
				/*echo "<pre>";
                print_r($terminals);
		        echo "</pre>";
				 exit;*/
					
				$join_array = array(
				array('airports', 'airports.aid = airportcontacts.aid', 'LEFT'),
				array('lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT')	
			);				
				$outlets=$this->common->JoinTable('*','outlets','lkpcategory','outlets.categoryid=lkpcategory.categoryid','LEFT',"aid = ".$airport['aid']." AND lkpcategory.categoryid !='' ORDER BY lkpcategory.categoryid");
				$contacts=$this->common->JoinTables('*','airportcontacts',$join_array,array('airportcontacts.aid'=> $airport['aid']),"");
				$annual=$this->common->JoinTable('*','airportsannual','airports','airports.aid = airportsannual.aid','LEFT',array('airportsannual.aid'=>$airport['aid']));
				$tbl_header.='<div class="alert alert-success"><b><div>'.$airport['acity'].",".$airport['astate'].'</div>';
				$tbl_header.='<div>'.$airport['aname'].'</div>';
				$tbl_header.='<div>'.$airport['IATA'].'</div></b></div>';
				$tbl_header.=' <div class="panel-group tabs-table" id="accordion'.$airport['aid'].'" role="tablist" aria-multiselectable="true">';
				if(in_array('`contacts`',$cols)) 
				{
					
					$tbl_header.='<div class="panel panel-primary">';
					$tbl_header.='<div class="panel-heading" role="tab" id="contact'.$airport['aid'].'">
								  <h4 class="panel-title">
                                  <a class="collapsed" data-parent="#accordion'.$airport['aid'].'" data-toggle="collapse" href="#collapseOne'.$airport['aid'].'" aria-expanded="false" aria-controls="collapseTwo">
                                   Contact Info
                                    </a>
                                    </h4></div>';
					$tbl_header.='<div id="collapseOne'.$airport['aid'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contact"><div class="panel-body">';	
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					$tbl_header.='<tr><td colspan="'.(count($col)+5).'">';
					$tbl_header.='<div class="rows">';
					if(count($contacts)>0)
					{
						foreach($contacts as $contact):
							$tbl_header.='<div class="col-sm-6 col-md-6"><div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
							$tbl_header.='<tr class="back"><td colspan="'.(count($col)+3).'"><i class="fa fa-arrow-right"></i> <strong>'.$contact['mgtresponsibilityname'].'</strong></td></tr>';
							$tbl_header.='<tr><td colspan="'.count($col).'"><i class="fa fa-phone"></i> Contact</td><td colspan="'.(count($col)+5).'"><b>'.$contact['alname'].', '.$contact['afname'].'</b></td></tr>';	
							$tbl_header.='<tr><td colspan="'.count($col).'"><i class="fa fa-book"></i> Title</td><td colspan="'.(count($col)+4).'" style="white-space: nowrap;">'.$contact['atitle'].'</td></tr>';	
							$tbl_header.='<tr><td colspan="'.count($col).'" style="white-space: nowrap;"><i class="fa fa-building-o"></i> Company</td><td colspan="'.(count($col)+5).'">'.substr($contact['acompany'],0,20).'..</td></tr>';	
							$tbl_header.='<tr><td colspan="'.count($col).'"><i class="fa fa-home"></i> Address	</td><td colspan="'.(count($col)+5).'">'.$contact['aaddress1'].'</td></tr>';	
							
							$tbl_header.='<tr style="white-space: nowrap;"><td colspan="'.count($col).'">	</td>	<td colspan="'.(count($col)+5).'">'.$contact['accity'].' '.$contact['acstate'].' '.$contact['aczip'].' '.$contact['accountry'].'</td></tr>';
							$tbl_header.='<tr><td colspan="'.count($col).'"><i class="fa fa-phone"></i> Phone </td><td colspan="'.(count($col)+5).'">'.$contact['aphone'].' '.$contact['aext'].'</td></tr>';	
							$tbl_header.='<tr><td colspan="'.count($col).'"><i class="fa fa-fax"></i> Fax </td><td colspan="'.(count($col)+5).'">'.$contact['afax'].'</td></tr>';
							$tbl_header.='<tr><td colspan="'.count($col).'"><i class="fa fa-envelope-o"></i> Email </td><td colspan="'.(count($col)+5).'">	<a href="mailto:'.$contact['aemail'].'">'.$contact['aemail'].'</a></td></tr>';
							$tbl_header.='<tr><td colspan="'.count($col).'"><i class="fa fa-globe"></i>  Website </td><td colspan="'.(count($col)+5).'"><a href="http://'.$contact['awebsite'].'">'.$contact['awebsite'].'</a></td></tr>';
							$tbl_header.='</br>';
							$tbl_header.='</table></div></div>';
							
							$rc++;												
						endforeach;						
					}
					else{
						$tbl_header.='<div class="alert alert-danger">No record found</div>';
						} 				
					
					$tbl_header.='</table></div>';		
					$tbl_header.='</div></div>';	
					$tbl_header.='</div>';	
					
				}
				if(in_array('`terminal`',$cols))
				{	
					$tbl_header.='<div class="panel panel-primary">';
					$tbl_header.='<div class="panel-heading" role="tab" id="terminal'.$airport['aid'].'">
								   <h4 class="panel-title">
                                    <a data-parent="#accordion'.$airport['aid'].'" class="collapsed" data-toggle="collapse" href="#collapsetwo'.$airport['aid'].'" aria-expanded="false" aria-controls="collapseTwo">
                                     Terminal Info
                                    </a>
                                    </h4></div>';
					$tbl_header.='<div id="collapsetwo'.$airport['aid'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="terminal"><div class="panel-body">';
					
				$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
				$tbl_header.='<tr><td colspan="'.count($col).'">';
				$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
				$tbl_header.='<tr class="back"><td>
				<i class="fa fa-arrow-right"></i> <strong>Airport Info</strong>
				</td></tr>';	
				$tbl_header.='<tr><td colspan="'.count($col).'">Airport Configuration: '.$airport['configuration'].'</td></tr>';	
				$tbl_header.='<tr><td colspan="'.count($col).'">Concessions Mgt. Type'.$airport['mgtstructure'].'</td></tr>';
				$tbl_header.='<tr><td colspan="'.count($col).'">Expansion Planned: '.$airport['texpansionplanned'].'</td></tr>';	
				$tbl_header.='<tr><td colspan="'.count($col).'">Add Sq. Ft. '.$airport['addsqft'].'</td></tr>';
				$tbl_header.='<tr><td colspan="'.count($col).'">Completion Date: '.$airport['completedexpdate'].'</td></tr>';
				$tbl_header.='</table></div>';
				$tbl_header.='</td><td colspan="'.count($col).'">';
								
				$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
				$tbl_header.='<tr class="back">';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Terminal/Concourses</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Abbr.</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Dominant Airline</strong></td>';
				$tbl_header.='</tr>';	
						
				foreach($terminals as $terminal):
					$tbl_header.='<tr>';
					$tbl_header.='<td colspan="'.count($col).'">'.$terminal['terminalname'].'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.$terminal['terminalabbr'].'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.$terminal['tdominantairline'].'</td>';
					$tbl_header.='</tr>';
				endforeach;				
				$tbl_header.='</table></div>';
				$tbl_header.='</td></tr></table></div>';				
				
					$tbl_header.='</div></div>';	
					$tbl_header.='</div>';
				}
				
				if(in_array('`passengertraffic`',$cols))
				{	
					$tbl_header.='<div class="panel panel-primary">';
					$tbl_header.='<div class="panel-heading" role="tab" id="passengertraffic'.$airport['aid'].'">
								   <h4 class="panel-title">
                                    <a data-parent="#accordion'.$airport['aid'].'" class="collapsed" data-toggle="collapse" href="#collapsethree'.$airport['aid'].'" aria-expanded="false" aria-controls="collapseTwo">
                                    Passenger Traffic
                                    </a>
                                    </h4></div>';
					$tbl_header.='<div id="collapsethree'.$airport['aid'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="terminal"><div class="panel-body">';
					
				/////////////Passenger Trafic Graphs/////////////
				$tpasstraffic=array();
				$tpasstrafficcompare=array();
				$tdeplaning=array();
				$tenplaning=array();
				$tepdomestic=array();
				$tepintl=array();
				//////////////Concession Totals-Terminal Breakdowns Graphs(Food/Beverage)////////////
				$fbgrosssales=array();
				$fbsalesep=array();
				$fbrentrev=array();
				$fbrentep=array();
				$fbcurrsqft=array();
				//////////////Concession Totals-Terminal Breakdowns Graphs(Specialty Retail)////////////
				$srgrosssales=array();
				$srsalesep=array();
				$srrentrev=array();
				$srrentep=array();
				$srcurrsqft=array();
				//////////////Concession Totals-Terminal Breakdowns Graphs(News/Gifts Retail)////////////
				$nggrosssales=array();
				$ngsalesep=array();
				$ngrentrev=array();
				$ngrentep=array();
				$ngcurrsqft=array();
				//////////////Concession Totals-Terminal Breakdowns Graphs(Duty Free Retail)////////////
				$dfgrosssales=array();
				$dfsalesep=array();
				$dfrentrev=array();
				$dfrentep=array();
				$dfcurrsqft=array();
				$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
				$tbl_header.='<tr class="back">';
				$tbl_header.='<td colspan="'.(count($col)+40).'"><i class="fa fa-plane fa-spin">&nbsp;</i> <strong>Passenger Traffic</strong></td></tr>';
				$tbl_header.='<tr>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Terminal</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Total</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>+/-%</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Deplanning</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Enplanning</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>EP Domestic</strong></td>';
				$tbl_header.='<td colspan="'.count($col).'"><strong>Ep Int\'l</strong></td>';
				$tbl_header.='</tr>';
				$ttpasstraffic=0;
				$ttpasstrafficcompare=0;
				$ttadeplaning=0;
				$taenplaning=0;
				$ttaepdomestic=0;
				$tttaepintl=0;
				
				
				/////////////Passenger Trafic Graphs/////////////
				$tpasstraffic[]=array('terminalabbr', 'Passenger Traffic (Total)'.' '.$airport['aname']);
				$tpasstrafficcompare[]=array('terminalabbr', 'Passenger Traffic (+/-%)');
				$tdeplaning[]=array('terminalabbr', 'Passenger Traffic (Deplanning)');
				$tenplaning[]=array('terminalabbr', 'Passenger Traffic (Enplanning)');
				$tepdomestic[]=array('terminalabbr', 'Passenger Traffic (EP Domestic)');
				$tepintl[]=array('terminalabbr', 'Passenger Traffic (Ep Intl)');
				//////////////Concession Totals-Terminal Breakdowns Graphs(Food/Beverage)////////////
				$fbgrosssales[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Food/Beverage-Gross Sales)');
				$fbsalesep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Food/Beverage-Sales/EP)');
				$fbrentrev[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Food/Beverage-Rent Rev to Airport)');
				$fbrentep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Food/Beverage-Rent/EP)');
				$fbcurrsqft[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Food/Beverage-Current Sq. Ft.)');
				//////////////Concession Totals-Terminal Breakdowns Graphs(Specialty Retail)////////////
				$srgrosssales[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Specialty Retail-Gross Sales)');
				$srsalesep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Specialty Retail-Sales/EP)');
				$srrentrev[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Specialty Retail-Rent Rev to Airport)');
				$srrentep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Specialty Retail-Rent/EP)');
				$srcurrsqft[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Specialty Retail-Current Sq. Ft.)');
				//////////////Concession Totals-Terminal Breakdowns Graphs(News/Gifts Retail)////////////
				$nggrosssales[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(News/Gifts Retail-Gross Sales)');
				$ngsalesep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(News/Gifts Retail-Sales/EP)');
				$ngrentrev[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(News/Gifts Retail-Rent Rev to Airport)');
				$ngrentep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(News/Gifts Retail-Rent/EP)');
				$ngcurrsqft[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(News/Gifts Retail-Current Sq. Ft.)');
				//////////////Concession Totals-Terminal Breakdowns Graphs(Duty Free Retail)////////////
				$dfgrosssales[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Duty Free Retail-Gross Sales)');
				$dfsalesep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Duty Free Retail-Sales/EP)');
				$dfrentrev[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Duty Free Retail-Rent Rev to Airport)');
				$dfrentep[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Duty Free Retail-Rent/EP)');
				$dfcurrsqft[]=array('terminalabbr', 'Concession Totals-Terminal Breakdowns(Duty Free Retail-Current Sq. Ft.)');
				foreach($terminals as $term):
					/////////////Passenger Trafic Graphs/////////////
					$tpasstraffic[]=array($term['terminalabbr'],intval($term['tpasstraffic']));
					$tpasstrafficcompare[]=array($term['terminalabbr'],intval($term['tpasstrafficcompare']));
					$tdeplaning[]=array($term['terminalabbr'],intval($term['tdeplaning']));
					$tenplaning[]=array($term['terminalabbr'],intval($term['tenplaning']));
					$tepdomestic[]=array($term['terminalabbr'],intval($term['tepdomestic']));
					$tepintl[]=array($term['terminalabbr'],intval($term['tepintl']));
					//////////////Concession Totals-Terminal Breakdowns Graphs(Food/Beverage)////////////
					$fbgrosssales[]=array($term['terminalabbr'],intval($term['fbgrosssales']));
					$fbsalesep[]=array($term['terminalabbr'],intval($term['fbsalesep']));
					$fbrentrev[]=array($term['terminalabbr'],intval($term['fbrentrev']));
					$fbrentep[]=array($term['terminalabbr'],intval($term['fbrentep']));
					$fbcurrsqft[]=array($term['terminalabbr'],intval($term['fbcurrsqft']));
					//////////////Concession Totals-Terminal Breakdowns Graphs(Specialty Retail)////////////
					$srgrosssales[]=array($term['terminalabbr'],intval($term['srgrosssales']));
					$srsalesep[]=array($term['terminalabbr'],intval($term['srsalesep']));
					$srrentrev[]=array($term['terminalabbr'],intval($term['srrentrev']));
					$srrentep[]=array($term['terminalabbr'],intval($term['srrentep']));
					$srcurrsqft[]=array($term['terminalabbr'],intval($term['srcurrsqft']));
					//////////////Concession Totals-Terminal Breakdowns Graphs(News/Gifts Retail)////////////
					$nggrosssales[]=array($term['terminalabbr'],intval($term['nggrosssales']));
					$ngsalesep[]=array($term['terminalabbr'],intval($term['ngsalesep']));
					$ngrentrev[]=array($term['terminalabbr'],intval($term['ngrentrev']));
					$ngrentep[]=array($term['terminalabbr'],intval($term['ngrentep']));
					$ngcurrsqft[]=array($term['terminalabbr'],intval($term['ngcurrsqft']));
					//////////////Concession Totals-Terminal Breakdowns Graphs(Duty Free Retail)////////////
					$dfgrosssales[]=array($term['terminalabbr'],intval($term['dfgrosssales']));
					$dfsalesep[]=array($term['terminalabbr'],intval($term['dfsalesep']));
					$dfrentrev[]=array($term['terminalabbr'],intval($term['dfrentrev']));
					$dfrentep[]=array($term['terminalabbr'],intval($term['dfrentep']));
					$dfcurrsqft[]=array($term['terminalabbr'],intval($term['dfcurrsqft']));
				
				
					$tbl_header.='<tr>';
					$tbl_header.='<td colspan="'.count($col).'">'.$term['terminalabbr'].'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($term['tpasstraffic']).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($term['tpasstrafficcompare']).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($term['tdeplaning']).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($term['tenplaning']).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($term['tepdomestic']).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($term['tepintl']).'</td>';
					$tbl_header.='</tr>';
					$ttpasstraffic+=$term['tpasstraffic'];
					$ttpasstrafficcompare+=$term['tpasstrafficcompare'];
					$ttadeplaning+=$term['tdeplaning'];
					$taenplaning+=$term['tenplaning'];
					$ttaepdomestic+=$term['tepdomestic'];
					$tttaepintl+=$term['tepintl'];					
				endforeach;		
					/////////////Passenger Trafic Graphs/////////////
					$alldata[]=$tpasstraffic;
					$alldata[]=$tpasstrafficcompare;
					$alldata[]=$tdeplaning;
					$alldata[]=$tenplaning;
					$alldata[]=$tepdomestic;
					$alldata[]=$tepintl;
					//////////////Concession Totals-Terminal Breakdowns Graphs(Food/Beverage)////////////
					$alldata[]=$fbgrosssales;
					$alldata[]=$fbsalesep;
					$alldata[]=$fbrentrev;
					$alldata[]=$fbrentep;
					$alldata[]=$fbcurrsqft;
					//////////////Concession Totals-Terminal Breakdowns Graphs(Specialty Retail)////////////
					$alldata[]=$srgrosssales;
					$alldata[]=$srsalesep;
					$alldata[]=$srrentrev;
					$alldata[]=$srrentep;
					$alldata[]=$srcurrsqft;
					//////////////Concession Totals-Terminal Breakdowns Graphs(News/Gifts Retail)////////////
					$alldata[]=$nggrosssales;
					$alldata[]=$ngsalesep;
					$alldata[]=$ngrentrev;
					$alldata[]=$ngrentep;
					$alldata[]=$ngcurrsqft;
					//////////////Concession Totals-Terminal Breakdowns Graphs(Duty Free Retail)////////////
					$alldata[]=$dfgrosssales;
					$alldata[]=$dfsalesep;
					$alldata[]=$dfrentrev;
					$alldata[]=$dfrentep;
					$alldata[]=$dfcurrsqft;
					$this->outputData['alldata']=$alldata;
												
					$tbl_header.='<tr>';
					$tbl_header.='<td colspan="'.count($col).'"><b>Totals</b></td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($ttpasstraffic).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($ttpasstrafficcompare).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($ttadeplaning).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($taenplaning).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($ttaepdomestic).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($tttaepintl).'</td>';
					$tbl_header.='</tr>';			
				$tbl_header.='</table></div>';
				
				
					$tbl_header.='</div></div>';	
					$tbl_header.='</div>';
				} 
				if(in_array('`airportpercentages`',$cols))
				{
					$tbl_header.='<div class="panel panel-primary">';
					$tbl_header.='<div class="panel-heading" role="tab" id="airportpercentages'.$airport['aid'].'">
								   <h4 class="panel-title">
                                    <a data-parent="#accordion'.$airport['aid'].'" class="collapsed" data-toggle="collapse" href="#collapsefour'.$airport['aid'].'" aria-expanded="false" aria-controls="collapseTwo">
                                     Airport Percentages
                                    </a>
                                    </h4></div>';
					$tbl_header.='<div id="collapsefour'.$airport['aid'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="terminal"><div class="panel-body">';	
					
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					$tbl_header.='<tr class="back">';
					$tbl_header.='<td colspan="'.(count($col)+20).'"><i class="fa fa-arrow-right"></i> <strong>Airport Percentages</strong></td></tr>';
					
					$tbl_header.='<tr>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Pre/Post Security</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Business to Leisure Ratio</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>O&D Transfer</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Average Dwell Time</strong></td>';				
					$tbl_header.='</tr>';
					
					$pre = $annual[0]['presecurity'].'/'.$annual[0]['postsecurity'];
					$tbl_header.='<tr>';
					$tbl_header.='<td colspan="'.count($col).'">'.$pre.'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.$annual[0]['ratiobusleisure'].'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.$annual[0]['ondtransfer'].'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.$annual[0]['avgdwelltime'].'</td>';
					$tbl_header.='</tr>';
					
					$tbl_header.='</table></div>';
				
					$tbl_header.='</div></div>';	
					$tbl_header.='</div>';
				} 
				
				if(in_array('`airportwideinfo`',$cols)) 
				{
					$tbl_header.='<div class="panel panel-primary">';
					$tbl_header.='<div class="panel-heading" role="tab" id="airportwideinfo'.$airport['aid'].'">
								   <h4 class="panel-title">
                                    <a data-parent="#accordion'.$airport['aid'].'" class="collapsed" data-toggle="collapse" href="#collapsefive'.$airport['aid'].'" aria-expanded="false" aria-controls="collapseTwo">
                                     Airportwide Info
                                    </a>
                                    </h4></div>';
					$tbl_header.='<div id="collapsefive'.$airport['aid'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="terminal"><div class="panel-body">';	
					
				$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
				$tbl_header.='<tr class="back">';
				$tbl_header.='<td colspan="'.(count($col)+3).'"><i class="fa fa-info-circle"></i>  <strong>Airportwide Info</strong></td></tr>';
				
				$tbl_header.='<tr><td colspan="'.count($col).'">';
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					$tbl_header.='<tr><td colspan="'.count($col).'"><strong>Parking</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Short</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Long</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Economy</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Valet</strong></td></tr>';
												
					
					$tbl_header.='<tr><td colspan="'.count($col).'"><strong>Hourly</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['hourlyshort'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['hourlylong'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['hourlyeconomy'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['hourlyvalet'],2).'</td></tr>';
					
					$tbl_header.='<tr><td colspan="'.count($col).'"><strong>Daily</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['dailyshort'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['dailylong'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['dailyeconomy'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['dailyvalet'],2).'</td></tr>';;
					
					$tbl_header.='<tr><td colspan="'.count($col).'"><strong># Spaces</strong></td>';
					
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($annual[0]['spacesshort'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($annual[0]['spaceslong'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($annual[0]['spaceseconomy'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($annual[0]['spacesvalet'],2).'</td></tr>';
					
					
					$tbl_header.='<tr><td colspan="'.(count($col)+0).'"><strong>Parking Revenue </strong></td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['parkingrev'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'"><b>Total Spaces</b></td>';					
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($annual[0]['parkingspaces'],2).'</td><td>&nbsp;</td></tr>';
					
					$tbl_header.='</table></div>';			
				$tbl_header.='</td><td colspan="'.count($col).'">';				
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					
					$tbl_header.='<tr><td colspan="'.count($col).'"><strong>Car Rentals</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Agencies</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Gross Rev</strong></td>';
					$tbl_header.='<td colspan="'.count($col).'"><strong>Gross Renta</strong></td></tr>';												
											
					
					$tbl_header.='<tr><td colspan="'.count($col).'">Car Rental On Site</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.$annual[0]['carrentalagenciesonsite'].'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['carrentalrevonsite'],2).'</td>';					
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['carrentalrevtoaironsite'],2).'</td></tr>';
					
					$tbl_header.='<tr><td colspan="'.count($col).'">Car Rental Off Site</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.$annual[0]['carrentalagenciesoffsite'].'</td>';
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['carrentalrevoffsite'],2).'</td>';					
					$tbl_header.='<td colspan="'.count($col).'">$'.@number_format($annual[0]['carrentalrevtoairoffsite'],2).'</td></tr>';
					
					$tbl_header.='<tr><td colspan="'.count($col).'">Total Cars Rented</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($annual[0]['totalcarsrented'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">&nbsp;</td>';					
					$tbl_header.='<td colspan="'.count($col).'">0</td></tr>';
						
					$tbl_header.='<tr><td colspan="'.count($col).'">Car Rental Sq. Ft.</td>';
					$tbl_header.='<td colspan="'.count($col).'">'.@number_format($annual[0]['carrentalsqft'],2).'</td>';
					$tbl_header.='<td colspan="'.count($col).'">&nbsp;</td>';					
					$tbl_header.='<td colspan="'.count($col).'"></td></tr>';
					$tbl_header.='</table></div>';					
					$tbl_header.='</td></tr>';
					$tbl_header.='</table></div>';
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					$tbl_header.='<tr><td colspan="'.count($col).'">';
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					$tbl_header.='<tr><td colspan="'.count($col).'">&nbsp;<td colspan="'.count($col).'"><b>Revenue</b></td><td colspan="'.count($col).'"><b>Rev. to Airport</b></td></tr>';
					$tbl_header.='<tr><td colspan="'.count($col).'"><b>Passenger Services</b></td><td colspan="'.count($col).'">$'.@number_format($annual[0]['passservicesrev'],2).'</td><td colspan="'.count($col).'">$'.@number_format($annual[0]['passservicesrevtoair'],2).'</td></tr>';
					$tbl_header.='<tr><td colspan="'.count($col).'"><b>Advertising</b></td><td colspan="'.count($col).'">$'.@number_format($annual[0]['advertisingrev'],2).'</td><td colspan="'.count($col).'">$'.@number_format($annual[0]['advertisingrevtoair'],2).'</td></tr>';
					$tbl_header.='<tr><td colspan="'.count($col).'"><b>Currency Exchange</b></td><td colspan="'.count($col).'">$'.@number_format($annual[0]['currencyexrev'],2).'</td><td colspan="'.count($col).'">$'.@number_format($annual[0]['currencyexrevtoair'],2).'</td></tr>';					
					$tbl_header.='</table></div>';	
					
					$tbl_header.='</table></div>';
				
					$tbl_header.='</div></div>';	
					$tbl_header.='</div>';
				} 
				
				if(in_array('`concessiontenantdetails`',$cols))  
				{
					$tbl_header.='<div class="panel panel-primary">';
					$tbl_header.='<div class="panel-heading" role="tab" id="concessiontenantdetails'.$airport['aid'].'">
								   <h4 class="panel-title">
                                    <a data-parent="#accordion'.$airport['aid'].'" class="collapsed" data-toggle="collapse" href="#collapsesix'.$airport['aid'].'" aria-expanded="false" aria-controls="collapseTwo">
                                     Concession Tenant Details
                                    </a>
                                    </h4></div>';
					$tbl_header.='<div id="collapsesix'.$airport['aid'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="terminal"><div class="panel-body">';
					 
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
						
						$tbl_header.='<tr class="back"><td colspan="'.count($col).'"><b>Concession Totals  ‐ Terminal Breakdowns</b>&nbsp;&nbsp;&nbsp;&nbsp; <b>( Food/Beverage, Specialty Retail,  News /Gifts Only )</b></td></tr>';
						
					$tbl_header.='</table></div>';					
					list($t1, $r['fb']) = $this->common->terminalannual2($terminals,'Food/Beverage','fb', true);
					$tbl_header .= $t1;	
					list($t2, $r['sr']) = $this->common->terminalannual2($terminals,'Specialty Retail','sr', true);
					$tbl_header .= $t2;
					list($t3, $r['ng']) = $this->common->terminalannual2($terminals,'News/Gifts Retail','ng', true);
					$tbl_header .= $t3;
					list($t4, $r['df']) = $this->common->terminalannual2($terminals,'Duty Free Retail','df', true);
					$tbl_header .= $t4;
					
					$grosssales = 0;
					$salesep = 0;
					$rentrev = 0;
					$rentep = 0;
					$currsqft = 0;
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					
					$tbl_header.='<tr>
									<td colspan="'.count($col).'"><b>Terminal Totals</b></td>
									<td colspan="'.count($col).'"><b>Gross Sales</b></td>
									<td colspan="'.count($col).'"><b>Sales/EP</b></td>
									<td colspan="'.count($col).'"><b>Rent Rev to Airport</b></td>
									<td colspan="'.count($col).'"><b>Rent/EP</b></td>
									<td colspan="'.count($col).'"><b>Current Sq. Ft.</b></td>
								</tr>';					
					
					$terms = array();
					foreach($r as $i=>$termin):
						if($i != 'df')
						{
							$grosssales += $termin[1];
							$salesep += $termin[2];
							$rentrev += $termin[3];
							$rentep += $termin[4];
							$currsqft += $termin[5];
						}
						if(!in_array($i, $terms))
						{
							$terms[] = $i;
							$tbl_header.='<tr>
										<td colspan="'.count($col).'">'.$termin[0].'</td>
										<td colspan="'.count($col).'">$'.@number_format($termin[1], 2).'</td>
										<td colspan="'.count($col).'">$'.@number_format($termin[2], 2).'</td>
										<td colspan="'.count($col).'">$'.@number_format($termin[3], 2).'</td>
										<td colspan="'.count($col).'">$'.@number_format($termin[4], 2).'</td>
										<td colspan="'.count($col).'">'.@number_format($termin[5], 2).'</td>
									</tr>';
						}
					endforeach;
					$enp = 0;
					foreach($terminals as $term)
						$enp += $term['tenplaning'];
						
					
					$tbl_header.='<tr>
						<td colspan="'.count($col).'"><b>Total:<br>(Excludes Duty Free)</b></td>
						<td colspan="'.count($col).'">$'.@number_format($grosssales, 2).'</td>
						<td colspan="'.count($col).'">$'.@number_format(($grosssales != 0)?$grosssales/$enp:0, 2).'</td>
						<td colspan="'.count($col).'">$'.@number_format($rentrev, 2).'</td>
						<td colspan="'.count($col).'">$'.@number_format(($rentrev != 0)?$rentrev/$enp:0, 2).'</td>
						<td colspan="'.count($col).'">'.@number_format($currsqft, 2).'</td></tr>';	
					
					$tbl_header.='</table></div>';
					
					
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					
					$tbl_header.='<tr class="back"><td colspan="'.count($col).'"><b>Concession Tenant Details (2010)</b></td></tr>';
					
					$tbl_header.='</table></div>';
					$old='Food/Beverage';
					$c=0;
					$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
					foreach($outlets as $o):
						
						if($old!=$o['category'] && $c!=0)
						{	
							$tbl_header.='<tr><td colspan="'.count($col).'"><b>'.$o['category'].' Tenant (Company)</b></td><td colspan="'.count($col).'"><b>Product Description</b></td><td colspan="'.count($col).'"><b>Terminal</b></td><td colspan="'.count($col).'"><b># locations</b></td><td colspan="'.count($col).'"><b># Sq. Ft.</b></td><td colspan="'.count($col).'"><b>Expires</b></tr>';
							
							$old=$o['category'];
						}
						if($c==0)
						{							
							
							$tbl_header.='<tr><td colspan="'.count($col).'"><b>Food/Beverage Tenant (Company)</b></td><td colspan="'.count($col).'"><b>Product Description</b></td><td colspan="'.count($col).'"><b>Terminal</b></td><td colspan="'.count($col).'"><b># locations</b></td><td colspan="'.count($col).'"><b># Sq. Ft.</b></td><td colspan="'.count($col).'"><b>Expires</b></tr>';
							
						}
						$tbl_header.='<tr>
						<td colspan="'.count($col).'">'.$o['outletname'].'</td>
						<td colspan="'.count($col).'">'.$o['productdescription'].'</td>
						<td colspan="'.count($col).'">'.$o['termlocation'].'</td>
						<td colspan="'.count($col).'">'.$o['numlocations'].'</td>
						<td colspan="'.count($col).'">'.$o['sqft'].'</td>';
						if($o['exp']!='') 
						{
							$tbl_header.='<td colspan="'.count($col).'">'.date('m/d/Y',strtotime($o['exp'])).'</td></tr>';
						}else
							{
								$tbl_header.='<td colspan="'.count($col).'">---</td></tr>';
							}
						$c++;
						
					endforeach;	
					$tbl_header.='</table></div>';
					$tbl_header.='</div></div>';
					$tbl_header.='</div>';	
				}
					
				$tbl_header.='</div>';	
				
					$tbl_header.='</table>';
						
			endforeach; 
			endforeach;
			
			
			$str = preg_replace('/\s\s+/', ' ', $tbl_header);
			return $str;		
		}
		public function ratioreport($year,$col)
		{
			$presecurity=array();
			$postsecurity=array();
			$ratiobusleisure=array();
			$ondtransfer=array();
			$avgdwelltime=array();
			$parkingspaces=array();
			$parkingrev=array();
			$parkingrevtoair=array();
			
			$cols=$this->reportmodel->getcols($col);
			$airport=$this->common->CustomQueryALL('SELECT DISTINCT(`IATA`),`aid` as aid,`acity` FROM `airports`');
			$ratio="Ratio Report";
			$val=$year.'Ratios Report';
			$tbl_header.='<div class="alert alert-info"><h3>'.$ratio.'</div></h3>';
			$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
			$tbl_header.='<tr class="back">';
			foreach($col as $key => $val):				
				$tbl_header.='<th width="'.round(100/count($col)).'">'.$this->reportmodel->coltitle($key).'</th>';		    	
			endforeach;
			$tbl_header.='</tr>';
			//$tbl_header.='<tr><td colspan="'.count($col).'"></td></tr>';
			$rcounter=0;
			
			$alldata=array();
			$presecurity[]=array('IATA', 'Pre Security');
			$postsecurity[]=array('IATA', 'Post Security');
			$ratiobusleisure[]=array('IATA', 'Ratio Bus Leisure');
			$ondtransfer[]=array('IATA', 'On Demand Transfer');
			$avgdwelltime[]=array('IATA', 'Avg. Dwell Time');
			$parkingspaces[]=array('IATA', 'Parking Spaces');
			$parkingrev[]=array('IATA', 'Parking Rev');
			$parkingrevtoair[]=array('IATA', 'Parking Rev to Air');	
			
			foreach($airport as $AP):
			$join_array2 = array(
				array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT')							
			);
			$select=$cols;
			$where=array('airports.aid'=>$AP['aid'],'airportsannual.ayear'=>$year);
			$info=$this->common->JoinTables($select,'airports',$join_array2,$where,"airports.aid");	
			//////////////	
			
				
			foreach($info as $in)
			{
				$presecurity[]=array($in['IATA'],intval($in['presecurity']));
				$postsecurity[]=array($in['IATA'],intval($in['postsecurity']));
				$ratiobusleisure[]=array($in['IATA'],intval($in['ratiobusleisure']));
				$ondtransfer[]=array($in['IATA'],intval($in['ondtransfer']));
				$avgdwelltime[]=array($in['IATA'],intval($in['avgdwelltime']));
				$parkingspaces[]=array($in['IATA'],intval($in['parkingspaces']));
				$parkingrev[]=array($in['IATA'],intval($in['parkingrev']));
				$parkingrevtoair[]=array($in['IATA'],intval($in['parkingrevtoair']));
				
				if($rcounter%2 == 0)
					{
						 $class= '<tr>'; 
					} else 
					{
						$class = '<tr>';
					}
				$tbl_header.=$class;
				foreach($col as $key => $val):
					$tbl_header.='<td>'.$this->reportmodel->is_numericcheck($key,$in[$key]).'</td>';
				endforeach;
				$tbl_header.='</tr>';								
				$rcounter++;
			}
			
			
			endforeach;
			$alldata[]=$presecurity;
			$alldata[]=$postsecurity;
			$alldata[]=$ratiobusleisure;
			$alldata[]=$ondtransfer;
			$alldata[]=$avgdwelltime;
			$alldata[]=$parkingspaces;
			$alldata[]=$parkingrev;
			$alldata[]=$parkingrevtoair;
			$this->outputData['alldata']=$alldata;
			
			
			$tbl_header.='</table></div>';
			//$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">'.date("Y-m-d H:i:s").' <i>Airport Revenue News.</i></div>');
			$str = preg_replace('/\s\s+/', ' ', $tbl_header);
			//$mpdf->WriteHTML($str);	
			//$mpdf->Output();	
			return $str;		
		}
		public function top50terminals($reportfor,$year,$col)
		{
			$tpasstraffic=array();
			$tpasstrafficcompare=array();
			$tenplaning=array();
			$tdeplaning=array();
			$tepdomestic=array();
			$tepintl=array();
			$tconcessiongrosssales=array();
			$ctsalesep=array();
			$ctrentrev=array();
			$ctrentep=array();
			$ctcurrsqft=array();
			$tdominantairline=array();
			$fbgrosssales=array();
			$fbsalesep=array();
			$fbrentrev=array();
			$fbrentep=array();
			$fbcurrsqft=array();
			$srgrosssales=array();
			$srsalesep=array();
			$srrentrev=array();
			$srrentep=array();
			$srcurrsqft=array();
			$nggrosssales=array();
			$ngsalesep=array();
			$ngrentrev=array();
			$ngrentep=array();
			$ngcurrsqft=array();
			$dfgrosssales=array();
			$dfsalesep=array();
			$dfrentrev=array();
			$dfrentep=array();
			$dfcurrsqft=array();
			
			
			$cols=$this->reportmodel->getcols($col);
			if($reportfor=='SR')
			{
				$reportfor='SR';
				$reptitle="Specialty Retail";	
				$orderby="srsalesep";
			}
			if($reportfor=='DF')
			{
				$reportfor='DF';
				$reptitle="Duty Free";
				$orderby="dfsalesep";
			}
			if($reportfor=='NG')
			{
				$reportfor='NG';
				$reptitle="News & Gifts";
				$orderby="ngsalesep";
			}
			if($reportfor=='FB')
			{
				$reportfor='FB';
				$reptitle="Food and Beverage";
				$orderby="fbsalesep";
			}
			if($reportfor=='PT')
			{
				$reportfor='PT';
				$reptitle="Passenger Traffic";
				$orderby="ptsalesep";
			}
			if($reportfor=="AD")
			{
				$reptitle='Advertising';
			}			
			
			$airport=$this->common->CustomQueryALL('SELECT DISTINCT(`IATA`),`aid` as aid,`acity` FROM `airports`');
			
			$val=$year.' Top '.$this->limit.' Terminals by '.$reptitle;
			$tbl = '';
			$tbl2 = '';
			$tbl_header='<div class="alert alert-info"><h3>'.$reptitle.'</h3></div>';			
			$users = array();			
			foreach ($airport as $AP)
			{
				$airportinfo=$this->common->GetSingleRowFromTableWhere('airports',array('aid'=>$AP['aid']));
					
				$tl=$this->common->JoinTable($cols,'terminals,airports','terminalsannual',
				'terminalsannual.tid=terminals.tid','LEFT',"terminals.aid = '".$AP['aid']."' AND terminalsannual.tyear='".$year."' ORDER BY terminals.tid limit 10");				
				if(count($tl)>0)
				{					
					foreach ($tl as $_) 
					{											 
						$users[] = $_;							
					} // END foreach ($tl as $_) 
				}// END IF count($tl)>0			
			} // END foreach($airport as $AP)						
			$songs = $this->common->subval_sort($users,$orderby); 			 
			$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">
		    <tr class="back">';
			
		    foreach($col as $key => $val):
			 		
			$tbl_header.='<th width='.round(100/count($col)).'>'.$this->reportmodel->coltitle($key).'</th>';		    	
			endforeach;			
			$tbl_header.='</tr>';				
			//$tbl_header.='<tr><td colspan="'.count($col).'">&nbsp;</td></tr>';
  	  		$counter=0;
			$alldata=array();
			$tpasstraffic[]=array('IATA', 'Passenger Traffic');
			$tpasstrafficcompare[]=array('IATA', 'Passenger Traffic Comparison');
			$tenplaning[]=array('IATA', 'Enplaning');
			$tdeplaning[]=array('IATA', 'Deplaning');
			$tepdomestic[]=array('IATA', 'EP Domestic');
			$tepintl[]=array('IATA', 'EP Intl');
			$tconcessiongrosssales[]=array('IATA', 'Concession Gross Sales');
			$ctsalesep[]=array('IATA', 'Sales EP');
			$ctrentrev[]=array('IATA', 'Rent Rev');
			$ctrentep[]=array('IATA', 'Rent EP');
			$ctcurrsqft[]=array('IATA', 'Current Sq. Ft.');
			$tdominantairline[]=array('IATA', 'Domain Air');
			$fbgrosssales[]=array('IATA', 'Food and Beverage Gross Sales');
			$fbsalesep[]=array('IATA', 'Food and Beverage Sales EP');
			$fbrentrev[]=array('IATA', 'Food and Beverage Rent Rev');
			$fbrentep[]=array('IATA', 'Food and Beverage Rent EP');
			$fbcurrsqft[]=array('IATA', 'Food and Beverage Current Sq.Ft.');
			$srgrosssales[]=array('IATA', 'Specialty Retail Gross Sales');
			$srsalesep[]=array('IATA', 'Specialty Retail Sales EP');
			$srrentrev[]=array('IATA', 'Specialty Retail Rent Rev');
			$srrentep[]=array('IATA', 'Specialty Retail Rent EP');
			$srcurrsqft[]=array('IATA', 'Specialty Retail Current Sq.Ft.');
			$nggrosssales[]=array('IATA', 'News and Gifts Gross Sales');
			$ngsalesep[]=array('IATA', 'News and Gifts Sales EP');
			$ngrentrev[]=array('IATA', 'News and Gifts Rent Rev');
			$ngrentep[]=array('IATA', 'News and Gifts Rent EP');
			$ngcurrsqft[]=array('IATA', 'News and Gifts Current Sq.Ft.');
			$dfgrosssales[]=array('IATA', 'Duty Free Gross Sales');
			$dfsalesep[]=array('IATA', 'Duty Free Sales EP');
			$dfrentrev[]=array('IATA', 'Duty Free Rent Rev');
			$dfrentep[]=array('IATA', 'Duty Free Rent EP');
			$dfcurrsqft[]=array('IATA', 'Duty Free Current Sq.Ft.');
				
			  for($i=count($songs)-1;$i>=0;$i--)
			  {
				$tpasstraffic[]=array($songs[$i]['IATA'],intval($songs[$i]['tpasstraffic']));
				$tpasstrafficcompare[]=array($songs[$i]['IATA'],intval($songs[$i]['tpasstrafficcompare']));
				$tenplaning[]=array($songs[$i]['IATA'],intval($songs[$i]['tenplaning']));
				$tdeplaning[]=array($songs[$i]['IATA'],intval($songs[$i]['tdeplaning']));
				$tepdomestic[]=array($songs[$i]['IATA'],intval($songs[$i]['tepdomestic']));
				$tepintl[]=array($songs[$i]['IATA'],intval($airportlist['tepintl']));
				$tconcessiongrosssales[]=array($songs[$i]['IATA'],intval($songs[$i]['tconcessiongrosssales']));
				$ctsalesep[]=array($songs[$i]['IATA'],intval($songs[$i]['ctsalesep']));
				$ctrentrev[]=array($songs[$i]['IATA'],intval($songs[$i]['ctrentrev']));
				$ctrentep[]=array($songs[$i]['IATA'],intval($songs[$i]['ctrentep']));
				$ctcurrsqft[]=array($songs[$i]['IATA'],intval($songs[$i]['ctcurrsqft']));
				$tdominantairline[]=array($songs[$i]['IATA'],intval($songs[$i]['tdominantairline']));
				$fbgrosssales[]=array($songs[$i]['IATA'],intval($songs[$i]['fbgrosssales']));
				$fbsalesep[]=array($songs[$i]['IATA'],intval($songs[$i]['fbsalesep']));
				$fbrentrev[]=array($songs[$i]['IATA'],intval($songs[$i]['fbrentrev']));
				$fbrentep[]=array($songs[$i]['IATA'],intval($songs[$i]['fbrentep']));
				$fbcurrsqft[]=array($songs[$i]['IATA'],intval($songs[$i]['fbcurrsqft']));
				$srgrosssales[]=array($songs[$i]['IATA'],intval($songs[$i]['srgrosssales']));
				$srsalesep[]=array($songs[$i]['IATA'],intval($songs[$i]['srsalesep']));
				$srrentrev[]=array($songs[$i]['IATA'],intval($songs[$i]['srrentrev']));
				$srrentep[]=array($songs[$i]['IATA'],intval($songs[$i]['srrentep']));
				$srcurrsqft[]=array($songs[$i]['IATA'],intval($songs[$i]['srcurrsqft']));
				$nggrosssales[]=array($songs[$i]['IATA'],intval($songs[$i]['nggrosssales']));
				$ngsalesep[]=array($songs[$i]['IATA'],intval($songs[$i]['ngsalesep']));
				$ngrentrev[]=array($songs[$i]['IATA'],intval($songs[$i]['ngrentrev']));
				$ngrentep[]=array($songs[$i]['IATA'],intval($songs[$i]['ngrentep']));
				$ngcurrsqft[]=array($songs[$i]['IATA'],intval($songs[$i]['ngcurrsqft']));
				$dfgrosssales[]=array($songs[$i]['IATA'],intval($songs[$i]['dfgrosssales']));
				$dfsalesep[]=array($songs[$i]['IATA'],intval($songs[$i]['dfsalesep']));
				$dfrentrev[]=array($songs[$i]['IATA'],intval($songs[$i]['dfrentrev']));
				$dfrentep[]=array($songs[$i]['IATA'],intval($songs[$i]['dfrentep']));
				$dfcurrsqft[]=array($songs[$i]['IATA'],intval($songs[$i]['dfcurrsqft']));
				  
				$counter++; 
				  if($i%2 == 0)
				  {
					$class= '<tr>';
				  } else
				  {
					$class = '<tr>';
				  }
				   	$tbl_header.=$class;
				  	foreach($col as $key => $val): //$songs[$i][$key]
					
						$tbl_header.='<td>'.$this->reportmodel->is_numericcheck($key,$songs[$i][$key]).'</td>';
						
					endforeach;
					$tbl_header.='</tr>';	
				if($counter==$this->limit)
				{
					break;
				}
			  }	 
			  
			    $alldata[]=$tpasstraffic;
				$alldata[]=$tpasstrafficcompare;
				$alldata[]=$tenplaning;
				$alldata[]=$tdeplaning;
				$alldata[]=$tepdomestic;
				$alldata[]=$tepintl;
				$alldata[]=$tconcessiongrosssales;
				$alldata[]=$ctsalesep;
				$alldata[]=$ctrentrev;
				$alldata[]=$ctrentep;
				$alldata[]=$ctcurrsqft;
				$alldata[]=$tdominantairline;
				$alldata[]=$fbgrosssales;
				$alldata[]=$fbsalesep;
				$alldata[]=$fbrentrev;
				$alldata[]=$fbrentep;
				$alldata[]=$fbcurrsqft;
				$alldata[]=$srgrosssales;
				$alldata[]=$srsalesep;
				$alldata[]=$srrentrev;
				$alldata[]=$srrentep;
				$alldata[]=$srcurrsqft;
				$alldata[]=$nggrosssales;
				$alldata[]=$ngsalesep;
				$alldata[]=$ngrentrev;
				$alldata[]=$ngrentep;
				$alldata[]=$ngcurrsqft;
				$alldata[]=$dfgrosssales;
				$alldata[]=$dfsalesep;
				$alldata[]=$dfrentrev;
				$alldata[]=$dfrentep;
				$alldata[]=$dfcurrsqft;
			  	$this->outputData['alldata']=$alldata;
  		$tbl_header.='<tr><td colspan="'.count($col).'">&nbsp;</td></tr>';		
     	$tbl_header.='</table></div>';
		$str = preg_replace('/\s\s+/', ' ', $tbl_header);
		
		//$mpdf->WriteHTML($str);
		//$mpdf->defaultfooterline=0;
		//$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">'.date("Y-m-d H:i:s").'&nbsp;&nbsp;&copy; 2014 Airport Revenue News, A Division of Urban Expositions</div>');
		//$mpdf->Output();
		
		
		return $str; 
		}
		public function leaseexpire($year,$type,$col)
		{	
			$cols=$this->reportmodel->getcols($col);		
			$resourcearray = array();
			$indexarray = array();
			$lease= "Lease Expire";
			$tbl_header='<div class="alert alert-info"><h3>'.$lease.'</h3></div>';
			$rcounter=0;
			$class="";
			$val='Leases Due to Expire by Year-End '.$year; 
			$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
		    $listsql = "select distinct category from lkpcategory";
		    $listsql2="SELECT count(oid) FROM `outlets` ";
			$list=$this->common->CustomQueryALL($listsql);
			$listcount=$this->common->CustomCountQuery($listsql);
			$nr_of_users=$this->common->CustomQueryALL($listsql2);
			$alldata=array();			
			foreach($list as $cmpnyterms):
				
				$sqft=array();
				$resourcearray=array();
				$catid=$cmpnyterms['category'];						
				$ref=trim($year);				
				$products = $this->common->CustomQueryALL("select DATE_FORMAT(exp,'%m-%d-%y') as expf,".$cols.",`exp` from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='".$catid."' and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and outlets.exp LIKE '%".$ref."%' order by DATE_FORMAT(exp,'%m-%d-%y') ASC");
				
				$tbl_header.='<tr class="back">
				<td><h4>'.$catid.'</h4></td><td colspan="'.(count($col)+50).'">'.date("Y-m-d H:i:s").'</td></tr>';
				$tbl_header.='<tr>';		
				
				foreach($col as $key => $val):
				
					$tbl_header.='<th  colspan="'.(count($col)+1).'">'.$this->reportmodel->coltitle($key).'</th>';
					
				endforeach;					
				$tbl_header.='</tr>';
				$sqft[0]=array('outletname', $catid." ( Sqft )");														
				foreach($products as $prod):	
												
					$sqft[]=array($prod['outletname'],intval($prod['sqft']));
					$aiddd=$prod['aid'];			
					$citystrSQL = "SELECT acity FROM airports WHERE aid='".$aiddd."'";							
					$acity_row=$this->common->CustomQueryROw($citystrSQL);
					 if(!$this->common->pdf_check_date($prod['exp']))
					 {  
					 	$date_val=date('m/d/Y', strtotime($prod['exp']));
				     } elseif(strtotime($prod['exp']) =='')
					 {			
						$date_val="00/00/0000";			
					 }elseif(is_null($prod['exp']))
					 {
        				$date_val="00/00/0000";
			        }elseif($prod['exp']=='0000-00-00')
        			{
				        $date_val="00/00/0000";
			        }
					else
					{
						$date_val=date('m/d/Y', strtotime($prod['exp'])); 
					}
					foreach($col as $key => $val):
						$nomi[$key]=$prod[$key];						
					endforeach;
					array_push($resourcearray, $nomi);
				    array_push($indexarray, $date);					
					endforeach;// $prod			
				
				$alldata[]=$sqft;														
				if(count($resourcearray)>0) {
					foreach($resourcearray as $resource) 
					{
						$tbl_header.='<tr>';					
						foreach($col as $key => $val):
							$tbl_header.='<td colspan="'.(count($col)+1).'">'.$resource[$key].'</td>';									
						endforeach;					
						$tbl_header.='</tr>';			
					}
				} else
				{
						$tbl_header.='<tr>';											
						$tbl_header.='<td colspan="'.(count($col)+50).'">';
						$tbl_header.='<br/><div class="alert alert-danger">No result found...</div>';
						$tbl_header.='</td>';														
						$tbl_header.='</tr>';
				}
				
			$this->outputData['alldata']=$alldata;
			endforeach;						
			$tbl_header.='</table></div>';						
			$str = preg_replace('/\s\s+/', ' ', $tbl_header);			
			return $str;
		}
		public function top50airport($year,$type,$col)
		{	
			$passenger=array();
			$apasstrafficcompare=array();
			$enplan=array();
			$deplan=array();
			$aepdomestic=array();
			$aepintl=array();
			$aconcessiongrosssales=array();
			$aconcessiongrosssalesdf=array();
			$asalesep=array();
			$asalesepdf=array();
			$arentrev=array();
			$arentrevdf=array();
			$arentep=array();
			$arentepdf=array();
			$acurrsqft=array();
			$acurrsqftdf=array();
			$afbgrosssales=array();
			$afbsalesep=array();
			$afbrentrev=array();
			$afbrentep=array();
			$afbcurrsqft=array();
			$asrgrosssales=array();
			$asrsalesep=array();
			$asrrentrev=array();
			$asrrentep=array();
			$asrcurrsqft=array();
			if($type=='T50A')
			{
				$top = "Top 50 Airports";
			}
			else
			{
				$top = "Top Int Airports";
			}
			$cols=$this->reportmodel->getcols($col);					
			$tbl_header='<div class="alert alert-info"><h3>'.$top.'</h3></div>';
			$rcounter=0;
			$class="";
			$sql="SELECT ".$cols." FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airporttotals.ayear='".$year."' AND airports.IATA!='YYJ' AND airports.IATA!='YVR'  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC";
			$val=$year.' Top '.$this->limit.' Performing North American Airports';			
			if($type=='TIA')
			{
				$sql="SELECT ".$cols.",((afbgrosssales+asrgrosssales+anggrosssales+adfgrosssales)/aenplaning) myvar
				FROM airporttotals
				INNER JOIN airports ON airporttotals.aid = airports.aid
				WHERE airporttotals.adfgrosssales!=0 AND airporttotals.ayear='".$year."' OR airports.IATA='YVR' OR airports.IATA='YYJ'
				ORDER BY CAST(myvar AS DECIMAL( 28, 4 ) ) DESC LIMIT 0,".$this->limit;
				$val=$year.' International Airports by Performance';	
			}					
			$ap_count=$this->common->CustomCountQuery($sql);
			$res=$this->common->CustomQueryALL($sql);								
			
			if($ap_count>0)
			{	
				if($type=="TIA")
				{
					$tbl_header.='<div class="alert alert-success"><b>Includes duty free sales</b></div>';
				} /*else
				{
					$tbl_header.='<div class="table-responsive"><table class="table table-condensed table-striped></table></div>';
				}*/
				$tbl_header.='<div class="table-responsive"><table class="table table-bordered table-hover table-striped">';
				$tbl_header.='<tr class="back">';				
				foreach($col as $key => $val):				
					$tbl_header.='<th  width="'.round(100/count($col)).'%">'.$this->reportmodel->coltitle($key).'</th>';		    	
				
				endforeach;									
				$tbl_header.='</tr>';
				$sumarray=array();	
				$alldata=array();
				$passenger[]=array('IATA', 'Passenger Traffic');
				$apasstrafficcompare[]=array('IATA', 'Passenger Traffic Comparison');
				$enplan[]=array('IATA', 'Enplaning');
				$deplan[]=array('IATA', 'Deplaning');
				$aepdomestic[]=array('IATA', 'EP Domestic');
				$aepintl[]=array('IATA', 'EP Intl');
				$aconcessiongrosssales[]=array('IATA', 'Concession Gross Sales');
				$aconcessiongrosssalesdf[]=array('IATA', 'Concession Gross Sales-DF');
				$asalesep[]=array('IATA', 'Sales EP');
				$asalesepdf[]=array('IATA', 'Sales EP-DF');
				$arentrev[]=array('IATA', 'Rent Rev');
				$arentrevdf[]=array('IATA', 'Rent Rev-DF');
				$arentep[]=array('IATA', 'Rent EP');
				$arentepdf[]=array('IATA', 'Rent EP-DF');
				$acurrsqft[]=array('IATA', 'Current Sq. Ft');
				$acurrsqftdf[]=array('IATA', 'Current Sq. Ft-DF');
				$afbgrosssales[]=array('IATA', 'Food and Beverage Gross Sales');
				$afbsalesep[]=array('IATA', 'Food and Beverage Sales EP');
				$afbrentrev[]=array('IATA', 'Food and Beverage Rent Rev');
				$afbrentep[]=array('IATA', 'Food and Beverage Rent EP');
				$afbcurrsqft[]=array('IATA', 'Food and Beverage Current Sq.Ft.');
				$asrgrosssales[]=array('IATA', 'Specialty Retail Gross Sales');
				$asrsalesep[]=array('IATA', 'Specialty Retail Sales EP');
				$asrrentrev[]=array('IATA', 'Specialty Retail Rent Rev');
				$asrrentep[]=array('IATA', 'Specialty Retail Rent EP');
				$asrcurrsqft[]=array('IATA', 'Specialty Retail Current Sq.Ft.');
				
				foreach($res as $airportlist):															
					$passenger[]=array($airportlist['IATA'],intval($airportlist['apasstraffic']));
					$apasstrafficcompare[]=array($airportlist['IATA'],intval($airportlist['apasstrafficcompare']));
					$enplan[]=array($airportlist['IATA'],intval($airportlist['aenplaning']));
					$deplan[]=array($airportlist['IATA'],intval($airportlist['adeplaning']));
					$aepdomestic[]=array($airportlist['IATA'],intval($airportlist['aepdomestic']));
					$aepintl[]=array($airportlist['IATA'],intval($airportlist['aepintl']));
					$aconcessiongrosssales[]=array($airportlist['IATA'],intval($airportlist['aconcessiongrosssales']));
					$aconcessiongrosssalesdf[]=array($airportlist['IATA'],intval($airportlist['aconcessiongrosssales-df']));
					$asalesep[]=array($airportlist['IATA'],intval($airportlist['asalesep']));
					$asalesepdf[]=array($airportlist['IATA'],intval($airportlist['asalesep-df']));
					$arentrev[]=array($airportlist['IATA'],intval($airportlist['arentrev']));
					$arentrevdf[]=array($airportlist['IATA'],intval($airportlist['arentrev-df']));
					$arentep[]=array($airportlist['IATA'],intval($airportlist['arentep']));
					$arentepdf[]=array($airportlist['IATA'],intval($airportlist['arentep-df']));
					$acurrsqft[]=array($airportlist['IATA'],intval($airportlist['acurrsqft']));
					$acurrsqftdf[]=array($airportlist['IATA'],intval($airportlist['acurrsqft-df']));
					$afbgrosssales[]=array($airportlist['IATA'],intval($airportlist['afbgrosssales']));
					$afbsalesep[]=array($airportlist['IATA'],intval($airportlist['afbsalesep']));
					$afbrentrev[]=array($airportlist['IATA'],intval($airportlist['afbrentrev']));
					$afbrentep[]=array($airportlist['IATA'],intval($airportlist['afbrentep']));
					$afbcurrsqft[]=array($airportlist['IATA'],intval($airportlist['afbcurrsqft']));
					$asrgrosssales[]=array($airportlist['IATA'],intval($airportlist['asrgrosssales']));
					$asrsalesep[]=array($airportlist['IATA'],intval($airportlist['asrsalesep']));
					$asrrentrev[]=array($airportlist['IATA'],intval($airportlist['asrrentrev']));
					$asrrentep[]=array($airportlist['IATA'],intval($airportlist['asrrentep']));
					$asrcurrsqft[]=array($airportlist['IATA'],intval($airportlist['asrcurrsqft']));
				
					if($rcounter%2 == 0)
						{
							 $class= '<tr>'; 
						} else 
						{
					    	$class = '<tr>';
						}
					$tbl_header.=$class;
					 foreach($col as $key => $val):				
						$tbl_header.='<td width='.round(100/count($col)).'>'.$this->reportmodel->is_numericcheck($key,$airportlist[$key]).'</td>';		    	
						if(is_numeric($airportlist[$key]) && $key!="ayear")
						{
							$sumarray[$key]+=($airportlist[$key] > 0 ? $airportlist[$key] : '');
						} else
						{
							$sumarray[$key]="";
						}
					endforeach;						
				  	$tbl_header.='</tr>';
					$rcounter++;
					if($rcounter==$this->limit)
					{
						break;
					}
				endforeach;
				$alldata[]=$passenger;
				$alldata[]=$apasstrafficcompare;
				$alldata[]=$enplan;
				$alldata[]=$deplan;
				$alldata[]=$aepdomestic;
				$alldata[]=$aepintl;
				$alldata[]=$aconcessiongrosssales;
				$alldata[]=$aconcessiongrosssalesdf;
				$alldata[]=$asalesep;
				$alldata[]=$asalesepdf;
				$alldata[]=$arentrev;
				$alldata[]=$arentrevdf;
				$alldata[]=$arentep;
				$alldata[]=$arentepdf;
				$alldata[]=$acurrsqft;
				$alldata[]=$acurrsqftdf;
				$alldata[]=$afbgrosssales;
				$alldata[]=$afbsalesep;
				$alldata[]=$afbrentrev;
				$alldata[]=$afbrentep;
				$alldata[]=$asrgrosssales;
				$alldata[]=$asrsalesep;
				$alldata[]=$asrrentrev;
				$alldata[]=$asrrentep;
				$alldata[]=$asrcurrsqft;
				
				
				
				$this->outputData['alldata']=$alldata;
				//$tbl_header.='<tr><td colspan="'.count($col).'"></td></tr><tr>';
				//foreach($sumarray as $key => $val):				
				//$tbl_header.='<th>'.$this->reportmodel->is_numericcheck($key,$sumarray[$key]).'</th>';		
				//endforeach;
				//$tbl_header.='</tr>';					
				$tbl_header.='<tr><td colspan='.count($col).'></td></tr></table></div>';
			} else 
			{				
				$tbl_header='<div>';
				$tbl_header.='<div class="alert alert-danger">No Data Update...</div></div>';	
			}  // END if(count($ap_count)>0)			
			$str = preg_replace('/\s\s+/', ' ', $tbl_header);
			//$mpdf->WriteHTML($str);
			//$mpdf->defaultfooterline=0;
			//$mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">'.date("Y-m-d H:i:s").'</div>');
			//$mpdf->Output();					
			//exit;	
			return $str;
			
		}
		public function exporttodoc()
		{			
			$compnaytype=$this->common->GetCurrentUserInfo('cc');			
			$listofcompanies=$this->common->TableGetAllOrderByWhere('companies',array('companytype'=>$compnaytype),'companyname','ASC');			
			header("Content-type: application/vnd.ms-word");
			header("Content-Disposition: attachment;Filename=document_name.doc");
			echo "<html>";
			echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
			echo "<body>";
			foreach($listofcompanies as $mycom):
			//print_r($mycom);			
			//"SELECT * FROM companycontacts left join companies on companycontacts.cid=companies.cid where companycontacts.cid = '$_SESSION[cid]' AND companycontacts.printed='Yes'";
			$cont=$this->common->JoinTable('*','companycontacts','companies','companycontacts.cid=companies.cid','LEFT',array('companycontacts.cid'=>$mycom['cid'],'companycontacts.printed'=>'Yes'));			
			$comp = $this->common->CustomQueryALL("SELECT companylocations.cid,companylocations.aid,companylocations.tid,companylocations.IATA,airports.aname,airports.acity,airports.astate,airports.acountry FROM companies 
	left join companylocations on companylocations.cid = companies.cid 
	left join airports on airports.aid = companylocations.aid 
	left join terminals on terminals.tid = companylocations.tid
	where companies.cid=".$mycom['cid']." order by airports.acity");			
			$comp_ = $this->common->CustomQueryALL("SELECT lkpairportlist.aid,lkpairportlist.IATA,lkpairportlist.acity,lkpairportlist.astate,lkpairportlist.aname,lkpairportlist.acountry,companylocations.cid,companylocations.aid,companylocations.tid,companylocations.IATA FROM companies 
	left join companylocations on companylocations.cid = companies.cid 
	left join lkpairportlist on lkpairportlist.aid = companylocations.aid 
	where companies.cid='".$mycom['cid']."' order by lkpairportlist.acity");
		$whoopee = array_merge($comp,$comp_);
		$this->common->array_sort_by_column($whoopee, 'acity');
		echo $cont[0]['companyname'];
		echo "<br>";
		foreach($cont as $result){
		echo $result['cfname']." ".$result['clname'];echo "<br>";
		echo $result['ctitle'];echo "<br>";
		echo $result['caddress1'];echo "<br>";
		if($result['caddress2'])
		{
			echo $result['caddress2'];echo "<br>";
		}
			echo $result['ccity'].", ".$result['cstate']." ".$result['czip']." ".$result['ccountry'];echo "<br>";
		if($result['cphone'])
		{
			echo "Phone: ".$result['cphone'];echo "<br>";
		}
		if($result['cfax']){
			echo "Fax: ".$result['cfax'];echo "<br>";
		}
		if($result['cemail']){
		echo "Email: <a href='mailto:".$result['cemail']."'>".$result['cemail']."</a>";echo "<br>";
		}
		if($result['cwebsite']){
		echo "Website: <a href='".$result['cwebsite']."'>".$result['cwebsite']."</a>";
		}
		echo "<br><br><br>";
	}
			echo "<div style='display:block;margin:0;padding:0;width:100%;'><pre style='font-family:times new roman;font-size:17px;'>".iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE',$result['companyinfo'])."</pre></div>";echo "<br>";
			foreach($whoopee as $loc)
			{ 
				if($loc['acity'] != '' && $loc['astate'] != '' && $loc['aname'] != '')
				{
					echo $loc['acity'].', '.$loc['astate'].' ('.$loc['aname'].')';echo "<br>";
				}
			}

	echo "</div>";

	echo "<br style='page-break-before: always;' clear='all' />";			
			endforeach;
			echo "</body>";
			echo "</html>";
		}		
		public function GenPdf()
		{
		error_reporting(0);	
		echo $this->common->pdfstyleing();
		$mpdf=new mPDF('utf-8','Letter');	
		$laid=$this->session->userdata('list');		
		$objQuery=$this->common->GetAllWhereIn("aid,IATA,aname,acity,astate,mgtstructure",'airports','aid',$laid);
		foreach($objQuery as $resultData):
		$aaid=$resultData["aid"];
		$mpdf->SetHTMLHeader('<div style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">'.$resultData["acity"].', '.$resultData["astate"].'</div>'); 
		$tbl_header.='<table width="100%" border="0">
						  <tr>
						<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">'.$resultData["aname"].'</td>
						  </tr>
						  <tr>
							<td style="color:#000;display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:12pt;font-weight:bold;">'.$resultData["IATA"].'</td>
						  </tr>
						</table>
						<table width="100%" border="0" cellpadding="2">
						  <tr bgcolor="#999999"  style="color:#fff;">
							<td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF">Contact(s)</td>
						  </tr>
						 </table>';	
				$tbl_header.= '<table width="100%" border="0">';

  			$dataQuery=$this->common->JoinTable("*","airportcontacts","lkpmgtresponsibility",
			"lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility","LEFT",array('aid'=>$aaid));



			$count = 0;

			foreach($dataQuery as $resultData3):
	     if($count % 2 == 0)
		$tbl_header.= '<tr width="40%" >';
		$tbl_header.= '
    <td><table width="40%" border="0" >
			<tr>

			  <td class="category" style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" colspan="2">'.$resultData3["mgtresponsibilityname"].'</td>

			</tr>

				  <tr>

				        <td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Contact</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["afname"].' '.$resultData3["alname"].'</td>

				  </tr>

				  <tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Title</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["atitle"].'</td>

				  </tr>

				  <tr>

				        <td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Company</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["acompany"].'</td>

				  </tr>

				  <tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Address</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["aaddress1"].'</td>

				  </tr>';



				 if($resultData3["aaddress2"]){

				$tbl_header.=  '<tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["aaddress2"].'</td>

				  </tr>';

				 }

				   

				 $tbl_header.= '<tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;"></td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["accity"].', '.$resultData3["acstate"].' '.$resultData3["aczip"].'</td>

				  </tr>

				  <tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Phone</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["aphone"].'</td>

				  </tr>

				  <tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Fax</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["afax"].'</td>

				  </tr>

				  <tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Email</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["aemail"].'</td>

				  </tr>

				  <tr>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">Website</td>

					<td style="font-family:Vectora LH 55 Roman,arial;color:#000;font-size:8pt;">'.$resultData3["awebsite"].'</td>

				  </tr>
             </table></td>';
	       if($count % 2 == 0)
			   $tbl_header.= '</tr>';
		   $count++;
			endforeach;
			 $tbl_header.= '</table>';
			 /* End Result Data 3__________________________________________________  */
			 	$tbl_header.= '<table width="100%" border="0" cellpadding="2">
				 <tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">
    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF">Airport Info</td>
	</tr>
</table>';
		
$airterm ="SELECT * FROM `airports` LEFT JOIN airportsannual ON airports.aid=airportsannual.aid 

		INNER JOIN terminals ON airports.aid=terminals.aid INNER JOIN terminalsannual ON terminals.tid=terminalsannual.tid 
		INNER JOIN airporttotals ON airports.aid=airporttotals.aid 
		WHERE airports.aid='$aaid ORDER BY airports.aid, terminals.tid'";
		$resultDatao=$this->common->CustomQueryROw($airterm);		
		$ap_count=$this->common->CustomCountQuery($airterm);		

		$exp_countall=$resultDatao["texpansionplanned"];			
		$exp_count = explode("#", $exp_countall);	
		$addsqft=$resultDatao["addsqft"];
		$addsq_count = explode("#", $addsqft);		
		$completedexpdate=$resultDatao["completedexpdate"];
		$completed_count=explode("#", $completedexpdate);		
		$add_sq="Add'l Sq. Ft.";		
		$tbl_header.='
<table width="100%" border="0" cellspacing="0" cellpadding="0"   style="text-align:left;border-collapse: collapse;">
 <tr>
    <td style="vertical-align:top;padding:0;"> 
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0 0 0 5px;">
						  <tr >
							  <td style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Airport Info</td>
							  <td style="padding:0;padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>
                                                 </tr>
						  <tr>
							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Airport Configuration</td>
							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["configuration"].'</td>
						  </tr>
						  <tr>
							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Concessions Mgt. Type</td>
							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["mgtstructure"].'</td>

						  </tr>';
						  $tbl_header.= '</table>';
						  for($kk=0;$kk<count($exp_count);$kk++)

		{

			$tbl_header.='<tr>

							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Expansion Planned</td>

							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$exp_count[$kk].'</td>

						  </tr>

						  <tr>

							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$add_sq.'</td>

							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format((float)$addsq_count[$kk]).'</td>

						  </tr>

						  <tr>

							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Complete Date</td>

							  <td style="padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$completed_count[$kk].'</td>

						  </tr>';

						  }						 
						  $tbl_header.='</table>

		</td> 

                <td style="vertical-align:top;padding:0;">

	 		<table width="100%" border="0" cellpadding="0" cellspacing="0" >

						  <tr>

							  <td style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminals/Conc.</td>

  							  <td style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Abbr.</td>

	          				  <td style="padding:0;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Dominant Airline</td>

						  </tr>';
						  	$resultData2=$this->common->CustomQuery($airterm);

		for ($r=0;$r<$ap_count;$r++) 

			{				   

						 //$resultData2=mysql_fetch_array($airtermQu); 

						 

						   

					   $tbl_header.= '<tr>

							  <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultData2[$r]["terminalname"].'</td>

  							  <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultData2[$r]["terminalabbr"].'</td>

	          				  	  <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultData2[$r]["tdominantair"].'</td>

					  </tr>';

			

			}
			if($resultData["IATA"]!='ORF')

{

if($resultData["IATA"]!='AVL')

{

if($resultData["IATA"]!='COS1')

{

if($resultData["IATA"]!='HRL')

{

if($resultData["IATA"]!='MEM1')

{

if($resultData["IATA"]!='YYZ1')

{

if($resultData["IATA"]!='OKC')

{

if($resultData["IATA"]!='MSY1')

{

			$tbl_header.='

	  <table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF;">Passenger Traffic</td>

	</tr>

 </table>';

	$tbl_header.='	<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

    <tr style="font-family:arial;font-size:9pt;font-weight:bold;">

    <td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">+ / - %</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Deplaning</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Enplaning</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">EP Domestic</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:14.3%">EP Int l</td>

  </tr>';

 $airportlist=$this->common->CustomQuery($airterm);

		for ($s=0;$s<$ap_count;$s++) 

			{

	

			//$airportlist=$airtermQu2[$s]; 

        $tbl_header.='<tr>

    <td style="padding:3px 20px 3px 3px;text-align:left !important;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$airportlist[$s]["terminalabbr"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($airportlist[$s]["tpasstraffic"]).'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$airportlist[$s]["tpasstrafficcompare"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($airportlist[$s]["tdeplaning"]).'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($airportlist[$s]["tenplaning"]).'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.
	($airportlist[$s]["tepdomestic"]).'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($airportlist[$s]["tepintl"]).'</td>

	</tr>';

  

  }

$tbl_header.='<tr><td style="text-align:letf;padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["apasstraffic"]).'</td>

<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["apasstrafficcompare"].'</td>

<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["adeplaning"]).'</td>

<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["aenplaning"]).'</td>

<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["aepdomestic"]).'</td>

<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["aepintl"]).'</td></tr></table>';

if($resultDatao["apasscomment"]!='')

{

    $tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["apasscomment"].'</p>';

}

if($resultDatao["avgdwelltime"]!='')

{

$avg_time=$resultDatao["avgdwelltime"].' minutes';

}

if($resultData["IATA"]!='VPS')

{

$tbl_header.=' 



<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse;>

	  

  <tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

 

    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF;"> Airport Percentages</td>

	</tr>

 </table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:left">

  <tr style="font-weight:bold;">

    	<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Pre/Post Security</td>

	<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Business to Leisure Ratio</td>

	<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">OD Transfer</td>

	<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Average Dwell Time</td>

  </tr>

  <tr>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["presecurity"].'/'.$resultDatao["postsecurity"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["ratiobusleisure"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["ondtransfer"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$avg_time.'</td>

  </tr></table>';



	$tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse;>

  <tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF;"> Airportwide Info</td>

	</tr>

 </table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right">

	<tr >

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Parking</td>

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;" >Short</td>

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Long</td>

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Economy</td>

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Valet</td>

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Car Rentals</td>

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Agencies</td>

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Rev</td>

			<td style="text-align:right;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Rentals</td>

	</tr>';

	

	$tbl_header.='<tr>

		<td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Hourly</td>

		<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["hourlyshort"])

		{

		$tbl_header.='$'.number_format($resultDatao["hourlyshort"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["hourlylong"])

		{

		$tbl_header.='$'.number_format($resultDatao["hourlylong"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["hourlyeconomy"])

		{

		$tbl_header.='$'.number_format($resultDatao["hourlyeconomy"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["hourlyvalet"])

		{

		$tbl_header.='$'.number_format($resultDatao["hourlyvalet"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental On Site</td>

		<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["carrentalagenciesonsite"].'</td>

		<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["carrentalrevonsite"])

		{

		$tbl_header.='$'.number_format($resultDatao["carrentalrevonsite"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["carrentalrevtoaironsite"])

		{

		$tbl_header.='$'.number_format($resultDatao["carrentalrevtoaironsite"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

	$tbl_header.='</tr>

	

        <tr>

		    <td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Daily</td>

		    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["dailyshort"])

		{

		$tbl_header.='$'.number_format($resultDatao["dailyshort"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["dailylong"])

		{

		$tbl_header.='$'.number_format($resultDatao["dailylong"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["dailyeconomy"])

		{

		$tbl_header.='$'.number_format($resultDatao["dailyeconomy"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["dailyvalet"])

		{

		$tbl_header.='$'.number_format($resultDatao["dailyvalet"], 2, '.', '').'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental Off Site:</td>

		    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["carrentalagenciesoffsite"].'</td>

		    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["carrentalrevoffsite"])

		{

		$tbl_header.='$'.number_format($resultDatao["carrentalrevoffsite"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["carrentalrevtoairoffsite"])

		{

		$tbl_header.='$'.number_format($resultDatao["carrentalrevtoairoffsite"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

	$tbl_header.='</tr>



	<tr>

	    <td  style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;"># Spaces</td>

	    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["spacesshort"]).'</td>

	    <td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["spaceslong"]).'</td>

	    <td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["spaceseconomy"]).'</td>

	    <td  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["spacesvalet"].'</td>

	    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Total Cars Rented</td>

	    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($resultDatao["totalcarsrented"]).'</td>

	    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>

	    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">&nbsp;</td>

	</tr>



	<tr>

    <td>&nbsp;</td>

    <td>&nbsp;</td>

    <td>&nbsp;</td>

    <td>&nbsp;</td>

    <td>&nbsp;</td>

	<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Car Rental Sq. Ft.</td>

	<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.($resultDatao["carrentalsqft"]).'</td>

	<td>&nbsp;</td>

	</tr>

</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:left">

<tr>

<td style="width:15%;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Parking Revenue</td>

<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

if($resultDatao["parkingrev"])

{

$tbl_header.='$'.number_format($resultDatao['parkingrev']).'</td>';

}

else

{

$tbl_header.='$0';

}

$tbl_header.='<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">Total Spaces</td>

<td style="font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

if($resultDatao["parkingspaces"])

{

$tbl_header.=number_format($resultDatao['parkingspaces']).'</td>';

}

$tbl_header.='<td>&nbsp;</td>

<td>&nbsp;</td>

<td>&nbsp;</td>

<td>&nbsp;</td>

<td>&nbsp;</td>

<td>&nbsp;</td><td>&nbsp;</td>

</tr>

</table>';	

	$tbl_header.='

	<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right">

	<tr style="font-weight:bold;">

			<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;width:160px" width="150px">&nbsp;</td>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;font-weight:bold;border-bottom:1px solid #000;">Revenue</td>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;font-weight:bold;border-bottom:1px solid #000;">Rev. to Airport</td>

			<td style="border-bottom:1px solid #000;">&nbsp;</td>

			<td style="border-bottom:1px solid #000;">&nbsp;</td>

			<td style="border-bottom:1px solid #000;">&nbsp;</td>

			<td style="border-bottom:1px solid #000;">&nbsp;</td>

			

	</tr>

	<tr>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Passenger Services</td>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["passservicesrev"])

		{

		$tbl_header.='$'.number_format($resultDatao["passservicesrev"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["passservicesrevtoair"])

		{

		$tbl_header.='$'.number_format($resultDatao["passservicesrevtoair"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

			

	$tbl_header.='</tr>

	<tr>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Advertising</td>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["advertisingrev"])

		{

		$tbl_header.='$'.number_format($resultDatao["advertisingrev"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["advertisingrevtoair"])

		{

		$tbl_header.='$'.number_format($resultDatao["advertisingrevtoair"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}	

	$tbl_header.='</tr>

	<tr>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;text-align:left;">Currency Exchange</td>

			<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["currencyexrev"])

		{

		$tbl_header.='$'.number_format($resultDatao["currencyexrev"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

		$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

		if($resultDatao["currencyexrevtoair"])

		{

		$tbl_header.='$'.number_format($resultDatao["currencyexrevtoair"]).'</td>';

		}

		 else

		{

		$tbl_header.='$0';

		}

	$tbl_header.='</tr></table>';

        if($resultDatao["awrevcomment"]!='')

        {

	$tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["awrevcomment"].'</p>';

        }
if($resultData["IATA"]!='MSN')

{

if($resultData["IATA"]!='RNO1')

{

if($resultData["IATA"]!='RIC')

{

if($resultData["IATA"]!='SHV')

{

if($resultData["IATA"]!='FWA')

{

		$tbl_header.='<table width="100%">

  <tr bgcolor="#999999"  style="color:#fff;font-weight:bold;" >

    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF;">Concession Totals - Terminal Breakdowns <span style="font-size:9px;">(Food/Beverage, Specialty

Retail, News/Gifts Only)</span></td>

  </tr>

 </table>

 <table width="100%" border="0" style="text-align:right" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; >

  <tr style="font-weight:bold;">

    <td style="text-align:left;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;">Terminal</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Sales</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Sales EP</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total Rent To Airport</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Rent EP</td>

    <td style="text-align:right;padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Current Sq. Ft</td>

	

  </tr>';

  $airtermQu3=mysql_query($airterm);

  for ($t=0;$t<$ap_count;$t++) 

	{				   

	$totairportList = mysql_fetch_assoc($airtermQu3);

	$totgr=$totairportList["fbgrosssales"]+$totairportList["srgrosssales"]+$totairportList["nggrosssales"]."";

	$totsalesep=$totgr/$totairportList["tenplaning"];

	$totrentrev=$totairportList["fbrentrev"]+$totairportList["srrentrev"]+$totairportList["ngrentrev"];

	//$totrentep=$totairportList["fbrentep"]+$totairportList["srrentep"]+$totairportList["ngrentep"];

    $totrentep=$totrentrev/$totairportList["tenplaning"];

	$totcurrsqft=$totairportList["fbcurrsqft"]+$totairportList["srcurrsqft"]+$totairportList["ngcurrsqft"];

  $tbl_header.='

  <tr>

  <td style="text-align:left;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$totairportList["terminalabbr"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

	if($totgr)

	{

		$tbl_header.='$'.number_format($totgr).'</td>';

	}

	 else

		{

		$tbl_header.='';

		}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

	if($totsalesep)

	 {

	 $tbl_header.='$'.number_format($totsalesep, 2, '.', '').'</td>';

	 }

	  else

		{

		$tbl_header.='$0.00';

		}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

	if($totrentrev)

	 {

	 $tbl_header.='$'.number_format($totrentrev).'</td>';

	 }

	  else

		{

		$tbl_header.='';

		}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">';

	 if($totrentep)

	 {

	 $tbl_header.='$'.number_format($totrentep, 2, '.', '').'</td>';

	 }

	  else

		{

		$tbl_header.='';

		}

    $tbl_header.='<td style="text-align:right;padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.number_format($totcurrsqft).'</td>

  </tr>';

 $totgrall += $totgr;

 $totsalesepall+= $totgr/$totairportList["aenplaning"];

 $totrentrevall += $totrentrev;

 $totrentepall += $totrentrev/$totairportList["aenplaning"];

 $totcurrsqftall += $totcurrsqft;

 }

$tbl_header.='<tr>

<td style="text-align:left;padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($totgrall)

	{

	$tbl_header.='$'.number_format($totgrall).'</td>';

	}

	 else

		{

		$tbl_header.='';

		}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($totsalesepall)

	 {

	 $tbl_header.='$'.number_format($totsalesepall, 2, '.', '').'</td>';

	 }

	  else

		{

		$tbl_header.='$0.00';

		}

$tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($totrentrevall)

	{

	$tbl_header.='$'.number_format($totrentrevall).'</td>';

	}

	 else

		{

		$tbl_header.='';

		}

$tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

if($totrentepall)

	{

	$tbl_header.='$'.number_format($totrentepall, 2, '.', '').'</td>';

	}

	 else

		{

		$tbl_header.='';

		}

$tbl_header.='<td style="padding:3px 20px 3px 3px;border-top:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($totcurrsqftall).'</td>

</tr>';

 unset($totgrall);

 unset($totsalesepall);

 unset($totrentrevall);

 unset($totrentepall);

 unset($totcurrsqftall);

$tbl_header.='</table>';

if($resultDatao["grosscomment"]!='')

{

$tbl_header.='<p  style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;">'.$resultDatao["grosscomment"].'</p>';

}

/*$tbl_header.= '<table width="100%" border="0">

  <tr>

    <td style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH Black;font-size:14pt;">'

	.$resultData["acity"].','.$resultData["astate"].'</td>

  </tr>

  </table>';*/

  $sym='Food/Beverage';

$tbl_header.='<table width="100%" border="0" cellpadding="2">

  <tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF;">Concession Program Details - Category Breakdowns</td>

	</tr>

 </table>

 

 <table width="100%" border="0"  style="text-align:right" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse;>

  <tr style="font-weight:bold;color:#a20000;">

   <td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Food<strong>/</strong>Beverage</span></td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Sales</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"><span>Sales<strong>/</strong>EP</span></td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total Rent To Airport</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Current Sq. Ft</td>

	

  </tr>';



 $airtermQu4=mysql_query($airterm);

  for ($u=0;$u<$ap_count;$u++) 


	{

	$fbairportList = mysql_fetch_assoc($airtermQu4);

	$all_fb=$fbairportList["fbsalesep"];

  $tbl_header.='

  <tr>

    <td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$fbairportList["terminalabbr"].'</td>';

	if($fbairportList["fbgrosssales"]&& $fbairportList["fbgrosssales"] !== '0')

	{

	$fb_gr_value='$'.number_format($fbairportList["fbgrosssales"]);

	}

	else

	{

	$fb_gr_value='';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$fb_gr_value.'</td>';

   $fb_salesep=$fbairportList["fbgrosssales"]/$fbairportList["tenplaning"];

	if($fb_salesep)

	 {

	  $fb_sp_value='$'.number_format($fb_salesep, 2, '.', '');

	 }

	 else

	{

	$fb_sp_value='$0.00';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$fb_sp_value.'</td>';

	if($fbairportList["fbrentrev"])

	{

	

	$fb_rv_value='$'.number_format($fbairportList["fbrentrev"]);

	}

	else

	{

	$fb_rv_value='';

	}

	  

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$fb_rv_value.'</td>';

	$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.number_format($fbairportList["fbcurrsqft"]).'</td></tr>';

	$allfbsales+=$fbairportList["fbgrosssales"];

	$allfbsalesep+=$fbairportList["fbgrosssales"]/$fbairportList["aenplaning"];

	$allfbrentrev+=$fbairportList["fbrentrev"];

	//$allfbrentep+=$fbairportList["fbrentep"];

	$allfbcurrsqft+=$fbairportList["fbcurrsqft"];

 }



$tbl_header.='<tr><td style="text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>';

if($allfbsales)

	{

	$fb_gr_totvalue='$'.number_format($allfbsales);

	}

	else

	{

	$fb_gr_totvalue='';

	}

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$fb_gr_totvalue.'</td>';

	if($allfbsalesep)

	{

	$fb_sp_totvalue='$'.number_format($allfbsalesep, 2, '.', '');

	}

	else

	{

	$fb_sp_totvalue.='$0.00';

	}

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$fb_sp_totvalue.'</td>';

	if($allfbrentrev)

	{

	$fb_rv_totvalue='$'.number_format($allfbrentrev);

	}

		else

	{

	$fb_rv_totvalue='';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$fb_rv_totvalue.'</td>';

	

	

	

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($allfbcurrsqft).'</td></tr></table>

<div style="height:10px"></div>

';

 unset($allfbsales);

 unset($allfbsalesep);

 unset($allfbrentrev);

 //unset($allfbrentep);

 unset($allfbcurrsqft);

$tbl_header.='

   <table width="100%" border="0"  style="text-align:right" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse;>

  <tr style="font-weight:bold;color:#a20000;">

   <td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;">Specialty Retail</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Sales</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"><span>Sales<strong>/</strong>EP</span></td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total Rent To Airport</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Current Sq. Ft</td>

	

  </tr>';



  $airtermQu5=mysql_query($airterm);

  for ($v=0;$v<$ap_count;$v++) 

	{

	$srairportList = mysql_fetch_assoc($airtermQu5);

  $tbl_header.='

  <tr>

  <td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>

   <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$srairportList["terminalabbr"].'</td>';

   

	if($srairportList["srgrosssales"])

	{

	$sr_gr_value='$'.number_format($srairportList["srgrosssales"]);

	}

		else

	{

	$sr_gr_value='';

	}

	$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$sr_gr_value.'</td>';   

$sr_salesep=$srairportList["srgrosssales"]/$srairportList["tenplaning"];

	if($sr_salesep)

	{

	$sr_sp_value='$'.number_format($sr_salesep, 2, '.', '');

	}

		else

	{

	$sr_sp_value='$0.00';

	}	

	$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$sr_sp_value.'</td>';

	if($srairportList["srrentrev"])

	{

	$sr_rv_value='$'.number_format($srairportList["srrentrev"]);

	}

	else

	{

	$sr_rv_value='';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$sr_rv_value.'</td>';

	$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.number_format($srairportList["srcurrsqft"]).'</td>

	  

	</tr>';

	$allsrsales+=$srairportList["srgrosssales"];

	$allsrsalesep+=$srairportList["srgrosssales"]/$srairportList["aenplaning"];

	$allsrrentrev+=$srairportList["srrentrev"];

	//$allsrrentep+=$srairportList["srrentep"];

	$allsrcurrsqft+=$srairportList["srcurrsqft"];

 }



$tbl_header.='<tr>

<td style="text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>';

	if($allsrsales)

	{

	$sr_gr_totvalue='$'.number_format($allsrsales);

	}

		else

	{

	$sr_gr_totvalue='';

	}

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$sr_gr_totvalue.'</td>';

if($allsrsalesep)

	{

	$sr_sp_totvalue='$'.number_format($allsrsalesep, 2, '.', '');

	}

		else

	{

	$sr_sp_totvalue='$0.00';

	}

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$sr_sp_totvalue.'</td>';

	if($allsrrentrev)

	{

		$sr_rv_totvalue.='$'.number_format($allsrrentrev);

	}

		else

	{

	$sr_rv_totvalue='';

	}

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$sr_rv_totvalue.'</td>';

	$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($allsrcurrsqft).'</td></tr>

</table>

<div style="height:10px"></div>

';	

 unset($allsrsales);

 unset($allsrsalesep);

 unset($allsrrentrev);

 //unset($allsrrentep);

 unset($allsrcurrsqft);

$tbl_header.='

   <table width="100%" border="0"  style="text-align:right" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse;>

  <tr style="font-weight:bold;color:#a20000;">

   <td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>News<strong>/</strong>Gifts Retail</span></td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Sales</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"><span>Sales<strong>/</strong>EP</span></td>

        <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total Rent To Airport</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Current Sq. Ft</td>

	

  </tr>';

  

   $airtermQu6=mysql_query($airterm);

  for ($w=0;$w<$ap_count;$w++) 

	{

	$ngairportList = mysql_fetch_assoc($airtermQu6);

  $tbl_header.='

  <tr>

  <td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>

  <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$ngairportList["terminalabbr"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';

	if($ngairportList["nggrosssales"])

	{

	$tbl_header.='$'.number_format($ngairportList["nggrosssales"]).'</td>';

	}

	else

	{

	$tbl_header.='';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';

	$ng_salesep=$ngairportList["nggrosssales"]/$ngairportList["tenplaning"];

	if($ng_salesep)

	{

	$tbl_header.='$'.number_format($ng_salesep, 2, '.', '').'</td>';

	}

	else

	{

	$tbl_header.='$0.00';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';

	if($ngairportList["ngrentrev"])

	{

	$tbl_header.='$'.number_format($ngairportList["ngrentrev"]).'</td>';

	}

	else

	{

	$tbl_header.='';

	}

   

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.number_format($ngairportList["ngcurrsqft"]).'</td>  

   </tr>';

		$allngsales+=$ngairportList["nggrosssales"];

	$allngsalesep+=$ngairportList["nggrosssales"]/$ngairportList["aenplaning"];

	$allngrentrev+=$ngairportList["ngrentrev"];

	//$allngrentep+=$ngairportList["ngrentep"];

	$allngcurrsqft+=$ngairportList["ngcurrsqft"];

 }





$tbl_header.='<tr><td style="text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>

<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($allngsales)

	{

	$tbl_header.='$'.number_format($allngsales).'</td>';

	}

    else

	{

	$tbl_header.='';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($allngsalesep)

	{

	$tbl_header.='$'.number_format($allngsalesep, 2, '.', '').'</td>';

	}

	else

	{

	$tbl_header.='$0.00';

	}

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($allngrentrev)

	{

	$tbl_header.='$'.number_format($allngrentrev).'</td>';

	}

	 else

	{

	$tbl_header.='';

	}

    

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($allngcurrsqft).'</td></tr></table>

';

unset($allngsales);

 unset($allngsalesep);

 unset($allngrentrev);

 unset($allngrentep);

 unset($allngcurrsqft);	

 $tbl_header.='

   <table width="100%" border="0"  style="text-align:right" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse;>

  <tr style="font-weight:bold;color:#a20000;">

   <td style="padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;width:30%;"><span>Duty Free Retail</span></td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal</td>

     <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Gross Sales</td>

    <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"><span>Sales<strong>/</strong>EP</span></td>

        <td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Total Rent To Airport</td>

		<td style="padding:3px 20px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Current Sq. Ft</td>

	

  </tr>';

  

   $airtermQu7=mysql_query($airterm);

  for ($x=0;$x<$ap_count;$x++) 

	{

	$dfairportList = mysql_fetch_assoc($airtermQu7);

	//echo "<pre>";print_r($ngairportList);

	

  $tbl_header.='

  <tr>

  <td style="border-bottom:1px solid #F0F0F0;">&nbsp;</td>

  <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$dfairportList["terminalabbr"].'</td>

    <td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';

	if($dfairportList["dfgrosssales"])

	{

	$tbl_header.='$'.number_format($dfairportList["dfgrosssales"]).'</td>';

	}

	else

	{

	$tbl_header.='';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';

	$df_salesep=$dfairportList["dfgrosssales"]/$dfairportList["tepintl"];

	if($df_salesep)

	{

	$tbl_header.='$'.number_format($df_salesep, 2, '.', '').'</td>';

	}

	else

	{

	$tbl_header.='$0.00';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';

	if($dfairportList["dfrentrev"])

	{

	$tbl_header.='$'.number_format($dfairportList["dfrentrev"]).'</td>';

	}

	else

	{

	$tbl_header.='';

	}

   

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.number_format($dfairportList["dfcurrsqft"]).'</td>  

   </tr>';

		$alldfsales+=$dfairportList["dfgrosssales"];

	$alldfsalesep+=$dfairportList["dfgrosssales"]/$dfairportList["aepintl"];

	$alldfrentrev+=$dfairportList["dfrentrev"];

	//$allngrentep+=$ngairportList["ngrentep"];

	$alldfcurrsqft+=$dfairportList["dfcurrsqft"];

 }





$tbl_header.='<tr><td style="text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Totals</td>

<td>&nbsp;</td>

<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($alldfsales)

	{

	$tbl_header.='$'.number_format($alldfsales).'</td>';

	}

    else

	{

	$tbl_header.='';

	}

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($alldfsalesep)

	{

	$tbl_header.='$'.number_format($alldfsalesep, 2, '.', '').'</td>';

	}

	else

	{

	$tbl_header.='$0.00';

	}

$tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($alldfrentrev)

	{

	$tbl_header.='$'.number_format($alldfrentrev).'</td>';

	}

	 else

	{

	$tbl_header.='';

	}

    

    $tbl_header.='<td style="padding:3px 20px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.number_format($alldfcurrsqft).'</td></tr></table>

';

unset($alldfsales);

 unset($alldfsalesep);

 unset($alldfrentrev);

 //unset($alldfrentep);

 unset($alldfcurrsqft);	

 }

 }

 }

 if($resultData["IATA"]!='SHV')

{

if($resultData["IATA"]!='FWA')

{

$tbl_header.= '<table width="100%" border="0" cellpadding="2">

  <tr bgcolor="#999999"  style="color:#fff;font-weight:bold;">

    <td style="font-family:Vectora LH 55 Roman,arial;font-size:10pt;font-weight:bold;color:#FFFFFF;">Concession Tenant Details</td>

	</tr>

 </table>';

   $tbl_header.='<table width="100%" border="0" style="text-align:left" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse;>';

   $listsql = "select distinct category from lkpcategory";

   $list=$this->common->Selectdistinct('lkpcategory','category');    
   	for($l=0;$l<count($list);$l++)

	{

	$cmpnyterms = $list[$i];

	 $catid=$cmpnyterms[$l]['category'];

         $products = $this->common->CustomQuery("select * from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='$catid' and outlets.aid = '$aaid' ORDER BY outletname") ;

		

		    $tbl_header.='<tr>

		<td style="width:45%;padding:3px 10px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$catid.'(Company)</td>

		<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Product Description</td>

		<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Terminal </td>

		<td style="white-space: nowrap;padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;"># locations</td>

		<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Sq. Ft.</td>

		<td style="padding:3px 5px 3px 3px;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">Expires</td>

		

	

  </tr>';



  if(!empty($products))

 

   	foreach($products as $prod)

	{

	

        if(!check_date($prod['exp']))

        {

        $date_val=$prod['exp']; 

        }

        elseif(strtotime($prod['exp']) =='')

        {

        $date_val="00/00/0000";

        }

        elseif(is_null($prod['exp']))

        {

        $date_val="00/00/0000";

        }

        elseif($prod['exp']=='0000-00-00')

        {

        $date_val="00/00/0000";

        }

	else

	{

	$date_val=date('m/d/Y', strtotime($prod['exp'])); 

	}

	if (empty($prod['companyname'])) 

	{

	$compny='';

	}

	else

	{

	$compny=' ('.$prod['companyname'].')';

	}

$tbl_header.='<tr>

		<td style="width:45%;padding:3px 10px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.stripslashes($prod['outletname']).$compny.'</td>

		<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$prod['productdescription'].'</td>

		<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;text-align:center;">'.$prod['termlocation'].'</td>

		<td style="padding:3px 5px 3px 3px;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;text-align:center;">'.$prod['numlocations'].'</td>

		<td style="padding:3px 5px 3px 3px;text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">';

	if($prod['sqft'])

	{

	$tbl_header.=number_format($prod['sqft']).'</td>';

	}

	else

	{

	$tbl_header.='';

	}

	$tbl_header.='<td style="text-align:right;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-bottom:1px solid #F0F0F0;">'.$date_val.'</td>

		

	

  </tr>';

 

 $tottermlocation += $prod['numlocations'];

 $totsqft += $prod['sqft'];



}



$tbl_header.='<tr>

		<td style="width:45%;padding:3px 10px 3px 3px;text-align:left;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$catid.' Totals</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td style="text-align:center;padding:3px 5px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">'.$tottermlocation.'</td>

		<td style="text-align:center;padding:3px 5px 3px 3px;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial;">';

	if($totsqft)

	{

	$tbl_header.=number_format($totsqft).'</td>';

	}

	else

	{

	$tbl_header.='';

	}

	$tbl_header.='<td>&nbsp;</td>

		</tr>';

 unset( $tottermlocation);

  unset( $totsqft);

		 }
		 
		 
  $tbl_header.='</table>';

}

}

}

}

}

}

}

}

}

}

}

}

}		

			$tbl_header.= '</table></td></tr></table>';

		endforeach;	
		$this->session->unset_userdata('list');
		$tbl_header.= '</table>';	
						
		$ftext="ARN's 2014 Fact Book";		
		$mpdf->SetHTMLFooter('<p style="text-align:center;font-size:8pt;font-family:Vectora LH 55 Roman,arial;border-top:1px solid #000;">'.$ftext.'</p>');		
		$mpdf->WriteHTML($tbl_header);		
		$mpdf->Output();
		
		} 
}