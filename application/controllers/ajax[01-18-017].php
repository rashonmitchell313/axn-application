<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ajax extends CI_Controller {

    public $dataarray;
    public $accesslevel;
    public $user_id;

    function __construct() {

        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            redirect(site_url());
            exit();
        }
        $this->common->ThisSecureArea('user_id');

        $accesslevel = $this->common->GetCurrentUserInfo('accesslevel');

        $user_id = $this->common->GetCurrentUserInfo('user_id'); //user_id

        $this->lang->load('config', 'english');
    }

    /**
     * 
     * Start All Airports (Required Id) Admin
     */
    public function list_of_all_airports_pagi() {

        $tblcolumnsarray = array('aid', 'IATA', 'aname', 'lastmodified', 'modifiedby', 'published', 'approved', 'aid', 'category');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        $searchwords = $_GET['search']['value'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $date = str_replace(' ', '_', $searchwords);

            $where = "(`aname` LIKE '%" . $searchwords . "%' OR `IATA` LIKE '%" . $searchwords . "%' OR  date_format(`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(`approved`, '%b_%d,%Y') LIKE '%" . $date . "%' OR `modifiedby` LIKE '%" . $searchwords . "%' OR `aid` LIKE '%" . $searchwords . "%') AND (published >1 AND airport_is_active=1)";

            $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', $where);

            $this->outputData['listofairport'] = $this->common->GetAllRowFromTableWhereLimitOrderBy('airports', $where, $start, $tblcolumnsarray[$columnsnumber], $ordertype);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', 'IATA !="" AND published > 0 AND airport_is_active = 1');

            $this->outputData['listofairport'] = $this->common->GetAllRowWithColumn1('airports', '*', 'IATA !="" AND published > 0 AND airport_is_active = 1', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofairport'] as $airport):

            $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

            $parsed_date = date_parse($lastmodified);

            $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

            if ($datee < '2016/3/17') {
                $lastmodified = '-';
            }

            $parsed_datep = date_parse($airport['published']);

            $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

            if ($datep < '2016/3/17') {
                $published = '-';
            } else {

                $published = $this->common->get_formatted_datetime($airport['published']);
            }

            $parsed_datea = date_parse($airport['approved']);

            $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

            if ($dateap < '2016/3/17') {
                $approved = '-';
            } else {
                $approved = $this->common->get_formatted_datetime($airport['approved']);
            }

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $airport['aid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $airport['IATA'], $airport['aname'], $lastmodified,
                $airport['modifiedby'], $published, $approved, $airport['aid'], $airport['category'], '<input type="checkbox" name="pdfrepoert" value="' . $airport['aid'] . '"' . $this->common->AirportPdfArrayVar($airport['aid']) . '>', '<a href="' . site_url("admin/airport/edit_airport/" . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i>

                </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $airport['aid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" aid="' . $airport['aid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End All Airports
     */

    /**
     * 
     * Start Deleted Airports Temporary (Required Id) Admin
     */
    public function list_of_del_airports_pagi() {

        $tblcolumnsarray = array('aid', 'IATA', 'aname', 'lastmodified', 'modifiedby', 'published', 'approved', 'aid', 'category');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        $searchwords = $_GET['search']['value'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $date = str_replace(' ', '_', $searchwords);

            $where = "(`aname` LIKE '%" . $searchwords . "%' OR `IATA` LIKE '%" . $searchwords . "%'OR date_format(`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(`approved`, '%b_%d,%Y') LIKE '%" . $date . "%' OR `modifiedby` LIKE '%" . $searchwords . "%' OR `aid` LIKE '%" . $searchwords . "%') AND (published >1 AND airport_is_active=0 OR published < 1 AND airport_is_active = -1)";

            $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', $where);

            $this->outputData['listofairport'] = $this->common->GetAllRowFromTableWhereLimitOrderBy('airports', $where, $start, $tblcolumnsarray[$columnsnumber], $ordertype);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', 'IATA !="" AND (published > 0 AND airport_is_active = 0 OR published < 1 AND airport_is_active = -1)');

            $this->outputData['listofairport'] = $this->common->GetAllRowWithColumn1('airports', '*', 'IATA !="" AND (published > 0 AND airport_is_active = 0 OR published < 1 AND airport_is_active = -1)', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofairport'] as $airport):

            $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

            $parsed_date = date_parse($lastmodified);

            $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

            if ($datee < '2016/3/17') {
                $lastmodified = '-';
            }

            $parsed_datep = date_parse($airport['published']);

            $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

            if ($datep < '2016/3/17') {
                $published = '-';
            } else {

                $published = $this->common->get_formatted_datetime($airport['published']);
            }

            $parsed_datea = date_parse($airport['approved']);

            $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

            if ($dateap < '2016/3/17') {
                $approved = '-';
            } else {
                $approved = $this->common->get_formatted_datetime($airport['approved']);
            }

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $airport['aid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $airport['IATA'], $airport['aname'], $lastmodified,
                $airport['modifiedby'], $published, $approved, $airport['aid'], $airport['category'], '<input type="checkbox" name="" value="' . $airport['aid'] . '"' . $this->common->AirportPdfArrayVar($airport['aid']) . '>', '<a href="javascript:void(0)" onclick="myFunction(' . $airport['aid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" aid="' . $airport['aid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Deleted Airports Temporary
     */

    /**
     * 
     * Start All FAQ's Category (Required Id) Admin
     */
    public function list_of_all_faq_category() {

        $tblcolumnsarray = array('category_id', 'category_name');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $dataarray = array();

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $like = array('category_name' => $searchwords);

            $orlike = array('category_id' => $searchwords);

            $this->outputData['total'] = $this->common->GetRecordCountLike('tbl_faq_category', $like, $orlike);

            $this->outputData['listofcategory'] = $this->common->GetAllRowFromTableLimitOrderByLike('tbl_faq_category', $this->uri->segment(3), $tblcolumnsarray[$columnsnumber], $ordertype, $like, $orlike);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCount('tbl_faq_category');

            $this->outputData['listofcategory'] = $this->common->GetAllRowWithColumn1(
                    'tbl_faq_category', '*', 'category_name !=""', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $_GET['length']);
        }

        foreach ($this->outputData['listofcategory'] as $category):

            if ($category['is_active'] == 1) {

                $sub = '<label class="label label-success arrowed-in arrowed-in-right" user_id="0">

                                       Active</label> ';
            } else {

                $sub = '<label class="label label-danger arrowed-in arrowed-in-right" user_id="0">

                                       Inactive</label> ';
            }

            $dataarray[] = array($category['category_id'], $category['category_name'], $sub, '<a href="' . site_url('admin/edit_category/' . $category['category_id']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i>

                </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $category['category_id'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" category_id="' . $category['category_id'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End All FAQ's Category
     */

    /**
     * 
     * Start Errorlogs (Required Id) Admin
     */
    public function errors_logs_pagi() {

        $tblcolumnsarray = array('error_log_id', 'error_log_url', 'first_name', 'error_log_subject', 'error_log_message', 'error_log_created_on');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `error_log_url` LIKE '%" . $searchwords . "%' OR `first_name` LIKE '%" . $searchwords . "%' OR `error_log_subject` LIKE '%" . $searchwords . "%' OR `error_log_message` LIKE '%" . $searchwords . "%' OR `error_log_created_on` LIKE '%" . $searchwords . "%' OR date_format(`error_log_created_on`, '%b_%d,%Y') LIKE '%" . $date . "%') AND (error_log_active = 1)";

            $this->outputData['total'] = $this->common->GetRecordCountWhere2('tbl_error_log_history', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'INNER', $where, '');

            $this->outputData['listoflogs'] = $this->common->GetAllRowWithColumnStart('tbl_error_log_history', '*', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'INNER', $where, 'last_name', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->JoinTableQuerytype1("*", "tbl_error_log_history", "users", "tbl_error_log_history.error_log_user_id=users.user_id", $tblcolumnsarray[$columnsnumber], $ordertype, "", true, 'error_log_active = 1');

            $this->outputData['listoflogs'] = $this->common->GetAllRowWithColumnStart('tbl_error_log_history', '*', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'inner', 'error_log_active = 1', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        $type = $this->common->GetAllRowOrderBy('support_ticket_type', 'support_ticket_type_id', 'DESC');

        foreach ($this->outputData['listoflogs'] as $log):

            $selectitem = '';

            $date = $this->common->get_formatted_datetime($log['error_log_created_on']);

            foreach ($type as $li):

                if ($log['error_log_status'] == $li['support_ticket_type_id']) {

                    $selectitem.='<option selected="selected" value="' . $li['support_ticket_type_id'] . '">' . $li['support_ticket_type_name'] . '</option>';
                } else {

                    $selectitem.='<option value="' . $li['support_ticket_type_id'] . '">' . $li['support_ticket_type_name'] . '</option>';
                }

            endforeach;

            $dataarray[] = array('<label><input type="checkbox" class="ace checkboxsingle" id="getall" value="' . $log['error_log_id'] . '"> <span class="lbl"></span> </label>', '<a href="' . $log['error_log_url'] . '" title="' . $log['error_log_url'] . '">Error URL</a>', $log['first_name'] . " " . $log['last_name'], ' <a href="' . site_url("admin/login_to_user/" . $log['error_log_user_id']) . '" class="btn btn-primary btn-xs">Login</a>', $log['error_log_subject'], $log['error_log_message'], $date, '<form method="post"><input name="error_log_id" value="' . $log['error_log_id'] . '" type="hidden"><input type="hidden" name="changestatus" value="changestatus" /><fieldset><select name="errorlogstatus">' . $selectitem . '</select></fieldset><br><button type="submit" class="btn btn-xs btn-success" data-rel="tooltip" title="View"> Update Status <i class="icon-arrow-right icon-on-right bigger-110"></i> </button></form>', '<a href="' . site_url('admin/error_log_view') . "/" . $log['error_log_id'] . '" class="tooltip-success" data-rel="tooltip" title="View"> <span class="green"> <i class="icon-ok bigger-120"></i></span> </a> &nbsp;&nbsp; <a href="' . site_url('admin/edit_error_log') . "/" . $log['error_log_id'] . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i>

                </span> </a> &nbsp;&nbsp; <a href="javascript:void(0)" onclick="myFunction(' . $log['error_log_id'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" error_log_id="' . $log['error_log_id'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End  Errorlogs
     */

    /**
     * 
     * Start Deleted Errorlogs Temporary (Required Id) Admin
     */
    public function del_errors_logs_pagi() {

        $tblcolumnsarray = array('error_log_id', 'error_log_url', 'first_name', '', 'error_log_subject', 'error_log_message', 'error_log_created_on');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `error_log_url` LIKE '%" . $searchwords . "%' OR `first_name` LIKE '%" . $searchwords . "%' OR `error_log_subject` LIKE '%" . $searchwords . "%' OR `error_log_message` LIKE '%" . $searchwords . "%' OR `error_log_created_on` LIKE '%" . $searchwords . "%' OR date_format(`error_log_created_on`, '%b_%d,%Y') LIKE '%" . $date . "%') AND (error_log_active = 0)";

            $this->outputData['total'] = $this->common->GetRecordCountWhere2('tbl_error_log_history', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'INNER', $where, '');

            $this->outputData['listoflogs'] = $this->common->GetAllRowWithColumnStart('tbl_error_log_history', '*', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'INNER', $where, 'last_name', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->JoinTableQuerytype1("*", "tbl_error_log_history", "users", "tbl_error_log_history.error_log_user_id=users.user_id", $tblcolumnsarray[$columnsnumber], $ordertype, "", true, 'error_log_active = 0');

            $this->outputData['listoflogs'] = $this->common->GetAllRowWithColumnStart('tbl_error_log_history', '*', 'users', 'tbl_error_log_history.error_log_user_id=users.user_id', 'inner', 'error_log_active = 0', 'error_log_id', $ordertype, $_GET['start'], $length);
        }

        $type = $this->common->GetAllRowOrderBy('support_ticket_type', 'support_ticket_type_id', 'DESC');

        foreach ($this->outputData['listoflogs'] as $log):

            $selectitem = '';

            $date = $this->common->get_formatted_datetime($log['error_log_created_on']);

            foreach ($type as $li):

                if ($log['error_log_status'] == $li['support_ticket_type_id']) {

                    $selectitem.='<option selected="selected" value="' . $li['support_ticket_type_id'] . '">' . $li['support_ticket_type_name'] . '</option>';
                } else {

                    $selectitem.='<option value="' . $li['support_ticket_type_id'] . '">' . $li['support_ticket_type_name'] . '</option>';
                }

            endforeach;

            $dataarray[] = array('<label><input type="checkbox" class="ace checkboxsingle" id="getall" value="' . $log['error_log_id'] . '"> <span class="lbl"></span> </label>', '<a href="' . $log['error_log_url'] . '" title="' . $log['error_log_url'] . '">Error URL</a>', $log['first_name'] . " " . $log['last_name'], ' <a href="' . site_url("admin/login_to_user/" . $log['error_log_user_id']) . '" class="btn btn-primary btn-xs">Login</a>', $log['error_log_subject'], $log['error_log_message'], $date, '<form method="post"><input name="error_log_id" value="' . $log['error_log_id'] . '" type="hidden"><input type="hidden" name="changestatus" value="changestatus" /><fieldset><select name="errorlogstatus">' . $selectitem . '</select></fieldset><br><button type="submit" class="btn btn-xs btn-success" data-rel="tooltip" title="View"> Update Status <i class="icon-arrow-right icon-on-right bigger-110"></i> </button></form>', '<a href="javascript:void(0)" onclick="myFunction(' . $log['error_log_id'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" error_log_id="' . $log['error_log_id'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Deleted Errorlogs Temporary
     */

    /**
     * 
     * Start Not Published Airports (Required Id) Admin
     */
    public function list_of_not_published_airports_pagination() {

        $tblcolumnsarray = array('aid', 'IATA', 'aname', 'lastmodified', 'published', 'approved', 'aid', 'category');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = '';

            $where.= "(aname LIKE '%" . $searchwords . "%' OR IATA LIKE '%" . $searchwords . "%' OR date_format(`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(`approved`, '%b_%d,%Y') LIKE '%" . $date . "%' OR `aid` LIKE '%" . $searchwords . "%')";

            $where.= ' AND published <1 AND airport_is_active != -1';

            $this->outputData['total'] = $this->common->GetTotalCount('airports', $where);

            $this->outputData['listofairport'] = $this->common->GetAllRowFromTableWhereLimitOrderBy('airports', $where, $start, $tblcolumnsarray[$columnsnumber], $ordertype);
        } else {

            $this->outputData['total'] = $this->common->GetTotalFromTableWhere('airports', 'published <1 AND airport_is_active != -1');

            $this->outputData['listofairport'] = $this->common->GetAllRowWithColumn1('airports', '*', 'published < 1 AND airport_is_active != -1', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofairport'] as $airport):

            $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

            $parsed_date = date_parse($lastmodified);

            $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

            if ($datee < '2016/3/17') {
                $lastmodified = '-';
            }

            $parsed_datep = date_parse($airport['published']);

            $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

            if ($datep < '2016/3/17') {
                $airport['published'] = '-';
            }

            $parsed_datea = date_parse($airport['approved']);

            $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

            if ($dateap < '2016/3/17') {
                $airport['approved'] = '-';
            }

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $airport['aid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $airport['IATA'], $airport['aname'], $lastmodified,
                $airport['published'], $airport['approved'], $airport['aid'], $airport['category'], '<input type="checkbox" name="" value="' . $airport['aid'] . '"' . $this->common->AirportPdfArrayVar($airport['aid']) . '>', '<a href="' . site_url("admin/airport/edit_airport/" . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i>

                </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $airport['aid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" aid="' . $airport['aid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Not Published Airports 
     */

    /**
     * 
     * Start Get Affiliations (Required Id)
     */
    public function list_of_affiliations_pagi() {

        $this->common->ThisSecureArea('user_id');

        $tblcolumnsarray = array('last_name', 'first_name', 'IATA', 'companyname');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $LIMIT = 'LIMIT ' . $_GET['start'] . ',' . $length;

            $searchwords = $_GET['search']['value'];

            $where = " WHERE users.is_active=1 AND affiliation.affi_is_active=1 AND (airports.airport_is_active=1 OR companies.comp_is_active=1)  AND (last_name LIKE '%" . $searchwords . "%' 

				OR first_name LIKE '%" . $searchwords . "%' 
                                    
				OR IATA LIKE '%" . $searchwords . "%' 

				OR companyname LIKE '%" . $searchwords . "%')";

            $sql = "select * from affiliation 

			  			left join users on affiliation.user_id=users.user_id

						left join airports on affiliation.aid=airports.aid

			   			left join companies on affiliation.cid=companies.cid" . $where . " ORDER BY {$tblcolumnsarray[$columnsnumber]} {$ordertype} " . $LIMIT;

            $countq = "select * from affiliation 

			  				left join users on affiliation.user_id=users.user_id

							left join airports on affiliation.aid=airports.aid

			   				left join companies on affiliation.cid=companies.cid" . $where . "";

            $cur = $this->common->CustomQueryALL($sql);

            $this->outputData['total'] = $this->common->CustomCountQuery($countq);
        } else {

            $LIMIT = 'LIMIT ' . $_GET['start'] . ',' . $length;

            $sql = "select * from affiliation 

			  			left join users on affiliation.user_id=users.user_id

						left join airports on affiliation.aid=airports.aid

			   			left join companies on affiliation.cid=companies.cid WHERE users.is_active=1 AND (airports.airport_is_active=1 OR companies.comp_is_active=1) AND affiliation.affi_is_active=1 ORDER BY {$tblcolumnsarray[$columnsnumber]} {$ordertype} " . $LIMIT;

            $countq = "select * from affiliation 

			  			left join users on affiliation.user_id=users.user_id

						left join airports on affiliation.aid=airports.aid

			   			left join companies on affiliation.cid=companies.cid WHERE users.is_active=1 AND (airports.airport_is_active=1 OR companies.comp_is_active=1) AND affiliation.affi_is_active=1 ";

            $cur = $this->common->CustomQueryALL($sql);

            $this->outputData['total'] = $this->common->CustomCountQuery($countq);
        }

        foreach ($cur as $val):

            $myarray['last_name'] = $val['last_name'];

            $myarray['first_name'] = $val['first_name'];

            $myarray['IATA'] = $val['IATA'];

            $myarray['companyname'] = $val['companyname'];

            $myarray['affid'] = '<a href="javascript:void()" title="Delete this affiliations"><i class="fa fa-times" onclick="delme(' . $val['affid'] . ')"></i></a>';

            $dataarray[] = array_values($myarray);

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Affiliations
     */

    /**
     * 
     * Start All Tenants (Required ID) Frontend
     */
    public function list_of_tenants_pagi() {

        $tblcolumnsarray = array('IATA', 'termlocation', 'lkpcategory.category', 'outletname', 'productdescription', 'companyname', 'numlocations', 'sqft', 'exp');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $dataarray = array();

        $searchwords = $_GET['search']['value'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $LIMIT = 'LIMIT ' . $_GET['start'] . ',' . $length;

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "airports.airport_is_active = 1 AND outlets.odeleted = 0 AND (airports.IATA LIKE '%" . $searchwords . "%' 

				OR lkpcategory.category LIKE '%" . $searchwords . "%'
          
				OR lkpcategory.productdescription LIKE '%" . $searchwords . "%' OR outlets.companyname LIKE '%" . $searchwords . "%'
				 
				OR outlets.outletname LIKE '%" . $searchwords . "%' OR date_format(outlets.`exp`, '%b_%d,%Y') LIKE '%" . $date . "%')";

            $join_array = array(
                array('lkpcategory', 'lkpcategory.categoryid=outlets.categoryid', 'left'),
                array('airports', 'airports.aid = outlets.aid', 'left')
            );

            $this->outputData['totalrecord'] = $this->common->JoinTableQueryCount2('outlets', $where, $join_array);

            $this->outputData['listoftenants'] = $this->common->JoinTableQuerytype2('*, lkpcategory.category as category', 'outlets', $join_array, $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], 0, $where);
        } else {

            $LIMIT = 'LIMIT ' . $_GET['start'] . ',' . $length;

            $where = 'airports.airport_is_active = 1 AND outlets.odeleted = 0';

            $join_array = array(
                array('lkpcategory', 'lkpcategory.categoryid=outlets.categoryid', 'left'),
                array('airports', 'airports.aid = outlets.aid', 'left')
            );

            $this->outputData['totalrecord'] = $this->common->JoinTableQueryCount2('outlets', $where, $join_array);

            $this->outputData['listoftenants'] = $this->common->JoinTableQuerytype2('*, lkpcategory.category as category', 'outlets', $join_array, $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], 0, $where);
        }

        foreach ($this->outputData['listoftenants'] as $user):

            if ($user['exp_reason'] != '') {

                $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $user['exp_reason']);

                if ($user['exp_date'] != '0000-00-00 00:00:00' && $user['exp_date'] != '' & $user['exp_reason'] != 'M-T-M') {
                    $expiredate = $reasons . '<br>' . date("M d,Y H:i:s", strtotime($user['exp_date']));
                } else if (($user['exp_date'] == '0000-00-00 00:00:00' || $user['exp_date'] == '') && $user['exp_reason'] != 'M-T-M') {
                    $expiredate = $reasons;
                } else {
                    $expiredate = $reasons;
                }
            } else {

                if ($user['exp'] != '') {
                    $expiredate = $this->common->get_formatted_datetime($user['exp']);
                } else {
                    $expiredate = '';
                }
            }

            $ten = number_format($user['sqft']);

            $dataarray[] = array($user['IATA'], $user['termlocation'], $user['category'], $user['outletname'], $user['productdescription'], $user['companyname'], $user['numlocations'], $ten, $expiredate);

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['totalrecord'] . ',

			  		"recordsFiltered": ' . $this->outputData['totalrecord'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End All Tenants
     */

    /**
     * 
     * Start All Users (Required ID) Admin
     */
    public function list_of_users_pagi() {

        $tblcolumnsarray = array('user_id', 'last_name', 'first_name', 'company', 'state_name', 'work_phone', 'work_phone_ext', 'email', 'user_id');

        $dataarray = array();

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $start = $_GET['start'];

            $where = '(`last_name` LIKE "%' . $searchwords . '%" OR `first_name` LIKE "%' . $searchwords . '%" OR `company` LIKE "%' . $searchwords . '%" OR `state_name` LIKE "%' . $searchwords . '%" OR `work_phone` LIKE "%' . $searchwords . '%" OR `work_phone_ext` LIKE "%' . $searchwords . '%" OR `email` LIKE "%' . $searchwords . '%" OR `user_id` LIKE "%' . $searchwords . '%") AND (user_id > 0 AND is_active = 1)';

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'users', 'states', 'states.states_id=users.state', 'LEFT', $where);

            $this->outputData['listofuser'] = $this->common->GetAllRowWithColumnStart('users', '*', 'states', 'states.states_id=users.state', 'LEFT', $where, 'last_name', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'users', 'states', 'states.states_id=users.state', 'LEFT', 'user_id > 0 AND is_active =1');

            $this->outputData['listofuser'] = $this->common->GetAllRowWithColumnStart('users', 'users.*,states.state_name', 'states', 'states.states_id=users.state', 'LEFT', 'user_id > 0 AND is_active =1', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofuser'] as $user):

            if ($user['fbosubscription'] == 1) {

                $sub = '<label class="label label-success arrowed-in arrowed-in-right" user_id="0">

                                       <i class="icon-plus"></i></label> 

                                       	<label title="Drop Subscription" class="label label-danger arrowed-in arrowed-in-right upsub" onclick="nomi(' . $user['user_id'] . ',0);" user_id="' . $user['user_id'] . '" style="cursor:pointer;" subscription="0" ><i class="icon-minus"></i></label>';
            } else {

                $sub = '<label class="label label-danger arrowed-in arrowed-in-right">

                                       <i class="icon-minus"></i></label>                                       

                                        <label title="Add Subscription" class="label label-success arrowed-in arrowed-in-right upsub" onclick="nomi(' . $user['user_id'] . ',1);" user_id="' . $user['user_id'] . '" style="cursor:pointer;" subscription="1" ><i class="icon-plus"></i></label>';
            };

            $dataarray[] = array('<input type="checkbox"  name="getall[]" value="' . $user['user_id'] . '" class="checkboxsingle" id="getall" class="getevery" />', $user['last_name'], $user['first_name'],
                $user['company'], $user['state_name'], $user['work_phone'], $user['work_phone_ext'], '<a href="mailto:' . $user['email'] . '"> ' . $user['email'] . '</a>', $user['user_id'], $sub, '<a href="' . site_url('admin/edit_user/' . $user['user_id']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i>

                    </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $user['user_id'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" user_id="' . $user['user_id'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End All Users
     */

    /**
     * 
     * Start Deleted Users Temporary (Required ID) Admin
     */
    public function list_of_del_users_pagi() {

        $tblcolumnsarray = array('user_id', 'last_name', 'first_name', 'company', 'state_name', 'work_phone', 'work_phone_ext', 'email', 'user_id');

        $dataarray = array();

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $where = '(`last_name` LIKE "%' . $searchwords . '%" OR `first_name` LIKE "%' . $searchwords . '%" OR `company` LIKE "%' . $searchwords . '%" OR `state_name` LIKE "%' . $searchwords . '%" OR `work_phone` LIKE "%' . $searchwords . '%" OR `work_phone_ext` LIKE "%' . $searchwords . '%" OR `email` LIKE "%' . $searchwords . '%" OR `user_id` LIKE "%' . $searchwords . '%") AND (user_id > 0 AND is_active = 0)';

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'users', 'states', 'states.states_id=users.state', 'LEFT', $where);

            $this->outputData['listofuser'] = $this->common->GetAllRowWithColumnStart('users', '*', 'states', 'states.states_id=users.state', 'LEFT', $where, 'last_name', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->JoinTableQuerytype1("users.*,states.state_name", "users", "states", "states.states_id=users.state", $tblcolumnsarray[$columnsnumber], $ordertype, "", true, 'user_id > 0 AND is_active =0');

            $this->outputData['listofuser'] = $this->common->GetAllRowWithColumnStart('users', 'users.*,states.state_name', 'states', 'states.states_id=users.state', 'LEFT', 'user_id > 0 AND is_active =0', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofuser'] as $user):

            if ($user['fbosubscription'] == 1) {

                $sub = '<label class="label label-success arrowed-in arrowed-in-right" user_id="0">

                                       <i class="icon-plus"></i></label> 

                                       	<label title="Drop Subscription" class="label label-danger arrowed-in arrowed-in-right upsub" user_id="' . $user['user_id'] . '" style="cursor:pointer;" subscription="0" ><i class="icon-minus"></i></label>';
            } else {

                $sub = '<label class="label label-danger arrowed-in arrowed-in-right">

                                       <i class="icon-minus"></i></label>                                       

                                        <label title="Add Subscription" class="label label-success arrowed-in arrowed-in-right upsub" user_id="' . $user['user_id'] . '" style="cursor:pointer;" subscription="1" ><i class="icon-plus"></i></label>';
            };

            $dataarray[] = array('<input type="checkbox"  name="getall[]" value="' . $user['user_id'] . '" class="checkboxsingle" id="getall" class="getevery" />', $user['last_name'], $user['first_name'],
                $user['company'], $user['state_name'], $user['work_phone'], $user['work_phone_ext'], '<a href="mailto:' . $user['email'] . '"> ' . $user['email'] . '</a>', $user['user_id'], $sub, '<a href="javascript:void(0)" onclick="myFunction(' . $user['user_id'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" user_id="' . $user['user_id'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Deleted Users Temporary
     */

    /**
     * 
     * Start All User's Activity (Required ID) 
     */
    public function list_of_users_acivity_pagi() {

        $tblcolumnsarray = array('id', 'acitivity', 'created_at');

        $dataarray = array();

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $start = $_GET['start'];

            $where = 'activity LIKE "%' . $searchwords . '%"';

            $this->outputData['total'] = $this->common->GetTotalCount('tbl_user_activity_log', $where);

            $this->outputData['listofuseracivity'] = $this->common->GetAllRowWithColumn1('tbl_user_activity_log', '*', $where, $tblcolumnsarray[$columnsnumber], $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCount('tbl_user_activity_log');

            $this->outputData['listofuseracivity'] = $this->common->GetAllRowWithColumn1('tbl_user_activity_log', '*', 'id != 0', $tblcolumnsarray[$columnsnumber], $ordertype, $start, $length);
        }

        foreach ($this->outputData['listofuseracivity'] as $useractivity):

            $dataarray[] = array($useractivity['id'], $useractivity['activity'], $useractivity['created_at']);

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End All User's Activity
     */

    /**
     * 
     * Start FAQ's Feedback (Required ID) Admin
     */
    public function list_faq_feedback() {

        $tblcolumnsarray = array('faq_id', 'faq_id', 'first_name', 'email', 'faq_question', 'faq_answer', 'feedback_status', 'last_name');

        $dataarray = array();

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $start = $_GET['start'];

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `email` LIKE '%" . $searchwords . "%' OR `faq_question` LIKE '%" . $searchwords . "%' OR `faq_answer` LIKE '%" . $searchwords . "%') AND (tbl_faq_feedback.faq_id > 0 AND is_active = 1)";

            $this->outputData['total'] = $this->common->JoinTableCount('tbl_faq_feedback.*', 'tbl_faq_feedback', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', $where);

            $this->outputData['listoffaq'] = $this->common->GetAllRowWithColumnStart('tbl_faq_feedback', 'tbl_faq_feedback.feedback_status,tbl_faq_feedback.faq_id,tbl_faq_feedback.feedback_id,tbl_faq_feedback.first_name,tbl_faq_feedback.last_name,tbl_faq_feedback.email, tbl_faq_feedback.reason, tbl_faq_feedback.admins_status, tbl_faq.faq_question, tbl_faq.faq_answer', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', $where, '`tbl_faq_feedback`.`faq_id`', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->JoinTableCount('tbl_faq_feedback.*', 'tbl_faq_feedback', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', array('tbl_faq_feedback.faq_id >' => 0, 'is_active' => 1));

            $this->outputData['listoffaq'] = $this->common->GetAllRowWithColumnStart('tbl_faq_feedback', 'tbl_faq_feedback.feedback_status,tbl_faq_feedback.faq_id,tbl_faq_feedback.feedback_id,tbl_faq_feedback.first_name,tbl_faq_feedback.last_name,tbl_faq_feedback.email, tbl_faq_feedback.reason, tbl_faq_feedback.admins_status, tbl_faq.faq_question, tbl_faq.faq_answer', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', array('tbl_faq_feedback.faq_id >' => 0, 'is_active' => 1), $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listoffaq'] as $user):

            $email = '<a href="mailto:' . $user['email'] . '">' . $user["email"] . '</a>';

            if ($user['feedback_status'] == 1) {

                $sub = '<label class="label label-success arrowed-in arrowed-in-right">Helpfull</label>';
            } else {

                $sub = '<label class="label label-danger arrowed-in arrowed-in-right">Not Helpfull</label>';
            }

            if ($user['feedback_status'] != 1) {

                $useremail = "'" . $user['email'] . "'";
                $username = "'" . $user['first_name'] . ' ' . $user['last_name'] . "'";

                if ($user['admins_status'] == '0') {
                    $admin_status = '<i class="icon-thumbs-up bigger-120 green" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 1, ' . $useremail . ', ' . $username . ');" style="cursor:pointer;"></i> &nbsp; <i class="icon-thumbs-down bigger-120 red" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 2);" style="cursor:pointer;"></i>';
                } else
                if ($user['admins_status'] == '1') {
                    $admin_status = '<i class="icon-thumbs-up bigger-160 green" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 1, ' . $useremail . ', ' . $username . ');" style="cursor:pointer;"></i> &nbsp; <i class="icon-thumbs-down bigger-90 red" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 2);" style="cursor:pointer;"></i> ';
                } else
                if ($user['admins_status'] == '2') {
                    $admin_status = '<i class="icon-thumbs-up bigger-90 green" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 1, ' . $useremail . ', ' . $username . ');" style="cursor:pointer;"></i> &nbsp;<i class="icon-thumbs-down bigger-160 red" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 2);" style="cursor:pointer;"></i>';
                }
            } else {
                $admin_status = '';
            }

            $dataarray[] = array('<input type="checkbox"  name="getall[]" value="' . $user['feedback_id'] . '" class="checkboxsingle" id="getall" class="getevery" />', $user['faq_id'], $user['first_name'] . " " . $user['last_name'], $email, $user['faq_question'],
                $user['faq_answer'], $user['reason'], $admin_status, $sub);

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End FAQ's Feedback 
     */

    /**
     * 
     * Start Deleted FAQ's Feedback Temporary (Required ID) Admin
     */
    public function list_deleted_faq_feedback() {

        $tblcolumnsarray = array('faq_id', 'faq_id', 'first_name', 'email', 'faq_question', 'faq_answer', 'feedback_status', 'last_name');

        $dataarray = array();

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $start = $_GET['start'];

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `email` LIKE '%" . $searchwords . "%' OR `faq_question` LIKE '%" . $searchwords . "%' OR `faq_answer` LIKE '%" . $searchwords . "%') AND (tbl_faq_feedback.faq_id > 0 AND is_active = 0)";

            $this->outputData['total'] = $this->common->JoinTableCount('tbl_faq_feedback.*', 'tbl_faq_feedback', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', $where);

            $this->outputData['listoffaq'] = $this->common->GetAllRowWithColumnStart('tbl_faq_feedback', '*', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', $where, '`tbl_faq_feedback`.`faq_id`', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->JoinTableCount('tbl_faq_feedback.*', 'tbl_faq_feedback', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', array('tbl_faq_feedback.faq_id >' => 0, 'is_active' => 0));

            $this->outputData['listoffaq'] = $this->common->GetAllRowWithColumnStart('tbl_faq_feedback', 'tbl_faq_feedback.feedback_status,tbl_faq_feedback.faq_id,tbl_faq_feedback.feedback_id,tbl_faq_feedback.first_name,tbl_faq_feedback.last_name,tbl_faq_feedback.email, tbl_faq_feedback.admins_status, tbl_faq_feedback.reason, tbl_faq.faq_question, tbl_faq.faq_answer', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', array('tbl_faq_feedback.faq_id >' => 0, 'is_active' => 0), $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listoffaq'] as $user):

            $email = '<a href="mailto:' . $user['email'] . '">' . $user["email"] . '</a>';

            if ($user['feedback_status'] != 1) {

                $useremail = "'" . $user['email'] . "'";
                $username = "'" . $user['first_name'] . ' ' . $user['last_name'] . "'";

                if ($user['admins_status'] == '0') {
                    $admin_status = '<i class="icon-thumbs-up bigger-120 green" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 1, ' . $useremail . ', ' . $username . ');" style="cursor:pointer;"></i> &nbsp; <i class="icon-thumbs-down bigger-120 red" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 2);" style="cursor:pointer;"></i>';
                } else
                if ($user['admins_status'] == '1') {
                    $admin_status = '<i class="icon-thumbs-up bigger-160 green" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 1, ' . $useremail . ', ' . $username . ');" style="cursor:pointer;"></i> &nbsp; <i class="icon-thumbs-down bigger-90 red" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 2);" style="cursor:pointer;"></i> ';
                } else
                if ($user['admins_status'] == '2') {
                    $admin_status = '<i class="icon-thumbs-up bigger-90 green" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 1, ' . $useremail . ', ' . $username . ');" style="cursor:pointer;"></i> &nbsp;<i class="icon-thumbs-down bigger-160 red" aria-hidden="true" onclick="like(' . $user['feedback_id'] . ', 2);" style="cursor:pointer;"></i>';
                }
            } else {
                $admin_status = '';
            }

            if ($user['feedback_status'] == 1) {

                $sub = '<label class="label label-success arrowed-in arrowed-in-right">Helpfull</label>';
            } else {

                $sub = '<label class="label label-danger arrowed-in arrowed-in-right">Not Helpfull</label>';
            };

            $dataarray[] = array('<input type="checkbox"  name="getall[]" value="' . $user['feedback_id'] . '" class="checkboxsingle" id="getall" class="getevery" />', $user['faq_id'], $user['first_name'] . " " . $user['last_name'], $email, $user['faq_question'],
                $user['faq_answer'], $user['reason'], $sub);

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Deleted FAQ's Feedback Temporary 
     */

    /**
     * 
     * Start Support Questions (Required ID) Admin
     */
    public function list_of_support_pagi() {

        $tblcolumnsarray = array('support_id', 'support_id', 'ticket_created_date', 'subject', '', 'first_name', 'email');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `first_name` LIKE '%" . $searchwords . "%' OR `last_name` LIKE '%" . $searchwords . "%' OR `subject` LIKE '%" . $searchwords . "%' OR `email` LIKE '%" . $searchwords . "%' OR date_format(`ticket_created_date`, '%b_%d,%Y') LIKE '%" . $date . "%') AND (`support`.`is_active` = 1)";

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'support', 'users', 'support.user_id = users.user_id', 'LEFT', $where);

            $this->outputData['listoftickets'] = $this->common->GetAllRowWithColumnStart('support', '*', 'users', 'support.user_id = users.user_id', 'LEFT', $where, 'support_id', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('support', 'is_active = 1');

            $this->outputData['listoftickets'] = $this->common->GetAllRowWithColumnStart('support', 'support.support_id, support.status, subject, ticket_created_date, support.is_active, first_name, last_name, email', 'users', 'support.user_id = users.user_id', 'inner', array('support.user_id >' => 0, 'support.is_active' => '1'), $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listoftickets'] as $ticket):

            $date = $this->common->get_formatted_datetime($ticket['ticket_created_date']);

            if ($ticket['status'] == 1 || $ticket['status'] == 0) {

                $sub = '<span class="label label-info">Open</span>';
            } else if ($ticket['status'] == 2) {

                $sub = '<span class="label label-success">Closed</span>';
            } else if ($ticket['status'] == 3) {

                $sub = '<span class="label label-danger">Re-open</span>';
            }

            $dataarray[] = array('<input type="checkbox"  name="getall[]" value="' . $ticket['support_id'] . '" class="checkboxsingle" id="getall" class="getevery" />', $ticket['support_id'], $date, $ticket['subject'], $sub, $ticket['first_name'] . " " . $ticket['last_name'], '<a href="mailto:' . $ticket['email'] . '"> ' . $ticket['email'] . '</a>', '<a href="' . site_url('admin/edit_tickets/' . $ticket['support_id']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i>

                </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $ticket['support_id'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" user_id="' . $ticket['support_id'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a> &nbsp;&nbsp; <a href="' . site_url('admin/view_single_ticket/' . $ticket['support_id']) . '">  <i class="icon-eye-open bigger-130" data-rel="tooltip" title="View"></i>

                 </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Support Questions
     */

    /**
     * 
     * Start Deleted Support Questions Temporary (Required ID) Admin
     */
    public function list_of_del_support_pagi() {

        $tblcolumnsarray = array('support_id', 'support_id', 'ticket_created_date', 'subject', '', 'first_name', 'email');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `first_name` LIKE '%" . $searchwords . "%' OR `last_name` LIKE '%" . $searchwords . "%' OR `subject` LIKE '%" . $searchwords . "%' OR `email` LIKE '%" . $searchwords . "%' OR date_format(`ticket_created_date`, '%b_%d,%Y') LIKE '%" . $date . "%') AND (`support`.`is_active` = 0)";

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'support', 'users', 'support.user_id = users.user_id', 'LEFT', $where);

            $this->outputData['listoftickets'] = $this->common->GetAllRowWithColumnStart('support', '*', 'users', 'support.user_id = users.user_id', 'LEFT', $where, 'support_id', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('support', 'is_active = 0');

            $this->outputData['listoftickets'] = $this->common->GetAllRowWithColumnStart('support', 'support.support_id, support.status, subject, ticket_created_date, support.is_active, first_name, last_name, email', 'users', 'support.user_id = users.user_id', 'inner', array('support.user_id >' => 0, 'support.is_active' => '0'), $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listoftickets'] as $ticket):

            $date = $this->common->get_formatted_datetime($ticket['ticket_created_date']);

            if ($ticket['status'] == 1 || $ticket['status'] == 0) {

                $sub = '<span class="label label-info">Open</span>';
            } else if ($ticket['status'] == 2) {

                $sub = '<span class="label label-success">Closed</span>';
            } else if ($ticket['status'] == 3) {

                $sub = '<span class="label label-danger">Re-open</span>';
            }

            $dataarray[] = array('<input type="checkbox"  name="getall[]" value="' . $ticket['support_id'] . '" class="checkboxsingle" id="getall" class="getevery" />', $ticket['support_id'], $date, $ticket['subject'], $sub, $ticket['first_name'] . " " . $ticket['last_name'], '<a href="mailto:' . $ticket['email'] . '"> ' . $ticket['email'] . '</a>',
                '<a href="javascript:void(0)" onclick="myFunction(' . $ticket['support_id'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" user_id="' . $ticket['support_id'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a> &nbsp;&nbsp; <a href="' . site_url('admin/view_single_ticket/' . $ticket['support_id']) . '">  <i class="icon-eye-open bigger-130" data-rel="tooltip" title="Delete"></i> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Deleted Support Questions Temporary
     */

    /**
     * 
     * Start User Logged In (Required ID) Admin
     */
    public function login_report_pagi() {

        $tblcolumnsarray = array('lid', 'user_id', 'first_name', 'email', 'login', 'logout');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `users`.`last_name` LIKE '%" . $searchwords . "%' OR `users`.`first_name` LIKE '%" . $searchwords . "%' OR `users`.`email` LIKE '%" . $searchwords . "%' OR `users`.`user_id` LIKE '%" . $searchwords . "%' OR date_format(logging.`login`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(logging.`logout`, '%b_%d,%Y') LIKE '%" . $date . "%') AND (lid > 0 AND log_is_active = 1)";

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'logging', 'users', 'logging.user_id=users.user_id', 'LEFT', $where);

            $this->outputData['logging'] = $this->common->GetAllRowWithColumnStart('logging', '*', 'users', 'logging.user_id=users.user_id', 'LEFT', $where, 'login', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('logging', 'lid > 0 AND log_is_active =1');

            $this->outputData['logging'] = $this->common->GetAllRowWithColumnStart('logging', 'logging.*,users.first_name,users.email,users.last_name', 'users', 'logging.user_id=users.user_id', 'LEFT', 'lid > 0 AND log_is_active =1', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['logging'] as $login):

            if ($login['login'] == '0000-00-00 00:00:00') {

                $logindate = '0000-00-00';
            } else {

                $logindate = $this->common->get_formatted_datetime_logging($login['login']);
            }

            if ($login['logout'] == '0000-00-00 00:00:00') {

                $logoutdate = '0000-00-00';
            } else {

                $logoutdate = $this->common->get_formatted_datetime_logging($login['logout']);
            }

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $login['lid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $login['user_id'], $login['first_name'] . ' ' . $login['last_name'], '<a href="mailto:' . $login['email'] . '"> ' . $login['email'] . '</a>', $logindate, $logoutdate);

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End User Logged In
     */

    /**
     * 
     * Start User Deleted Logged In Temporary (Required ID) Admin
     */
    public function del_login_report_pagi() {

        $tblcolumnsarray = array('lid', 'user_id', 'first_name', 'email', 'login', 'logout');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "(concat_ws(' ',first_name,last_name) LIKE '%" . $searchwords . "%' OR `users`.`last_name` LIKE '%" . $searchwords . "%' OR `users`.`first_name` LIKE '%" . $searchwords . "%' OR `users`.`email` LIKE '%" . $searchwords . "%' OR `users`.`user_id` LIKE '%" . $searchwords . "%' OR date_format(logging.`login`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(logging.`logout`, '%b_%d,%Y') LIKE '%" . $date . "%') AND (lid > 0 AND log_is_active = 0)";

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'logging', 'users', 'logging.user_id=users.user_id', 'LEFT', $where);

            $this->outputData['logging'] = $this->common->GetAllRowWithColumnStart('logging', '*', 'users', 'logging.user_id=users.user_id', 'LEFT', $where, 'login', $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('logging', 'lid > 0 AND log_is_active =0');

            $this->outputData['logging'] = $this->common->GetAllRowWithColumnStart('logging', 'logging.*,users.first_name,users.email,users.last_name', 'users', 'logging.user_id=users.user_id', 'LEFT', 'lid > 0 AND log_is_active =0', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['logging'] as $login):

            if ($login['login'] == '0000-00-00 00:00:00') {

                $logindate = '0000-00-00';
            } else {

                $logindate = $this->common->get_formatted_datetime_logging($login['login']);
            }

            if ($login['logout'] == '0000-00-00 00:00:00') {

                $logoutdate = '0000-00-00';
            } else {

                $logoutdate = $this->common->get_formatted_datetime_logging($login['logout']);
            }

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $login['lid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $login['user_id'], $login['first_name'] . ' ' . $login['last_name'], '<a href="mailto:' . $login['email'] . '"> ' . $login['email'] . '</a>', $logindate, $logoutdate);

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End User Deleted Logged In Temporary
     */

    /**
     * 
     * Start Companies Published (Required ID) Admin
     */
    public function list_of_company_pagi() {

        $tblcolumnsarray = array('cid', 'dbe', 'companyname', 'companytype', 'phone', 'city', 'state',);

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $like = array('companyname' => $searchwords);

            $orlike = array('city' => $searchwords, 'phone' => $searchwords, 'state_name' => $searchwords, 'companytype' => $searchwords);

            $this->outputData['total'] = $this->common->GetRecordCountLikeWhere2('companies', 'states', 'companies.state=states.state_code', 'LEFT', $like, $orlike, 'cid > 0 AND published > 0 AND comp_is_active = 1');

            $this->outputData['listofcompany'] = $this->common->GetAllRowFromTableLimitOrderByLikeWhere2('companies', 'states', 'companies.state=states.state_code', 'LEFT', $this->uri->segment(2), $tblcolumnsarray[$columnsnumber], $ordertype, $like, $orlike, 'cid > 0 AND published >0 AND comp_is_active= 1');
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('companies', 'cid > 0 AND published >0 AND comp_is_active= 1');

            $this->outputData['listofcompany'] = $this->common->GetAllRowWithColumnStart('companies', '*', 'states', 'companies.state=states.state_code', 'LEFT', 'cid > 0 AND published >0 AND comp_is_active= 1', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofcompany'] as $row_rsCompany):

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $row_rsCompany['cid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $row_rsCompany['dbe'], $row_rsCompany['companyname'], $row_rsCompany['companytype'],
                $row_rsCompany['phone'], $row_rsCompany['city'], $row_rsCompany['state_name'], '<input type="checkbox" name="pdfrepoert" value="' . $row_rsCompany['cid'] . '"' . $this->common->CompanyPdfArrayVar($row_rsCompany['cid']) . '>', '<a href="' . site_url('admin/company/edit_company/' . $row_rsCompany['cid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i> </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $row_rsCompany['cid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" cid="' . $row_rsCompany['cid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Companies Published
     */

    /**
     * 
     * Start Companies deleted Temporary (Required ID) Admin
     */
    public function list_of_del_company_pagi() {

        $tblcolumnsarray = array('cid', 'dbe', 'companyname', 'companytype', 'phone', 'city', 'state',);

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $where = "(`companies`.`companyname` LIKE '%" . $searchwords . "%' OR `companies`.`city` LIKE '%" . $searchwords . "%' OR `companies`.`companytype` LIKE '%" . $searchwords . "%' OR `companies`.`phone` LIKE '%" . $searchwords . "%' OR `states`.`state_name` LIKE '%" . $searchwords . "%') AND cid > 0 AND (published >0 AND comp_is_active= 0 OR published < 1 AND comp_is_active= -1)";

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'companies', 'states', 'companies.state=states.state_code', 'LEFT', $where);

            $this->outputData['listofcompany'] = $this->common->GetAllRowWithColumnStart('companies', '*', 'states', 'companies.state=states.state_code', 'LEFT', $where, $tblcolumnsarray[$columnsnumber], $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountWhere('companies', 'cid > 0 AND (published >0 AND comp_is_active= 0 OR published < 1 AND comp_is_active= -1)');

            $this->outputData['listofcompany'] = $this->common->GetAllRowWithColumnStart('companies', '*', 'states', 'companies.state=states.state_code', 'LEFT', 'cid > 0 AND (published >0 AND comp_is_active= 0 OR published < 1 AND comp_is_active= -1)', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofcompany'] as $row_rsCompany):

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $row_rsCompany['cid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $row_rsCompany['dbe'], $row_rsCompany['companyname'], $row_rsCompany['companytype'],
                $row_rsCompany['phone'], $row_rsCompany['city'], $row_rsCompany['state_name'], '<a href="javascript:void(0)" onclick="myFunction(' . $row_rsCompany['cid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" cid="' . $row_rsCompany['cid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Companies deleted Temporary
     */

    /**
     * 
     * Start Companies Not Published (Required ID) Admin
     */
    public function list_of_company_not_published_pagi() {

        $tblcolumnsarray = array('cid', 'dbe', 'companyname', 'companytype', 'phone', 'city', 'state',);

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $dataarray = array();

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $like = array('companyname' => $searchwords);

            $orlike = array('city' => $searchwords, 'phone' => $searchwords, 'state_name' => $searchwords, 'companytype' => $searchwords);

            $this->outputData['total'] = $this->common->GetRecordCountLikeWhere2('companies', 'states', 'companies.state=states.state_code', 'LEFT', $like, $orlike, '(published IS NULL OR published <1) AND comp_is_active = 0');

            $this->outputData['listofcompany'] = $this->common->GetAllRowFromTableLimitOrderByLikeWhere2('companies', 'states', 'companies.state=states.state_code', 'LEFT', $this->uri->segment(3), $tblcolumnsarray[$columnsnumber], $ordertype, $like, $orlike, '(published IS NULL OR published <1) AND comp_is_active = 0');
        } else {

            $this->outputData['total'] = $this->common->GetRecordCountwhere('companies', '(published IS NULL OR published < "2015-03-11 18:48:11") AND comp_is_active = 0');

            $this->outputData['listofcompany'] = $this->common->GetAllRowWithColumnStart('companies', '*', 'states', 'companies.state=states.state_code', 'LEFT', '(published IS NULL OR published <1) AND comp_is_active = 0', $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofcompany'] as $row_rsCompany):

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $row_rsCompany['cid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $row_rsCompany['dbe'], $row_rsCompany['companyname'], $row_rsCompany['companytype'],
                $row_rsCompany['phone'], $row_rsCompany['city'], $row_rsCompany['state_name'], '<a href="' . site_url('admin/company/edit_company/' . $row_rsCompany['cid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i> </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $row_rsCompany['cid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" cid="' . $row_rsCompany['cid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Companies Not Published
     */

    /**
     * 
     * Start Companies by Category (Required Category, ID) Admin
     */
    public function list_of_companycategory_pagi() {

        $tblcolumnsarray = array('cid', 'dbe', 'companyname', 'companytype', 'phone', 'city', 'state', 'published');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($this->input->post('cc') != "") {

            $this->session->set_userdata(array('cc' => ''));

            $this->session->set_userdata('cc', $this->input->post('cc'));

            $cc = $this->input->post('cc');
        } else if ($this->common->GetCurrentUserInfo('cc') != "") {

            $cc = $this->common->GetCurrentUserInfo('cc');
        } else {

            $cc = "Food And beverage";
        }

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $like = array('companyname' => $searchwords);

            $orlike = array('city' => $searchwords, 'phone' => $searchwords, 'state_name' => $searchwords, 'companytype' => $searchwords);

            $date = str_replace(' ', '_', $searchwords);

            $where = '(`companytype` = "' . $cc . '" AND `published` > 0 AND `comp_is_active` = 1) AND (`companyname` LIKE "%' . $searchwords . '%" OR `city` LIKE "%' . $searchwords . '%" OR `phone` LIKE "%' . $searchwords . '%" OR `state_name` LIKE "%' . $searchwords . '%" OR `companytype` LIKE "%' . $searchwords . '%" OR date_format(`published`, "%b_%d,%Y") LIKE "%' . $date . '%")';

            $this->outputData['total'] = $this->common->JoinTableCount('*', 'companies', 'states', 'companies.state=states.state_code', 'LEFT', $where);

            $this->outputData['listofcompany'] = $this->common->GetAllRowWithColumnStart('companies', '*', 'states', 'companies.state=states.state_code', 'LEFT', $where, $tblcolumnsarray[$columnsnumber], $ordertype, $start, $length);
        } else {

            $this->outputData['total'] = $this->common->GetTotalFromTableWhere('companies', array('companytype' => $cc, 'published >' => 0, 'comp_is_active' => 1));

            $this->outputData['listofcompany'] = $this->common->GetAllRowWithColumnStart('companies', '*', 'states', 'companies.state=states.state_code', 'LEFT', array('companytype' => $cc, 'comp_is_active' => 1, 'published >' => 0), $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofcompany'] as $row_rsCompany):

            if ($row_rsCompany['published'] != '' && $row_rsCompany['published'] != 0) {

                $published = $this->common->get_formatted_datetime($row_rsCompany['published']);
            } else {

                $published = $row_rsCompany['published'];
            }

            $parsed_datep = date_parse($published);

            $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

            if ($datep < '2016/3/17') {
                $published = '-';
            }

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $row_rsCompany['cid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $row_rsCompany['dbe'], $row_rsCompany['companyname'], $row_rsCompany['companytype'],
                $row_rsCompany['phone'], $row_rsCompany['city'], $row_rsCompany['state_name'], $published, '<a href="' . site_url('admin/company/edit_company/' . $row_rsCompany['cid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i> </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $row_rsCompany['cid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" cid="' . $row_rsCompany['cid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Companies by Category
     */

    /**
     * 
     * Start Company Locations (Required Id) Admin
     */
    public function list_of_companylocation_pagi() {

        $tblcolumnsarray = array('aid', 'IATA', 'aname', 'acity', 'astate', 'acountry');

        $columnsnumber = $_GET['order'][0]['column'];

        $ordertype = $_GET['order'][0]['dir'];

        $start = $_GET['start'];

        $dataarray = array();

        if ($this->input->post('cc') != "") {

            $this->session->set_userdata(array('ccity' => ''));

            $this->session->set_userdata('ccity', $this->input->post('cc'));

            $cc = $this->input->post('cc');
        } else if ($this->common->GetCurrentUserInfo('ccity') != "") {

            $cc = $this->common->GetCurrentUserInfo('ccity');
        } else {

            $cc = "Annapolis";
        }

        if ($_GET['length'] < 1) {

            $length = 10;
        } else {

            $length = $_GET['length'];
        }

        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {

            $searchwords = $_GET['search']['value'];

            $where = "(`IATA` LIKE '%" . $searchwords . "%' OR `aname` LIKE '%" . $searchwords . "%' OR `acity` LIKE '%" . $searchwords . "%' OR `astate` LIKE '%" . $searchwords . "%' OR `acountry` LIKE '%" . $searchwords . "%') AND (flag = 1 AND acity ='" . $cc . "')";

            $this->outputData['total'] = $this->common->GetRecordCountWhere('lkpairportlist', $where);

            $this->outputData['listofcompany'] = $this->common->GetAllRowFromTableWhereLimitOrderBy('lkpairportlist', $where, $start, $tblcolumnsarray[$columnsnumber], $ordertype);
        } else {

            $this->outputData['total'] = $this->common->GetTotalFromTableWhere('lkpairportlist', array('acity' => $cc, 'flag' => 1));

            $this->outputData['listofcompany'] = $this->common->GetAllRowWithColumn1('lkpairportlist', '*', array('acity' => $cc, 'flag' => 1), $tblcolumnsarray[$columnsnumber], $ordertype, $_GET['start'], $length);
        }

        foreach ($this->outputData['listofcompany'] as $row_rsCompany):

            $dataarray[] = array('<input type="checkbox" name="getall[]" value="' . $row_rsCompany['aid'] . '" class="checkboxsingle" id="getall" class="getevery" />', $row_rsCompany['IATA'], $row_rsCompany['aname'], $row_rsCompany['acity'],
                $row_rsCompany['astate'], $row_rsCompany['acountry'], '<a href="' . site_url('admin/company/lookup_list_for_locations_edit/' . $row_rsCompany['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <span class="green"> <i class="icon-edit bigger-120"></i> </span> </a> &nbsp;&nbsp;  <a href="javascript:void(0)" onclick="myFunction(' . $row_rsCompany['aid'] . ')" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" cid="' . $row_rsCompany['aid'] . '"> <span class="red"> <i class="icon-trash bigger-120"></i> </span> </a>');

        endforeach;

        echo '{';

        echo '"draw": ' . $_GET['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Company Locations
     */

    /**
     * 
     * Start All Airports List
     */
    public function getairportlist() {

        if ($this->input->is_ajax_request()) {

            $airportid = $this->input->post('aid');

            $a = $this->common->GetAllRowWithSomeColumn('airports', array('aid', 'aname'), 'aname', 'ASC');

            echo json_encode($a);
        }
    }

    /**
     * 
     * End All Airports
     */

    /**
     * 
     * Start Get Reports Columns
     */
    public function getreportcolumns() {

        if ($this->input->is_ajax_request()) {

            $reportinfo = $this->common->GetSingleRowFromTableWhere('reports', array('report_id' => $this->input->post('reportid')));

            $segment = $reportinfo['url'];

            switch ($segment) {

                case "top-int-airports2":

                    echo $segment;

                    break;

                case "all-airports":

                case "airport-reports":

                    $res = array('contacts' => 'Contact Info', 'terminal' => 'Terminal Info', 'passengertraffic' => 'Passenger Traffic', 'airportpercentages' => 'Airport Percentages', 'airportwideinfo' => 'Airportwide Info', 'concessiontenantdetails' => 'Concession Tenant Details');

                    echo json_encode($res);

                    break;

                case "ratio-report":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'presecurity' => 'Pre Security', 'postsecurity' => 'Post Security', 'ratiobusleisure' => 'Ratio Bus Leisure', 'ondtransfer' => 'On Demand Transfer', 'avgdwelltime' => 'Avg. Dwell Time', 'parkingspaces' => 'Parking Spaces', 'parkingrev' => 'Parking Rev', 'parkingrevtoair' => 'Parking Rev to Air');

                    echo json_encode($res);

                    break;

                case "advertising":

                    echo $segment;

                    break;

                case "food-beverage":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'terminalabbr' => 'Terminal Abbrevation', 'terminalname' => 'Terminal Name', 'tyear' => 'Terminal Year', 'tpasstraffic' => 'Passenger Traffic', 'tpasstrafficcompare' => 'Passenger Traffic Comparison', 'tenplaning' => 'Enplaning', 'tdeplaning' => 'Deplaning', 'tepdomestic' => 'EP Domestic', 'tepintl' => 'EP Intl.', 'tconcessiongrosssales' => 'Concession Gross Sales', 'ctsalesep' => 'Sales EP', 'ctrentrev' => 'Rent Rev', 'ctrentep' => 'Rent EP', 'ctcurrsqft' => 'Current Sq. Ft.', 'tdominantairline' => 'Domain Air', 'fbgrosssales' => 'Food and Beverage Gross Sales', 'fbsalesep' => 'Food and Beverage Sales EP', 'fbrentrev' => 'Food and Beverage Rent Rev', 'fbrentep' => 'Food and Beverage Rent EP', 'fbcurrsqft' => 'Food and Beverage Current Sq.Ft.', 'srgrosssales' => 'Specialty Retail Gross Sales', 'srsalesep' => 'Specialty Retail Sales EP', 'srrentrev' => 'Specialty Retail Rent Rev', 'srrentep' => 'Specialty Retail Rent EP', 'srcurrsqft' => 'Specialty Retail Current Sq.Ft.', 'nggrosssales' => 'News and Gifts Gross Sales', 'ngsalesep' => 'News and Gifts Sales EP', 'ngrentrev' => 'News and Gifts Rent Rev', 'ngrentep' => 'News and Gifts Rent EP', 'ngcurrsqft' => 'News and Gifts Current Sq.Ft.', 'dfgrosssales' => 'Duty Free Gross Sales', 'dfsalesep' => 'Duty Free Sales EP', 'dfrentrev' => 'Duty Free Rent Rev', 'dfrentep' => 'Duty Free Rent EP', 'dfcurrsqft' => 'Duty Free Current Sq.Ft.');

                    echo json_encode($res);

                    break;

                case "specialty-retail":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'terminalabbr' => 'Terminal Abbrevation', 'terminalname' => 'Terminal Name', 'tyear' => 'Terminal Year', 'tpasstraffic' => 'Passenger Traffic', 'tpasstrafficcompare' => 'Passenger Traffic Comparison', 'tenplaning' => 'Enplaning', 'tdeplaning' => 'Deplaning', 'tepdomestic' => 'EP Domestic', 'tepintl' => 'EP Intl.', 'tconcessiongrosssales' => 'Concession Gross Sales', 'ctsalesep' => 'Sales EP', 'ctrentrev' => 'Rent Rev', 'ctrentep' => 'Rent EP', 'ctcurrsqft' => 'Current Sq. Ft.', 'tdominantairline' => 'Domain Air', 'fbgrosssales' => 'Food and Beverage Gross Sales', 'fbsalesep' => 'Food and Beverage Sales EP', 'fbrentrev' => 'Food and Beverage Rent Rev', 'fbrentep' => 'Food and Beverage Rent EP', 'fbcurrsqft' => 'Food and Beverage Current Sq.Ft.', 'srgrosssales' => 'Specialty Retail Gross Sales', 'srsalesep' => 'Specialty Retail Sales EP', 'srrentrev' => 'Specialty Retail Rent Rev', 'srrentep' => 'Specialty Retail Rent EP', 'srcurrsqft' => 'Specialty Retail Current Sq.Ft.', 'nggrosssales' => 'News and Gifts Gross Sales', 'ngsalesep' => 'News and Gifts Sales EP', 'ngrentrev' => 'News and Gifts Rent Rev', 'ngrentep' => 'News and Gifts Rent EP', 'ngcurrsqft' => 'News and Gifts Current Sq.Ft.', 'dfgrosssales' => 'Duty Free Gross Sales', 'dfsalesep' => 'Duty Free Sales EP', 'dfrentrev' => 'Duty Free Rent Rev', 'dfrentep' => 'Duty Free Rent EP', 'dfcurrsqft' => 'Duty Free Current Sq.Ft.');

                    echo json_encode($res);

                    break;

                case "news-gifts":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'terminalabbr' => 'Terminal Abbrevation', 'terminalname' => 'Terminal Name', 'tyear' => 'Terminal Year', 'tpasstraffic' => 'Passenger Traffic', 'tpasstrafficcompare' => 'Passenger Traffic Comparison', 'tenplaning' => 'Enplaning', 'tdeplaning' => 'Deplaning', 'tepdomestic' => 'EP Domestic', 'tepintl' => 'EP Intl.', 'tconcessiongrosssales' => 'Concession Gross Sales', 'ctsalesep' => 'Sales EP', 'ctrentrev' => 'Rent Rev', 'ctrentep' => 'Rent EP', 'ctcurrsqft' => 'Current Sq. Ft.', 'tdominantairline' => 'Domain Air', 'fbgrosssales' => 'Food and Beverage Gross Sales', 'fbsalesep' => 'Food and Beverage Sales EP', 'fbrentrev' => 'Food and Beverage Rent Rev', 'fbrentep' => 'Food and Beverage Rent EP', 'fbcurrsqft' => 'Food and Beverage Current Sq.Ft.', 'srgrosssales' => 'Specialty Retail Gross Sales', 'srsalesep' => 'Specialty Retail Sales EP', 'srrentrev' => 'Specialty Retail Rent Rev', 'srrentep' => 'Specialty Retail Rent EP', 'srcurrsqft' => 'Specialty Retail Current Sq.Ft.', 'nggrosssales' => 'News and Gifts Gross Sales', 'ngsalesep' => 'News and Gifts Sales EP', 'ngrentrev' => 'News and Gifts Rent Rev', 'ngrentep' => 'News and Gifts Rent EP', 'ngcurrsqft' => 'News and Gifts Current Sq.Ft.', 'dfgrosssales' => 'Duty Free Gross Sales', 'dfsalesep' => 'Duty Free Sales EP', 'dfrentrev' => 'Duty Free Rent Rev', 'dfrentep' => 'Duty Free Rent EP', 'dfcurrsqft' => 'Duty Free Current Sq.Ft.');

                    echo json_encode($res);

                    break;

                case "duty-free":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'terminalabbr' => 'Terminal Abbrevation', 'terminalname' => 'Terminal Name', 'tyear' => 'Terminal Year', 'tpasstraffic' => 'Passenger Traffic', 'tpasstrafficcompare' => 'Passenger Traffic Comparison', 'tenplaning' => 'Enplaning', 'tdeplaning' => 'Deplaning', 'tepdomestic' => 'EP Domestic', 'tepintl' => 'EP Intl.', 'tconcessiongrosssales' => 'Concession Gross Sales', 'ctsalesep' => 'Sales EP', 'ctrentrev' => 'Rent Rev', 'ctrentep' => 'Rent EP', 'ctcurrsqft' => 'Current Sq. Ft.', 'tdominantairline' => 'Domain Air', 'fbgrosssales' => 'Food and Beverage Gross Sales', 'fbsalesep' => 'Food and Beverage Sales EP', 'fbrentrev' => 'Food and Beverage Rent Rev', 'fbrentep' => 'Food and Beverage Rent EP', 'fbcurrsqft' => 'Food and Beverage Current Sq.Ft.', 'srgrosssales' => 'Specialty Retail Gross Sales', 'srsalesep' => 'Specialty Retail Sales EP', 'srrentrev' => 'Specialty Retail Rent Rev', 'srrentep' => 'Specialty Retail Rent EP', 'srcurrsqft' => 'Specialty Retail Current Sq.Ft.', 'nggrosssales' => 'News and Gifts Gross Sales', 'ngsalesep' => 'News and Gifts Sales EP', 'ngrentrev' => 'News and Gifts Rent Rev', 'ngrentep' => 'News and Gifts Rent EP', 'ngcurrsqft' => 'News and Gifts Current Sq.Ft.', 'dfgrosssales' => 'Duty Free Gross Sales', 'dfsalesep' => 'Duty Free Sales EP', 'dfrentrev' => 'Duty Free Rent Rev', 'dfrentep' => 'Duty Free Rent EP', 'dfcurrsqft' => 'Duty Free Current Sq.Ft.');

                    echo json_encode($res);

                    break;

                case "passenger-services":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'terminalabbr' => 'Terminal Abbrevation', 'terminalname' => 'Terminal Name', 'tyear' => 'Terminal Year', 'tpasstraffic' => 'Passenger Traffic', 'tpasstrafficcompare' => 'Passenger Traffic Comparison', 'tenplaning' => 'Enplaning', 'tdeplaning' => 'Deplaning', 'tepdomestic' => 'EP Domestic', 'tepintl' => 'EP Intl.', 'tconcessiongrosssales' => 'Concession Gross Sales', 'ctsalesep' => 'Sales EP', 'ctrentrev' => 'Rent Rev', 'ctrentep' => 'Rent EP', 'ctcurrsqft' => 'Current Sq. Ft.', 'tdominantairline' => 'Domain Air', 'fbgrosssales' => 'Food and Beverage Gross Sales', 'fbsalesep' => 'Food and Beverage Sales EP', 'fbrentrev' => 'Food and Beverage Rent Rev', 'fbrentep' => 'Food and Beverage Rent EP', 'fbcurrsqft' => 'Food and Beverage Current Sq.Ft.', 'srgrosssales' => 'Specialty Retail Gross Sales', 'srsalesep' => 'Specialty Retail Sales EP', 'srrentrev' => 'Specialty Retail Rent Rev', 'srrentep' => 'Specialty Retail Rent EP', 'srcurrsqft' => 'Specialty Retail Current Sq.Ft.', 'nggrosssales' => 'News and Gifts Gross Sales', 'ngsalesep' => 'News and Gifts Sales EP', 'ngrentrev' => 'News and Gifts Rent Rev', 'ngrentep' => 'News and Gifts Rent EP', 'ngcurrsqft' => 'News and Gifts Current Sq.Ft.', 'dfgrosssales' => 'Duty Free Gross Sales', 'dfsalesep' => 'Duty Free Sales EP', 'dfrentrev' => 'Duty Free Rent Rev', 'dfrentep' => 'Duty Free Rent EP', 'dfcurrsqft' => 'Duty Free Current Sq.Ft.');

                    echo json_encode($res);

                    break;

                case "top-50-airports":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'ayear' => 'Airport Year', 'apasstraffic' => 'Passenger Traffic', 'apasstrafficcompare' => 'Passenger Traffic Comparison', 'aenplaning' => 'Enplaning', 'adeplaning' => 'Deplaning', 'aepdomestic' => 'EP Domestic', 'aepintl' => 'EP Intl.', 'aconcessiongrosssales' => 'Concession Gross Sales', 'aconcessiongrosssales-df' => 'Concession Gross Sales - DF', 'asalesep' => 'Sales EP', 'asalesep-df' => 'Sales EP-DF', 'arentrev' => 'Rent Rev', 'arentrev-df' => 'Rent Rev-DF', 'arentep' => 'Rent EP', 'arentep-df' => 'Rent EP-DF', 'acurrsqft' => 'Current Sq. Ft.', 'acurrsqft-df' => 'Current Sq. Ft - DF', 'afbgrosssales' => 'Food and Beverage Gross Sales', 'afbsalesep' => 'Food and Beverage Sales EP', 'afbrentrev' => 'Food and Beverage Rent Rev', 'afbrentep' => 'Food and Beverage Rent EP', 'afbcurrsqft' => 'Food and Beverage Current Sq.Ft.', 'asrgrosssales' => 'Specialty Retail Gross Sales', 'asrsalesep' => 'Specialty Retail Sales EP', 'asrrentrev' => 'Specialty Retail Rent Rev', 'asrrentep' => 'Specialty Retail Rent EP', 'asrcurrsqft' => 'Specialty Retail Current Sq.Ft.');

                    echo json_encode($res);

                    break;

                case "top-int-airports":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'ayear' => 'Airport Year', 'apasstraffic' => 'Passenger Traffic', 'apasstrafficcompare' => 'Passenger Traffic Comparison', 'aenplaning' => 'Enplaning', 'adeplaning' => 'Deplaning', 'aepdomestic' => 'EP Domestic', 'aepintl' => 'EP Intl.', 'aconcessiongrosssales' => 'Concession Gross Sales', 'aconcessiongrosssales-df' => 'Concession Gross Sales - DF', 'asalesep' => 'Sales EP', 'asalesep-df' => 'Sales EP-DF', 'arentrev' => 'Rent Rev', 'arentrev-df' => 'Rent Rev-DF', 'arentep' => 'Rent EP', 'arentep-df' => 'Rent EP-DF', 'acurrsqft' => 'Current Sq. Ft.', 'acurrsqft-df' => 'Current Sq. Ft - DF', 'afbgrosssales' => 'Food and Beverage Gross Sales', 'afbsalesep' => 'Food and Beverage Sales EP', 'afbrentrev' => 'Food and Beverage Rent Rev', 'afbrentep' => 'Food and Beverage Rent EP', 'afbcurrsqft' => 'Food and Beverage Current Sq.Ft.', 'asrgrosssales' => 'Specialty Retail Gross Sales', 'asrsalesep' => 'Specialty Retail Sales EP', 'asrrentrev' => 'Specialty Retail Rent Rev', 'asrrentep' => 'Specialty Retail Rent EP', 'asrcurrsqft' => 'Specialty Retail Current Sq.Ft.');

                    echo json_encode($res);

                    break;

                case "lease-expire2":

                    echo $segment;

                    break;

                case "lease-expire":

                    $res = array('category' => 'Category', 'outletname' => 'Outlet Name', 'companyname' => 'Company Name', 'numlocations' => 'Number of Locations', 'sqft' => 'Sq.Ft', 'exp' => 'Expire', 'termlocation' => 'Term Location');

                    echo json_encode($res);

                    break;

                case "tenant-listing":

                    $res = array('IATA' => 'IATA', 'aname' => 'Airport Name', 'acity' => 'Airport City', 'astate' => 'Airport State', 'termlocation' => 'Terminal Location', 'category' => 'Category', 'productdescription' => 'Product Description', 'outletname' => 'Tenant Name', 'companyname' => 'Company Name', 'numlocations' => '# Locations', 'sqft' => 'Square Footage', 'exp' => 'Lease Expires');

                    echo json_encode($res);

                    break;

                default:

                    $res = array('error' => '<div class="alert alert-danger">No Columns found</div>');

                    echo json_encode($res);

                    exit;
            }
        }
    }

    /**
     * 
     * End Get Reports Columns
     */

    /**
     * 
     * Start Get Affiliations Old (Required Id)
     */
    public function tableresult() {

        $action = $this->input->post('2') . $this->input->post('3') . $this->input->post('4');

        $DataArray = array();

        if ($this->input->is_ajax_request()) {

            if ($action == "aff") {

                $cols = "users.last_name, users.first_name, airports.aid, affiliation.affid, companies.companyname,airports.IATA";

                $join_array = array(
                    array('users', 'affiliation.user_id=users.user_id', 'left'),
                    array('airports', 'affiliation.aid=airports.aid', 'left'),
                    array('companies', 'affiliation.cid=companies.cid', 'left')
                );

                $cur = $this->common->JoinTables($cols, 'affiliation', $join_array, "", "users.last_name, users.first_name ASC");

                foreach ($cur as $val):

                    $myarray['last_name'] = $val['last_name'];

                    $myarray['first_name'] = $val['first_name'];

                    $myarray['IATA'] = $val['IATA'];

                    $myarray['companyname'] = $val['companyname'];

                    $myarray['affid'] = '<a href="javascript:void(0);" title="Delete this affiliations"><i class="fa fa-times" onclick="delme(' . $val['affid'] . ')"></i></a>';

                    $DataArray[] = array_values($myarray);

                endforeach;

                echo json_encode(array('aaData' => $DataArray));
            }
        } else {

            $this->common->redirect(site_url(), 'refresh');

            exit();
        }
    }

    /**
     * 
     * End Get Affiliations Old
     */

    /**
     * 
     * Start Lease Expiration frontend (Required Id)
     */
    public function GetExpense() {

        $type = $this->input->post('2') . "" . $this->input->post('3');

        $year = date('Y');

        $year = $year + 1;

        $length = '';

        $futuredate = $year . "-" . date("m-d");

        $CategoryList = array('fb' => 'Food/Beverage', 'sr' => 'Specialty Retail', 'ng' => 'News/Gifts', 'df' => 'Duty Free', 'ps' => 'Passenger Services', 'ad' => 'Advertising');

        $tblcolumnsarray = array('outletname', 'companyname', 'acity', 'numlocations', 'sqft', 'exp');

        $columnsnumber = $_POST['order'][0]['column'];

        $ordertype = $_POST['order'][0]['dir'];

        $dateask = " AND outlets.exp BETWEEN CURDATE() and '" . $futuredate . "'";

        $dataarray = array();

        if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {

            $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $_POST['length'];

            $searchwords = $_POST['search']['value'];

            $date = str_replace(' ', '_', $searchwords);

            $where = "(outlets.`outletname` LIKE '%" . $searchwords . "%' 

				OR outlets.`companyname` LIKE '%" . $searchwords . "%' 

				OR airports.`acity` LIKE '%" . $searchwords . "%' OR date_format(outlets.`exp`, '%b_%d,%Y') LIKE '%" . $date . "%') AND lkpcategory.category = '" . $CategoryList[$type] . "' " . $dateask . "";

            $join_array = array(
                array('lkpcategory', 'lkpcategory.categoryid = outlets.categoryid', 'left'),
                array('airports', 'outlets.aid = airports.aid', 'left')
            );

            $this->outputData['totalrecord'] = $this->common->JoinTableQueryCount2('outlets', $where, $join_array);

            $this->outputData['listoftenants'] = $this->common->JoinTableQuerytype2('`outletname`,`companyname`,`acity`,`numlocations`,`sqft`,`exp`', 'outlets', $join_array, $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], 0, $where);
        } else {

            if ($_POST['length'] < 1) {

                $length = 10;
            } else {

                $length = $_POST['length'];
            }

            $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

            $searchwords = $_POST['search']['value'];

            $where = "lkpcategory.category = '" . $CategoryList[$type] . "' " . $dateask . "";

            $join_array = array(
                array('lkpcategory', 'lkpcategory.categoryid = outlets.categoryid', 'left'),
                array('airports', 'outlets.aid = airports.aid', 'left')
            );

            $this->outputData['totalrecord'] = $this->common->JoinTableQueryCount2('outlets', $where, $join_array);

            $this->outputData['listoftenants'] = $this->common->JoinTableQuerytype2('`outletname`,`companyname`,`acity`,`numlocations`,`sqft`,`exp`', 'outlets', $join_array, $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], 0, $where);
        }

        foreach ($this->outputData['listoftenants'] as $user): {
                
            };

            if ($user['exp'] != "") {

                $dateexp = $this->common->get_formatted_datetime($user['exp']);
            } else {

                $dateexp = $user['exp'];
            }

            $dataarray[] = array($user['outletname'], $user['companyname'],
                $user['acity'], $user['numlocations'], number_format($user['sqft']), $dateexp);

        endforeach;

        echo '{';

        echo '"draw": ' . $_POST['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['totalrecord'] . ',

			  		"recordsFiltered": ' . $this->outputData['totalrecord'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End Lease Expiration frontend
     */

    /**
     * 
     * Start Get Records for Modify Data frontend (Required Id)
     */
    public function GetRecord() {

        $length = $_POST['length'];

        if ($this->input->is_ajax_request()) {

            $category = $this->input->post('1');

            $type = $this->input->post('3');

            $user_id = $this->session->userdata('user_id');

            $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

            $columnsnumber = $_POST['order'][0]['column'];

            $tblcolumnsarray = array('aid', 'IATA', 'aname', 'lastmodified', 'published', 'approved', 'modifiedby');

            $tblcolumnsarraycomp = array('aid', 'aname', 'lastmodified', 'published', 'approved', 'modifiedby');

            $ordertype = $_POST['order'][0]['dir'];

//            $start = $_GET['start'];

            $dataarray = array();

            if (!isset($type) || $type == "" || !isset($category) || $category == "") {

                $this->common->redirect(site_url(), 'refresh');

                exit();
            }

            /**
             * 
             * Start Search Airports by Caegory Frontend
             */
            if ($category != 'S' && $type == 'A') {
                /**
                 * 
                 * Access Level Is not 4
                 */
// HERE*****
                if ($this->session->userdata('accesslevel') != 4) {

                    if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {

                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $searchwords = $_POST['search']['value'];

                        $date = str_replace(' ', '_', $searchwords);

                        $where = " where (airports.`IATA` LIKE '%" . $searchwords . "%' 

						OR airports.`aname` LIKE '%" . $searchwords . "%' 

						OR airports.`modifiedby` LIKE '%" . $searchwords . "%' OR date_format(airports.`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(airports.`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(airports.`approved`, '%b_%d,%Y') LIKE '%" . $date . "%') AND (user_id = $user_id) AND (category = '$category') AND affi_is_active=1";

                        $sql = "select airports.aid, IATA, aname, lastmodified, published, approved, modifiedby from airports 

						INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` " . $where . " ORDER BY {$tblcolumnsarray[$columnsnumber]} {$ordertype} " . $LIMIT;

                        $countq = "select airports.aid, IATA, aname, lastmodified, published, approved, modifiedby from airports 

						INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` " . $where . "";

                        $this->outputData['total'] = $this->common->CustomCountQuery($countq);

                        $this->outputData['listofairport'] = $this->common->CustomQueryALL($sql);
                    } else {

                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $sql = "SELECT airports.aid, IATA, aname, lastmodified, published, approved, modifiedby FROM airports 

						INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` 

						WHERE user_id = $user_id AND category = '$category' AND affi_is_active=1 ORDER BY {$tblcolumnsarray[$columnsnumber]} {$ordertype} " . $LIMIT;

                        $countq = "select airports.aid, IATA, aname, lastmodified, published, approved, modifiedby from airports INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` WHERE user_id = $user_id AND category = '$category'   AND affi_is_active=1 ";

                        $this->outputData['total'] = $this->common->CustomCountQuery($countq);

                        $this->outputData['listofairport'] = $this->common->CustomQueryALL($sql);
                    }

                    foreach ($this->outputData['listofairport'] as $airport):


                        $parsed_datea = date_parse($airport['approved']);

                        $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

                        if ($dateap < '2016/3/17') {
                            $airport['approved'] = '<i class="fa fa-times-circle-o fa-2x" style="color:#e60000;" title="Not Approved"></i>';
                        } else {
                            $airport['approved'] = '<i class="fa fa-check-circle-o fa-2x" style="color:#337ab7;" title="Approved"></i>';
                        }

                        $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

                        $parsed_date = date_parse($lastmodified);

                        $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

                        if ($datee < '2016/3/17') {
                            $lastmodified = '-';
                        }

                        if ($airport['published'] != '' && $airport['published'] != 0) {

                            $published = $this->common->get_formatted_datetime($airport['published']);
                        } else {

                            $published = $airport['published'];
                        }

                        $parsed_datep = date_parse($published);

                        $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

                        if ($datep < '2016/3/17') {
                            $published = '-';
                        }

                        $dataarray[] = array('<a href="' . site_url('wizard/airport/' . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <i class="fa fa-pencil-square-o"></i> Edit</a>', $airport['IATA'], $airport['aname'], $lastmodified,
                            $published, $airport['approved'], $airport['modifiedby']);

                    endforeach;
                }

                /**
                 * 
                 * Access Level Is 4
                 */ else {

                    if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {

                        $searchwords = $_POST['search']['value'];

                        $like = array('IATA' => $searchwords);
                        $orlike = array('aname' => $searchwords, 'modifiedby' => $searchwords);

                        $date = str_replace(' ', '_', $searchwords);

                        $where = "`category` =  '" . $category . "' AND  ( `IATA`  LIKE '%" . $searchwords . "%' OR  `aname`  LIKE '%" . $searchwords . "%'  OR  `modifiedby`  LIKE '%" . $searchwords . "%' OR date_format(airports.`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(airports.`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(airports.`approved`, '%b_%d,%Y') LIKE '%" . $date . "%')";

                        $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', $where);

                        $this->outputData['listofairport'] = $this->common->GetAllRowWithColumn1('airports', '`aid`, `IATA`, `aname`, `lastmodified`, `published`, `approved`, `modifiedby`', $where, $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], $length);
                    } else {

                        $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', array('category' => $category));

                        $this->outputData['listofairport'] = $this->common->GetAllRowWithColumn1('airports', '`aid`, `IATA`, `aname`, `lastmodified`, `published`, `approved`, `modifiedby`', array('`category`' => $category), $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], $length);
                    }

                    foreach ($this->outputData['listofairport'] as $airport):


                        $parsed_datea = date_parse($airport['approved']);

                        $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

                        if ($dateap < '2016/3/17') {
                            $airport['approved'] = '<i class="fa fa-times-circle-o fa-2x" style="color:#e60000;" title="Not Approved"></i>';
                        } else {
                            $airport['approved'] = '<i class="fa fa-check-circle-o fa-2x" style="color:#337ab7;" title="Approved"></i>';
                        }

                        $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

                        $parsed_date = date_parse($lastmodified);

                        $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

                        if ($datee < '2016/3/17') {
                            $lastmodified = '-';
                        }

                        if ($airport['published'] != '' && $airport['published'] != 0) {

                            $published = $this->common->get_formatted_datetime($airport['published']);
                        } else {

                            $published = $airport['published'];
                        }

                        $parsed_datep = date_parse($published);

                        $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

                        if ($datep < '2016/3/17') {
                            $published = '-';
                        }

                        $dataarray[] = array('<a href="' . site_url('wizard/airport/' . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <i class="fa fa-pencil-square-o"></i> Edit</a>', $airport['IATA'], $airport['aname'], $lastmodified,
                            $published, $airport['approved'], $airport['modifiedby']);

                    endforeach;
                }
            }

            /**
             * 
             * End Search Caegory A Airports Frontend
             */
            /**
             * 
             * Start Search All Airports Frontend
             */ else if ($category == 'S' && $type == 'A') {

                /**
                 * 
                 * Access Level Is Not 4
                 */
                if ($this->session->userdata('accesslevel') != 4) {

                    if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {


                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $searchwords = $_POST['search']['value'];

                        $date = str_replace(' ', '_', $searchwords);

                        $where = " where (airports.`IATA` LIKE '%" . $searchwords . "%' 

								OR airports.`aname` LIKE '%" . $searchwords . "%' 

								OR airports.`modifiedby` LIKE '%" . $searchwords . "%' OR date_format(airports.`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(airports.`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(airports.`approved`, '%b_%d,%Y') LIKE '%" . $date . "%')  AND user_id = $user_id AND affi_is_active=1";

                        $sql = "select airports.aid, IATA, aname, lastmodified, published, approved, modifiedby from airports 

									INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` " . $where . " ORDER BY {$tblcolumnsarray[$columnsnumber]} {$ordertype} " . $LIMIT;

                        $countq = "select airports.aid, IATA, aname, lastmodified, published, approved, modifiedby from airports 

									INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` " . $where . "";

                        $this->outputData['total'] = $this->common->CustomCountQuery($countq);

                        $this->outputData['listofairport'] = $this->common->CustomQueryALL($sql);
                    } else {


                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $sql = "SELECT airports.aid, IATA, aname, lastmodified, published, approved, modifiedby FROM airports 

						INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` 

						WHERE user_id =" . $user_id . "  AND affi_is_active=1 ORDER BY {$tblcolumnsarray[$columnsnumber]} {$ordertype} " . $LIMIT;

                        $countq = "select airports.aid, IATA, aname, lastmodified, published, approved, modifiedby from airports 

									INNER JOIN `affiliation` ON `airports`.`aid` = `affiliation`.`aid` WHERE user_id = $user_id AND affi_is_active=1";

                        $this->outputData['total'] = $this->common->CustomCountQuery($countq);

                        $this->outputData['listofairport'] = $this->common->CustomQueryALL($sql);
                    }

                    foreach ($this->outputData['listofairport'] as $airport):

                        $parsed_datea = date_parse($airport['approved']);

                        $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

                        if ($dateap < '2016/3/17') {
                            $airport['approved'] = '<i class="fa fa-times-circle-o fa-2x" style="color:#e60000;" title="Not Approved"></i>';
                        } else {
                            $airport['approved'] = '<i class="fa fa-check-circle-o fa-2x" style="color:#337ab7;" title="Approved"></i>';
                        }

                        $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

                        $parsed_date = date_parse($lastmodified);

                        $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

                        if ($datee < '2016/3/17') {
                            $lastmodified = '-';
                        }

                        if ($airport['published'] != '' && $airport['published'] != 0) {

                            $published = $this->common->get_formatted_datetime($airport['published']);
                        } else {

                            $published = $airport['published'];
                        }

                        $parsed_datep = date_parse($published);

                        $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

                        if ($datep < '2016/3/17') {
                            $published = '-';
                        }

                        $dataarray[] = array('<a href="' . site_url('wizard/airport/' . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <i class="fa fa-pencil-square-o"></i> Edit</a>', $airport['IATA'], $airport['aname'], $lastmodified,
                            $published, $airport['approved'], $airport['modifiedby']);

                    endforeach;
                }

                /**
                 * 
                 * Access Level Is 4
                 */ else {



                    if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {

                        $searchwords = $_POST['search']['value'];

                        $date = str_replace(' ', '_', $searchwords);

                        $where = "(`IATA` LIKE '%" . $searchwords . "%' OR airports.`aname` LIKE '%" . $searchwords . "%' OR airports.`modifiedby` LIKE '%" . $searchwords . "%' OR date_format(airports.`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(airports.`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(airports.`approved`, '%b_%d,%Y') LIKE '%" . $date . "%')";

                        $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', $where);

                        $this->outputData['listofairport'] = $this->common->GetAllRowWithColumn1('airports', '*', $where, $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], $length);
                    } else {

                        $this->outputData['total'] = $this->common->GetRecordCountWhere('airports', 'aid > 0');

                        $this->outputData['listofairport'] = $this->common->GetAllRowWithColumn1('airports', 'aid, IATA, aname, lastmodified, published, approved, modifiedby', 'IATA != "" ', $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], $length);
                    }

                    foreach ($this->outputData['listofairport'] as $airport):

                        $parsed_datea = date_parse($airport['approved']);

                        $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

                        if ($dateap < '2016/3/17') {
                            $airport['approved'] = '<i class="fa fa-times-circle-o fa-2x" style="color:#e60000;" title="Not Approved"></i>';
                        } else {
                            $airport['approved'] = '<i class="fa fa-check-circle-o fa-2x" style="color:#337ab7;" title="Approved"></i>';
                        }

                        $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

                        $parsed_date = date_parse($lastmodified);

                        $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

                        if ($datee < '2016/3/17') {
                            $lastmodified = '-';
                        }

                        if ($airport['published'] != '' && $airport['published'] != 0) {

                            $published = $this->common->get_formatted_datetime($airport['published']);
                        } else {

                            $published = $airport['published'];
                        }

                        $parsed_datep = date_parse($published);

                        $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

                        if ($datep < '2016/3/17') {
                            $published = '-';
                        }

                        $dataarray[] = array('<a href="' . site_url('wizard/airport/' . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <i class="fa fa-pencil-square-o"></i> Edit</a>', $airport['IATA'], $airport['aname'], $lastmodified,
                            $published, $airport['approved'], $airport['modifiedby']);

                    endforeach;
                }
            }
            /**
             * 
             * End Search All Airports Frontend
             */
            /**
             * 
             * Start Search Companies Frontend
             */ else if ($category == 'K' && $type == 'C') {

                /**
                 * 
                 * Access Level Is Not 4
                 */
                if ($this->session->userdata('accesslevel') != 4) {

                    if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {

                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $searchwords = $_POST['search']['value'];

                        $date = str_replace(' ', '_', $searchwords);

                        $where = " where (companies.`companyname` LIKE '%" . $searchwords . "%' 

                                                OR companies.`modifiedby` LIKE '%" . $searchwords . "%' 

                                                OR date_format(companies.`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(companies.`published`, '%b_%d,%Y') LIKE '%" . $date . "%' OR date_format(companies.`approved`, '%b_%d,%Y') LIKE '%" . $date . "%') AND user_id = $user_id AND affi_is_active=1";

                        $sql = "select `affiliation`.`cid` as aid,`companyname` AS `aname`, `lastmodified`, `modifiedby`, 

						`approved`, `published` from companies 

						INNER JOIN `affiliation` ON `companies`.`cid` = `affiliation`.`cid` " . $where . " ORDER BY {$tblcolumnsarraycomp[$columnsnumber]} {$ordertype} " . $LIMIT;

                        $countq = "select`affiliation`.`cid` as aid,`companyname` AS `aname`, `lastmodified`, `modifiedby`, 

						`approved`, `published` from companies 

						INNER JOIN `affiliation` ON `companies`.`cid` = `affiliation`.`cid` " . $where . "";

                        $this->outputData['total'] = $this->common->CustomCountQuery($countq);

                        $this->outputData['listofairport'] = $this->common->CustomQueryALL($sql);
                    } else {

                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $sql = "SELECT `affiliation`.`cid` as aid,`companyname` AS `aname`, `lastmodified`, `modifiedby`, 

						`approved`, `published` FROM companies 

						INNER JOIN `affiliation` ON `companies`.`cid` = `affiliation`.`cid` 

						WHERE user_id = $user_id  AND affi_is_active=1 ORDER BY {$tblcolumnsarraycomp[$columnsnumber]} {$ordertype} " . $LIMIT;

                        $countq = "select `affiliation`.`cid` as aid,`companyname` AS `aname`, `lastmodified`, `modifiedby`, 

						`approved`, `published` FROM companies  

						INNER JOIN `affiliation` ON `companies`.`cid` = `affiliation`.`cid` 

						WHERE user_id = $user_id AND affi_is_active=1";

                        $this->outputData['total'] = $this->common->CustomCountQuery($countq);

                        $this->outputData['listofairport'] = $this->common->CustomQueryALL($sql);
                    }

                    foreach ($this->outputData['listofairport'] as $airport):

                        $parsed_datea = date_parse($airport['approved']);

                        $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

                        if ($dateap < '2016/3/17') {
                            $airport['approved'] = '<i class="fa fa-times-circle-o fa-2x" style="color:#e60000;" title="Not Approved"></i>';
                        } else {
                            $airport['approved'] = '<i class="fa fa-check-circle-o fa-2x" style="color:#337ab7;" title="Approved"></i>';
                        }

                        $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

                        $parsed_date = date_parse($lastmodified);

                        $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

                        if ($datee < '2016/3/17') {
                            $lastmodified = '-';
                        }

                        if ($airport['published'] != '' && $airport['published'] != 0) {

                            $published = $this->common->get_formatted_datetime($airport['published']);
                        } else {

                            $published = $airport['published'];
                        }

                        $parsed_datep = date_parse($published);

                        $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

                        if ($datep < '2016/3/17') {
                            $published = '-';
                        }

                        $dataarray[] = array('<a href="' . site_url('wizard/company/' . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <i class="fa fa-pencil-square-o"></i> Edit</a>', $airport['aname'], $lastmodified, $published, $airport['approved'], $airport['modifiedby']);

                    endforeach;
                }

                /**
                 * 
                 * Access Level Is 4
                 */ else {

                    if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {

                        $LIMIT = 'LIMIT ' . $_POST['start'] . ',' . $length;

                        $start = $_POST['start'];

                        $searchwords = $_POST['search']['value'];

                        $date = str_replace(' ', '_', $searchwords);

                        $where = "(companies.approved LIKE '%" . $searchwords . "%' 

								OR companies.companyname LIKE '%" . $searchwords . "%'

								OR companies.modifiedby LIKE '%" . $searchwords . "%' OR date_format(companies.`lastmodified`, '%b_%d,%Y') LIKE '%" . $date . "%'  OR date_format(companies.`published`, '%b_%d,%Y') LIKE '%" . $date . "%')";

                        $this->outputData['total'] = $this->common->JoinTableCount('`cid` as aid,`companyname` AS `aname`,`companies`.`lastmodified`,`modifiedby`,`approved`,`published`', 'companies', 'lkpcategory', 'lkpcategory.categoryid = companies.categoryid', 'LEFT', $where);

                        $this->outputData['listofairport'] = $this->common->GetAllRowWithColumnStart('companies', '`cid` as aid,`companyname` AS `aname`,`companies`.`lastmodified`,`modifiedby`,`approved`,`published`', 'lkpcategory', 'lkpcategory.categoryid = companies.categoryid', 'LEFT', $where, $tblcolumnsarray[$columnsnumber], $ordertype, $start, $length);
                    } else {

                        $where1 = "cid > 0";

                        $this->outputData['total'] = $this->common->JoinTableCount('`cid` as aid,`companyname` AS `aname`,`companies`.`lastmodified`,`modifiedby`,`approved`,`published`', 'companies', 'lkpcategory', 'lkpcategory.categoryid = companies.categoryid', 'LEFT', $where1);

                        $this->outputData['listofairport'] = $this->common->GetAllRowWithColumnStart('companies', '`cid` as aid,`companyname` AS `aname`,`companies`.`lastmodified`,`modifiedby`,`approved`,`published`', 'lkpcategory', 'lkpcategory.categoryid = companies.categoryid', 'LEFT', $where1, $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], $length);
                    }

                    foreach ($this->outputData['listofairport'] as $airport):

                        $parsed_datea = date_parse($airport['approved']);

                        $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];

                        if ($dateap < '2016/3/17') {
                            $airport['approved'] = '<i class="fa fa-times-circle-o fa-2x" style="color:#e60000;" title="Not Approved"></i>';
                        } else {
                            $airport['approved'] = '<i class="fa fa-check-circle-o fa-2x" style="color:#337ab7;" title="Approved"></i>';
                        }

                        if ($airport['published'] != '' && $airport['published'] != 0) {

                            $published = $this->common->get_formatted_datetime($airport['published']);
                        } else {

                            $published = $airport['published'];
                        }

                        $parsed_datep = date_parse($published);

                        $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

                        if ($datep < '2016/3/17') {
                            $published = '-';
                        }

                        $lastmodified = $this->common->get_formatted_datetime($airport['lastmodified']);

                        $parsed_date = date_parse($lastmodified);

                        $datee = $parsed_date['year'] . "/" . $parsed_date['month'] . "/" . $parsed_date['day'];

                        if ($datee < '2016/3/17') {
                            $lastmodified = '-';
                        }

                        $dataarray[] = array('<a href="' . site_url('wizard/company/' . $airport['aid']) . '" class="tooltip-success" data-rel="tooltip" title="Edit"> <i class="fa fa-pencil-square-o"></i> Edit</a>', $airport['aname'], $lastmodified, $published, $airport['approved'], $airport['modifiedby']);

                    endforeach;
                }
            }

            /**
             * 
             * End Search Companies Frontend
             */
            echo '{';

            echo '"draw": ' . $_POST['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

            echo '"data":';

            echo json_encode($dataarray);

            echo "}";
        }
    }

    /**
     * 
     * End Get Records for Modify Data frontend
     */

    /**
     * 
     * Start Multiple ACtion Cases like (Delete, Uupdate, email etc...)
     */
    public function doaction() {

        if ($this->input->is_ajax_request()) {

            $method = $this->input->post('action');

            switch ($method) {
                /**
                 * Start Delete Company location (Required Id)
                 */
                case "dellocation":

                    $affid = $this->input->post('affid');

                    $this->common->DeleteRecord("companylocations", array('clid' => $affid));
                    
                    $this->common->getactivity('delete_company_location','');

                    $this->common->setmessage("Company Location has been Deleted Successfully.", "1");

                    echo 1;

                    break;
                /**
                 * Start Delete Company Conact (Required Id)
                 */
                case "delcontact":

                    $affid = $this->input->post('affid');

                    $this->common->DeleteRecord("companycontacts", array('ccid' => $affid));

                    $effected = $this->db->affected_rows();
                    if ($effected > 0) {
                        $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));
                    }
                    
                    $this->common->getactivity('delete_company_contact','');

                    $this->common->setmessage("Contact information has been Deleted Successfully.", "1");

                    echo 1;

                    break;
                /**
                 * Start Delete affiliation (Required Id)
                 */
                case "delaffiliations":

                    $affid = $this->input->post('affid');

                    $this->common->DeleteRecord('affiliation', array('affid' => $affid));
                    
                    $this->common->getactivity('delete_affiliation','');

                    $this->common->setmessage("Affiliation has been Deleted Successfully.", "1");

                    echo 1;

                    break;

                case "sendrequest":

                    echo "sendrequest";

                    break;
                /**
                 * Start Change Password & Send Email (Required Id)
                 */
                case "changepassword":

                    $cpassword = $this->input->post('cpassword');

                    $npassword = $this->input->post('npassword');

                    $opassword = $this->input->post('opassword');

                    if ($npassword == $opassword) {

                        echo "Your new Password must different than current password.";
                    } else if (str_replace(" ", "", $npassword) != $npassword) {

                        echo "White Space in not allowed in password";
                    } else {

                        $currentUserinfo = $this->common->GetSingleRowFromTableWhere('users', array('user_id' => $this->common->GetCurrentUserInfo('user_id'))); //$user_id 

                        if ($currentUserinfo['password'] == md5($opassword)) {
                            $email = $this->session->userdata('email');

                            $this->common->UpdateRecord("users", array('user_id' => $currentUserinfo['user_id']), array('password' => md5($this->input->post('npassword'))));

                            $this->common->getactivity('change_password','');
                            
                            $link = 'changepassword';
                            $where1 = array('email_temp_name' => $link);
                            $email_template = $this->common->GetSingleRowFromTableWhere('tbl_email_temp', $where1);

                            $search_array = array('#first_name', '#last_name');

                            $replace_array = array($this->session->userdata('first_name'), $this->session->userdata('last_name'));

                            $message1 = $email_template['email_temp_text'];

                            $message = str_replace($search_array, $replace_array, $message1);

                            $this->load->library('email');

                            $config['smtp_host'] = 'mail.arnfactbook.com';
                            $config['smtp_user'] = 'support@arnfactbook.com';
                            $config['smtp_pass'] = 'Urbanexpo1';
                            $config['charset'] = 'iso-8859-1';
                            $config['mailtype'] = 'html';
                            $config['wordwrap'] = TRUE;
                            $this->email->initialize($config);

                            $this->email->from('support@arnfactbook.com', 'Arnfactbook Support');
                            $this->email->to($email);
                            $this->email->subject('Notification: ARNFactbook Password Changed.');

                            $data = array(
                                'message' => $message
                            );

                            $messagee = $this->email->message($this->load->view('frontend/email_temp', $data, true));

                            $this->email->send();

                            echo 1;
                        } else {

                            echo "Invalid original password.Your original password does not match.";
                        }
                    }

                    break;
                /**
                 * Start Chang eRe-Login Time (Required Id)
                 */
                case "changerelogin_time":

                    $relogin_time = $this->input->post('relogin_time');

                    if ($relogin_time != '') {

                        $this->common->UpdateRecord("tbl_setting", array('setting_id' => 1), array('re_login_time' => $this->input->post('relogin_time')));

                        $this->common->getactivity('change_relogin_time','');
                        
                        echo 1;
                    }

                    break;

                /**
                 * Start Delete Airport's Look Up Location (Required Id)
                 */
                case "lookup_list_for_locations":

                    $this->common->UpdateRecord("lkpairportlist", array('aid' => $this->input->post('aid')), array('flag' => 0));

                    $this->common->setmessage("Lookup list for locations of airport has been deleted successfully.", 1);

//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['delete_airport_lookup']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('delete_airport_lookup','');

                    echo 1;

                    break;
                /**
                 * Start Select Text Language (Required Id)
                 */
                case "getnewdata":

                    $cols = array('text_language_id');

                    $res = $this->common->CustomQueryALL('SELECT text_language_id FROM text_language');

                    break;
                /**
                 * Start Delete Ads (Required Id)
                 */
                case "deleteAds":

                    $this->common->DeleteRecord("advertising", array('adid' => $this->input->post('adid')));

                    $this->common->setmessage("Advertise has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Company Temporary (Required Id)
                 */
                case "deletecompany":

                    $this->common->UpdateRecord("companies", array('cid' => $this->input->post('cid')), array("comp_is_active" => 0));

                    $this->common->getactivity('inactive_company','');
                    
                    $this->common->setmessage("Company has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Company Permanent (Required Id)
                 */
                case "deletecompanyp":

                    $this->common->DeleteRecord("companies", array('cid' => $this->input->post('cid')));
                    
                    $this->common->getactivity('delete_company','');

                    $this->common->setmessage("Company has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Not Published Company Temporary (Required Id)
                 */
                case "deletenotpublishcompany":

                    $this->common->UpdateRecord("companies", array('cid' => $this->input->post('cid')), array("comp_is_active" => -1));
                    
                    $this->common->getactivity('inactive_company','');

                    $this->common->setmessage("Company has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Published Airport Temporary (Required Id)
                 */
                case "deleteairport":

                    $this->common->UpdateRecord("airports", array('aid' => $this->input->post('aid')), array("airport_is_active" => 0));

                    $this->common->setmessage("Airport has been deleted successfully.", 1);

//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['inactive_airport']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('inactive_airport','');

                    echo 1;

                    break;
                /**
                 * Start Delete Airport Permanent (Required Id)
                 */
                case "deleteairportp":

                    $this->common->DeleteRecord("airports", array('aid' => $this->input->post('aid')));

                    $this->common->setmessage("Airport has been Permanentaly deleted successfully.", 1);

//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['delete_airport']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('delete_airport','');

                    echo 1;

                    break;
                /**
                 * Start Delete Not Published Airport Temporary (Required Id)
                 */
                case "deletenotpublishairport":

                    $this->common->UpdateRecord("airports", array('aid' => $this->input->post('aid')), array("airport_is_active" => -1));
                    
//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['inactive_airport']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('inactive_airport','');

                    $this->common->setmessage("Airport has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Terminal (Required Id)
                 */
                case "delterminalname":

                    $this->common->DeleteRecord("terminals", array('tid' => $this->input->post('tid')));

                    $this->common->DeleteRecord('terminalsannual', array('tid' => $this->input->post('tid')));

//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['delete_terminal']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('delete_terminal','');

                    $this->common->setmessage("Terminals has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Update Subscriber (Required Id)
                 */
                case "updatesubscription":

                    $this->common->UpdateRecord("users", array('user_id' => $this->input->post('user_id')), array('fbosubscription' => $this->input->post('subscription')));

//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['user_subscription']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('user_subscription','');
                    
                    $this->common->setmessage("User Subscription has been update successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete User Temporary (Required Id)
                 */
                case "deleteuser":

                    $this->common->UpdateRecord("users", array('user_id' => $this->input->post('user_id')), "is_active = '0'");

                    $this->common->setmessage("User has been deleted successfully.", 1);

//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['inactive_user']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('inactive_user','');

                    echo 1;

                    break;
                /**
                 * Start Delete User Permanent (Required Id)
                 */
                case "deleteuserp":

                    $this->common->DeleteRecord("users", array('user_id' => $this->input->post('user_id')));

                    $this->common->setmessage("User has been Permanentaly deleted successfully.", 1);

//                    $log_id = $this->session->userdata('user_id');
//                    $first_name = $this->session->userdata('first_name');
//                    $last_name = $this->session->userdata('last_name');
//                    $adata = array('log_id' => $log_id, 'activity' => $first_name . ' ' . $last_name . ' ' . $this->config->config['delete_user']);
//                    $this->common->InsertInDb('tbl_user_activity_log', $adata);
                    
                    $this->common->getactivity('delete_user','');

                    echo 1;

                    break;
                /**
                 * Start Feedback on FAQ's (Required Id)
                 */
                case "updatefeedback":

                    $feedback = "";

                    $faqid = $this->input->post('faqid');

                    $first_name = $this->session->userdata('first_name');

                    $last_name = $this->session->userdata('last_name');

                    $email = $this->session->userdata('email');

                    $reason = $this->input->post('reason');

                    $check = $this->common->GetAllRowWithColumn('tbl_faq_feedback', 'faq_id,first_name,last_name', 'feedback_id > 0', 'feedback_id', 'ASC');

                    $cr_time = $this->common->get_created_time();

                    $getf = $_POST['feedback'];

                    if ($getf == 1) {

                        $getfeedback = $getf;
                    } else {

                        $getfeedback = 2;
                    }

                    foreach ($check as $c)
                        if ($faqid == $c['faq_id'] && $first_name == $c['first_name'] && $last_name == $c['last_name']) {

                            $this->common->setmessage("You have already submitted your feedback about this question.", -1);

                            echo -1;

                            exit;
                        }

                    $newarray = array('faq_id' => $faqid, 'feedback_status' => $getfeedback, 'created_date' => $cr_time, 'is_active' => 1, 'first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'reason' => $reason);

                    $insert = $this->common->InsertInDb('tbl_faq_feedback', $newarray);
                    
                    $this->common->getactivity('insert_faq_fb','');

                    $this->common->setmessage("Your feedback has been submitted successfully", 1);

                    echo 1;

                    break;
                /**
                 * Start Update Feedback Status and send email if Approved (Required Id)
                 */
                case "updatefeedbackadmin":

                    $feedbackid = $this->input->post('feedback_id');

                    $admin_status = $this->input->post('admin_status');

                    $email = $this->input->post('email');

                    $name = $this->input->post('name');

                    $update = $this->common->UpdateRecord('tbl_faq_feedback', array('feedback_id' => $feedbackid), array('admins_status' => $admin_status));

                    $this->common->getactivity('approve_faq_fb','');
                    
                    if ($admin_status == 1) {

                        $link = 'feedbacksuggestion';
                        $where1 = array('email_temp_name' => $link);
                        $email_template = $this->common->GetSingleRowFromTableWhere('tbl_email_temp', $where1);

                        $search_array = array('#first_name #last_name');

                        $replace_array = array($name);

                        $message1 = $email_template['email_temp_text'];

                        $message = str_replace($search_array, $replace_array, $message1);

                        $this->load->library('email');

                        $config['smtp_host'] = 'mail.arnfactbook.com';
                        $config['smtp_user'] = 'support@arnfactbook.com';
                        $config['smtp_pass'] = 'Urbanexpo1';
                        $config['charset'] = 'iso-8859-1';
                        $config['mailtype'] = 'html';
                        $config['wordwrap'] = TRUE;
                        $this->email->initialize($config);

                        $this->email->from('support@arnfactbook.com', 'Arnfactbook Support');
                        $this->email->to($email);
                        $this->email->subject('Feedback Segustion');

                        $data = array(
                            'message' => $message
                        );

                        $messagee = $this->email->message($this->load->view('frontend/email_temp', $data, true));

                        $this->email->send();
                    }

                    $this->common->setmessage("Your feedback has been submitted successfully", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete FAQ Category (Required Id)
                 */
                case "deletefaqcategory":

                    $this->common->UpdateRecord('tbl_faq_category', array('category_id' => $this->input->post('category_id')), array('is_active' => 0));

                    $this->common->getactivity('inactive_faq_category','');
                    
                    $this->common->setmessage("Category has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete FAQ Temporary/Permanent (Required Id)
                 */
                case "deletefaq":

                    $status = $this->input->post('faq_status');

                    if ($status == 1) {

                        $this->common->UpdateRecord('tbl_faq', array('faq_id' => $this->input->post('faq_id')), array('faq_is_active' => 0));

                        $this->common->getactivity('inactive_faq','');
                        
                        $this->common->setmessage("FAQ has been deleted successfully.", 1);
                    } else if ($status == 0) {

                        $this->common->DeleteRecord('tbl_faq', array('faq_id' => $this->input->post('faq_id')));

                        $this->common->getactivity('delete_faq','');
                        
                        $this->common->setmessage("FAQ has been permanently deleted successfully.", 1);
                    }

                    echo 1;

                    break;
                /**
                 * Start Delete Support Question Temporary (Required Id)
                 */
                case "deleteticket":

                    $this->common->UpdateRecord('support', array('support_id' => $this->input->post('support_id')), array('is_active' => 0));

                    $this->common->getactivity('inactive_support_q','');
                    
                    $this->common->setmessage("Ticket has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Support Question Permanent (Required Id)
                 */
                case "deleteticketp":

                    $this->common->DeleteRecord("support", array('support_id' => $this->input->post('support_id')));

                    $this->common->getactivity('delete_support_q','');
                    
                    $this->common->setmessage("Ticket has been Permanently deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Error Log Temporary (Required Id)
                 */
                case "deletelog":

                    $this->common->UpdateRecord('tbl_error_log_history', array('error_log_id' => $this->input->post('error_log_id')), array('error_log_active' => 0));

                    $this->common->getactivity('inactive_error_log','');
                    
                    $this->common->setmessage("Error log has been deleted successfully.", 1);

                    echo 1;

                    break;
                /**
                 * Start Delete Error Log Permanent (Required Id)
                 */
                case "deletelogp":

                  $this->common->DeleteRecord("tbl_error_log_history", array('error_log_id' => $this->input->post('error_log_id')));

                    $this->common->getactivity('delete_error_log','');
                    
                    $this->common->setmessage("Error log has been Permanently deleted successfully.", 1);

                    echo 1;

                    break;


                /**
                 * Start Set Airports List for Multiple Airport's report (Required Id)
                 */
                case "listofairport":

                    $list = explode(",", $this->input->post('listOfAirport'));

                    $i = 0;

                    if ($this->session->userdata('list') != "" && count($this->session->userdata('list')) > 0) {

                        $oldlist = $this->session->userdata('list');

                        while ($i < count($list)) {

                            if (!in_array($list[$i], $oldlist)) {

                                array_push($oldlist, $list[$i]);
                            }

                            $i++;
                        }

                        $this->session->set_userdata('list', $oldlist);
                    } else {

                        $list = explode(",", $this->input->post('listOfAirport'));

                        $this->session->set_userdata('list', $list);
                    }

                    echo 1;

                    break;
                /**
                 * Start Set Companies List for Multiple Company's report (Required Id)
                 */
                case "listofcompany":

                    $list = explode(",", $this->input->post('listOfCompany'));

                    $i = 0;

                    if ($this->session->userdata('listofcomp') != "" && count($this->session->userdata('listofcomp')) > 0) {

                        $oldlist = $this->session->userdata('listofcomp');

                        while ($i < count($list)) {

                            if (!in_array($list[$i], $oldlist)) {

                                array_push($oldlist, $list[$i]);
                            }

                            $i++;
                        }

                        $this->session->set_userdata('listofcomp', $oldlist);
                    } else {

                        $list = explode(",", $this->input->post('listOfCompany'));

                        $this->session->set_userdata('listofcomp', $list);
                    }

                    echo 1;

                    break;

                case "generatepdf":



                    break;

                default:

                    echo -1;
            }
        } else {

            $this->common->redirect(site_url());

            exit();
        }
    }

    /**
     * 
     * END Multiple Cases
     */

    /**
     * 
     * Start User Access Config
     */
    public function access_config() {

        $this->outputData['access_config'] = $this->common->GetAllRowFromTableWhere('tbl_access_config', 'is_active = 1');
    }

    /**
     * 
     * END User Access Config
     */

    /**
     * 
     * Start All Company Locations
     */
    public function location_pagi() {

        $cid = $_POST['id'];

        $tblcolumnsarray = array('acity');

        $columnsnumber = $_POST['order'][0]['column'];

        $ordertype = $_POST['order'][0]['dir'];

        $dataarray = array();

//        $this->outputData['companysearchadds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3')  AND advertising.cid='$cid'");

        $from = 'companies';

        $where = "companies.cid = '" . $_POST['id'] . "' AND airports.aname != '' AND airports.acity != '' AND airports.astate != '' ";

        $select = "companylocations.cid,companylocations.aid,companylocations.tid,companylocations.IATA,airports.aname,airports.acity,airports.astate,airports.acountry";

        $join_array2 = array(
            array('companylocations', 'companylocations.cid = companies.cid', 'LEFT'),
            array('airports', 'airports.aid = companylocations.aid', 'LEFT'),
            array('terminals', 'terminals.tid = companylocations.tid', 'LEFT')
        );

        $this->outputData['clinfo'] = $this->common->JoinTables($select, $from, $join_array2, $where, 'airports.acity');

        $sqlp = "select * from companies WHERE cid = " . $cid . " ";

        $published_date = $this->common->CustomQueryALL($sqlp);

        $parsed_datep = date_parse($published_date[0]['published']);

        $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

        if ($datep > '2016/3/17') {

            if (isset($_POST['search']['value']) && $_POST['search']['value'] != "") {

                $searchwords = $_POST['search']['value'];

                $like = array('acity' => $searchwords);

                $orlike = array('astate' => $searchwords);

                $this->outputData['total'] = $this->common->JoinTableQueryLikeCounttype2($from, $join_array2, $tblcolumnsarray[$columnsnumber], $ordertype, $this->uri->segment(2), $like, $orlike, $where);

                $this->outputData['clinfo'] = $this->common->JoinTableQueryLiketype2($from, $join_array2, $tblcolumnsarray[$columnsnumber], $ordertype, $this->uri->segment(2), $like, $orlike, $where);
            } else {

                $this->outputData['total'] = $this->common->JoinTableQuerytype2($select, $from, $join_array2, $tblcolumnsarray[$columnsnumber], $ordertype, "", true, $where);

                $this->outputData['clinfo'] = $this->common->GetAllRowWithColumnStart1($from, $select, $join_array2, $where, $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], $_POST['length']);
            }
        } else {
            $this->outputData['total'] = $this->common->JoinTableQuerytype2($select, $from, $join_array2, $tblcolumnsarray[$columnsnumber], $ordertype, "", true, "companies.cid = -1 AND airports.aname != '' AND airports.acity != '' AND airports.astate != '' ");

            $this->outputData['clinfo'] = $this->outputData['clinfo'] = $this->common->GetAllRowWithColumnStart1($from, $select, $join_array2, "companies.cid = -1 AND airports.aname != '' AND airports.acity != '' AND airports.astate != '' ", $tblcolumnsarray[$columnsnumber], $ordertype, $_POST['start'], $_POST['length']);
        }

        foreach ($this->outputData['clinfo'] as $ci):

            $record = $ci['acity'] . ",&nbsp;" . $ci['astate'] . "&nbsp;(" . $ci['aname'] . ")";

            $dataarray[] = array($record);

        endforeach;

        echo '{';

        echo '"draw": ' . $_POST['draw'] . ',

		  			"recordsTotal": ' . $this->outputData['total'] . ',

			  		"recordsFiltered": ' . $this->outputData['total'] . ',';

        echo '"data":';

        echo json_encode($dataarray);

        echo "}";
    }

    /**
     * 
     * End All Company Locations 
     */

    /**
     * 
     * Start Edit Airport Tenant (Required Id) 
     */
    public function edit_tanant() {

        $oid = $this->input->post('oid');

        $edittanants = $this->common->get_all_row_from_table_where('outlets', array('oid' => $oid));

        $this->outputData['edittanants'] = $edittanants;

        $this->outputData['total'] = $edittanants;

        foreach ($this->outputData['edittanants'] as $edittanants):

            $exp = $this->common->get_formatted_datetime_n($edittanants['exp']);

            if ($edittanants['exp_date'] != '0000-00-00 00:00:00' && $edittanants['exp_date'] != '') {
                $exp_date = $edittanants['exp_date'];
            } else {
                $exp_date = '';
            }

            $dataarray[] = array($edittanants['outletname'], $edittanants['companyname'], $edittanants['categoryid'], $edittanants['numlocations'], $edittanants['termlocation'], $edittanants['sqft'], $exp, $edittanants['exp_reason'], $exp_date);

        endforeach;

        echo json_encode($dataarray);
    }

    /**
     * 
     * End Edit Airport Tenant 
     */

    /**
     * 
     * Start Edit Airport Contact (Required Id) 
     */
    public function edit_contact() {

        $aid = $this->input->post('aid');
        $this->common->check_user_airport($aid);
        $acid = $this->input->post('acid');

        $this->common->check_user_airport($aid);

        if ($this->input->post('MM_update') != "") {
            $acid = $this->input->post('airport_contact_id');
            $newarray = array(
                'mgtresponsibility' => $this->input->post('mgtresponsibility'),
                'afname' => $this->input->post('afname'),
                'alname' => $this->input->post('alname'),
                'atitle' => $this->input->post('atitle'),
                'acompany' => $this->input->post('acompany'),
                'aaddress1' => $this->input->post('aaddress1'),
                'aaddress2' => $this->input->post('aaddress2'),
                'accity' => $this->input->post('accity'),
                'acstate' => $this->input->post('acstate'),
                'aczip' => $this->input->post('aczip'),
                'accountry' => $this->input->post('accountry'),
                'aphone' => $this->input->post('aphone1'),
                'afax' => $this->input->post('afax1'),
                'aemail' => $this->input->post('aemail'),
                'awebsite' => $this->input->post('awebsite'),
                'priority' => $this->input->post('priority')
            );
            $where = array(
                'acid' => $acid);
            $this->common->UpdateRecord('airportcontacts', $where, $newarray);
            
            $IATA = $this->common->get_ariport_info($aid);
            
            $this->common->getactivity('edit_airport_contact',' ('.$IATA.')');
            
            $this->common->setmessage("Contact has been updated successfully.", 1);
        }

        $contact = $this->common->JoinTableQuerytype1('*', 'airportcontacts', 'lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'airportcontacts.priority', 'ASC', '', '', array('acid' => $acid));

        $this->outputData['airport_contact'] = $contact;

        foreach ($this->outputData['airport_contact'] as $a_contact):

            $dataarray[] = array($a_contact['mgtresponsibility'], $a_contact['afname'], $a_contact['aaddress1'], $a_contact['accity'], $a_contact['aczip'],
                $a_contact['aphone'], $a_contact['aemail'], $a_contact['atitle'], $a_contact['acompany'], $a_contact['alname'], $a_contact['aaddress2'],
                $a_contact['acstate'], $a_contact['accountry'], $a_contact['afax'], $a_contact['awebsite'], $a_contact['priority']);


        endforeach;

        echo json_encode($dataarray);
    }

    /**
     * 
     * End Edit Airport Contact
     */

    /**
     * 
     * Start FAQ's Activity log (Required id)
     */
    public function view_activity_log() {

        $start = $this->input->get('start') . " 00:00:00";

        $end = $this->input->get('end') . " 00:00:00";

        $dataarray = array();

        $this->outputData['listoffaq'] = $this->common->GetAllRowWithColumnWhere('tbl_faq_feedback', '*', 'tbl_faq', 'tbl_faq_feedback.faq_id=tbl_faq.faq_id', 'inner', array('tbl_faq_feedback.faq_id >' => 0, 'is_active' => 1, 'created_date >' => $start, 'created_date <' => $end));

        foreach ($this->outputData['listoffaq'] as $user):

            $date = date("Y-m-d", strtotime($user['created_date'])) . "T" . date("H:i:s", strtotime($user['created_date']));

            $q = "Q: " . $user['faq_question'];
            $ans = "\nAns: " . $user['faq_answer'];

            if ($user['reason'] != "" || $user['reason'] != " ") {
                $reason = "\nReason: " . $user['reason'];
            } else {
                $reason = "";
            }

            $dataarray[] = array('id' => $user["feedback_id"], 'title' => $user['first_name'] . " " . $user['last_name'], 'start' => $date, 'description' => "Q: " . $user['faq_question'] . "\nAns: " . $user['faq_answer'] . $reason);

        endforeach;

        echo json_encode($dataarray);
    }

    /**
     * 
     * End FAQ's Activity log
     */

    /**
     * 
     * Start Error Activity log (Required id)
     */
    public function view_error_activity_log() {

        $start = $this->input->get('start') . " 00:00:00";

        $end = $this->input->get('end') . " 00:00:00";

        $dataarray = array();

        $this->outputData['listoferrorlogs'] = $this->common->get_all_row_from_table_where('tbl_error_log_history', array('error_log_active' => 1, 'error_log_created_on >' => $start, 'error_log_created_on <' => $end));

        foreach ($this->outputData['listoferrorlogs'] as $errorlog):

            $date = date("Y-m-d", strtotime($errorlog['error_log_created_on'])) . "T" . date("H:i:s", strtotime($errorlog['error_log_created_on']));

            $dataarray[] = array('id' => $errorlog["error_log_id"], 'title' => $errorlog['error_log_subject'], 'start' => $date);

        endforeach;

        echo json_encode($dataarray);
    }

    /**
     * 
     * End Error Activity log
     */

    /**
     * 
     * Start Airport Activity log (Required id)
     */
    public function view_aiorport_activity_log() {

        $start = $this->input->get('start') . " 00:00:00";

        $end = $this->input->get('end') . " 00:00:00";

        $dataarray = array();

        $listofairport = $this->common->get_all_row_from_table_where('airports', array('IATA !=' => '', 'published >' => 0, 'airport_is_active' => 1, 'lmodified >' => $start, 'lmodified <' => $end));

        foreach ($listofairport as $airportlog):

            $date = date("Y-m-d", strtotime($airportlog['lmodified'])) . "T" . date("H:i:s", strtotime($airportlog['lmodified']));

            $dataarray[] = array('id' => $airportlog["aid"], 'class' => 'test', 'title' => "\n" . $airportlog['IATA'] . "\n M By: " . $airportlog['modifiedby'], 'start' => $date, 'description' => $airportlog['modifiedby']);

        endforeach;

        echo json_encode($dataarray);
    }

    /**
     * 
     * End Airport Activity log
     */

    /**
     * 
     * Start Company Activity log (Required id)
     */
    public function view_company_activity_log() {

        $start = $this->input->get('start') . " 00:00:00";

        $end = $this->input->get('end') . " 00:00:00";

        $dataarray = array();

        $listofcompanies = $this->common->get_all_row_from_table_where('companies', array('cid >' => 0, 'published >' => 0, 'comp_is_active' => 1, 'lmodified >' => $start, 'lmodified <' => $end));

        foreach ($listofcompanies as $companylog):

            $date = date("Y-m-d", strtotime($companylog['lmodified'])) . "T" . date("H:i:s", strtotime($companylog['lmodified']));

            $dataarray[] = array('id' => $companylog["cid"], 'title' => $companylog['companyname'] . "\n M By: " . $companylog['modifiedby'], 'start' => $date, 'description' => $companylog['modifiedby']);

        endforeach;

        echo json_encode($dataarray);
    }

    /**
     * 
     * End Company Activity log
     */

    /**
     * 
     * Start User Activity log (Required id)
     */
    public function view_user_activity_log() {

        $start = $this->input->get('start') . " 00:00:00";

        $end = $this->input->get('end') . " 00:00:00";

        $dataarray = array();

        $listofusers = $this->common->get_all_row_from_table_where('users', array('user_id >' => 0, 'is_active' => 1, 'lastmodified >' => $start, 'lastmodified <' => $end));

        foreach ($listofusers as $userlog):

            $date = date("Y-m-d", strtotime($userlog['lastmodified'])) . "T" . date("H:i:s", strtotime($userlog['lastmodified']));

            $dataarray[] = array('id' => $userlog["user_id"], 'title' => $userlog['first_name'] . " " . $userlog['last_name'] . "\n M By: " . $userlog['modifiedby'], 'start' => $date, 'description' => $userlog['modifiedby']);

        endforeach;

        echo json_encode($dataarray);
    }

    /**
     * 
     * End User Activity log
     */

    /**
     * 
     * Start frontend Search (Required string)
     */
    public function search_links() {

        if ($this->input->is_ajax_request()) {

            $search = $this->input->post('slink');

            if ($search !== '') {
                $searchname = $this->common->GetAllRowWithColumn1('tbl_search_links', 'search_name, search_link', "search_name LIKE '%$search%'", 'search_name', 'ASC', 0, 50);
            }

            $ar = array();

            foreach ($searchname as $s) {
                $ar[] = array('<a href="' . site_url($s['search_link']) . '">' . $s['search_name'] . '</a>',);
            }
            echo json_encode($ar);

            exit();
        }
    }

    /**
     * 
     * End frontend Search
     */
}
