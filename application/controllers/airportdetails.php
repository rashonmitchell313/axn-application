<?php
class airportdetails extends CI_Controller
{
	public $outputData="";	
	function __construct()	
	{		
		parent::__construct();	
		$this->common->ThisSecureArea('user');
		$this->outputData['menu']='frontend/include/menu';		
		$this->outputData['footer']='frontend/include/footer';
		$this->outputData['header']='frontend/include/header';
		$this->outputData['rsStates']=$this->common->GetAllRowOrderBy("states","state_code","asc");
		$this->outputData['row_rsCountries']=$this->common->GetAllRowOrderBy("countries","countries_name","asc");
		$this->outputData['ListofCompanies']=$this->common->GetAllRowOrderBy("companies","companyname","asc");
		$this->outputData['ListOfAirports']=$this->common->GetAllRowOrderBy("airports","aname","asc");
	}
	public function index()
	{
		if($this->input->post('apid') == "" || $this->input->post('arr') == "" || $this->input->post('year') == "")
		{
			$this->load->view('frontend/404',$this->outputData);
		}
		else
		{
			$this->outputData['filter'] = $this->input->post('arr');
			$aids = implode(",", $this->input->post('apid'));
			$ayear = implode(",", $this->input->post('year'));
			$join = array(
						array('airportcontacts', 'airports.aid = airportcontacts.aid', 'LEFT'),
						array('lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility = airportcontacts.mgtresponsibility', 'LEFT'),
						array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'),		
						array('airporttotals', 'airports.aid=airporttotals.aid', 'LEFT'),			
						array('terminals', 'airports.aid=terminals.aid', 'LEFT'),			
						array('terminalsannual', 'terminals.tid=terminalsannual.tid', 'LEFT')
					);
	foreach($this->input->post('year') as $ay):		
	$obj_res = $this->common->JoinTables('*', 'airports', $join, array("airports.aid IN ($aids) AND airports.aid >"=>0), "airports.aid");
			
				if(count($obj_res) > 0)
				{
				
				
				//var_dump($this->db);exit;
					foreach($obj_res as $obj):
						$this->outputData['ap'][$ay]['contactlist'][$obj['aid']][$obj['acid']] = $obj;
						$this->outputData['ap'][$ay]['terminal'][$obj['aid']][$obj['tid']] = $obj;
						$this->outputData['ap'][$ay]['airportinfo'][$obj['aid']][$obj['aaid']] = $obj;
						$this->outputData['ap'][$ay]['airport_listing'][$obj['aid']] = $obj;
					endforeach;
     
					foreach($this->input->post('apid') as $aid)
					{
						//echo $this->db->last_query();exit;
						
						$this->outputData['ap'][$ay]['allcat'][$aid] = $this->common->CustomQueryALL('SELECT DISTINCT category FROM `outlets` INNER JOIN lkpcategory ON outlets.categoryid = lkpcategory.categoryid  WHERE aid = '.$aid.' GROUP BY outlets.categoryid');
						$tdeplaning = array();
						$tenplaning = array();
						$tepdomestic = array();
						$ctdata = array();
					//	 $gdata[] = array($t['terminalabbr'], intval($t['tdeplaning']), intval($t['tenplaning']), intval($t['tepdomestic']));	
						$tdeplaning[] = array('Years');
						$tenplaning[] = array('Years');
						$tepdomestic[] = array('Years');
						
						//$ctdata[] = array('Airport Terminal','Sales EP');
						$ctdata[] = array('Year');
						$year = $this->input->post('year');
						//$val = array();
						$terminal = array();
						$terminal2 = array();
						foreach($this->outputData['ap'][$ay]['terminal'][$aid] as $t):		
							$terminal[] = $t;
							array_push($tdeplaning[0], $t['terminalabbr']);
							array_push($tenplaning[0], $t['terminalabbr']);
							array_push($tepdomestic[0], $t['terminalabbr']);
							array_push($ctdata[0], $t['terminalabbr']);
						//	$val = intval($t['tdeplaning']);
																			
								
						endforeach;
						foreach($year as $j=>$y): // Year...
							//$this->db->close();
							//$this->db =  $this->load->database($y, TRUE);
							$obj_res = $this->common->JoinTables('*', 'airports', $join, array("airports.aid IN ($aids) AND airports.aid >"=>0), "airports.aid");
							foreach($obj_res as $obj):
							$this->outputData['ap'][$y]['contactlist'][$obj['aid']][$obj['acid']] = $obj;
							$this->outputData['ap'][$y]['terminal'][$obj['aid']][$obj['tid']] = $obj;
							$this->outputData['ap'][$y]['airportinfo'][$obj['aid']][$obj['aaid']] = $obj;
							$this->outputData['ap'][$y]['airport_listing'][$obj['aid']] = $obj;
							endforeach;
							foreach($this->outputData['ap'][$y]['terminal'][$aid] as $t2):		
									$terminal2[] = $t2;
							endforeach;
							$tdeplaning[$j+1] = array($y);
							$tenplaning[$j+1] = array($y);
							$tepdomestic[$j+1] = array($y);
							$ctdata[$j+1]= array($y);
							foreach($terminal2 as $i=>$term):	
								array_push($tdeplaning[$j+1], intval($term['tdeplaning']));
								array_push($tenplaning[$j+1], intval($term['tenplaning']));
								array_push($tepdomestic[$j+1], intval($term['tepdomestic']));
								
								$totgr = $term["fbgrosssales"] + $term["srgrosssales"] + $term["nggrosssales"]."";
												
								$totsalesep = floatval(number_format(($term["tenplaning"] != 0)?($totgr/$term["tenplaning"]):0, 2, '.', ''));
					
								array_push($ctdata[$j+1],$totsalesep);
					
					//$ctdata[] = array($ctdata[$j+1],$t['terminalabbr'], $totsalesep);
								
								
								
							endforeach;
							
							$terminal2=array();							
						endforeach;
						/*echo "<pre>";
							print_r($gdata);
						echo "</pre>";
						
						exit;*/
						$this->outputData['ap'][$ay]['ayear'] = $ay;
						$this->outputData['ap'][$ay]['ctdata'][$aid]=$ctdata;
						$this->outputData['ap'][$ay]['ptgdata'][$aid]=$tdeplaning;
						$this->outputData['ap'][$ay]['dtgdata'][$aid]=$tenplaning;
						$this->outputData['ap'][$ay]['ftgdata'][$aid]=$tepdomestic;
					}	
				}
				else{
				$this->load->view('frontend/404',$this->outputData);
				}
				
				endforeach;
				$this->load->view('frontend/airport/airportdetails',$this->outputData);
		}
	}
	
	public function get_result()
	{
		$category = $this->input->post('cate');
		$aid = $this->input->post('aid');
		$start = (int)$this->input->post('start');
		$limit = (int)$this->input->post('length');
		$products = $this->common->CustomQueryALL("SELECT SQL_CALC_FOUND_ROWS * FROM outlets LEFT JOIN lkpcategory ON outlets.categoryid = lkpcategory.categoryid WHERE lkpcategory.category = '$category' AND aid = $aid ORDER BY outletname LIMIT $start, $limit");
		
		$total_rows = $this->common->CustomQueryROw("SELECT FOUND_ROWS() AS num");
		$data = array();
		foreach($products as $i=>$product)
		{
			$date_val = $this->common->check_date($product['exp']);
			
			$company = (empty($product['companyname']))?'':' ('.$product['companyname'].')';								
			
			$data[$i]['outletname'] = addslashes($product['outletname']).$company;
			$data[$i]['productdescription'] = addslashes($product['productdescription']);
			$data[$i]['termlocation'] = $product['termlocation'];
			$data[$i]['numlocations'] = $product['numlocations'];
			$data[$i]['sqft'] = number_format($product['sqft']);
			
			if(!$this->common->check_date($product['exp']))
			{												
				$date_val=$product['exp']; 												
			}												
			elseif(strtotime($product['exp']) =='')												
			{												
				$date_val="00/00/0000";												
			}												
			elseif(is_null($product['exp']))												
			{												
				$date_val="00/00/0000";												
			}												
			elseif($product['exp']=='0000-00-00')												
			{												
				$date_val="00/00/0000";												
			}												
			else												
			{												
				$date_val=date('m/d/Y', strtotime($product['exp'])); 												
			}
			$data[$i]['exp'] = $date_val;
		}
			
		echo json_encode(array('aaData'=>$data, 'sEcho'=>$start+1, 'iTotalRecords'=>$total_rows['num'], 'iTotalDisplayRecords'=>$total_rows['num']));
	}
}
?>