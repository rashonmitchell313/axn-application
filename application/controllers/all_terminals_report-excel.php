<?php 
require_once('../../Connections/dbconn.php'); 

require_once('../../includes/common.php');

require_once('../../checklogin.php');

require_once('../../includes/portal.php');

$reportfor='';
$reptitle="";	
function subval_sort($a,$subkey) 
{
	foreach($a as $k=>$v) {
		$b[$k] = strtolower($v[$subkey]);
	}
	asort($b);
	foreach($b as $key=>$val) {
		$c[] = $a[$key];
	}
	return $c;
}
$airport = read_table("","SELECT DISTINCT(`IATA`),aid as aid,`acity` FROM `airports`");
$users = array();
$TCurrentSqFt=0;
	$TGrossSale=0;
	$TSalesEp=0;
	$TRentRev=0;
foreach ($airport as $AP) 
{
	$sql = "select * from terminals left join terminalsannual on terminalsannual.tid=terminals.tid 
 where aid='".$AP['aid']."' ORDER BY terminals.tid";	
	$airportname = "SELECT * FROM airports  where aid=".$AP['aid']."";
	$airportinfo=read_table("",$airportname);		
	$tl=read_table("",$sql);	
	$fbgrosssalestotal	= 0;
	$srgrosssalestotal	= 0;
	$nggrosssalestotal	= 0;				
	$fbrentrevtotal	= 0;
	$srrentrevtotal	= 0;
	$ngrentrevtotal	= 0;
	$indi_terms_list="";
	$FBCurrentSqFt=0 ;
	$FBGrossSale=0;
	$FBSalesEp=0;
	$FBRentRev=0;
	$NGCurrentSqFt=0 ;
	$NGGrossSale=0;
	$NGSalesEp=0;
	$NGRentRev=0;
	$SRCurrentSqFt=0 ;
	$SRGrossSale=0;
	$SRSalesEp=0;
	$SRRentRev=0;
	$DFCurrentSqFt=0 ;
	$DFGrossSale=0;
	$DFSalesEp=0;
	$DFRentRev=0;	
	$indi_terms_list="";
	if($tl != "")
	{
		
		foreach ($tl as $_) 
		{				
					
			$TCurrentSqFt+=$CurrentSqFt;	
			$TGrossSale+=$GrossSale;
			$TSalesEp+=$SalesEp;
			$TRentRev+=$RentRev;	 
			$FBCurrentSqFt= $_["fbcurrsqft"];
						$FBGrossSale=$_["fbgrosssales"];
						$FBSalesEp=$_["fbsalesep"];
						$FBRentRev=$_["fbrentrev"];	
						$NGCurrentSqFt= $_["ngcurrsqft"];
						$NGGrossSale=$_["nggrosssales"];
						$NGSalesEp=$_["ngsalesep"];
						$NGRentRev=$_["ngrentrev"];	
						$SRCurrentSqFt= $_["srcurrsqft"];
						$SRGrossSale=$_["srgrosssales"];
						$SRSalesEp=$_["srsalesep"];
						$SRRentRev=$_["srrentrev"];
						$DFCurrentSqFt= $_["dfcurrsqft"];
						$DFGrossSale=$_["dfgrosssales"];
						$DFSalesEp=$_["dfsalesep"];
						$DFRentRev=$_["dfrentrev"];	
						if($_['tenplaning']==0)
						{
							$SalesEP=($FBGrossSale+$NGGrossSale+$SRGrossSale+$DFGrossSale)/1;						
						} else
						{
							$SalesEP=($FBGrossSale+$NGGrossSale+$SRGrossSale+$DFGrossSale)/$_['tenplaning'];	
						}
						
						$users[] = array('IATA_ID' => $airportinfo[0]['IATA'],'Terminal_Abbr' => $AP["acity"],
						'Terminal_Name'=>$_["terminalname"],'EPAX'=>0,'Intl_EPAX'=>0,
						'FBCurrentSqFt'=>$FBCurrentSqFt,'FBGrossSale'=>$FBGrossSale,
						'NGCurrentSqFt'=>$NGCurrentSqFt,'NGGrossSale'=>$NGGrossSale,
						'SRCurrentSqFt'=>$SRCurrentSqFt,'SRGrossSale'=>$SRGrossSale,
						'DFCurrentSqFt'=>$DFCurrentSqFt,'DFGrossSale'=>$DFGrossSale,
						'SalesEP'=>$SalesEP,'tenplaning'=>$_['tenplaning'],'tepintl'=>$_['tepintl']);		
		}
	}
} // end foreach ($airport as $AP) 

$songs = subval_sort($users,'SalesEP');
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Los_Angeles');
if (PHP_SAPI == 'cli')
	die('This report should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';
 


$val='Top 50 TERMNLS '.$reptitle;
$ref = date("Y"); 

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle($val)
							 ->setSubject($val)
							 ->setDescription($val)
							 ->setKeywords($val)
							 ->setCategory($val);
//$val='Top 50 Airports '.$ref;
// Style for the Main heading
$BGstyleArray = array(
	'fill'  => array(
			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
			'argb' => 'F2F2F2'
			)
	 	)
	);
$styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ));
$style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );
// Style for the column titles
$heading = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    )
	);
$titlestyle = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    ),
	'borders' => array(
	'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    ),
    'top' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    )
  )
);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A1',$year.' All Terminals');
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('All_TRMNL_'.str_replace(" ","_",$reptitle));
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);		
		$aenplaning    = '';
		$afbgrosssales = '';
		$asrgrosssales = '';
		$anggrosssales = '';
		$avg_sp 	   = '';
		$aconcessiongrosssales = '';
		$airterm ="SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airports.IATA!='YYJ' AND airports.IATA!='YVR'  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC LIMIT 0,50";
		$count 		= 1;
		$headcell	= 2;
		$titlecell	= 3;
		$cell 		= 4;		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headcell.':E'.$headcell)->applyFromArray($heading);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$headcell.':G'.$headcell);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$headcell.':G'.$headcell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$headcell, 'Food and Beverage');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$headcell.':I'.$headcell);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$headcell.':I'.$headcell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$headcell, 'News/Gifts');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$headcell.':K'.$headcell);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$headcell.':K'.$headcell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$headcell, 'Specialty Retail');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$headcell.':M'.$headcell);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$headcell.':M'.$headcell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$headcell, 'Duty Free');
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headcell,' ');			
		$objPHPExcel->getActiveSheet()->getStyle('A'.$titlecell.':M'.$titlecell)->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$titlecell,'IATA ID');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$titlecell, 'City');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$titlecell, 'Terminal Name');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$titlecell, 'EPAX');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$titlecell, 'Intl. EPAX');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$titlecell, 'Current Sq.Ft.');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$titlecell, 'Gross Sales');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$titlecell, 'Current Sq.Ft.');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$titlecell, 'Gross Sales');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$titlecell, 'Current Sq.Ft.');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$titlecell, 'Gross Sales');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$titlecell, 'Current Sq.Ft');			
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$titlecell, 'Gross Sales');
		//$objPHPExcel->getActiveSheet()->setCellValue('H'.$titlecell, 'Sales E/P');
		$counter=0;
 		 for($i=count($songs)-1;$i>=0;$i--)
		{
			//$airportlist=mysql_fetch_assoc($airtermQuery); 
			//$all=$airportlist["afbgrosssales"]+$airportlist["asrgrosssales"]+$airportlist["anggrosssales"];
			if($counter%2 == 0){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':M'.$cell)->applyFromArray($BGstyleArray);
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell, stripslashes($songs[$i]["IATA_ID"]));	
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$cell, $songs[$i]["Terminal_Abbr"]);	
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$cell, $songs[$i]["Terminal_Name"]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$cell, $songs[$i]["tenplaning"]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$cell, $songs[$i]["tepintl"]);	
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell, $songs[$i]["FBCurrentSqFt"]);				
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$cell, "$ ".number_format($songs[$i]["FBGrossSale"],2));
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$cell,$songs[$i]["NGCurrentSqFt"]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$cell, "$ ".number_format($songs[$i]["NGGrossSale"],2));
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$cell, $songs[$i]["SRCurrentSqFt"]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$cell, "$ ".number_format($songs[$i]["SRGrossSale"],2));
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$cell, $songs[$i]["DFCurrentSqFt"]);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$cell, "$ ".number_format($songs[$i]["DFGrossSale"],2));			
			$counter++;
			$cell++;			
		}
		$cell=$cell+2;

		//Collect Total
		/*$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':B'.$cell)->applyFromArray($heading);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$cell.':B'.$cell);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell,'Totals');
		//Collect Total Enplanements Numbers
		if($aenplaning)
			{$tot_aenplaning=number_format($aenplaning);}
			else{$tot_aenplaning='';}
		$objPHPExcel->getActiveSheet()->getStyle('C'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$cell,$tot_aenplaning);
		//Collect Total F&B Total Sales
		if($afbgrosssales)
			{$fb_afbgrosssales='$'.number_format($afbgrosssales);}
			else{$fb_afbgrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$cell,$fb_afbgrosssales);
		//Collect Specialty Total Sales
		if($asrgrosssales)
			{$sr_asrgrosssales='$'.number_format($asrgrosssales);}
			else{$sr_asrgrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('E'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$cell,$sr_asrgrosssales);
		//Collect NG Total Sales
		if($anggrosssales)
			{$ng_anggrosssales='$'.number_format($anggrosssales);}
			else{$ng_anggrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('F'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell,$ng_anggrosssales);
		//Collect Total Sales (Excluding DF)
		if($aconcessiongrosssales)
			{$to_aconcessiongrosssales='$'.number_format($aconcessiongrosssales);}
			else{$to_aconcessiongrosssales='';}
		$objPHPExcel->getActiveSheet()->getStyle('G'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$cell,$to_aconcessiongrosssales);
		//Collect Total Sales E/P
		if($avg_sp)
			{$to_avg_sp='$'.number_format($avg_sp, 2, '.', '');}
			else{$to_avg_sp='';}
		$objPHPExcel->getActiveSheet()->getStyle('H'.$cell)->applyFromArray($heading);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$cell,'AVG='.$to_avg_sp);
		//Add Date In the Footer
		$cell = $cell+5;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cell)->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell,date("Y-m-d H:i:s"));*/
//===================== Unset All Variables =========================================
unset($aenplaning);
unset($afbgrosssales);
unset($asrgrosssales);
unset($anggrosssales);
unset($aconcessiongrosssales);
unset($avg_sp);
//==============================================================

//==============================================================

$sheetName = 'All_Terminals_by_'.str_replace(" ","_",$reptitle).'.xlsx';
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$sheetName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
$objWriter->save('php://output');


exit;



//==============================================================

//==============================================================

//==============================================================





?>



