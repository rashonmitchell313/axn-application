<?php



class wizard extends CI_Controller {



    public $dataarray;

    public $outputData = array();



    function __construct() {

        parent::__construct();

        $this->common->ThisSecureArea('user');

        $this->outputData['menu'] = 'frontend/include/menu';

        $this->outputData['footer'] = 'frontend/include/footer';

        $this->outputData['header'] = 'frontend/include/header';

        $this->outputData['rsStates'] = $this->common->GetAllRowOrderBy("states", "state_code", "asc");

        $this->outputData['row_rsCountries'] = $this->common->GetAllRowOrderBy("countries", "countries_name", "asc");

        $this->load->library('typography');

        $this->lang->load('config', 'english');

    }



    /**

     * 

     * START Edit Company from Modify Data {Add & Edit Contacts, Locations & Company Info and Publish} (Required id)

     */

    public function company() {



        $str = $this->session->userdata('user_access');



        $lstr = strtolower(str_replace(" ", "", $str));



        $a = explode(",", $lstr);



        $aa = array_values($a);



        if (in_array("modifydata", $aa)) {



            $cid = $this->uri->segment(3);

            $this->common->check_user_company($cid);

            $now = $this->common->get_created_time();

//            $this->outputData['company_search_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3')  AND advertising.cid='$cid'");

            if ($this->input->post('action') && $this->input->post('action') != "") {



                $company = $this->common->GetSingleRowFromTableWhere('companies', array('cid' => $this->input->post('cid')));



                $cname = $company['companyname'];



                $username = $this->input->post('name');

                $email = $this->input->post('email');

                $comments = $this->input->post('comments');



                if ($this->input->post('action')) {

                    $actiontype = $this->input->post('actiontype');



                    //INSERT INTO companylocations (cid,aid) VALUES ('$_SESSION[c_cid]','$_REQUEST[c_aid]')"

                    if ($actiontype == "publish") {

                        if ($this->input->post('action') == 'approve') {

                            $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('approved' => $now));

                            $this->common->getactivity('approve_company','');

//                        $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('modifiedby' => $this->common->GetSessionKey('user_id')));

                            $effected = $this->db->affected_rows();

                            if ($effected > 0) {

                                $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));

                            }

                            $this->common->setmessage('Compnay information published successfully.', 1);

                            $this->common->setmessage('Compnay information <b>Approved</b> successfully.', 1);

                        }

                        if ($this->input->post('action') == 'publish') {

                            $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('published' => $now));

                            $this->common->getactivity('publish_company','');

                            $effected = $this->db->affected_rows();

                            if ($effected > 0) {

                                $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));

                            }



                            $this->send_email1($username, '', $cname, $email, $comments);



                            $this->common->setmessage('Compnay information published successfully.', 1);

                        }

                    }

                    if ($actiontype == "addlocation") {

                        $c_aid = $this->input->post('c_aid');

                        $ainfo = $this->common->GetSingleRowFromTableWhere('lkpairportlist', array('aid' => $c_aid));

                        $c_cid = $this->input->post('cid');

                        $dataarray = array('cid' => $c_cid, 'aid' => $c_aid);

                        $this->common->InsertInDb('companylocations', $dataarray);

                        $this->common->getactivity('insert_company_location','');



                        $effected = $this->db->affected_rows();

                        if ($effected > 0) {

                            $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));

                        }



                        $this->common->setmessage('New Location has been added successfully.', 1);

                    }

                    if ($actiontype == "updatecompnayinfo") {

                        $companytype = $this->input->post('companytype');

                        $dbe = $this->input->post('dbe');

                        $companyinfo = $this->input->post('companyinfo');



                        $companyinfo2 = str_replace(array('‘', '´', '`', '’', '‘', '“', '”'), array("'", "'", "'", "'", "'", '"', '"'), $companyinfo);



                        $companyinfo1 = $this->common->normalize($companyinfo2);



                        $dataarray = array('companytype' => $companytype, 'dbe' => $dbe, 'companyinfo' => $companyinfo1);



                        $this->common->UpdateRecord('companies', array('cid' => $cid), $dataarray);

                        $this->common->getactivity('edit_company','');

                        $effected = $this->db->affected_rows();

                        if ($effected > 0) {

                            $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));

                        }

                        $this->common->setmessage('Information has been update successfully.', 1);

                    } elseif ($actiontype == "updatecontact" || $actiontype == "addcontact") {

                        $printed = $this->input->post('printed');

                        if ($printed != "") {

                            $printed = 'Yes';

                        } else {

                            $printed = 'NO';

                        }

                        $cfname = $this->input->post('cfname');

                        $clname = $this->input->post('clname');

                        $cid = $this->input->post('cid');

                        $ctitle = $this->input->post('ctitle');

                        $caddress1 = $this->input->post('caddress1');

                        $caddress2 = $this->input->post('caddress2');

                        $cstate = $this->input->post('cstate');

                        $ccity = $this->input->post('ccity');

                        $ccountry = $this->input->post('ccountry');

                        $cemail = $this->input->post('cemail');

                        $cwebsite = $this->input->post('cwebsite');

                        $czip = $this->input->post('czip');

                        $cext = $this->input->post('cext');

                        $cphone = $this->input->post('cphone1') . "-" . $this->input->post('cphone2') . "-" .

                                $this->input->post('cphone3');

                        $cfax = $this->input->post('cfax1') . '-' . $this->input->post('cfax2') . '-' . $this->input->post('cfax3');

                        $cdisplay_order = $this->input->post('display_order');

                        $dataarray = array('ccity' => $ccity, 'czip' => $czip, 'cid' => $cid, 'cfname' => $cfname, 'clname' => $clname, 'ctitle' => $ctitle,

                            'caddress1' => $caddress1, 'caddress2' => $caddress2, 'cstate' => $cstate, 'ccountry' => $ccountry,

                            'cemail' => $cemail, 'cwebsite' => $cwebsite, 'cext' => $cext, 'cphone' => $cphone,

                            'cfax' => $cfax, 'display_order' => $cdisplay_order, 'printed' => $printed);

                        if ($actiontype == "updatecontact") {

                            $ccid = $this->input->post('ccid');

                            $this->common->UpdateRecord('companycontacts', array('ccid' => $ccid), $dataarray);

                            $this->common->getactivity('edit_company_contact','');

                            $this->common->setmessage('Contact information has been update successfully.', 1);

                        } else if ($actiontype == "addcontact") {

                            $this->common->InsertInDb('companycontacts', $dataarray);

                            $this->common->getactivity('add_company_contact','');

                            $this->common->setmessage('New Contact added successfully.', 1);

                        }

                        $effected = $this->db->affected_rows();

                        if ($effected > 0) {

                            $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));

                        }

                    }

                }

            }

            $contact_edit = $this->uri->segment(6);

            if (isset($contact_edit) && $contact_edit > 0) {



                $where = "companycontacts.cid = '" . $cid . "' AND companycontacts.is_active=1 

			AND companycontacts.ccid = " . $contact_edit . "  ORDER BY display_order";

                $this->outputData['ccontactedit'] = $this->common->JoinTable('*', 'companycontacts', 'companies', 'companycontacts.cid=companies.cid', 'LEFT', $where);

            }



            $this->outputData['ctype'] = $this->common->GetContentsAll('companytype', 'companies', 'companytype');



            $sell = "aid,aname,IATA,acity,astate";

            $this->outputData['listofloc'] = $this->common->CustomQueryALL("SELECT aid, aname, acity, IATA, astate FROM airports union select aid, aname, acity, IATA, astate from lkpairportlist order by acity ASC");



            $this->outputData['csinfo'] = $this->common->get_all_row_from_table('companies', array('cid' => $cid), '*');



            $where = "companycontacts.cid = '" . $cid . "' AND companycontacts.is_active=1 ORDER BY display_order";

            $this->outputData['ccontact'] = $this->common->JoinTable('*', 'companycontacts', 'companies', 'companycontacts.cid=companies.cid', 'LEFT', $where);



            $join_array = array(

                array('companylocations', 'companylocations.cid = companies.cid', 'LEFT'),

                array('airports', 'airports.aid = companylocations.aid', 'LEFT'),

                array('lkpairportlist', 'lkpairportlist.aid = companylocations.aid', 'LEFT')

            );



            $this->outputData['cloc'] = $this->common->CustomQueryALL("SELECT st1.*, st2.* FROM (SELECT `aid`, `acity`, `astate`,`aname` FROM `airports` 

                                                    UNION SELECT `aid`, `acity`, `astate`,`aname` FROM `lkpairportlist`) st1 

                                                    LEFT JOIN `companylocations` st2 ON `st2`.`aid` = `st1`.`aid`

                                                    LEFT JOIN `companies` ON `companies`.`cid` = `st2`.`cid` 

                                                    WHERE `companies`.`cid` = " . $this->db->escape($cid) . " ORDER BY `st1`.`acity`, `st1`.`astate`");



            $from = 'companies';

            $select = "companylocations.cid,companylocations.aid,companylocations.clid,companylocations.tid,companylocations.IATA,airports.aname,airports.acity,airports.astate,airports.acountry";

            $join_array2 = array(

                array('companylocations', 'companylocations.cid = companies.cid', 'LEFT'),

                array('airports', 'airports.aid = companylocations.aid', 'LEFT'),

                array('terminals', 'terminals.tid = companylocations.tid', 'LEFT')

            );

            $this->outputData['clinfo'] = $this->common->JoinTables($select, $from, $join_array2);



            $njoin_array = array(

                array('companylocations', 'companylocations.cid = companies.cid', 'LEFT'),

                array('lkpairportlist', 'lkpairportlist.aid = companylocations.aid', 'LEFT')

            );

            $this->outputData['cinfo'] = $this->common->JoinTables('*', 'companies', $njoin_array, array('companies.cid' => $cid), 'lkpairportlist.acity, lkpairportlist.astate');

            if ($this->uri->segment(4) == 'edit' || $this->uri->segment(4) == 'add' && in_array($this->uri->segment(5), array('step1', 'step2', 'step3'))) {

                $this->load->view("frontend/company/view-company-" . $this->uri->segment(5), $this->outputData);

            } else if ($this->uri->segment(4) == 'publish' || $this->uri->segment(4) == 'approved') {

                $this->load->view("frontend/company/publish", $this->outputData);

            } else {

                $this->load->view("frontend/company/view-company", $this->outputData);

            }

        } else {



            $this->common->setmessage("You are not allowed to access that page.", -1);



            redirect(base_url());

        }

    }

    /**

     * 

     * END Edit Company from Modify Data

     */



    /**

     * 

     * START Publish Airport (Required id)

     */

    public function airport_publish() {



        $str = $this->session->userdata('user_access');



        $lstr = strtolower(str_replace(" ", "", $str));



        $a = explode(",", $lstr);



        $aa = array_values($a);



        if (in_array("modifydata", $aa)) {



            $aid = $this->uri->segment(3);

            $this->common->check_user_airport($aid);

            $now = $this->common->get_created_time();



            if ($this->input->post('action') && $this->input->post('action') != "") {



                $airport = $this->common->GetSingleRowFromTableWhere('airports', array('aid' => $this->input->post('aid')));

                $IATA = $airport['IATA'];

                $aname = $airport['aname'];



                $username = $this->input->post('name');

                $email = $this->input->post('email');

                $comments = $this->input->post('comments');



                if ($this->input->post('action')) {

                    $actiontype = $this->input->post('actiontype');

                    if ($actiontype == "publish") {

                        if ($this->input->post('action') == 'approve') {

                            $this->common->UpdateRecord('airports', array('aid' => $this->input->post('aid')), array('approved' => $now));

                            $this->common->getactivity('approve_airport','');

                            $effected = $this->db->affected_rows();

                            if ($effected > 0) {

//                                $this->common->UpdateRecord('airports', array('aid' => $this->input->post('aid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));

                            }



                            $this->common->setmessage('Airport information published successfully.', 1);

                            $this->common->setmessage('Airport information <b>Approved</b> successfully.', 1);

                        }

                        if ($this->input->post('action') == 'publish') {

                            $this->common->UpdateRecord('airports', array('aid' => $this->input->post('aid')), array('published' => $now));

                            $this->common->getactivity('publish_airport','');



                            $effected = $this->db->affected_rows();

                            if ($effected > 0) {

                                $this->common->UpdateRecord('airports', array('aid' => $this->input->post('aid')), array('lastmodified' => date('Y-m-d'), 'modifiedby' => $this->common->GetSessionKey('email')));

                            }

                           
                         
                             $this->send_email1($username, $IATA, $aname, $email, $comments);
                              
                                 

                              

                            $this->common->setmessage('Airport information published successfully.', 1);

                        }

                    }

                }

            }



            if ($this->uri->segment(4) == 'publish' || $this->uri->segment(4) == 'approved') {

                $this->load->view("frontend/airport/publish", $this->outputData);

            } else {

                redirect(base_url('wizard/airport/' . $aid));

            }

        } else {



            $this->common->setmessage("You are not allowed to access that page.", -1);



            redirect(base_url());

        }

    }

    /**

     * 

     * END Publish Airport

     */



    /**

     * 

     * START Send Email to Admins for Published Airport/Company (Required id)

     */

    function send_email1($username, $IATA, $aname, $email, $comments) {



        //send out email 

        $this->load->library('email');

        $config['smtp_host'] = 'mail.arnfactbook.com';

        $config['smtp_user'] = 'support@arnfactbook.com';

        $config['smtp_pass'] = 'Urbanexpo1';

        $config['charset'] = 'iso-8859-1';

        $config['mailtype'] = 'html';

        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->from($email);



        if ($IATA != "") {

            $this->email->to('barb@airportrevenuenews.com');

            $admin = $this->common->GetSingleRowFromTableWhere('users', array('email' => 'barb@airportrevenuenews.com'));

            $adminname = $admin['first_name'] . " " . $admin['last_name'];

        } else if ($IATA == "") {

            $this->email->to('andrea@airportrevenuenews.com');

//            $admin = $this->common->GetSingleRowFromTableWhere('users', array('email' => 'andrea@airportrevenuenews.com'));

//            $adminname = $admin['first_name'] . " " . $admin['last_name'];

            $adminname = 'Andrea Caballero';

        }



        $this->email->cc('techleadz.test3@gmail.com');



        $this->email->subject($IATA . " " . $aname . ' is ready to be published');



        $link = 'publish';

        $where1 = array('email_temp_name' => $link);

//        $email_template = $this->commonmodel->get_single_row_from_table_where('tbl_email_temp', $where1);

        $email_template = $this->common->GetSingleRowFromTableWhere('tbl_email_temp', $where1);



        $search_array = array('#admin', '#aname', '#username', '#comments');



        $replace_array = array($adminname, $aname, $username, $comments);



        $message1 = $email_template['email_temp_text'];



        $message = str_replace($search_array, $replace_array, $message1);



        $data = array(

            'message' => $message

        );



        $messagee = $this->email->message($this->load->view('frontend/email_temp', $data, true));



         $this->email->send();
      

    }

    /**

     * 

     * END Send Email to Admins

     */



}



?>