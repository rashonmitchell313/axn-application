<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class frontend extends CI_Controller {

    public $outputData = "";

    /**
     * 
     * START Construct
     */
    function __construct() {
        parent::__construct();
        error_reporting(1);
//        error_reporting(E_ALL);
        $this->outputData['menu'] = 'frontend/include/menu';
        $this->outputData['footer'] = 'frontend/include/footer';
        $this->outputData['header'] = 'frontend/include/header';
        $this->outputData['rsStates'] = $this->common->GetAllRowOrderBy("states", "state_code", "asc");
        $this->outputData['row_rsCountries'] = $this->common->GetAllRowOrderBy("countries", "countries_name", "asc");
        $this->outputData['ListofCompanies'] = $this->common->GetAllRowOrderBy("companies", "companyname", "asc");
        $this->outputData['ListOfAirports'] = $this->common->GetAllRowOrderBy("airports", "aname", "asc");
        $this->lang->load('config', 'english');
    }

    /**
     * 
     * END Construct
     */
    public function airportdetails() {
        echo "airportdetails";
    }

    /**
     * 
     * START View Page on Not Found Error
     */
    public function notfound() {
        $this->load->view('frontend/404', $this->outputData);
    }

    /**
     * 
     * END Not Found Error
     */
    public function forget() {
        if (isset($_GET['info'])) {
            $data['info'] = $_GET['info'];

            $this->outputData = $data['error'];
        }
        if (isset($_GET['error'])) {
            $data['error'] = $_GET['error'];

            $this->outputData = $data['error'];
        }

        $this->load->view('frontend/login-forget', $this->outputData);
    }

    public function doforget() {
        $this->load->helper('url');
        $email = $_POST['email'];
        $q = $this->db->query("select * from users where email='" . $email . "'");

        if ($q->num_rows > 0) {
            $r = $q->result();
            $user = $r[0];
            $this->resetpassword($user);
            $info = "Password has been reset and has been sent to email id: " . $email;
            redirect('/frontend/forget?info=' . $info, 'refresh');
        }
        $error = "The email id you entered not found on our database ";
        redirect('/frontend/forget?error=' . $error, 'refresh');
    }

    private function resetpassword($user) {
        date_default_timezone_set('GMT');
        $this->load->helper('string');
        $password = random_string('alnum', 16);
        $this->db->where('id', $user->id);
        $this->db->update('users', array('password' => MD5($password)));
        $this->load->library('email');
        $this->email->from('cantreply@youdomain.com', 'Your name');
        $this->email->to($user->email);
        $this->email->subject('Password reset');
        $this->email->message('You have requested the new password, Here is you new password:' . $password);
        $this->email->send();
    }

    /**
     * 
     * START Glossary Page (Required Login)
     */
    public function glossary() {
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }
        $this->common->ThisSecureArea('user');
        $this->load->view('frontend/glossary', $this->outputData);
    }

    /**
     * 
     * END Glossary Page
     */

    /**
     * 
     * START Setting Page (Required Login)
     */
    public function setting() {
        $this->common->ThisSecureArea('user');
        $this->load->view('frontend/setting', $this->outputData);
    }

    /**
     * 
     * END Setting Page
     */

    /**
     * 
     * START Contact Us Page (Required Login)
     */
    public function contactus() {
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }
//        $this->outputData['contact_us_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising,advertising_mid WHERE advertising_mid.admid=advertising.mid AND (adlocation ='L1' OR adlocation ='L2' 
//	OR adlocation ='L3') AND advertising.mid=11");

        $this->common->ThisSecureArea('user');
        $this->load->view('frontend/contactus', $this->outputData);
    }

    /**
     * 
     * END Contact Us Page
     */
    /* public function contents()
      {
      $this->outputData['content']=$this->common->GetContents('login_content','text_home');
      $this->load->view('frontend/frontend',$this->outputData);
      } */

    /**
     * 
     * START Lease Expiration Page with Categories
     */
    public function salesexpense() {

        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }
//        $this->outputData['lease_expiration_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising,advertising_mid WHERE advertising_mid.admid=advertising.mid AND (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3') AND advertising.mid=3");
        $this->common->ThisSecureArea('user');

        $list = $this->common->GetAllRowOrderBy('lkpcategory', 'categoryid', 'ASC', 'category');

        $year = date('Y');
        $year = $year + 1;
        $futuredate = $year . "-" . date("m-d");
        $dateask = " AND outlets.exp BETWEEN CURDATE() and '" . $futuredate . "'";
        $i = 0;
        foreach ($list as $l):

            $where = "lkpcategory.category = '" . $l['category'] . "' " . $dateask;

            $join_array = array(
                array('lkpcategory', 'lkpcategory.categoryid=outlets.categoryid', 'left'),
                array('airports', 'outlets.aid = airports.aid', 'left')
            );

            $cur = $this->common->JoinTables("*", "outlets", $join_array, $where, "outlets.exp", 0, "");

            $this->outputData[str_replace(array(" ", "/"), array("", ""), $l['category'])] = $cur;

        endforeach;

        $this->load->view('frontend/salesexpense', $this->outputData);
    }

    /**
     * 
     * END Lease Expiration Page
     */

    /**
     * 
     * START View Home (Login) Page
     */
    public function index() {
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }

        /*
          $msie = strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE') ? true : false;
          $firefox = strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox') ? true : false;
          $safari = strpos($_SERVER["HTTP_USER_AGENT"], 'Safari') ? true : false;
          $chrome = strpos($_SERVER["HTTP_USER_AGENT"], 'Chrome') ? true : false;
          $mozila = strpos($_SERVER["HTTP_USER_AGENT"], 'Mozilla') ? true : false;


          if ($msie)
          {
          echo 'MSIE';
          exit;
          }
          else if ($firefox)
          {
          echo 'Firefox';
          exit;
          }
          else if ($chrome)
          {
          echo 'Chrome';
          exit;
          }
          else if ($safari)
          {
          echo 'Safari';
          exit;
          }

          else if ($mozila)
          {
          echo 'Mozilla';
          exit;
          }

          $this->load->library('user_agent');

          if ($this->agent->is_browser()) {
          $agent = $this->agent->browser().' '.$this->agent->version();
          //            $browser = $this->agent->browser();
          //            $version = $this->agent->version();
          //            if ($browser == '' && $version == '8')
          //            {
          //
          //            }
          }
          //        elseif ($this->agent->is_robot()) {
          //            $agent = $this->agent->robot();
          //        } elseif ($this->agent->is_mobile()) {
          //            $agent = $this->agent->mobile();
          //        } else {
          //            $agent = 'Unidentified User Agent';
          //        }

          //        echo $agent;


          //
          //
          //        if ($this->agent->is_browser('')) {
          //
          //        }
          //
          //        if(preg_match('/(?i)msie [5-11]/',$_SERVER['HTTP_USER_AGENT']))
          //{
          //    // if IE<=8
          //    echo 'yes';
          //    exit;
          //}

          //        echo $this->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)

          exit; */
//        $this->outputData['home_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising,advertising_mid WHERE advertising_mid.admid=advertising.mid AND (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3') AND advertising.mid=2");
        $this->outputData['content'] = $this->common->GetContents('login_content', 'text_home');
        $this->load->view('frontend/frontend', $this->outputData);
    }

    /**
     * 
     * END View Home Page
     */

    /**
     * 
     * START Request Access (to Airport or Company) Page (Required Login)
     */
    public function requestaccess() {
        $this->load->view('frontend/request-access', $this->outputData);
    }

    /**
     * 
     * END Request Access
     */

    /**
     * 
     * START User Manual (Required Login)
     */
    public function UserManual() {

        if ($this->session->userdata('user_id') != '2452') {

            redirect(base_url());
            exit;
        }
//        $this->common->ThisSecureArea('user');
        $this->common->ThisSecureArea('Admin');
        $this->load->view('frontend/manual', $this->outputData);
    }

    /**
     * 
     * END User Manual
     */

    /**
     * 
     * START Airport Wizard for admin and Normal User (Required Id)
     */
    public function airport_wizard() {
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            $this->common->ThisSecureArea('user');
            $accesslevel = $this->common->GetCurrentUserInfo('accesslevel');
            $id = $this->common->GetCurrentUserInfo('user_id');
            if ($accesslevel == 4) {
//                $this->outputData['modify_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising,advertising_mid WHERE 					   	advertising_mid.admid=advertising.mid AND (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3') AND 
//	advertising.mid=17");


                $this->load->view('frontend/airport/airport-wizard', $this->outputData);
            } else {
                $this->load->view('frontend/airport/airport-wizard-for-normal-user', $this->outputData);
            }
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Airport Wizard for admin and Normal User
     */

    /**
     * 
     * START Edit Airport for Modify data (Required Id)
     */
    public function airport_edit() {
        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            $this->common->ThisSecureArea("user");
            $aid = $this->uri->segment(3);
            $this->common->check_user_airport($aid);

//            $this->outputData['modify_airport_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR 					adlocation ='L3')  AND advertising.aid='$aid'");
            $contact = $this->common->CustomQueryALL("SELECT * FROM airportcontacts  
		LEFT JOIN lkpmgtresponsibility ON lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility 
		WHERE aid=" . $this->db->escape($aid) . " AND adeleted=0
		ORDER BY airportcontacts.priority ASC");

            $this->outputData['airport_contact'] = $contact;

            $grossSales = $this->common->CustomQueryALL("select * from terminals
		left join terminalsannual on terminalsannual.tid=terminals.tid 
		where aid=" . $this->db->escape($aid) . " ORDER BY terminals.terminalabbr ASC");
            $this->outputData['airport_grossSales'] = $grossSales;

            $mainData = $this->common->CustomQueryALL("SELECT * FROM airportsannual INNER JOIN airports ON 
			airportsannual.aid=airports.aid
			LEFT JOIN airporttotals ON airporttotals.aid = airportsannual.aid 
			WHERE airportsannual.aid=" . $this->db->escape($aid));
            $this->outputData['main_data'] = $mainData;

            $wideInfo_traffic = $this->common->CustomQueryALL("SELECT * FROM `airports` LEFT JOIN airportsannual 
			ON airports.aid=airportsannual.aid 
			INNER JOIN terminals ON airports.aid=terminals.aid INNER JOIN terminalsannual ON terminals.tid=terminalsannual.tid 
			INNER JOIN airporttotals ON airports.aid=airporttotals.aid 
			WHERE airports.aid=" . $this->db->escape($aid) . " ORDER BY airports.aid, terminals.terminalabbr ASC");
            $this->outputData['wideInfo_traffic'] = $wideInfo_traffic;
            // first need to fetch category name to get each category data
            $categories = $this->common->CustomQueryALL("select distinct category from lkpcategory");
            $this->outputData['get_category_name'] = $categories;
            // now fetch each category data
            foreach ($categories as $categoryName) {
                $category = $this->common->CustomQueryALL("select * from outlets left join lkpcategory 
			ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category = '" . $categoryName['category'] . "' 
			and outlets.aid = " . $this->db->escape($aid) . " AND outlets.odeleted = 0 ORDER BY outletname");

                $this->outputData['categories'][$categoryName['category']] = $category;
            }



            $this->load->view('frontend/airport/airport_edit', $this->outputData);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Edit Airport for Modify data
     */

    /**
     * 
     * START Airport's Contact Place (Required Id)
     */
    public function checkplace() {

        $aid = $this->input->post('aid');
        $contact = $this->common->CustomQueryALL("SELECT acid, priority FROM airportcontacts WHERE aid='$aid' ORDER BY airportcontacts.priority ASC");

//        $MAX = $contact[count($contact) - 1]['priority'];

        $assigned = 0;
        $priority = array();
        foreach ($contact as $acontacts) {
            $priority[] = $acontacts['priority'];
        }
        $priority = implode(", ", $priority);

        $test = explode(",", $priority);

        for ($p = 0; $p < count($test); $p++) {

            if ($this->input->post('priority') == $test[$p]) {

//                $this->common->setmessage("This Placemennt order already assigned. You can add Placemennt order grater than " . $MAX . ".", -1);
                $this->common->setmessage("This Placemennt order already assigned. You can add Placemennt order other than these;(" . $priority . ").", -1);

                echo -1;
            }
        }
    }

    /**
     * 
     * END Airport's Contact Place
     */

    /**
     * 
     * START Airport Edit Steps (Required Id)
     */
    public function airport_edit_steps() {

        $now = $this->common->GetCurrrentDate();

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            $step = $this->uri->segment(4);
            $cid = $this->uri->segment(5);
            $this->common->check_user_airport($cid);
//            $this->outputData['modify_airport_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR 					adlocation ='L3')  AND advertising.aid='$cid'");
            switch ($step) {

                case "1":

                    $join_array = array(
                        array('airports', 'airportsannual.aid=airports.aid', 'INNER'),
                        array('airporttotals', 'airporttotals.aid = airportsannual.aid', 'LEFT')
                    );

                    $mainData = $this->common->JoinTables('*', 'airportsannual', $join_array, array('airportsannual.aid' => $cid));

//                    $contact = $this->common->GetAllRowjoinOrderBy('airportcontacts', 'lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT', 'airportcontacts.priority', 'ASC', array('aid' => $cid, 'adeleted' => 0));

                    $contact = $this->common->CustomQueryALL("SELECT * FROM airportcontacts  
                        LEFT JOIN lkpmgtresponsibility ON lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility 
                        WHERE aid=" . $this->db->escape($cid) . " AND adeleted=0
                        ORDER BY airportcontacts.priority ASC");

                    $this->outputData['main_data'] = $mainData;
                    $this->outputData['airport_contact'] = $contact;

                    if ($this->input->post('addmorecontacthidden') != "") {
                        $newarray = array('aid' => $cid,
                            'mgtresponsibility' => $this->input->post('mgtresponsibility'),
                            'afname' => $this->input->post('afname'),
                            'alname' => $this->input->post('alname'),
                            'atitle' => $this->input->post('atitle'),
                            'acompany' => $this->input->post('acompany'),
                            'aaddress1' => $this->input->post('aaddress1'),
                            'aaddress2' => $this->input->post('aaddress2'),
                            'accity' => $this->input->post('accity'),
                            'acstate' => $this->input->post('acstate'),
                            'aczip' => $this->input->post('aczip'),
                            'accountry' => $this->input->post('accountry'),
                            'aphone' => $this->input->post('aphone1'),
                            'afax' => $this->input->post('afax1'),
                            'aemail' => $this->input->post('aemail'),
                            'awebsite' => $this->input->post('awebsite'),
                            'priority' => $this->input->post('priority')
                        );

                        $this->common->InsertInDb('airportcontacts', $newarray);
                        $IATA = $this->common->get_ariport_info($cid);
                        $this->common->getactivity('add_airport_contact', ' (' . $IATA . ')');
                        $this->common->setmessage("New Airport contact has been added successfully.", 1);

                        $effected = $this->db->affected_rows();
                        if ($effected > 0) {
                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }

                        $this->common->redirect(site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $cid);
                    }

                    $this->load->view('frontend/airport/airport-wizard-step-add-1', $this->outputData);
                    break;
                case "2":

                    $join_array1 = array(
                        array('airports', 'airportsannual.aid=airports.aid', 'INNER'),
                        array('airporttotals', 'airporttotals.aid = airportsannual.aid', 'LEFT')
                    );
                    $mainData = $this->common->JoinTables('*', 'airportsannual', $join_array1, array('airportsannual.aid' => $cid));

//                    $contact = $this->common->GetAllRowjoinOrderBy('airportcontacts', 'lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT', 'airportcontacts.priority', 'ASC', array('aid' => $cid, 'adeleted' => 0));
//                    $con_mgt_type = $this->common->GetContentsAll('lkpmgttype', 'lkpmgttype');

                    $con_mgt_type = $this->common->CustomQueryALL("select lkpmgttype from lkpmgttype");

                    $join_array2 = array(
                        array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'),
                        array('terminals', 'airports.aid=terminals.aid', 'INNER'),
                        array('terminalsannual', 'terminals.tid=terminalsannual.tid', 'INNER'),
                        array('airporttotals', 'airports.aid=airporttotals.aid', 'INNER')
                    );

                    $wideInfo_traffic = $this->common->JoinTables('*', 'airports', $join_array2, array('airports.aid' => $cid), 'airports.aid, terminals.terminalabbr');

                    $this->outputData['wideInfo_traffic'] = $wideInfo_traffic;
                    $this->outputData['main_data'] = $mainData;
//                    $this->outputData['airport_contact'] = $contact;
                    $this->outputData['con_mgt_type'] = $con_mgt_type;

                    if ($this->input->post('step2_airport') != "") {
                        $newarray = array(
                            'configuration' => $this->input->post('configuration'),
                            'mgtstructure' => $this->input->post('mgtstructure')
                        );
                        $where = array(
                            'aid' => $cid);
                        $this->common->UpdateRecord('airports', $where, $newarray);

                        $effected1 = $this->db->affected_rows();
                        if ($effected1 > 0) {
                            $IATA = $this->common->get_ariport_info($cid);

                            $this->common->getactivity('edit_airport_info', ' (' . $IATA . ')');
                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }

                        /*     if ($this->input->post('arr_texpansionplanned') != "") {
                          $texpansionplanned = implode('#', $this->input->post('arr_texpansionplanned'));
                          $addsqft = implode('#', $this->input->post('arr_addsqft'));
                          $completedexpdate = implode('#', $this->input->post('arr_completedexpdate'));
                          //                            for ($t = 0; $t < count($texpansionplanned); $t++) {
                          //                                if (!empty($texpansionplanned[$t])) {
                          //                                    $ter_id = $this->common->InsertInDb('airportsannual', array('aid' => $cid, 'texpansionplanned' => $texpansionplanned[$t], 'addsqft' => $addsqft[$t], 'completedexpdate' => $completedexpdate[$t]));
                          $ter_id = $this->common->UpdateRecord('airportsannual', array('aid' => $cid, 'texpansionplanned' => $texpansionplanned, 'addsqft' => $addsqft, 'completedexpdate' => $completedexpdate));
                          //                                }
                          //                            }
                          } */

                        $texpansionplanned = implode('#', $this->input->post('texpansionplanned'));

                        $addsqft = implode('#', $this->input->post('addsqft'));

                        $completedexpdate = implode('#', $this->input->post('completedexpdate'));

                        $ter_id = $this->common->UpdateRecord('airportsannual', array('aid' => $cid), array('texpansionplanned' => $texpansionplanned, 'addsqft' => $addsqft, 'completedexpdate' => $completedexpdate));

                        $newarray3 = array(
                            'presecurity' => $this->input->post('presecurity'),
                            'postsecurity' => $this->input->post('postsecurity'),
                            'ratiobusleisure' => $this->input->post('ratiobusleisure'),
                            'ondtransfer' => $this->input->post('ondtransfer'),
                            'avgdwelltime' => $this->input->post('avgdwelltime'),
                            'passservicesrev' => $this->input->post('passservicesrev'),
                            'passservicesrevtoair' => $this->input->post('passservicesrevtoair'),
                            'advertisingrev' => $this->input->post('advertisingrev'),
                            'advertisingrevtoair' => $this->input->post('advertisingrevtoair'),
                            'currencyexrev' => $this->input->post('currencyexrev'),
                            'currencyexrevtoair' => $this->input->post('currencyexrevtoair')
                        );
                        $where3 = array(
                            'aid' => $cid);
                        $this->common->UpdateRecord('airportsannual', $where3, $newarray3);

                        $effected = $this->db->affected_rows();
                        if ($effected > 0) {

                            $IATA = $this->common->get_ariport_info($cid);

                            $this->common->getactivity('edit_airport_revnue_ratio', ' (' . $IATA . ')');

                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }
                        $this->common->setmessage("Airport information has been updated successfully.", 1);
                        $this->common->redirect(site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $cid);
                    }

                    $this->load->view('frontend/airport/airport-wizard-step-add-2', $this->outputData);
                    break;
                case "3":

                    $join_array = array(
                        array('airports', 'airportsannual.aid=airports.aid', 'INNER'),
                        array('airporttotals', 'airporttotals.aid = airportsannual.aid', 'LEFT')
                    );
                    $mainData = $this->common->JoinTables('*', 'airportsannual', $join_array, array('airportsannual.aid' => $cid));

                    $join_array = array(
                        array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'),
                        array('terminals', 'airports.aid=terminals.aid', 'INNER'),
                        array('terminalsannual', 'terminals.tid=terminalsannual.tid', 'INNER'),
                        array('airporttotals', 'airports.aid=airporttotals.aid', 'INNER')
                    );

                    $wideInfo_traffic = $this->common->JoinTables('*', 'airports', $join_array, array('airports.aid' => $cid), 'airports.aid, terminals.terminalabbr');

                    $this->outputData['main_data'] = $mainData;

                    $this->outputData['wideInfo_traffic'] = $wideInfo_traffic;


                    if ($this->input->post('step4_step5_step6') != "") {

                        $newarray = array(
//                            'apasstraffic' => $this->input->post('apasstraffic'),
                            'apasstrafficcompare' => $this->input->post('apasstrafficcompare'),
                            /* 'adeplaning' => $this->input->post('adeplaning'),
                              'aenplaning' => $this->input->post('aenplaning'),
                              'aepdomestic' => $this->input->post('aepdomestic'),
                              'aepintl' => $this->input->post('aepintl'), */
                            'apasscomment' => $this->input->post('apasscomment'),
                            'grosscomment' => $this->input->post('grosscomment'),
                                /*  'afbcurrsqft' => $this->input->post('afbcurrsqft'),
                                  'asrcurrsqft' => $this->input->post('asrcurrsqft'),
                                  'angcurrsqft' => $this->input->post('angcurrsqft'),
                                  'adfcurrsqft' => $this->input->post('adfcurrsqft'),
                                  'afbgrosssales' => $this->input->post('afbgrosssales'),
                                  'asrgrosssales' => $this->input->post('asrgrosssales'),
                                  'anggrosssales' => $this->input->post('anggrosssales'),
                                  'adfgrosssales' => $this->input->post('adfgrosssales'),
                                  'afbsalesep' => $this->input->post('afbsalesep'),
                                  'asrsalesep' => $this->input->post('asrsalesep'),
                                  'angsalesep' => $this->input->post('angsalesep'),
                                  'adfsalesep' => $this->input->post('adfsalesep'),
                                  'afbrentrev' => $this->input->post('afbrentrev'),
                                  'asrrentrev' => $this->input->post('asrrentrev'),
                                  'angrentrev' => $this->input->post('angrentrev'),
                                  'adfrentrev' => $this->input->post('adfrentrev'),
                                  'afbrentep' => $this->input->post('afbrentep'),
                                  'asrrentep' => $this->input->post('asrrentep'),
                                  'angrentep' => $this->input->post('angrentep'),
                                  'adfrentep' => $this->input->post('adfrentep'),
                                  'acurrsqft' => $this->input->post('acurrsqft'),
                                  'acurrsqft_incl_df' => $this->input->post('acurrsqft_incl_df'),
                                  'aconcessiongrosssales' => $this->input->post('aconcessiongrosssales'),
                                  'aconcessiongrosssales_incl_df' => $this->input->post('aconcessiongrosssales_incl_df'),
                                  'asalesep' => $this->input->post('asalesep'),
                                  'asalesep_incl_df' => $this->input->post('asalesep_incl_df'),
                                  'arentrev' => $this->input->post('arentrev'),
                                  'arentrev_incl_df' => $this->input->post('arentrev_incl_df'),
                                  'arentep' => $this->input->post('arentep'),
                                  'arentep_incl_df' => $this->input->post('arentep_incl_df') */
                        );

                        $where = array(
                            'aid' => $cid);

                        $this->common->UpdateRecord('airporttotals', $where, $newarray);
                        $effected = $this->db->affected_rows();
                        if ($effected > 0) {

                            $IATA = $this->common->get_ariport_info($cid);

                            $this->common->getactivity('edit_airport_pt_gs_cmt', ' (' . $IATA . ')');

                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('IATA' => $this->input->post('IATA'), 'lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }
                        $this->common->setmessage("Airport total has been updated successfully.", 1);
                        $this->common->redirect(site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $cid);
                    }


                    $this->load->view('frontend/airport/airport-wizard-step-add-3', $this->outputData);
                    break;
                case "4":

                    $this->outputData['main_data'] = $this->common->JoinTable('*', 'airportsannual', 'airports', 'airports.aid = airportsannual.aid', 'LEFT', array('airports.aid' => $cid));

                    if ($this->input->post('step4_airport') != "") {
                        $airportsannual = array('carrentalagenciesonsite', 'carrentalrevonsite', 'carrentalrevtoaironsite', 'carrentalrevoffsite', 'carrentalrevtoairoffsite', 'carrentalagenciesoffsite', 'totalcarsrented', 'carrentalsqft', 'parkingspaces', 'parkingrev', 'parkingrevtoair', 'hourlyshort', 'hourlylong', 'hourlyeconomy', 'hourlyvalet', 'dailyshort', 'dailylong', 'dailyeconomy', 'dailyvalet', 'spacesshort', 'spaceslong', 'spaceseconomy', 'spacesvalet', 'awrevcomment');

                        $data_array = array();
                        foreach ($airportsannual as $x) {
                            $data_array[] = $this->input->post($x);
                        }
                        $data_array = array_combine($airportsannual, $data_array);
                        $this->common->UpdateRecord('airportsannual', array('aid' => $cid), $data_array);
                        $effected = $this->db->affected_rows();
                        if ($effected > 0) {
                            $IATA = $this->common->get_ariport_info($cid);
                            $this->common->getactivity('edit_airport_rental_parking', ' (' . $IATA . ')');
                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }
                        $this->common->setmessage("Airport information has been updated successfully.", 1);
                        $this->common->redirect(site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $cid);
                    }
                    $this->load->view('frontend/airport/airport-wizard-step-4', $this->outputData);
                    break;
                case "5":

                    $join_array = array(
                        array('airports', 'airportsannual.aid=airports.aid', 'INNER'),
                        array('airporttotals', 'airporttotals.aid = airportsannual.aid', 'LEFT')
                    );
                    $mainData = $this->common->JoinTables('*', 'airportsannual', $join_array, array('airportsannual.aid' => $cid));


                    $this->outputData['main_data'] = $mainData;
//                    $categories = $this->common->CustomQueryALL("select distinct category from lkpcategory");

                    $categories = $this->common->Selectdistinct('lkpcategory', 'category');



                    $this->outputData['get_category_name'] = $categories;
                    // now fetch each category data 
                    foreach ($categories as $categoryName) {
                        $category = $this->common->CustomQueryALL("select * from outlets left join lkpcategory 
                            ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category = " . $this->db->escape($categoryName[category]) . " 
                            and outlets.aid = " . $this->db->escape($cid) . " AND outlets.odeleted=0  ORDER BY outletname");

//  $category = $this->common->GetAllRowjoinOrderBy('outlets', 'lkpcategory', 'outlets.categoryid = lkpcategory.categoryid', 'LEFT', 'outletname', 'ASC', array('lkpcategory.category' => $categoryName['category'], 'outlets.aid' => $cid, 'outlets.odeleted' => 0));

                        $this->outputData['categories'][$categoryName['category']] = $category;
                    }

                    $con_mgt_type = $this->common->GetAllRowWithSomeColumn('lkpcategory', '*', 'category,productdescription', 'ASC');

                    $this->outputData['con_mgt_type'] = $con_mgt_type;

                    $terminals = $this->common->GetAllRowWithColumn('terminals', '*', array('aid' => $cid), 'terminalabbr', 'ASC');


                    $this->outputData['terminals'] = $terminals;

                    $outlet = $this->common->GetAllRowjoinOrderBy('outlets', 'lkpcategory', 'outlets.categoryid = lkpcategory.categoryid', 'LEFT', 'outletname', 'ASC', array('outlets.aid' => $cid, 'outlets.odeleted' => 0));

                    $this->outputData['outlets'] = $outlet;

                    if ($this->input->post('step5add') != "") {


                        if ($this->input->post('exp_reason') != '') {
//                $now_dt = date("Y/m/d H:i:s");
                            $now_dt = $this->input->post('exp_date');
                        } else {
                            $now_dt = '';
                        }

                        $newarray = array('aid' => $cid,
                            'outletname' => $this->input->post('outletname'),
                            'companyname' => $this->input->post('companyname'),
                            'categoryid' => $this->input->post('categoryid'),
                            'numlocations' => $this->input->post('numlocations'),
                            'termlocation' => $this->input->post('termlocation'),
                            'sqft' => $this->input->post('sqft'),
                            'exp' => $this->input->post('exp'),
                            'exp_reason' => $this->input->post('exp_reason'),
                            'exp_date' => $now_dt
                        );

                        $this->common->InsertInDb('outlets', $newarray);

                        $IATA = $this->common->get_ariport_info($cid);

                        $this->common->getactivity('add_airport_tenants', ' (' . $IATA . ')');

                        $effected = $this->db->affected_rows();
                        if ($effected > 0) {
                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }

                        $this->common->setmessage("New Tenants has been added successfully.", 1);
                        $this->common->redirect(site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $cid);
                    }
                    $this->load->view('frontend/airport/airport-wizard-step-add-5', $this->outputData);
                    break;
                case "6":

                    $join_array = array(
                        array('airports', 'airportsannual.aid=airports.aid', 'INNER'),
                        array('airporttotals', 'airporttotals.aid = airportsannual.aid', 'LEFT')
                    );
                    $mainData = $this->common->JoinTables('*', 'airportsannual', $join_array, array('airportsannual.aid' => $cid));

                    $this->outputData['main_data'] = $mainData;

                    $cols = "*";
                    $from = "airports";
                    $join_array = array(
                        array('arn_lat_long', 'arn_lat_long.IATA= airports.IATA`', 'LEFT')
                    );
                    $where = "airports.aid =" . $cid . "";
                    $OrderBy = "airports.aname";
                    $this->outputData['ainfo'] = $this->common->JoinTables($cols, $from, $join_array, $where, $OrderBy);

                    $this->load->view('frontend/airport/airport-wizard-step-6', $this->outputData);
                    break;
            }
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Airport Edit Steps
     */

    /**
     * 
     * START Airport Step 1 Add Contact (Required Id)
     */
    public function airport_add_steps_1() {


        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            $cid = $this->uri->segment(3);
            $this->common->check_user_airport($cid);
//            $this->outputData['modify_airport_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR 					adlocation ='L3')  AND advertising.aid='$cid'");
            /*
              $mainData = $this->common->CustomQueryALL("SELECT * FROM airportsannual INNER JOIN airports ON
              airportsannual.aid=airports.aid
              LEFT JOIN airporttotals ON airporttotals.aid = airportsannual.aid
              WHERE airportsannual.aid='$cid'");

              $join_array = array(
              array('airports', 'airportsannual.aid=airports.aid', 'INNER'),
              array('airporttotals', 'airporttotals.aid = airportsannual.aid', 'LEFT')
              );
              $mainData1 = $this->common->JoinTables('*', 'airportsannual', $join_array, array('airportsannual.aid' => $cid));


              $contact = $this->common->CustomQueryALL("SELECT * FROM airportcontacts
              LEFT JOIN lkpmgtresponsibility ON lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility
              WHERE aid='$cid' AND adeleted=0
              ORDER BY lkpmgtresponsibility.mgtresponsibilityname");
              $this->outputData['main_data'] = $mainData;
              $this->outputData['airport_contact'] = $contact; */
            $this->outputData['airport_id'] = $cid;

            $this->load->view('frontend/airport/airport-wizard-step-1', $this->outputData);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Airport Step 1 Add Contact
     */

    /**
     * 
     * START Airport Step 1 Edit Contact (Required Id)
     */
    public function airport_edit_step_1() {

        $now = $this->common->GetCurrrentDate();

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            $aid = $this->uri->segment(5);

            $this->common->check_user_airport($aid);

            if ($this->input->post('MM_update') != "") {

                $acid = $this->input->post('airport_contact_id');
                $newarray = array(
                    'mgtresponsibility' => $this->input->post('mgtresponsibility'),
                    'afname' => $this->input->post('afname'),
                    'alname' => $this->input->post('alname'),
                    'atitle' => $this->input->post('atitle'),
                    'acompany' => $this->input->post('acompany'),
                    'aaddress1' => $this->input->post('aaddress1'),
                    'aaddress2' => $this->input->post('aaddress2'),
                    'accity' => $this->input->post('accity'),
                    'acstate' => $this->input->post('acstate'),
                    'aczip' => $this->input->post('aczip'),
                    'accountry' => $this->input->post('accountry'),
                    'aphone' => $this->input->post('aphone1'),
                    'afax' => $this->input->post('afax1'),
                    'aemail' => $this->input->post('aemail'),
                    'awebsite' => $this->input->post('awebsite'),
                    'priority' => $this->input->post('priority')
                );
                $where = array(
                    'acid' => $acid);

                $aid = $this->input->post('aid');

                $contact = $this->common->GetAllRowWithColumn('airportcontacts', 'acid, priority', array('aid' => $aid), 'airportcontacts.priority', 'ASC');

                $priority = array();
                foreach ($contact as $acontacts) {
                    $priority[] = $acontacts['priority'];
                }
                $priority = implode(", ", $priority);

                $test = explode(",", $priority);

                echo $contact[0]['priority'];

                for ($p = 0; $p < count($test); $p++) {

                    if ($this->input->post('priority') == $test[$p]) {
                        $oldpriority = $this->input->post('priority_old');

                        $this->common->UpdateRecord('airportcontacts', array('priority' => $this->input->post('priority'), 'aid' => $aid), array('priority' => $oldpriority));
                    }
                }

                $this->common->UpdateRecord('airportcontacts', $where, $newarray);
                $effected = $this->db->affected_rows();
                if ($effected > 0) {
                    $IATA = $this->common->get_ariport_info($aid);

                    $this->common->getactivity('edit_airport_contact', ' (' . $IATA . ')');

                    $this->common->UpdateRecord('airports', array('aid' => $aid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                }
//		$this->common->setmessage("New Company contact has been updated successfully.",1);	
                $this->common->setmessage("Contact has been updated successfully.", 1);
//		$this->common->redirect(site_url('wizard/airport/airport_edit_step_1').'/'.$aid.'/'.$acid);
//                echo site_url('wizard/airport/airport_edit_steps/1/').'/'.$aid; exit;
                $this->common->redirect(site_url('wizard/airport/airport_edit_steps/1/') . '/' . $aid);
            } else {
                
            }

//	$this->load->view('frontend/airport/airport-wizard-step-1',$this->outputData);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Airport Step 1 Edit Contact
     */

    /**
     * 
     * START Airport Step 3 Edit Terminals (Required Id)
     */
    public function airport_edit_step_3() {

        $now = $this->common->GetCurrrentDate();

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            $cid = $this->uri->segment(4);
            $this->common->check_user_airport($cid);
            $tid = $this->uri->segment(5);
//            $this->outputData['modify_airport_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR 					adlocation ='L3')  AND advertising.aid='$cid'");

            $join_array = array(
                array('airports', 'airportsannual.aid=airports.aid', 'INNER'),
                array('airporttotals', 'airporttotals.aid = airportsannual.aid', 'LEFT')
            );
            $mainData = $this->common->JoinTables('*', 'airportsannual', $join_array, array('airportsannual.aid' => $cid));

            $join_array = array(
                array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'),
                array('terminals', 'airports.aid=terminals.aid', 'INNER'),
                array('terminalsannual', 'terminals.tid=terminalsannual.tid', 'INNER'),
                array('airporttotals', 'airports.aid=airporttotals.aid', 'INNER')
            );

            $wideInfo_traffic = $this->common->JoinTables('*', 'airports', $join_array, array('airports.aid' => $cid), 'airports.aid, terminals.terminalabbr');

            $join_array = array(
                array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'),
                array('terminals', 'airports.aid=terminals.aid', 'INNER'),
                array('terminalsannual', 'terminals.tid=terminalsannual.tid', 'INNER'),
                array('airporttotals', 'airports.aid=airporttotals.aid', 'INNER')
            );

            $ste1_step2_step3 = $this->common->JoinTables('*', 'airports', $join_array, array('airports.aid' => $cid, 'terminals.tid' => $tid), 'terminalabbr');

            $this->outputData['main_data'] = $mainData;
            $this->outputData['wideInfo_traffic'] = $wideInfo_traffic;
            $this->outputData['ste1_step2_step3'] = $ste1_step2_step3;

            if ($this->input->post('step1_step2_step3') != "") {

                $tdeplaning = $this->common->parseNumber($this->input->post('tdeplaning'));

                $tepintl = $this->common->parseNumber($this->input->post('tepintl'));
                $tepdomestic = $this->common->parseNumber($this->input->post('tepdomestic'));
                $fbgrosssales = $this->common->parseNumber($this->input->post('fbgrosssales'));
                $srgrosssales = $this->common->parseNumber($this->input->post('srgrosssales'));
                $nggrosssales = $this->common->parseNumber($this->input->post('nggrosssales'));
                $dfgrosssales = $this->common->parseNumber($this->input->post('dfgrosssales'));

                $tenplaning = $tepintl + $tepdomestic;

                if ($tenplaning > 0) {

                    $sep_fb = $fbgrosssales / $tenplaning;

                    $fbsalesep = $sep_fb;

                    $sep_sr = $srgrosssales / $tenplaning;

                    $srsalesep = $sep_sr;

                    $sep_ng = $nggrosssales / $tenplaning;

                    $ngsalesep = $sep_ng;
                } else {
                    $fbsalesep = "-";
                    $srsalesep = "-";
                    $ngsalesep = "-";
                }
                if ($tepintl > 0) {
                    $sep_df = $dfgrosssales / $tepintl;

                    $dfsalesep = $sep_df;
                } else {
                    $dfsalesep = "-";
                }

                $newarray = array(
                    'tpasstraffic' => $tenplaning + $tdeplaning,
                    'tenplaning' => $tenplaning,
                    'tpasstrafficcompare' => $this->common->parseNumber($this->input->post('tpasstrafficcompare')),
                    'tdeplaning' => $this->common->parseNumber($this->input->post('tdeplaning')),
//                    'tenplaning' => $this->input->post('tenplaning'),
                    'tepdomestic' => $this->common->parseNumber($this->input->post('tepdomestic')),
                    'tepintl' => $this->common->parseNumber($this->input->post('tepintl')),
                    'fbcurrsqft' => $this->common->parseNumber($this->input->post('fbcurrsqft')),
                    'srcurrsqft' => $this->common->parseNumber($this->input->post('srcurrsqft')),
                    'ngcurrsqft' => $this->common->parseNumber($this->input->post('ngcurrsqft')),
                    'dfcurrsqft' => $this->common->parseNumber($this->input->post('dfcurrsqft')),
                    'fbgrosssales' => $this->common->parseNumber($this->input->post('fbgrosssales')),
                    'srgrosssales' => $this->common->parseNumber($this->input->post('srgrosssales')),
                    'nggrosssales' => $this->common->parseNumber($this->input->post('nggrosssales')),
                    'dfgrosssales' => $this->common->parseNumber($this->input->post('dfgrosssales')),
                    'fbsalesep' => $fbsalesep,
                    'srsalesep' => $srsalesep,
                    'ngsalesep' => $ngsalesep,
//                    'dfsalesep' => $this->input->post('dfsalesep'),
                    'dfsalesep' => $dfsalesep,
                    'fbrentrev' => $this->common->parseNumber($this->input->post('fbrentrev')),
                    'srrentrev' => $this->common->parseNumber($this->input->post('srrentrev')),
                    'ngrentrev' => $this->common->parseNumber($this->input->post('ngrentrev')),
                    'dfrentrev' => $this->common->parseNumber($this->input->post('dfrentrev')),
                    'tdominantair' => $this->input->post('tdominantair')
                );
                $where = array(
                    'tid' => $tid);

                $this->common->UpdateRecord('terminalsannual', $where, $newarray);

                $effected1 = $this->db->affected_rows();
                if ($effected1 > 0) {

                    $IATA = $this->common->get_ariport_info($cid);

                    $this->common->getactivity('edit_airport_terminal_info', ' (' . $IATA . ')');
                }

                $newarray1 = array(
                    'terminalabbr' => $this->input->post('terminalabbr')
                );
                $this->common->UpdateRecord('terminals', $where, $newarray1);
                $effected = $this->db->affected_rows();
                if ($effected > 0) {
                    $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                }

                $this->common->setmessage("Terminals info has been updated successfully.", 1);
                $this->common->redirect(site_url('wizard/airport/airport_edit_step_3') . '/' . $cid . '/' . $tid);
            }

            if ($this->input->post('step4_step5_step6') != "") {

                $total = "";
                $per = "";
                $tdeplng = "";
                $tenplng = "";
                $tepdoemstic = "";
                $tepint = "";
                $sr_grssale_tot_value = "";
                $fb_grssale_tot_value = "";
                $ng_grssale_tot_value = "";
                $df_grssale_tot_value = "";
                $fb_grsrent_tot_value = "";
                $sr_grsrent_tot_value = "";
                $ng_grsrent_tot_value = "";
                $df_grsrent_tot_value = "";
                $dutyfree_grosssale = "";
                $dutyfree_epintl = "";
                $fb_cursqft_tot_value = "";
                $sr_cursqft_tot_value = "";
                $ng_cursqft_tot_value = "";
                $df_cursqft_tot_value = "";

                $gt_apt_categy = $this->common->get_all_row_from_table('airports', array('aid' => $cid), "category");

                $airport_category = $gt_apt_categy[0]['category'];

                $terminal = $this->common->get_all_row_from_table('terminals', array('aid' => $cid), "*");

                foreach ($terminal as $terminalid) {
                    $terminalidd = $terminalid['tid'];

                    $select_val = $this->common->get_all_row_from_table('terminalsannual', array('tid' => $terminalidd), "*");

                    foreach ($select_val as $terminalval) {

                        if ($terminalval['tepintl'] != '0') {
                            $dutyfree_grosssale += $terminalval['dfgrosssales'];
                            $dutyfree_epintl += $terminalval['tepintl'];
                        }

                        $total += $terminalval['tpasstraffic'];
                        //$per = $per + str_replace('%','',$terminalval['tpasstrafficcompare']);
                        $tdeplng += $terminalval['tdeplaning'];
                        $tenplng += $terminalval['tenplaning'];
                        $tepdoemstic += $terminalval['tepdomestic'];
                        $tepint += $terminalval['tepintl'];
                        $fb_cursqft_tot_value += $terminalval['fbcurrsqft'];
                        $sr_cursqft_tot_value += $terminalval['srcurrsqft'];
                        $ng_cursqft_tot_value += $terminalval['ngcurrsqft'];
                        $df_cursqft_tot_value += $terminalval['dfcurrsqft'];
                        $fb_grssale_tot_value += str_replace(",", "", $terminalval['fbgrosssales']);
                        $sr_grssale_tot_value += $terminalval['srgrosssales'];
                        $ng_grssale_tot_value += $terminalval['nggrosssales'];
                        $df_grssale_tot_value += $terminalval['dfgrosssales'];
                        $fb_grsrent_tot_value += $terminalval['fbrentrev'];
                        $sr_grsrent_tot_value += $terminalval['srrentrev'];
                        $ng_grsrent_tot_value += $terminalval['ngrentrev'];
                        $df_grsrent_tot_value += $terminalval['dfrentrev'];
                    }
                }

                $apasstraffic = $total;

                $adeplaning = $tdeplng;

                $aenplaning = $tenplng;

                $aepdomestic = $tepdoemstic;

                $aepintl = $tepint;

                if (strtolower($airport_category) != 'c') {

                    $afbcurrsqft = $fb_cursqft_tot_value;

                    $asrcurrsqft = $sr_cursqft_tot_value;

                    $angcurrsqft = $ng_cursqft_tot_value;

                    $adfcurrsqft = $df_cursqft_tot_value;

                    $afbgrosssales = $fb_grssale_tot_value;

                    $asrgrosssales = $sr_grssale_tot_value;

                    $anggrosssales = $ng_grssale_tot_value;

                    $adfgrosssales = $df_grssale_tot_value;

                    $afbrentrev = $fb_grsrent_tot_value;

                    $asrrentrev = $sr_grsrent_tot_value;

                    $angrentrev = $ng_grsrent_tot_value;

                    $adfrentrev = $df_grsrent_tot_value;

                    if ($tenplng > 0) {

                        $angsalesep = number_format($ng_grssale_tot_value / $tenplng, 2);

                        $asrsalesep = number_format($sr_grssale_tot_value / $tenplng, 2);

                        $afbsalesep = number_format($fb_grssale_tot_value / $tenplng, 2);

                        $adfsalesep = number_format($dutyfree_grosssale / $dutyfree_epintl, 2);

                        $afbrentep = number_format($fb_grsrent_tot_value / $tenplng, 2);

                        $asrrentep = number_format($sr_grsrent_tot_value / $tenplng, 2);

                        $angrentep = number_format($ng_grsrent_tot_value / $tenplng, 2);

                        $adfrentep = number_format($df_grsrent_tot_value / $tepint, 2);
                    } else {
                        $angsalesep = "-";
                        $asrsalesep = "-";
                        $afbsalesep = "-";
                        $adfsalesep = "-";
                        $afbrentep = "-";
                        $asrrentep = "-";
                        $angrentep = "-";
                        $adfrentep = "-";
                    }

                    /*                     * *****Coded to calculate airport totals****** */
                    $atcursqft = str_replace(',', '', $afbcurrsqft) + str_replace(',', '', $asrcurrsqft) + str_replace(',', '', $angcurrsqft);

                    $acurrsqft = $atcursqft;

                    $atgrosssales = str_replace(',', '', $afbgrosssales) + str_replace(',', '', $asrgrosssales) + str_replace(',', '', $anggrosssales);

                    $aconcessiongrosssales = $atgrosssales;

                    $atsalesepp = str_replace(',', '', $afbsalesep) + str_replace(',', '', $asrsalesep) + str_replace(',', '', $angsalesep);

                    $asalesep = $atsalesepp;

                    $atgrossrentals = str_replace(',', '', $afbrentrev) + str_replace(',', '', $asrrentrev) + str_replace(',', '', $angrentrev);

                    $arentrev = $atgrossrentals;

                    $atrentep = str_replace(',', '', $afbrentep) + str_replace(',', '', $asrrentep) + str_replace(',', '', $angrentep);

                    $arentep = $atrentep;

                    $acurrsqft_incl_df1 = str_replace(',', '', $afbcurrsqft) + str_replace(',', '', $asrcurrsqft) + str_replace(',', '', $angcurrsqft) + str_replace(',', '', $adfcurrsqft);

                    $acurrsqft_incl_df = $acurrsqft_incl_df1;

                    $aconcessiongrosssales_incl_df1 = str_replace(',', '', $afbgrosssales) + str_replace(',', '', $asrgrosssales) + str_replace(',', '', $anggrosssales) + str_replace(',', '', $adfgrosssales);

                    $aconcessiongrosssales_incl_df = $aconcessiongrosssales_incl_df1;

                    $asalesep_incl_df1 = str_replace(',', '', $afbsalesep) + str_replace(',', '', $asrsalesep) + str_replace(',', '', $angsalesep) + str_replace(',', '', $adfsalesep);

                    $asalesep_incl_df = number_format($asalesep_incl_df1, 2);

                    $arentrev_incl_df1 = str_replace(',', '', $afbrentrev) + str_replace(',', '', $asrrentrev) + str_replace(',', '', $angrentrev) + str_replace(',', '', $adfrentrev);

                    $arentrev_incl_df = $arentrev_incl_df1;

                    $arentep_incl_df1 = str_replace(',', '', $afbrentep) + str_replace(',', '', $asrrentep) + str_replace(',', '', $angrentep) + str_replace(',', '', $adfrentep);

                    $arentep_incl_df = $arentep_incl_df1;

                    $asalesep1 = number_format($aconcessiongrosssales / $aenplaning, 2);
                }

                $newarray = array(
                    'apasstraffic' => $apasstraffic,
//                    'apasstrafficcompare' => $apasstrafficcompare,
                    'apasstrafficcompare' => $this->input->post('apasstrafficcompare'),
                    'adeplaning' => $adeplaning,
                    'aenplaning' => $aenplaning,
                    'aepdomestic' => $aepdomestic,
                    'aepintl' => $aepintl,
                    'apasscomment' => $this->input->post('apasscomment'),
                    'grosscomment' => $this->input->post('grosscomment'),
                    'afbcurrsqft' => $afbcurrsqft,
                    'asrcurrsqft' => $asrcurrsqft,
                    'angcurrsqft' => $angcurrsqft,
                    'adfcurrsqft' => $adfcurrsqft,
                    'afbgrosssales' => $afbgrosssales,
                    'asrgrosssales' => $asrgrosssales,
                    'anggrosssales' => $anggrosssales,
                    'adfgrosssales' => $adfgrosssales,
                    'afbsalesep' => $afbsalesep,
                    'asrsalesep' => $asrsalesep,
                    'angsalesep' => $angsalesep,
                    'adfsalesep' => $adfsalesep,
                    'afbrentrev' => $afbrentrev,
                    'asrrentrev' => $asrrentrev,
                    'angrentrev' => $angrentrev,
                    'adfrentrev' => $adfrentrev,
                    'afbrentep' => $afbrentep,
                    'asrrentep' => $asrrentep,
                    'angrentep' => $angrentep,
                    'adfrentep' => $adfrentep,
                    'acurrsqft' => $acurrsqft,
                    'acurrsqft_incl_df' => $acurrsqft_incl_df,
                    'aconcessiongrosssales' => $aconcessiongrosssales,
                    'aconcessiongrosssales_incl_df' => $aconcessiongrosssales_incl_df,
//                    'asalesep' => $asalesep,
                    'asalesep' => $asalesep1,
                    'asalesep_incl_df' => $asalesep_incl_df,
                    'arentrev' => $arentrev,
                    'arentrev_incl_df' => $arentrev_incl_df,
                    'arentep' => $arentep,
                    'arentep_incl_df' => $arentep_incl_df
                );

                $where = array(
                    'aid' => $cid);

                $this->common->UpdateRecord('airporttotals', $where, $newarray);

                $effected = $this->db->affected_rows();
                if ($effected > 0) {

                    $IATA = $this->common->get_ariport_info($cid);

                    $this->common->getactivity('edit_airport_total_info', ' (' . $IATA . ')');

                    $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                }
                $this->common->setmessage("Airport total has been updated successfully.", 1);
                $this->common->redirect(site_url('wizard/airport/airport_edit_step_3') . '/' . $cid . '/' . $tid);
            }


            $this->load->view('frontend/airport/airport-wizard-step-3', $this->outputData);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Airport Step 3 Edit Terminals
     */

    /**
     * 
     * START Airport Step 5 Edit Tenants (Required Id)
     */
    public function airport_edit_step_5_() {

        $now = $this->common->GetCurrrentDate();

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            if ($this->input->post('step5edit') != "") {
                $cid = $this->uri->segment(4);
                $aid = $this->uri->segment(5);
                $oid = $this->input->post('tenante_id');

                if ($this->input->post('exp_reason') != '') {

                    $now_dt = $this->input->post('exp_datee');
                } else {
                    $now_dt = '';
                }

                $newarray = array(
                    'outletname' => $this->input->post('outletnamee'),
                    'companyname' => $this->input->post('companyname'),
                    'categoryid' => $this->input->post('categoryide'),
                    'numlocations' => $this->input->post('numlocations'),
                    'termlocation' => $this->input->post('termlocation'),
                    'sqft' => $this->input->post('sqft'),
                    'exp' => $this->input->post('exp'),
                    'exp_reason' => $this->input->post('exp_reason'),
                    'exp_date' => $now_dt
                );

                $where = array(
                    'oid' => $oid);
                $this->common->UpdateRecord('outlets', $where, $newarray);

                $IATA = $this->common->get_ariport_info($aid);

                $this->common->getactivity('edit_airport_tenants', ' (' . $IATA . ')');

                $effected = $this->db->affected_rows();
                if ($effected > 0) {
                    $this->common->UpdateRecord('airports', array('aid' => $aid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                }

                $this->common->setmessage("Tanants has been updated successfully.", 1);
            }
            $this->common->redirect(site_url('wizard/airport/airport_edit_steps') . '/' . $cid . '/' . $aid);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Airport Step 5 Edit Tenants
     */
    /*
      public function airport_edit_step_5() {

      $now = $this->common->GetCurrrentDate();

      $str = $this->session->userdata('user_access');

      $lstr = strtolower(str_replace(" ", "", $str));

      $a = explode(",", $lstr);

      $aa = array_values($a);

      if (in_array("modifydata", $aa)) {

      $cid = $this->uri->segment(4);
      $this->common->check_user_airport($cid);
      $oid = $this->uri->segment(5);
      $this->outputData['modify_airport_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR 					adlocation ='L3')  AND advertising.aid='$cid'");
      $mainData = $this->common->CustomQueryALL("SELECT * FROM airportsannual INNER JOIN airports ON
      airportsannual.aid=airports.aid
      LEFT JOIN airporttotals ON airporttotals.aid = airportsannual.aid
      WHERE airportsannual.aid='$cid'");
      $this->outputData['main_data'] = $mainData;
      $categories = $this->common->CustomQueryALL("select distinct category from lkpcategory");
      $this->outputData['get_category_name'] = $categories;
      // now fetch each category data
      foreach ($categories as $categoryName) {
      $category = $this->common->CustomQueryALL("select * from outlets left join lkpcategory
      ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category = '$categoryName[category]'
      and outlets.aid = '$cid' AND outlets.odeleted=0  ORDER BY outletname");
      $this->outputData['categories'][$categoryName['category']] = $category;
      }

      $con_mgt_type = $this->common->CustomQueryALL("SELECT * FROM lkpcategory ORDER BY category,productdescription");
      $this->outputData['con_mgt_type'] = $con_mgt_type;
      $terminals = $this->common->CustomQueryALL("SELECT * FROM terminals WHERE aid='$cid' ORDER BY terminalabbr");
      $this->outputData['terminals'] = $terminals;
      $grossSales = $this->common->CustomQueryALL("select * from terminals
      left join terminalsannual on terminalsannual.tid=terminals.tid
      where aid='$cid' ORDER BY terminals.terminalabbr ASC");
      $this->outputData['airport_grossSales'] = $grossSales;
      //echo $this->db->last_query(); exit;
      $edittanants = $this->common->CustomQueryALL("SELECT * FROM outlets

      LEFT JOIN airports ON airports.aid = outlets.aid

      WHERE airports.aid='$cid' and outlets.oid = '$oid]'");

      $this->outputData['edittanants'] = $edittanants;

      if ($this->input->post('step5edit') != "") {
      $newarray = array(
      'outletname' => $this->input->post('outletname'),
      'companyname' => $this->input->post('companyname'),
      'categoryid' => $this->input->post('categoryid'),
      'numlocations' => $this->input->post('numlocations'),
      'termlocation' => $this->input->post('termlocation'),
      'sqft' => $this->input->post('sqft'),
      'exp' => $this->input->post('exp')
      );

      $where = array(
      'oid' => $oid);
      $this->common->UpdateRecord('outlets', $where, $newarray);
      $effected = $this->db->affected_rows();
      if ($effected > 0) {
      $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
      }
      $this->common->setmessage("Tanants has been updated successfully.", 1);
      $this->common->redirect(site_url('wizard/airport/airport_edit_step_5') . '/' . $cid . '/' . $oid);
      }

      $this->load->view('frontend/airport/airport-wizard-step-5', $this->outputData);
      } else {

      $this->common->setmessage("You are not allowed to access that page.", -1);

      redirect(base_url());
      }
      }
     */

    /**
     * 
     * START Airport Step 1 & 5 Delete Contacts & Tenants (Required Id)
     */
    public function airport_delete_step_1() {

        $now = $this->common->GetCurrrentDate();

        $cid = $this->input->post('aid');

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("modifydata", $aa)) {

            if ($this->input->is_ajax_request()) {
                $method = $this->input->post('action');


                switch ($method) {
                    case "delstep1":

                        $acid = $this->input->post('acid');

                        $cid = $this->input->post('aid');

                        $newarray = array(
                            'adeleted' => 1,
                        );
                        $where = array(
                            'acid' => $acid);

                        $this->common->DeleteRecord('airportcontacts', $where);

                        $IATA = $this->common->get_ariport_info($cid);

                        $this->common->getactivity('delete_airport_contact', ' (' . $IATA . ')');

                        $effected = $this->db->affected_rows();
                        if ($effected > 0) {

                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }
                        $this->common->UpdateRecord('outlets', array('action' => 0), array('action' => 1));
                        $this->common->setmessage("Airport contact has been Deleted successfully.", 1);

                        echo 1;

                        break;

                    case "delstep5":
                        $oid = $this->input->post('oid');

                        $cid = $this->input->post('aid');

                        $newarray = array(
                            'odeleted' => 1,
                        );
                        $where = array(
                            'oid' => $oid);

                        $this->common->DeleteRecord('outlets', $where);

                        $IATA = $this->common->get_ariport_info($cid);

                        $this->common->getactivity('delete_airport_tenants', ' (' . $IATA . ')');

                        $effected = $this->db->affected_rows();
                        if ($effected > 0) {
                            $this->common->UpdateRecord('airports', array('aid' => $cid), array('lastmodified' => $now, 'modifiedby' => $this->session->userdata('email')));
                        }
                        $this->common->setmessage("Tanants has been Deleted successfully.", 1);
                        echo 1;

                        break;
                }
            } else {
                $this->common->redirect(site_url());
                exit();
            }
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }

    /**
     * 
     * END Airport Step 1 & 5 Delete Contacts & Tenants
     */

    /**
     * 
     * START Create Support Ticket (Required Login)
     */
    public function support() {
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == "") {
            $this->common->setmessage("Please login to create ticket.", -1);
            redirect(site_url());
            exit();
        }
        $this->common->ThisSecureArea('user');

        $cr_time = $this->common->get_created_time();

        $subject = $this->input->post('subject');
        if ($subject != "") {
            $message = $this->input->post('message');
            $newarray = array('user_id' => $user_id, 'subject' => $subject, 'is_active' => 1, 'ticket_created_date' => $cr_time);
            $insert = $this->common->InsertInDb('support', $newarray);

            $data_array = array('support_id' => $insert, 'sender_bit' => 1, 'fr_ticket_conversion_message' => $message, 'fr_ticket_conversion_status' => 1, 'fr_ticket_created_on' => $cr_time, 'fr_ticket_is_active' => 1, 'fr_ticket_modified_by' => $user_id);
            $this->common->InsertInDb('support_ticket_conversion_frontend', $data_array);

            $this->common->setmessage("Your ticket has been created successfully <a href='" . site_url('frontend/viewtickets') . "'>View All Tickets</a>.", 1);
        }
        //redirect($_SERVER['HTTP_REFERER']);
        redirect(site_url());
        exit;
    }

    /**
     * 
     * END Create Support Ticket
     */

    /**
     * 
     * START View Support Ticket (Required Id)
     */
    public function viewtickets() {
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }
        $this->common->ThisSecureArea('user');
        $user_id = $this->session->userdata('user_id');
        $this->outputData['listoftickets'] = $this->common->JoinTables('support.support_id,support.status, subject, ticket_created_date, support.is_active, first_name, last_name', 'support', array(
            //array('support_ticket_conversion_frontend sf',"support.support_id = sf.support_id", 'left'),
            array('users', 'support.user_id = users.user_id', 'inner')
                ), array('support.user_id' => $user_id));
        $this->load->model('commonmodel');

        $this->load->view('frontend/tickets/viewtickets', $this->outputData);
    }

    /**
     * 
     * END View Support Ticket
     */

    /**
     * 
     * START Support Ticket Conversation (Required Id)
     */
    public function conversation() {
        $this->common->ThisSecureArea('user');
        $id = $this->uri->segment(3);
        $by = $this->session->userdata('user_id');
        $check = $this->input->post('submitted');
        if ($this->input->post('ticketid')) {
            $ticketid = $this->input->post('ticketid');
            $ticketstatus = $this->input->post('ticketstatus');
            $tname = $this->common->GetSingleRowWithColumn('support_ticket_type', array('support_ticket_type_id' => $ticketstatus), 'support_ticket_type_name');
            $this->common->UpdateRecord('support', array('support_id' => $ticketid), array('status' => $ticketstatus));
            $this->common->setmessage('Your support ticket has been <b>' . $tname['support_ticket_type_name'] . " </b>successfully.", 1);
        }
        $this->outputData['listoftype'] = $this->common->GetAllRowOrderBy("support_ticket_type", "support_ticket_type_id", "ASC");
        if ($check != "") {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('message', 'Message', 'trim|required|min_length[1]|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $this->common->setmessage("Please enter a message first...." . validation_errors(), -1);
                redirect(current_url());
                exit();
            }
            $cr_time = $this->common->get_created_time();
            $message = $this->input->post('message');
            $newarray = array('support_id' => $id, 'sender_bit' => 1, 'fr_ticket_conversion_message' => $message, 'fr_ticket_is_active' => 1, 'fr_ticket_created_on' => $cr_time, 'fr_ticket_modified_by' => $by);
            $insert = $this->common->InsertInDb('support_ticket_conversion_frontend', $newarray);
        }
        $this->outputData['receivemessage'] = $this->common->JoinTables('support.support_id,support.status,subject, ticket_created_date, support.is_active, sender_bit, fr_ticket_created_on, fr_ticket_conversion_message, first_name, last_name', 'support', array(
            array('support_ticket_conversion_frontend sf', "support.support_id = sf.support_id", 'left'),
            array('users', 'support.user_id = users.user_id', 'inner')
                ), array('support.user_id' => $by, 'support.support_id' => $id));
        if (count($this->outputData['receivemessage']) < 1) {
            $this->load->view('frontend/404', $this->outputData);
        }

        $this->load->model('commonmodel');
        $this->load->view('frontend/tickets/conversation', $this->outputData);
    }

    /**
     * 
     * END Support Ticket Conversation
     */

    /**
     * 
     * START View FAQ's Frontend (Required Id)
     */
    public function faq() {

        $this->common->ThisSecureArea('user_id');

        $this->outputData['faqs'] = $this->common->GetAllRowWithColumn('tbl_faq', '*', 'faq_is_active = 1', 'category_id', 'ASC');

        $this->outputData['faqcate'] = $this->common->get_all_row_from_table_where('tbl_faq_category', array('is_active' => 1));

        $this->load->view('frontend/faq', $this->outputData);
    }

    /**
     * 
     * END View FAQ's Frontend
     */

    /**
     * 
     * START User's FAQ Feedback (Required Id)
     */
    public function user_feedback() {
        $feedback = "";
        $faqid = $this->input->post('faqid');
        $first_name = $this->session->userdata('first_name');
        $last_name = $this->session->userdata('last_name');
        $email = $this->session->userdata('email');
        $check = $this->common->GetAllRowWithColumn('tbl_faq_feedback', 'faq_id,first_name,last_name', 'feedback_id > 0', 'feedback_id', 'ASC');
        $cr_time = $this->common->get_created_time();
        $yes = $this->input->post('yes');
        $no = $this->input->post('no');
        if ($yes == 1) {
            $feedback = $yes;
        } else {
            $feedback = $no;
        }
        foreach ($check as $c) {
            if ($faqid == $c['faq_id'] && $first_name == $c['first_name'] && $last_name == $c['last_name']) {
                $this->common->setmessage("You have already submitted your feedback about this question.", -1);
                redirect(site_url('faq-view'));
                exit();
            }
        }
        $newarray = array('faq_id' => $faqid, 'feedback_status' => $feedback, 'created_date' => $cr_time, 'is_active' => 1, 'first_name' => $first_name, 'last_name' => $last_name, 'email' => $email);
        $insert = $this->common->InsertInDb('tbl_faq_feedback', $newarray);
        $this->common->setmessage("Your feedback has been submitted successfully", 1);
        redirect(site_url('faq-view'));
    }

    /**
     * 
     * END User's FAQ Feedback
     */
}
