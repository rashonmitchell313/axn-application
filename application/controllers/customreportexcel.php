<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class customreportexcel extends CI_Controller 
{
		public $dataarray;
		public $style;	
		public $mpdf;
		public $limit;
		function __construct()	
		{			
			parent::__construct();
			$this->common->ThisSecureArea('user');
			include_once APPPATH.'Classes/PHPExcel.php';
			$this->common->pdfstyleing();
			$this->load->model('reportmodel');
			error_reporting(0);			
		}
		public function index()
		{
			/*$ayear=$this->uri->segment(0);
			if(is_numeric($ayear) && $ayear>2009 && $ayear<=date("Y"))
			{
				$ayear=$this->uri->segment(0);
			}else
			{
				$ayear=date("Y");	
			}	*/		
			$ayear = $this->input->post('year');
			if(!(is_numeric($ayear) && $ayear>2009 && $ayear<=date("Y")) && !is_array($ayear))
				$ayear = date("Y");
					
			$this->limit=$this->input->post('limit');
			$listofcolumns=$this->input->post('arr');
			$format=$this->input->post('format');			
			$this->input->post('reportid');			
			$reportinfo=$this->common->GetSingleRowFromTableWhere('reports',array('report_id'=>$this->input->post('reportid')));
			$segment=$reportinfo['url'];			
			switch($segment)
			{
				case "advertising":					
					$this->top50terminals('AD',$ayear);					
				break;
				case "ratio-report":					
					$this->ratioreport($ayear,$listofcolumns);
				break;
				case "food-beverage":					
					$this->top50terminals('FB',$ayear,$listofcolumns);					
				break;
				case "specialty-retail":					
					$this->top50terminals('SR',$ayear,$listofcolumns);		
				break;
				case "news-gifts":					
					$this->top50terminals('NG',$ayear,$listofcolumns);		
				break;
				case "duty-free":					
					$this->top50terminals('DF',$ayear,$listofcolumns);		
				break;
				case "passenger-services":					
					$this->top50terminals('PT',$ayear,$listofcolumns);		
				break;
				case "lease-expire":
					$this->leaseexpire($ayear,'test',$listofcolumns);
				break;
				case "top-50-airports":					
					$this->top50airport($ayear,$listofcolumns);					
				break;
				case "top-int-airports":
					$this->topIntairport($ayear,$listofcolumns);					
				break;
				case "all-airports":
						$this->allairport($ayear,$listofcolumns);
				break;
				case "tenant-listing":
					$this->tenant_listing($ayear, $listofcolumns);
				break;
				case "airport-reports":
					$this->allairport($ayear, $listofcolumns, $this->input->post('apid'));
				break;
				default:
					$this->common->setmessage('Invalid URL',-1);
					$this->common->redirect(site_url('reports'));	
				exit;
			}			 								
		}
		public function tenant_listing($ayear, $col)
		{
			$cols = $this->reportmodel->getcols($col);
			$cols = str_replace('category', 'lkpcategory`.`category', $cols);
			$tenant_list = $this->common->CustomQuery("SELECT $cols FROM outlets 
							LEFT JOIN lkpcategory ON lkpcategory.categoryid = outlets.categoryid
							LEFT JOIN airports ON airports.aid = outlets.aid LIMIT 0, ".$this->limit);
			
			$val = "Tenant Listing $ayear";
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle($val)
							 ->setSubject($val)
							 ->setDescription($val)
							 ->setKeywords($val)
							 ->setCategory($val);
			
			$BGstyleArray = array('fill'  => array('type'=> PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('argb' => 'F2F2F2')));
			$styleArray = array('font'  => array('bold'  => true,'color' => array('hex' => '000'),'size'  => 12,'name'  => 'Verdana'));
			$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
			// Style for the column titles
			$heading = array('font'  => array('bold'  => true,'color' => array('hex' => '000'),'size'  => 10,'name'  => 'Verdana'));
			$titlestyle = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('hex' => '000'),
					'size'  => 10,
					'name'  => 'Verdana'
				),
				'borders' => array(
				'bottom' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THICK,
				),
				'top' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THICK,
				)
			  )
			);
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1');
			$h = 0;//var_dump($col);exit();
			$titlecell = 2;
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1');
			$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtoupper($this->reportmodel->colname(count($col))).'2')->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Tenant Listing '.$ayear);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.strtoupper($this->reportmodel->colname(count($col))).'2')->applyFromArray($titlestyle);
			foreach($col as $key => $val):					
				$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$titlecell, $this->reportmodel->coltitle($key));
				$h++;
			endforeach;
			
			$cell = 3;
			foreach($tenant_list as $tenant):
				$h = 0;
				foreach($col as $key => $val):
					$objPHPExcel->getActiveSheet()->setCellValue($this->reportmodel->colname($h).''.$cell, $tenant[$key]);
					$h++;
				endforeach;
				$cell++;
			endforeach;
			
			
			$objPHPExcel->getActiveSheet()->setTitle('Tenant_Listing_'.$ayear);
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			$sheetName = $ayear.'_Tenant_Listing.xlsx';
			// Redirect output to a client’s web browser (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$sheetName.'"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
			$objWriter->save('php://output');
			///////////////////////////////////
			
			/*$tbl_header .= '<tr style="width: 100%;font-family:Vectora LH 55 Roman,arial;">';
			$n = (count($col) != 0)?100/count($col):100;
			foreach($col as $i=>$coll):
				$column = $this->reportmodel->coltitle($i);
				$tbl_header .= '<th width="'.$n.'%">'.$column.'</th>
								';
			endforeach;
			$tbl_header .= '</tr>';
			
			$color = '#EEEEEE';
			foreach($tenant_list as $tenant):
				$color = ($color == '#EEEEEE')?'#FFFFFF':'#EEEEEE';
				$tbl_header .= '<tr style="background-color:'.$color.';">';
				foreach($col as $i=>$coll):
					$tbl_header .= '<td align="center">'.$tenant[$i].'</td>
									';
				endforeach;
				$tbl_header .= '</tr>';
			endforeach;
			$tbl_header .= '</table>';
			//echo $tbl_header;
			$str = preg_replace('/\s\s+/', ' ', $tbl_header);
			$mpdf->WriteHTML($str);	
			$mpdf->Output();*/
		}
		public function allairport($ayear, $col, $aid = "")
		{	
			if($aid != "")
				$aid = " WHERE airports.aid IN (".implode(",", $aid).")";
			
			$limit = ($this->limit != "")?" LIMIT 0,".$this->limit:"";
		
			$cols = ($col == "")?array():explode(",",$this->reportmodel->getcols($col));
			$ref = $ayear; 
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle($ref." International Airports by Performance")
							 ->setSubject($ref." International Airports by Performance")
							 ->setDescription($ref." International Airports by Performance")
							 ->setKeywords($ref." International Airports by Performance")
							 ->setCategory($ref." International Airports by Performance");
			// Style for the Main heading
			$BGstyleArray = array(
				'fill'  => array(
						'type'       => PHPExcel_Style_Fill::FILL_SOLID,
						'startcolor' => array(
						'argb' => 'F2F2F2'
						)
					)
				);
			$styleArray = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('hex' => '000'),
				
					'size'  => 12,
					'name'  => 'Verdana'
					
				),
				'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )				
				
				
				);
			$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);
		// Style for the column titles
		$heading = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('hex' => '000'),
				'size'  => 10,
				'name'  => 'Verdana'
			)
			);
$titlestyle = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    ),
	'borders' => array(
	'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    ),
    'top' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    )
  )
);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

/*
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);*/
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('All Airports');

//(Includes duty free sales)
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
		$count 		= 1;
		$headcell	= 2;
		$titlecell	= 3;
		$cell 		= 4;

$sql="SELECT * FROM airports $aid $limit";				
			$ap_count=$this->common->CustomCountQuery($sql);
			$airportlist=$this->common->CustomQueryALL($sql);
			if($ap_count >0)
		{	
		$ay = array();
		if(!is_array($ayear))
			$ay[] = $ayear;
		else
			$ay = $ayear;
		foreach($ay as $year):
			//$it = 0;
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.($count).':I'.($count));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':H'.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count++), "Airport Report $year");
			foreach($airportlist as $airport): 
			$join_array = array(
				array('airports', 'airports.aid = airportcontacts.aid', 'LEFT'),
				array('lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT')	
			);
			$outlets=$this->common->JoinTable('*','outlets','lkpcategory','outlets.categoryid=lkpcategory.categoryid','LEFT',"aid = ".$airport['aid']." AND lkpcategory.categoryid !='' ORDER BY lkpcategory.categoryid");
			$contacts=$this->common->JoinTables('*','airportcontacts',$join_array,array('airportcontacts.aid'=> $airport['aid']),"");
			$terminals=$this->common->JoinTable('*','terminals','terminalsannual','terminalsannual.tid = terminals.tid','LEFT',array('aid'=>$airport['aid']));
			$annual=$this->common->JoinTable('*','airportsannual','airports','airports.aid = airportsannual.aid','LEFT',array('airportsannual.aid'=>$airport['aid']));
			$t = 0;//if($it > 0) {echo $count+$t;exit;}$it++;
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.($count+$t).':I'.($count+$t));
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.($count+(++$t)).':I'.($count+$t));
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.($count+(++$t)).':I'.($count+$t));
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.($count+$t).':I'.($count+$t),'A'.($count+$t-1).':I'.($count+$t-1));
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$t-2).':H'.($count+$t-2))->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$t-1).':H'.($count+1))->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$t).':H'.($count+$t))->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$t-2),$airport['acity'].",".$airport['astate']);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$t-1),$airport['aname']);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($t++)),$airport['IATA']);
			$counter=0;//if($it > 0) {echo $count+$t;exit;}$it++;
			$t += 2;
			if(in_array('`contacts`',$cols)) {
				if(count($contacts)>0)
				{
					$s = 0;
					foreach($contacts as $contact):
					$r = $t;
					$p = $r;
					if($counter%2==0 )
						{
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':C'.($count+$r))->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),$contact['mgtresponsibilityname']);	

		//$objPHPExcel->getActiveSheet()->getStyle('A'.($count+4))->applyFromArray($titlestyle);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Contact');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Title');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Company');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Address');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Phone');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Fax');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Email');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Website');
		
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+(++$p)))->applyFromArray($BGstyleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$contact['alname'].', '.$contact['afname']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$contact['atitle']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),substr($contact['acompany'],0,20));
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$contact['aaddress1'].' '. $contact['accity'].' '.$contact['acstate'].' '.$contact['aczip'].' '.$contact['accountry']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$contact['aphone'].' '.$contact['aext']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$contact['afax']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$contact['aemail']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$contact['awebsite']);
		
					
						}
					else if($counter%2!=0)
					{++$p;
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r).':G'.($count+($r)))->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),$contact['mgtresponsibilityname']);	
		

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Contact');

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Title');

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Company');

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Address');

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Phone');

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Fax');

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Email');

		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Website');
				
		$objPHPExcel->getActiveSheet()->getStyle('G'.($count+(++$p)))->applyFromArray($BGstyleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),$contact['alname'].', '.$contact['afname']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),$contact['atitle']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),substr($contact['acompany'],0,20));
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),$contact['aaddress1'].' '. $contact['accity'].' '.$contact['acstate'].' '.$contact['aczip'].' '.$contact['accountry']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),$contact['aphone'].' '.$contact['aext']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),$contact['afax']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),$contact['aemail']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($p++)),$contact['awebsite']);				
				
				
				
				
					}			
					
						//$count+=5;
						$t += 5;
						$counter++;
						$s = ($r > $p)?$r:$p;
					endforeach;//if($it > 0) {echo $s;exit;}$it++;
					$t = $s;
					$s = 0;
				}
			}//if($it > 0) {echo $count+$t;exit;}$it++;
			if(in_array('`terminal`',$cols)) {
		//$count+=4;
		$r = $t;
		$p = $r + 2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');	
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':H'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'Airport Info');
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Airport Configuration:');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Concessions Mgt. Type:');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Expansion Planned:');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Add Sq. Ft. ');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Completion Date:');
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+$p), 'Terminal/Concourses');
		$objPHPExcel->getActiveSheet()->getStyle('G'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+$p), 'Abbr.');
		$objPHPExcel->getActiveSheet()->getStyle('H'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.($count+$p), 'Dominant Airline');
		$s = $p;//echo $count+$t;exit;
		
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$airport['configuration']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$airport['mgtstructure']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$airport['texpansionplanned']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$airport['addsqft']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+($p++)),$airport['completedexpdate']);
		//$p += 2;
		foreach($terminals as $term)://echo $count+(++$s);exit;
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+(++$s)),$term['terminalname']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+$s),$term['terminalabbr']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.($count+$s),$term['tdominantairline']);//echo $p;exit;
			//$count++;
		endforeach;
		$t = $p + 2;
			}
			if(in_array('`passengertraffic`',$cols)) {
		//$count+=4;
		$r = $t;
			
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':G'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'Passenger Traffic');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), 'Terminal');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Total');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r), '+/-%');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r), 'Deplanning');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r), 'Enplanning');
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+$r), 'EP Domestic');
		$objPHPExcel->getActiveSheet()->getStyle('G'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($r++)), 'Ep Int\'l');
		$ttpasstraffic=0;
		$ttadeplaning=0;
		$ttaenplaning=0;
		$ttaepdomestic=0;
		$tttaepintl=0;
		
		foreach($terminals as $term):
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),$term['terminalabbr']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),@number_format($term['tpasstraffic']));
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),@number_format($term['tpasstrafficcompare']));
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),@number_format($term['tdeplaning']));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),@number_format($term['tenplaning']));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+$r),@number_format($term['tepdomestic']));
			$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($r++)),@number_format($term['tepintl']));
			//$count++;
			$ttpasstraffic+=$term['tpasstraffic'];
			$ttadeplaning+=$term['tdeplaning'];
			$ttaenplaning+=$term['tenplaning'];
			$ttaepdomestic+=$term['tepdomestic'];
			$tttaepintl+=$term['tepintl'];	
		endforeach;
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+(++$r)))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Total');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),number_format($ttpasstraffic));
			$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'');
			$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),number_format($ttadeplaning));
			$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),number_format($ttaenplaning));
			$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+$r),number_format($ttaepdomestic));
			$objPHPExcel->getActiveSheet()->getStyle('G'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+($r++)),number_format($tttaepintl));
			//$t = $r + 2;
			$t = $r;
			}
			////////////////Airport Percentage //////////////////
			if(in_array('`airportpercentages`',$cols)) {
				$r = $t + 2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');	
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':G'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'Airport Percentages');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), 'Pre/Post Security');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Business to Leisure Ratio');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r), 'O&D Transfer');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+($r++)), 'Average Dwell Time');
		$pre = $annual[0]['presecurity'].'/'.$annual[0]['postsecurity'];
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),$pre);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),$annual[0]['ratiobusleisure']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),$annual[0]['ondtransfer']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+($r++)),$annual[0]['avgdwelltime']);
		$t = $r;
			}
			////////////////////////////////////////////////////
			if(in_array('`airportwideinfo`',$cols)){
				$r = $t + 2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');	
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':J'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'Airportwide Info');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), 'Parking');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Short');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r), 'Long');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r), 'Economy');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r), 'Valet');
		
		$objPHPExcel->getActiveSheet()->getStyle('G'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+$r), 'Car Rentals');
		$objPHPExcel->getActiveSheet()->getStyle('H'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.($count+$r), 'Agencies');
		$objPHPExcel->getActiveSheet()->getStyle('I'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.($count+$r), 'Gross Rev');
		$objPHPExcel->getActiveSheet()->getStyle('J'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.($count+($r++)), 'Gross Renta');
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Hourly');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($annual[0]['hourlyshort'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.@number_format($annual[0]['hourlylong'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.@number_format($annual[0]['hourlyeconomy'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.@number_format($annual[0]['hourlyvalet'],2));
		
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+$r),'Car Rental On Site');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.($count+$r),$annual[0]['carrentalagenciesonsite'],2);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.($count+$r),'$'.@number_format($annual[0]['carrentalrevonsite'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('J'.($count+($r++)),'$'.@number_format($annual[0]['carrentalrevtoaironsite'],2));

		
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Daily');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($annual[0]['dailyshort'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.@number_format($annual[0]['dailylong'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.@number_format($annual[0]['dailyeconomy'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.@number_format($annual[0]['dailyvalet'],2));
		
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+$r),'Car Rental Off Site');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.($count+$r),$annual[0]['carrentalagenciesoffsite'],2);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.($count+$r),'$'.@number_format($annual[0]['carrentalrevoffsite'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('J'.($count+($r++)),'$'.@number_format($annual[0]['carrentalrevtoairoffsite'],2));
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'# Spaces');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),@number_format($annual[0]['spacesshort'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),@number_format($annual[0]['spaceslong'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),@number_format($annual[0]['spaceseconomy'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),@number_format($annual[0]['spacesvalet'],2));
		
		$objPHPExcel->getActiveSheet()->getStyle('G'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+$r),'Total Cars Rented');
		$objPHPExcel->getActiveSheet()->getStyle('H'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.($count+$r),$annual[0]['totalcarsrented'],2);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.($count+$r),'');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.($count+($r++)),'0');
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Parking Revenue');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($annual[0]['parkingrev'],2));
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'Total Spaces');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.@number_format($annual[0]['parkingspaces'],2));
		
		$objPHPExcel->getActiveSheet()->getStyle('G'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.($count+$r),'Car Rental Sq. Ft');
		$objPHPExcel->getActiveSheet()->getStyle('H'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.($count+$r),$annual[0]['carrentalsqft'],2);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.($count+$r),'');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.($count+($r++)),'0');

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), '');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Revenue');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+($r++)), 'Rev. to Airport');
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Passenger Services');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($annual[0]['passservicesrev'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+($r++)),'$'.@number_format($annual[0]['passservicesrevtoair'],2));

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Advertising');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($annual[0]['advertisingrev'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+($r++)),'$'.@number_format($annual[0]['advertisingrevtoair'],2));

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Currency Exchange');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($annual[0]['currencyexrev'],2));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+($r++)),'$'.@number_format($annual[0]['currencyexrevtoair'],2));
		$t = $r;
			}
			if(in_array('`concessiontenantdetails`',$cols)) {
				$r = $t + 2;
		/////////////////////////////food/bearage start/////////////////////////////////////
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':G'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'Concession Totals - Terminal Breakdowns ( Food/Beverage, Specialty Retail, News /Gifts Only )');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Food/Beverage');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), 'Terminal');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Gross Sales');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r), 'Sales/EP');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r), 'Rent Rev to Airport');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r), 'Rent/EP');
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Current Sq. Ft.');
		$tfbgrosssales=0;
		$tfbsalesep=0;
		$tfbrentrev=0;
		$tfbrentep=0;	
		$tfbcurrsqft=0;

		foreach($terminals as $term):
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),$term['terminalabbr']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($term['fbgrosssales'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.@number_format(($term['fbgrosssales'] != 0)?$term['fbgrosssales']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.@number_format($term['fbrentrev'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.@number_format(($term[$p.'rentrev'] != 0)?$term[$p.'rentrev']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),@number_format($term['fbcurrsqft'],2));
		
			$tfbgrosssales+=$term['fbgrosssales'];
			$tfbsalesep+=($term['fbgrosssales'] != 0)?$term['fbgrosssales']/$term['tenplaning']:0;
			$tfbrentrev+=$term['fbrentrev'];
			$tfbrentep+=($term['fbrentrev'] != 0)?$term['fbrentrev']/$term['tenplaning']:0;	
			$tfbcurrsqft+=$term['fbcurrsqft'];
			//$count++;
		endforeach;
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+(++$r)))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Total');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.number_format($tfbgrosssales,2));
			$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.number_format($tfbsalesep,2));
			$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.number_format($tfbrentrev,2));
			$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.number_format($tfbrentep,2));
			$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),number_format($tfbcurrsqft,2));		
		//////////////////////////////food/berage end ///////////////////////////////////
		////////////////////////////////Specialty Retail//////////////////////////////////
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Specialty Retail');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), 'Terminal');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Gross Sales');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r), 'Sales/EP');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r), 'Rent Rev to Airport');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r), 'Rent/EP');
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Current Sq. Ft.');
		$tfbgrosssales=0;
		$tfbsalesep=0;
		$tfbrentrev=0;
		$tfbrentep=0;	
		$tfbcurrsqft=0;

		foreach($terminals as $term):
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),$term['terminalabbr']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($term['srgrosssales'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.@number_format(($term['srgrosssales'] != 0)?$term['srgrosssales']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.@number_format($term['srrentrev'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.@number_format(($term['srrentrev'] != 0)?$term['srrentrev']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),@number_format($term['srcurrsqft'],2));
		
			$tfbgrosssales+=$term['srgrosssales'];
			$tfbsalesep+=($term['srgrosssales'] != 0)?$term['srgrosssales']/$term['tenplaning']:0;
			$tfbrentrev+=$term['srrentrev'];
			$tfbrentep+=($term['srrentrev'] != 0)?$term['srrentrev']/$term['tenplaning']:0;	
			$tfbcurrsqft+=$term['srcurrsqft'];
			//$count++;
		endforeach;
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Total');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.number_format($tfbgrosssales,2));
			$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.number_format($tfbsalesep,2));
			$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.number_format($tfbrentrev,2));
			$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.number_format($tfbrentep,2));
			$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),number_format($tfbcurrsqft,2));		
		/////////////////////////////////////////////////////////////////
				////////////////////////////////News/Gifts Retail//////////////////////////////////
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'News/Gifts Retail');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), 'Terminal');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Gross Sales');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r), 'Sales/EP');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r), 'Rent Rev to Airport');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r), 'Rent/EP');
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Current Sq. Ft.');
		$tfbgrosssales=0;
		$tfbsalesep=0;
		$tfbrentrev=0;
		$tfbrentep=0;	
		$tfbcurrsqft=0;

		foreach($terminals as $term):
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),$term['terminalabbr']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($term['nggrosssales'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.@number_format(($term['nggrosssales'] != 0)?$term['nggrosssales']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.@number_format($term['ngrentrev'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.@number_format(($term['ngrentrev'] != 0)?$term['ngrentrev']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),@number_format($term['ngcurrsqft'],2));
		
			$tfbgrosssales+=$term['nggrosssales'];
			$tfbsalesep+=($term['nggrosssales'] != 0)?$term['nggrosssales']/$term['tenplaning']:0;
			$tfbrentrev+=$term['ngrentrev'];
			$tfbrentep+=($term['ngrentrev'] != 0)?$term['ngrentrev']/$term['tenplaning']:0;	
			$tfbcurrsqft+=$term['ngcurrsqft'];
			//$count++;
		endforeach;
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+(++$r)))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Total');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.number_format($tfbgrosssales,2));
			$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.number_format($tfbsalesep,2));
			$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.number_format($tfbrentrev,2));
			$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.number_format($tfbrentep,2));
			$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),number_format($tfbcurrsqft,2));		
		/////////////////////////////////News/Gifts Retail ends////////////////////////////////
		////////////////////////////////Duty Free Retail//////////////////////////////////
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)),'');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r).':F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Duty Free Retail');
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r), 'Terminal');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r), 'Gross Sales');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r), 'Sales/EP');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r), 'Rent Rev to Airport');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r), 'Rent/EP');
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)), 'Current Sq. Ft.');
		$tfbgrosssales=0;
		$tfbsalesep=0;
		$tfbrentrev=0;
		$tfbrentep=0;	
		$tfbcurrsqft=0;

		foreach($terminals as $term):
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),$term['terminalabbr']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.@number_format($term['dfgrosssales'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.@number_format(($term['dfgrosssales'] != 0)?$term['dfgrosssales']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.@number_format($term['dfrentrev'],2));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.@number_format(($term['dfrentrev'] != 0)?$term['dfrentrev']/$term['tenplaning']:0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),@number_format($term['dfcurrsqft'],2));
		
			$tfbgrosssales+=$term['dfgrosssales'];
			$tfbsalesep+=($term['dfgrosssales'] != 0)?$term['dfgrosssales']/$term['tenplaning']:0;
			$tfbrentrev+=$term['dfrentrev'];
			$tfbrentep+=($term['dfrentrev'] != 0)?$term['dfrentrev']/$term['tenplaning']:0;	
			$tfbcurrsqft+=$term['dfcurrsqft'];
			//$count++;
		endforeach;
			$objPHPExcel->getActiveSheet()->getStyle('A'.($count+(++$r)))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$r),'Total');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$r),'$'.number_format($tfbgrosssales,2));
			$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$r),'$'.number_format($tfbsalesep,2));
			$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$r),'$'.number_format($tfbrentrev,2));
			$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$r),'$'.number_format($tfbrentep,2));
			$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$r))->applyFromArray($titlestyle);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($r++)),number_format($tfbcurrsqft,2));
			$t = $r + 2;		
				
		/////////////////////////////////Duty Free Retail ends////////////////////////////////
			////////////////////////////////Concession Tenant Details (2010)l//////////////////////////////////
			
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+(++$r)),'');
		//echo $r;exit();
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+(++$r)).':F'.($count+$r))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+($r++)), 'Concession Tenant Details (2010)');
	$old='Food/Beverage';
					$c=0;
	$p = $r + 2;
	foreach($outlets as $o):
	if($old!=$o['category'] && $c!=0)
						{
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+(++$p)).':F'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$p),$o['category'].'Tenant (Company)');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$p), 'Terminal');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$p), '# locations');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$p), '# Sq. Ft.');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+($p++)), 'Expires');
		$old=$o['category'];
		//$count+=2;
	}
	if($c==0)
						{//$p = $r-2;
		$objPHPExcel->getActiveSheet()->getStyle('A'.($count+(++$p)).':F'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$p),'Food/Beverage Tenant (Company)');
		$objPHPExcel->getActiveSheet()->getStyle('B'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$p), 'Product Description');
		$objPHPExcel->getActiveSheet()->getStyle('C'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$p), '# Terminal');
		$objPHPExcel->getActiveSheet()->getStyle('D'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$p), '# locations');
		$objPHPExcel->getActiveSheet()->getStyle('E'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$p), '# Sq. Ft.');		
		$objPHPExcel->getActiveSheet()->getStyle('F'.($count+$p))->applyFromArray($titlestyle);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+($p++)), 'Expires.');			
						}
						
						
											
			//$p = $r-1;			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($count+$p),$o['outletname']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($count+$p),$o['productdescription']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.($count+$p),$o['termlocation']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.($count+$p),$o['numlocations']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.($count+$p),$o['sqft']);
			if($o['exp']!='') 
						{
			$objPHPExcel->getActiveSheet()->setCellValue('F'.($count+$p),date('m/d/Y',strtotime($o['exp'])));
			}
		$p++;
		$t = $p;
		//$count++;
		$c++;
		endforeach;	//echo $count+$t;exit;
		++$t;
		//$t += 25;
		//$t += $p + 7;
			}
		///////////////////////////////Concession Tenant Details (2010)////////////////////////////////
			///
			$count += $t;
			$cell++;
		endforeach;
		endforeach;
		}
		 else 
			{	
		 $cell=$cell=15;			
			//exit();
	 	  $objPHPExcel->getActiveSheet()->setCellValue('D'.$cell, 'F&B dsadasdas Sales');
	
			}	

$sheetName = 'All_Airports.xlsx';
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$sheetName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
$objWriter->save('php://output');
		
		
		
		}
		public function ratioreport($year,$col)
		{
			$cols=$this->reportmodel->getcols($col);	
			$airport=$this->common->CustomQueryALL('SELECT DISTINCT(`IATA`),`aid` as aid,`acity` FROM `airports`');
			$val=$year.' Ratios Report';						
			$ref = $year; 		
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle($val)
							 ->setSubject($val)
							 ->setDescription($val)
							 ->setKeywords($val)
							 ->setCategory($val);
			$BGstyleArray = array('fill'  => array('type'=> PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('argb' => 'F2F2F2')));
			$styleArray = array('font'  => array('bold'  => true,'color' => array('hex' => '000'),'size'  => 12,'name'  => 'Verdana'));
			$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
			// Style for the column titles
			$heading = array('font'  => array('bold'  => true,'color' => array('hex' => '000'),'size'  => 10,'name'  => 'Verdana'));
			$titlestyle = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('hex' => '000'),
					'size'  => 10,
					'name'  => 'Verdana'
				),
				'borders' => array(
				'bottom' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THICK,
				),
				'top' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THICK,
				)
			  )
			);
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1');
			$h=1;
			foreach($col as $key => $val):		
				$objPHPExcel->getActiveSheet()->getColumnDimension(strtoupper($this->reportmodel->colname($h)))->setWidth($this->reportmodel->getcolwidth($key));
			$h++;
			endforeach;			
			$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle("A1:".strtoupper($this->reportmodel->colname(count($col)-1))."1")->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->setCellValue('A1','Ratios Report '.$ref);
			$tbl = '';			
			$tbl2 = '';			
			$tbl_header='';			
			$tbl_header2='';
			$rcounter=0;
			$h=0;
			$titlecell=2;
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.strtoupper($this->reportmodel->colname(count($col)-1)).'2')->applyFromArray($titlestyle);
			foreach($col as $key => $val):					
				$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$titlecell,$this->reportmodel->coltitle($key));
			$h++;
			endforeach;
			
			$cell=3;
			foreach($airport as $AP):			
				$join_array2 = array(array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'));
				$select=$cols;
				//$select="airports.IATA,airports.acity,airportsannual.presecurity,airportsannual.postsecurity,airportsannual.ratiobusleisure,airportsannual.avgdwelltime";
				$where=array('airports.aid'=>$AP['aid'],'airportsannual.ayear'=>$year);
				$info=$this->common->JoinTables($select,'airports',$join_array2,$where,"airports.aid");					
				foreach($info as $in):
					$h=0;
					foreach($col as $key => $val):
						$objPHPExcel->getActiveSheet()->setCellValue($this->reportmodel->colname($h).''.$cell, $this->reportmodel->is_numericcheck($key,$in[$key]));	
						//$tbl_header.='<td align="center">'.$this->reportmodel->is_numericcheck($key,$in[$key]).'</td>';
					$h++;
				endforeach;							
					$cell++;							
				endforeach;	
			endforeach;							
			$objPHPExcel->getActiveSheet()->setTitle('Ratios_Report_'.$ref);
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			$sheetName = $year.'Ratios_Report.xlsx';
			// Redirect output to a client’s web browser (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$sheetName.'"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
			$objWriter->save('php://output');
		}
		public function top50terminals($reportfor,$year,$col)
		{
			$cols=$this->reportmodel->getcols($col);			
			$objPHPExcel = new PHPExcel();			
			if($reportfor=='SR')
			{
				$reportfor='SR';
				$reptitle="Specialty Retail";	
				$orderby="srsalesep";
			}
			if($reportfor=='DF')
			{
				$reportfor='DF';
				$reptitle="Duty Free";
				$orderby="dfsalesep";
			}
			if($reportfor=='NG')
			{
				$reportfor='NG';
				$reptitle="News & Gifts";
				$orderby="ngsalesep";
			}
			if($reportfor=='FB')
			{
				$reportfor='FB';
				$reptitle="Food and Beverage";
				$orderby="fbsalesep";
			}
			if($reportfor=='PT')
			{
				$reportfor='PT';
				$reptitle="Passenger Traffic";
				$orderby="ptsalesep";
			}
			if($reportfor=="AD")
			{
				$reptitle='Advertising';
			}
			$airport=$this->common->CustomQueryALL('SELECT DISTINCT(`IATA`),`aid` as aid,`acity` FROM `airports`');
			$users = array();
			$TCurrentSqFt=0;
			$TGrossSale=0;
			$TSalesEp=0;
			$TRentRev=0;
			foreach ($airport as $AP) 
			{
				$airportinfo=$this->common->GetSingleRowFromTableWhere('airports',array('aid'=>$AP['aid']));	
				$tl=$this->common->JoinTable($cols,'terminals,airports','terminalsannual',
				'terminalsannual.tid=terminals.tid','LEFT',"terminals.aid = '".$AP['aid']."' AND terminalsannual.tyear='".$year."' ORDER BY terminals.tid limit 10");
				if(count($tl)>0)
				{					
					foreach ($tl as $_) 
					{				
						if(count($tl)>0)
						{					
							foreach ($tl as $_) 
							{											 
								$users[] = $_;							
							} // END foreach ($tl as $_) 
						}
					}
				}
			} // end foreach ($airport as $AP) 
		//print_r($users);
		//echo "<pre>";
		//print_r($users);
		//echo "</pre>";
		//exit;
		$songs =  $this->common->subval_sort($users,$orderby);
		$val='Top '.$this->limit.' TERMNLS '.$reptitle;
		$ref = date("Y"); 		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle($val)
							 ->setSubject($val)
							 ->setDescription($val)
							 ->setKeywords($val)
							 ->setCategory($val);
//$val='Top 50 Airports '.$ref;
// Style for the Main heading
		$BGstyleArray = array(
			'fill'  => array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
					'argb' => 'F2F2F2'
					)
				)
			);
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('hex' => '000'),
				'size'  => 12,
				'name'  => 'Verdana'
			));
		$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				)
			);
		// Style for the column titles
		$heading = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('hex' => '000'),
				'size'  => 10,
				'name'  => 'Verdana'
			)
			);
		$titlestyle = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('hex' => '000'),
				'size'  => 10,
				'name'  => 'Verdana'
			),
			'borders' => array(
			'bottom' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THICK,
			),
			'top' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THICK,
			)
		  )
		);
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.strtolower($this->reportmodel->colname(count($col)-1)).'1');
		$h=1;
		foreach($col as $key => $val):		
			$objPHPExcel->getActiveSheet()->getColumnDimension(strtolower($this->reportmodel->colname($h)))->setWidth($this->reportmodel->getcolwidth($key));
		$h++;
		endforeach;
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtolower($this->reportmodel->colname(count($col)-1)).'1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtolower($this->reportmodel->colname(count($col)-1)).'1')->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A1',$year.' Top '.$this->limit.' Terminals by '.$reptitle);
		$objPHPExcel->getActiveSheet()->setTitle('Top_'.$this->limit.'_TRMNL_'.str_replace(" ","_",$reptitle));
		$objPHPExcel->setActiveSheetIndex(0);		
		$count 		= 1;
		$headcell	= 2;
		$titlecell	= 3;
		$cell 		= 4;		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headcell.':'.strtolower($this->reportmodel->colname(count($col)-1)).$headcell)->applyFromArray($heading);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$headcell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).$headcell);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headcell,' ');			
		$objPHPExcel->getActiveSheet()->getStyle('A'.$titlecell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).$titlecell)->applyFromArray($titlestyle);
		$h=0;
		foreach($col as $key => $val):					
			$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$titlecell,$this->reportmodel->coltitle($key));
		$h++;
		endforeach;	
		$counter=0; 		 
		for($i=count($songs)-1;$i>=0;$i--)
		{			
			if($counter%2 == 0){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':'.strtoupper($this->reportmodel->colname(count($col))).$cell)->applyFromArray($BGstyleArray);
			}
			$h=0;
			foreach($col as $key => $val): //$songs[$i][$key]
				$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$cell,$this->reportmodel->is_numericcheck($key,$songs[$i][$key]));
			$h++;
			endforeach;
			if($counter==$this->limit-1)
			{
				break;	
			}
			$counter++;
			$cell++;
		}  
		/*$cell=$cell+2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell, "Total");
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$cell, "");
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$cell, "");
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$cell, number_format($nomiCurrentSqFt,2));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$cell, "$".number_format($nomiGrossSales,2));
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$cell, "$".number_format($nomiSalesEP,2));
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$cell, "$".number_format($nomiGrossRentals,2));		
		*/
//===================== Unset All Variables =========================================
unset($aenplaning);
unset($afbgrosssales);
unset($asrgrosssales);
unset($anggrosssales);
unset($aconcessiongrosssales);
unset($avg_sp);
//==============================================================

//==============================================================

$sheetName = $year.'_Top_'.$this->limit.'_Terminals_by_'.str_replace(" ","_",$reptitle).'.xlsx';
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$sheetName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
$objWriter->save('php://output');


exit;

			
		} // END OF FUnction
		public function leaseexpire($ayear,$type,$col)
		{
			$cols=$this->reportmodel->getcols($col);
			$ref=$ayear;
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
										 ->setLastModifiedBy("Shawn Hanlon")
										 ->setTitle("Leases Due to Expire by Year-End".$ref)
										 ->setSubject("Leases Due to Expire by Year-End".$ref)
										 ->setDescription("Leases Due to Expire by Year-End".$ref)
										 ->setKeywords("Leases Due to Expire by Year-End".$ref)
										 ->setCategory("Leases Due to Expire by Year-End".$ref);
			$val='Leases Due to Expire by Year-End '.$ref;
			$styleArray = array('font'  => array('bold'  => true,'color' => array('hex' => '000'),'size'  => 12,
					'name'  => 'Verdana'
				));
			 $style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);
			// Style for the column titles
			$heading = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('hex' => '000'),
					'size'  => 10,
					'name'  => 'Verdana'
				)
				);
			$titlestyle = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('hex' => '000'),
					'size'  => 10,
					'name'  => 'Verdana'
				),
				'borders' => array(
				'bottom' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THICK,
				),
				'top' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THICK,
				)
				)
				);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.strtolower($this->reportmodel->colname(count($col)-1)).'1');
			$h=1;
			foreach($col as $key => $val):		
				$objPHPExcel->getActiveSheet()->getColumnDimension(strtolower($this->reportmodel->colname($h)))->setWidth($this->reportmodel->getcolwidth($key));
			$h++;
			endforeach;			
			$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtolower($this->reportmodel->colname(count($col)-1)).'1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtolower($this->reportmodel->colname(count($col)-1)).'1')->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->setCellValue('A1','Leases Due to Expire by Year-End '.$ref);			
			$objPHPExcel->getActiveSheet()->setTitle('Leases_Due_Expire_'.$ref);			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			$tbl = '';
			$tbl2 = '';
			$tbl_header='';			
			$tbl_header2='';			
			$listsql = "select distinct category from lkpcategory";
		    $listsql2="SELECT count(oid) FROM `outlets` ";
			$headcell	= 2;
			$titlecell	= 3;
			$cell 		= 4;			
			$list=$this->common->CustomQueryALL($listsql);
		   	$listcount=$this->common->CustomCountQuery($listsql);
		   	$nr_of_users=$this->common->CustomQueryALL($listsql2);		
			foreach($list as $cmpnyterms)
			{			
				 $catid=$cmpnyterms['category'];
				 $ref=trim($ayear);			
				 $products = $this->common->CustomQueryALL("select DATE_FORMAT(exp,'%m-%d-%y') as expf,".$cols.",`aid` from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='".$catid."' and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and outlets.exp LIKE '%".$ref."%' order by DATE_FORMAT(exp,'%m-%d-%y') ASC ");
				 //echo 'A'.$headcell.':'.strtolower($this->reportmodel->colname(count($col)-1)).''.$headcell;
				 $objPHPExcel->getActiveSheet()->getStyle('A'.$headcell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$headcell)->applyFromArray($heading);
				 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$headcell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$headcell);
				 $objPHPExcel->getActiveSheet()->setCellValue('A'.$headcell,$catid);
				 $datetime	= date("Y/m/d H:i:s");
			     $objPHPExcel->getActiveSheet()->setCellValue('A'.$headcell,$datetime);
				 $objPHPExcel->getActiveSheet()->getStyle('A'.$headcell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				 $objPHPExcel->getActiveSheet()->getStyle('A'.$titlecell.':'.strtolower($this->reportmodel->colname(count($col)-1)).''.$titlecell)->applyFromArray($titlestyle);
				 $h=0;
				 foreach($col as $key => $val):					
					$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$titlecell,$this->reportmodel->coltitle($key));
				 $h++;
				 endforeach;
				 $resourcearray = array();
				 $indexarray = array();
				foreach($products as $prod)			
				 {
					$aiddd=$prod['aid'];			
					$citystrSQL = "SELECT acity FROM airports WHERE aid='".$aiddd."'";							
					$acity_row=$this->common->CustomQueryROw($citystrSQL);
					 if(!$this->common->pdf_check_date($prod['exp']))
					 {  
					 	$date_val=date('m/d/Y', strtotime($prod['exp']));
				     } elseif(strtotime($prod['exp']) =='')
					 {			
						$date_val="00/00/0000";			
					 }elseif(is_null($prod['exp']))
					 {
        				$date_val="00/00/0000";
			        }elseif($prod['exp']=='0000-00-00')
        			{
				        $date_val="00/00/0000";
			        }
					else
					{
						$date_val=date('m/d/Y', strtotime($prod['exp'])); 
					}
					foreach($col as $key => $val):
						$nomi[$key]=$prod[$key];						
					endforeach;					
					array_push($resourcearray, $nomi);				    
				 }				
				foreach($resourcearray as $resource) 
				{
					$h=0;
					foreach($col as $key => $val):
							$objPHPExcel->getActiveSheet()->setCellValue(''.$this->reportmodel->colname($h).''.$cell, $this->reportmodel->is_numericcheck($key,$resource[$key]));						
					$h++;
					endforeach;				
				$cell++; 
				}			
			 	$headcell = $cell;
			 	$titlecell = $cell+1;
			 	$cell = $cell+2;
			 }
			$sheetName = 'Leases_Due_to_Expire_by_Year_End_'.$ref.'.xlsx';
			// Redirect output to a client’s web browser (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$sheetName.'"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
			$objWriter->save('php://output');			

		}
		public function top50airport($year,$col)
		{
                      
//                    var_dump($ayear); exit;
			$cols=$this->reportmodel->getcols($col);
                        if($_POST['years']!="" && intval($_POST['years'])==$_POST['years'])
                        {
                            $ref = $_POST['years'];
                            $year=$ref;
                        }  else 
                         {
                         $ref=$year;
                        }                        
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")->setLastModifiedBy("Shawn Hanlon")
			->setTitle("Top ".$this->limit." Airports ".$ref)
			->setSubject("Top ".$this->limit." Airports ".$ref)
			->setDescription("Top ".$this->limit." Airports ".$ref)
			->setKeywords("Top ".$this->limit." Airports ".$ref)
			->setCategory("Top ".$this->limit." Airports ".$ref);
			$val='Top '.$this->limit.' Airports '.$ref;
			// Style for the Main heading
			$BGstyleArray = array(
			'fill'  => array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
					'argb' => 'F2F2F2'
					)
				)
			);
			$styleArray = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('hex' => '000'),
					'size'  => 12,
					'name'  => 'Verdana'
				));
			$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);
				// Style for the column titles
				$heading = array(
					'font'  => array(
						'bold'  => true,
						'color' => array('hex' => '000'),
						'size'  => 10,
						'name'  => 'Verdana'
					)
					);
				$titlestyle = array(
					'font'  => array(
						'bold'  => true,
						'color' => array('hex' => '000'),
						'size'  => 10,
						'name'  => 'Verdana'
					),
					'borders' => array(
					'bottom' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THICK,
					),
					'top' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				  )
				);				
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1');
				$h=1;
				foreach($col as $key => $val):		
					$objPHPExcel->getActiveSheet()->getColumnDimension(strtoupper($this->reportmodel->colname($h)))->setWidth($this->reportmodel->getcolwidth($key));
				$h++;
				endforeach;				
				$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle("A1:".strtoupper($this->reportmodel->colname(count($col)-1))."1")->applyFromArray($style);
				$objPHPExcel->getActiveSheet()->setCellValue('A1',$ref.' Top '.$this->limit.' Performing North American Airports');
				// Rename worksheet
				$objPHPExcel->getActiveSheet()->setTitle('Top_'.$this->limit.'_Airports_'.$ref);
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);
				$aenplaning    = '';
				$afbgrosssales = '';
				$asrgrosssales = '';
				$anggrosssales = '';
				$avg_sp 	   = '';
				$aconcessiongrosssales = '';			
				$sql="SELECT ".$cols." FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airporttotals.ayear='".$year."' AND airports.IATA!='YYJ' AND airports.IATA!='YVR'  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC LIMIT 0,".$this->limit;
				$ap_count=$this->common->CustomCountQuery($sql);
				$airportlist=$this->common->CustomQueryALL($sql);		
				$count 		= 1;
				$headcell	= 2;
				$titlecell	= 3;
				$cell 		= 4;				
				$objPHPExcel->getActiveSheet()->getStyle('A'.$headcell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$headcell)->applyFromArray($heading);
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$headcell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$headcell);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$headcell,' ');					
				$objPHPExcel->getActiveSheet()->getStyle('A'.$titlecell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$titlecell)->applyFromArray($titlestyle);
				$h=0;
//                                echo $this->db->last_query(); exit;
				foreach($col as $key => $val):					
					$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$titlecell,$this->reportmodel->coltitle($key));
				$h++;
				endforeach;
				$sumarray=array();	
		if($ap_count >0)
		{
			foreach($airportlist as $airportlist1): 
				//$all=$airportlist["afbgrosssales"]+$airportlist["asrgrosssales"]+$airportlist["anggrosssales"];
				if($count%2 == 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$cell)->applyFromArray($BGstyleArray);
				}
				$h=0;
				foreach($col as $key => $val): //$songs[$i][$key]
					$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$cell,$this->reportmodel->is_numericcheck($key,$airportlist1[$key]));
					if(is_numeric($airportlist1[$key]) && $key!="ayear")
						{
							$sumarray[$key]+=($airportlist1[$key] > 0 ? $airportlist1[$key] : '');
						} else
						{
							$sumarray[$key]="";
						}
				$h++;
				endforeach;							
				$count++;
				$cell++;
			endforeach;
		}
		$ft=0;	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$cell)->applyFromArray($heading);
		foreach($sumarray as $key => $val):	
		if($ft==0 && $sumarray[$key]=="")
		{			
			$objPHPExcel->getActiveSheet()->setCellValue($this->reportmodel->colname($ft).$cell,"Grand Total");
		} else
		{
			$objPHPExcel->getActiveSheet()->setCellValue($this->reportmodel->colname($ft).$cell,$this->reportmodel->is_numericcheck($key,$sumarray[$key]));
		}
		$ft++;
		endforeach;
		$cell = $cell+1; 
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$cell.':B'.$cell);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cell)->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell,date("Y-m-d H:i:s"));
//===================== Unset All Variables =========================================
unset($aenplaning);
unset($afbgrosssales);
unset($asrgrosssales);
unset($anggrosssales);
unset($aconcessiongrosssales);
unset($avg_sp);
//==============================================================

//==============================================================

$sheetName = 'Top_'.$this->limit.'_Airports_'.$ref.'.xlsx';
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$sheetName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
//echo $onjWriter; exit
$objWriter->save('php://output');
		} // END top50airport
		public function topIntairport($year,$col)
		{
			$cols=$this->reportmodel->getcols($col);			
			$ref = $year; 
			$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
							 ->setLastModifiedBy("Shawn Hanlon")
							 ->setTitle($ref."International Airports by Performance")
							 ->setSubject($ref."International Airports by Performance")
							 ->setDescription($ref."International Airports by Performance")
							 ->setKeywords($ref."International Airports by Performance")
							 ->setCategory($ref."International Airports by Performance");
$val='Top 50 Airports '.$ref;
// Style for the Main heading
$BGstyleArray = array(
	'fill'  => array(
			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
			'argb' => 'F2F2F2'
			)
	 	)
	);
$styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ));
$style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );
// Style for the column titles
$heading = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    )
	);
$titlestyle = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('hex' => '000'),
        'size'  => 10,
        'name'  => 'Verdana'
    ),
	'borders' => array(
	'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    ),
    'top' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
    )
  )
);


		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1');
		$h=1;
		foreach($col as $key => $val):		
			$objPHPExcel->getActiveSheet()->getColumnDimension(strtoupper($this->reportmodel->colname($h)))->setWidth($this->reportmodel->getcolwidth($key));
		$h++;
		endforeach;				
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.strtoupper($this->reportmodel->colname(count($col)-1)).'1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle("A1:".strtoupper($this->reportmodel->colname(count($col)-1))."1")->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A1',$ref.' International Airports by Performance');
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Int Airports by Perf '.$ref);
		$objPHPExcel->setActiveSheetIndex(0);		
		$aenplaning    = '';
		$afbgrosssales = '';
		$asrgrosssales = '';
		$anggrosssales = '';
		$adfgrosssales  = 0;
		$avg_sp 	   = '';
		$aconcessiongrosssales = '';
		$sql="SELECT ".$cols.",((afbgrosssales+asrgrosssales+anggrosssales+adfgrosssales)/aenplaning) myvar
				FROM airporttotals
				INNER JOIN airports ON airporttotals.aid = airports.aid
				WHERE airporttotals.adfgrosssales!=0 AND airporttotals.ayear='".$year."' OR airports.IATA='YVR' OR airports.IATA='YYJ'
				ORDER BY CAST(myvar AS DECIMAL( 28, 4 ) ) DESC LIMIT 0,50";
		
		$val=$year.' International Airports by Performance';				
		$ap_count=$this->common->CustomCountQuery($sql);
		$airportlist=$this->common->CustomQueryALL($sql);		
		$count 		= 1;
		$headcell	= 2;
		$titlecell	= 3;
		$cell 		= 4;
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:'.strtoupper($this->reportmodel->colname(count($col)-1)).'2');			
		$objPHPExcel->getActiveSheet()->getStyle('A2:'.strtoupper($this->reportmodel->colname(count($col)-1)).'2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle("A2:".strtoupper($this->reportmodel->colname(count($col)-1))."2")->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A2','(Includes duty free sales)');		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$titlecell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$titlecell)->applyFromArray($titlestyle);
		$h=0;		
		foreach($col as $key => $val):								
			$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$titlecell,$this->reportmodel->coltitle($key));
		$h++;
		endforeach;
		$sumarray=array();	
		foreach($col as $key => $val):
				$sumarray[$key]='';
		endforeach;				
		if($ap_count >0)
		{
			foreach($airportlist as $airportinfo): 				
			if($count%2 == 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$cell)->applyFromArray($BGstyleArray);				
			}			
			
			$h=0;	
			foreach($col as $key => $val): //$songs[$i][$key]
				$objPHPExcel->getActiveSheet()->setCellValue(strtoupper($this->reportmodel->colname($h)).$cell,$this->reportmodel->is_numericcheck($key,$airportinfo[$key]));
				if(is_numeric($airportinfo[$key]) && $key!="ayear")
					{
						$sumarray[$key]+=($airportinfo[$key] > 0 ? $airportinfo[$key] : '');
					}
			$h++;
			endforeach;					
			$count++; 
			$cell++;
		endforeach;
		}
		$ft=0;	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cell.':'.strtoupper($this->reportmodel->colname(count($col)-1)).''.$cell)->applyFromArray($heading);
		foreach($sumarray as $key => $val):	
			if($ft==0 && $sumarray[$key]=="")
			{			
				$objPHPExcel->getActiveSheet()->setCellValue($this->reportmodel->colname($ft).$cell,"Grand Total");
			} else
			{
				$objPHPExcel->getActiveSheet()->setCellValue($this->reportmodel->colname($ft).$cell,$this->reportmodel->is_numericcheck($key,$sumarray[$key]));
			}
			$ft++;
		endforeach;
		$cell = $cell+1; 
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$cell.':B'.$cell);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cell)->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$cell,date("Y-m-d H:i:s"));

		$sheetName = 'International_Airports_by_Performance'.$ref.'.xlsx';
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$sheetName.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");
		$objWriter->save('php://output');
		
		}		
}// ENd PDF Class