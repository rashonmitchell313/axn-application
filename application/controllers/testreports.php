<?php

class Testreports extends CI_Controller {

    public $dataarray;
    public $style;
    public $mpdf;
    public $limit;

    function __construct() {

        parent::__construct();

//        $this->common->ThisSecureArea('user');

        include_once APPPATH . 'mpdf/mpdf.php';

        $this->common->pdfstyleing();

        error_reporting(1);
    }

    public function index() {

        $id = $this->uri->segment(2);

        $report_gen = $this->common->GetAllRowWithColumn('tbl_reports_gen', '*', array('is_active' => 1, 'id' => $id), 'report_part', 'ASC');

        foreach ($report_gen as $r_part):

            $mpdf = new mPDF('utf-8', 'Letter');

            $tbl_header = "";

            $rcounter = 0;

            $class = "";

            $tbl_header1 = "style { .trs {font-family:arial;font-size:9pt;font-weight:bold; border: 1px solid #000; } 
            
                 .tds{ padding:3px 20px 3px 3px;text-align:left;border-bottom:1px solid #000;font-size:8pt;font-weight:bold;font-family:Vectora LH 55 Roman,arial; border: 1px solid #000; }
                 
                 .trs1{ background-color:#EEEEEE; border: 1px solid #000; }
                 
                 .tds1{ padding:3px 20px 3px 0;font-size:8pt;font-family:Vectora LH 55 Roman,arial; border: 1px solid #000; }
                 
                 .tds1w{width:15%}

                    }";

            $sql = "select * ,lkpcategory.category as category from outlets 
                    left join lkpcategory on lkpcategory.categoryid=outlets.categoryid
                    left join airports on airports.aid = outlets.aid WHERE airports.airport_is_active = 1 AND outlets.odeleted = 0 ORDER BY `outlets`.`oid` ASC LIMIT " . $r_part['report_id_start'] . "," . $r_part['report_id_end'];

            $val1 = 'ARN FactBook all Tenants Record';
            $val = $r_part['file_name'];

            $ap_count = $this->common->CustomCountQuery($sql);

            $res = $this->common->CustomQueryALL($sql);

            $MAX = $res[count($res) - 1]['oid'];

            $MIN = $res[0]['oid'];


//            $countquery = "select count(*) as total from outlets 
//                    left join lkpcategory on lkpcategory.categoryid=outlets.categoryid
//                    left join airports on airports.aid = outlets.aid WHERE airports.airport_is_active = 1 AND outlets.odeleted = 0 AND `outlets`.`lastmodified` > '" . $r_part['lastmodified'] . "' AND oid>=" . $MIN . " AND oid<=" . $MAX . " ORDER BY `outlets`.`oid` ASC ";
//            $p = $this->common->CustomQueryROw($countquery);
            $countquery = "select count(*) as total from outlets 
                    left join lkpcategory on lkpcategory.categoryid=outlets.categoryid
                    left join airports on airports.aid = outlets.aid WHERE airports.airport_is_active = 1 AND outlets.odeleted = 0 AND oid>=" . $MIN . " AND oid<=" . $MAX . " ORDER BY `outlets`.`oid` ASC ";
            $p = $this->common->CustomQueryROw($countquery);

            $mpdf->SetHTMLHeader('<div style="display:block;width:100%;text-align:center;margin:6px 0;font-family:Vectora LH 95 Black,arial;font-size:14pt;font-weight:bold;">  ' . $val1 . '</div>');

            if ($p['total'] > 0) {

                if ($ap_count > 0) {

                    $tbl_header.='<table width="100%" border="0" cellspacing="0" cellpadding="0"  border-collapse: collapse; style="text-align:right ">

				<tr class="trs">

				<td class="tds">IATA Code</td>

				<td class="tds">Terminal Location</td>

				<td class="tds">Product Category</td>
                                
				<td class="tds">Tenant Name</td>

				<td class="tds">Product Description</td>

				<td class="tds">Company Name</td>

				<td class="tds">#Locations</td>
                                
				<td class="tds">Square Footage</td>
                                
				<td class="tds">Lease Expires</td>

				</tr>';

                    foreach ($res as $tenant_list):
/*
                        if ($tenant_list['exp'] != "" && (!preg_match("/^[a-z\s\-\/]+$/i", $tenant_list['exp']))) {

                            $expiredate = $this->common->get_formatted_datetime($tenant_list['exp']);
                        } else {

                            $expiredate = $tenant_list['exp'];
                        }*/

                        if ($tenant_list['exp_reason'] != '') {

//                            $reasons = str_replace(array('M-T-M', 'TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $tenant_list['exp_reason']);
                            $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $tenant_list['exp_reason']);
//                            $reasons = $tenant_list['exp_reason'];

//                            if ($tenant_list['exp_date'] != '0000-00-00 00:00:00' && $tenant_list['exp_date'] != '') {
//                                $expiredate = '<b>' . $reasons . '</b><br>' . date("M d,Y H:i:s", strtotime($tenant_list['exp_date']));
//                            } 
                            
                             if ($tenant_list['exp_date'] != '0000-00-00 00:00:00' && $tenant_list['exp_date'] != '' & $tenant_list['exp_reason'] != 'M-T-M') {
                                $expiredate =  $reasons . '<br>' . date("M d,Y H:i:s", strtotime($tenant_list['exp_date']));
                            } else if (($tenant_list['exp_date'] == '0000-00-00 00:00:00' || $tenant_list['exp_date'] == '') && $tenant_list['exp_reason'] != 'M-T-M') {
                                $expiredate = $reasons;
                            }
                            
                            else {
                                $expiredate = $reasons;
                            }
                        } else {

                            if ($tenant_list['exp'] != "" && (!preg_match("/^[a-z\s\-\/]+$/i", $tenant_list['exp']))) {

                            $expiredate = $this->common->get_formatted_datetime($tenant_list['exp']);
                        } else {

                            $expiredate = $tenant_list['exp'];
                        }
                        }

                        if ($rcounter % 2 == 0) {

                            $class = '<tr>';
                        } else {

                            $class = '<tr class="trs1">';
                        }

                        $tbl_header.=$class;

                        $tbl_header.='<td class="tds1">' . $tenant_list["IATA"] . '</td>

                                    <td class="tds1">' . $tenant_list["termlocation"] . '</td>

				    <td class="tds1">' . $tenant_list["category"] . '</td>
                                        
				    <td class="tds1">' . $tenant_list["outletname"] . '</td>

				    <td class="tds1">' . $tenant_list["productdescription"] . '</td>

				    <td class="tds1">' . $tenant_list["companyname"] . '</td>

				    <td class="tds1">' . $tenant_list["numlocations"] . '</td>

				    <td class="tds1">' . number_format($tenant_list["sqft"], 2) . '</td>

				    <td class="tds1">' . $expiredate . '</td>

					</tr>';

                        $rcounter++;

                    endforeach;

                    $tbl_header.='</table>';
                } else {

                    $tbl_header = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

                    $tbl_header.='<tr><td style="font-family:verdana;color:red;">No Data Update...</td></tr></table>';
                }  // END if(count($ap_count)>0)			

                $str = preg_replace('/\s\s+/', ' ', $tbl_header);

                $mpdf->WriteHTML($tbl_header1, 1);

                $mpdf->WriteHTML($str);

                $mpdf->defaultfooterline = 0;

//                $mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '</div>');

                $mpdf->SetHTMLFooter('<div style="text-align: left; font-size: 8pt; font-style: italic;">' . date("Y-m-d H:i:s") . '&nbsp;&nbsp;&copy; ' . date("Y") . ' <i>Airport Revenue News</i></div>');


                $mpdf->Output('uploads/pdf/' . $val, "F");
            }
        endforeach;

        exit;
    }

// END all tenants report

    /*
      public function number_of_reports()
      {
      $totaltenats = $this->common->GetRecordCountWhere('outlets', array('oid > ' => 0));

      $n_e_reports = $this->common->GetRecordCountWhere('tbl_reports_gen', array('id > ' => 0));

      $numberofnewreports = $totaltenats / 450;

      if($numberofnewreports > $n_e_reports)
      {

      $newreportpart = $n_e_reports+1;

      $newpartstart = $n_e_reports * 450;

      $dataarray = array('report_name' => 'All Tenants', 'report_part' => $newreportpart, 'file_name' => $newreportpart.'.pdf', 'report_id_start' => $newpartstart, 'report_id_end' => '450', );

      $this->common->InsertInDb('tbl_reports_gen', $dataarray);
      }

      echo $numberofnewreports. ' seccess!!';

      exit;
      }
     */
}
