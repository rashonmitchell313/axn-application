<?php

class airportsearch extends CI_Controller {

    public $outputData = "";

    function __construct() {
        parent::__construct();
        $this->common->ThisSecureArea('user');
        $this->outputData['menu'] = 'frontend/include/menu';
        $this->outputData['footer'] = 'frontend/include/footer';
        $this->outputData['header'] = 'frontend/include/header';
        $this->outputData['searchaction'] = '';
    }

    /**
     * 
     * START Search Airport (Required IATA, City/State from list & ID)
     */
    public function index() {

        error_reporting(0);
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {
            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {
                $this->common->setmessage("You are not allowed to access that page.", -1);
                redirect(base_url() . max($this->session->userdata('year')));
                exit();
            }
        }

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("airports", $aa)) {

            $cat = 1;
            $user_id = $this->common->GetSessionKey('user_id');

            $CityState = $this->common->GetAllRowWithColumn('airports', 'aid,aname,IATA,acity,astate', array('airport_is_active' => 1), 'acity,astate', 'ASC');

            $IATA = $this->common->GetAllRowWithColumn('airports', 'aid,aname,IATA,acity,astate', array('airport_is_active' => 1), 'IATA', 'ASC');

            if ($this->input->post('load')) {

                if ($this->input->post('c_aid') != "") {

                    $AID = $this->input->post('c_aid');
//                $this->outputData['airport_search_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3')  AND advertising.aid='$AID'");
                } else {
                    $AID = $this->input->post('i_aid');
//                $this->outputData['airport_search_adds'] = $this->common->CustomQueryALL("SELECT * FROM advertising WHERE  (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3')  AND advertising.aid='$AID'");
                }
                $this->outputData['AID'] = $AID;

                if ($this->input->post('cat') != "") {
                    if ($this->input->post('cat') == 'C') {
                        $cat = "category = 'C'";
                    } else if ($this->input->post('cat') == 'B') {
                        $cat = "category = 'B'";
                    } else if ($this->input->post('cat') == 'A') {
                        $cat = "category = 'A'";
                    } else {
                        $cat = 1;
                    }
                }

                $cols = "*";
                $from = "airports";
                $join_array = array(
                    array('arn_lat_long', 'arn_lat_long.IATA= airports.IATA`', 'LEFT')
                );
                $where = "airports.aid =" . $AID . " AND airport_is_active = 1";
                $OrderBy = "airports.aname";
                $this->outputData['ainfo'] = $this->common->JoinTables($cols, $from, $join_array, $where, $OrderBy);

                $published_date = $this->common->GetAllRowFromTableWhere('airports', array('aid' => $AID));

                $parsed_datep = date_parse($published_date[0]['published']);

                $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];

                if ($datep > '2016/3/17') {

                    $this->outputData['airporcontactinfo'] = $this->common->JoinTable('*', 'airportcontacts', 'lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT', "aid = " . $AID . " AND adeleted = 0 ORDER BY lkpmgtresponsibility.mgtresponsibilityname");

                    $this->outputData['terminalinfo'] = $this->common->GetAllRowjoinOrderBy('terminals', 'terminalsannual', 'terminalsannual.tid=terminals.tid', 'LEFT', 'terminals.terminalabbr', 'ASC', array('aid' => $AID));

                    $join_array2 = array(
                        array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'),
                        array('terminals', 'airports.aid=terminals.aid', 'INNER'),
                        array('terminalsannual', 'terminals.tid=terminalsannual.tid', 'INNER'),
                        array('airporttotals', 'airports.aid=airporttotals.aid', 'INNER')
                    );
                    $this->outputData['aainfo'] = $this->common->JoinTables('*', $from, $join_array2, "airports.aid = '" . $AID . "'", 'airports.aid, terminals.terminalabbr ASC');

                    $this->outputData['agrosssale'] = $this->common->GetAllRowjoinOrderBy('terminals', 'terminalsannual', 'terminalsannual.tid=terminals.tid', 'LEFT', 'terminals.terminalabbr', 'ASC', array('aid' => $AID));

                    $this->outputData['listofcate'] = $this->common->CustomQueryALL('SELECT `categoryid`,`category` FROM `lkpcategory` group by `category` order by `categoryid`');
                } else {
                    $this->common->setmessage("This Airport is <b>Not Published</b> yet.", -1);
                }
            }

            $this->outputData['listoflocation'] = $CityState;

            $this->outputData['listofiata'] = $IATA;

            $this->load->view('frontend/airportsearch/viewairportsearch', $this->outputData);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }
    /**
     * 
     * END Search Airport
     */

}
