<?php

class tenants extends CI_Controller {

    public $outputData = "";

    function __construct() {

        parent::__construct();

        $this->outputData['menu'] = 'frontend/include/menu';

        $this->outputData['footer'] = 'frontend/include/footer';

        $this->outputData['header'] = 'frontend/include/header';

        $this->outputData['searchaction'] = '';
    }

    /**
     * 
     * Start Tenants View 
     */
    public function index() {

        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {

            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {

                $this->common->setmessage("You are not allowed to access that page.", -1);

                redirect(base_url() . max($this->session->userdata('year')));

                exit();
            }
        }

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("tenants", $aa)) {

            $this->common->ThisSecureArea('user');

            $this->load->view('frontend/tenants/tenants', $this->outputData);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }
    /**
     * 
     * End Tenants View 
     */

    public function IATA() {

        if ($this->input->is_ajax_request()) {

            $search = $_GET['term'];

            $tenants = $this->common->GetAllRowWithColumn1('airports', array("IATA"), array("IATA LIKE '%$search%' AND aid >" => 0), 'aid', 'ASC', 0, 5);

            $ar = array();

            foreach ($tenants as $c)
                $ar[] = $c['IATA'];

            echo json_encode($ar);

            exit();
        }
    }

    public function category() {

        if ($this->input->is_ajax_request()) {

            $search = $_GET['term'];

            $category = $this->common->GetAllRowWithColumn1('lkpcategory', array("category"), array("category LIKE '%$search%' AND categoryid >" => 0), 'categoryid', 'ASC', 0, 3);

            $ar = array();

            foreach ($category as $a)
                $ar[] = $a['category'];

            echo json_encode($ar);

            exit();
        }
    }

    public function tenantname() {

        if ($this->input->is_ajax_request()) {

            $search = $_GET['term'];

            $tenantname = $this->common->GetAllRowWithColumn1('outlets', array("outletname"), array("outletname LIKE '%$search%' AND oid >" => 0), 'oid', 'ASC', 0, 5);

            $ar = array();

            foreach ($tenantname as $a)
                $ar[] = $a['outletname'];

            echo json_encode($ar);

            exit();
        }
    }

    public function clearsearch() {



        $this->common->EmptySessionIndex('tenants_search');

        $this->common->EmptySessionIndex('exp');

        $this->common->EmptySessionIndex('sqftask');

        $this->common->EmptySessionIndex('IATA');

        $this->common->EmptySessionIndex('category');

        $this->common->EmptySessionIndex('outletname');

        $this->common->redirect(site_url('tenants'));
    }

}
