<?php

class reports extends CI_Controller {

    public $outputData = "";

    function __construct() {

        parent::__construct();


        $this->common->ThisSecureArea('user');

        $this->outputData['searchaction'] = '';

        $this->outputData['menu'] = 'frontend/include/menu';

        $this->outputData['footer'] = 'frontend/include/footer';

        $this->outputData['reportslist'] = $this->common->GetAllRowOrderBy("reports", "report_id", "asc");

        $this->outputData['header'] = 'frontend/include/header';

        $this->outputData['rsStates'] = $this->common->GetAllRowOrderBy("states", "state_code", "asc");

        $this->outputData['row_rsCountries'] = $this->common->GetAllRowOrderBy("countries", "countries_name", "asc");

        $this->outputData['ListofCompanies'] = $this->common->GetAllRowOrderBy("companies", "companyname", "asc");

        $this->outputData['ListOfAirports'] = $this->common->TableGetAllOrderByWhere("airports", "airport_is_active = 1", "aname", "asc");

        $this->outputData['listofcate'] = $this->common->GetAllRowOrderBy('lkpcategory', 'categoryid', 'ASC', 'category');

    }

    /**
     * 
     * Start View Reports Page
     */
    public function index() {

          if ($_SERVER['REMOTE_ADDR'] == '202.141.226.196') {

                    ?>
                    <pre>
                        <?php
                        // print_r($this->outputData);
                        // echo "NOIM ok custom report ";
                        // exit;
                        ?>
                    </pre>
                    <?PHP
                }
        if (($this->session->userdata('accesslevel') != 4) && $this->session->userdata('year') != "") {

            if (!in_array($this->uri->segment(0), $this->session->userdata('year'))) {

                $this->common->setmessage("You are not allowed to access that page.", -1);

                redirect(base_url() . max($this->session->userdata('year')));

                exit();
            }
        }

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("reports", $aa)) {

            if ($this->common->GetSessionKey('accesslevel') != 2) {
                
              
                $this->load->view('frontend/reports/view-reports', $this->outputData);
            } else {

                $this->load->view('frontend/reports/view-custom-reports', $this->outputData);
            }
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }
    /**
     * 
     * End View Reports Page
     */

    /**
     * 
     * Start DBE Companies
     */
    public function dbe_companies() {

        $str = $this->session->userdata('user_access');

        $lstr = strtolower(str_replace(" ", "", $str));

        $a = explode(",", $lstr);

        $aa = array_values($a);

        if (in_array("reports", $aa)) {

            $this->outputData['dbecompany'] = $this->common->TableGetAllOrderByWhere('companies', array('dbe' => 'yes', 'comp_is_active' => '1'), 'companyname', 'ASC');

            $this->load->view('frontend/reports/dbe_companies', $this->outputData);
        } else {

            $this->common->setmessage("You are not allowed to access that page.", -1);

            redirect(base_url());
        }
    }
    /**
     * 
     * End DBE Companies
     */

    public function searchreports() {

        $this->load->view('frontend/reports/search-reports', $this->outputData);
    }

}
?>