<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class email extends CI_Controller 
{
	public $EmailTemplateHeader;
	public $EmailTemplateBody;
	public $EmailTemplateFoter;
	public $EmailTemplateBlankRow;
	public $agent;
	public $ip;
	public $msg;
	public $todayis;
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->library('user_agent');
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;		
		$this->email->initialize($config);
		$this->todayis= date("l, F j, Y, g:i a");
		$this->EmailTemplateHeader.='<table width="100%" style="font-family:verdana;" cellpadding="0" cellspacing="0" border="0">';
		$this->EmailTemplateHeader.='<tr style="background:#e60000;color:white;height:138px;">';
		$this->EmailTemplateHeader.='<td colspan="3"><img src="'.base_url('fassests/images/logo.png').'"></td></tr>';				
		$this->EmailTemplateBlankRow.='<tr><td colspan="3">&nbsp;</td></tr>';		
		$this->EmailTemplateFoter.=$this->EmailTemplateBlankRow;	
		$this->EmailTemplateFoter.='<tr><td colspan="3">Thanks</td></tr>';	
		$this->EmailTemplateFoter.='<tr><td colspan="3">&nbsp;</td></tr>';	
		$this->EmailTemplateFoter.='<tr><td colspan="3">Regards,</td></tr>';	
		$this->EmailTemplateFoter.='<tr><td colspan="3">The ARN Team</td></tr></table>';
		$this->load->library('user_agent');		
		if ($this->agent->is_browser())
		{
			$this->agent = $this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
			$this->agent= $this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
			$this->agent = $this->agent->mobile();
		}
		else
		{
			$this->agent = 'Unidentified User Agent';
		}
		$this->ip = $this->input->ip_address();	
	}
	public function sendemail()
	{			
		if($this->input->is_ajax_request()) {	
			$action=$this->input->post('action');
			$emailContent=$this->common->GetSingleRowFromTableWhere('email_template',array('email_template_action'=>$action));			
			$info['fullname']=$this->input->post('fieldname');
			$info['senderemail']=$this->input->post('fieldemail');			
			$info['fieldcomments']=$this->input->post('fieldcomments');
			$info['requestIATA']=$this->input->post('requestIATA');
			$info['requestcompany']=$this->input->post('requestcompany');
			$content=$emailContent['email_template_content'];
			$searchArray=array('@fieldname','@fieldemail','@requestIATA','@requestcompany','@fieldcomments');
			$replacewith=array($info['fullname'],$info['senderemail'],$info['requestIATA'],$info['requestcompany'],$info['fieldcomments']);
			$b=str_replace($searchArray,$replacewith,$content);
			$httpref =$this->ip;			
			$httpagent = $this->agent;							
			$info['to']=$emailContent['email_template_to'];
			$info['toname']=$emailContent['email_template_to_name'];
			$info['cc']=$emailContent['email_template_cc'];
			$info['bcc']=$emailContent['email_template_bcc'];
			$info['subject']=$emailContent['email_template_subject'];			
			$info['body']=$emailContent['email_template_content'];		
			$this->EmailTemplateBody.="<tr><td colspan='3'><strong>Hi&nbsp;".$info['toname'].",</strong></td><tr>";
			$this->EmailTemplateBody.=$this->EmailTemplateBlankRow;
			$this->EmailTemplateBody.=$b;		
			$this->msg.=$this->EmailTemplateHeader;				
			$this->msg.=$this->EmailTemplateBody;			
			$this->msg.=$this->EmailTemplateFoter;
			$this->email->from($info['senderemail'], $info['fullname']);
			$this->email->to($info['to']); 
			if(isset($info['cc']) && $info['cc']!="")
			{
				$this->email->cc($info['cc']); 
			}
			if(isset($info['bcc']) && $info['bcc']!="")
			{
				$this->email->bcc($info['bcc']);
			}
			$this->email->subject($info['subject']);						
			$this->email->message($this->msg);			
			echo $this->email->send();
		} else
		{
			redirect(site_url(),'refresh');
			exit();
		} 
	}
}