<?php
class privacy extends CI_Controller
{
	public $outputData="";		
	function __construct()	
	{			
		parent::__construct();
		$this->common->ThisSecureArea('admin');			
		$this->outputData['menu']='frontend/include/menu';		
		$this->outputData['footer']='frontend/include/footer';
		$this->outputData['header']='frontend/include/header';
		
	}
	public function index()
	{
		
		$this->load->view('frontend/privacy.php',$this->outputData);
	}
}