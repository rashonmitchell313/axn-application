<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class company extends CI_Controller {



    public $dataarray;



    function __construct() {

        parent::__construct();

        $this->common->ThisSecureArea('admin');

        $this->outputData['topnav'] = 'admin/include/topnav';

        $this->outputData['leftnav'] = 'admin/include/leftnav';

        $this->outputData['breadcrumb'] = 'admin/include/breadcrumb';

        $this->outputData['footer'] = 'admin/include/footer';

        $this->outputData['header'] = 'admin/include/header';

        $this->outputData['searchaction'] = '';

        $this->outputData['rsStates'] = $this->common->GetAllRowOrderBy("states", "state_code", "asc");

        $this->outputData['row_rsCountries'] = $this->common->GetAllRowOrderBy("countries", "countries_name", "asc");

        $this->outputData['CompanyCategory'] = $this->common->GetAllRowOrderBy("tbl_company_category", "category_name", "asc");

        $this->lang->load('config', 'english');

    }



    /**

     * 

     * START Add company location Admin (Required Name, IATA, City & State)

     */

    public function add_airport_lookup() {

        if ($this->input->post('aid') && $this->input->post('aid') != "") {

            $IATA = $this->input->post('IATA');

            $aname = $this->input->post('aname');

            $acity = $this->input->post('acity');

            $astate = $this->input->post('astate');

            $acountry = $this->input->post('acountry');

            $this->load->library('form_validation');

            $this->form_validation->set_rules('IATA', 'IATA', 'trim|required|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('aname', 'Airport Name', 'trim|required|min_length[2]|max_length[100]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('acity', 'City', 'trim|required|min_length[2]|max_length[100]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('astate', 'State', 'trim|max_length[50]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('acountry', 'Country', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');



            if ($this->form_validation->run() == FALSE) {

                $this->common->setmessage("Please enter valid data." . validation_errors(), "-1");

                redirect(current_url());

                exit();

            }

            $dataarray = array('IATA' => $IATA, 'aname' => $aname, 'acity' => $acity, 'astate' => $astate, 'acountry' => $acountry);

            $this->common->UpdateRecord("lkpairportlist", array('aid' => $this->input->post('aid')), $dataarray);



            $this->common->getactivity('edit_airport_lookup','');



            $this->common->setmessage("lookup list for locations of airport has been Updates successfully.", 1);

        }

        if ($this->uri->segment(4) != "" && $this->uri->segment(4) > 0) {

            $olkpairportlist = $this->common->GetSingleRowFromTableWhere('lkpairportlist', array('aid' => $this->uri->segment(4)));

            if (count($olkpairportlist) > 0) {

                $this->outputData['lookuplistinfo'] = $olkpairportlist;

            } else {

                $this->common->redirect(site_url('admin'));

            }

        }

        //SELECT * FROM lkpairportlist WHERE flag=1 ORDER BY aname ASC

        if ($this->input->post('MM_update') != "") {

            $IATA = $this->input->post('IATA');

            $aname = $this->input->post('aname');

            $acity = $this->input->post('acity');

            $astate = $this->input->post('astate');

            $acountry = $this->input->post('acountry');

            $this->load->library('form_validation');

            $this->form_validation->set_rules('IATA', 'IATA', 'trim|required|min_length[2]|max_length[10]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('aname', 'Airport Name', 'trim|required|min_length[2]|max_length[100]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('acity', 'City', 'trim|required|min_length[2]|max_length[100]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('astate', 'State', 'trim|max_length[50]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('acountry', 'Country', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            if ($this->form_validation->run() == FALSE) {

                $this->common->setmessage("Please enter valid data." . validation_errors(), "-1");

                redirect(current_url());

                exit();

            }

            $dataarray = array('IATA' => $IATA, 'aname' => $aname, 'acity' => $acity, 'astate' => $astate, 'acountry' => $acountry);

            if ($this->common->InsertInDb('lkpairportlist', $dataarray) > 0) {



                $this->common->getactivity('insert_airport_lookup','');



                $this->common->setmessage("New Airport to the Lookup List for Companies has been added successfully.", 1);

            }

        }



        $this->load->view("admin/company/addairportlookup", $this->outputData);

    }



    /**

     * 

     * END Add company location Admin 

     */



    /**

     * 

     * START List of Companies Locations Admin (Required Category and Company data)

     */

    public function lookup_list_for_locations() {

        if ($this->input->post('cc') != "") {

            $this->session->set_userdata(array('ccity' => ''));

            $this->session->set_userdata('ccity', $this->input->post('cc'));

            $cc = $this->input->post('cc');

        } else if ($this->common->GetCurrentUserInfo('ccity') != "") {

            $cc = $this->common->GetCurrentUserInfo('ccity');

        } else {

            $cc = "Annapolis";

        }

        if ($this->input->post('search') != "") {



            $this->common->SetSessionKey(array('locationsearch' => $this->input->post('search')));

        }

        if ($this->common->GetSessionKey('locationsearch') != "") {

            $like = array('acity' => $this->common->GetSessionKey('locationsearch'));

            $orlike = array('IATA' => $this->common->GetSessionKey('locationsearch'));

            $this->outputData['total'] = $this->common->GetRecordCountLikeWhere('lkpairportlist', $like, $orlike, array('acity' => $cc, 'flag' => 1));

            $this->outputData['listofcompany'] = $this->common->

                    GetAllRowFromTableLimitOrderByLikeWhere('lkpairportlist', $this->uri->segment(3), 'IATA', 'ASC', $like, $orlike, array('acity' => $cc, 'flag' => 1));

            $this->outputData['cc'] = $cc;

            $this->outputData['listofcompanytype'] = $this->common->Selectdistinct('lkpairportlist', 'acity');

        } else {

            $this->outputData['total'] = $this->common->GetTotalFromTableWhere('lkpairportlist', array('acity' => $cc, 'flag' => 1));

            $this->outputData['listofcompany'] = $this->common->GetAllRowFromTableLimitWhere('lkpairportlist', $this->uri->segment(3), 'aname', array('acity' => $cc, 'flag' => 1));

            //echo $this->db->last_query(); 

            $this->outputData['cc'] = $cc;

            $this->outputData['listofcompanytype'] = $this->common->Selectdistinct('lkpairportlist', 'acity');

        }

        $this->load->view("admin/company/listofcompanylocation", $this->outputData);

    }



    /**

     * 

     * END List of Companies Locations Admin

     */



    /**

     * 

     * START Clear Search Companies Locations Admin

     */

    public function clearsearch2() {

        $this->common->EmptySessionIndex('locationsearch');

        $this->common->redirect(site_url('admin/company/lookup_list_for_locations'));

    }



    /**

     * 

     * END Clear Search Companies Locations Admin

     */



    /**

     * 

     * START Clear Search Companies by Categories Admin

     */

    public function clearsearch1() {

        $this->common->EmptySessionIndex('companysearch');

        $this->common->redirect(site_url('admin/company/companies_by_category'));

    }



    /**

     * 

     * END Clear Search Companies by Categories Admin

     */



    /**

     * 

     * START Companies by Categories Admin (Required Category and Company data)

     */

    public function companies_by_category() {





        if ($this->input->post('cc') != "") {

            $this->session->set_userdata(array('cc' => ''));

            $this->session->set_userdata('cc', $this->input->post('cc'));

            $cc = $this->input->post('cc');

        } else if ($this->common->GetCurrentUserInfo('cc') != "") {

            $cc = $this->common->GetCurrentUserInfo('cc');

        } else {

            $cc = "Food And beverage";

            $this->session->set_userdata(array('cc' => 'Food And beverage'));

        }

        if ($this->input->post('search') != "") {



            $this->common->SetSessionKey(array('companysearch' => $this->input->post('search')));

        }

        if ($this->common->GetSessionKey('companysearch') != "") {

            $like = array('companyname' => $this->common->GetSessionKey('companysearch'));

            $orlike = array('city' => $this->common->GetSessionKey('companysearch'));

            $this->outputData['total'] = $this->common->GetRecordCountLikeWhere('companies', $like, $orlike, array('companytype' => $cc));

            $this->outputData['listofcompany'] = $this->common->

                    GetAllRowFromTableLimitOrderByLikeWhere('companies', $this->uri->segment(4), 'companyname', 'ASC', $like, $orlike, array('companytype' => $cc));

            $this->outputData['cc'] = $cc;

            $this->outputData['listofcompanytype'] = $this->common->SelectdistinctWhere('companies', 'companytype', 'companytype != ""');

        } else {

            $this->outputData['total'] = $this->common->GetTotalFromTableWhere('companies', array('companytype' => $cc));

            $this->outputData['listofcompany'] = $this->common->GetAllRowFromTableLimitWhere('companies', $this->uri->segment(4), 'companyname', array('companytype' => $cc));

            //echo $this->db->last_query();

            $this->outputData['cc'] = $cc;

            $this->outputData['listofcompanytype'] = $this->common->SelectdistinctWhere('companies', 'companytype', 'companytype != ""');

        }
		
		/*
			if($_SERVER['SERVER_ADDR'] == '192.232.204.26'){
				echo "<pre>";
				print_r($this->outputData);
				exit();
			}*/

        $this->load->view("admin/company/companiesbycategory", $this->outputData);

    }



    /**

     * 

     * END Companies by Categories Admin

     */



    /**

     * 

     * START Edit Company Admin (Required Id)

     */

    public function edit_company() {

        if ($this->input->post('MM_update') != "") {



            $publish = $this->common->GetSingleRowWithColumn("companies", array("cid" => $this->input->post('cid')), "published, approved, comp_is_active");





            if ($this->input->post('published') != "") {



                $parsed_datep = date_parse($publish['published']);



                $datep = $parsed_datep['year'] . "/" . $parsed_datep['month'] . "/" . $parsed_datep['day'];



                if ($datep < '2016/03/17' || $publish['published'] == 0) {



                    $published = $this->common->GetCreatedTime();

                    $comp_is_active = 1;



                    $this->common->getactivity('publish_company','');



                    $parsed_datea = date_parse($publish['approved']);



                    $dateap = $parsed_datea['year'] . "/" . $parsed_datea['month'] . "/" . $parsed_datea['day'];



                    if ($dateap < '2016/03/17' || $publish['approved'] == 0) {

                        $approved = $this->common->GetCreatedTime();

                        $this->common->getactivity('approve_company','');

                    } else {

                        $approved = $publish['approved'];

                    }

                } else {

                    $published = $publish['published'];

                    $comp_is_active = $publish['comp_is_active'];

                    $approved = $publish['approved'];

                }

            } else {

                $published = '2015-03-10 18:48:11';

                $comp_is_active = 0;

                $approved = $publish['approved'];



                $this->common->getactivity('unpublish_company','');

            }





            $lastmodified = $this->common->GetCreatedTime();

            $companyname = $this->input->post('companyname');

            $companytype = $this->input->post('companytype');

            $address1 = $this->input->post('address1');

            $address2 = $this->input->post('address2');

            $city = $this->input->post('city');

            $state = $this->input->post('state');

            $zip = $this->input->post('zip');

            $country = $this->input->post('country');

            $website = $this->input->post('website');

            $phone = $this->input->post('phone');

            $fax = $this->input->post('fax');

            $companyinfo1 = $this->input->post('companyinfo');

            $companyinfo2 = str_replace(array('‘', '´', '`', '’', '‘', '“', '”'), array("'", "'", "'", "'", "'", '"', '"'), $companyinfo1);

            $companyinfo = $this->common->normalize($companyinfo2);

         

            $dataarray = array('companyname' => $companyname,

                'companytype' => $companytype,

                'address1' => $address1,

                'address2' => $address2,

                'city' => $city,

                'state' => $state,

                'zip' => $zip,

                'country' => $country,

                'website' => $website,

                'phone' => $phone,

                'fax' => $fax,

                'companyinfo' => $companyinfo,

                'published' => $published, 'comp_is_active' => $comp_is_active,

                'approved' => $approved, 'lastmodified' => $lastmodified);

            $this->load->library('form_validation');

            $this->form_validation->set_rules('companyname', 'Company Name', 'trim|required|min_length[2]|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('companytype', 'Company Type', 'trim|required|min_length[2]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('address1', 'Address', 'trim|required|min_length[2]|xss_clean');

            $this->form_validation->set_rules('city', 'City', 'trim|min_length[2]|max_length[100]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('state', 'State', 'trim|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('country', 'Country', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|min_length[2]|max_length[50]|xss_clean|alpha_numeric');

            $this->form_validation->set_rules('website', 'Website', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('fax', 'Fax', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            if ($this->form_validation->run() == FALSE) {

                $this->common->setmessage("Please enter valid data." . validation_errors(), "-1");

                redirect(current_url());

                exit();

            }

            

            $this->common->UpdateRecord('companies', array('cid' => $this->input->post('cid')), $dataarray);



            $effected1 = $this->db->affected_rows();

            if ($effected1 > 0) {

                $this->common->getactivity('edit_company','');

            }



            $this->common->setmessage("Company information has been Updated Successfully.", 1);

        }

        $this->outputData['company_info'] = $this->common->GetSingleRowFromTableWhere('companies', array('cid' => $this->uri->segment('4')));

        $this->load->view("admin/company/editcompany", $this->outputData);

    }



    /**

     * 

     * END Edit Company Admin

     */



    /**

     * 

     * START Add Company Admin (Required Name, Type & Phone#)

     */

    public function add_company() {

        if ($this->input->post('MM_update') != "") {



            if ($this->input->post('published') != "") {



                $published = '2015-03-10 18:48:11';

//                $published = $this->common->GetCreatedTime();

//                $approved = $this->common->GetCreatedTime();

//                $comp_is_active = 1;

            } else {



                $published = '2015-03-10 18:48:11';

                $comp_is_active = 1;

                $approved = 0;

            }



            $lastmodified = $this->common->GetCreatedTime();

            $companyname = $this->input->post('companyname');

            $companytype = $this->input->post('companytype');

            $address1 = $this->input->post('address1');

            $address2 = $this->input->post('address2');

            $city = $this->input->post('city');

            $state = $this->input->post('state');

            $zip = $this->input->post('zip');

            $country = $this->input->post('country');

            $website = $this->input->post('website');

            $phone = $this->input->post('phone');

            $fax = $this->input->post('fax');

            $companyinfo1 = $this->input->post('companyinfo');

            $companyinfo2 = str_replace(array('‘', '´', '`', '’', '‘', '“', '”'), array("'", "'", "'", "'", "'", '"', '"'), $companyinfo1);

            $companyinfo = $this->common->normalize($companyinfo2);

            $dataarray = array('companyname' => $companyname,

                'companytype' => $companytype,

                'address1' => $address1,

                'address2' => $address2,

                'city' => $city,

                'state' => $state,

                'zip' => $zip,

                'country' => $country,

                'website' => $website,

                'phone' => $phone,

                'fax' => $fax,

                'companyinfo' => $companyinfo,

                'published' => $published, 'comp_is_active' => $comp_is_active,

                'approved' => $approved, 'lastmodified' => $lastmodified);

            $this->load->library('form_validation');

            $this->form_validation->set_rules('companyname', 'Company Name', 'trim|required|min_length[2]|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('companytype', 'Company Type', 'trim|required|min_length[2]|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('address1', 'Address', 'trim|required|min_length[2]|xss_clean');

            $this->form_validation->set_rules('city', 'City', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[a-z0-9\s\']+$/i]');

            $this->form_validation->set_rules('state', 'State', 'trim|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('country', 'Country', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|min_length[2]|max_length[50]|xss_clean|alpha_numeric');

            $this->form_validation->set_rules('website', 'Website', 'trim|min_length[2]|max_length[50]|xss_clean|alpha_numaric');

            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            $this->form_validation->set_rules('fax', 'Fax', 'trim|min_length[2]|max_length[50]|xss_clean|regex_match[/^[0-9()\+\-]+$/]');

            if ($this->form_validation->run() == FALSE) {

                $this->common->setmessage("Please enter valid data." . validation_errors(), "-1");

                redirect(current_url());

                exit();

            }

            $this->common->InsertInDb('companies', $dataarray);



            $this->common->getactivity('insert_company','');



            $this->common->setmessage("New company has been added Successfully.", 1);

        }

        $this->load->view("admin/company/addcompany", $this->outputData);

    }



    /**

     * 

     * END Add Company Admin

     */



    /**

     * 

     * START Clear Search Company Admin

     */

    public function clearsearch() {

        $this->common->EmptySessionIndex('companysearch');

        $this->common->redirect(site_url('admin/company'));

    }



    /**

     * 

     * END Clear Search Company Admin

     */



    /**

     * 

     * START Clear Search Not Published Company Admin

     */

    public function clearsearch3() {

        $this->common->EmptySessionIndex('companynotpublishsearch');

        $this->common->redirect(site_url('admin/company/company_not_published'));

    }



    /**

     * 

     * END Clear Search Not Published Company Admin

     */



    /**

     * 

     * START List of All Published Companies Admin (Required Id)

     */

    public function index() {

        if ($this->input->post('search') != "") {



            $this->common->SetSessionKey(array('companysearch' => $this->input->post('search')));

        }

        if ($this->common->GetSessionKey('companysearch') != "") {

            $like = array('companyname' => $this->common->GetSessionKey('companysearch'));

            $orlike = array('city' => $this->common->GetSessionKey('companysearch'));

            $this->outputData['total'] = $this->common->GetRecordCountLike('companies', $like, $orlike);

            $this->outputData['listofcompany'] = $this->common->

                    GetAllRowFromTableLimitOrderByLike('companies', $this->uri->segment(2), 'companyname', 'ASC', $like, $orlike);

            //echo $this->db->last_query();	

        } else {

            $this->outputData['total'] = $this->common->GetRecordCount('companies');

            $this->outputData['listofcompany'] = $this->common->GetAllRowFromTableLimitOrderBy

                    ('companies', $this->uri->segment(3), 'companyname', 'ASC');

        }

        $this->load->view("admin/company/listofcompany", $this->outputData);

    }



    /**

     * 

     * END List of All Published Companies Admin

     */



    /**

     * 

     * START List of Not Published Companies Admin (Required Id)

     */

    public function company_not_published() {

        $this->outputData['searchaction'] = site_url('admin/company/company_not_published');

        //admin/airport/list_of_airport_not_published

        if (isset($_REQUEST['search']) && $_REQUEST['search'] != "" && $_REQUEST['search'] == 'clear') {

            $this->common->EmptySessionIndex('companynotpublishsearch');

        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('companynotpublishsearch' => $this->input->post('search')));

        }

        if ($this->common->GetSessionKey('companynotpublishsearch') != "") {

            $like = array('companyname' => $this->common->GetSessionKey('companynotpublishsearch'));

            $orlike = array('city' => $this->common->GetSessionKey('companynotpublishsearch'));

            $this->outputData['total'] = $this->common->

                    GetRecordCountLikeWhere('companies', $like, $orlike, 'published <1');

            $this->outputData['listofcompany'] = $this->common->

                    GetAllRowFromTableLimitOrderByLikeWhere('companies', $this->uri->segment(3), 'city', 'ASC', $like, $orlike, 'published <1  AND comp_is_active = 0');

        } else {

            //SELECT * FROM companies WHERE published = '0000/00/00' ORDER BY companyname ASC

            $this->outputData['total'] = $this->common->GetTotalFromTableWhere('companies', 'published <1');

            //$this->outputData['total']=$this->common->GetTotalCount('companies',array('published'=>'0000/00/00'));

            $this->outputData['listofcompany'] = $this->common->

                    GetAllRowFromTableWhereLimitOrderBy('companies', 'published <1  AND comp_is_active = 0', $this->uri->segment(4), 'companyname', 'ASC');

            //('companies',$this->uri->segment(3),'companyname','ASC');

        }

        $this->load->view("admin/company/listofcompanynotpublished", $this->outputData);

    }



    /**

     * 

     * END List of Not Published Companies Admin

     */



    /**

     * 

     * START List of Deleted Companies Admin (Required Id)

     */

    public function company_deleted() {

        $this->outputData['searchaction'] = site_url('admin/company/company_deleted');

        //admin/airport/list_of_airport_not_published

        if (isset($_REQUEST['search']) && $_REQUEST['search'] != "" && $_REQUEST['search'] == 'clear') {

            $this->common->EmptySessionIndex('companydeleted');

        }

        if ($this->input->post('search') != "") {

            $this->common->SetSessionKey(array('companydeleted' => $this->input->post('search')));

        }

        if ($this->common->GetSessionKey('companydeleted') != "") {

            $like = array('companyname' => $this->common->GetSessionKey('companydeleted'));

            $orlike = array('city' => $this->common->GetSessionKey('companydeleted'));

            $this->outputData['total'] = $this->common->

                    GetRecordCountLikeWhere('companies', $like, $orlike, 'published <1');

            $this->outputData['listofcompany'] = $this->common->

                    GetAllRowFromTableLimitOrderByLikeWhere('companies', $this->uri->segment(3), 'city', 'ASC', $like, $orlike, 'published <1  AND comp_is_active = 0');

        } else {

            $this->outputData['total'] = $this->common->GetTotalFromTableWhere('companies', 'published <1');

            $this->outputData['listofcompany'] = $this->common->

                    GetAllRowFromTableWhereLimitOrderBy('companies', 'published <1  AND comp_is_active = 0', $this->uri->segment(4), 'companyname', 'ASC');

        }

        $this->load->view("admin/company/listofdelcompany", $this->outputData);

    }



    /**

     * 

     * END List of Deleted Companies Admin

     */

}

