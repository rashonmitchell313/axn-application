<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class google extends CI_controller
{
	public function translate()
	{
		if($this->input->post('content_type')!="")
		{ 
			$home_content=$this->input->post('home_content');
			$home_content2=$this->input->post('home_content2');
			$login_content=$this->input->post('login_content');
			require_once APPPATH.'google/src/Google_Client.php';
			require_once APPPATH.'google/src/contrib/Google_TranslateService.php';
			$content_type=$this->input->post('content_type');
			$full_content=preg_split('/(?<=[.?!])\s+(?=[a-z])/i',$this->input->post($content_type));
			$main_lang=$this->input->post('lang_id');
			$client = new Google_Client();			
			$client->setApplicationName('Google Translate PHP Starter Application');
			$client->setDeveloperKey('AIzaSyC4tm2Qfp9EJA7HTOKnvI1fu9g63_wFJs8');
			$service = new Google_TranslateService($client);
			$langs = $service->languages->listLanguages();
			$translations = $service->translations->listTranslations($full_content,$main_lang);
			$field_name=array("login_content","home_content","home_content2");
			$val1="";			
			foreach($translations as $text):
				$val1.=$text[0]['translatedText'];				
			endforeach;
			if($login_content == ""){$login_content='Welcome to the ARN Online Fact Book (OFB), the premier resource for revenue data on airports in North America. This online service, like its hard copy version, contains comprehensive data on the food and beverage, specialty retail, news and gifts, duty-free, advertising, parking and car rental concessions in major airports. Plus, it provides detailed information on all of the key companies doing business in airports. All data is based on previous calendar year information. This online service comes with the added feature of regular updates on a quarterly basis at airports and concessions companies. Each time the data has been updated you will receive notice of the changes. The quarterly updates will include gross sales, passenger totals and sales per enplanement at each airport for that quarter as well as any tenant changes or contact information changes. For those unfamiliar with the figures contained within the OFB, please review the Glossary page to understand precisely the meaning of the various data points. Also, please navigate all the menu options for a variety of valuable and useful reports, such as the top-performing airports by sales per enplanement; leases due to expire within the next 18 months; as well as a snapshot of important aspects of every airport. Each of the participating airports included in the OFB have provided their information by manually entering it into the ARN customized OFB program. Each participant has the ability to review the data before submitting it for publication. Likewise, each concessions company has directly entered its own information. All participants in the OFB have been informed of the high priority placed on the accuracy of this information and ARN is not responsible for any discrepancies that may occur. ARN is grateful to all of the participants who took the time to enter their information, making this service the most valuable, one-of-a-kind resource in the airport revenue industry. As always, ARN is interested to know what you think of the new OFB. Please give us a call at 561.477.3417 or fax, or contact us at info@airportrevenuenews.com. The ARN Fact Book and Online Fact Book is published by Airport Revenue News, which also publishes the Airport Revenue News magazine and hosts the annual Airport Revenue Conference & Exhibition. For more information, please call 561.477.3417 or fax your questions to 561.228.0882. Or, visit our Web site at www.airportrevenuenews.com. If you are not already a subscriber and are interested in gaining access, you can view subscription pricing and purchase access at http://www.airportrevenuenews.com/store/';}
			if($home_content== ""){
				$home_content='Welcome to the 2014 ARN Online Fact Book (OFB), the premier resource for revenue data on airports in North America. This online service, like its hard copy version, contains comprehensive data on the food and beverage, specialty retail, news and gifts, duty-free, advertising, parking and car rental concessions in over one hundred major airports. Plus, it provides detailed information on all of the key companies doing business in airports. All data is based on calendar year 2012 information. The 2014 edition of the ARN Fact Book is expected to be available online from Summer 2014 to Summer 2015, as well as in a hard copy version. The majority of the information you are currently viewing is based on 2013 data. As of January 1st, 2015, we will be gathering information for the 2015 edition (based on 2014 data). For those unfamiliar with the figures contained within the OFB, please review the Glossary page to understand precisely the meaning of the various data points. Also, please navigate all the menu options for a variety of valuable and useful reports, such as the top-performing airports by sales per enplanement; leases due to expire within the next 18 months; as well as a snapshot of important aspects of every airport.';}
				if($home_content2== ""){
					$home_content2='Each of the participating airports included in the Online Fact Book (OFB) have provided their information by manually entering it into our proprietary OFB program. Each participant has the ability to review the data before submitting it for publication. Likewise, each concessions company has directly entered its own information. All participants in the OFB have been informed of the high priority placed on the accuracy of this information and ARN is not responsible for any discrepancies that may occur. 

ARN is grateful to all of the participants who took the time to enter their information, making this service the most valuable, one-of-a-kind resource in the airport revenue industry. 

As always, ARN is interested to know what you think of the new OFB. Please give us a call at 561.477.3417 or fax, or contact us at info@airportrevenuenews.com. 

The ARN Fact Book and Online Fact Book is published by Airport Revenue News, a division of Urban Expositions. The company also publishes the Airport Revenue News magazine and hosts the annual Airport Revenue Conference & Exhibition. For more information, please call 561.477.3417 or fax your questions to 561-228-0882. Or, visit our Web site at www.airportrevenuenews.com.';}
			$newdataarray=array('login_content'=>$login_content,'home_content'=>$home_content,'home_content2'=>$home_content2);
			$this->common->UpdateRecord('text_home',array('text_home_id >'=>0), $newdataarray);
			$this->common->UpdateRecord('text_multilang',array('language'=>$main_lang),array($content_type=>$val1));
			$this->common->setmessage('Content has been convert & save Successfully.',1);
			$this->common->redirect(site_url('admin/edit_content'));
		}
	}
}
?>