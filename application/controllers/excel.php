<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class excel extends CI_Controller {

    public $dataarray;
    public $style;
    public $mpdf;
    public $limit;

    function __construct() {

        parent::__construct();

        $this->common->ThisSecureArea('user');
        //$this->common->ThisSecureArea('admin');

        include_once APPPATH . 'Classes/PHPExcel.php';

        $this->common->pdfstyleing();

        error_reporting(1);
    }

    /**
     * 
     * Start Default function for Goto Required Report (Required Year, Type)
     */
    public function report() {
           
           $ayear = $this->input->post('areportyear');

        if ($this->uri->segment(3) != '') {


            $segment = strtolower($this->uri->segment(3));
            // echo $segment."llll";exit();
            //$ayear = $this->uri->segment(4);
            
//            switch ('total-tenants') {
            switch ($segment) {
//                case "total-tenants":
//
//                    $this->total_tenants();
//
//                    break;
                case "lease-expire":

                    $this->leaseexpire($ayear, 'TIA');

                    break;
            }
        }
      

        if ($this->uri->segment(4) > date("Y")) {
            $this->common->setmessage('Invalid URL Year.', -1);

            $this->common->redirect(site_url('reports'));

            exit;
        }

        $ayear = $this->uri->segment(4);

        if (!is_numeric($ayear)) {

            $this->common->setmessage('Invalid URL Year.', -1);

            $this->common->redirect(site_url('reports'));
        }




        if ($this->common->GetSessionKey('accesslevel') == 2 && $this->uri->segment(5) != "" && is_numeric($this->uri->segment(5)) && $this->uri->segment(5) > 0 && $this->uri->segment(5) != 50) {

            $this->limit = $this->uri->segment(5);
        } else {

            $this->limit = 50;
        }

        $segment = strtolower($this->uri->segment(3));

        switch ($segment) {

            case "advertising":

                $this->top50terminals('AD', $ayear);

                break;

            case "ratio-report":

                $this->ratioreport($ayear);

                break;

            case "food-beverage":

                $this->top50terminals('FB', $ayear);

                break;

            case "specialty-retail":

                $this->top50terminals('SR', $ayear);

                break;

            case "news-gifts":

                $this->top50terminals('NG', $ayear);

                break;

            case "duty-free":

                $this->top50terminals('DF', $ayear);

                break;

            case "passenger-services":

                $this->top50terminals('PT', $ayear);

                break;

            case "lease-expire":

                $this->leaseexpire($ayear, 'test');

                break;

            case "top-50-airports":

                $ayear = $this->uri->segment(4);

                $this->top50airport($ayear);

                break;

            case "top-int-airports":

                $ayear = $this->uri->segment(4);

                if (is_numeric($ayear)) {

                    $this->topIntairport($ayear);
                } else {

                    $this->common->setmessage('Invalid URL Year.', -1);

                    $this->common->redirect(site_url('reports'));
                }

                break;

            case "all-airports":

                $this->allairport($ayear);

                break;
            
            case "total-airports-passenger":

                $ayear = $this->uri->segment(4);

                $this->totalairportpassenger($ayear, 'TAP');

                break;

            default:

                $this->common->setmessage('Invalid URL', -1);

                $this->common->redirect(site_url('reports'));

                exit;
        }
    }
    /**
     * 
     * End Default function
     */

    /**
     * 
     * Start All Airports (Required Year)
     */
    public function allairport($year) {



        $ref = $year;

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
                ->setLastModifiedBy("Shawn Hanlon")
                ->setTitle($ref . "International Airports by Performance" . $ref)
                ->setSubject($ref . "International Airports by Performance" . $ref)
                ->setDescription($ref . "International Airports by Performance" . $ref)
                ->setKeywords($ref . "International Airports by Performance" . $ref)
                ->setCategory($ref . "International Airports by Performance" . $ref);


        // Style for the Main heading

        $BGstyleArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'F2F2F2'
                )
            )
        );

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 12,
                'name' => 'Verdana'
        ));

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        // Style for the column titles

        $heading = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            )
        );

        $titlestyle = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                )
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', $ref . ' All Airports');

        // Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('All_Airports_' . $ref);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $objPHPExcel->setActiveSheetIndex(0);

        $aenplaning = '';

        $afbgrosssales = '';

        $asrgrosssales = '';

        $anggrosssales = '';

        $avg_sp = '';

        $aconcessiongrosssales = '';
// ======================================== QUERY CHANGED AT 7/30/2018 FROM===============================================
        // $sql = "SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airports.airport_is_active = 1  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC ";
// TO BELOW QUERY
       $sql = "SELECT airports.acity,airports.IATA,airporttotals.aenplaning,(select sum(fbgrosssales) as afbgrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS afbgrosssales,(select  sum(srgrosssales) as srgrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS asrgrosssales,(select sum(dfgrosssales) as dfgrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS adfgrosssales,(select sum(nggrosssales) as nggrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS anggrosssales,(select (sum(nggrosssales) / sum(tenplaning)) + (sum(srgrosssales) / sum(tenplaning)) + (sum(fbgrosssales) / sum(tenplaning)) from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) as asalesepp

FROM airporttotals

INNER JOIN airports ON airporttotals.aid = airports.aid
WHERE airports.airport_is_active = 1 

ORDER BY CAST(asalesepp AS DECIMAL( 28, 4 ) ) DESC";

        $ap_count = $this->common->CustomCountQuery($sql);

        $airportlist = $this->common->CustomQueryALL($sql);

        $count = 1;

        $headcell = 2;

        $titlecell = 3;

        $cell = 4;



        $objPHPExcel->getActiveSheet()->getStyle('A' . $headcell . ':H' . $headcell)->applyFromArray($heading);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $headcell . ':H' . $headcell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $headcell, ' ');



        $objPHPExcel->getActiveSheet()->getStyle('A' . $titlecell . ':H' . $titlecell)->applyFromArray($titlestyle);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $titlecell, 'All Airports');

        $objPHPExcel->getActiveSheet()->setCellValue('B' . $titlecell, 'IATA');

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $titlecell, 'Enplanements');

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $titlecell, 'F&B Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $titlecell, 'Specialty Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $titlecell, 'NG Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $titlecell, 'Total Sales (Excluding DF)');

        $objPHPExcel->getActiveSheet()->setCellValue('H' . $titlecell, 'Sales E/P');



        if ($ap_count > 0) {

            foreach ($airportlist as $airportlist):

                $all = $airportlist["afbgrosssales"] + $airportlist["asrgrosssales"] + $airportlist["anggrosssales"];

                if ($count % 2 == 0) {

                    $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':H' . $cell)->applyFromArray($BGstyleArray);
                }



                $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, stripslashes($airportlist["acity"]));

                $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $airportlist["IATA"]);

                $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, number_format($airportlist["aenplaning"]));

                $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, '$' . number_format($airportlist["afbgrosssales"]));

                //$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, '$' . number_format($airportlist["asrgrosssales"]));

                $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, '$' . number_format($airportlist["anggrosssales"]));

                $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, '$' . number_format($all));

                $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, '$' . number_format($airportlist["asalesepp"], 2, '.', ''));



//                $aenplaning+=$airportlist["aenplaning"];
//
//                $afbgrosssales+=$airportlist["afbgrosssales"];
//
//                $asrgrosssales+=$airportlist["asrgrosssales"];
//
//                $anggrosssales+=$airportlist["anggrosssales"];
//
//                $aconcessiongrosssales+=$all;
//
//                $avg_sp+=$airportlist["asalesep"] / $ap_count;



                $count++;

                $cell ++;

            endforeach;
        } else {

            $cell = $cell = 15;

            //exit();

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, 'F&B dsadasdas Sales');
        }





        //Collect Total
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':B' . $cell)->applyFromArray($heading);
//
//        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $cell . ':B' . $cell);
//
//        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, 'Totals');
//
//        //Collect Total Enplanements Numbers
//
//        if ($aenplaning) {
//            $tot_aenplaning = number_format($aenplaning);
//        } else {
//            $tot_aenplaning = '';
//        }
//
//        $objPHPExcel->getActiveSheet()->getStyle('C' . $cell)->applyFromArray($heading);
//
//        $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $tot_aenplaning);
//
//        //Collect Total F&B Total Sales
//
//        if ($afbgrosssales) {
//            $fb_afbgrosssales = '$' . number_format($afbgrosssales);
//        } else {
//            $fb_afbgrosssales = '';
//        }
//
//        $objPHPExcel->getActiveSheet()->getStyle('D' . $cell)->applyFromArray($heading);
//
//        $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, $fb_afbgrosssales);
//
//        //Collect Specialty Total Sales
//
//        if ($asrgrosssales) {
//            $sr_asrgrosssales = '$' . number_format($asrgrosssales);
//        } else {
//            $sr_asrgrosssales = '';
//        }
//
//        $objPHPExcel->getActiveSheet()->getStyle('E' . $cell)->applyFromArray($heading);
//
//        $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, $sr_asrgrosssales);
//
//        //Collect NG Total Sales
//
//        if ($anggrosssales) {
//            $ng_anggrosssales = '$' . number_format($anggrosssales);
//        } else {
//            $ng_anggrosssales = '';
//        }
//
//        $objPHPExcel->getActiveSheet()->getStyle('F' . $cell)->applyFromArray($heading);
//
//        $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, $ng_anggrosssales);
//
//        //Collect Total Sales (Excluding DF)
//
//        if ($aconcessiongrosssales) {
//            $to_aconcessiongrosssales = '$' . number_format($aconcessiongrosssales);
//        } else {
//            $to_aconcessiongrosssales = '';
//        }
//
//        $objPHPExcel->getActiveSheet()->getStyle('G' . $cell)->applyFromArray($heading);
//
//        $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, $to_aconcessiongrosssales);
//
//        //Collect Total Sales E/P
//
//        if ($avg_sp) {
//            $to_avg_sp = '$' . number_format($avg_sp, 2, '.', '');
//        } else {
//            $to_avg_sp = '';
//        }
//
//        $objPHPExcel->getActiveSheet()->getStyle('H' . $cell)->applyFromArray($heading);
//
//        $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, 'AVG=' . $to_avg_sp);
        //Add Date In the Footer

        $cell = $cell + 5;

        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell)->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, date("Y-m-d H:i:s"));

//===================== Unset All Variables =========================================
//        unset($aenplaning);
//
//        unset($afbgrosssales);
//
//        unset($asrgrosssales);
//
//        unset($anggrosssales);
//
//        unset($aconcessiongrosssales);
//
//        unset($avg_sp);
//==============================================================
//==============================================================



        $sheetName = 'All_Airports_' . $ref . '.xlsx';

// Redirect output to a client�s web browser (Excel2007)

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $sheetName . '"');

        header('Cache-Control: max-age=0');

// If you're serving to IE 9, then the following may be needed

        header('Cache-Control: max-age=1');



// If you're serving to IE over SSL, then the following may be needed

        header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

        $objWriter->save('php://output');
    }
    /**
     * 
     * End All Airports
     */
//  Start Old All Ariports function
    /*
      public function allairport($year) {





      $ref = $year;

      $objPHPExcel = new PHPExcel();

      $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
      ->setLastModifiedBy("Shawn Hanlon")
      ->setTitle($ref . "International Airports by Performance")
      ->setSubject($ref . "International Airports by Performance")
      ->setDescription($ref . "International Airports by Performance")
      ->setKeywords($ref . "International Airports by Performance")
      ->setCategory($ref . "International Airports by Performance");

      $val = 'Top 50 Airports ' . $ref;

      // Style for the Main heading

      $BGstyleArray = array(
      'fill' => array(
      'type' => PHPExcel_Style_Fill::FILL_SOLID,
      'startcolor' => array(
      'argb' => 'F2F2F2'
      )
      )
      );

      $styleArray = array(
      'font' => array(
      'bold' => true,
      'color' => array('hex' => '000'),
      'size' => 12,
      'name' => 'Verdana'
      ),
      'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
      );

      $style = array(
      'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
      );

      // Style for the column titles

      $heading = array(
      'font' => array(
      'bold' => true,
      'color' => array('hex' => '000'),
      'size' => 10,
      'name' => 'Verdana'
      )
      );

      $titlestyle = array(
      'font' => array(
      'bold' => true,
      'color' => array('hex' => '000'),
      'size' => 10,
      'name' => 'Verdana'
      ),
      'borders' => array(
      'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
      ),
      'top' => array(
      'style' => PHPExcel_Style_Border::BORDER_THICK,
      )
      )
      );

      $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);

      $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

      $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

      $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

      $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

      $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

      $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

      $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);





      //  $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);

      // $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);

      // Rename worksheet

      $objPHPExcel->getActiveSheet()->setTitle('All Airports ' . $ref);



      //(Includes duty free sales)
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet

      $objPHPExcel->setActiveSheetIndex(0);

      $count = 1;

      $headcell = 2;

      $titlecell = 3;

      $cell = 4;



      //        $sql = "SELECT * FROM airports WHERE `airport_is_active` = 1 LIMIT 0,25";
      $sql = "SELECT * FROM airports WHERE `airport_is_active` = 1 ";

      $ap_count = $this->common->CustomCountQuery($sql);

      $airportlist = $this->common->CustomQueryALL($sql);

      if ($ap_count > 0) {

      foreach ($airportlist as $airport):

      $join_array = array(
      array('airports', 'airports.aid = airportcontacts.aid', 'LEFT'),
      array('lkpmgtresponsibility', 'lkpmgtresponsibility.mgtresponsibility=airportcontacts.mgtresponsibility', 'LEFT')
      );

      $outlets = $this->common->JoinTable('*', 'outlets', 'lkpcategory', 'outlets.categoryid=lkpcategory.categoryid', 'LEFT', "aid = " . $airport['aid'] . " AND lkpcategory.categoryid !='' ORDER BY lkpcategory.categoryid");

      $contacts = $this->common->JoinTables('*', 'airportcontacts', $join_array, array('airportcontacts.aid' => $airport['aid']), "");

      $terminals = $this->common->JoinTable('*', 'terminals', 'terminalsannual', 'terminalsannual.tid = terminals.tid', 'LEFT', array('aid' => $airport['aid']));

      $annual = $this->common->JoinTable('*', 'airportsannual', 'airports', 'airports.aid = airportsannual.aid', 'LEFT', array('airportsannual.aid' => $airport['aid'], 'airport_is_active' => 1));

      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $count . ':I' . $count);

      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . ($count + 1) . ':I' . ($count + 1));

      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . ($count + 2) . ':I' . ($count + 2));

      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $count . ':I' . $count, 'A' . ($count + 1) . ':I' . ($count + 1));

      $objPHPExcel->getActiveSheet()->getStyle('A' . $count . ':H' . $count)->applyFromArray($styleArray);

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 1) . ':H' . ($count + 1))->applyFromArray($styleArray);

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 2) . ':H' . ($count + 2))->applyFromArray($styleArray);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . $count, $airport['acity'] . "," . $airport['astate']);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 1), $airport['aname']);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 2), $airport['IATA']);

      $counter = 0;



      if (count($contacts) > 0) {

      foreach ($contacts as $contact):

      if ($counter % 2 == 0) {

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 3) . ':C' . ($count + 3))->applyFromArray($styleArray);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 3), $contact['mgtresponsibilityname']);



      //$objPHPExcel->getActiveSheet()->getStyle('A'.($count+4))->applyFromArray($titlestyle);



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 4), 'Contact');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 5), 'Title');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 6), 'Company');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 7), 'Address');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 8), 'Phone');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 9), 'Fax');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 10), 'Email');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 11), 'Website');



      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 4))->applyFromArray($BGstyleArray);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 4), $contact['alname'] . ', ' . $contact['afname']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 5), $contact['atitle']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 6), substr($contact['acompany'], 0, 20));

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 7), $contact['aaddress1'] . ' ' . $contact['accity'] . ' ' . $contact['acstate'] . ' ' . $contact['aczip'] . ' ' . $contact['accountry']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 8), $contact['aphone'] . ' ' . $contact['aext']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 9), $contact['afax']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 10), $contact['aemail']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 11), $contact['awebsite']);
      } else if ($counter % 2 != 0) {

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 3) . ':G' . ($count + 3))->applyFromArray($styleArray);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 3), $contact['mgtresponsibilityname']);





      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 4), 'Contact');



      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 5), 'Title');



      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 6), 'Company');



      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 7), 'Address');



      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 8), 'Phone');



      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 9), 'Fax');



      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 10), 'Email');



      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 11), 'Website');



      $objPHPExcel->getActiveSheet()->getStyle('G' . ($count + 4))->applyFromArray($BGstyleArray);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 4), $contact['alname'] . ', ' . $contact['afname']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 5), $contact['atitle']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 6), substr($contact['acompany'], 0, 20));

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 7), $contact['aaddress1'] . ' ' . $contact['accity'] . ' ' . $contact['acstate'] . ' ' . $contact['aczip'] . ' ' . $contact['accountry']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 8), $contact['aphone'] . ' ' . $contact['aext']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 9), $contact['afax']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 10), $contact['aemail']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 11), $contact['awebsite']);
      }



      $count+=5;

      $counter++;

      endforeach;
      }



      $count+=4;

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 3), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 4) . ':H' . ($count + 4))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 4), 'Airport Info');



      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 5), 'Airport Configuration:');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 6))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 6), 'Concessions Mgt. Type:');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 7), 'Expansion Planned:');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 8))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 8), 'Add Sq. Ft. ');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 9))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 9), 'Completion Date:');



      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 5), 'Terminal/Concourses');

      $objPHPExcel->getActiveSheet()->getStyle('G' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 5), 'Abbr.');

      $objPHPExcel->getActiveSheet()->getStyle('H' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('H' . ($count + 5), 'Dominant Airline');



      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 5), $airport['configuration']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 6), $airport['mgtstructure']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 7), $airport['texpansionplanned']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 8), $airport['addsqft']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 9), $airport['completedexpdate']);

      foreach ($terminals as $term):

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 6), $term['terminalname']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 6), $term['terminalabbr']);

      $objPHPExcel->getActiveSheet()->setCellValue('H' . ($count + 6), $term['tdominantairline']);

      $count++;

      endforeach;





      $count+=4;





      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 4) . ':G' . ($count + 4))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 4), 'Passenger Traffic');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 5), 'Terminal');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 5), 'Total');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 5), '+/-%');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 5), 'Deplanning');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 5), 'Enplanning');

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 5), 'EP Domestic');

      $objPHPExcel->getActiveSheet()->getStyle('G' . ($count + 5))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 5), 'Ep Int\'l');

      $ttpasstraffic = 0;

      $ttadeplaning = 0;

      $ttaenplaning = 0;

      $ttaepdomestic = 0;

      $tttaepintl = 0;



      foreach ($terminals as $term):

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 6), $term['terminalabbr']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 6), $term['tpasstraffic']);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 6), $term['tpasstrafficcompare']);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 6), $term['tdeplaning']);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 6), $term['tenplaning']);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 6), $term['tepdomestic']);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 6), $term['tepintl']);

      $count++;

      $ttpasstraffic+=$term['tpasstraffic'];

      $ttadeplaning+=$term['tdeplaning'];

      $ttaenplaning+=$term['tenplaning'];

      $ttaepdomestic+=$term['tepdomestic'];

      $tttaepintl+=$term['tepintl'];

      endforeach;

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 7), 'Total');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 7), '$' . number_format($ttpasstraffic));

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 7), '');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 7), '$' . number_format($ttadeplaning));

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 7), '$' . number_format($ttaenplaning));

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 7), '$' . number_format($ttaepdomestic));

      $objPHPExcel->getActiveSheet()->getStyle('G' . ($count + 7))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 7), '$' . number_format($tttaepintl));



      ////////////////Airport Percentage //////////////////

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 8), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 9) . ':G' . ($count + 9))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 9), 'Airport Percentages');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 10))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 10), 'Pre/Post Security');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 10))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 10), 'Business to Leisure Ratio');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 10))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 10), 'O&D Transfer');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 10))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 10), 'Average Dwell Time');

      $pre = $annual['presecurity'] . '/' . $annual[0]['postsecurity'];

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 11), $pre);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 11), $annual[0]['ratiobusleisure']);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 11), $annual[0]['ondtransfer']);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 11), $annual[0]['avgdwelltime']);



      ////////////////////////////////////////////////////



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 12), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 13) . ':J' . ($count + 13))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 13), 'Airportwide Info');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 14), 'Parking');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 14), 'Short');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 14), 'Long');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 14), 'Economy');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 14), 'Valet');



      $objPHPExcel->getActiveSheet()->getStyle('G' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 14), 'Car Rentals');

      $objPHPExcel->getActiveSheet()->getStyle('H' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('H' . ($count + 14), 'Agencies');

      $objPHPExcel->getActiveSheet()->getStyle('I' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('I' . ($count + 14), 'Gross Rev');

      $objPHPExcel->getActiveSheet()->getStyle('J' . ($count + 14))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('J' . ($count + 14), 'Gross Renta');





      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 15), 'Hourly');

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 15), @number_format($annual[0]['hourlyshort'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 15), @number_format($annual[0]['hourlylong'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 15), @number_format($annual[0]['hourlyeconomy'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 15), @number_format($annual[0]['hourlyvalet'], 2));



      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 15), 'Car Rental On Site');

      $objPHPExcel->getActiveSheet()->setCellValue('H' . ($count + 15), $annual[0]['carrentalagenciesonsite'], 2);

      $objPHPExcel->getActiveSheet()->setCellValue('I' . ($count + 15), @number_format($annual[0]['carrentalrevonsite'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('J' . ($count + 15), @number_format($annual[0]['carrentalrevtoaironsite'], 2));







      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 16), 'Daily');

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 16), @number_format($annual[0]['dailyshort'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 16), @number_format($annual[0]['dailylong'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 16), @number_format($annual[0]['dailyeconomy'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 16), @number_format($annual[0]['dailyvalet'], 2));



      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 16), 'Car Rental Off Site');

      $objPHPExcel->getActiveSheet()->setCellValue('H' . ($count + 16), $annual[0]['carrentalagenciesoffsite'], 2);

      $objPHPExcel->getActiveSheet()->setCellValue('I' . ($count + 16), @number_format($annual[0]['carrentalrevoffsite'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('J' . ($count + 16), @number_format($annual[0]['carrentalrevtoairoffsite'], 2));



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 17), '# Spaces');

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 17), @number_format($annual[0]['spacesshort'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 17), @number_format($annual[0]['spaceslong'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 17), @number_format($annual[0]['spaceseconomy'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 17), @number_format($annual[0]['spacesvalet'], 2));



      $objPHPExcel->getActiveSheet()->getStyle('G' . ($count + 17))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 17), 'Total Cars Rented');

      $objPHPExcel->getActiveSheet()->getStyle('H' . ($count + 17))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('H' . ($count + 17), $annual[0]['totalcarsrented'], 2);

      $objPHPExcel->getActiveSheet()->setCellValue('I' . ($count + 17), '');

      $objPHPExcel->getActiveSheet()->setCellValue('J' . ($count + 17), '0');



      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 18))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 18), 'Parking Revenue');

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 18), @number_format($annual[0]['parkingrev'], 2));

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 18))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 18), 'Total Spaces');

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 18), @number_format($annual[0]['parkingspaces'], 2));



      $objPHPExcel->getActiveSheet()->getStyle('G' . ($count + 18))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('G' . ($count + 18), 'Car Rental Sq. Ft');

      $objPHPExcel->getActiveSheet()->getStyle('H' . ($count + 18))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('H' . ($count + 18), $annual[0]['carrentalsqft'], 2);

      $objPHPExcel->getActiveSheet()->setCellValue('I' . ($count + 18), '');

      $objPHPExcel->getActiveSheet()->setCellValue('J' . ($count + 18), '0');



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 19), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 20))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 20), '');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 20))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 20), 'Revenue');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 20))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 20), 'Rev. to Airport');





      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 21), 'Passenger Services');

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 21), @number_format($annual[0]['passservicesrev'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 21), @number_format($annual[0]['passservicesrevtoair'], 2));



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 22), 'Advertising');

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 22), @number_format($annual[0]['advertisingrev'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 22), @number_format($annual[0]['advertisingrevtoair'], 2));



      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 23), 'Currency Exchange');

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 23), @number_format($annual[0]['currencyexrev'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 23), @number_format($annual[0]['currencyexrevtoair'], 2));





      /////////////////////////////food/bearage start/////////////////////////////////////

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 24), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 25) . ':G' . ($count + 25))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 25), 'Concession Totals - Terminal Breakdowns ( Food/Beverage, Specialty Retail, News /Gifts Only )');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 26))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 26), 'Food/Beverage');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 27))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 27), 'Terminal');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 27))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 27), 'Gross Sales');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 27))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 27), 'Sales/EP');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 27))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 27), 'Rent Rev to Airport');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 27))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 27), 'Rent/EP');

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 27))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 27), 'Current Sq. Ft.');

      $tfbgrosssales = 0;

      $tfbsalesep = 0;

      $tfbrentrev = 0;

      $tfbrentep = 0;

      $tfbcurrsqft = 0;



      foreach ($terminals as $term):

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 28), $term['terminalabbr']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 28), @number_format($term['fbgrosssales'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 28), @number_format($term['fbsalesep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 28), @number_format($term['fbrentrev'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 28), @number_format($term['fbrentep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 28), @number_format($term['fbcurrsqft'], 2));



      $tfbgrosssales+=$term['fbgrosssales'];

      $tfbsalesep+=$term['fbsalesep'];

      $tfbrentrev+=$term['fbrentrev'];

      $tfbrentep+=$term['fbrentep'];

      $tfbcurrsqft+=$term['fbcurrsqft'];

      $count++;

      endforeach;

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 29))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 29), 'Total');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 29))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 29), '$' . number_format($tfbgrosssales));

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 29))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 29), '$' . number_format($tfbsalesep));

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 29))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 29), '$' . number_format($tfbrentrev));

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 29))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 29), '$' . number_format($tfbrentep));

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 29))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 29), '$' . number_format($tfbcurrsqft));

      //////////////////////////////food/berage end ///////////////////////////////////
      ////////////////////////////////Specialty Retail//////////////////////////////////

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 30), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 31) . ':F' . ($count + 31))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 31), 'Specialty Retail');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 32))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 32), 'Terminal');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 32))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 32), 'Gross Sales');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 32))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 32), 'Sales/EP');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 32))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 32), 'Rent Rev to Airport');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 32))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 32), 'Rent/EP');

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 32))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 32), 'Current Sq. Ft.');

      $tfbgrosssales = 0;

      $tfbsalesep = 0;

      $tfbrentrev = 0;

      $tfbrentep = 0;

      $tfbcurrsqft = 0;



      foreach ($terminals as $term):

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 33), $term['terminalabbr']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 33), @number_format($term['srgrosssales'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 33), @number_format($term['srsalesep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 33), @number_format($term['srrentrev'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 33), @number_format($term['srrentep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 33), @number_format($term['srcurrsqft'], 2));



      $tfbgrosssales+=$term['srgrosssales'];

      $tfbsalesep+=$term['srsalesep'];

      $tfbrentrev+=$term['srrentrev'];

      $tfbrentep+=$term['srrentep'];

      $tfbcurrsqft+=$term['srcurrsqft'];

      $count++;

      endforeach;

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 33))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 33), 'Total');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 33))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 33), '$' . number_format($tfbgrosssales));

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 33))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 33), '$' . number_format($tfbsalesep));

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 33))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 33), '$' . number_format($tfbrentrev));

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 33))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 33), '$' . number_format($tfbrentep));

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 33))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 33), '$' . number_format($tfbcurrsqft));

      /////////////////////////////////////////////////////////////////
      ////////////////////////////////News/Gifts Retail//////////////////////////////////

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 34), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 35) . ':F' . ($count + 35))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 35), 'News/Gifts Retail');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 36))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 36), 'Terminal');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 36))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 36), 'Gross Sales');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 36))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 36), 'Sales/EP');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 36))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 36), 'Rent Rev to Airport');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 36))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 36), 'Rent/EP');

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 36))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 36), 'Current Sq. Ft.');

      $tfbgrosssales = 0;

      $tfbsalesep = 0;

      $tfbrentrev = 0;

      $tfbrentep = 0;

      $tfbcurrsqft = 0;



      foreach ($terminals as $term):

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 37), $term['terminalabbr']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 37), @number_format($term['nggrosssales'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 37), @number_format($term['ngsalesep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 37), @number_format($term['ngrentrev'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 37), @number_format($term['ngrentep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 37), @number_format($term['ngcurrsqft'], 2));



      $tfbgrosssales+=$term['nggrosssales'];

      $tfbsalesep+=$term['ngsalesep'];

      $tfbrentrev+=$term['ngrentrev'];

      $tfbrentep+=$term['ngrentep'];

      $tfbcurrsqft+=$term['ngcurrsqft'];

      $count++;

      endforeach;

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 38))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 38), 'Total');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 38))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 38), '$' . number_format($tfbgrosssales));

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 38))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 38), '$' . number_format($tfbsalesep));

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 38))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 38), '$' . number_format($tfbrentrev));

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 38))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 38), '$' . number_format($tfbrentep));

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 38))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 38), '$' . number_format($tfbcurrsqft));

      /////////////////////////////////News/Gifts Retail ends////////////////////////////////
      ////////////////////////////////Duty Free Retail//////////////////////////////////

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 39), '');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 40) . ':F' . ($count + 40))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 40), 'Duty Free Retail');

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 41))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 41), 'Terminal');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 41))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 41), 'Gross Sales');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 41))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 41), 'Sales/EP');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 41))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 41), 'Rent Rev to Airport');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 41))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 41), 'Rent/EP');

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 41))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 41), 'Current Sq. Ft.');

      $tfbgrosssales = 0;

      $tfbsalesep = 0;

      $tfbrentrev = 0;

      $tfbrentep = 0;

      $tfbcurrsqft = 0;



      foreach ($terminals as $term):

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 42), $term['terminalabbr']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 42), @number_format($term['dfgrosssales'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 42), @number_format($term['dfsalesep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 42), @number_format($term['dfrentrev'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 42), @number_format($term['dfrentep'], 2));

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 42), @number_format($term['dfcurrsqft'], 2));



      $tfbgrosssales+=$term['dfgrosssales'];

      $tfbsalesep+=$term['dfsalesep'];

      $tfbrentrev+=$term['dfrentrev'];

      $tfbrentep+=$term['dfrentep'];

      $tfbcurrsqft+=$term['dfcurrsqft'];

      $count++;

      endforeach;

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 43))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 43), 'Total');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 43))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 43), '$' . number_format($tfbgrosssales));

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 43))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 43), '$' . number_format($tfbsalesep));

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 43))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 43), '$' . number_format($tfbrentrev));

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 43))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 43), '$' . number_format($tfbrentep));

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 43))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 43), '$' . number_format($tfbcurrsqft));

      /////////////////////////////////Duty Free Retail ends////////////////////////////////
      ////////////////////////////////Concession Tenant Details (2010)l//////////////////////////////////

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 44), '');



      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 45) . ':F' . ($count + 45))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 45), 'Concession Tenant Details (2010)');

      $old = 'Food/Beverage';

      $c = 0;



      foreach ($outlets as $o):

      if ($old != $o['category'] && $c != 0) {

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 48) . ':F' . ($count + 48))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 48), $o['category'] . 'Tenant (Company)');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 48))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 48), 'Terminal');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 48))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 48), '# locations');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 48))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 48), '# Sq. Ft.');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 48))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 48), 'Expires');

      $old = $o['category'];

      $count+=2;
      }

      if ($c == 0) {

      $objPHPExcel->getActiveSheet()->getStyle('A' . ($count + 46) . ':F' . ($count + 46))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 46), 'Food/Beverage Tenant (Company)');

      $objPHPExcel->getActiveSheet()->getStyle('B' . ($count + 46))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 46), 'Product Description');

      $objPHPExcel->getActiveSheet()->getStyle('C' . ($count + 46))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 46), '# Terminal');

      $objPHPExcel->getActiveSheet()->getStyle('D' . ($count + 46))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 46), '# locations');

      $objPHPExcel->getActiveSheet()->getStyle('E' . ($count + 46))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 46), '# Sq. Ft.');

      $objPHPExcel->getActiveSheet()->getStyle('F' . ($count + 46))->applyFromArray($titlestyle);

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + 46), 'Expires.');
      }









      $objPHPExcel->getActiveSheet()->setCellValue('A' . ($count + 47), $o['outletname']);

      $objPHPExcel->getActiveSheet()->setCellValue('B' . ($count + 47), $o['productdescription']);

      $objPHPExcel->getActiveSheet()->setCellValue('C' . ($count + 47), $o['termlocation']);

      $objPHPExcel->getActiveSheet()->setCellValue('D' . ($count + 47), $o['numlocations']);

      $objPHPExcel->getActiveSheet()->setCellValue('E' . ($count + 47), $o['sqft']);

      if ($o['exp'] != '') {

      $objPHPExcel->getActiveSheet()->setCellValue('F' . ($count + $start), date('m/d/Y', strtotime($o['exp'])));
      }

      $count++;



      $c++;

      endforeach;

      ///////////////////////////////Concession Tenant Details (2010)////////////////////////////////

      $count++;

      $count++;

      $count++;

      $count+=2;

      $count++;

      $count++;

      ///

      $count+=50;

      $count++;

      $cell++;

      endforeach;
      } else {

      $cell = $cell = 15;

      //exit();

      $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, 'F&B dsadasdas Sales');
      }



      $sheetName = 'All_Airports_' . $ref . '.xlsx';

      // Redirect output to a client�s web browser (Excel2007)

      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

      header('Content-Disposition: attachment;filename="' . $sheetName . '"');

      header('Cache-Control: max-age=0');

      // If you're serving to IE 9, then the following may be needed

      header('Cache-Control: max-age=1');



      // If you're serving to IE over SSL, then the following may be needed

      header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

      header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

      header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

      header('Pragma: public'); // HTTP/1.0



      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

      //$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

      $objWriter->save('php://output');
      }
     */
//  End Old All Ariports function

    /**
     * 
     * Start Ratio Report (Required Year)
     */
    public function ratioreport($year) {

        $airport = $this->common->SelectdistinctWhere('airports', '(`IATA`),aid as aid,acity', array('airport_is_active' => 1));
        
        $val = $year . ' Ratios Report';

        $ref = $year;

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
                ->setLastModifiedBy("Shawn Hanlon")
                ->setTitle($val)
                ->setSubject($val)
                ->setDescription($val)
                ->setKeywords($val)
                ->setCategory($val);

        $BGstyleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('argb' => 'F2F2F2')));

        $styleArray = array('font' => array('bold' => true, 'color' => array('hex' => '000'), 'size' => 12, 'name' => 'Verdana'));

        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));

        // Style for the column titles

        $heading = array('font' => array('bold' => true, 'color' => array('hex' => '000'), 'size' => 10, 'name' => 'Verdana'));

//        $titlestyle = array('font' => array('bold' => true, 'color' => array('hex' => '000'), 'size' => 10, 'name' => 'Verdana'),
//            'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK,), 'top' =>
//                array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $titlestyle = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                )
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);

        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Ratios Report ' . $ref);

        $tbl = '';

        $tbl2 = '';

        $tbl_header = '';

        $tbl_header2 = '';

        $titlecell = 2;

        $rcounter = 0;

        $objPHPExcel->getActiveSheet()->getStyle('A' . $titlecell . ':E' . $titlecell)->applyFromArray($titlestyle);

        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'IATA');

        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'City');

        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Business to Leisure ratio');

        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Pre/Post Security ratio');

        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Avg Dwell Time (Minutes)');

        $objPHPExcel->getActiveSheet()->getStyle('A2:E1')->applyFromArray($styleArray);

        $cell = 3;

        foreach ($airport as $AP):

            $join_array2 = array(array('airportsannual', 'airports.aid=airportsannual.aid', 'LEFT'));

            $select = "airports.IATA,airports.acity,airportsannual.presecurity,airportsannual.postsecurity,airportsannual.ratiobusleisure,airportsannual.avgdwelltime";

            $where = array('airports.aid' => $AP['aid'], 'airportsannual.ayear' => $year);

            $info = $this->common->JoinTables($select, 'airports', $join_array2, $where, "airports.aid");

//            foreach ($info as $in):
//
//                $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, $in["IATA"]);
//
//                $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $in["acity"]);
//
//                $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $in["presecurity"]);
//
//                $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, $in["postsecurity"]);
//
//                $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, $in["ratiobusleisure"]);
//
//                $cell++;
//
//            endforeach;

            foreach ($info as $in):

                if ($in["ratiobusleisure"] != '' && $in["ratiobusleisure"] != '0') {
                    $ratiobusleisure = $in["ratiobusleisure"];
                } else {
                    $ratiobusleisure = '0/0';
                }

                if ($in["presecurity"] != '') {
                    $presecurity = $in["presecurity"];
                } else {
                    $presecurity = '0';
                }

                if ($in["postsecurity"] != '') {
                    $postsecurity = $in["postsecurity"];
                } else {
                    $postsecurity = '0';
                }

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, $in["IATA"]);

                $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $in["acity"]);

                $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $ratiobusleisure);

                $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, $presecurity . '/' . $postsecurity);

                $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, $in["avgdwelltime"]);

                $cell++;

            endforeach;

        endforeach;

        $objPHPExcel->getActiveSheet()->setTitle('Ratios_Report_' . $ref);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $objPHPExcel->setActiveSheetIndex(0);

        $sheetName = $year . 'Ratios_Report.xlsx';

        // Redirect output to a client�s web browser (Excel2007)

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $sheetName . '"');

        header('Cache-Control: max-age=0');

        // If you're serving to IE 9, then the following may be needed

        header('Cache-Control: max-age=1');



        // If you're serving to IE over SSL, then the following may be needed

        header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

        header('Pragma: public'); // HTTP/1.0



        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        //$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

        $objWriter->save('php://output');
    }
    /**
     * 
     * End Ratio Report
     */

    /**
     * 
     * Start Top 50 Terminals by Category (Required Category, Year)
     * Category {Food/Beverage, Specialty Retail, News/Gifts, Duty Free & Passenger Services} 
     */
    public function top50terminals($reportfor, $year) {

        $objPHPExcel = new PHPExcel();

        if ($reportfor == 'SR') {

            $reportfor = 'SR';

            $reptitle = "Specialty Retail";
        }

        if ($reportfor == 'DF') {

            $reportfor = 'DF';

            $reptitle = "Duty Free";
        }

        if ($reportfor == 'NG') {

            $reportfor = 'NG';

            $reptitle = "News & Gifts";
        }

        if ($reportfor == 'FB') {

            $reportfor = 'FB';

            $reptitle = "Food and Beverage";
        }

        if ($reportfor == 'PT') {

            $reportfor = 'PT';

            $reptitle = "Passenger Traffic";
        }

        if ($reportfor == "AD") {

            $reptitle = 'Advertising';
        }

        $airport = $this->common->SelectdistinctWhere(airports, '(`IATA`),aid as aid,acity', array('airport_is_active' => 1));

        $users = array();

        $TCurrentSqFt = 0;

        $TGrossSale = 0;

        $TSalesEp = 0;

        $TRentRev = 0;

        foreach ($airport as $AP) {

//              $airportinfo=$this->common->GetSingleRowFromTableWhere('airports',array('aid'=>$AP['aid'], 'airport_is_active'=>1));    

            $airportinfo = $this->common->GetSingleRowFromTableWhere('airports', array('aid' => $AP['aid']));

            $tl = $this->common->JoinTable('*', 'terminals', 'terminalsannual', 'terminalsannual.tid=terminals.tid', 'LEFT', "aid = '" . $AP['aid'] . "' AND terminalsannual.tyear='" . $year . "' ORDER BY terminals.tid");

            $fbgrosssalestotal = 0;

            $srgrosssalestotal = 0;

            $nggrosssalestotal = 0;

            $fbrentrevtotal = 0;

            $srrentrevtotal = 0;

            $ngrentrevtotal = 0;

            $indi_terms_list = "";

            if (count($tl) > 0) {

                foreach ($tl as $_) {

                    if ($reportfor == 'PT') {

                        $CurrentSqFt = $_["dfcurrsqft"];

                        $GrossSale = $_["dfgrosssales"];

                        $SalesEp = $_["tpasstraffic"];

                        $RentRev = $_["dfrentrev"];
                    }

                    if ($reportfor == 'DF') {

                        $CurrentSqFt = $_["dfcurrsqft"];

                        $GrossSale = $_["dfgrosssales"];

                        $SalesEp = $_["dfsalesep"];

                        $RentRev = $_["dfrentrev"];
                    }

                    if ($reportfor == 'SR') {

                        $CurrentSqFt = $_["srcurrsqft"];

                        $GrossSale = $_["srgrosssales"];

                        $SalesEp = $_["srsalesep"];

                        $RentRev = $_["srrentrev"];
                    }

                    if ($reportfor == 'FB') {

                        $CurrentSqFt = $_["fbcurrsqft"];

                        $GrossSale = $_["fbgrosssales"];

                        $SalesEp = $_["fbsalesep"];

                        $RentRev = $_["fbrentrev"];
                    }

                    if ($reportfor == 'NG') {

                        $CurrentSqFt = $_["ngcurrsqft"];

                        $GrossSale = $_["nggrosssales"];

                        $SalesEp = $_["ngsalesep"];

                        $RentRev = $_["ngrentrev"];
                    }

                    $TCurrentSqFt+=$CurrentSqFt;

                    $TGrossSale+=$GrossSale;

                    $TSalesEp+=$SalesEp;

                    $TRentRev+=$RentRev;

                    $users[] = array('IATA_ID' => $airportinfo['IATA'], 'Terminal_Abbr' => $AP["acity"],
                        'Terminal_Name' => $_["terminalname"],
                        'CurrentSqFt' => $CurrentSqFt, 'GrossSales' => $GrossSale,
                        'SalesEP' => $SalesEp, 'GrossRentals' => $RentRev);
                }
            }
        } // end foreach ($airport as $AP) 
        //print_r($users);

        $songs = $this->common->subval_sort($users, 'SalesEP');

        $val = 'Top ' . $this->limit . ' TERMNLS ' . $reptitle;

        $ref = date("Y");

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
                ->setLastModifiedBy("Shawn Hanlon")
                ->setTitle($val)
                ->setSubject($val)
                ->setDescription($val)
                ->setKeywords($val)
                ->setCategory($val);

//$val='Top 50 Airports '.$ref;
// Style for the Main heading

        $BGstyleArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'F2F2F2'
                )
            )
        );

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 12,
                'name' => 'Verdana'
        ));

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        // Style for the column titles

        $heading = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            )
        );

        $titlestyle = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                )
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', $year . ' Top ' . $this->limit . ' Terminals by ' . $reptitle);

        // Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Top_' . $this->limit . '_TRMNL_' . str_replace(" ", "_", $reptitle));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $objPHPExcel->setActiveSheetIndex(0);

        $aenplaning = '';

        $afbgrosssales = '';

        $asrgrosssales = '';

        $anggrosssales = '';

        $avg_sp = '';

        $aconcessiongrosssales = '';

        $airterm = "SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airports.IATA!='YYJ' AND airports.IATA!='YVR' AND airports.airport_is_active = 1 ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC LIMIT 0," . $this->limit - 1;

        //$airtermQuery=mysql_query($airterm);
        //$ap_count=mysql_num_rows($airtermQuery);

        $count = 1;

        $headcell = 2;

        $titlecell = 3;

        $cell = 4;

        $objPHPExcel->getActiveSheet()->getStyle('A' . $headcell . ':H' . $headcell)->applyFromArray($heading);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $headcell . ':H' . $headcell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $headcell, ' ');

        $objPHPExcel->getActiveSheet()->getStyle('A' . $titlecell . ':H' . $titlecell)->applyFromArray($titlestyle);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $titlecell, 'IATA ID');

        $objPHPExcel->getActiveSheet()->setCellValue('B' . $titlecell, 'City');

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $titlecell, 'Terminal Name');

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $titlecell, 'Current Sq.Ft.');

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $titlecell, 'Gross Sales');

        if ($reportfor != 'PT') {

            $objPHPExcel->getActiveSheet()->setCellValue('F' . $titlecell, 'Sales/EP');
        } else {

            $objPHPExcel->getActiveSheet()->setCellValue('F' . $titlecell, 'Passenger Traffic');
        }

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $titlecell, 'Gross Rentals');

        //$objPHPExcel->getActiveSheet()->setCellValue('H'.$titlecell, 'Sales E/P');

        $counter = 0;

        for ($i = count($songs) - 1; $i >= 0; $i--) {

            //$airportlist=mysql_fetch_assoc($airtermQuery); 
            //$all=$airportlist["afbgrosssales"]+$airportlist["asrgrosssales"]+$airportlist["anggrosssales"];

            if ($counter % 2 == 0) {

                $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':G' . $cell)->applyFromArray($BGstyleArray);
            }



            $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, stripslashes($songs[$i]["IATA_ID"]));

            $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $songs[$i]["Terminal_Abbr"]);

            $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $songs[$i]["Terminal_Name"]);

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, number_format($songs[$i]["CurrentSqFt"], 2));

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, "$" . number_format($songs[$i]["GrossSales"], 2));

            if ($reportfor != 'PT') {

                $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, "$" . number_format($songs[$i]["SalesEP"], 2));
            } else {

                $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, "" . number_format($songs[$i]["SalesEP"], 2));
            }

            $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, "$" . number_format($songs[$i]["GrossRentals"], 2));

            //$objPHPExcel->getActiveSheet()->setCellValue('H'.$cell, '$'.number_format($airportlist["asalesep"], 2, '.', ''));

            $nomiCurrentSqFt+=$songs[$i]["CurrentSqFt"];

            $nomiGrossSales+=$songs[$i]["GrossSales"];

            $nomiSalesEP+=$songs[$i]["SalesEP"];

            $nomiGrossRentals+=$songs[$i]["GrossRentals"];

            if ($counter == $this->limit - 1) {

                break;
            }

            $counter++;

            $cell ++;
        }

        $cell = $cell + 2;

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, "Total");

        $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, "");

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, "");

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, number_format($nomiCurrentSqFt, 2));

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, "$" . number_format($nomiGrossSales, 2));

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, "$" . number_format($nomiSalesEP, 2));

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, "$" . number_format($nomiGrossRentals, 2));

//===================== Unset All Variables =========================================

        unset($aenplaning);

        unset($afbgrosssales);

        unset($asrgrosssales);

        unset($anggrosssales);

        unset($aconcessiongrosssales);

        unset($avg_sp);

//==============================================================
//==============================================================



        $sheetName = $year . '_Top_' . $this->limit . '_Terminals_by_' . str_replace(" ", "_", $reptitle) . '.xlsx';

// Redirect output to a client�s web browser (Excel2007)

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $sheetName . '"');

        header('Cache-Control: max-age=0');

// If you're serving to IE 9, then the following may be needed

        header('Cache-Control: max-age=1');



// If you're serving to IE over SSL, then the following may be needed

        header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

        header('Pragma: public'); // HTTP/1.0



        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

        $objWriter->save('php://output');





        exit;
    }
    /**
     * 
     * End Top 50 Terminals by Category
     */

// END OF FUnction

    /**
     * 
     * Start Lease Expiration (Required Year) 
     */
    public function leaseexpire($ayear, $type) {
        


        $ref = $ayear;

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
                ->setLastModifiedBy("Shawn Hanlon")
                ->setTitle("Leases Due to Expire by Year-End" . $ref)
                ->setSubject("Leases Due to Expire by Year-End" . $ref)
                ->setDescription("Leases Due to Expire by Year-End" . $ref)
                ->setKeywords("Leases Due to Expire by Year-End" . $ref)
                ->setCategory("Leases Due to Expire by Year-End" . $ref);



        $val = 'Leases Due to Expire by Year-End ' . $ref;


        $styleArray = array('font' => array('bold' => true, 'color' => array('hex' => '000'), 'size' => 12,
                'name' => 'Verdana'
        ));


        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );



        // Style for the column titles

        $heading = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            )
        );

        $titlestyle = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                )
            )
        );



        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(55);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);

        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Leases Due to Expire by Year-End ' . $ref);



        // Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Leases_Due_Expire_' . $ref);


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $objPHPExcel->setActiveSheetIndex(0);






        $tbl = '';



        $tbl2 = '';



        $tbl_header = '';



        $tbl_header2 = '';



//        $listsql = "select distinct category from lkpcategory"; 

//        $listsql2 = "SELECT count(oid) FROM `outlets` ";

        // $list=mysql_query($listsql);
        // $count=mysql_query($listsql2);
        //$nr_of_users = mysql_fetch_array($count);



        $headcell = 2;

        $titlecell = 3;

        $cell = 4;
          
          // echo "string";exit();
        $list = $this->common->Selectdistinct('lkpcategory', 'category');
        
        
//        $listcount = $this->common->CustomCountQuery($listsql);

//        $nr_of_users = $this->common->CustomQueryALL($listsql2);
        
        $nr_of_users = $this->common->GetRecordCount('outlets');

        foreach ($list as $cmpnyterms) {

            //$cmpnyterms = mysql_fetch_assoc($list);           

            $catid = $cmpnyterms['category'];

//            ECHO $catid; exit;
            //echo $headcell."----".$catid."<br />";
            //echo $titlecell."----Outlet Name/Description(Company)<br />";
             // echo "dddddddd";
             // echo "<pre>";
             // print_r($ref);exit();
             // foreach ($ref as $key => $value) {
             //    echo $value.'oooo';exit();
             //     # code...
             // }

            

             $first = reset($ref);
             $last = end($ref);
             

            
            

            

//            $products = $this->common->CustomQueryALL("select DATE_FORMAT(exp,'%m-%d-%y') as expf, outlets.* from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid where lkpcategory.category ='" . $catid . "' and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and outlets.exp LIKE '%" . $ref . "%' order by DATE_FORMAT(exp,'%m-%d-%y') ASC ");

            $products = $this->common->CustomQueryALL("select outlets.companyname, outlets.aid, outlets.oid, outlets.numlocations, outlets.sqft, outlets.outletname, airports.acity, "
                    . "
                            case when length(date(str_to_date(outlets.exp,'%Y-%m-%d'))) is not NULL then str_to_date(outlets.exp,'%Y-%m-%d')
                                when length(date(str_to_date(outlets.exp,'%b %d %Y'))) is not NULL then str_to_date(outlets.exp,'%b %d %Y')
                                when length(date(str_to_date(outlets.exp,'%m/%d/%Y'))) is not NULL then str_to_date(outlets.exp,'%m/%d/%Y')
                                when length(date(str_to_date(outlets.exp,'%Y/%m/%d'))) is not NULL then str_to_date(outlets.exp,'%Y/%m/%d')
                            end as exp"
                    . " from outlets left join lkpcategory ON outlets.categoryid = lkpcategory.categoryid 
                        left join   airports ON outlets.aid =   airports.aid 
                    where lkpcategory.category =" . $this->db->escape($catid) . " and outlets.outletname!='3 win' and outlets.outletname!='Shop 2' and airports.airport_is_active = 1 and outlets.exp BETWEEN'" . $this->db->escape_like_str($first) . "' AND  '" . $this->db->escape_like_str($last) . "' order by exp, acity ASC ");


            $objPHPExcel->getActiveSheet()->getStyle('A' . $headcell . ':E' . $headcell)->applyFromArray($heading);

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D' . $headcell . ':E' . $headcell);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $headcell, $catid);

            $datetime = date("Y/m/d H:i:s");

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $headcell, $datetime);

            $objPHPExcel->getActiveSheet()->getStyle('D' . $headcell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->getStyle('A' . $titlecell . ':E' . $titlecell)->applyFromArray($titlestyle);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $titlecell, 'Outlet Name/Description(Company)');

            $objPHPExcel->getActiveSheet()->setCellValue('B' . $titlecell, 'Airport Location');

            $objPHPExcel->getActiveSheet()->setCellValue('C' . $titlecell, '#');

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $titlecell, 'Sq. Ft.');

            $objPHPExcel->getActiveSheet()->setCellValue('E' . $titlecell, 'Expires');

            if (count($products) > 0)
                $resourcearray = array();

            $indexarray = array();

            foreach ($products as $prod) {

//                print_r(count($prod)); exit;

                $aiddd = $prod['aid'];

                //$citystrSQL = "SELECT acity FROM airports WHERE aid='$aiddd'";            

//                $acity_row = $this->common->GetSingleRowWithColumn('airports', array('aid' => $aiddd), 'acity');
                $acity_row = $prod['acity'] ;

                if (!$this->common->check_date($prod['exp'])) {

                    $date_val = date('m/d/Y', strtotime($prod['exp']));
                } elseif (strtotime($prod['exp']) == '') {

                    $date_val = "00/00/0000";
                } elseif (is_null($prod['exp'])) {

                    $date_val = "00/00/0000";
                } elseif ($prod['exp'] == '0000-00-00') {

                    $date_val = "00/00/0000";
                } else {

                    $date_val = date('m/d/Y', strtotime($prod['exp']));
                }

                if (empty($prod['companyname'])) {

                    $compny = '';
                } else {

                    $compny = ' (' . $prod['companyname'] . ')';
                }

                //Get character information

                $date = $date_val;

                $outletname = stripslashes($prod['outletname']) . $compny;

//                if (isset($acity_row['acity']) && $acity_row['acity'] != "") {
//
//                    $acity_row = $acity_row['acity'];
//                } else {
//
//                    $acity_row = '';
//                }

                $numlocations = $prod['numlocations'];

                $sqft = number_format($prod['sqft']);

                //and anything else you want to add goes here, of course

                array_push($resourcearray, array('outletname' => $outletname, 'acity' => $acity_row, 'numlocations' => $numlocations, 'sqft' => $sqft, 'date' => $date,));

                array_push($indexarray, $date);

                //array_push($indexarray, $numlocations);
            }

            foreach ($resourcearray as $resource) {



                //echo stripslashes($resource['outletname'])."==".$cell."<br/>";

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, stripslashes($resource['outletname']));

                $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $resource['acity']);

                $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, strtoupper(substr($resource['numlocations'], 0, 1)));

                $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, $resource['sqft']);

                $objPHPExcel->getActiveSheet()->getStyle('D' . $cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, $resource['date']);



                $cell ++;
            }



            $headcell = $cell;

            $titlecell = $cell + 1;

            $cell = $cell + 2;
        }

//echo '<pre>';
//print_r($objPHPExcel); exit;
        //==============================================================
        //==============================================================
        //==============================================================





        $sheetName = 'Leases_Due_to_Expire_by_Year_End_' . $ref . '.xlsx';

        // Redirect output to a client�s web browser (Excel2007)

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $sheetName . '"');

        header('Cache-Control: max-age=0');

        // If you're serving to IE 9, then the following may be needed

        header('Cache-Control: max-age=1');



        // If you're serving to IE over SSL, then the following may be needed

        header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

        header('Pragma: public'); // HTTP/1.0



        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        //$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

        $objWriter->save('php://output');
    }
    /**
     * 
     * End Lease Expiration
     */

    /**
     * 
     * Start Top 50 Airports (Required Year) 
     */
    public function top50airport($year) {

        //echo $this->limit;
        //exit;

        $ref = $year;

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")->setLastModifiedBy("Shawn Hanlon")
                ->setTitle("Top " . $this->limit . " Airports " . $ref)
                ->setSubject("Top " . $this->limit . " Airports " . $ref)
                ->setDescription("Top " . $this->limit . " Airports " . $ref)
                ->setKeywords("Top " . $this->limit . " Airports " . $ref)
                ->setCategory("Top " . $this->limit . " Airports " . $ref);

        $val = 'Top ' . $this->limit . ' Airports ' . $ref;

        // Style for the Main heading

        $BGstyleArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'F2F2F2'
                )
            )
        );

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 12,
                'name' => 'Verdana'
        ));

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        // Style for the column titles

        $heading = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            )
        );

        $titlestyle = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                )
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', $ref . ' Top ' . $this->limit . ' Performing North American Airports');

        // Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Top_' . $this->limit . '_Airports_' . $ref);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $objPHPExcel->setActiveSheetIndex(0);

        $aenplaning = '';

        $afbgrosssales = '';

        $asrgrosssales = '';

        $anggrosssales = '';

        $avg_sp = '';

        $aconcessiongrosssales = '';

        // $sql = "SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airporttotals.ayear=" . $this->db->escape($year) . " AND airports.IATA!='YYJ' AND airports.IATA!='YVR' AND airports.airport_is_active = 1  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC LIMIT 0," . $this->limit;
		
		 $sql = "SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesepp as asalesep from (SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,(select (sum(nggrosssales) / sum(tenplaning)) + (sum(srgrosssales) / sum(tenplaning)) + (sum(fbgrosssales) / sum(tenplaning)) from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) as asalesepp FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airporttotals.ayear=" . $this->db->escape($year) . " AND airports.IATA!='YYJ' AND airports.IATA!='YVR' AND airports.airport_is_active = 1  ORDER BY cast(asalesepp AS decimal( 38, 10 )) DESC LIMIT 0,". $this->limit.") as derived_table";
		
        $ap_count = $this->common->CustomCountQuery($sql);

        $airportlist = $this->common->CustomQueryALL($sql);

        $count = 1;

        $headcell = 2;

        $titlecell = 3;

        $cell = 4;



        $objPHPExcel->getActiveSheet()->getStyle('A' . $headcell . ':H' . $headcell)->applyFromArray($heading);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $headcell . ':H' . $headcell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $headcell, ' ');



        $objPHPExcel->getActiveSheet()->getStyle('A' . $titlecell . ':H' . $titlecell)->applyFromArray($titlestyle);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $titlecell, 'Top ' . $this->limit . ' Airports');

        $objPHPExcel->getActiveSheet()->setCellValue('B' . $titlecell, 'IATA');

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $titlecell, 'Enplanements');

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $titlecell, 'F&B Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $titlecell, 'Specialty Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $titlecell, 'NG Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $titlecell, 'Total Sales (Excluding DF)');

        $objPHPExcel->getActiveSheet()->setCellValue('H' . $titlecell, 'Sales E/P');



        if ($ap_count > 0) {

            foreach ($airportlist as $airportlist):

                $all = $airportlist["afbgrosssales"] + $airportlist["asrgrosssales"] + $airportlist["anggrosssales"];

                if ($count % 2 == 0) {

                    $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':H' . $cell)->applyFromArray($BGstyleArray);
                }



                $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, stripslashes($airportlist["acity"]));

                $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $airportlist["IATA"]);

                $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, number_format($airportlist["aenplaning"]));

                $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, '$' . number_format($airportlist["afbgrosssales"]));

                //$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, '$' . number_format($airportlist["asrgrosssales"]));

                $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, '$' . number_format($airportlist["anggrosssales"]));

                $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, '$' . number_format($all));

                $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, '$' . number_format($airportlist["asalesep"], 2, '.', ''));



                $aenplaning+=$airportlist["aenplaning"];

                $afbgrosssales+=$airportlist["afbgrosssales"];

                $asrgrosssales+=$airportlist["asrgrosssales"];

                $anggrosssales+=$airportlist["anggrosssales"];

                $aconcessiongrosssales+=$all;

                $avg_sp+=$airportlist["asalesep"] / $ap_count;



                $count++;

                $cell ++;

            endforeach;
        } else {

            $cell = $cell = 15;

            //exit();

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, 'F&B dsadasdas Sales');
        }





        //Collect Total

        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':B' . $cell)->applyFromArray($heading);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $cell . ':B' . $cell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, 'Totals');

        //Collect Total Enplanements Numbers

        if ($aenplaning) {
            $tot_aenplaning = number_format($aenplaning);
        } else {
            $tot_aenplaning = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('C' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $tot_aenplaning);

        //Collect Total F&B Total Sales

        if ($afbgrosssales) {
            $fb_afbgrosssales = '$' . number_format($afbgrosssales);
        } else {
            $fb_afbgrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('D' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, $fb_afbgrosssales);

        //Collect Specialty Total Sales

        if ($asrgrosssales) {
            $sr_asrgrosssales = '$' . number_format($asrgrosssales);
        } else {
            $sr_asrgrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('E' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, $sr_asrgrosssales);

        //Collect NG Total Sales

        if ($anggrosssales) {
            $ng_anggrosssales = '$' . number_format($anggrosssales);
        } else {
            $ng_anggrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('F' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, $ng_anggrosssales);

        //Collect Total Sales (Excluding DF)

        if ($aconcessiongrosssales) {
            $to_aconcessiongrosssales = '$' . number_format($aconcessiongrosssales);
        } else {
            $to_aconcessiongrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('G' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, $to_aconcessiongrosssales);

        //Collect Total Sales E/P

        if ($avg_sp) {
            $to_avg_sp = '$' . number_format($avg_sp, 2, '.', '');
        } else {
            $to_avg_sp = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('H' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, 'AVG=' . $to_avg_sp);

        //Add Date In the Footer

        $cell = $cell + 5;

        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell)->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, date("Y-m-d H:i:s"));

//===================== Unset All Variables =========================================

        unset($aenplaning);

        unset($afbgrosssales);

        unset($asrgrosssales);

        unset($anggrosssales);

        unset($aconcessiongrosssales);

        unset($avg_sp);

//==============================================================
//==============================================================



        $sheetName = 'Top_' . $this->limit . '_Airports_' . $ref . '.xlsx';

// Redirect output to a client�s web browser (Excel2007)

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $sheetName . '"');

        header('Cache-Control: max-age=0');

// If you're serving to IE 9, then the following may be needed

        header('Cache-Control: max-age=1');



// If you're serving to IE over SSL, then the following may be needed

        header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

        $objWriter->save('php://output');
    }
    /**
     * 
     * End Top 50 Airports
     */

    /**
     * 
     * Start Top International Airports (Required Year) 
     */
    public function topIntairport($year) {



        $ref = $year;

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")
                ->setLastModifiedBy("Shawn Hanlon")
                ->setTitle($ref . "International Airports by Performance")
                ->setSubject($ref . "International Airports by Performance")
                ->setDescription($ref . "International Airports by Performance")
                ->setKeywords($ref . "International Airports by Performance")
                ->setCategory($ref . "International Airports by Performance");

        $val = 'Top International Airports ' . $ref;

// Style for the Main heading

        $BGstyleArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'F2F2F2'
                )
            )
        );

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 12,
                'name' => 'Verdana'
        ));

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

// Style for the column titles

        $heading = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            )
        );

        $titlestyle = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                )
            )
        );





        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');





        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);



        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', $ref . ' International Airports by Performance');



// Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Int Airports by Perf ' . $ref);



//(Includes duty free sales)
// Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $objPHPExcel->setActiveSheetIndex(0);



        $aenplaning = '';

        $afbgrosssales = '';

        $asrgrosssales = '';

        $anggrosssales = '';

        $adfgrosssales = 0;

        $avg_sp = '';

        $aconcessiongrosssales = '';

//        $sql = "SELECT *,((afbgrosssales+asrgrosssales+anggrosssales+adfgrosssales)/aenplaning) myvar
//
//              FROM airporttotals
//
//              INNER JOIN airports ON airporttotals.aid = airports.aid
//
//              WHERE airporttotals.adfgrosssales!=0  AND (airporttotals.ayear='" . $year . "' OR airports.IATA='YVR' OR airports.IATA='YYJ') AND airports.airport_is_active = 1 
//
//              ORDER BY CAST(myvar AS DECIMAL( 28, 4 ) ) DESC LIMIT 0,50";

//===============================changed at 7/26/2018============================
        //from this query
        // $sql = "SELECT *,((afbgrosssales+asrgrosssales+anggrosssales+adfgrosssales)/aenplaning) myvar

        //         FROM airporttotals

        //         INNER JOIN airports ON airporttotals.aid = airports.aid

        //         WHERE airporttotals.adfgrosssales!=0  OR airports.IATA='YVR' OR airports.IATA='YYJ' 

        //         ORDER BY CAST(myvar AS DECIMAL( 28, 4 ) ) DESC LIMIT 0,50";

        // to under written query...


$sql = "SELECT airports.acity,airports.IATA,airporttotals.aenplaning,(select sum(fbgrosssales) as afbgrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS afbgrosssales,(select  sum(srgrosssales) as srgrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS asrgrosssales,(select sum(dfgrosssales) as dfgrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS adfgrosssales,(select sum(nggrosssales) as nggrosssales from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) AS anggrosssales,(select (sum(nggrosssales) / sum(tenplaning)) + (sum(srgrosssales) / sum(tenplaning)) + (sum(fbgrosssales) / sum(tenplaning)) from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) as asalesepp

FROM airporttotals

INNER JOIN airports ON airporttotals.aid = airports.aid
WHERE (airporttotals.adfgrosssales!=0 OR airports.IATA='YVR' OR airports.IATA='YYJ') and (select (sum(nggrosssales) / sum(tenplaning)) + (sum(srgrosssales) / sum(tenplaning)) + (sum(fbgrosssales) / sum(tenplaning)) from terminals left join terminalsannual on terminalsannual.tid=terminals.tid where aid=airports.aid ORDER BY terminals.terminalabbr ASC) > 6.19

ORDER BY CAST(asalesepp AS DECIMAL( 28, 4 ) ) DESC";
        $val = $year . ' International Airports by Performance';

        $ap_count = $this->common->CustomCountQuery($sql);

        $airportlist = $this->common->CustomQueryALL($sql);





        $count = 1;

        $headcell = 2;

        $titlecell = 3;

        $cell = 4;

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:H2');

        $objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A2:H2")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A2', '(Includes duty free sales)');







        $objPHPExcel->getActiveSheet()->getStyle('A' . $titlecell . ':I' . $titlecell)->applyFromArray($titlestyle);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $titlecell, '');

        $objPHPExcel->getActiveSheet()->setCellValue('B' . $titlecell, 'IATA');

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $titlecell, 'Enplanements');

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $titlecell, 'F&B Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $titlecell, 'Specialty Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $titlecell, 'NG Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $titlecell, 'DF Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('H' . $titlecell, 'Total Sales');

        $objPHPExcel->getActiveSheet()->setCellValue('I' . $titlecell, 'Sales E/P');

        if ($ap_count > 0) {

            foreach ($airportlist as $airportlist):

                $all = $airportlist["afbgrosssales"] + $airportlist["asrgrosssales"] + $airportlist["anggrosssales"] + $airportlist["adfgrosssales"];



                $salesep = number_format($airportlist['asalesepp'],2);



                if ($count % 2 == 0) {

                    $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':H' . $cell)->applyFromArray($BGstyleArray);
                }



                $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, stripslashes($airportlist["acity"]));

                $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $airportlist["IATA"]);

                $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $airportlist["aenplaning"]);

                $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, '$'.$airportlist["afbgrosssales"]);

                //$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, '$'.$airportlist["asrgrosssales"]);



                $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, '$'.$airportlist["anggrosssales"]);

                $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, '$'.$airportlist["adfgrosssales"]);

                $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, '$'.$all);

//                $objPHPExcel->getActiveSheet()->setCellValue('I' . $cell, $airportlist["asalesep"]);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $cell, '$'.number_format($salesep, 2, '.', ''));



                $aenplaning+=$airportlist["aenplaning"];

                $afbgrosssales+=$airportlist["afbgrosssales"];

                $asrgrosssales+=$airportlist["asrgrosssales"];

                $anggrosssales+=$airportlist["anggrosssales"];

                $adfgrosssales+=$airportlist["adfgrosssales"];

                $aconcessiongrosssales+=$all;

                $avg_sp+=$salesep / $ap_count;

                $count++;

                $cell ++;

            endforeach;
        } else {

            $cell = $cell = 15;

            //exit();

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, 'F&B dsadasdas Sales');
        }



        //Collect Total

        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':B' . $cell)->applyFromArray($heading);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $cell . ':B' . $cell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, 'Totals');

        //Collect Total Enplanements Numbers

        if ($aenplaning) {
            $tot_aenplaning = number_format($aenplaning);
        } else {
            $tot_aenplaning = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('C' . $cell)->applyFromArray($heading);

//        $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, '=SUM(C4:C' . ($cell - 1) . ')');

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $tot_aenplaning);

        //Collect Total F&B Total Sales

        if ($afbgrosssales) {
            $fb_afbgrosssales = '$' . number_format($afbgrosssales);
        } else {
            $fb_afbgrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('D' . $cell)->applyFromArray($heading);

//        $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, '=SUM(D4:D' . ($cell - 1) . ')');

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, $fb_afbgrosssales);

        //Collect Specialty Total Sales

        if ($asrgrosssales) {
            $sr_asrgrosssales = '$' . number_format($asrgrosssales);
        } else {
            $sr_asrgrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('E' . $cell)->applyFromArray($heading);

//        $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, '=SUM(E4:E' . ($cell - 1) . ')');

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, $sr_asrgrosssales);

        //Collect NG Total Sales

        if ($anggrosssales) {
            $ng_anggrosssales = '$' . number_format($anggrosssales);
        } else {
            $ng_anggrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('F' . $cell)->applyFromArray($heading);

//        $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, '=SUM(F4:F' . ($cell - 1) . ')');

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, $ng_anggrosssales);

        //Collect Total Sales (Excluding DF)

        if ($adfgrosssales) {
            $to_adfgrosssales = '$' . number_format($adfgrosssales);
        } else {
            $to_adfgrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('G' . $cell)->applyFromArray($heading);

//        $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, '=SUM(G4:G' . ($cell - 1) . ')');

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, $to_adfgrosssales);

        //Collect Total Sales E/P



        if ($aconcessiongrosssales) {
            $to_aconcessiongrosssales = '$' . number_format($aconcessiongrosssales);
        } else {
            $to_aconcessiongrosssales = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('H' . $cell)->applyFromArray($heading);

//        $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, '=SUM(H4:H' . ($cell - 1) . ')');

        $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, $to_aconcessiongrosssales);


        if ($avg_sp) {
            $to_avg_sp = '$' . number_format($avg_sp, 2, '.', '');
        } else {
            $to_avg_sp = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('I' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('I' . $cell, 'AVG=' . $to_avg_sp);

        //Add Date In the Footer

        $cell = $cell + 5;

        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell)->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, date("Y-m-d H:i:s"));

//===================== Unset All Variables =========================================

        unset($aenplaning);

        unset($afbgrosssales);

        unset($asrgrosssales);

        unset($anggrosssales);

        unset($aconcessiongrosssales);

        unset($avg_sp);

//==============================================================
//==============================================================



        $sheetName = 'International_Airports_by_Performance' . $ref . '.xlsx';

// Redirect output to a client�s web browser (Excel2007)

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $sheetName . '"');

        header('Cache-Control: max-age=0');

// If you're serving to IE 9, then the following may be needed

        header('Cache-Control: max-age=1');



// If you're serving to IE over SSL, then the following may be needed

        header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

        header('Pragma: public'); // HTTP/1.0



        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

        $objWriter->save('php://output');
    }
    /**
     * 
     * End Top International Airports
     */

    public function index() {

        $laid = $this->session->userdata('list');

        print_r($laid);
    }
    
    /**
     * 
     * Start Total Passengers of All Airports (Required Year) 
     */
    public function totalairportpassenger($year, $type) {

        $ref = $year;

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Shawn Hanlon")->setLastModifiedBy("Shawn Hanlon")
                ->setTitle("Airports Passenger Traffic " . $ref)
                ->setSubject("Airports Passenger Traffic " . $ref)
                ->setDescription("Airports Passenger Traffic " . $ref)
                ->setKeywords("Airports Passenger Traffic " . $ref)
                ->setCategory("Airports Passenger Traffic " . $ref);

        $val = 'Airports Passenger Traffic ' . $ref;

        // Style for the Main heading

        $BGstyleArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'F2F2F2'
                )
            )
        );

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 12,
                'name' => 'Verdana'
        ));

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        // Style for the column titles

        $heading = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            )
        );

        $titlestyle = array(
            'font' => array(
                'bold' => true,
                'color' => array('hex' => '000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                )
            )
        );

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

//        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle("A1:G1")->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', $ref . ' Airports Passenger Traffic');

        // Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Airports_Passenger_Traffic_' . $ref);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $objPHPExcel->setActiveSheetIndex(0);

        $aenplaning = '';

        $afbgrosssales = '';

        $asrgrosssales = '';

        $anggrosssales = '';

        $avg_sp = '';

        $aconcessiongrosssales = '';

//        $sql = "SELECT acity,IATA,aenplaning,afbgrosssales,asrgrosssales,anggrosssales,asalesep FROM airports INNER JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.asalesep!=0 AND airporttotals.ayear='" . $year . "' AND airports.IATA!='YYJ' AND airports.IATA!='YVR' AND airports.airport_is_active = 1  ORDER BY cast(airporttotals.asalesep AS decimal( 38, 10 )) DESC LIMIT 0," . $this->limit;

        $sql = "SELECT * FROM airports LEFT JOIN airporttotals ON airports.aid = airporttotals.aid WHERE airporttotals.apasstraffic!=0 AND airporttotals.ayear=" . $this->db->escape($year) . " AND airports.airport_is_active = 1  ORDER BY cast(airporttotals.apasstraffic AS decimal( 38, 10 )) DESC";

                
        $ap_count = $this->common->CustomCountQuery($sql);

        $airportlist = $this->common->CustomQueryALL($sql);

        $count = 1;

        $headcell = 2;

        $titlecell = 3;

        $cell = 4;



        $objPHPExcel->getActiveSheet()->getStyle('A' . $headcell . ':G' . $headcell)->applyFromArray($heading);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $headcell . ':G' . $headcell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $headcell, ' ');



        $objPHPExcel->getActiveSheet()->getStyle('A' . $titlecell . ':G' . $titlecell)->applyFromArray($titlestyle);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $titlecell, 'Airports');

        $objPHPExcel->getActiveSheet()->setCellValue('B' . $titlecell, 'IATA');

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $titlecell, 'Passenger Traffic');

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $titlecell, 'Enplanements');

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $titlecell, 'Deplaning');

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $titlecell, 'EP Domestic');

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $titlecell, 'EP Int`l');

//        $objPHPExcel->getActiveSheet()->setCellValue('H' . $titlecell, 'Sales E/P');



        if ($ap_count > 0) {

            foreach ($airportlist as $airportlist):

//                $all = $airportlist["afbgrosssales"] + $airportlist["asrgrosssales"] + $airportlist["anggrosssales"];

                if ($count % 2 == 0) {

                    $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':G' . $cell)->applyFromArray($BGstyleArray);
                }



                $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, stripslashes($airportlist["acity"]));

                $objPHPExcel->getActiveSheet()->setCellValue('B' . $cell, $airportlist["IATA"]);

                $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, number_format($airportlist["apasstraffic"]));

                $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, number_format($airportlist["aenplaning"]));

                //$objPHPExcel->getActiveSheet()->getStyle('D'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, number_format($airportlist["adeplaning"]));

                $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, number_format($airportlist["aepdomestic"]));

                $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, number_format($airportlist["aepintl"]));

//                $objPHPExcel->getActiveSheet()->setCellValue('H' . $cell, '$' . number_format($airportlist["asalesep"], 2, '.', ''));



                $apasstraffic+=$airportlist["apasstraffic"];

                $aenplaning+=$airportlist["aenplaning"];

                $adeplaning+=$airportlist["adeplaning"];

                $aepdomestic+=$airportlist["aepdomestic"];

                $aepintl+=$airportlist["aepintl"];
                
                
                
//                $aenplaning+=$airportlist["aenplaning"];
//
//                $afbgrosssales+=$airportlist["afbgrosssales"];
//
//                $asrgrosssales+=$airportlist["asrgrosssales"];
//
//                $anggrosssales+=$airportlist["anggrosssales"];
//
//                $aconcessiongrosssales+=$all;
//
//                $avg_sp+=$airportlist["asalesep"] / $ap_count;



                $count++;

                $cell ++;

            endforeach;
        } else {

            $cell = $cell = 15;

            //exit();

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, 'F&B dsadasdas Sales');
        }





        //Collect Total

        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell . ':B' . $cell)->applyFromArray($heading);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $cell . ':B' . $cell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, 'Totals');

        //Collect Total Passenger Numbers

        if ($apasstraffic) {
            $tot_apasstraffic = number_format($apasstraffic);
        } else {
            $tot_apasstraffic = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('C' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('C' . $cell, $tot_apasstraffic);

        //Collect Total Enplanements Numbers

        
        if ($aenplaning) {
            $tot_aenplaning = number_format($aenplaning);
        } else {
            $tot_aenplaning = '';
        }
        
        $objPHPExcel->getActiveSheet()->getStyle('D' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('D' . $cell, $tot_aenplaning);

        //Collect Total Deplaning Numbers

        if ($adeplaning) {
            $sr_adeplaning = number_format($adeplaning);
        } else {
            $sr_adeplaning = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('E' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('E' . $cell, $sr_adeplaning);

        //Collect Total EP Domestic

        if ($aepdomestic) {
            $ng_aepdomestic = number_format($aepdomestic);
        } else {
            $ng_aepdomestic = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('F' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $cell, $ng_aepdomestic);

        //Collect Total EpInt'l

        if ($aepintl) {
            $to_aepintl = number_format($aepintl);
        } else {
            $to_aepintl = '';
        }

        $objPHPExcel->getActiveSheet()->getStyle('G' . $cell)->applyFromArray($heading);

        $objPHPExcel->getActiveSheet()->setCellValue('G' . $cell, $to_aepintl);

        

        //Add Date In the Footer

        $cell = $cell + 5;

        $objPHPExcel->getActiveSheet()->getStyle('A' . $cell)->applyFromArray($style);
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $cell . ':D' . $cell);

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, date("Y-m-d H:i:s"). '  © ' . date("Y") . ' Airport Revenue News');

//===================== Unset All Variables =========================================
         
        unset($apasstraffic);

        unset($aenplaning);

        unset($adeplaning);

        unset($aepdomestic);

        unset($aepintl);

//        unset($avg_sp);

//==============================================================
//==============================================================



        $sheetName = 'Airports_Passenger_Traffic_' . $ref . '.xlsx';

// Redirect output to a client�s web browser (Excel2007)

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $sheetName . '"');

        header('Cache-Control: max-age=0');

// If you're serving to IE 9, then the following may be needed

        header('Cache-Control: max-age=1');



// If you're serving to IE over SSL, then the following may be needed

        header('Expires: Mon, 26 Jul 2018 05:00:00 GMT'); // Date in the past

        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified

        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1

        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//$objPHPExcel = PHPExcel_IOFactory::load("myExcelFile.xlsx");

        $objWriter->save('php://output');
    }
    /**
     * 
     * End Total Passengers of All Airports
     */

}

// ENd EXCEL Class