<?php

class affiliations extends Ci_Controller {

    public $outputData;

    /**
     * 
     * Start Check and Assign Affiliations (Required IDs) Admin
     */
    public function __construct() {
        parent::__construct();
        $this->common->ThisSecureArea('admin');
        $joinon = 'companies.categoryid = lkpcategory.categoryid';
        $this->outputData['menu'] = 'frontend/include/menu';
        $this->outputData['footer'] = 'frontend/include/footer';
        $this->outputData['header'] = 'frontend/include/header';
        $this->outputData['searchaction'] = '';
        $this->lang->load('config', 'english');
        if ($this->input->post('saveaff') == 1) {
            $user_id = $this->input->post('user_id');
            $cid = $this->input->post('cid');
            $aid = $this->input->post('aid');
            $dataarray = array('user_id' => $user_id, 'cid' => $cid, 'aid' => $aid);

            $u_dataarray = array('user_id' => $user_id, 'accesslevel' => 1);

            $user = $this->common->GetSingleRowFromTableWhere('users', array('user_id' => $user_id));

            if ($this->common->GetTotalFromTableWhere('users', $u_dataarray)) {
                $this->common->setmessage("<b>" . $user['last_name'] . ' ' . $user['first_name'] . "</b> access level is <b>Read Only</b>. You cann't allow to <b>Modify Data</b> untill change it.", "-1");
            } elseif ($this->common->GetTotalFromTableWhere('affiliation', $dataarray) > 0) {/*
              if($this->common->GetTotalFromTableWhere('affiliation',array('user_id'=>$user_id,'cid'=>$cid,'aid'=>$aid, 'affi_is_active' => 0)))
              {
              $this->common->UpdateRecord('affiliation',array('user_id'=>$user_id,'cid'=>$cid,'aid'=>$aid, 'affi_is_active' => 0),array('affi_is_active' => 1));
              $this->common->setmessage("Affiliation has been assigned Successfully.","1");
              }
              else if ($this->common->GetTotalFromTableWhere('affiliation',array('user_id'=>$user_id,'cid'=>$cid,'aid'=>$aid, 'affi_is_active' => 1)))
              { */
                $this->common->setmessage("You are already assigned same affiliation.", "-1");
            } else {
                $this->common->setmessage("Affiliation has been assigned Successfully.", "1");
                $this->common->InsertInDb('affiliation', $dataarray);
                $this->common->getactivity('insert_affiliation','');
            }
        }
        $this->outputData['listofuser'] = $this->common->GetAllRowWithColumn('users', 'user_id,last_name,first_name', 'is_active = 1', 'last_name, first_name', 'ASC');

        $this->outputData['listofairports'] = $this->common->GetAllRowWithColumn('airports', '*', 'airport_is_active = 1', 'aname', 'ASC');
        $this->outputData['listofcompanies'] = $this->common->JoinTable('companyname,cid,companytype,lkpcategory.productdescription', 'companies', 'lkpcategory', $joinon, 'LEFT', array('cid >' => 0));

        $cols = "users.last_name, users.first_name, airports.aid, affiliation.affid, companies.companyname,airports.IATA";
        $join_array = array(
            array('users', 'affiliation.user_id=users.user_id', 'left'),
            array('airports', 'affiliation.aid=airports.aid', 'left'),
            array('companies', 'affiliation.cid=companies.cid', 'left')
        );
        $this->outputData['aff_list'] = $this->common->JoinTables($cols, 'affiliation', $join_array, "", "users.last_name, users.first_name ASC");
    }
    /**
     * 
     * End Check and Assign Affiliations
     */

    /**
     * 
     * Start View Affiliations Admin
     */
    public function index() {
//		$this->outputData['affiliations_adds']=$this->common->CustomQueryALL("SELECT * FROM advertising,advertising_mid WHERE advertising_mid.admid=advertising.mid AND (adlocation ='L1' OR adlocation ='L2' OR adlocation ='L3') AND advertising.mid=16");

        $this->load->view('frontend/affiliations/affiliations-view', $this->outputData);
    }
    /**
     * 
     * End View Affiliations Admin
     */

}

?>