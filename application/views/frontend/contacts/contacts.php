<?php echo $this->load->view($header); ?>

<title>

    <?php

    if ($this->uri->segment(0) != '') {

        echo 'Contacts | ARN Fact Book ' . $this->uri->segment(0);

    } else {

        echo 'Contacts | ARN Fact Book';

    }

    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/jquery-ui-1.10.3.full.min.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables2.bootstrap.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>       

        <section class="main-content">        	

            <?php /* ?><section class="col-sm-2">

              <?php foreach($contacts_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>            	            	

                <div class="col-md-3 col-sm-12 col-sm-3 login-left-sect">

                    <h1 class="default-title">Contacts can be searched by:</h1>

                    <ul class="list-group margin-top-ten">

                        <li class="list-group-item"><a href="<?php echo site_url('contacts/company/a'); ?>"><i class="fa fa-building margin-right-5">&nbsp;</i><span class="default-text">Companies</span></a></li>

                        <li class="list-group-item text-capitalize text-center text-primary"><b>OR</b></li>

                        <li class="list-group-item"><a href="<?php echo site_url('contacts/airport/a'); ?>"><i class="fa fa-plane margin-right-5">&nbsp;</i><span class="default-text">Airports</span></a></li>

                    </ul>

                    <div class="clearfix"></div>

                </div>

                <div class="col-md-9 col-sm-9 col-xs-12 login-right-sect">

                    <?php if ($this->common->GetSessionKey('contactsearch') != "") { ?>



                        <div class="alert alert-info">

                            <strong>(<?php echo $total ?>)</strong> Contacts has been found.

                            <form action="<?php echo site_url('contacts/clearsearccontact'); ?>" method="post">

                                <button  class="cleansearch btn btn-danger btn-sm" name="submit" >

                                    <i class="fa fa-trash"></i>&nbsp;Clear Search</button>

                            </form>

                        </div>

                    <?php } ?>

                    <form class="form-horizontal" action="<?php echo site_url('contacts'); ?>" method="post">

                        <div class="form-group">

                            <label for="inputEmail3" class="col-sm-2 control-label">Contact Search</label>

                            <div class="col-sm-10">

                                <input type="text" name="search" class="form-control" id="search" placeholder="Contact Search">

                            </div>

                        </div>                 

                        <div class="form-group">

                            <div class="col-sm-offset-2 col-sm-10">

                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>

                            </div>

                        </div>

                    </form>

                    <?php $abc = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z') ?>

                    <div class="row">

                        <div class="col-sm-12 padding-left-none padding-right-none">

                            <nav>

                                <ul class="pagination pagination-sm">

                                    <?php for ($i = 0; $i < count($abc); $i++) { ?>

                                        <li <?php if (strtolower($abc[$i]) == $this->uri->segment(3)) { ?>class="active"<?php } ?>>

                                            <a href="<?php echo site_url('contacts/company') . "/" . strtolower($abc[$i]); ?>"><?php echo $abc[$i]; ?><span class="sr-only">(current)</span>

                                            </a>

                                        </li>

                                        <?php if (strtolower($abc[$i] == 'x')) { ?><br/><br/><?php } ?>



                                    <?php } ?>



                                </ul>

                            </nav>

                        </div>                      

                    </div>



                    <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 

                        <ul id="myTabs" class="nav nav-tabs" role="tablist"> 

                            <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><i class="margin-right-5 fa fa-qrcode">&nbsp;</i>Grid View</a></li> 

                            <li role="presentation" class="" ><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="margin-right-5 fa fa-table">&nbsp;</i>Table View</a></li>

                        </ul> 

                        <br />

                        <div id="myTabContent" class="tab-content"> 

                            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">



                                <div class="row">

                                    <?php

                                    $comd = 0;

                                    foreach ($compnaylist as $com): $comd++;

                                        ?>                        

                                        <?php if ($comd % 2 == 0) { ?>

                                            <div class="responsive-padding-none col-md-6 padding-left-none margin-bottom-ten">

                                                <div class="panel panel-primary" style="min-height:330px;">

                                                    <div class="panel-heading"><b><?php echo $com['cfname'] . "&nbsp;" . $com['clname']; ?></b></div>

                                                    <div class="panel-body padding-none">

                                                        <div class="table-responsive">

                                                            <table class="table-bordered table-hover table-striped"width="100%">

                                                                <tr>

                                                                    <td width="100"><i class="fa fa-phone margin-right-5">&nbsp;</i>Contact</td>

                                                                    <td><?php echo $com['cfname'] . "&nbsp;" . $com['clname']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-book margin-right-5">&nbsp;</i>Title</td>

                                                                    <td><?php echo $com['ctitle']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-home">&nbsp;</i>Address</td>

                                                                    <td><?php echo $com['caddress1'] . ' ' . $com['caddress2'] . ' ' . $com['ccity'] . ',' . ' ' . $com['cstate'] . ' ' . $com['czip'] . ' ' . $com['ccountry']; ?></td>             

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-phone">&nbsp;</i>Phone</td>

                                                                    <td><?php echo $com['cphone']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-fax">&nbsp;</i>Fax</td>

                                                                    <td><?php echo $com['cfax']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-envelope-o"></i>&nbsp;&nbsp;E-mail</td>

                                                                    <td><a href="mailto:<?php echo $com['cemail']; ?>"><?php echo $com['cemail']; ?></a></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-globe"></i>&nbsp;Web site</td>

                                                                    <td><a href="http://<?php echo $com['cwebsite']; ?>" target="new"><?php echo $com['cwebsite']; ?></a></td>            

                                                                </tr>

                                                            </table> 

                                                        </div>

                                                    </div>                                   

                                                    <div class="clearfix"></div>

                                                </div>

                                            </div>

                                        <?php } else { ?>

                                            <div class="responsive-padding-none col-md-6 padding-left-none margin-bottom-ten">

                                                <div class="panel panel-primary" style="min-height:330px;">

                                                    <div class="panel-heading"><b><?php echo $com['cfname'] . "&nbsp;" . $com['clname']; ?></b></div>

                                                    <div class="panel-body padding-none">

                                                        <div class="table-responsive">

                                                            <table class="table-bordered table-hover table-striped"width="100%">

                                                                <tr>

                                                                    <td width="100"><i class="fa fa-phone margin-right-5"></i>&nbsp;Contact</td>

                                                                    <td><?php echo $com['cfname'] . "&nbsp;" . $com['clname']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-book margin-right-5"></i>&nbsp;Title</td>

                                                                    <td><?php echo $com['ctitle']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-home"></i>&nbsp;Address</td>

                                                                    <td><?php echo $com['caddress1'] . ' ' . $com['caddress2'] . ' ' . $com['ccity'] . ',' . ' ' . $com['cstate'] . ' ' . $com['czip'] . ' ' . $com['ccountry']; ?></td>          

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-phone"></i>&nbsp;Phone</td>

                                                                    <td><?php echo $com['cphone']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-fax"></i>&nbsp;Fax</td>

                                                                    <td><?php echo $com['cfax']; ?></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-envelope-o"></i>&nbsp;E-mail</td>

                                                                    <td><a href="mailto:<?php echo $com['cemail']; ?>"><?php echo $com['cemail']; ?></a></td>            

                                                                </tr>

                                                                <tr>

                                                                    <td><i class="margin-right-5 fa fa-globe"></i>&nbsp;Web site</td>

                                                                    <td><a href="http://<?php echo $com['cwebsite']; ?>" target="_new"><?php echo $com['cwebsite']; ?></a></td>            

                                                                </tr>

                                                            </table> 

                                                        </div>

                                                    </div>                                   

                                                    <div class="clearfix"></div>

                                                </div>

                                            </div>

                                        <?php } ?>

                                    <?php endforeach; ?>

                                    <div class="row">

                                        <div class="col-sm-12">                            	

                                            <?php

                                            $this->common->getpaginationF(site_url('contacts'), $total, "pagination-sm pull-right");

                                            ?>                                

                                        </div>

                                    </div>

                                </div>

                            </div>



                            <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">

                                <div class="row">

                                    <!--<div class="panel-body">-->

                                    <div class="table-responsive">

                                        <table  width="100%" id="table-view-Data" class="table-bordered table-hover table-striped contact2">

                                            <thead>

                                                <tr  style="background-color:#eee;">

                                                    <th>Contact</th>

                                                    <th>Title</th>

                                                    <th>Address</th>

                                                    <th>Phone</th>

                                                    <th>Fax</th>

                                                    <th>E-mail</th>

                                                    <th>Web site</th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php

                                                $comd = -1;

                                                foreach ($compnaylist as $com): $comd++;

                                                    ?>                            



                                                    <?php if ($comd % 2 == 0) { ?>

                                                        <tr>

                                                            <td><?php echo $com['cfname'] . "&nbsp;" . $com['clname']; ?></td>

                                                            <td><?php echo $com['ctitle']; ?></td>

                                                            <td><?php echo $com['caddress1'] . ' ' . $com['caddress2'] . ',' . ' ' . $com['ccity'] . ',' . ' ' . $com['cstate'] . ' ' . $com['czip'] . ' ' . $com['ccountry']; ?></td>

                                                            <td><?php echo $com['cphone']; ?></td>

                                                            <td><?php echo $com['cfax']; ?></td>

                                                            <td><a href="mailto:<?php echo $com['cemail']; ?>"><?php echo $com['cemail']; ?></a></td>

                                                            <td><a href="http://<?php echo $com['cwebsite']; ?>" target="new"><?php echo $com['cwebsite']; ?></a></td>

                                                        </tr>

                                                    <?php } else { ?>

                                                        <tr>

                                                            <td><?php echo $com['cfname'] . "&nbsp;" . $com['clname']; ?></td>

                                                            <td><?php echo $com['ctitle']; ?></td>

                                                            <td><?php echo $com['caddress1'] . ' ' . $com['caddress2'] . ',' . ' ' . $com['ccity'] . ',' . ' ' . $com['cstate'] . ' ' . $com['czip'] . ' ' . $com['ccountry']; ?></td>

                                                            <td><?php echo $com['cphone']; ?></td>

                                                            <td><?php echo $com['cfax']; ?></td>

                                                            <td><a href="mailto:<?php echo $com['cemail']; ?>"><?php echo $com['cemail']; ?></a></td>

                                                            <td><a href="http://<?php echo $com['cwebsite']; ?>" target="new"><?php echo $com['cwebsite']; ?></a></td>

                                                        </tr>



                                                    <?php } ?>

                                                <?php endforeach; ?>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>



                </div>	                      



            </section>

        </section>

        <?php echo $this->load->view($footer); ?>

    </div>

    <!--<script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>-->

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>

    <script src="<?php echo base_url('assets'); ?>/js/typeahead-bs2.min.js"></script>

    <script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.3.full.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            $('.contact2').DataTable({

                "dom": 'T<"clear">lfrtip',

//                "processing": true,

//                "serverSide": true,

                "lengthMenu": [5, 10, 15, 20, 25],

                "dom": 'T<"clear">lfrtip',

                "tableTools": {

//                    "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"

                    "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"

                },

                //"bProcessing": true,

                "oLanguage": {

                    "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                }

            });

        });

        $(function () {

            function split(val) {

                return val.split(/,\s*/);

            }

            function extractLast(term) {

                return split(term).pop();

            }

            var s = $("#search").val();

            data: "search=" + s,

                    $("#search")

                    // don't navigate away from the field on tab when selecting an item

                    .bind("keydown", function (event) {

                        if (event.keyCode === $.ui.keyCode.TAB &&

                                $(this).autocomplete("instance").menu.active) {

                            event.preventDefault();

                        }

                    })

                    .autocomplete({

                        source: function (request, response) {

                            $.getJSON("<?php echo site_url('contacts'); ?>", {

                                term: extractLast(request.term)}, response);

                        },

                        search: function () {

                            // custom minLength

                            var term = extractLast(this.value);

                            if (term.length < 2) {

                                return false;

                            }

                        },

                        focus: function () {

                            // prevent value inserted on focus

                            return false;

                        },

                        select: function (event, ui) {

                            var terms = split(this.value);

                            // remove the current input

                            terms.pop();

                            // add the selected item

                            terms.push(ui.item.value);

                            // add placeholder to get the comma-and-space at the end

                            terms.push("");

                            this.value = terms.join("");

                            return false;

                        }

                    });

        });



    </script>       

</body>

</html>

