<?php echo $this->load->view($header);?>

<title>
<?php 
	if($this->uri->segment(0)!='')
	{
	echo 'Welcome to ARN Fact Book | Login '.$this->uri->segment(0);
	
	}
	else 
	{
	echo 'Welcome to ARN Fact Book | Login';
	}
?>
</title>

</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>
        <section class="main-content">
				<?php /*?><section class="col-sm-2">
<?php foreach($home_adds as $adds): ?>
<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
<?php endforeach; ?>				
				</section><?php */?>		     	
        	<section class="container-fluid">
            <div class="msg alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert">&times;</a>Please enter a message first...</div>           	
            	
<div class="widget-box ">
											<div class="widget-header">
												<h4 class="lighter smaller">
													<i class="icon-comment blue"></i>
													Conversation &nbsp; <b><?php echo $row_rsTickets['subject'];?></b>
												</h4>
												
											</div>
												
											

											<div class="widget-body">
											
											
												<div class="widget-main no-padding">
													
											
													<div class="dialogs">
													
													<?php for($i = 0 ; $i < 10 ; $i++): ?>
													
														<div class="itemdiv dialogdiv">
															<div class="user">
																<img alt="Alexa's Avatar" src="<?php echo base_url(); ?>assets/avatars/avatar1.png" />
															</div>

															<div class="body">
																<div class="time">
																	<i class="icon-time"></i>
																	<span class="green">4 sec</span>
																</div>

																<div class="name">
																	<a href="#">Shawn</a>
																</div>
																<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

																<div class="tools">
																	<a href="#" class="btn btn-minier btn-info">
																		<i class="icon-only icon-share-alt"></i>
																	</a>
																</div>
															</div>
														</div>
													<?php endfor; ?>
													</div>

													<form method="post" action="<?php echo site_url('frontend/conversation/'.$row_rsTickets['support_id']);?>">
														<div class="form-actions">
															<div class="input-group">
                                                            	
																<input placeholder="Type your message here ..." type="text" class="form-control" name="message" id="message"  />
																<span class="input-group-btn">
																	<button id="send-button" class="btn btn-sm btn-info no-radius" type="submit">
																		<i class="icon-share-alt"></i>
																		Send
																	</button>
																</span>
															</div>
														</div>
													</form>
												</div><!-- /widget-main -->
											</div><!-- /widget-body -->
										</div>
               </div>                
            </section>
        </section>
        <?php echo $this->load->view($footer); ?>
    </div>
<script src="<?php echo base_url('assets');?>/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script> 

<script type="text/javascript">
$("#send-button").on('click', function(){
	
	
var message = $("#message").val();

if(message == "")
{
			$('.msg').show();
			return false;
}
});
</script>  
<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
		</script>
       <script type="text/javascript">
$('.dialogs,.comments').slimScroll({
					height: '260px'
			    });
	   </script>     
</body>
</html>
