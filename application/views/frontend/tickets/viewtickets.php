<?php 
echo $this->load->view($header);?>
<title>
<?php 
	if($this->uri->segment(0)!='')
	{
	echo 'Welcome to ARN Fact Book | Login '.$this->uri->segment(0);
	
	}
	else 
	{
	echo 'Welcome to ARN Fact Book | View Tickets';
	}
?>
</title>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>
        <section class="main-content">	     	
        	<section class="container-fluid">     
            	<div class="panel panel-primary">
                <div class="panel-heading"><i class="fa fa-arrow-right"></i> All Tickets</div>
                <div class="panel-body">    
                <?php 
						
							if(count($listoftickets)>0) {
						?>  	
            		<div class="table-responsive">                    	
                		<table class="table table-hover table-striped table-bordered">
                            <tr class="back">
                                <th>Ticket Number</th>
                                <th>Created at</th>
                                <th>Subject</th>
                                <th>Status</th>
                                <th>Last Updated By</th>
                                <th>Action</th>
                            </tr>
                        	 <?php  foreach($listoftickets as $ticket): ?>
                        	<tr>
                                <td><?php echo $ticket['support_id'];?></td>
                                <td><?php echo $this->common->get_formatted_datetime($ticket['ticket_created_date']);?></td>
                                <td><?php echo $ticket['subject'];?></td>
                                <td>
									<?php 
											if($ticket['status']==1 || $ticket['status']==0)
											{ 
												echo '<span class="label label-info">Open</span>';
											} 
											else if($ticket['status']==2) 
												{
														echo '<span class="label label-success">Closed</span>';
												}
												else if($ticket['status']==3) 
												{
														echo '<span class="label label-danger">Re-open</span>';
												}
										?>
                                  </td>
                                <td><?php echo $ticket['first_name'].' '.$ticket['last_name']; ?></td>
                                <td><a class="btn btn-primary btn-xs"  href="<?php echo site_url('frontend/conversation/'.$ticket['support_id']);?>">
<i class="fa fa-search-plus"></i> View</a></td>
                        	</tr>
  						 <?php endforeach; ?>
						</table>
           			 </div>    
                     <?php } else { ?>
                     		<div class="alert alert-danger">No any ticket found.
                            <a href="#support" data-toggle="modal" class="text-primary test btn btn-primary btn-sm">Click to Create Ticket</a></div>
                     <?php } ?>
                </div>      
            	</div>          
            </section>
        </section>
        <?php echo $this->load->view($footer); ?>
    </div>

<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script> 
<script type="text/javascript">
	function show(){
	$(".login").addClass("hide");
	$(".forget").removeClass("hide");
	}
</script>
<script type="text/javascript">
			function sendpassword()
			{
				var EmailID = $("#email").val();
					if(EmailID == "")
					{
						$(".error").html('<div class="alert alert-danger">Invalid Email Address.</div>');					
						return false;
					} else
					{
						var datastring = "email="+EmailID+"&action=send_password_rest_link";
					//	$(".error").html('<img src="<?php echo base_url('assets');?>/images/loading.gif">');
						$.ajax({
								url:"<?php echo site_url('reset_password');?>",
								async:false,
								type:"POST",
								cache:false,
								data:datastring,
								success: function(html)
								{
									if(html!=1)
									{
										$(".error").empty();
										$(".error").html(html);
									} else 
									{
										$(".error").empty();
										$(".error").html('<div class="alert alert-danger">An email has been sent to reset your password. Please read your email and set new password.</div>');
									}
								}
						});
						return false;
					}
			}
</script>   
</body>
</html>
