<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
	echo 'Welcome to ARN Fact Book | Login '.$this->uri->segment(0);
}
else 
{
	echo 'Welcome to ARN Fact Book | Conversation';
}
?>
</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace.min.css" />
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
<style type="text/css">
.scrollup
{
	float:right;	
}
</style>
</head>
<body>
   <div class="main-wrapper">
      <?php echo $this->load->view($menu);?>
      <div class="clearfix"></div>
      <section class="container-fluid">
      <div class="msg alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert">&times;</a>Please enter a message first...</div>  
         <div class="row">         
            <div class="col-xs-12">
               <div class="error"><?php echo $this->common->getmessage();?></div>
               <div class="widget-box">
                  <div class="widget-header">
                  <!--   <h4 class="lighter smaller">
                        <i class="icon-comment blue"></i>
                       &nbsp; <b><?php //echo $receivemessage[0]['subject'];?></b>&nbsp; | &nbsp;
                     </h4>                     
                     <a href="<?php //echo current_url(); ?>">
                     <h5 style="float:right;padding-right:3em;"><i class="fa fa-refresh">&nbsp;</i>Reload</h5>
                     </a>-->
                     <form class="form-inline" action="<?php echo current_url();?>" method="post">
                      <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Update Status</label>
                        <div class="input-group">
                          <div class="input-group-addon">Update Status</div>
                         	<input type="hidden" name="ticketid" value="<?php  echo $this->uri->segment(3) ?>">
                            <select type="text" class="form-control" name="ticketstatus">
                            <?php foreach($listoftype as $type): ?>
                            <option value="<?php echo $type['support_ticket_type_id'];?>" <?php if($receivemessage[0]['status']==$type['support_ticket_type_id']){ ?> selected <?php } ?>><?php echo $type['support_ticket_type_name'];?></option>
                            <?php  endforeach ?>
                            </select>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check">&nbsp;</i>Update</button>
                    </form>
                  </div>
                  <div class="widget-body">
                     <div class="widget-main no-padding">
                        <div class="dialogs">
                           <?php foreach($receivemessage as $r): ?>
                           <div class="itemdiv dialogdiv">
                              <div class="user">
                                 <img alt="Alexa's Avatar" src="<?php echo base_url(); ?>assets/avatars/avatar1.png" />
                              </div>
                              <div class="body alert <?php echo ($r['sender_bit'] != 1)?'alert-info':'alert-success' ?>">
                                 <div class="time">
                                    <i class="icon-time"></i>
                                    <span class="green"><?php echo $this->commonmodel->get_formatted_datetime($r['fr_ticket_created_on']); ?></span>
                                 </div>
                                 <div class="name">
                                    <a href="#"><?php echo ucwords(($r['sender_bit'] != 1)?'Support':$r['first_name'].' '.$r['last_name']); ?></a>
                                 </div>
                                 <div class="text"><?php echo $r['fr_ticket_conversion_message']; ?></div>
                              </div>
                           </div>
                           <?php endforeach; ?>
                        </div>
                        <form method="post" action="<?php echo site_url('frontend/conversation/'.$r['support_id']);?>">
                           <div class="form-actions">
                              <div class="input-group">
                                 <input placeholder="Type your message here ..." type="text" class="form-control" name="message" id="message" required />
                                 <span class="input-group-btn">
                                 <button  id="send-button" name="submitted" value="1" class="btn btn-md btn-info no-radius" type="submit">
                                 <i class="icon-share-alt"></i>
                                 Send
                                 </button>
                                 </span>
                              </div>
                           </div>
                        </form>
                     </div>                    
                  </div>                  
               </div>              
            </div>            
         </div>
      </section>
      <?php echo $this->load->view($footer); ?>
   </div>
   <script src="<?php echo base_url('assets');?>/js/jquery.slimscroll.min.js"></script>
   <script type="text/javascript">
      try{ace.settings.check('main-container' , 'fixed')}catch(e){}
   </script>
   <script type="text/javascript">
		$('.dialogs,.comments').slimScroll({
			height: '260px'
		});  	   
   </script>  
   <script type="text/javascript">
   $(document).ready(function(e) {
    $("#top").hide();	
	$(".dialogs").animate({scrollTop: $('.widget-main').height()}, "slow");
});
$("#send-button").on('click', function(){
	
	
var message = $("#message").val();

if(message == "")
{
			$('.msg').show();
			return false;
}
});
</script>  
</body>
</html>