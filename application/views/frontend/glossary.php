<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Glossary | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Glossary | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>       
        <section class="main-content">  
				<?php /*?><section class="col-sm-2">
<?php foreach($glossary_adds as $adds): ?>
<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
<?php endforeach; ?>				
				</section><?php */?>		     	
        	<section class="container-fluid">
            	 <?php echo $this->load->view('frontend/include/breadcrumb');?>            	            	
                <div class="col-md-12 col-sm-12 col-xs-12 login-right-sect">                	
                   <div class="row">
				   <div class="col-sm-12">
                    	<div class="col-sm-12">
                        <div role="tabpanel" class="tab-pane active">
                            <div class="table-responsive padding-table">
    							<h1 class="default-title"><i class="fa fa-plane"></i>&nbsp;Glossary</h1>
                             </div>
                             <p>The following information is provided in this year’s ARN Fact Book, unless otherwise unavailable.</p>
							</div>
                        </div>
                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><i class="fa fa-file-pdf-o"></i>&nbsp;Airport Instructions</a></li>
      <li role="presentation" class=""><a href="#airportlistings" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="fa fa-list"></i>&nbsp;Airport Listings</a></li>
      <li role="presentation" class=""><a href="#pleasenote" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="fa fa-info-circle"></i>&nbsp;Please Note</a></li>
      <li role="presentation" class=""><a href="#glossary" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="fa fa-gears"></i>&nbsp;Glossary</a></li>      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">        
       <br/>
       <div class="panel panel-primary">
       <div class="panel-heading">Airport Instructions</div>
       <div class="panel-body">
            	<a href="<?php echo base_url('prereports/FBOInstructions.pdf'); ?>" target="new"><i class="fa fa-download"></i>&nbsp;Click Here</a> to Download Airport Instructions.           
        </div></div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="airportlistings" aria-labelledby="profile-tab">
            	<br/>
                <div class="panel panel-primary">
                <div class="panel-heading">Airport Listings</div>
                <div class="panel-body">                	 
                	Airport Listings: Each airport included in ARN’s OFB <?php echo date('Y');?> has entered their information directly into this database. This year more than 90 airports submitted their concessions data to ARN for the purposes of this service.  The majority of airports are reporting their information on a terminal-by-terminal basis (all information is based on 2012 data). Where an airport was unable to provide the information broken down by concourse or terminal, the data is shown in the aggregate for the airport’s entire program. The book indicates where “airportwide information only” is shown.                
        </div></div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="pleasenote" aria-labelledby="profile-tab">
       <br/>
       <div class="panel panel-primary">
                <div class="panel-heading">Please Note</div>
                <div class="panel-body">
       	The information contained in this book has been submitted to ARN directly from the participating airports and concessionaires, and has been approved by all participants. ARN is not liable for any discrepancies that may exist.
       </div>
        </div>       
      </div>
      <div role="tabpanel" class="tab-pane fade" id="glossary" aria-labelledby="profile-tab">
      <br/>
       <div class="panel panel-primary">
       			
                <div class="panel-heading">Airport Listings</div>
                <div class="panel-body">
                <dl class="dl-horizontal">
      <dt>Airport Configuration:</dt>
      <dd><p>Describes the shape of your airport. For example, T-shaped, U-shaped, Fingers, etc.</p></dd>
      <dt>Advertising:</dt>
      <dd>Gross revenue and rent to the airport, where available, are provided. This category includes all revenue generated from advertising space at the airport that has been bought by companies paying for said space for the purpose of marketing their brand, including dioramas, wall space, banners, etc.</dd>     
      <dt>Average Dwell Time:</dt>
      <dd>The average amount of time a departing passenger spends within the airport.</dd>
       <dt>Car Rental:</dt>
      <dd>Shows the number of car rental companies at the airport, on-site and off-site, the gross revenue generated by the rental companies and the total amount of rent paid to the airport.</dd>      
      <dt>Concessionaire Company Details:</dt>
      <dd>
      <p>
      	All companies are categorized by type of business and are listed alphabetically within the section describing its type of business. The categories include food/beverage operator, specialty retailer, news/gift operator, duty-free operator, passenger services, aviation/airport consultant, concessions manager, private developer, architect/interior design and construction companies, advertising concessionaire, concession supplier, retail publication and trade association.
      </p>
      <p>
      For each company, pertinent information is provided for the appropriate company contact.  Most companies provide a synopsis of their history and track record, as well as a list of airport clients.
      </p>
      </dd>
      <dt>Concessions Sales:</dt>
      <dd>
      	<p>A breakdown by terminal/concourse of the airport’s concessions sales for food/beverage, specialty retail and news/gift only, unless designated “airportwide information only”.
In airportwide only cases, the aggregate gross sales for food/beverage, specialty retail and news/gift is shown. This section includes the sales per enplanement (sales/EP), rent revenue to the airport (in some cases this information was not provided by the airport and therefore a “0” will appear) and current square footages (aggregate amount of space allocated to these categories) for these categories combined in each terminal/concourse. Also included is a list of the dominant airlines servicing the given terminal/concourse.
Unless otherwise noted, Canadian airports have converted their sales into U.S. dollars using the average 2013 calendar year exchange rate of $.97 Canadian = $1.00 US Dollars (as quoted by the Bank of Canada).</p>
      </dd>
      <dt>Deplaning:</dt>
      <dd>The number of people disembarking an airline and arriving through the airport to baggage claim or ground transportation.</dd>
      <dt>Dominant Airline:</dt>
      <dd>Name of the airline that occupies the most gates in the respective terminal.</dd>
      
      <dt>Duty Free:</dt>
      <dd>Retail shops that do not apply local or national taxes and duties to products sold.</dd>
      
      <dt>Enplaning:</dt>
      <dd>Passengers departing the airport on airlines leaving the airport to their predetermined destinations</dd>
      <dt>Expansion Planned:</dt>
      <dd>A reference as to whether the airport is expanding its concessions and/or other space in any given terminal or zone in its facility, how much additional space is planned and when the expansion is expected to be complete.
      </dd>
      <dt>Expires:</dt>
      <dd>Month, date and year in which the contract with the associated concept and operator expires.</dd>
      <dt>Food/Beverage:</dt>
      <dd>Any store within an airport that utilizes more than 50% of its space to offer food and beverage products for sale to the public.</dd>
      <dt>Gross Rentals:</dt>
      <dd>The amount of revenue received by the airport in the form of rent paid by concessionaires.</dd>
      <dt>List of Terminals/Concourses:</dt>
      <dd>A list of names for each terminal in the airport, or concourse as the case may be, and its abbreviation for easy reference in later areas of the listing.</dd>
      <dt>Management Contacts:</dt>
      <dd>Key people to be contacted for further information about the airport’s concessions program.</dd>
      <dt>Management Structure:</dt>
      <dd>Describes how the airport manages its concessions program, whether by its own staff directly (where it leases and oversees the program without a third-party manager/developer); master concessionaire (one company which operates all of the food or all of the retail); multiple primes (where more than one operator is contracted to operate several stores in the program); private developer (third-party firm that typically invests capital, configures space, leases and manages the program but does not operate its own stores); or a hybrid structure (any combination of the above).</dd>
      
      <dt>News/Gifts:</dt>
      <dd>Any store within an airport that utilizes the majority of its space for the sale of news publications and gift items.</dd>
      
      <dt>O&D to Transfers: </dt>
      <dd>This is ratio denoting the percentage of origination and destination passengers relative to the percentage of passengers transferring from one flight to another in the airport.</dd>
      
      <dt>Parking Information:</dt>
      <dd>Shows all parking rates; gross revenue from parking; the number of concessionaires are located on- or off-airport; daily rate concessionaire(s) charge to parkers; concession fee paid to airport (if applicable) and total rent to the airport from parking.</dd>
      
      <dt>Passenger Services:</dt>
      <dd>Refers to all non-retail or non-food services provided by the airport such as massage, ATMs, banking, business centers, barber shops, etc.</dd>
      
      <dt>Passenger Traffic:</dt>
      <dd> A breakdown of passenger traffic by terminal or concourse, by enplaning (the number of passengers leaving the airport on departing flights) and deplaning (the number of passengers arriving at the airport on arriving flights), by international (outside the U.S.) and domestic (inside the U.S.).
The passenger information data block also shows the increase or decrease in traffic (+/- %) compared to the 2013 figures in last year’s book. Also included where available are average dwell times for each terminal/concourse, which is the average time spent by passengers before a flight in the given terminal.</dd>
      
      <dt>Program Details – Category Breakdowns:</dt>
      <dd>Shows the same information broken down by category, i.e. food/beverage, specialty retail, news/gifts, duty-free and advertising, by terminal/concourse. This section includes gross sales, sales per enplanement, rent revenue to the airport and current square footages for each category in each terminal/concourse. The book also includes revenues from passenger services, currency exchange and details on revenues from parking and car rental. These figures represent airportwide information only.</dd>
      <dt>Ratio of Business to Leisure:</dt>
      <dd>The percentage of total enplaning passengers that are traveling on business versus the percentage traveling for leisure or pleasure, i.e. 75/25 would translate to 75% of the people departing the airport are taking a business trip and 25% are on a vacation trip.</dd>
      
      <dt>Ratio of Pre- to Post- Security:</dt>
      <dd>The percentage of concessions space that is located pre- versus post-security, i.e. 20/80 would translate to 20% of the concession space is located before security and 80% is located beyond security.</dd>
      
      <dt>Rent Per Enplanement: </dt>
      <dd>The estimated amount of money paid to the airport in the form of rent on a per passenger basis. This figure is derived by dividing the total amount of rent from a given category by the number of enplaning passengers.</dd>
      
      <dt>Sales Per Enplanement:</dt>
      <dd>The estimated amount of money each departing passenger is spending before boarding their flight. This figure is a standard measure of performance in the industry and is derived by dividing the total gross sales figure by the number of enplaning passengers.</dd>
      <dt>Specialty Retail:</dt>
      <dd>A store that utilizes a majority of its space to sell non-news, non-food, retail products such as apparel, souvenirs, gift items, art products, jewelry, etc.</dd>
      
      <dt>Sq. Ft.</dt>
      <dd>Stands for square footage, or the amount of space occupied by the associated category.</dd>
      
      <dt>Tenant Details:</dt>
      <dd>Name of each outlet or store (all outlets are grouped together by category); company licensed to operate the concept; general description of the primary products sold in the outlet; terminal/concourse in which the store is located; number of locations occupied by the concept; total square footage of location(s); and date on which the company’s lease expires. In some cases, a lease may have expired but the airport has extended the contract after the publication of this book.</dd>
      
      <dt>Terminal Configuration:</dt>
      <dd>A brief description of the airport’s terminal layout.</dd>
      <dt>+/- and %</dt>
      <dd>A comparison of numbers from the current Fact Book year to the previous. Currently 2014 numbers compared to 2013. All symbols should be typed included. i.g. +2.4 % or -3.1 % .</dd>
      
    </dl>
                </div>
       </div>
      </div>
    </div>
  </div>
					  </div>
                    </div>                     
                </div>                
            </section>
        </section>
        <?php echo $this->load->view($footer);?>
    </div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
</body>
</html>
