<?php echo $this->load->view($header); ?>

<title>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/choseen.css">
<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />-->
</head>
<body>
    <div class="main-wrapper">
        <?php echo $this->load->view($menu); ?>
        <div class="clearfix"></div>       
        <section class="main-content">        	        	
            <?php /* ?><section class="col-sm-2">
              <?php  	  foreach($company_search_adds as $adds): ?>
              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
              <?php endforeach;   ?>
              </section><?php */ ?>
            <section class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $this->load->view('frontend/include/breadcrumb'); ?>
                    </div>                     
                </div>
                <?php echo $this->common->getmessage(); ?>
                <div class="row">
                    <div class="col-sm-12" style="font-family:verdana;">
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                            <div class="btn-group" role="group">
                                <a  href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>/edit/step1" class="btn text-uppercase<?php
                                if ($this->uri->segment(5) == "step1") {
                                    echo " btn-warning";
                                } else {
                                    echo " btn-primary";
                                }
                                ?>">
                                    <u>Step 1</u><br /> Contacts
                                </a>                                
                            </div>
                            <div class="btn-group " role="group">
                                <a  href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>/edit/step2" class="btn text-uppercase<?php
                                if ($this->uri->segment(5) == "step2") {
                                    echo " btn-warning";
                                } else {
                                    echo " btn-primary";
                                }
                                ?>">
                                    <u>Step 2</u><br /> company info
                                </a>                                
                            </div>
                            <div class="btn-group" role="group">
                                <a  href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>/edit/step3" class="btn text-uppercase<?php
                                if ($this->uri->segment(5) == "step3") {
                                    echo " btn-warning";
                                } else {
                                    echo " btn-primary";
                                }
                                ?>">
                                    <u>Step 3</u><br /> location
                                </a>
                            </div>
                            <div class="btn-group" role="group">
                                <a href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>" class="btn text-uppercase<?php
                                if ($this->uri->segment(4) == "") {
                                    echo " btn-warning";
                                } else {
                                    echo " btn-primary";
                                }
                                ?>">
                                    <u>Step 4</u><br /> Review
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-12">

                        <h4><p class="text-default"><strong><?php echo $csinfo[0]['companyname']; ?></strong></p>
                            <p class="text-danger"><strong><?php echo $csinfo[0]['companytype']; ?></strong></p></h4>
                    </div>
                </div>
                <br/>
                <div class="panel panel-primary">
                    <div class="panel-heading">Company Info</div>
                    <div class="panel-body">                        	
                        <form action="<?php echo current_url(); ?>" method="post">
                            <input type="hidden" name="cid" value="<?php echo $csinfo[0]['cid']; ?>" />
                            <input type="hidden" name="action" value="contact" />
                            <input type="hidden" name="actiontype" value="updatecompnayinfo" />                              <div class="form-group">
                                <label for="CompanyType">1. Company Type:</label>
                                <select class="chosen form-control" name="companytype">
                                    <?php foreach ($ctype as $c): ?>
                                        <option value="<?php echo $c['companytype']; ?>" 
                                                <?php if ($csinfo[0]['companytype'] == $c['companytype']) { ?> selected="selected" <?php } ?>>
                                                    <?php echo $c['companytype']; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">2. Are you a certified Disadvantaged Business Enterprise? 	</label>
                                <select class="chosen form-control" name="dbe">
                                    <option value="?" <?php if ($csinfo[0]['dbe'] == "?") { ?> selected="selected" <?php } ?>>?</option>
                                    <option value="Yes"<?php if ($csinfo[0]['dbe'] == "Yes") { ?> selected="selected" <?php } ?>>Yes</option>
                                    <option value="No" <?php if ($csinfo[0]['dbe'] == "No") { ?> selected="selected" <?php } ?>>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">3. Enter a detailed description of your company as you wished published in the Online Fact Book.<br><p class="text-danger">Text is limited to 2500 characters or roughly 500 words.</p> 	</label>
                                <!--<textarea class="form-control" style="resize:none;" rows="16" name="companyinfo" placeholder="description"><?php // echo $this->typography->format_characters(str_replace(array('â€', 'â€“'), array('"', '–') ,htmlspecialchars_decode($csinfo[0]['companyinfo'], ENT_HTML5 ))); ?></textarea>-->
                                <textarea class="form-control" style="resize:none;" rows="16" name="companyinfo" placeholder="description"><?php echo $this->common->get_formatted_string_company($csinfo[0]['companyinfo']); ?></textarea>
                                
                                
                                <!--<textarea class="form-control" style="resize:none;" rows="16" name="companyinfo" placeholder="description"><?php // echo html_entity_decode($csinfo[0]['companyinfo']); ?></textarea>-->
                                <!--<textarea class="form-control" style="resize:none;" rows="16" name="companyinfo" placeholder="description"><?php // $this->load->library('portable-utf8',$config); echo preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $csinfo[0]['companyinfo']); ?></textarea>-->
                                <!--<textarea class="form-control" style="resize:none;" rows="16" name="companyinfo" placeholder="description"><?php //  echo $this->portable_utf8->utf8_split((string) $csinfo[0]['companyinfo']); ?></textarea>-->
                                <!--<textarea class="form-control" style="resize:none;" rows="16" name="companyinfo" placeholder="description"><?php // echo $this->typography->format_characters($csinfo[0]['companyinfo']); ?></textarea>-->
                            </div>                                                            
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Save Changes</button>
                            &nbsp;<a href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)) ?>" class="btn btn-danger">
                                <i class="fa fa-times"></i>&nbsp;Cancel
                            </a> 
                        </form>
                    </div>
                </div>    

            </section>

            <div class="col-sm-12" style="font-family:verdana;">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">

                        <div class="col-sm-2 pull-left" style="padding-left: 0px;">
                            <label class="control-label">Next, click on </label>
                            <a  href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>/edit/step3" class="btn text-uppercase<?php
                            if ($this->uri->segment(5) == "step3") {

                                echo " btn-warning";
                            } else {

                                echo " btn-primary";
                            }
                            ?>">
                                <u>Step 3</u><br /> location
                            </a>               
                        </div>
                    </div>  
                </div>
            </div>

        </section>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="hidden" id="recdelid" value="">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Conifm Delete</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure want to delete this?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <i class="fa fa-times">&nbsp;</i>Cancel</button>        
                        <button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete
                        </button>        
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->load->view($footer); ?>
    </div>
    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        
    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('fassests'); ?>/js/chosen.jquery.min.js"></script>
    <script type="text/javascript">

        var seg = "<?php echo $this->uri->segment(0) ?>";

        if (seg != '') {
            $('title').html("Modify Data Company Step2 | ARN Fact Book " + seg);
        }
        else {
            $('title').html("Modify Data Company Step2 | ARN Fact Book");
        }
        function showmodal(bit)
        {
            if (bit == 1)
            {
                $("#myModal").modal("show");
            } else
            {
                $("#myModal").modal("hide");
            }
        }
        function delme(idd)
        {
            $("#recdelid").attr("value", idd);
            showmodal(1);
        }
        $(".suredel").click(function ()
        {
            var delid = $("#recdelid").val();
            $(this).html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Deleteing...');
            $.ajax({
                url: "<?php echo site_url('ajax/doaction'); ?>",
                type: "POST",
                cache: false,
                data: "affid=" + delid + "&action=delcontact",
                success: function (html)
                {
                    if (html == 1)
                    {
                        showmodal(0);
                        window.location = "<?php echo current_url(); ?>";
                    } else
                    {
                        $(".modal-body").html(html);
                    }
                }
            });

        });

        jQuery(document).ready(function () {
            jQuery(".chosen").chosen();
        });
    </script>
</body>
</html>