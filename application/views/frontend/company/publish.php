<?php echo $this->load->view($header); ?>
<title>
    
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">
</head>
<body>
    <div class="main-wrapper">
        <?php echo $this->load->view($menu); ?>
        <div class="clearfix"></div>       
        <section class="main-content">        	        	
      <!--             <section class="col-sm-2">
            <?php //  	  foreach($company_search_adds as $adds): ?>
      <a href="<?php // echo $adds['adlink']; ?>" target="_new"><img src="<?php // echo base_url(); ?><?php // echo s$adds['imglink']; ?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
            <?php // endforeach;   ?>	
                   </section>-->
            <section class="col-sm-12">           
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $this->load->view('frontend/include/breadcrumb'); ?>                         
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="font-family:verdana;">
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                            <div class="btn-group" role="group">
                                <a  href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>/edit/step1" class="btn text-uppercase<?php if ($this->uri->segment(5) == "step1") {
                            echo " btn-warning";
                        } else {
                            echo " btn-primary";
                        } ?>">
                                    <u>Step 1</u><br /> Contacts
                                </a>                                
                            </div>
                            <div class="btn-group " role="group">
                                <a  href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>/edit/step2" class="btn text-uppercase<?php if ($this->uri->segment(5) == "step2") {
                            echo " btn-warning";
                        } else {
                            echo " btn-primary";
                        } ?>">
                                    <u>Step 2</u><br /> company info
                                </a>                                
                            </div>
                            <div class="btn-group" role="group">
                                <a  href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>/edit/step3" class="btn text-uppercase<?php if ($this->uri->segment(5) == "step3") {
                            echo " btn-warning";
                        } else {
                            echo " btn-primary";
                        } ?>">
                                    <u>Step 3</u><br /> location
                                </a>
                            </div>
                            <div class="btn-group" role="group">
                                <a href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>" class="btn text-uppercase<?php if ($this->uri->segment(4) == "") {
                            echo " btn-warning";
                        } else {
                            echo " btn-primary";
                        } ?>">
                                    <u>Step 4</u><br /> Review
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <br />                 
<?php
//print_r($this->session->all_userdata());
?>
                <div class="panel panel-primary">
                    <div class="panel-heading">Request to Publish</div>
                    <div class="panel-body">
                        <div class="error"></div>
                        <form class="form-horizontal" onSubmit="return checkvali()" action="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>" method="post">
                            <input type="hidden" name="cid" value="<?php echo $this->uri->segment(3); ?>">
                            <input type="hidden" name="action" value="publish">
                            <input type="hidden" name="actiontype" value="publish">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Your Name : </label>
                                <div class="col-sm-6">
                                    <input  name="name" type="text" id="name" value="<?php echo $this->common->GetSessionKey('first_name') . " " . $this->common->GetSessionKey('last_name'); ?>"  size="50" maxlength="75" class="form-control" placeholder="Your Name" tabindex="1" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Your Email : </label>
                                <div class="col-sm-6">
                                    <input name="email" type="text" id="email" value="<?php echo $this->common->GetSessionKey('email'); ?>" size="50" class="form-control" size="50" maxlength="100"  placeholder="Your Email" tabindex="2" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Comment : </label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="comments" name="comments" placeholder="Type yours Comment here.." tabindex="3"></textarea>
                                </div>
                            </div>                          
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Submit</button>
                                    <a href="<?php echo site_url('wizard/company/' . $this->uri->segment(3)); ?>" class="btn btn-danger"><i class="fa fa-times">&nbsp;</i>Cancel</a>
                                </div>                            
                            </div>
                        </form>
                        <div class="alert alert-danger">
                            <p><strong>Please Read!</strong></p>
                            <p>This form will be sent to the staff at ARN, to notify them that you have completed this data entry process.</p>
                            <p>&nbsp;</p>
                            <p>You may return as often as you'd like, to review your information.</p>
                            <p><strong>VERY IMPORTANT -</strong></p>
                            <p>Should you need to modify any data after sending this "Request to Publish" notification, feel free to do so, but it is imperative that you click on publish every time, so the staff is aware of changes.</p>
                            <p>&nbsp;</p>
                            <p>Thank you, from Airport Revenue News.</p>
                        </div>
                    </div>
                </div>
            </section>                 
        </section>
<?php echo $this->load->view($footer); ?>
    </div>
    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        
    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript">

                                var seg = "<?php echo $this->uri->segment(0) ?>";

                                if (seg != '') {
                                    $('title').html("Modify Data Company Publish | ARN Fact Book " + seg);
                                }
                                else {
                                    $('title').html("Modify Data Company Publish | ARN Fact Book");
                                }

                                function checkvali()
                                {
                                    var comments = $("#comments").val();
                                    if (comments == "" || comments.replace("", " ") == "")
                                    {
                                        $(".error").html('<div class="alert alert-danger">Please enter Comment..</div>');
                                    } else
                                    {
                                        $(".error").html('<div class="alert alert-success"><i class="fa fa-spin fa-spinner">&nbsp;</i>Wait..</div>');
                                        return true;
                                    }
                                    return false;
                                }
    </script>
</body>
</html>
