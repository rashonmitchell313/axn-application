<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Contact Us | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Contact Us | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>       
        <section class="main-content">        	
				<?php /*?><section class="col-sm-2">
<?php foreach($contact_us_adds as $adds): ?>
<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
<?php endforeach; ?>				
				</section><?php */?>		     	
        	<section class="container-fluid">
            	 <?php echo $this->load->view('frontend/include/breadcrumb');?>            	            	
                <div class="col-md-12 col-sm-12 col-xs-12 login-right-sect">                	
                	<div class="row">                    	
                        	<div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-envelope"></i>&nbsp;Mailing Address</h3>
                              </div>
                              <div class="panel-body">
                                <h1 class="default-title">Airport Revenue News</h1>
                                <address>
                                  3200 N. Military Trail.<br>
                                  Suite 110<br>
                                  Boca Raton, FL 33431<br>                                  
                                </address>
                                <!--<div id="map" style="width:100%;min-height:400px;"></div>-->
                                <img class="" src="<?php echo base_url('fassests'); ?>/images/address.png" alt="" style="width:100%;min-height:400px;max-height: 500px;" />
                              </div>
                            </div>
                            <div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-envelope"></i>&nbsp;Emails and Phone Extensions</h3>
                              </div>
                              <div class="panel-body table-responsive">
                               <table class="table table-hover">                                  
                                  <tbody>
                                    <tr>
                                      <td scope="row"><i class="fa fa-phone"></i>&nbsp;<strong>Phone:</strong></td>
                                      <td>561.477.3417</td> 
                                      <td>&nbsp;</td>                                                     
                                      </tr>
                                     <tr>                                      
                                      <td scope="row"><i class="fa fa-fax"></i>&nbsp;<strong>Fax: </strong></td>
                                      <td colspan="2">561.228.0882</td>                                      
                                    </tr>
                                     <tr>                                      
                                      <td scope="row" colspan="3">&nbsp;</td>                                     
                                    </tr>                                    
                                    <tr>
                                      <th scope="row"><i class="fa fa-arrow-circle-right"></i>&nbsp;
                                      <a href="mailto:info@airportrevenuenews.com">Information</a></th>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>                                      
                                    </tr>
                                    <?php	
									$contactlist=array();
									$contactlist[]=array('Andrea Caballero','andrea@airportrevenuenews.com','Business Development Assistant');
									$contactlist[]=array('Barb Moreno','barb@armbrustaviation.com','Director of Conference','<i class="fa fa-phone"></i>&nbsp;561.257.1024');
									$contactlist[]=array('Claire Cole','claire@airportrevenuenews.com','Editorial Assistant');
                                                                        $contactlist[]=array('Donna Crowley','donna@airportrevenuenews.com','Administrative Coordinator','<i class="fa fa-phone"></i>&nbsp;561.257.1026');
                                                                        $contactlist[]=array('Beth Hanlon','beth@armbrustaviation.com','Business Manager','<i class="fa fa-phone"></i>&nbsp;561.257.1025');
                                                                        $contactlist[]=array('Shawn Hanlon','shawn@airportrevenuenews.com','Technical Services Coordinator');
									$contactlist[]=array('Ramon G. Lo','ramon@airportrevenuenews.com','Publisher','<i class="fa fa-phone"></i>&nbsp;561.257.1020');
									$contactlist[]=array('John Manly','john@airportrevenuenews.com','Creative Services Manager');
                                                                        $contactlist[]=array('Howard Mintz','howard@airportrevenuenews.com','Director of Business Development & Finance','<i class="fa fa-phone"></i>&nbsp;561.257.1023');
                                                                        $contactlist[]=array('Melissa Montes','mel@airportrevenuenews.com','Director of Business Development & Marketing','<i class="fa fa-phone"></i>&nbsp;561.257.1022');
                                                                        $contactlist[]=array('Roger Schinkler','roger@airportrevenuenews.com','Director of Creative Services','<i class="fa fa-mobile"></i>&nbsp;954.850.9119');
                                                                        $contactlist[]=array('Andrew Tellijohn','andrew@airportrevenuenews.com','Senior Reporter','<i class="fa fa-phone"></i>&nbsp;612.827.5290 ,&nbsp;&nbsp;<i class="fa fa-mobile"></i>&nbsp;612.702.5373');
                                                                        $contactlist[]=array('Carol Ward','carol@airportrevenuenews.com','Editorial Director','<i class="fa fa-mobile"></i>&nbsp;619.750.7736');
										
																	
									 foreach($contactlist as $contact):
									 ?>
                                    <tr>
                                      <th scope="row"><i class="fa fa-arrow-circle-right"></i>&nbsp;
                                      <a href="mailto:<?php echo  $contact[1]; ?>">
                                      <?php echo  $contact[0]; ?></a></th>
                                      <td><?php echo  $contact[2]; ?></td>
                                      <td><?php echo  $contact[3]; ?></td>                                      
                                    </tr>
                                    <?php endforeach; ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>                        
                    </div>   
                </div>                     
                </div>                
            </section>
        </section>
        <?php echo $this->load->view($footer);?>
    </div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>

<script>
      /*      var latt;
            var longg;
//            latt = 26.3815616;
//            longg = -80.1201957;
            latt = 26.3815844;
            longg = -80.1207369;
            var myLatLng;
            var map;
            function initMap() {
                var myLatLng = {lat: latt, lng: longg};
                map = new google.maps.Map(document.getElementById('map'), {
                    center: myLatLng,
                    zoom: 20
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map
                });
            }
*/
        </script>
<!--        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXrRAa4kiHZF8A5YPAeruoZIkVKBXsPu0&callback=initMap"
        async defer></script>-->
</body>
</html>
