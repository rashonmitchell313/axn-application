<?php echo $this->load->view($header);?>
<title>
<?php 
	if($this->uri->segment(0)!='')
	{
	echo 'Welcome to ARN Fact Book | Login '.$this->uri->segment(0);
	
	}
	else 
	{
	echo 'Welcome to ARN Fact Book | Login';
	}
?>
</title>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu); ?>
        <div class="clearfix"></div>
        <section class="main-content">
        	<section class="container-fluid">
            	<div class="row text-center">
                    <h1 class="default-title" style="font-size:6em;">404 <br/>Oops! The Page you requested was not found.&nbsp;&nbsp;<a href="<?php echo base_url();?>" title="Go to home page">Home page</a></h1>
                    
                </div>	                                 
            </section>
        </section>
        <?php echo $this->load->view($footer); ?>  
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>              
    </div>
</body>
</html>
