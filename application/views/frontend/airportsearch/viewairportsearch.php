<?php echo $this->load->view($header); ?>



<title>



    <?php

    if ($this->uri->segment(0) != '') {



        echo 'Airport Search | ARN Fact Book ' . $this->uri->segment(0);

    } else {



        echo 'Airport Search | ARN Fact Book';

    }

    ?>



</title>



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables2.bootstrap.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >



<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/choseen.css">



<style>



    .dataTables_wrapper .dataTables_paginate .paginate_button {



        padding : 0px;



        margin-left: 0px;



        display: inline;



        border: 0px;



    }



    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {



        border: 0px;



    }



    .close1{



        cursor:pointer;

        float: right;

        font-size: 21px;

        font-weight: bold;

        line-height: 1;

        color: #000;



    }



</style>



</head>



<body>



    <div class="main-wrapper">



        <?php echo $this->load->view($menu); ?>



        <div class="clearfix"></div>   



        <section class="main-content">   



            <section class="container-fluid">



                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>            	            	



                <div class="row"> 



                    <div class="col-sm-12 hidden">                    

                        <span type="button" id="sidebar2" class="btn btn-success" data-toggle="tooltip" data-title="Click Here to view search box.">

                            <i class="fa fa-angle-double-right" data-icon1="fa fa-angle-double-left" data-icon2="fa fa-angle-double-right"></i>&nbsp;</span>

                        <br/>

                        <br/>

                    </div>



                    <div class="col-sm-12"> 



                        <?php echo $this->common->getmessage(); ?>                   	                        

                        <div  id="hidee">

                            <div class="col-sm-3 padding-left-none padding-right-none">



                                <div class="panel panel-primary" >



                                    <div class="panel-heading">Airport Search &nbsp;&nbsp;<span class="close1" id="sidebar" data-toggle="tooltip" data-title="Click Here to expand the view."><i style="color:#FFF;" class="fa fa-angle-double-left"></i></span></div>



                                    <div class="panel-body">



                                        <div class="alert alert-info">



                                            <strong>How to use the search.</strong><br />



                                            Select either from the Location/City list or the IATA Code list then click on Search.



                                        </div>



                                        <div class="error hide">



                                            <small>



                                                <div class="alert alert-danger">



                                                    <b>Error!</b> Please Select either from the Location/City list or the IATA Code list then click on Search.



                                                </div>



                                            </small>



                                        </div>



                                        <form action="<?php echo site_url('airportsearch'); ?>" method="post" onSubmit="return valid();">



                                            <input type="hidden" name="action" value="sairports">



                                            <input type="hidden" name="load" value="Y">	 											                                      <input type="hidden" name="saveaff" value="1">



                                            <div class="form-group">



                                                <label for="exampleInputEmail1">Location / City:</label>



                                                <select class="chosen form-control"  name="c_aid" id="c_aid">



                                                    <option value="" selected="selected">Please Select</option>



                                                    <?php foreach ($listoflocation as $loc): ?>



                                                        <option value="<?php echo $loc['aid']; ?>" <?php if ($this->input->post('c_aid') != "" && $this->input->post('c_aid') == $loc['aid']) { ?> selected="selected" <?php } ?>><?php echo $loc['acity'] . " , " . $loc['astate'] . " ==> " . $loc['IATA'] . ""; ?></option>



                                                    <?php endforeach; ?>



                                                </select>



                                            </div>



                                            <div class="form-group">



                                                <label for="exampleInputPassword1">IATA Code:</label>



                                                <select class="chosen form-control"  name="i_aid" id="i_aid">



                                                    <option value="" selected="selected">Please Select</option>



                                                    <?php foreach ($listofiata as $IATA): ?>



                                                        <option value="<?php echo $IATA['aid']; ?>" <?php if ($this->input->post('i_aid') != "" && $this->input->post('i_aid') == $IATA['aid']) { ?> selected="selected" <?php } ?> >



                                                            <?php echo $IATA['IATA']; ?>



                                                        </option>



                                                    <?php endforeach; ?>



                                                </select>



                                            </div>             



                                            <button type="reset" id="reset" class="btn btn-danger reset"><i class="fa fa-times"></i>&nbsp;Reset</button>



                                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>



                                        </form>                                   



                                    </div>



                                </div>



                            </div> 



                        </div>



                        <div id="expand" class="col-sm-9 padding-right-none airport-search-padding-none expand">



                            <?php if (count($ainfo) > 0) { ?>



                                <div class="panel panel-primary">



                                    <div class="panel-heading">



                                        <strong>



                                            <?php echo $ainfo[0]['aname'] . " (" . $ainfo[0]['IATA'] . ")"; ?><br />                                        	<?php echo $ainfo[0]['acity'] . ", " . $ainfo[0]['astate'] . " ", $ainfo[0]['acountry'] . "."; ?>



                                        </strong>



                                    </div>



                                    <div class="panel-body">                                   



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="headingOne">



                                                <h4 class="panel-title">



                                                    <a data-toggle="collapse" data-parent="#accordion" 



                                                       href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">



                                                        Find us on google map



                                                    </a>



                                                </h4>



                                            </div>



                                            <div id="collapseOne" class="panel-collapse collapse in" 



                                                 role="tabpanel" aria-labelledby="headingOne">



                                                <div class="panel-body">



                                                    <?php

                                                    $latitude = number_format($ainfo[0]['Latitude'], 6, '.', '');



                                                    $Longitude = number_format($ainfo[0]['Longitude'], 6, '.', '');

                                                    ?>



                                                    <div id="map" style="width:100%;min-height:300px;"></div>                                     



                                                </div>



                                            </div>



                                        </div>                                    



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="contact" data-toggle="tooltip" title="Click on Text to Close / Open">



                                                <h4 class="panel-title">



                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">



                                                        Contact(s)



                                                    </a>



                                                </h4>



                                            </div>



                                            <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contact">



                                                <br />



                                                <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 



                                                    <ul id="myTabs" class="nav nav-tabs" role="tablist"> 



                                                        <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><i class="margin-right-5 fa fa-qrcode">&nbsp;</i>Grid View</a></li> 



                                                        <li role="presentation" class="" ><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="margin-right-5 fa fa-table">&nbsp;</i>Table View</a></li>



                                                    </ul> 



                                                    <br />



                                                    <div id="myTabContent" class="tab-content"> 



                                                        <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">



                                                            <div class="panel-body">



                                                                <?php

                                                                if (count($airporcontactinfo) > 0) {



                                                                    foreach ($airporcontactinfo as $contact):

                                                                        ?>



                                                                        <div class="col-sm-6 padding-left-none">



                                                                            <div class="panel panel-primary" style="min-height:344px;">



                                                                                <div class="panel-heading">



                                                                                    <small><?php echo $contact['afname'] . " " . $contact['alname']; ?> (<b><?php echo $contact['mgtresponsibilityname']; ?></b>)</small></div>



                                                                                <div class="panel-body">



                                                                                    <div class="table-responsive">



                                                                                        <small>



                                                                                            <table class="table-bordered table-hover table-striped" width="100%">



                                                                                                <tbody><tr>



                                                                                                        <td width="100"><i class="fa fa-phone margin-right-5"></i>&nbsp;Contact</td>



                                                                                                        <td><?php echo $contact['afname'] . " " . $contact['alname']; ?></td>            



                                                                                                    </tr>



                                                                                                    <tr>



                                                                                                        <td><i class="margin-right-5 fa fa-book margin-right-5"></i>&nbsp;Title</td>



                                                                                                        <td><?php echo $contact['atitle']; ?></td>            



                                                                                                    </tr>



                                                                                                    <tr>



                                                                                                        <td><i class="margin-right-5 fa fa-building margin-right-5"></i>&nbsp;Company</td>



                                                                                                        <td><?php echo $contact['acompany']; ?></td>            



                                                                                                    </tr>



                                                                                                    <tr>



                                                                                                        <td><i class="margin-right-5 fa fa-home"></i>&nbsp;Address</td>



                                                                                                        <td><?php echo $contact['aaddress1'] . ' ' . $contact['aaddress2'] . ' ' . $contact['accity'] . ',' . ' ' . $contact['acstate'] . ' ' . $contact['aczip'] . ' ' . $contact['accountry']; ?></td>            



                                                                                                    </tr>



                                                                                                    <tr>



                                                                                                        <td><i class="margin-right-5 fa fa-phone"></i>&nbsp;Phone</td>



                                                                                                        <td><?php echo $contact['aphone']; ?></td>            



                                                                                                    </tr>



                                                                                                    <tr>



                                                                                                        <td><i class="margin-right-5 fa fa-fax"></i>&nbsp;Fax</td>



                                                                                                        <td><?php echo $contact['afax']; ?></td>            



                                                                                                    </tr>



                                                                                                    <tr>



                                                                                                        <td><i class="margin-right-5 fa fa-envelope-o"></i>&nbsp;E-mail</td>



                                                                                                        <td><a href="mailto:<?php echo $contact['aemail']; ?>"><?php echo $contact['aemail']; ?></a></td>            



                                                                                                    </tr>



                                                                                                    <tr>



                                                                                                        <td><i class="margin-right-5 fa fa-globe"></i>&nbsp;Web site</td>



                                                                                                        <td><a href="<?php echo $this->common->weblink($contact['awebsite']); ?>" target="_new"><?php echo $contact['awebsite']; ?></a></td>            



                                                                                                    </tr>



                                                                                                </tbody></table>



                                                                                        </small> 



                                                                                    </div>



                                                                                </div>                                   



                                                                                <div class="clearfix"></div>



                                                                            </div>



                                                                            <br/>



                                                                        </div>



                                                                        <?php

                                                                    endforeach;

                                                                } else {

                                                                    ?>



                                                                    <div class="alert alert-danger">



                                                                        No Result found...



                                                                    </div>



                                                                    <?php

                                                                }

                                                                ?>	



                                                            </div>



                                                        </div>



                                                        <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">



                                                            <div class="panel-body">



                                                                <div class="table-responsive">



                                                                    <table  width="100%" id="table-view-Data" class="table-bordered table-hover table-striped contact2">



                                                                        <thead>



                                                                            <tr  style="background-color:#eee;">



                                                                                <th>Category</th>



                                                                                <th>Contact</th>



                                                                                <th>Title</th>



                                                                                <th>Company</th>



                                                                                <th>Address</th>



                                                                                <th>Phone</th>



                                                                                <th>Fax</th>



                                                                                <th>E-mail</th>



                                                                                <th>Web site</th>



                                                                            </tr>



                                                                        </thead>



                                                                        <tbody>



                                                                            <?php

                                                                            if (count($airporcontactinfo) > 0) {



                                                                                foreach ($airporcontactinfo as $contact):

                                                                                    ?>



                                                                                    <tr>



                                                                                        <td><?php echo $contact['mgtresponsibilityname']; ?></td> 



                                                                                        <td><?php echo $contact['afname'] . " " . $contact['alname']; ?></td> 



                                                                                        <td><?php echo $contact['atitle']; ?></td>



                                                                                        <td><?php echo $contact['acompany']; ?></td>



                                                                                        <td><?php echo $contact['aaddress1'] . ' ' . $contact['aaddress2'] . ' ' . $contact['accity'] . ',' . ' ' . $contact['acstate'] . ' ' . $contact['aczip'] . ' ' . $contact['accountry']; ?></td>



                                                                                        <td><?php echo $contact['aphone']; ?></td> 



                                                                                        <td><?php echo $contact['afax']; ?></td>



                                                                                        <td><a href="mailto:<?php echo $contact['aemail']; ?>"><?php echo $contact['aemail']; ?></a></td>



                                                                                        <td><a href="<?php echo $this->common->weblink($contact['awebsite']); ?>" target="_new"><?php echo $contact['awebsite']; ?></a></td>



                                                                                    </tr>



                                                                                    <?php

                                                                                endforeach;

                                                                            } else {

                                                                                ?>



                                                                            <div class="alert alert-danger">



                                                                                No Result found...



                                                                            </div>



                                                                            <?php

                                                                        }

                                                                        ?>



                                                                        </tbody>



                                                                    </table>



                                                                </div>



                                                            </div>



                                                        </div>



                                                    </div>



                                                </div>



                                            </div>



                                        </div>



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="headingThree">



                                                <h4 class="panel-title">



                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">



                                                        Airportwide Information 



                                                    </a>



                                                </h4>



                                            </div>



                                            <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">



                                                <div class="panel-body">



                                                    <h1 class="default-title">Airport Info</h1><br />



                                                    <?php if (count($terminalinfo) > 0) { ?>



                                                        <div class="row">



                                                            <div class="col-sm-5">



                                                                <h3 class="default-title" style="font-size:1.2em">Airport Configuration 

                                                                    <i class="fa fa-question-circle" data-original-title="Describes the shape of your airport. For example, T-shaped, U-shaped, Fingers, etc." 

                                                                       data-container="body"data-toggle="tooltip"></i></h3>



                                                            </div>



                                                            <div class="col-sm-5">



                                                                <strong><i><?php echo $ainfo[0]['configuration']; ?></i></strong>



                                                            </div>



                                                        </div>



                                                        <div class="row">



                                                            <div class="col-sm-5">



                                                                <h3 class="default-title" style="font-size:1.2em">Concessions Mgt. Type</em></h3>



                                                            </div>



                                                            <div class="col-sm-5">



                                                                <strong><i><?php echo $ainfo[0]['mgtstructure']; ?></i></strong>



                                                            </div>



                                                        </div>



                                                        <div class="row">



                                                            <div class="col-sm-5">



                                                                <h3 class="default-title" style="font-size:1.2em">&nbsp;</h3>



                                                            </div>



                                                            <div class="col-sm-5 tbl-abbreviation">



                                                                <table  width="100%" class="table table-bordered table-hover table-condensed table-responsive">



                                                                    <tr>



                                                                        <td><h3 class="default-title" style="font-size:1.2em">Terminals/Concourses</h3></td>



                                                                        <td><h3 class="default-title" style="font-size:1.2em">Abbreviation</h3></td>



                                                                    </tr>



                                                                    <?php

                                                                    foreach ($terminalinfo as $tm):

                                                                        ?>



                                                                        <tr>



                                                                            <td>



                                                                                <small><?php echo strtoupper($tm['terminalname']); ?></small>



                                                                            </td>



                                                                            <td>



                                                                                <small><?php echo strtoupper($tm['terminalabbr']); ?></small>



                                                                            </td>



                                                                        </tr>



                                                                        <?php

                                                                    endforeach;

                                                                    ?>



                                                                </table>	



                                                            </div>



                                                        </div>



                                                    <?php } else { ?>



                                                        <div class="alert alert-danger">



                                                            No result found...



                                                        </div>



                                                    <?php } ?>      



                                                </div>



                                            </div>



                                        </div>



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="headingFour">



                                                <h4 class="panel-title">



                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">



                                                        Passenger Traffic



                                                    </a>



                                                    <i class="fa fa-question-circle" data-original-title="A breakdown of passenger traffic by terminal or concourse, by enplaning (the number of passengers leaving the airport on departing flights) and deplaning (the number of passengers arriving at the airport on arriving flights), by international (outside the U.S.) and domestic (inside the U.S.)." 

                                                       data-container="body"data-toggle="tooltip"></i>



                                                </h4>



                                            </div>



                                            <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">



                                                <div class="panel-body">



                                                    <h1 class="default-title">Passenger Traffic</h1><br />



                                                    <div class="table-responsive">



                                                        <?php if (count($terminalinfo) > 0) { ?>



                                                            <table class="table table-striped table-hover table-bordered">



                                                                <thead>



                                                                    <tr class="info">



                                                                        <th>Terminal</th>



                                                                        <th>Total</th>



                                                                        <th>+/_% <i class="fa fa-question-circle" data-original-title="A comparison of numbers from the current Fact Book year to the previous. Currently <?php echo date("Y"); ?> numbers compared to <?php echo date("Y") - 1; ?>. All symbols should be typed included. i.g. +2.4 % or -3.1 %." 

                                                                                    data-container="body"data-toggle="tooltip"></i></th>



                                                                        <th>Deplaning <i class="fa fa-question-circle" data-original-title="The number of people disembarking an airline and arriving through the airport to baggage claim or ground transportation." 

                                                                                         data-container="body"data-toggle="tooltip">

                                                                        </th>



                                                                        <th>Enplaning <i class="fa fa-question-circle" data-original-title="Passengers departing the airport on airlines leaving the airport to their predetermined destinations." 

                                                                                         data-container="body"data-toggle="tooltip"></th>



                                                                        <th>EP Domestic</th>



                                                                        <th>EP Int'l</th>



                                                                    </tr>



                                                                </thead>



                                                                <?php

                                                                $TP = 0;



                                                                $ttpasstraffic = 0;



                                                                $ttdeplaning = 0;



                                                                $ttenplaning = 0;



                                                                $ttepdomestic = 0;



                                                                $ttepintl = 0;

                                                                $TP = str_replace(array("%", " "), "", $aainfo[0]['apasstrafficcompare']);

                                                                foreach ($terminalinfo as $pt):



//                                                                    $TP+=str_replace(array("%", " "), "", $pt['tpasstrafficcompare']);



                                                                    $ttpasstraffic+=$pt['tpasstraffic'];



                                                                    $ttdeplaning+=$pt['tdeplaning'];



                                                                    $ttenplaning+=$pt['tenplaning'];



                                                                    //$ttenplaning+=$pt['tenplaning'];



                                                                    $ttepdomestic+=$pt['tepdomestic'];



                                                                    $ttepintl+=$pt['tepintl'];

                                                                    ?>               



                                                                    <tbody>



                                                                        <tr scope="row">



                                                                            <td><?php echo $pt['terminalabbr']; ?></td>



                                                                            <td><?php echo number_format($pt['tpasstraffic']); ?></td> 



                                                                            <td><?php echo $pt['tpasstrafficcompare']; ?></td>                    



                                                                            <td><?php echo number_format($pt['tdeplaning']); ?></td> 



                                                                            <td><?php echo number_format($pt['tenplaning']); ?></td> 



                                                                            <td><?php echo number_format($pt['tepdomestic']); ?></td> 



                                                                            <td><?php

                                                                                if ($pt['tepintl'] == '') {

                                                                                    $pt['tepintl'] = 0;

                                                                                }

                                                                                echo number_format($pt['tepintl']);

                                                                                ?>

                                                                            </td>



                                                                        </tr>



                                                                    <?php endforeach; ?>



                                                                    <tr scope="row">



                                                                        <td><h1 class="default-title" style="font-size:1.3em;">Total</h1></td>



                                                                        <td><?php echo number_format($ttpasstraffic); ?></td>



                                                                        <td><?php echo $TP; ?></td>



                                                                        <td><?php echo number_format($ttdeplaning); ?></td>



                                                                        <td><?php echo number_format($ttenplaning); ?></td>



                                                                        <td><?php echo number_format($ttepdomestic); ?></td> 



                                                                        <td><?php echo number_format($ttepintl); ?></td>                    



                                                                    </tr>



                                                                </tbody>            



                                                            </table>



                                                        <?php } else { ?>



                                                            <div class="alert alert-danger">



                                                                No Result found...



                                                            </div>



                                                        <?php } ?>



                                                    </div>



                                                    <br/><br/>



                                                    <table class="table"> 



                                                        <tbody>



                                                            <tr>



                                                                <th align="left"><h1 class="default-title">Comment</h1></th>



                                                        <th align="left">&nbsp;</th>



                                                        <th align="right">&nbsp;</th>



                                                        <th align="right">&nbsp;</th>



                                                        </tr>



                                                        <tr>



                                                            <td colspan="4" align="left"><?php echo $aainfo[0]['apasscomment']; ?></td>



                                                        </tr>



                                                        </tbody>



                                                    </table>       



                                                </div>



                                            </div>



                                        </div>



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="headingFive">



                                                <h4 class="panel-title">



                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" 



                                                       href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">



                                                        Airport Specifics	



                                                    </a>



                                                </h4>



                                            </div>



                                            <div id="collapseFive" class="panel-collapse collapse in" 



                                                 role="tabpanel" aria-labelledby="headingFive">



                                                <div class="panel-body">



                                                    <h1 class="default-title">Airport Specifics</h1><br />        



                                                    <div class="table-responsive">



                                                        <table class="table table-striped table-hover table-bordered">                                             



                                                            <tbody>



                                                                <tr>



                                                                    <th scope="row">Pre/Post Security % of SqFt: <i class="fa fa-question-circle" data-original-title="The percentage of concessions space that is located pre- versus post-security, i.e. 20/80 would translate to 20% of the concession space is located before security and 80% is located beyond security." 

                                                                                                                    data-container="body"data-toggle="tooltip"></i></th>



                                                                    <td>Pre: <?php echo $aainfo[0]['presecurity']; ?>%</td>



                                                                    <td>Post: <?php echo $aainfo[0]['postsecurity']; ?>%</td>



                                                                </tr>



                                                                <tr>



                                                                    <th scope="row">Ratio of Business to Leisure: <i class="fa fa-question-circle" data-original-title="The percentage of total enplaning passengers that are traveling on business versus the percentage traveling for leisure or pleasure, i.e. 75/25 would translate to 75% of the people departing the airport are taking a business trip and 25% are on a vacation trip." 

                                                                                                                     data-container="body"data-toggle="tooltip"></i></th>



                                                                    <td><?php echo $aainfo[0]['ratiobusleisure']; ?></td>



                                                                    <td>&nbsp;</td>                                                  



                                                                </tr>



                                                                <tr>



                                                                    <th scope="row">O & D To Transfer: <i class="fa fa-question-circle" data-original-title="This is ratio denoting the percentage of origination and destination passengers relative to the percentage of passengers transferring from one flight to another in the airport." 

                                                                                                          data-container="body"data-toggle="tooltip"></i></th>



                                                                    <td><?php echo $aainfo[0]['ondtransfer']; ?></td>



                                                                    <td>&nbsp;</td>                                                  



                                                                </tr>



                                                                <tr>



                                                                    <th>Average Dwell Time: <i class="fa fa-question-circle" data-original-title="The average amount of time a departing passenger spends within the airport." 

                                                                                               data-container="body"data-toggle="tooltip"></i></th>



                                                                    <td><?php echo $aainfo[0]['avgdwelltime']; ?> min.</td>



                                                                    <td>&nbsp;</td> 



                                                                </tr>



                                                            </tbody>



                                                        </table>



                                                    </div>



                                                </div>



                                            </div>



                                        </div>



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="headingSix">



                                                <h4 class="panel-title">



                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" 



                                                       href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">



                                                        Gross Sales



                                                    </a>



                                                </h4>



                                            </div>



                                            <div id="collapseSix" class="panel-collapse collapse in" 



                                                 role="tabpanel" aria-labelledby="headingSix">



                                                <div class="panel-body">



                                                    <h1 class="default-title">Gross Sales</h1><br />        



                                                    <div class="table-responsive">                                           	



                                                        <?php

                                                        $ttenplaning = 0;



                                                        foreach ($agrosssale as $ags):





                                                            $tfbcurrsqft+=$ags['fbcurrsqft'];



                                                            $tfbgrosssales+=$ags['fbgrosssales'];



                                                            $tfbsalesep+=$ags['fbsalesep'];



                                                            $tfbrentrev+=$ags['fbrentrev'];





                                                            $tsrcurrsqft+=$ags['srcurrsqft'];



                                                            $tsrgrosssales+=$ags['srgrosssales'];



                                                            $tsrsalesep+=$ags['srsalesep'];



                                                            $tsrrentrev+=$ags['srrentrev'];





                                                            $tngcurrsqft+=$ags['ngcurrsqft'];



                                                            $tnggrosssales+=$ags['nggrosssales'];



                                                            $tngsalesep+=$ags['ngsalesep'];



                                                            $tngrentrev+=$ags['ngrentrev'];





                                                            $tdfcurrsqft+=$ags['dfcurrsqft'];



                                                            $tdfgrosssales+=$ags['dfgrosssales'];



                                                            $tdfsalesep+=$ags['dfsalesep'];



                                                            $tdfrentrev+=$ags['dfrentrev'];



                                                            $ttenplaning+=$ags['tenplaning'];

                                                            ?>



                                                            <table class="table table-striped table-hover table-bordered">                                             				<tbody>



                                                                <thead>



                                                                    <tr class="info">



                                                                        <th scope="row">Terminal - <?php echo $ags['terminalabbr']; ?></th>



                                                                        <th>Current Sq. Ft.</th>



                                                                        <th>Gross Sales</th>



                                                                        <th>Sales/EP</th>



                                                                        <th>Gross Rentals</th>



                                                                    </tr>



                                                                </thead>



                                                                <tbody>



                                                                    <tr>



                                                                        <th scope="row" align="right">Food & Beverage</th>



                                                                        <td><?php echo number_format($ags['fbcurrsqft']); ?></td>



                                                                        <td>$<?php echo number_format($ags['fbgrosssales'], 2); ?></td>



                                                                        <td>$<?php echo number_format($ags['fbgrosssales'] / $ags['tenplaning'], 2); ?></td>



                                                                        <td>$<?php echo number_format($ags['fbrentrev'], 2); ?></td>                                                </tr>



                                                                    <tr>



                                                                        <th scope="row">Specialty Retail</th>



                                                                        <td><?php echo number_format($ags['srcurrsqft']); ?></td>



                                                                        <td>$<?php echo number_format($ags['srgrosssales'], 2); ?></td>



                                                                        <td>$<?php echo number_format($ags['srgrosssales'] / $ags['tenplaning'], 2); ?></td>



                                                                        <td>$<?php echo number_format($ags['srrentrev'], 2); ?></td>                                                </tr>



                                                                    <tr>



                                                                        <th>News & Gifts:</th>



                                                                        <td><?php echo number_format($ags['ngcurrsqft']); ?></td>



                                                                        <td>$<?php echo number_format($ags['nggrosssales'], 2); ?></td>



                                                                        <td>$<?php echo number_format($ags['nggrosssales'] / $ags['tenplaning'], 2); ?></td>



                                                                        <td>$<?php echo number_format($ags['ngrentrev'], 2); ?></td>



                                                                    </tr>



                                                                    <tr>



                                                                        <th>Duty Free:</th>



                                                                        <td><?php echo number_format($ags['dfcurrsqft']); ?></td>



                                                                        <td>$<?php echo number_format($ags['dfgrosssales'], 2); ?></td>



                                                                                                                                                                                                                                                                                                                                                                                        <!--<td>$<?php // echo number_format($ags['dfgrosssales'] / $ags['tenplaning'], 2);                                       ?></td>-->

                                                                        <td>$<?php echo number_format($ags['dfsalesep'], 2); ?></td>



                                                                        <td>$<?php echo number_format($ags['dfrentrev'], 2); ?></td> 



                                                                    </tr>



                                                                    <tr>



                                                                        <td colspan="6">&nbsp;</td>



                                                                    </tr>



                                                                    <tr>



                                                                        <th>Enplanement</th>



                                                                        <td colspan="5">



                                                                            <?php echo number_format($ags['tenplaning']); ?>



                                                                        </td>



                                                                    </tr>



                                                                    <tr>



                                                                        <th>Deplanement</th>



                                                                        <td colspan="5">



                                                                            <?php echo number_format($ags['tdeplaning']); ?>



                                                                        </td>



                                                                    </tr>



                                                                    <tr>



                                                                        <th>Dominant Airline <i class="fa fa-question-circle" data-original-title="Name of the airline that occupies the most gates in the respective terminal." 

                                                                                                data-container="body"data-toggle="tooltip"></i></th>



                                                                        <td colspan="5">



                                                                            <?php echo strtoupper($ags['tdominantair']); ?>



                                                                        </td>



                                                                    </tr>



                                                                </tbody>



                                                            </table>



                                                            <br/>



                                                            <?php

                                                        endforeach;

                                                        ?>



                                                        <br/>



                                                        <table class="table table-striped table-hover table-bordered">                                             	<thead>



                                                                <tr class="success">



                                                                    <th scope="row">Terminal - Total</th>



                                                                    <th align="right">Current Sq. Ft.</th>



                                                                    <th align="right">Gross Sales</th>



                                                                    <th align="right">Sales/EP</th>



                                                                    <th align="right">Gross Rentals</th>



                                                                    <th align="right">Rent/EP</th>



                                                                </tr>



                                                            </thead>



                                                            <tbody>



                                                                <tr>                                                    



                                                                    <td>Food & Beverage</td>



                                                                    <td><?php echo number_format($tfbcurrsqft); ?></td>



                                                                    <td>$<?php echo number_format($tfbgrosssales, 2); ?></td>



                                                                    <td>$<?php echo number_format($tfbgrosssales / $ttenplaning, 2); ?></td>



                                                                    <td>$<?php echo number_format($tfbrentrev, 2); ?></td>



                                                                    <td>$<?php echo number_format($tfbrentrev / $ttenplaning, 2); ?></td>



                                                                </tr>



                                                                <tr>                                                    



                                                                    <td>Specialty Retail</td>



                                                                    <td><?php echo number_format($tsrcurrsqft); ?></td>



                                                                    <td>$<?php echo number_format($tsrgrosssales, 2); ?></td>



                                                                    <td>$<?php echo number_format($tsrgrosssales / $ttenplaning, 2); ?></td>



                                                                    <td>$<?php echo number_format($tsrrentrev, 2); ?></td>



                                                                    <td>$<?php echo number_format($tsrrentrev / $ttenplaning, 2); ?></td>



                                                                </tr>



                                                                <tr>                                                    



                                                                    <td>News & Gifts</td>



                                                                    <td><?php echo number_format($tngcurrsqft); ?></td>



                                                                    <td>$<?php echo number_format($tnggrosssales, 2); ?></td>



                                                                    <td>$<?php echo number_format($tnggrosssales / $ttenplaning, 2); ?></td>



                                                                    <td>$<?php echo number_format($tngrentrev, 2); ?></td>



                                                                    <td>$<?php echo number_format($tngrentrev / $ttenplaning, 2); ?></td>



                                                                </tr>



                                                                <tr>                                                    



                                                                    <td>Duty Free:</td>



                                                                    <td><?php echo number_format($tdfcurrsqft); ?></td>



                                                                    <td>$<?php echo number_format($tdfgrosssales, 2); ?></td>



                                                                    <td>$<?php echo number_format($aainfo[0]['adfsalesep'], 2); ?></td>



                                                                    <td>$<?php echo number_format($tdfrentrev, 2); ?></td>



                                                                    <td>$<?php echo number_format($aainfo[0]['adfrentep'], 2); ?></td>



                                                                </tr>



                                                                <tr class="info">



                                                                    <td>Airport Totals:<br/><b><i>(excludes Duty Free)</i></b></td>



                                                                    <td><?php echo number_format(($tfbcurrsqft + $tsrcurrsqft + $tngcurrsqft + $tdfcurrsqft) - $tdfcurrsqft); ?>



                                                                    </td>



                                                                    <td>$<?php echo number_format($tfbgrosssales + $tsrgrosssales + $tnggrosssales, 2); ?>



                                                                    </td>



                                                                    <td>$<?php echo number_format(($tnggrosssales / $ttenplaning) + ($tsrgrosssales / $ttenplaning) + ($tfbgrosssales / $ttenplaning), 2); ?>



                                                                    </td>



                                                                    <td>$<?php echo number_format($tfbrentrev + $tsrrentrev + $tngrentrev, 2); ?>



                                                                    </td>



                                                                    <td>$<?php echo number_format(($tfbrentrev / $ttenplaning) + ($tngrentrev / $ttenplaning) + ($tsrrentrev / $ttenplaning), 2); ?>	



                                                                    </td>



                                                                </tr>



                                                            </tbody>



                                                        </table>



                                                        <br>

                                                        <br>



                                                        <table class="table"> 



                                                            <tbody>



                                                                <tr>



                                                                    <th align="left"><h1 class="default-title">Gross Sales Comment</h1></th>



                                                            <th align="left">&nbsp;</th>



                                                            <th align="right">&nbsp;</th>



                                                            <th align="right">&nbsp;</th>



                                                            </tr>



                                                            <tr>



                                                                <td colspan="4" align="left"><?php echo $aainfo[0]['grosscomment']; ?></td>



                                                            </tr>



                                                            </tbody>



                                                        </table>



                                                    </div>



                                                </div>



                                            </div>



                                        </div>



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="headingSeven">



                                                <h4 class="panel-title">



                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" 



                                                       href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">



                                                        Airportwide Revenue	



                                                    </a>



                                                </h4>



                                            </div>



                                            <div id="collapseSeven" class="panel-collapse collapse in" 



                                                 role="tabpanel" aria-labelledby="headingSeven">



                                                <div class="panel-body">



                                                    <h1 class="default-title">Airportwide Revenue</h1><br />



                                                    <div class="table-responsive">



                                                        <table class="table table-striped table-hover table-bordered">                                             	



                                                            <tbody>



                                                                <tr class="info">



                                                                    <th width="30%"></th>



                                                                    <th width="22%"></th>



                                                                    <th align="right" width="22%">Revenue</th>



                                                                    <th align="right" width="22%">Revenue to Airport</th>



                                                                </tr>



                                                                <tr>



                                                                    <th align="left" colspan="2">Passenger Services</th>



                                                                    <td>$<?php echo number_format($aainfo[0]['passservicesrev'], 2); ?></td>



                                                                    <td>$<?php echo number_format($aainfo[0]['passservicesrevtoair'], 2); ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <th align="left" colspan="2">Advertising</th>



                                                                    <td>$<?php echo number_format($aainfo[0]['advertisingrev'], 2); ?></td>



                                                                    <td>$<?php echo number_format($aainfo[0]['advertisingrevtoair'], 2); ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <th align="left" colspan="2">Currency Exchange</th>



                                                                    <td>$<?php echo number_format($aainfo[0]['currencyexrev'], 2); ?></td>



                                                                    <td>$<?php echo number_format($aainfo[0]['currencyexrevtoair'], 2); ?></td>



                                                                </tr>



                                                                <tr style="background:white;">



                                                                    <td colspan="4">&nbsp;</td>



                                                                </tr>



                                                                <tr class="info"> 



                                                                    <th>Car Rentals <i class="fa fa-question-circle" data-original-title="Shows the number of car rental companies at the airport, on-site and off-site, the gross revenue generated by the rental companies and the total amount of rent paid to the airport." 

                                                                                       data-container="body"data-toggle="tooltip"></i>

                                                                    </th>



                                                                    <th>Agencies</th>



                                                                    <th>Gross Revenue</th>



                                                                    <th>Gross Rentals</th>



                                                                </tr>



                                                                <tr>



                                                                    <td>Car Rental On Site:</td>



                                                                    <td><?php echo number_format($aainfo[0]['carrentalagenciesonsite']); ?></td>



                                                                    <td>$<?php echo number_format($this->common->parseNumber($aainfo[0]['carrentalrevonsite']), 2); ?></td> 



                                                                    <td>$<?php echo number_format($this->common->parseNumber($aainfo[0]['carrentalrevtoaironsite']), 2); ?></td>    



                                                                </tr>



                                                                <tr>



                                                                    <td>Car Rental Off Site:</td>



                                                                    <td><?php echo number_format($aainfo[0]['carrentalagenciesoffsite']); ?></td>



                                                                    <td>$<?php echo number_format($this->common->parseNumber($aainfo[0]['carrentalrevoffsite']), 2); ?></td> 



                                                                    <td>$<?php echo number_format($this->common->parseNumber($aainfo[0]['carrentalrevtoairoffsite']), 2); ?></td>  



                                                                </tr>



                                                                <tr>



                                                                    <td>Total Cars Rented:</td>    



                                                                    <td><?php echo number_format($aainfo[0]['totalcarsrented']); ?></td>    



                                                                    <td>&nbsp;</td>    



                                                                    <td>&nbsp;</td>



                                                                </tr>



                                                                <tr>



                                                                    <td>Car Rental Sq. Ft.</td>    



                                                                    <td><?php echo number_format($aainfo[0]['carrentalsqft']); ?></td>    



                                                                    <td>&nbsp;</td>    



                                                                    <td>&nbsp;</td>



                                                                </tr>



                                                                <tr class="info">



                                                                    <th colspan="4">Parking 

                                                                        <i class="fa fa-question-circle" data-original-title="Shows all parking rates; gross revenue from parking; the number of concessionaires are located on- or off-airport; daily rate concessionaire(s) charge to parkers; concession fee paid to airport (if applicable) and total rent to the airport from parking." 

                                                                           data-container="body"data-toggle="tooltip"></i>

                                                                    </th>    



                                                                </tr>



                                                                <tr>



                                                                    <td>Parking.</td>    



                                                                    <td><?php echo number_format($aainfo[0]['parkingspaces']); ?></td>    



                                                                    <td>$<?php echo number_format($this->common->parseNumber($aainfo[0]['parkingrev']), 2); ?></td>    



                                                                    <td>$<?php echo number_format($this->common->parseNumber($aainfo[0]['parkingrevtoair']), 2); ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td colspan="4">



                                                                        <table width="100%" cellpadding="0" cellspacing="0" class="table-bordered">



                                                                            <tr  class="info">



                                                                                <td><strong>Parking Rates</strong></td>



                                                                                <td><strong>Short</strong></td>



                                                                                <td><strong>Long</strong></td>



                                                                                <td><strong>Economy</strong></td>



                                                                                <td><strong>Valet</strong></td>



                                                                            </tr>



                                                                            <tr>



                                                                                <td>Hourly</td>



                                                                                <td>$<?php echo $aainfo[0]['hourlyshort']; ?></td>



                                                                                <td>$<?php echo $aainfo[0]['hourlylong']; ?></td>



                                                                                <td>$<?php echo $aainfo[0]['hourlyeconomy']; ?></td>



                                                                                <td>$<?php echo $aainfo[0]['hourlyvalet']; ?></td>



                                                                            </tr>



                                                                            <tr>



                                                                                <td>Daily</td>



                                                                                <td>$<?php echo $aainfo[0]['dailyshort']; ?></td>



                                                                                <td>$<?php echo $aainfo[0]['dailylong']; ?></td>



                                                                                <td>$<?php echo $aainfo[0]['dailyeconomy']; ?></td>



                                                                                <td>$<?php echo $aainfo[0]['dailyvalet']; ?></td>



                                                                            </tr>



                                                                            <tr>



                                                                                <td># Spaces</td>



                                                                                <td><?php echo number_format($aainfo[0]['spacesshort']); ?></td>



                                                                                <td><?php echo number_format($aainfo[0]['spaceslong']); ?></td>



                                                                                <td><?php echo number_format($aainfo[0]['spaceseconomy']); ?></td>



                                                                                <td><?php echo number_format($aainfo[0]['spacesvalet']); ?></td>



                                                                            </tr>



                                                                        </table>



                                                                    </td>



                                                                </tr>



                                                                <tr class="info">



                                                                    <td colspan="4">Airportwide Comment</td>       



                                                                </tr>



                                                                <tr>



                                                                    <td colspan="4">



                                                                        <?php echo $aainfo[0]['awrevcomment']; ?>        



                                                                    </td>



                                                                </tr>



                                                            </tbody>



                                                        </table>										  



                                                    </div>



                                                </div>



                                            </div>



                                        </div>



                                        <div class="panel panel-primary">



                                            <div class="panel-heading" role="tab" id="headingEight">



                                                <h4 class="panel-title">



                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" 



                                                       href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">



                                                        Concession Tenant Details	



                                                    </a>



                                                </h4>



                                            </div>



                                            <div id="collapseEight" class="panel-collapse collapse in" 



                                                 role="tabpanel" aria-labelledby="headingEight">



                                                <div class="panel-body">



                                                    <h1 class="default-title">Concession Tenant Details</h1><br />        



                                                    <div class="table-responsive">



                                                        <!--<br/>-->                                                            



                                                        <!--                                                        <div class="alert alert-danger">

                                                        

                                                                                                                    No Result found..

                                                        

                                                                                                                </div>-->



                                                        <!--                                                    </div>

                                                        

                                                                                                        </div>

                                                        

                                                                                                    </div>-->



                                                        <!--                                        </div>-->



                                                        <?php

                                                        foreach ($listofcate as $cat):



                                                            $outinfo = $this->common->outlets($cat['category'], $AID);



                                                            if (count($outinfo) > 0) {

                                                                ?>                                    



                                                                <div class="panel panel-primary">



                                                                    <div class="panel-heading" role="tab" id="heading<?php echo $cat['categoryid']; ?>">



                                                                        <h4 class="panel-title">



                                                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" 



                                                                               href="#collapse<?php echo $cat['categoryid']; ?>" aria-expanded="false" aria-controls="collapseNine">



                                                                                <?php echo $cat['category']; ?>	



                                                                            </a>

                                                                            <i class="fa fa-question-circle" data-original-title=" 

                                                                            <?php

                                                                            if ($cat['category'] == 'Advertising') {

                                                                                echo 'Gross revenue and rent to the airport, where available, are provided. This category includes all revenue generated from advertising space at the airport that has been bought by companies paying for said space for the purpose of marketing their brand, including dioramas, wall space, banners, etc.';

                                                                            } elseif ($cat['category'] == 'Duty Free') {

                                                                                echo 'Retail shops that do not apply local or national taxes and duties to products sold.';

                                                                            } elseif ($cat['category'] == 'Food/Beverage') {

                                                                                echo 'Any store within an airport that utilizes more than 50% of its space to offer food and beverage products for sale to the public.';

                                                                            } elseif ($cat['category'] == 'News/Gifts') {

                                                                                echo 'Any store within an airport that utilizes the majority of its space for the sale of news publications and gift items.';

                                                                            } elseif ($cat['category'] == 'Passenger Services') {

                                                                                echo 'Refers to all non-retail or non-food services provided by the airport such as massage, ATMs, banking, business centers, barber shops, etc.';

                                                                            } elseif ($cat['category'] == 'Specialty Retail') {

                                                                                echo 'A store that utilizes a majority of its space to sell non-news, non-food, retail products such as apparel, souvenirs, gift items, art products, jewelry, etc.';

                                                                            }

                                                                            ?>

                                                                               "

                                                                               data-container="body"data-toggle="tooltip"

                                                                               ></i>



                                                                        </h4>



                                                                    </div>



                                                                    <div id="collapse<?php echo $cat['categoryid']; ?>" class="panel-collapse collapse in" 



                                                                         role="tabpanel" aria-labelledby="heading<?php echo $cat['categoryid']; ?>">



                                                                        <div class="panel-body">



                                                                            <h1 class="default-title"><?php echo $cat['category']; ?></h1><br />        



                                                                            <div class="table-responsive col-sm-12">                                           	



                                                                                <table width="100%" class="table table-striped table-hover example" id="example">



                                                                                    <thead>



                                                                                        <tr class="info">



                                                                                            <th>Tenant Name (Company)</th>



                                                                                            <th>Company Name</th>



                                                                                            <th>Product Description</th>



                                                                                            <th>Terminal</th>



                                                                                            <th>#</th>



                                                                                            <th> Sq Ft <i class="fa fa-question-circle" data-original-title="Stands for square footage, or the amount of space occupied by the associated category." 

                                                                                                          data-container="body"data-toggle="tooltip"></i></th>



                                                                                            <th>Expires <i class="fa fa-question-circle" data-original-title="Month, date and year in which the contract with the associated concept and operator expires." 

                                                                                                           data-container="body"data-toggle="tooltip"></i></th>



                                                                                        </tr>



                                                                                    </thead>



                                                                                    <tbody>



                                                                                        <?php foreach ($outinfo as $o): ?>



                                                                                            <tr scop="row">



                                                                                                <td><?php echo $o['outletname']; ?></td>



                                                                                                <td><?php echo $o['companyname']; ?></td>



                                                                                                <td><?php echo $o['productdescription']; ?></td>



                                                                                                <td><?php echo $o['termlocation']; ?></td>



                                                                                                <td><?php echo $o['numlocations']; ?></td>



                                                                                                <td><?php echo number_format($o['sqft']); ?></td>



                                                                                                <td>



                                                                                                    <?php

                                                                                                    if ($o['exp_reason'] != '') {

//                                                                                        $reasons = str_replace(array('M-T-M', 'TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $o['exp_reason']);

                                                                                                        // $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $o['exp_reason']);
                                                                                                         $reasons = str_replace(array('Permanently Closed', 'Temporarily Closed'), 'Closed', $o['exp_reason']);
//                                                                                        $reasons = $o['exp_reason'];



                                                                                                        if ($o['exp_date'] != '0000-00-00 00:00:00' && $o['exp_date'] != '' & $o['exp_reason'] != 'M-T-M') {

                                                                                                            echo $reasons . '<br>' . date("M d,Y H:i:s", strtotime($o['exp_date']));

                                                                                                        } else if (($o['exp_date'] == '0000-00-00 00:00:00' || $o['exp_date'] == '') && $o['exp_reason'] != 'M-T-M') {

                                                                                                            echo $reasons;

                                                                                                        } else {

                                                                                                            echo $reasons;

                                                                                                        }

                                                                                                    } else {



                                                                                                        if (!$this->common->check_date($o['exp'])) {



                                                                                                            if ($o['exp'] != '') {

                                                                                                                $date_val = $this->common->get_formatted_datetime($o['exp']);

                                                                                                            } else {

                                                                                                                $date_val = '';

                                                                                                            }

                                                                                                            /*

                                                                                                              elseif (strtotime($o['exp']) == '') {



                                                                                                              $date_val = "00/00/0000";

                                                                                                              } elseif (is_null($o['exp'])) {



                                                                                                              $date_val = "00/00/0000";

                                                                                                              } elseif ($o['exp'] == '0000-00-00') {



                                                                                                              $date_val = "00/00/0000";

                                                                                                              }

                                                                                                             */

                                                                                                        } else {



//                                                                                        $date_val = date('m/d/Y', strtotime($o['exp']));



                                                                                                            $date_val = $this->common->get_formatted_datetime($o['exp']);

                                                                                                        }



                                                                                                        echo $date_val;

                                                                                                    }

                                                                                                    ?>



                                                                                                </td>



                                                                                            </tr>



                                                                                        <?php endforeach; ?>



                                                                                    </tbody>



                                                                                </table>



                                                                            </div>



                                                                        </div>



                                                                    </div>



                                                                </div>



                                                                <?php

                                                            }



                                                        endforeach;

                                                        ?>



                                                    </div>



                                                </div>



                                            </div>



                                        </div>

                                    <?php } else { ?>



                                        <div class="panel panel-primary">



                                            <div class="panel-heading">



                                                Airport Search



                                            </div>



                                            <div class="panel-body">



                                                <div class="alert alert-info">



                                                    <strong>How to use the search.</strong><br/><br/>



                                                    Please Select either from the Location/City list or the IATA Code list then click on Search.



                                                </div>



                                            </div>



                                        </div>



                                    <?php } ?>



                                </div>                      



                            </div>   



                        </div>                     



                    </div>                



                </div>                



            </section>



        </section>



        <?php echo $this->load->view($footer); ?>



    </div>



    <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>



    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.bootstrap.js"></script>



    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>



    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>



    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/dataTables.responsive.js"></script>



    <script src="<?php echo base_url('fassests'); ?>/js/chosen.jquery.min.js"></script>



    <script>

                                            var latt;

                                            var longg;

                                            latt = <?php echo $latitude; ?>;

                                            longg = <?php echo $Longitude; ?>;

                                            var myLatLng;

                                            var map;

                                            function initMap() {

                                                var myLatLng = {lat: latt, lng: longg};

                                                map = new google.maps.Map(document.getElementById('map'), {

                                                    center: myLatLng,

                                                    zoom: 14

                                                });



                                                var marker = new google.maps.Marker({

                                                    position: myLatLng,

                                                    map: map

                                                });

                                            }



    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXrRAa4kiHZF8A5YPAeruoZIkVKBXsPu0&callback=initMap"

    async defer></script>



    <script type="text/javascript">



                                            $('span[data-toggle="tooltip"]').tooltip({

                                                animated: 'fade',

                                                placement: 'top',

                                            });



                                            $("#sidebar").click(function () {

                                                $("#hidee").toggle({animated: 'fade'});

                                                $("#expand").removeClass("col-sm-9");

                                                $("#sidebar2").parent('div').removeClass("hidden");



                                            });



                                            $("#sidebar2").click(function () {

                                                $("#hidee").toggle({animated: 'fade'});

                                                $("#sidebar2").parent('div').addClass("hidden");

                                                $("#expand").addClass("col-sm-9");



                                            }).tooltip({animated: 'fade',

                                                placement: 'top'});



                                            var seg = "<?php echo $this->uri->segment(0) ?>";



                                            if (seg != '') {

                                                $('title').html("Airport Search | ARN Fact Book " + seg);

                                            }

                                            else {

                                                $('title').html("Airport Search | ARN Fact Book");

                                            }



                                            $('.example').dataTable({

                                                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],

                                                "pagingType": "simple_numbers",

                                                "dom": 'T<"clear">lfrtip',

                                                "oTableTools": {

                                                    "aButtons": [

                                                        "print",

                                                        {

                                                            "sExtends": "collection",

                                                            "sButtonText": "Save",

                                                            "aButtons": ["copy", "csv", "xls", "pdf"]

                                                        }

                                                    ],

                                                    "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"

                                                },

                                                "oLanguage": {

                                                    "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                                                }

                                            });



                                            $('.contact2').DataTable({

                                                "lengthMenu": [5, 10, 15, 20, 25],

                                                "pagingType": "simple_numbers",

                                                "dom": 'T<"clear">lfrtip',

                                                "oTableTools": {

                                                    "aButtons": [

                                                        "print",

                                                        {

                                                            "sExtends": "collection",

                                                            "sButtonText": "Save",

                                                            "aButtons": ["copy", "csv", "xls", "pdf"]

                                                        }

                                                    ],

                                                    "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"

                                                },

                                                "oLanguage": {

                                                    "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"



                                                }



                                            });

                                            $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {

                                                console.log(message);

                                            };



                                            function valid()

                                            {

                                                var c_aid = $("#c_aid").val();

                                                var i_aid = $("#i_aid").val();

                                                if (c_aid == "" && i_aid == "")

                                                {

                                                    $(".error").removeClass('hide', 1000);

                                                    return false;

                                                } else

                                                {

                                                    $(".error").addClass("hide", 1000);

                                                }

                                                return true;

                                            }



                                            jQuery(document).ready(function () {



                                                $('#c_aid').change(function () {



                                                    $('#i_aid').prop('selectedIndex', 0).trigger('chosen:updated');

                                                });

                                                $('#i_aid').change(function () {



                                                    $('#c_aid').prop('selectedIndex', 0).trigger('chosen:updated');

                                                });

                                                $("#reset").on("click", function () {



                                                    $('#i_aid,#c_aid').prop('selectedIndex', 0).trigger('chosen:updated');

                                                });

                                                jQuery(".chosen").chosen();



                                                $('[data-toggle="tooltip"]').tooltip({

                                                    animated: 'fade',

                                                    placement: 'top',

                                                });



                                            });

    </script>



</body>



</html>