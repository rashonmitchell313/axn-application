<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Subsribe Request | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Subsribe Request | ARN Fact Book';
}
?>
</title>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu); ?>
        <div class="clearfix"></div>
        <section class="main-content">
        	<section class="container-fluid">
            	<div class="col-md-3 col-sm-12 col-sm-3 login-left-sect">
                     <?php echo $this->common->getmessage();?>
                    <?php echo $this->load->view('frontend/include/loginform');?>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 login-right-sect">
                	<!--<div class="row">
                    	<div class="video-box">
                        	<iframe width="640" height="360" src="//www.youtube.com/embed/jSdglW1utg8?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>                        	<div class="clearfix"></div>
                        </div>
                    </div>-->
                    <div class="row">
                    	<div class="">
                            <div class="col-md-12">
                            	<div class="alert alert-info">
								
                                	<p>You are logged in using <strong><?php echo $email;?>, </strong>but it appears you do not have an active subscription to the Fact Book Online (FBO).</p>
                                    <p><strong>Please visit</strong> <a href="https://airportrevenuenews.com/store">https://airportrevenuenews.com/store</a> to purchase a subscription.</p>
                                    <p>If you believe you have recieved this message by mistake, please fill out the form below:</p>
                               </div>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                    	<div class="text-bottom-login">
						<h3><p class="text-center"> Contact Us </p></h3>
<p class="text-center text-danger">*Indicates Required</p>
							<form class="form-horizontal" method="POST"  action="<?php echo site_url('login/unsubsribeuser'); ?>">
							  <div class="form-group">
								<label for="fname" class="col-sm-2 control-label">First Name <i class="text-danger">*</i></label>
								<div class="col-sm-10">
								
								  <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required  value="<?php echo $userinfo['first_name']; ?>"/>
								</div>
							  </div>
								<div class="form-group">
								<label for="lname" class="col-sm-2 control-label">Last Name <i class="text-danger">*</i></label>
								<div class="col-sm-10">
								  <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" required value="<?php echo $userinfo['last_name']; ?>"/>
								</div>
							  </div>
							  <div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email <i class="text-danger">*</i></label>
								<div class="col-sm-10">
								  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required value="<?php echo $userinfo['email']; ?>"/>
								</div>
								</div>
								 <div class="form-group">
								 <label for="email" class="col-sm-2 control-label">Description <i class="text-danger">*</i></label>
								<div class="col-sm-10">
							<textarea class="form-control" rows="3" name="detail" placeholder="Enter message here...." required ></textarea>
							</div></div>
							  <div class="form-group ">
							  <div class="col-sm-2 ">
							  </div>
							  <div class="form-inline" >
								
								  <button type="submit" style="margin-left: 15px;" class="btn btn-success"><i class="fa fa-check"></i>&nbsp; Request</button>
								  
								  &nbsp;&nbsp;

								  <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Reset</button>
							  </div>
							  </div>
							</form>
							<div class="error1">
							
							</div>
                            <div class="padding-top-20 padding-bottom-10">
                                <p class="text-center titled-font-bottom">If you are not already a subscriber and are interested in gaining access,</p>
                                <p class="text-center titled-font-bottom">you can view subscription pricing and purchase access at</p>
                                <p class="text-center titled-font-bottom"><a href="http://www.airportrevenuenews.com/store/">www.airportrevenuenews.com/store/</a></p>
                            </div>
                        </div>
                    </div>
                </div>                
            </section>
        </section>
        <?php echo $this->load->view($footer); ?>                
    </div>
        <script type="text/javascript">
			function checkpasswords()
			{
		
				var user_password		=	$("#pass").val();
				var user_cpassword	=	$("#cpass").val();						
				if(user_password=="" || user_cpassword=="")
				{
					$(".error").html('<div class="alert alert-danger">Invalid Password.</div>');
				}else if(user_cpassword.length<6)
				{
					$(".error").html('<div class="alert alert-danger">Password length must be at least 6.</div>');			
				} else if(user_password!=user_cpassword)
				{
					$(".error").html('<div class="alert alert-danger"> Confirm Password must be match with password.</div>');
				} else
				{
					$(".error").html('<i class="fa fa-spinner fa-spin fx"><i>');
					var key = $("#key").val();
					var datastring = "user_encrypt_key="+key+"&pass="+user_password+"&action=update_password";
					$.ajax({
							url:"<?php echo site_url('reset_password');?>",
							async:false,
							type:"POST",
							cache:false,
							data:datastring,
							success: function(html)
							{
								if(html!=1)
								{
									$(".error").empty();
									$(".error").html(html);
								} else 
								{
									$(".error").empty();
									$("#success_message").show("slow");
								}
							}
					});
					return false;
				}
				return false;
			}
		</script>
<script type="text/javascript">
	function show(){
	$(".login").addClass("hide");
	$(".forget").removeClass("hide");
	}
</script>
</body>
</html>
