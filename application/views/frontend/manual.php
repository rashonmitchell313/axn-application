<?php // echo $this->load->view($header);                      ?>
<title>
    <?php
    if ($this->uri->segment(0) != '') {
        echo 'Welcome to ARN Fact Book | Login ' . $this->uri->segment(0);
    } else {
        echo 'Welcome to ARN Fact Book | Documentation';
    }
    ?>
</title>


<meta charset="utf-8">
<link rel="shortcut icon" href="<?php echo base_url('assets/img'); ?>/favicon4.ico" type="image/x-icon" />
<link rel="canonical" href="http://www.templatemonster.com/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"/> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"> 
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/bootstrap1.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/responsive.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/prettify.css"  media="screen">
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/prettyPhoto.css"  media="screen">
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/idea.css"  media="screen">
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/style1.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/contact-form.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url('dist'); ?>/css/lightbox.min.css">
<link rel="stylesheet" href="<?php echo base_url('manual'); ?>/css/scrollbar.css">


<style>


    div a img {
        width: 200px;
        height: 150px;
    }

</style>


<!--[if IE 8]>
<link rel="stylesheet" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
            <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>  
      <![endif]-->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
<![endif]-->
</head>







<body>  

    <!-- header
    ================================================== -->
    <header class="header" style="background-color: #e60000;">
        <div class="container">
            <div class="row">
                <!--<article class="span4"><a class="brand" href="/help/" target="_blank"></a></article>-->
                <article class="span6">
                    <h1>
                        <!--<img alt="" src="https://www.airportrevenuenews.com/wp-content/themes/bootstrap-genesis/images/logo.png">-->
                        <img alt="" src="<?php echo base_url('manual'); ?>/images/logo.png">
                        <!--<a href="<?php // echo base_url();     ?>"><img src="<?php // echo base_url('fassests');     ?>/images/logo.png" title="logo" /></a>-->
                        ARN Fact Book Documentation</h1>
                </article> 
                <!--  <article class="span2">
                    <div id="languages" class="select-menu pull-right">
                        <div class="select-menu_icon"><b>En</b><i class="icon-angle-down"></i></div>
                        <ul class="select-menu_list">
                          
                        </ul>
                      </div>
                      <div id="versions" class="select-menu pull-right">
                        <div class="select-menu_icon"><b>v1-1</b><i class="icon-angle-down"></i></div>
                        <ul class="select-menu_list">
                          <li><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-0/index_en.html"><span>v1-0</span></a></li>
                          <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"><span>v1-1</span></a></li>
                          <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-2/"><span>v1-2</span></a></li>
                        </ul>
                      </div>
                  </article>   -->
            </div>    
        </div>
    </header>
    <div id="content"> 
        <div class="bg-content-top">
            <div class="container">

                <!-- Docs nav
                    ================================================== -->
                <div class="row">
                    <div class="span3"> 

                        <!--<div id="nav_container" style="overflow-y: scroll; height: 550px;">-->
                        <div id="nav_container" class="scrollbar style-3" style="overflow-y: scroll; height: 550px;">
                            <a href="javascript:;" id="affect_all">
                                <span class="expand">
                                    <i class="icon-angle-down"></i>
                                </span>            
                                <span class="close">
                                    <i class="icon-angle-up"></i>
                                </span>
                            </a>
                            <div class="clearfix"></div>    
                            <ul class="nav nav-list bs-docs-sidenav"  id="nav">
                                <li class="nav-item item1">
                                    <dl class="slide-down">
                                        <dt><a href="#introduction" class="icon-info-sign">Introduction</a></dt>
                                        <dd></dd>
                                    </dl>   
                                </li>
                                <!--                  <li class="nav-item item2">
                                                    <dl class="slide-down">
                                                       <dt><a href="#unzipping-template-package" class="icon-check">Unzipping Template Package</a></dt>  
                                                       <dd></dd>                       
                                                     </dl> 
                                                  </li>           -->
                                <li class="nav-item item3">
                                    <dl class="slide-down">
                                        <dt><a href="#general-information" class="icon-play">General Information</a> <i class="icon-sample"></i></dt>
                                        <dd>
                                            <ul class="list">          
                                                <li><a href="#glossary">Glossary</a></li>
                                                <li><a href="#contact-us">Contact Us</a></li>
                                                <li><a href="#faqs">FAQ's</a></li>
                                            </ul>
                                        </dd>
                                    </dl> 

                                </li> 
                                <li class="nav-item item9">
                                    <dl class="slide-down">
                                        <dt><a href="#airportsearch" class="icon-plane">Airport Search</a></dt>  
                                        <dd></dd>                       
                                    </dl> 
                                </li>           
                                <li class="nav-item item2">
                                    <dl class="slide-down">
                                        <dt><a href="#leaseexp" class="icon-calendar-empty">Lease Expiration</a></dt>  
                                        <dd></dd>                       
                                    </dl> 
                                </li>           
                                <li class="nav-item item4">
                                    <dl class="slide-down">
                                        <dt><a href="#contacts" class="icon-comments">Contacts</a></dt>  
                                        <dd></dd>                       
                                    </dl> 
                                </li>           
                                <li class="nav-item item5">
                                    <dl class="slide-down">
                                        <dt><a href="#companysearch" class="icon-building">Company Search</a></dt>  
                                        <dd></dd>                       
                                    </dl> 
                                </li>           
                                <li class="nav-item item7">
                                    <dl class="slide-down">
                                        <dt><a href="#tenants" class="icon-bar-chart">Tenants</a></dt>  
                                        <dd></dd>                       
                                    </dl> 
                                </li> 


                                <li class="nav-item item4">
                                    <dl class="slide-down">
                                        <dt><a href="#Reports" class="icon-copy">Reports</a></dt>  
                                        <dd></dd>                       
                                    </dl> 
                                </li> 

                                <?php if ($this->session->userdata('accesslevel') == 4) { ?>

                                    <li class="nav-item item5">
                                        <dl class="slide-down">
                                            <dt><a href="#affiliations" class="icon-fullscreen">Affiliations</a></dt>  
                                            <dd></dd>                       
                                        </dl> 
                                    </li> 

                                <?php } ?>

                                <li class="nav-item item6">
                                    <dl class="slide-down">
                                        <dt><a href="#modify-data" class="icon-wrench">Modify Data</a> <i class="icon-sample"></i></dt>  
                                        <dd>
                                            <ul class="list">          
                                                <li><a href="#e-airport">Edit Airport</a></li>
                                                <li><a href="#e-company">Edit-company</a></li>
                                            </ul>
                                        </dd>                       
                                    </dl> 
                                </li> 

                                <li class="nav-item item9">
                                    <dl class="slide-down">
                                        <dt><a href="#setting" class="icon-cog">Setting</a></dt>  
                                        <dd></dd>                       
                                    </dl> 
                                </li> 


                                <?php if ($this->session->userdata('accesslevel') == 4) { ?>

                                    <li class="nav-item item8">
                                        <dl class="slide-down">
                                            <dt><a href="#admin" class="icon-user">Admin</a> </dt>
                                            <dd>

                                            </dd>
                                        </dl> 
                                    </li>

                                    <li class="nav-item item9">
                                        <dl class="slide-down">
                                            <dt><a href="#dashboard" class="icon-cog">Dashboard</a></dt>  

                                            <dd></dd>                       
                                        </dl> 
                                    </li>  
                                    <li class="nav-item item9">

                                        <dl class="slide-down">
                                            <dt> <a href="#users" class="icon-user">  Users</a> <i class="icon-sample"></i></dt>
                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#users-list">List All Users</a></li>
                                                    <li><a href="#deleted-users">List deleted users</a></li>
                                                    <li><a href="#create-users">Create users</a></li>
                                                    <li><a href="#loggedIn-users">Logged in report</a></li>
                                                    <li><a href="#delete-logged-in">Deleted logged in report</a></li>
                                                    <li><a href="#activity-log">View user's activity log</a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li> 


                                    <li class="nav-item item9">

                                        <dl class="slide-down">
                                            <dt> <a href="#airports" class="icon-plane">Airports</a> <i class="icon-sample"></i></dt>

                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#list-airport">List All Airports</a></li>
                                                    <li><a href="#not-published">Not Published</a></li>
                                                    <li><a href="#deleted-airport">List Deleted Airports</a></li>
                                                    <li><a href="#airport-wizard">Airports Wizard</a></li>
                                                    <li><a href="#main-airports">Add Main Airports</a></li>
                                                    <li><a href="#add-airport">Add An Airport</a></li>
                                                    <li><a href="#log-airport">View Airport Activity Log</a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li>   

                                    <li class="nav-item item10">

                                        <dl class="slide-down">
                                            <dt> <a href="#companies" class="icon-building">Companies</a> <i class="icon-sample"></i></dt>

                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#list-company">List All Companies</a></li>
                                                    <li><a href="#not-published-comp">Not Published Companies</a></li>
                                                    <li><a href="#deleted-company">Deleted Companies</a></li>
                                                    <li><a href="#company-wizard">Company Wizard</a></li>
                                                    <li><a href="#add-company">Add Company</a></li>
                                                    <li><a href="#log-company">View Companies Activity Log</a></li>
                                                    <li><a href="#company-category">Companies by Category</a></li>
                                                    <li><a href="#airport-lookup">Add Airport Lookup Lists</a></li>
                                                    <li><a href="#lookup-list">Lookup List for Locations</a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li>   

                                    <li class="nav-item item11">

                                        <dl class="slide-down">
                                            <dt> <a href="#ads" class="icon-desktop">Ads</a> <i class="icon-sample"></i></dt>

                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#list-ads">List All Ads</a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li>   

                                    <li class="nav-item item1">

                                        <dl class="slide-down">
                                            <dt> <a href="#econtent" class="icon-edit">Edit Content</a> <i class="icon-sample"></i></dt>

                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#edit-content">Edit Content</a></li>
                                                    <li><a href="#multi-language"> Multi Language</a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li>   

                                    <li class="nav-item item2">

                                        <dl class="slide-down">
                                            <dt> <a href="#support" class="icon-flag">Support</a> <i class="icon-sample"></i></dt>

                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#support-question">Support Question</a></li>
                                                    <li><a href="#deleted-support"> Deleted Support Question</a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li>   

                                    <li class="nav-item item3">

                                        <dl class="slide-down">
                                            <dt> <a href="#error-log" class="icon-eye-open">Error Log</a> <i class="icon-sample"></i></dt>

                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#all-error">View All Error Log</a></li>
                                                    <li><a href="#deleted-error"> View Deleted Error Log</a></li>
                                                    <li><a href="#error-setting"> Error Log Setting</a></li>
                                                    <li><a href="#error-activity-log"> View Errors Activity Log</a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li> 

                                    <li class="nav-item item4">

                                        <dl class="slide-down">
                                            <dt> <a href="#admin-faqs" class="icon-question">FAQs</a> <i class="icon-sample"></i></dt>

                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#create-faq"> Create FAQ's </a></li>
                                                    <li><a href="#view-faq"> View FAQ's </a></li>
                                                    <li><a href="#faq-feedback"> View FAQ's Feedback </a></li>
                                                    <li><a href="#del-feedback"> View Deleted FAQ's Feedback </a></li>
                                                    <li><a href="#faq-activity-log"> View FAQ's Activity Log  </a></li>

                                                </ul>
                                            </dd>
                                        </dl>

                                    </li>  

                                    <li class="nav-item item5">
                                        <dl class="slide-down">
                                            <dt> <a href="#email" class="icon-envelope">Email</a> <i class="icon-sample"></i></dt>
                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#send-email"> Send Email </a></li>
                                                    <li><a href="#email-content"> Email Content Setting </a></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </li>   

                                    <?php if ($this->session->userdata('user_id') == 2452) { ?>

                                    <li class="nav-item item6">
                                        <dl class="slide-down">
                                            <dt> <a href="#tech-info" class="icon-cogs">Technical Information</a> <i class="icon-sample"></i></dt>
                                            <dd>
                                                <ul class="list"> 
                                                    <li><a href="#t-info"> Databases Connections Info </a></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </li>

                                    <?php } ?>


                                <?php } ?>
                                <!---

<li class="nav-item item6">
<dl class="slide-down">
<dt><a href="#tm-add-ons" class="icon-wrench">TM add-ons</a> <i class="icon-sample"></i></dt>
<dd>
<ul class="list">          
   <li><a href="#contact-form-manual">Contact form</a></li>
   <li><a href="#search-engine">Search Engine</a></li>
   <li><a href="#under-construction">Under Construction</a></li>
   <li><a href="#subscription-form">Subscription Form</a></li>
   <li><a href="#multimedia-gallery">TM Gallery</a></li>
   <li><a href="#video-bg">Video background</a></li>
   <li><a href="#parallax-bg">Parallax background</a></li>
</ul> 
</dd>
</dl> 
</li> 
<li class="nav-item item7-1">
<dl class="slide-down">
<dt><a href="#cookie-policy" class="icon-copy">Cookie Policy</a></dt>
<dd></dd>
</dl> 
</li> 
<li class="nav-item item7">
<dl class="slide-down">
<dt><a href="#uploading-template" class="icon-cogs">Uploading template</a></dt>
<dd></dd>
</dl> 
</li> 
<li class="nav-item item8">
<dl class="slide-down">
<dt><a href="#conclusion" class="icon-question-sign">Addendum</a></dt>
<dd></dd>
</dl> 
</li> -->
                            </ul>
                        </div>
                    </div> 


                    <div class="span9">

                        <!-- box-content
                        ================================================== -->
                        <div class="box-content">
                            <!-- block-started
                            ================================================== -->
                            <section class="block-started"  id="introduction">
                                <h2 class="item1"><i class="icon-info-sign"></i> Introduction <small><i>Welcome to the ARN Online Fact Book (OFB), the premier resource for revenue data on airports in North America.</i></small></h2>
                                <p>This online service, like its hard copy version, contains comprehensive data from major airports on:</p>
                                <ul>
                                    <li>Food and beverage</li>
                                    <li>Specialty retail</li>
                                    <li>News and gifts</li>
                                    <li>Duty-free</li>
                                    <li>Advertising</li>
                                    <li>Parking</li>
                                    <li>Car rental concessions</li>

                                </ul>

                                <p>Plus, it provides detailed information on all of the key companies doing business in airports. All data is based on previous calendar year information. </p>
                                <p>This online service comes with the added feature of regular updates on a quarterly basis at airports and concessions companies. Each time the data has been updated you will receive notice of the changes. The quarterly updates will include gross sales, passenger totals and sales per enplanement at each airport for that quarter as well as any tenant changes or contact information changes.</p>
                                <p>For all these first of all <a href="<?php echo site_url('login') ?>">CLICK HERE</a> for login </p>

                            </section>

<!--<section class="block-templates" id="unzipping-template-package">  
  <h2 class="item2"><i class="icon-check"></i> Unzipping Template Package </h2>
  <p>After unzipping the template package you will find 3 folders: &quot;<strong>documentation</strong>&quot;, &quot;<strong>screenshots</strong>&quot; and &quot;<strong>site</strong>&quot;. You can also see a zip archive called &quot;<strong>sources_############.zip</strong>&quot; that you need to unzip. </p>
            <p>The sources package contains all source files for the template. The package is password protected and can be extracted only with <a href="/help/unzipping-your-template.html" target="_blank">WinZip (Windows OS)</a> and <a href="/help/how-unzip-template-mac.html" target="_blank">StuffitExpander (MAC OS)</a> software.</p>
            <p>You can download both applications for free using the free trial options:</p>
            <ul  class="square-list">
            <li><a href="http://www.winzip.com/downauto.cgi?o=1&amp;file=http://download.winzip.com/winzip160.exe&amp;email=&amp;er=&amp;os=win" target="_blank">WinZip</a></li>
            <li><a href="http://www.stuffit.com/mac-stuffit-download.html" target="_blank">StuffitExpander</a></li>
          </ul>
            <p class="notification-box">While unzipping "sources_############.zip" archive you will be prompted to enter a password, which you can find at your Product Download Page (open the link in the email you received from our company).</p>
            <p>As soon as you are done with the unzipping the template you should have 4 folders in total: &quot;<strong>documentation</strong>&quot;, &quot;<strong>screenshots</strong>&quot;, &quot;<strong>site</strong>&quot; and &quot;<strong>sources</strong>&quot;.</p>
</section>-->

                            <section class="block-templates" id="general-information">  
                                <h2 class="item3"><i class="icon-play"></i> About Us </h2>

                                <h4 id="glossary" class="scrollTo">GLOSSARY</h4>
                                <p>The Glossary page to understand precisely the meaning of the various data points. Also, please navigate all the menu options for a variety of valuable and useful reports, 
                                    such as the top-performing airports by sales per enplanement; leases due to expire within the next 18 months; as well as a snapshot of important aspects of every airport. </p>
                                <p>It's our <a href="<?php echo site_url('glossary') ?>">Glossary</a>. </p>

                                <h4 id="contact-us" class="scrollTo">Contact Us</h4>
                                <p>For any information you can <a class="last" href="<?php echo site_url('contact-us') ?>">Contact Us</a> via call, fax & email. </p>
<!--                                <p>Link for Contact Our Staff: </p>
                                <p class="cols-1"><a class="last" href="<?php // echo site_url('contact-us')                      ?>">Click Here</a></p>-->

                                <h4 id="faqs" class="scrollTo">FAQ's</h4>
                                <p> Here are some frequently asked questions which helps you for exploring on <strong>ARN Fact Book</strong>. </p>
                                <p class="cols-1">Please <a class="last" href="<?php echo site_url('faq-view') ?>">Click Here</a> for going on FAQ's Page.</p>

                            </section>

                            <section class="block-templates" id="airportsearch">
                                <h2 class="item9"><i class="icon-plane"></i> Airport Search </h2>

                                <p>Here You can search the Airport by Location/City or IATA Code.</p>
                                <p><strong>How to use the search.</strong></p>
                                <p>Select either from the Location/City list or the IATA Code list then click on <a class="last" href="<?php echo site_url('airportsearch') ?>">Search</a>.</p>
                                <p><strong>After search result.</strong></p>
                                <ul
                                    <li> Airport name.</li>
                                    <li>City name</li>
                                    <li>Airport location on Google map</li>
                                    <li>Contacts Detail</li>
                                    <li>Airport wide Information</li>
                                    <li>Passenger Traffic Detail</li>
                                    <li>Airport Specifics</li>
                                    <li>Each terminal Gross Sales</li>
                                    <li>Total Gross Sales</li>
                                    <li>Airport wide Revenue</li>
                                    <li>Concession Tenant Details</li>
                                </ul>
                                <p> <strong>Tenant Categories</strong></p>

                                <ul>
                                    <li>Food/Beverage</li>
                                    <li>Specialty Retail</li>
                                    <li>News/Gifts</li>
                                    <li>Duty Free</li>
                                    <li>Passenger Services</li>
                                    <li>Advertising</li>
                                </ul> 

                                <div>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/airport-search.png" data-lightbox="example-1" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/airport-search.png" alt=""/></a>
                                </div>

                            </section>



                            <section class="block-templates" id="leaseexp">
                                <h2 class="item2"><i class="icon-calendar-empty"></i> Lease Expiration </h2>

                                <p>This Lease Expiration list displays all leases due to expire in the next two years. You can select a category or scroll down to view the entire list. You can search by specifics by going to the Tenants page and using the search options available.</p>
                                <!--<p><strong>How to use the search.</strong></p>-->
                                <p><a class="last" href="<?php echo site_url('lease_expiration') ?>">Click Here</a> for visit.</p>

                            </section>

                            <section class="block-templates" id="contacts">
                                <h2 class="item4"><i class="icon-comments"></i> Contacts </h2>

                                <p>In this section the contact of <a class="last" href="<?php echo site_url('contacts/company/a') ?>">Companies</a> and <a class="last" href="<?php echo site_url('contacts/airport/a') ?>">Airports</a> viewed and Searched by last name.</p>
                                <!--<p><strong>How to use the search.</strong></p>-->
                                <!--<p><a class="last" href="<?php // echo site_url('lease_expiration')                  ?>">Click Here</a> for visit.</p>-->

                            </section>

                            <section class="block-templates" id="companysearch">
                                <h2 class="item5"><i class="icon-building"></i> Company Search </h2>

                                <p><strong>How to use the search.</strong></p>
                                <p>Enter a full or partial name of a company to search for, click <a class="last" href="<?php echo site_url('companysearch') ?>">Search</a> then click on the name of desired company.</p>

<!--<p><a class="last" href="<?php // echo site_url('companysearch')                  ?>">Click Here</a> for visit.</p>-->

                                <P><strong>Search Results.</strong></P>
                                <ul>
                                    <li>Company Name</li>
                                    <li>Contact Detail</li>
                                    <li>Company Info</li>
                                    <li>Company Location</li>
                                </ul>
                            </section>


                            <section class="block-templates" id="tenants">
                                <h2 class="item6"><i class="icon-bar-chart"></i> Tenants </h2>

                                <p>Here You can View and Search the <a class="last" href="<?php echo site_url('tenants') ?>">Tenants</a> by typing any one of these given below things in Search Box.</p>

                                <ul>
                                    <li>IATA Code</li>
                                    <li>Product Category</li>
                                    <li>Tenant Name</li>
                                    <li>Product Description</li>
                                    <li>Company Name</li>
                                    <li>Expiry Date</li>
                                </ul>
                                <!--<p><strong>How to use the search.</strong></p>-->
                                <!--<p><a class="last" href="<?php // echo site_url('airportsearch')                 ?>">Search</a>.</p>-->

                            </section>

                            <!--(IATA Code, Product Category, Tenant Name, Product Description, Company Name & Expiry Date)-->
                            <section class="block-templates" id="Reports">
                                <h2 class="item4"><i class="icon-copy"></i> Reports</h2>
                                <p>The reports represent the ARN Fact Book results for a specific year. Remember the year references the year the Fact Book was published which represents numbers from the previous year.

                                    Each report opens as a PDF file in a separate window. You can peruse these reports online or once a report is open, you can easily print or save as a PDF on your own computer.
                                    Here You can View the <a href="<?php echo site_url('reports') ?>"> Reports.</a></p>
                                <p><strong>Report Detail</strong> </p>   
                                <p>DBE ( Disadvantaged Business Enterprise) Companies Detail</p>
                                <p>Airport Detail year by year</p>

                                <div>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/reports.png" data-lightbox="example-1" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/reports.png" alt=""/></a>
                                </div>

                            </section>
                            <?php if ($this->session->userdata('accesslevel') == 4) { ?>

                                <section class="block-templates" id="affiliations">
                                    <h2 class="item5"><i class="icon-fullscreen"></i> Affiliations </h2>

                                    <p>Here is the Detail about Affiliations between Users and Airports/Companies.
                                        Click to view  <a href=" <?php echo site_url('affiliations') ?>">Affiliations.</a></p>
                                    <p>Here Modify Access of Users to Modify Data by selecting the User allowed to modify,
                                        Name of Company allowed to modify and IATA of Airport allowed to modify field.</p>
                                    <p>Here is permission to the user to modify the specific Company or Airport which is affiliated with this user.</p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/affiliation.png" data-lightbox="example-1" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/affiliation.png" alt=""/></a>
                                    </div>

                                </section>
                            <?php } ?>
                            <section class="block-templates" id="modify-data">
                                <h2 class="item6"><i class="icon-wrench"></i> Modify Data </h2>

                                <p>Here If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", 
                                    if you are a company click "Company Listing". Click to view <a href="<?php echo site_url('wizard/airport') ?>" >Modify-Data.</a></p>


                                <div>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/modify-data.png" data-lightbox="example-1" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/modify-data.png" alt=""/></a>
                                </div>


                                <p><strong>Available Date List</strong></p>
                                <ul>
                                    <li>Airport Instructions</li>
                                    <li>Category A Airport Listing</li>
                                    <li>Category B Airport Listing</li>
                                    <li>Category C Airport Listing</li>
                                    <li>Show All Airport Listing</li>
                                    <li>Company Listing</li>
                                </ul>

                                <!--<p><strong>Edit Airport</strong></p>-->
                                <h5 id="e-airport" class="scrollTo"> Edit Airport</h5>

                                <div>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step1-airport.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step1-airport.png" alt=""/></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step2-airport.png" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step2-airport.png" alt="" /></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step3-airport.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step3-airport.png" alt="" /></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step4-airport.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step4-airport.png" alt="" /></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step5-airport.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step5-airport.png" alt="" /></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step6-airport.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-airport/step6-airport.png" alt="" /></a>
                                </div>

                                <ul>
                                    <p>For modify/update Airport's data the Admin or Affiliated Staff of the airport are the only users who can modify airport's data.</p>
                                    <p>First of all select the airport which's data you want to modify in the Listing of Category A, B & C or All Airport, then click on the link <a href="javascript:void(0)" class="icon-edit">Edit</a> which show left side of the page.</p>
                                    <p>Here you have a summary view of the existing data and also the 6 steps with buttons for Modify and add any information about airport. </p>

                                    <li><strong>Step 1 (Contacts)</strong></li>
                                    <p>In this step you can add new contact, edit and delete the existing contacts of the airport. </p>

                                    <li><strong>Step 2 (Airport Info)</strong></li>
                                    <p>In this step you can add/edit the info about Airport Configuration, Airport Configuration, Expansion Planned, Revenue, Ratio and also can view the existing terminals. </p>

                                    <li><strong>Step 3 (Terminals)</strong></li>
                                    <p> Here first select a Terminal to Modify Data:(Click on the icon <a href="javascript:void(0)" class="icon-edit">Edit</a> Terminal next to desired terminal).
                                        Then you can modify the data of the selected terminal like (current sq.ft, Gross Sales, Sales/EP & Gross Rentals for the tenant like 
                                        (Food & Beverage, Specialty Retail, News & Gifts and Duty Free)) and also can add/edit the information of the terminal's total Passenger Traffic information.
                                        After adding up all new data click on save changes button and then click on Updates Total button (which is at the end of the edit terminal's page) 
                                        for update Airport's total informations </p>

                                    <p><strong>Note:</strong> Please take the tour before modify any informations on "Edit terminal" section.</p>

                                    <li><strong>Step 4 (Rental & Parking)</strong></li>
                                    <p>Here add the info about Car Rental, info about Parking area and charges and also can add the comments. </p>

                                    <li><strong>Step 5 (Tenants)</strong></li>
                                    <p>On this step you can add any new tenant by category wise also can view, edit & delete the existing tenant. </p>

                                    <li><strong>Step 6 (Location)</strong></li>
                                    <p>At this step you can only view the airport's location on Live Google Maps. </p>
                                    <p>After Modifying new data you have a final view page and end of this page you have a link for publish your informations (which sent an email with comments to the ARN Fact Book's Staff). </p>

                                </ul>

                                <!--<p><strong>Edit Company</strong></p>-->
                                <h5 id="e-company" class="scrollTo"> Edit Company</h5>

                                <div>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step1-company.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step1-company.png" alt=""/></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step2-company.png" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step2-company.png" alt="" /></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step3-company.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step3-company.png" alt="" /></a>
                                    <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step4-company.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/frontend/edit-company/step4-company.png" alt="" /></a>
                                </div>

                                <ul>
                                    <p>For modify/update Company's data the Admin or Affiliated Staff of the company are the only users who can modify company's data.</p>
                                    <p>First of all select the Company which's data you want to modify in the Company Listing, then click on the link <a href="javascript:void(0)" class="icon-edit">Edit</a> which show left side of the page.</p>
                                    <p>Here you have a summary view named |Step 4" of the existing data and also the 4 steps with buttons for Modify and add information of Company. </p>

                                    <li><strong>Step 1 (Contacts)</strong></li>
                                    <p>In this step you can add new contact, edit and delete the existing contacts of the Company. </p>

                                    <li><strong>Step 2 (Company Info)</strong></li>
                                    <p>In this step you can add/edit the info about Company Type, Disadvantaged Business Enterprise & Detailed Description. </p>

                                    <li><strong>Step 3 (Location)</strong></li>
                                    <p>In this step you can add/delete the info about Company's existing locations from the locations list. </p>

                                    <li><strong>Step 4 (Review)</strong></li>
                                    <p>In this step you can REview the info about Company's all modified data and at the end of this page you have a link for publish your informations (which sent an email with comments to the ARN Fact Book's Staff). </p>

                                </ul>


                            </section>

                            <section class="block-templates" id="setting">
                                <h2 class="item9"><i class="icon-cog"></i> Setting </h2>
                                <p>In this section you can change your password by typing the original password 
                                    and then new password twice in provided fields and then click on the "set new password" Button. 
                                    Click to view <a href="<?php echo site_url('frontend/setting') ?>">Setting.</a></p>

                            </section>
                            <?php if ($this->session->userdata('accesslevel') == 4) { ?>

                                <section class="block-templates" id="admin">
                                    <h2 class="item8"><i class="icon-user"> </i>Admin </h2>
                                    <p>In this area there are all details about Reports, Users, Companies,
                                        Airports,Ads,Content,Support, Error Log,FAQs and Email</p>
                                    <p>In this area Admin is able to manage the all data, able to 
                                        add,modify or delete any data as well as generate the reports. 
                                        Click to view <a href="<?php echo site_url('admin') ?>"> admin.</a></p>
                                </section>

                                <section class="block-templates" id="dashboard">      
                                    <h2  class="item-black"> <i class="icon-cog"> </i>Dashboard</h2>
                                    <p>On the <a href="<?php echo site_url('admin') ?>">Dashboard</a> there are Graphical reports to the Total companies, 
                                        Company types, Total register users, Top 10 login users and Total airports.</p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/dashbord.png" data-lightbox="example-1" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/dashbord.png" alt=""/></a>
                                    </div>

                                </section>

                                <section class="block-templates" id="users">    
                                    <h2  class="item9"> <i class="icon-user"> </i>Users</h2>

                                    <p>Here is the list of all detail about users like, list all users,list deleted users,
                                        create users, logged in report, deleted logged in report,view user's activity report. </p>


                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/user/all-users-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/user/all-users-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/user/del-users-admin.png" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/user/del-users-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/user/create-user-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/user/create-user-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/user/users-loggedin-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/user/users-loggedin-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/user/users-del-loggedin-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/user/users-del-loggedin-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/user/users-activitylog-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/user/users-activitylog-admin.png" alt="" /></a>
                                    </div>


                                    <h5 id="users-list" class="scrollTo">List All Users</h5>
                                    <p>Here is listed the <a href="<?php echo site_url('admin/list_of_users') ?>">All Users</a> with full detail, also admin can delete, 
                                        edit and subscribe or un subscribe the users. if admin deleted any user 
                                        it will delete but not permanently, it will listed in "List Deleted Users" section.  </p>

                                    <h5 id="deleted-users" class="scrollTo">List Deleted Users</h5>
                                    <p>Here is the list of the <a href="<?php echo site_url('admin/list_of_del_users') ?>">Deleted Users</a>, in this section admin can delete these users permanently or restore any user. </p>

                                    <h5 id="create-users" class="scrollTo"> Create Users</h5> 
                                    <p>Here admin can create new <a href="<?php echo site_url('admin/create_user') ?>">User</a>, and set the access level to the user.</p>

                                    <h5 id="loggedIn-users" class="scrollTo"> Logged In Report</h5> 
                                    <p>Here is the detail about <a href="<?php echo site_url('admin/logged_in_report') ?>">Logged In</a> users, logged in user id, name, email, 
                                        logged in time as well as log out time, also admin can delete these 
                                        reports but these will not be deleted permanently, these will listed in the "Deleted Logged In Report" section. </p> 

                                    <h5 id="delete-logged-in" class="scrollTo"> Deleted Logged In Report</h5> 
                                    <p>Here is the detail about <a href="<?php echo site_url('admin/delete_logged_report') ?>">Deleted</a> logged in users, admin can restore these deleted logged in report</p>

                                    <h5 id="activity-log" class="scrollTo"> View User's Activity Log</h5> 
                                    <p>Here is the view about users <a href="<?php echo site_url('admin/view_user_activity_log') ?>">Activities</a> by the admin like any modification by 
                                        the admin is displayed against the related user</p>
                                </section>

                                <section class="block-templates" id="airports">    
                                    <h2  class="item9"> <i class="icon-plane"> </i>Airports</h2>

                                    <p>Here is the list of all detail about airports like, list all airports,Not published,
                                        List deleted airports, Airport Wizard, Add main airports, 
                                        Add an airport and View airport activity log. </p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/airport/all-airports-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/airport/all-airports-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/airport/not-published-airports-admin.png" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/airport/not-published-airports-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/airport/del-airports-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/airport/del-airports-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/airport/add-main-airport-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/airport/add-main-airport-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/airport/add-airport-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/airport/add-airport-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/airport/airports-activitylog-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/airport/airports-activitylog-admin.png" alt="" /></a>
                                    </div>


                                    <h5 id="list-airport" class="scrollTo"> List All Airports</h5>
                                    <p>Here are listed the all <a href="<?php echo site_url('admin/airport') ?>">Airports</a>, admin can delete airports but not permanently also can edit any airport,
                                        export airport detail in pdf and doc formate.  </p>

                                    <h5 id="not-published" class="scrollTo"> Not Published</h5>
                                    <p>Here are listed the all <a href="<?php echo site_url('admin/airport/list_of_airport_not_published') ?>">Not Published</a> airports, here admin can delete, 
                                        edit and export detail about airport in pdf formate. </p>

                                    <h5 id="deleted-airport" class="scrollTo"> List Deleted Airports</h5>
                                    <p>Here are listed all <a href="<?php echo site_url('admin/airport/list_of_del_airport') ?>">Deleted</a> airports, here admin can delete these airports permanently,
                                        also export airports detail in pdf formate. </p>

                                    <h5 id="airport-wizard" class="scrollTo"> Airports Wizard</h5>
                                    <p>Here admin can modify <a href="<?php echo site_url('wizard/airport') ?>">Airport</a>'s data, and can search and select Airport from the following sections by category or all.</p>
                                    <p><strong>Available Section for Airports</strong></p>
                                    <ul>
                                        <li><strong>Category A Airport Listing</strong></li>
                                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here shows only those Airports which have "<strong>A</strong>" category.</p>
                                        <li><strong>Category B Airport Listing</strong></li>
                                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here shows only those Airports which have "<strong>B</strong>" category.</p>
                                        <li><strong>Category C Airport Listing</strong></li>
                                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here shows only those Airports which have "<strong>C</strong>" category.</p>
                                        <li><strong>Show All Airport Listing</strong></li>
                                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here shows the All Airports.</p>
                                    </ul>

                                    <h5 id="main-airports" class="scrollTo"> Add Main Airports</h5>  
                                    <p>This section is used to <a href="<?php echo site_url('admin/airport/add_main_airport') ?>">Add</a> the new airport.</p>

                                    <h5 id="add-airport" class="scrollTo"> Add An Airport</h5>  
                                    <p>This section is used to add an Airport to the <a href="<?php echo site_url('admin/airport/add_airport') ?>">Company Location</a> list.</p>

                                    <h5 id="log-airport" class="scrollTo"> View Airport Activity Log</h5>  
                                    <p>Here is the view about airport <a href="<?php echo site_url('admin/view_aiorport_activity_log') ?>">Activities</a> like any modification by 
                                        the admin or Airport is displayed with time, IATA and Modified by's Email Address.</p>


                                </section>


                                <section class="block-templates" id="companies">    
                                    <h2  class="item10"> <i class="icon-building"> </i>Companies</h2>

                                    <p>Here is the list of all detail about Companies like, list all Companies, Not published Companies,
                                        List deleted Companies, Company Wizard, Add Company,View Company activity log,
                                        Companies by Category, Add Airport Lookup Lists and Lookup List for Locations. </p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/all-companies-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/all-companies-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/not-published-companies-admin.png" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/not-published-companies-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/del-companies-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/del-companies-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/add-company-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/add-company-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/companies-activitylog-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/companies-activitylog-admin.png" alt="" /></a>
                                        <!--<a class="example-image-link" href="<?php // echo base_url('manual');           ?>/images/admin/company/companies-by-category-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php // echo base_url('manual');           ?>/images/admin/company/companies-by-category-admin.png" alt="" /></a>-->
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/companies-by-category-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/companies-by-category-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/add-airport-lookup-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/add-airport-lookup-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/company/lookup-list-location-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/company/lookup-list-location-admin.png" alt="" /></a>
                                    </div>


                                    <h5 id="list-company" class="scrollTo"> List All Companies</h5>
                                    <p>Here are listed the all <a href="<?php echo site_url('admin/company') ?>">Companies</a>, admin can edit any company also can delete companies but not permanently,
                                        export multiple companies detail in PDF and doc formate.  </p>

                                    <h5 id="not-published-comp" class="scrollTo"> Not Published Companies</h5>
                                    <p>Here are listed the all <a href="<?php echo site_url('admin/company/company_not_published') ?>">Not Published</a> companies, here admin can edit any company and also can delete. </p>

                                    <h5 id="deleted-company" class="scrollTo"> List Deleted Companies</h5>
                                    <p>Here are listed all <a href="<?php echo site_url('admin/company/company_deleted') ?>">Deleted</a> companies, which deleted from List All Companies and Not published sections,
                                        here admin can restore or delete these Companies permanently. </p>

                                    <h5 id="company-wizard" class="scrollTo"> Company Wizard</h5>
                                    <p>Here admin can view the all companies which are published or not, by clicking on "<a href="<?php echo site_url('admin/wizard/company') ?>">Company Listing</a>" 
                                        and also can search the company by Name, Last Modify data, published date and with the email address who Lastly Change the company's data,
                                        also can add/edit their information in detailed.</p>


                                    <h5 id="add-company" class="scrollTo"> Add Company</h5>  
                                    <p>This section is used to <a href="<?php echo site_url('admin/company/add_company') ?>">Add</a> the new company.</p>

                                    <h5 id="log-company" class="scrollTo"> View Companies Activity Log</h5>  
                                    <p>Here is the view about companies <a href="<?php echo site_url('admin/view_company_activity_log') ?>">Activities</a> which based on last modified date and also show the emial address who Lastly Change the company's data.</p>

                                    <h5 id="company-category" class="scrollTo"> Companies by Category</h5>  
                                    <p>This section shows the companies by <a href="<?php echo site_url('admin/company/companies_by_category') ?>">Category</a> wise. And also give the link to edit company's information.</p>

                                    <h5 id="airport-lookup" class="scrollTo"> Add Airport Lookup Lists</h5>  
                                    <p>Here admin <a href="<?php echo site_url('admin/company/add_airport_lookup') ?>">Add</a> the Airport's informations like IATA, Airport Name, City, State and Country. Which can be set as Companies Locations by edit company from Modify data section on frontend site.</p>

                                    <h5 id="lookup-list" class="scrollTo"> Lookup List for Locations</h5>  
                                    <p>Here is the list of all those airport <a href="<?php echo site_url('admin/company/lookup_list_for_locations') ?>">Locations</a> which already saved.</p>


                                </section>

                                <section class="block-templates" id="ads">    
                                    <h2  class="item11"> <i class="icon-desktop"> </i>Ads</h2>

                                    <p>Here is the list of all pages on which admin can set the Ads. </p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/all-ads-admin.png" data-lightbox="example-1"><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/all-ads-admin.png" alt="image-1" /></a>
                                    </div>

                                    <h5 id="list-ads" class="scrollTo"> List All Ads</h5>
                                    <p>Here are listed the all Pages on which admin can set <a href="<?php echo site_url('admin/adds') ?>">Ads</a> individually on any page's four different locations.
                                        Like any airport, company and any other page which were in drop down list. </p>

                                </section>

                                <section class="block-templates" id="econtent">    
                                    <h2  class="item1"> <i class="icon-edit"> </i> Edit Content</h2>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/edit-content-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/edit-content-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/multi-language-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/multi-language-admin.png" alt="" /></a>
                                    </div>

                                    <p>Here is the content which can be modified for the given sections. </p>

                                    <h5 id="edit-content" class="scrollTo"> Edit Content</h5>
                                    <p>Here admin can edit the <a href="<?php echo site_url('admin/edit_content') ?>">Contact</a> of the Login Page and also at section 1 & 2 of Home Page. </p>

                                    <h5 id="multi-language" class="scrollTo"> Multi Language</h5>
                                    <p>Here are listed the different <a href="<?php echo site_url('admin/multi_language') ?>">Languages</a> which translate the content of Login Page and also at section 1 & 2 of Home Page in selected language. </p>

                                </section>

                                <section class="block-templates" id="support">    
                                    <h2  class="item2"> <i class="icon-flag"> </i> Support</h2>

                                    <p>Here is the support questions which asked by users. </p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/support-question-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/support-question-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/del-support-question-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/del-support-question-admin.png" alt="" /></a>
                                    </div>

                                    <h5 id="support-question" class="scrollTo"> Support Question</h5>
                                    <p>Here admin can View the information of <a href="<?php echo site_url('admin/view_tickets') ?>">Support Question</a>. Like Ticket Number,
                                        Created Time, Subject, Status, Submitted User Name, Submitted User Email Address and also can edit, delete single or multiple (not permanently) &
                                        view discussion by clicking on their's buttons. </p>

                                    <h5 id="deleted-support" class="scrollTo"> Deleted Support Question</h5>
                                    <p>Here admin can View the information of <a href="<?php echo site_url('admin/view_del_tickets') ?>">Deleted Support Question</a>. Like Ticket Number,
                                        Created Time, Subject, Status, Submitted User Name, Submitted User Email Address and also can restore multiple, delete permanently &
                                        view discussion by clicking on their's buttons. </p>

                                </section>

                                <section class="block-templates" id="error-log">    
                                    <h2  class="item3"> <i class="icon-eye-open"> </i> Error Log</h2>

                                    <p>Here is the list of error which occurred during user's activities. </p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/error-log/all-errorlog-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/error-log/all-errorlog-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/error-log/del-errorlog-admin.png" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/error-log/del-errorlog-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/error-log/errorlog-setting-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/error-log/errorlog-setting-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/error-log/errorlog-activity-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/error-log/errorlog-activity-admin.png" alt="" /></a>
                                    </div>

                                    <h5 id="all-error" class="scrollTo"> View All Error Log</h5>
                                    <p>Here admin can View the information of <a href="<?php echo site_url('admin/errors_log') ?>">Errors</a>. Like Error URL,
                                        User Name who have error, Login as that User, Error Subject, Error Message, Created Time, Status with Open, Close & Re-Open text 
                                        and also view, edit & delete single or multiple (not permanently) access by clicking on their's buttons. </p>

                                    <h5 id="deleted-error" class="scrollTo"> View Deleted Error Log</h5>
                                    <p>Here admin can View the information of <a href="<?php echo site_url('admin/del_errors_log') ?>">Deleted Errors</a> from all error log. Like Error URL,
                                        User Name who have error, Login as that User, Error Subject, Error Message, Created Time, Status with Open, Close & Re-Open text 
                                        and also can restore multiple, delete permanently & view discussion by clicking on their's buttons. </p>

                                    <h5 id="error-setting" class="scrollTo"> Error Log Setting</h5>
                                    <p>Here admin can change the email subject, email to, CC & BCC of support team who receive the email on error created and resolved it. 
                                        All these <a href="<?php echo site_url('admin/error_log_setting') ?>">Settings</a> are saved with respect to error subject. </p>

                                    <h5 id="error-activity-log" class="scrollTo"> View Errors Activity Log</h5>
                                    <p>Here admin can view the error time and subject in <a href="<?php echo site_url('admin/error_log_setting') ?>">Activity</a> calender view. </p>

                                </section>

                                <section class="block-templates" id="admin-faqs">    
                                    <h2  class="item4"> <i class="icon-question"> </i> FAQs</h2>

                                    <p>Here admin can create, view, edit & delete the FAQs. And also view the feedback of these FAQs. </p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/faqs/create-faqs-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/faqs/create-faqs-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/faqs/view-faqs-admin.png" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/faqs/view-faqs-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/faqs/faqs-feedback-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/faqs/faqs-feedback-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/faqs/faqs-del-feedback-admin.png" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/faqs/faqs-del-feedback-admin.png" alt="" /></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/faqs/faq-activitylog-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/faqs/faq-activitylog-admin.png" alt="" /></a>
                                    </div>

                                    <h5 id="create-faq" class="scrollTo"> Create FAQ's</h5>
                                    <p>Here admin can <a href="<?php echo site_url('admin/create-faq') ?>">Create FAQ</a> with credentials of Question, Category and Answer. </p>

                                    <h5 id="view-faq" class="scrollTo"> View FAQ's </h5>
                                    <p>Here admin can View the information of <a href="<?php echo site_url('admin/view-faq') ?>">FAQs</a> with credentials of Question, Category, Answer and active status.
                                        And also can be edit delete (not permanently by one time click and permanently by click again on delete button of that FAQ which's status already "Deleted"). </p>

                                    <h5 id="faq-feedback" class="scrollTo"> View FAQ's Feedback</h5>
                                    <p>Here admin can view the <a href="<?php echo site_url('admin/view_faq_feedback') ?>">Feedback</a> of the FAQs with these following informations; 
                                        FAQ Id, User Name, User Email, FAQ Question, FAQ Answer, Reason (if FAQ not Helpful), Admin's status and Feedback. 
                                        In case user gives feedback as Not Helpful & give the valid reason and admin click on green thumbs up icon in status column against that reason,
                                        then an appreciation email has been sent to that user. Also delete the multiple FAQ's Feedbacks. </p>

                                    <h5 id="del-feedback" class="scrollTo"> View Deleted FAQ's Feedback </h5>
                                    <p>Here admin can view the <a href="<?php echo site_url('admin/view_deleted_faq_feedback') ?>">Deleted Feedback</a> of the FAQs with these following informations; 
                                        FAQ Id, User Name, User Email, FAQ Question, FAQ Answer, Reason (if FAQ not Helpful), Admin's status and Feedback. And also restore or delete the multiple FAQ's Feedbacks. </p>

                                    <h5 id="faq-activity-log" class="scrollTo"> View FAQ's Activity Log</h5>
                                    <p>Here admin can view the feedback time with User Name in <a href="<?php echo site_url('admin/view_activity_log') ?>">Activity</a> calender view. </p>

                                </section>

                                <section class="block-templates" id="email">    
                                    <h2  class="item5"> <i class="icon-envelope"> </i> Email</h2>

                                    <p>Here admin can send email to single or multiple users and also can edit the email content order by email subject. </p>

                                    <div>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/send-email-admin.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/send-email-admin.png" alt=""/></a>
                                        <a class="example-image-link" href="<?php echo base_url('manual'); ?>/images/admin/edit-email-content-admin.png" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url('manual'); ?>/images/admin/edit-email-content-admin.png" alt="" /></a>
                                    </div>

                                    <h5 id="send-email" class="scrollTo"> Send Email </h5>
                                    <p>Here admin can <a href="<?php echo site_url('admin/compose') ?>">Send Email</a> to single or multiple users with any subject and email content. </p>

                                    <h5 id="email-content" class="scrollTo"> Email Content Setting </h5>
                                    <p>Here admin can Edit the  <a href="<?php echo site_url('admin/email_content') ?>">Email Content</a> by selecting the email subject from drop down list 
                                        and modify the Email content with adding the required words which show between email subject and content. </p>

                                </section>

                                <?php if ($this->session->userdata('user_id') == 2452) { ?>

                                    <section class="block-templates" id="tech-info">    
                                        <h2  class="item6"> <i class="icon-cogs"> </i> Technical Information</h2>

                                        <p>Here the information about database connections with all sites of ARN Fact Book (old & new). </p>

                                        <h5 id="t-info" class="scrollTo"> Databases Connections Info: </h5>
                                        <!--<p>Here admin can <a href="<?php // echo site_url('admin/compose')     ?>">Send Email</a> . </p>-->

                                        <ul>
                                            <li><strong><a href="http://www.arnfactbook.com/2016"> www.arnfactbook.com/2016</a></strong></li>
                                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database: <strong>"factbook_2016"</strong>.</p>
                                            <li><strong><a href="http://arnfactbook.com"> arnfactbook.com </a></strong></li>
                                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database: <strong>"factbook_arndev2015"</strong>.</p>
                                            <li><strong><a href="http://new.arnfactbook.com"> new.arnfactbook.com </a></strong></li>
                                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database: Old: <strong>"factbook_fbo3"</strong>, Current: <strong>"factbook_2016"</strong>.</p>
                                            <li><strong><a href="http://arnfactbook.com/2015"> arnfactbook.com/2015 </a></strong></li>
                                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database: <strong>"factbook_2015"</strong>.</p>
                                            <li><strong><a href="http://arnfactbook.com/2014"> arnfactbook.com/2014 </a></strong></li>
                                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database: <strong>"factbook_2014"</strong>.</p>
                                            <li><strong><a href="http://arnfactbook.com/2013"> arnfactbook.com/2013</a></strong> OR <strong><a href="http://2013.arnfactbook.com"> 2013.arnfactbook.com</a></strong></li>
                                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database: <strong>"factbook_fbo2"</strong>.</p>
                                            <li><strong><a href="http://arnfactbook.com/2012"> arnfactbook.com/2012</a></strong> OR <strong><a href="http://2012.arnfactbook.com"> 2012.arnfactbook.com</a></strong></li>
                                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database: <strong>"factbook_fbo"</strong>.</p>
                                        </ul>


                                    </section>



                                <?php } ?>

                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Footer
     ================================================== -->
    <footer>
        <div class="container">        
            ARN Fact Book Documentation<br>
            &copy; <span id="copyright-year"></span> <span id="copyright">ARN Fact Book</span>
        </div>
    </footer>

    <div id="back-top">
        <a href="#"><i class="icon-double-angle-up"></i></a>
    </div>


    <?php // echo $this->load->view($footer);  ?> 


<!-- <script src="js/bootstrap-scrollspy.js"></script>  -->


    <!--<div class="language-modal">
      <div class="modal_bg"></div>
      <div class="modal">
        <div class="modal-header">
          <span class="modal_remove pull-right"><i class="icon-remove"></i></span>
          <h3>Choose language</h3>
        </div>
        <div class="modal-body">
          <ul id="modal_languages" class="nav nav-list bs-docs-sidenav"></ul>
        </div>
      </div>
    </div>-->

    <script src="<?php echo base_url('dist'); ?>/js/lightbox-plus-jquery.min.js"></script>

    <script src="<?php echo base_url('manual'); ?>/js/jquery.js" ></script>
    <script src="<?php echo base_url('manual'); ?>/js/jquery.scrollTo.js"></script>
    <script src="<?php echo base_url('manual'); ?>/js/jquery-migrate-1.1.0.js"></script>
    <script src="<?php echo base_url('manual'); ?>/js/prettify.js"></script>
    <script src="<?php echo base_url('manual'); ?>/js/bootstrap-affix.js"></script>
    <script src="<?php echo base_url('manual'); ?>/js/jquery.prettyPhoto.js"></script> 

    <script src="<?php echo base_url('manual'); ?>/js/TMForm.js"></script>
    <script src="<?php echo base_url('manual'); ?>/js/modal.js"></script>  
    <script src="<?php echo base_url('manual'); ?>/js/bootstrap-filestyle.js"></script>    

    <script src="<?php echo base_url('manual'); ?>/js/sForm.js"></script>  
    <script src="<?php echo base_url('manual'); ?>/js/jquery.countdown.min.js"></script>
    <script src="<?php echo base_url('manual'); ?>/js/scripts.js"></script>




</body> 






</html>
