<?php  $this->load->view($header);?>
<title>
<?php 
	if($this->uri->segment(0)!='')
	{
	echo 'Airport Details | ARN Fact Book'.$this->uri->segment(0);
	
	}
	else 
	{
	echo 'Airport Details | ARN Fact Book';
	}
?>
</title>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu); ?>
        <div class="clearfix"></div>
        <section class="main-content">
        	<section class="container-fluid">                       	
                	<div class="panel panel-primary">
						<div class="panel-heading">
                        	<i class="fa fa-arrow-right"></i> Airport Report
						</div> 
                        
                        <div class="panel-body font" id="printable"> 
                              <button class="btn btn-success pull-right" onClick="printDiv('printable')"><i class="fa fa-print input-icon"></i> Print Report</button><br><br>
                               
                                <?php 
									//echo "<pre>";
                       				//	print_r($alldata);
		                            //echo "</pre>";
								 ?> 
                                <ul class="nav nav-tabs">
                                	<li class="active"><a data-toggle="tab" href="#t1">Data</a></li>
                                	<li><a data-toggle="tab" href="#ch">Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="t1" class="tab-pane fade in active">
                                    <?php echo $output; ?>
                                    </div>
                                    <div id="ch" class="tab-pane fade"></div>
                                </div>
                            </div>                             
                        </div>  
                                
            </section>
        </section>
        <?php echo $this->load->view($footer); ?>      
		 <script type="text/javascript">		
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() 	  
	  {        
	  	  var counter=1;
		  <?php foreach($alldata as $gdata):  ?>		  
		  if(<?php echo $this->common->getsum($gdata,1) ?> > 0) {
	  	  var options = {
          title: '<?php  echo preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $gdata[0][1]);?>',
		  height: 450,
  		  width: 1100,
          hAxis: {title: '2010',  titleTextStyle: {color: '#333'},  slantedText:true, slantedTextAngle:30},
          vAxis: {minValue: 0},	
		  trigger: 'none',	 
		  pointSize: 6,
          pointShape: 'circle',				    
		  tooltip:{isHtml: true}, 		    
        };	
		$("#ch").append('<div id="gdata'+counter+'"></div>');
		var data = google.visualization.arrayToDataTable(<?php  echo json_encode($gdata); ?>);
        var chart = new google.visualization.AreaChart(document.getElementById('gdata'+counter+''));
        chart.draw(data, options);			
		counter++;
		  }
		<?php 		 
		endforeach; ?>
      }
    </script>	
    
    <script type="text/javascript">
	 function printDiv(printable) {
	 $(".collapse").addClass("in"); 
     var printContents = document.getElementById('printable').innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
	
}
//$(".panel-heading").on('click', function(e){
	//$(this).siblings('.panel-collapse').toggle('collapse');
	//$(".fa-chevron-up").removeClass("icon-chevron-up").addClass("fa-chevron-down");
	//$(".fa-chevron-down").removeClass("hide").removeClass("fa-chevron-up").addClass("fa-chevron-up");
//});	

</script>
    		
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>           
    </div>
</body>
</html>
