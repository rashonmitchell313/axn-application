<?php echo $this->load->view($header); ?>

<title>

    <?php

    if ($this->uri->segment(0) != '') {

        echo 'Reports | ARN Fact Book ' . $this->uri->segment(0);

    } else {

        echo 'Reports | ARN Fact Book';

    }

    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/choseen.css">

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>       

        <section class="main-content">                      

            <?php /* ?><section class="col-sm-2">

              <?php foreach($reports_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>             

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>              

                <?php echo $this->common->getmessage(); ?>

                <div class="panel panel-default">

                    <div class="panel-body">

                        <h1 class="default-title"><i class="fa fa-file-pdf-o"></i>&nbsp;Reports for ARN Online Fact Book</h1>

                        <p>

                            The reports listed in the table below represent the ARN Fact Book results for a specific year. Remember

                            the year references the year the Fact Book was published which represents numbers from the previous

                            year.

                        </p>

                        <p>

                            Each report opens as a PDF file in a separate window. You can peruse these reports online or once a

                            report is open, you can easily print or save as a PDF on your own computer.

                        </p>

                        <a href="<?php echo site_url('reports/dbe_companies'); ?>" class="btn btn-danger full-width">

                            <i class="fa fa-arrow-right"></i>&nbsp;DBE Companies</a>

                        <div class="form-inline pull-right padding-right-none select-year" style="padding-right: 20px;">

                            <form method="POST" action="<?php echo site_url('reports'); ?>" >



                                <?php /* ?><input value="<?php if(isset($_POST['search'])){ echo $_POST['search']; } ?>" type="text" class="form-control" name="search" placeholder="search by year" /><?php */ ?>

                                <select value="<?php

                                if (isset($_POST['search'])) {

                                    echo $_POST['search'];

                                }

                                ?>" class="chosen form-control" name="search" id="search">

                                    <option value="">Select Year</option>

                                    <option value="2010">2010</option>

                                    <!-- for($i=2009; $i<=date("Y")-1; $i++){-->

                                    <?php

                                    // for($i=2013; $i<=date("Y")-1; $i++){ 

                                    for ($i = 2013; $i <= date("Y"); $i++) {

                                        echo '<option>' . $i . '</option>';

                                    }

                                    ?>

                                </select>

                                &nbsp;&nbsp;&nbsp;

                                 <!--<input class="btn btn-danger" type="submit" name="submit" id="butn" value="Search">-->

                                <button class="btn btn-danger" type="submit" name="submit" id="butn"><i class="fa fa-search"></i> Search</button>



                            </form>

                        </div>

                        <br/><br/>

                        <div class="panel-group tabs-table" id="accordion" role="tablist" aria-multiselectable="true">

                            <?php

                            if (isset($_POST['submit'])) {

                                $searchyear = $_POST['search'];



//                   for($y=2013;$y>=2010;$y--) {

//                   for($y=date("Y")-1;$y>=2010;$y--) {

                                for ($y = date("Y"); $y >= 2010; $y--) {

                                    if ($searchyear == $y) {

                                        ?>                            

                                        <div class="panel panel-primary">

                                            <div class="panel-heading" role="tab" id="heading<?php echo $y; ?>">

                                                <h4 class="panel-title">

                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $y; ?>" aria-controls="collapse<?php echo $y; ?>">

                                                        Historic Editions <?php echo $y; ?>

                                                    </a>

                                                </h4>

                                            </div>

                                            <div id="collapse<?php echo $y; ?>" class="panel-collapse collapse<?php

                                            if ($y == $searchyear) {

                                                echo " in";

                                            }

                                            ?>" role="tabpanel" aria-labelledby="heading<?php echo $y; ?>">

                                                <div class="panel-body">

                                                    <div class="table-responsive">

                                                        <table class="table table-hover">

            <!--                                        <tr>

                                      <th>Report Title</th>

                                  <th>Year</th>

                                  <th>MS EXCEL</th>

                                  <th>PDF</th>

                              </tr>                                       -->

                                                            <tr>

                                                                <td>Top 50 Airports &acute;<?php echo $y; ?></td>                                            

                                                                <td><?php echo $y ?></td>

                                                                <td><a href="<?php echo site_url('excel/report/top-50-airports/' . $y); ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                <td><a href="<?php echo site_url('pdf/report/top-50-airports/' . $y); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>

                                                            </tr>                                       

                                                            <tr>

                                                                <td>Top Int&acute; Airports &acute;<?php echo $y; ?></td>                                            

                                                                <td><?php echo $y ?></td>

                                                                <td><a href="<?php echo site_url('excel/report/top-int-airports/' . $y); ?>"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                <td><a href="<?php echo site_url('pdf/report/top-int-airports/' . $y); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>

                                                            </tr>

                                                            <tr>

                                                                <td>Lease Expire &acute;<?php echo $y; ?></td>                                            

                                                                <td><?php echo $y ?></td>

                                                                <td><a href="<?php echo site_url('excel/report/lease-expire/' . $y); ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                <td><a href="<?php echo site_url('pdf/report/lease-expire/' . $y); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>

                                                            </tr>



<!--                                                            <tr>



                                                                                                            <form action="<?php // echo site_url('pdf/report/lease-expire/' . $year);     ?>">

                                                            <form action="<?php // echo site_url('pdf/report/lease-expire/');     ?>">

                                                                <td>Lease Expire &acute;(All Years)</td>                                            

                                                                <td><?php // echo $y      ?><input name="areportyear" type="text" id="areportyear" size="20" maxlength="20"></td>

                                                                <td>



                                                                    <span  onclick="CheckReport(2);"><i class="fa fa-file-excel-o"></i>&nbsp;</span>



                                                                </td>

                                                                <td>



                                                                    <span onclick="CheckReport(1);"><i class="fa fa-file-pdf-o"></i>&nbsp;</span>



                                                                </td>

                                                            </form>

                                                            </tr>-->



                                                            <tr>

                                                                <td>Ratio Report - includes:(<small>Business to Leisure,Pre to Post Security,Average Dwell Time</small>)</td>

                                                                <td><?php echo $y ?></td>

                                                                <td><a href="<?php echo site_url('excel/report/ratio-report/' . $y); ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                <td><a href="<?php echo site_url('pdf/report/ratio-report/' . $y); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>

                                                            </tr>

                                                            <tr>

                                                                <td>All Airports <?php echo $y; ?></td>

                                                                <td><?php echo $y; ?></td>

                                                                <td><a href="<?php echo site_url('excel/report/all-airports/' . $y); ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                <td><a href="<?php echo site_url('pdf/report/all-airports/' . $y); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>                                            

                                                            </tr>

                                                            <?php

                                                            foreach ($listofcate as $cate):

                                                                if ($cate['category'] != "Advertising") {

                                                                    ?>



                                                                    <tr>

                                                                        <td> Top 50 Terminals by <?php echo $cate['category']; ?> &acute;<?php echo $y; ?></td>                                            

                                                                        <td><?php echo $y ?></td>

                                                                        <td><a href="<?php echo site_url('excel/report/' . strtolower(str_replace(array(' ', '/'), "-", $cate['category'])) . '/' . $y); ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                        <td><a href="<?php echo site_url('pdf/report/' . strtolower(str_replace(array(' ', '/'), "-", $cate['category'])) . '/' . $y); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>

                                                                    </tr> 

                                                                     



                                                                    

                                        <!--                                            <tr>

                                                                                        <td> Top 10 Airports based on Car Rental Revenue &acute;<?php // echo $y;     ?></td>                                            

                                                                                        <td><?php // echo $y     ?></td>

                                                                                        <td><a href="<?php // // echo site_url('excel/report/'.strtolower(str_replace(array(' ','/'),"-",$cate['category'])).'/'.$y);    ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                                        <td><a href="<?php // echo site_url('pdf/report/top10carrental/'.$y);     ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>

                                                                                    </tr> -->

                                                                <?php } ?>                                       

                                                            <?php endforeach; ?>

                                                                    

                                                                    <tr>

                                                                        <td> Airport's Passenger Traffic &acute;<?php echo $y; ?></td>                                            

                                                                        <td><?php echo $y ?></td>

                                                                        <td><a href="<?php echo site_url('excel/report/total-airports-passenger/'.$y);      ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a></td>

                                                                        <td><a href="<?php echo site_url('pdf/report/total-airports-passenger/' . $y); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a></td>

                                                                    </tr>

                                                                    

                                                                    <tr>



                                                                                            <!--<form action="<?php // echo site_url('pdf/report/lease-expire/' . $year);     ?>">-->

                                                                    <form method="POST" id="report_form" target="_blank" action="<?php // echo site_url('pdf/report/lease-expire/');     ?>" >

                                                                        <td>Lease Expire &acute;(All Years)</td>                                            

                                                                        <td><input name="areportyear" type="text" id="areportyear" size="20" maxlength="20" placeholder="Enter Year"></td>

                                                                        <td>



                                                                            <a href="javascript:void(0)"><span  onclick="CheckReport(2);"><i class="fa fa-file-excel-o"></i>&nbsp;</span></a>



                                                                        </td>

                                                                        <td>



                                                                            <a href="javascript:void(0)"> <span onClick="CheckReport(1);"><i class="fa fa-file-pdf-o"></i>&nbsp;</span></a>



                                                                        </td>

                                                                    </form>



                                                                    </tr>



                                                        </table>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                        <?php

                                    }

                                }

                            } else {

                                ?>                      

                                <div class="panel panel-primary">

                                    <div class="panel-heading" role="tab" id="heading<?php echo $this->common->GetSessionKey('accesslevel'); ?>">

                                        <h4 class="panel-title">

                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $this->common->GetSessionKey('accesslevel'); ?>" aria-controls="collapse<?php echo $this->common->GetSessionKey('accesslevel'); ?>">

                                                <!--Historic Editions <?php // echo $this->common->lastyear();      ?>-->

                                                Historic Editions <?php echo date("Y"); ?>

                                            </a>

                                        </h4>

                                    </div>

                                    <div id="collapse<?php echo $this->common->GetSessionKey('accesslevel'); ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $this->common->GetSessionKey('accesslevel'); ?>">

                                        <div class="panel-body">

                                            <div class="table-responsive">

                                                <table class="table table-hover">

                                                    <tr>

                                                        <th>Report Title</th>

                                                        <th>Year</th>

                                                        <?php

                                                        if ($this->common->GetSessionKey('accesslevel') == 2) {

                                                            ?>

                                                            <th>&nbsp;</th>

                                                            <?php

                                                        }

                                                        ?>

                                                        <th>MS EXCEL</th>

                                                        <th>PDF</th>

                                                    </tr>                                       

                                                    <?php /* foreach($reportslist as $report): ?>

                                                      <tr>

                                                      <td>

                                                      <?php echo $report['report_title']; ?>

                                                      </td>

                                                      <td>

                                                      <?php echo $this->common->lastyear(); ?>

                                                      </td>

                                                      <?php

                                                      if($this->common->GetSessionKey('accesslevel')==2)

                                                      {

                                                      ?>

                                                      <td>

                                                      <?php if(strpos($report['report_title'],'50')) { ?>

                                                      <div class="col-sm-5">

                                                      <input type="text" class="form-control col-sm-12" value="50" id="max<?php echo $report['report_id']; ?>">

                                                      </div>

                                                      <?php } ?>

                                                      </td>

                                                      <?php

                                                      }

                                                      ?>

                                                      <td align="center">

                                                      <?php

                                                      if($this->common->GetSessionKey('accesslevel')==2 && strpos($report['report_title'],'50')>0)

                                                      {

                                                      ?>

                                                      <a href="javascript:void" onClick="makelink('<?php echo site_url('excel/report/'.$report['url'].'/'.$this->common->lastyear()); ?>','<?php echo "max".$report['report_id']; ?>')" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a>

                                                      <?php } else { ?>

                                                      <a href="<?php echo site_url('excel/report/'.$report['url'].'/'.$this->common->lastyear()); ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a>

                                                      <?php } ?>

                                                      </td>

                                                      <td align="center">

                                                      <?php

                                                      if($this->common->GetSessionKey('accesslevel')==2 && strpos($report['report_title'],'50')>0)

                                                      {

                                                      ?>

                                                      <a href="javascript:void" onClick="makelink('<?php echo site_url('pdf/report/'.$report['url'].'/'.$this->common->lastyear()); ?>','<?php echo "max".$report['report_id']; ?>')" target="_blank"><i class="fa fa-file-pdf-o" title="Click Here to Download"></i>&nbsp;</a>

                                                      <?php } else { ?>

                                                      <a href="<?php echo site_url('pdf/report/'.$report['url'].'/'.$this->common->lastyear()); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a>

                                                      <?php } ?>

                                                      </td>

                                                      </tr>

                                                      <?php endforeach; */ ?>






                                                    <tr>



                                                                            <!--<form action="<?php // echo site_url('pdf/report/lease-expire/' . $year);     ?>">-->

                                                    <form method="POST" id="report_form" target="_blank" action="<?php // echo site_url('pdf/report/lease-expire/');     ?>" >

                                                        <td >Lease Expire &acute;(All Years)</td>                                            

                                                        <td>
                                                       <!--  <input name="areportyear" type="text" id="areportyear" size="20" maxlength="20" placeholder="2018 E.g."> -->
                                                    <!-- <select name="areportyear" id="areportyear">

    <?php 
    //for($i=2011; $i<=2040; $i++)
     {
      ?>

     <option value="<?php //echo $i;?>"><?php //echo $i;?></option>
    <?php
        }
        ?> 
 <option name="areportyear"> </option>   
    </select>  -->



    <select id="areportyear" multiple="multiple"  name="areportyear[]" style="width: 70% " >
<?php
    for($i=2011; $i<=2040; $i++){
        $j= $i+1;
        if( $i == 2018 ){ 
            echo "<option selected = 'selected' value='".$i."'>".$i."</option>";
        } else{ 
            
            echo "<option value='".$i."'>".$i."</option>";
        }
    }
    ?>
</select>
                                                        </td>

                                                        <td align="center">



                                                            <a href="javascript:void(0)"> <span  onclick="CheckReport(2);"><i class="fa fa-file-excel-o"></i>&nbsp;</span></a>



                                                        </td>

                                                        <td align="center">



                                                            <a href="javascript:void(0)"> <span onClick="CheckReport(1);"><i class="fa fa-file-pdf-o"></i>&nbsp;</span></a>



                                                        </td>

                                                    </form>



                                                    </tr>

                                                    <?php foreach ($reportslist as $report): ?>

                                                        <tr>

                                                            <td>

                                                                <?php echo $report['report_title']; ?>

                                                            </td>

                                                            <td>

                                                                <?php echo date("Y"); ?>

                                                            </td>

                                                            <?php

                                                            if ($this->common->GetSessionKey('accesslevel') == 2) {

                                                                ?>

                                                                <td>

                                                                    <?php if (strpos($report['report_title'], '50')) { ?>                                                                       

                                                                        <div class="col-sm-5">

                                                                            <input type="text" class="form-control col-sm-12" value="50" id="max<?php echo $report['report_id']; ?>">

                                                                        </div>

                                                                    <?php } ?>

                                                                </td>

                                                                <?php

                                                            }

                                                            ?>

                                                            <td align="center">

                                                                <?php

                                                                if ($this->common->GetSessionKey('accesslevel') == 2 && strpos($report['report_title'], '50') > 0) {

                                                                    ?>

                                                                    <a href="javascript:void" onClick="makelink('<?php echo site_url('excel/report/' . $report['url'] . '/' . date("Y")); ?>', '<?php echo "max" . $report['report_id']; ?>')" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a>

                                                                <?php } else { ?>

                                                                    <a href="<?php echo site_url('excel/report/' . $report['url'] . '/' . date("Y")); ?>" target="_blank"><i class="fa fa-file-excel-o" title="Click Here to Download"></i>&nbsp;</a>

                                                                <?php } ?>

                                                            </td>

                                                            <td align="center">

                                                                <?php

                                                                if ($this->common->GetSessionKey('accesslevel') == 2 && strpos($report['report_title'], '50') > 0) {

                                                                    ?>

                                                                    <a href="javascript:void" onClick="makelink('<?php echo site_url('pdf/report/' . $report['url'] . '/' . date("Y")); ?>', '<?php echo "max" . $report['report_id']; ?>')" target="_blank"><i class="fa fa-file-pdf-o" title="Click Here to Download"></i>&nbsp;</a>

                                                                <?php } else { ?>

                                                                    <a href="<?php echo site_url('pdf/report/' . $report['url'] . '/' . date("Y")); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;</a>

                                                                <?php } ?>                                                            

                                                            </td>

                                                        </tr>

                                                    <?php endforeach; ?>



                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                </div>                                              

                            <?php }

                            ?>

                        </div>

                    </div>                 

                </div>

            </section>

        </section> <!-- END class="main-content">-->

        

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">



            <div class="modal-dialog">



                <div class="modal-content">



                    <div class="modal-header">



                        <input type="hidden" id="recdelid" value="">



                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>



                        </button>



                        <h4 class="modal-title" id="myModalLabel">Alert!</h4>



                    </div>



                    <div class="modal-body">



                        Please Enter the Year first



                    </div>



                    <div class="modal-footer">



                        <button type="button" class="btn btn-success" data-dismiss="modal">



                            <i class="fa fa-check">&nbsp;</i>OK</button>        



<!--                        <button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete



                        </button>        -->



                    </div>



                </div>



            </div>



        </div>

        

        <?php echo $this->load->view($footer); ?>

    </div>

    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        

    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/chosen.jquery.min.js"></script>

    <script type="text/javascript">

                                                            function makelink(url, id)

                                                            {

                                                                if (document.getElementById(id).value != 50)

                                                                {

                                                                    var nurl = url + "/" + document.getElementById(id).value;

                                                                    window.open(nurl, '_new');

                                                                } else

                                                                {

                                                                    window.open(url, '_new');

                                                                }

                                                            }

                                                            jQuery(document).ready(function ()

                                                            {

                                                                jQuery(".chosen").chosen();









                                                            });



                                                            $('#butn').on('click', function () {

                                                                if ($("#search").val() == '')

                                                                {

                                                                    alert("Please select a year");

                                                                    return false;

                                                                }

                                                                else

                                                                {

                                                                    return true;

                                                                }

                                                            });





                                                            var areportyear = '';



                                                            function CheckReport(e)

                                                            {



                                                                //areportyear = $('#areportyear').val();

                                                                //Loading the variable
                                                               

                                                                //Splitting it with : as the separator
                                                                //var i=0;
                                                                 $.each($('#areportyear').val(), function(i, val){ 

                                                                   // alert(i);
                                                                    if(i==0){

                                                                    areportyear += val;
                                                                    }

                                                                    else{
                                                                  
                                                                   areportyear += " "+val;
                                                               }
                                                                    
                                                                });
                                                                
                                                            
                                                                                                                            

                                                                if (areportyear != '')

                                                                {

                                                                if (e == 1)

                                                                {





//                                                                    alert(areportyear);

//                                                                    return false;

                                                                    var action = '<?php echo site_url('pdf/report/lease-expire'); ?>';

//                                                                    alert(action);

//                                                                     return false;

                                                                    $('#report_form').attr('action', action);

                                                                    $('#report_form').submit();

                                                                }

                                                                else if (e == 2)

                                                                {

                                                                    var action = '<?php echo site_url('excel/report/lease-expire'); ?> ';

                                                                    $('#report_form').attr('action', action);

                                                                    $('#report_form').submit();

                                                                    //alert(e);

                                                                }

                                                                }

                                                                else

                                                                {

                                                                    $("#myModal").modal("show");

//                                                                    alert('Please Enter the Year First');

                                                                }

                                                                //alert('ok');

                                                            }

    </script>    



</body>

</html>

