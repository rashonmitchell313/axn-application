<?php echo $this->load->view($header);?>
    <title>
		<?php 
        if($this->uri->segment(0)!='')
        {
        echo 'Reports | ARN Fact Book '.$this->uri->segment(0);
        }
        else 
        {
        echo 'Reports | ARN Fact Book';
        }
        ?>
    </title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets');?>/css/choseen.css">
</head>
<body>
<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>       
        <div class="container-fluid">
	 		 <div class="row">             
         	 	  <section class="main-content">
                  		<section class="container-fluid">
                        	<?php echo $this->load->view('frontend/include/breadcrumb');?>              
               				<?php echo $this->common->getmessage(); ?>                            
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <h1 class="default-title"><i class="fa fa-file-pdf-o"></i>&nbsp;Reports for ARN Online Fact Book</h1>                                
                        		 The reports listed in the table below represent the ARN Fact Book results for a specific year. Remember
the year references the year the Fact Book was published which represents numbers from the previous
year.
                    </p>
                    <p>
                    Each report opens as a PDF file in a separate window. You can peruse these reports online or once a
report is open, you can easily print or save as a PDF on your own computer.
                    </p>
                    <a href="<?php echo site_url('reports/dbe_companies'); ?>" class="btn btn-danger">
                    <i class="fa fa-arrow-right"></i>&nbsp;DBE Companies</a>
                                </div>
                              </div>
                             <div class="panel panel-default">
                              <div class="row">
                              <div class="panel-body">
                    				<section class="main-content">        	        	
										<section class="col-sm-4">
                                        	<div class="panel panel-default">
                                              <div class="panel-heading">Select Report</div>
                                              <div class="panel-body">
                                               <ul class="nav nav-pills nav-stacked nav-pills-stacked-example">                                                 
                                                  <?php
												  	foreach($reportslist as $report):
												  ?>
                                                 	 <li role="presentation" class="getfield <?php if($report['report_id']==1) { echo "active"; } ?>" id="<?php echo $report['report_id'];  ?>">
                                                     	<a id="heading" href="javascript:void(0)"><i class="fa fa-hand-o-right">&nbsp;</i>
															<?php echo $report['report_title'];  ?>
                                                        </a>
                                                        
                                                      </li>                                                 
                                                  <?php endforeach; ?>
                                                </ul>
                                              </div>
                                            </div>
                                        </section>
                                        <section class="col-sm-8 container-fluid">
                                        	 <div id="check" class="alert alert-danger hide">Check atleast one checkbox checked.</div>
                                              <div id="years" class="alert alert-danger hide">Please enter a year.</div>
                                             
                                             <form class="form-horizontal mform" target="new" id="meform" action="<?php echo site_url('customreports'); ?>" method="post" class="form-horizontal">
                                             	
                                                <input type="hidden" id="reporttype" name="reporttype" value="pdf">
                                            	<input type="hidden" name="reportid" id="reportid" value="1">
                                                <input type="hidden" id="reportformat" name="reportformat" value="pdf"> 
                                                <div class="subfluid listed-items-check"></div>	                                                
                                                <div class="form-group listed-items-check2">
                                                    <label for="inputPassword3" class="col-sm-2 control-label">Show Records</label>
                                                    <div class="col-sm-2">
                                                    	<input type="text" class="form-control" name="limit" placeholder="Show Records" value="50">
                                                    </div>
                                                </div>
                                               <?php if($this->session->userdata('year') != "") { 
                                                echo '<div class="form-group listed-items-check3">
                                                    <label for="inputPassword3" class="col-sm-2 control-label">Year</label>
                                                    <div class="col-sm-7">
                                                         <select name="years" class="form-control">
                                                  	<option value="">Select Year</option>';
													
													 foreach(($this->session->userdata('year')) as $year)
													
													{
                                                  		echo '<option value="'.$year.'">'.$year.'</option>';
														
													} 
                                                  echo '</select>
                                                    </div>
                                                </div>';
                                                 } 
                                                 else if($this->session->userdata('year') == "") { ?>
                                                 <div class="form-group listed-items-check3">
                                                    <label for="inputPassword3" class="col-sm-2 control-label">Year</label>
                                                    <div class="col-sm-6">
                                                    	<!--<input type="text" class="form-control" name="years" placeholder="Show year">-->
														<select class="chosen form-control" id="search" name="years">
                                                        	  <option value="">Select Year</option>
                                                              <?php for($i=2009; $i<=date("Y")-1; $i++){
                                                              echo '<option>'.$i.'</option>';  } ?>
                                                             
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                
                                                <div class="form-group listed-items-check4">
                                                    <div class="col-sm-3">
                                                        <button type="submit" class="btn btn-success" report="pdf">
                                                            <i class="fa fa-file-pdf-o"></i>&nbsp;Generate Report
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <button type="submit" class="btn btn-success" report="excel">
                                                            <i class="fa fa-file-excel-o"></i>&nbsp;Generate Report
                                                        </button>
                                                    </div>
                                                     <div class="col-sm-3">
                                                        <button type="submit" class="btn btn-success" report="web" >
                                                            <i class="fa fa-globe"></i>&nbsp;Web Preview
                                                        </button>
                                                    </div>
                                                </div>    
                                             </form>
                                             <form target="_new" id="rep2" method="post" class="form-horizontal singleairport hide">
                                             
                                              <div class="subfluid listed-items-check"></div>
                                             
                                              <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-12 control-label waitlist" style="text-align:left;"></label>                                                
                                              </div>
                                              <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Select Year</label>
                                                <div class="col-sm-10">
                                                  <select name="year[]" multiple id="air_year" class="form-control" onChange="getairportlist(this)">
                                                  	<option value="">---Select Year---</option>
													<?php for($y=2010;$y<date("Y")-1;$y++)
													{
                                                  		echo '<option value="'.$y.'">'.$y.'</option>';
													} ?>
                                                  </select>
                                                  <input type="hidden" id="reportformat" name="reportformat" value="pdf">
                                                  <input type="hidden" name="reportid" id="report_id" value="1">
                                                </div>
                                              </div>
                                              
                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Select Airports</label>
                                                <div class="col-sm-10">
                                                  <select name="apid[]" multiple class="form-control aplist" id="apid">
                                                  	<option value="">---Select Airports---</option>													
                                                  </select>
                                                </div>
                                              </div>                                              
                                              <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                  <button report="pdf" type="submit" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF Report</button>
                                                  <button report="excel" type="submit" class="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp;Excel Report</button>
                                                  <button report="web" type="submit" class="btn btn-primary"><i class="fa fa-globe"></i>&nbsp;Web View</button>
                                                </div>
                                              </div>
                                            </form>
                                        </section>
                                        </section>                                       
                              			 </div>
                               
                              </div>                           
                  </section>
             </div>
        </div>
</div>       
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('fassests');?>/js/chosen.jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
        $(".getfield").first().trigger('click');
    });
	function getairportlist(v)
	{
		if(v.value>0)
		{
			$(".waitlist").html('<i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom"></i>');
			$.ajax({ 
					url:"<?php echo site_url('ajax/getairportlist') ?>",
					data:"aid="+v.value+"&action=alist",
					type:"POST",
					cache:false,
					dataType:"json",
					success: function(data)
					{					
						$(".aplist").empty();
						$.each(data,function(ele,val)
						{
							$(".aplist").append('<option value="'+val.aid+'">'+val.aname+'</option>');
						});
						$(".waitlist").empty();
					}
			});
		} else
		{
			$(".aplist").empty();
		}
	}
	$(".btn-primary").click(function()
	{
		var apid = $("#apid").val();
		if(apid == "" || apid == null)
		{
			$(".waitlist").html('<div class="alert alert-danger">Select Airpot first..</div>').slice(1,10);	
			$(".waitlist").removeClass("hide");
			return false;
		}
		else
		{
			if($(this).attr("report")=="excel")
			{
				$("#rep2").attr("action","<?php echo site_url('customreportexcel'); ?>");
			}
			else if($(this).attr("report")=="web")
			{				
				$("#rep2").attr("action","<?php echo site_url('airportdetails'); ?>");
			}
			else
			{				
				$("#rep2").attr("action","<?php echo site_url('customreports'); ?>");
			}
			$("#reportformat").val($(this).attr("report"));
			var ischek=false;
			$(".checkbox input:checked").each(function() 
			{
				ischek=true;
			});
			if(ischek==false)
			{
				$(".alert").removeClass("hide");
			} else
			{
				$(".alert").addClass("hide");	
				return true;
			}		
			return false;
			return true;
		}
	});
	$(".btn-success").click(function()
	{			
		if($(this).attr("report")=="excel")
		{
			$("#reporttype").val("excel");	
			$("#meform").attr("action","<?php echo site_url('customreportexcel'); ?>");
		} else if($(this).attr("report")=="web")
		{
			$("#reporttype").val("web");	
			$("#meform").attr("action","<?php echo site_url('webreport'); ?>");			
		}
		else
		{
			$("#reporttype").val("pdf");	
			$("#meform").attr("action","<?php echo site_url('customreports'); ?>");
		}
		$("#reportformat").val($(this).attr("report"));
		var ischek=false;
		$(".checkbox input:checked").each(function() 
		{
           	ischek=true;
        });
		if(ischek==false)
		{
			$("#check").removeClass("hide");
		} else
		{
			$("#check").addClass("hide");	
			return true;
		}		
		return false;
	});
	$(".getfield").click(function(e) 
	{           		
		$(".nav-pills li").each(function(index, element) 
		{
           	$(element).removeClass("active");
        });	
		var ic=0;
		var rs="",re="";
		$(this).addClass("active");	
		$(".subfluid").html('<i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom"></i>');
		var reportid=$(this).attr("id");		
		$("#reportid").val(reportid);		
		$("#report_id").val(reportid);
		$.ajax({ 
			url:"<?php echo site_url("ajax/getreportcolumns"); ?>",
			type:"POST",
			cache:false,
			dataType:"json",
			data:"reportid="+reportid+"",			
			success: function(html)
			{				
				 $(".subfluid").empty();	
				 $.each(html, function(i, v) 
				 {					 
					 var ht='<div class="col-sm-4"><div class="checkbox ckd spcing-check"><label><div class="squaredTwo"><input name="arr['+i+']" type="checkbox"><span for="squaredTwo"></span></div><p>'+v+'</p></label></div></div>';
					 ic++;
					 $(".subfluid").append(ht);					
				 });			
				 if(reportid!=12)
				 {
					 $(".mform").removeClass("hide");
					 $(".singleairport").addClass("hide");
				 } else
				 {
					$(".mform").addClass("hide").slideUp(300);
					$(".singleairport").removeClass("hide");					
				 }
			}				
		});
		return false;
	});		
	
	jQuery(document).ready(function()
	{
	jQuery(".chosen").chosen();
	});

	$('.btn-success').on('click', function() {
		if($("#search").val() == '')
		{
			alert("Please select a year"); 
			return false;
		}
		else
		{
			return true;
		}
	});
</script>
</body>
</html>
