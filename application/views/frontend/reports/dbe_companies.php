<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Dbe Companies | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Dbe Companies | ARN Fact Book';
}
?>
</title>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>       
        <section class="main-content">        	        	
				<?php /*?><section class="col-sm-2">
<?php foreach($reports_adds as $adds): ?>
<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
<?php endforeach; ?>				
				</section><?php */?>		     	
        	<section class="container-fluid">
            	<?php echo $this->load->view('frontend/include/breadcrumb');?>              
                <?php echo $this->common->getmessage(); ?>
                <div class="panel panel-primary">
                      <div class="panel-heading">List of DBE Companies</div>
                      <div class="panel-body">
                    	  	<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                               		<tr class="info">
                                    	<th>DBE</th>
                                        <th>Company</th>
                                        <th>Company Type</th>
                                        <th>More Info</th>
                                    </tr>
                                    <?php foreach($dbecompany as $co): ?>
										<tr>
                                        	<td>YES</td>
                                            <td><?php echo $co['companyname']; ?></td>
                                            <td><?php echo $co['companytype']; ?></td>
                                            <td><i class="fa fa-search"></i>&nbsp;<a href="<?php echo site_url('wizard/company/'.$co['cid']); ?>">More Details</a></td>
                                        </tr>	
									<?php  endforeach; ?>
                                </table>
                            </div>
                      </div>             
                </div>
            </section>
		  </section> <!-- END class="main-content">-->
        <?php echo $this->load->view($footer);?>
    </div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
</body>
</html>
