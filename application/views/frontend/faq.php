<?php echo $this->load->view($header); ?>
<title>
    <?php
    if ($this->uri->segment(0) != '') {
        echo 'FAQ | ARN Fact Book ' . $this->uri->segment(0);
    } else {
        echo 'FAQ | ARN Fact Book';
    }
    ?>
</title>
</head>

<link href="<?php echo base_url('fassests'); ?>/css/faq.css" rel="stylesheet" type="text/css" />
<body>
    <div class="wrapper_main">
        <?php echo $this->load->view($menu); ?>
        <section class="container-fluid">

            <div class="section-first">
                <div class="wrap_inner"><br>
                    <?php echo $this->load->view('frontend/include/breadcrumb'); ?>
                    <?php /* ?><div class="panel panel-default">
                      <div class="panel-heading">
                      <h4 class="panel-title">Categories</h4>
                      </div>
                      <div class="panel-body">
                      <?php foreach($faqs as $faq): ?>

                      <a href="#cate<?php echo $faq['category_id']; ?>" onClick="myFunction(<?php echo $faq['category_id'];?>)"><?php echo $faq['category_name'].'<br>';?></a>

                      <?php endforeach;?>

                      </div>
                      </div><?php */ ?>
                    <div class="error"><?php echo $this->common->getmessage(); ?></div>
                    <div class="tabbable tabs-left">
                        <div class="panel panel-primary col-lg-2 col-md-3">
                            <div class="panel-heading">FAQ Categories</div>
                            <div class="panel-body">
                                <ul class="nav nav-tabs" id="myTab3">

                                    <?php foreach ($faqcate as $category) { ?>
                                        <li class="col-md-12 col-sm-3 col-xs-12">
                                            <a class="tog" data-toggle="tab" href="#home<?php echo $category['category_id']; ?>">
                                                <i class="fa fa-angle-double-right"></i>
                                                <?php echo $category['category_name']; ?>
                                            </a>
                                        </li>                 
                                    <?php } ?>
                                </ul></div></div>
                        <div id="accordion">
                            <div class="tab-content">

                                <?php
                                $count = 0;
                                foreach ($faqcate as $category):
                                    ?>
                                    <div id="home<?php echo $category['category_id']; ?>" class="tab-pane <?php
                                    if ($count == 0) {
                                        echo "active";
                                    }
                                    ?>">
                                    <!--<div id="home<?php // echo $category['category_id'];                  ?>" class="tab-pane">-->
                                        <div class="col-md-offset-1">  

                                            <?php
                                            foreach ($faqs as $faq):
                                                if ($faq['category_id'] == $category['category_id']) {
                                                    ?>	
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading" style="cursor: pointer;">
                                                            <h5><i class="fa fa-chevron-right"></i> <?php echo html_entity_decode($faq['faq_question'], ENT_COMPAT, 'ISO-8859-1'); ?></h5>
                                                        </div>
                                                        <div class="tabs">
                                                            <div class="panel-body">
                                                                <p><i class="fa fa-arrow-right"></i> <?php echo html_entity_decode($faq['faq_answer'], ENT_COMPAT, 'ISO-8859-1'); ?></p><br>
                                                                <p>Was this answer helpfull?</p>
                                                                <form method="post">
                                                                    <input type="hidden" value="<?php echo $faq['faq_id']; ?>" name="faqid">
                                                                    <button type="button" value="1" faqid="<?php echo $faq['faq_id']; ?>" name="yes" class="btn btn-success btn-xs faqfeed">
                                                                        <i class="fa fa-thumbs-o-up"></i> Yes</button> 
                                                                    <button type="button" value="2" faqid="<?php echo $faq['faq_id']; ?>" name="no" class="btn btn-danger btn-xs faqfeed">
                                                                        <i class="fa fa-thumbs-o-down"></i> No</button> <br><br>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div> <?php
                                                }
                                            endforeach;
                                            ?>


                                        </div>
                                    </div>
                                    <?php
                                    $count++;
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>



                    <div class="clearfix"></div>

                    <div class="row">



                    </div>





                </div>



                <?php /* ?><div id="accordion">
                  <?php foreach($faqs as $k=>$faq): ?>
                  <div id="cate<?php echo $faq['category_id']; ?>" style="display:none;">
                  <div class="panel panel-default">
                  <div class="panel-heading"><h4><?php echo html_entity_decode($faq['faq_question'], ENT_COMPAT, 'ISO-8859-1');?></h4></div>
                  <div class="panel-body">
                  <p><?php echo html_entity_decode($faq['faq_answer'], ENT_COMPAT, 'ISO-8859-1'); ?></p>
                  </div>
                  </div>
                  </div>
                  <?php endforeach; ?>
                  </div><?php */ ?>
            </div>

        </section>
    </div>
    <!--</div>
    </div>-->
    <!--</section>-->

    <!-- Button trigger modal -->
    <!--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
      Launch demo modal
    </button>-->

    <!-- Modal -->
    <div class="modal fade" id="reason_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title" id="myModalLabel">Why this this answer not helpfull?</h4>
                </div>
                <form action="" method="post" role="form" id="faqfeedback" data-toggle="validator">
                    <div class="modal-body">

                        <div class="form-group has-feedback feedfaq">
                            <label for="message-text" class="control-label">Message:</label>
                            <textarea class="form-control" name="reason" id="reason" placeholder="Please type the reason." data-error="Please enter any Suggestion/reason in given Message box." required ></textarea>
                            <div class="col-sm-10 help-block with-errors"></div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="clearold" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success disabled" id="btn2">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php echo $this->load->view($footer); ?>
</div>
<!--Page Specific JS Files-->
<?php //$this->load->view('frontend/include/external_scripts');    ?>
<!--End Page Specific JS Files-->
<script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('fassests'); ?>/js/modernizr.min.js"></script>
<script src="<?php echo base_url('fassests'); ?>/js/jquery-ui.js"></script>
<script src="<?php echo base_url('fassests'); ?>/js/jquery.slicknav.js"></script>
<script src="<?php echo base_url(); ?>assets/js/validator.js"></script>
<script type="text/javascript">

    $('#faqfeedback').validator();

    $(".faqfeed").click(function ()
    {


//        var feedback = $(this).attr("value");
//        var faqid = $(this).attr("faqid");
        feedback = $(this).attr("value");
        faqid = $(this).attr("faqid");
//        alert(feedback+ " , "+ faqid); return false;
//        reason = '';

        if (feedback == '2')
        {

//            $("#reason_modal").modal('show');
//            $("#reason_modal").modal('toggle');
            $("#reason_modal").show();
            $("#reason_modal").modal({
                backdrop: 'static',
                keyboard: false
            });

//            $("#reason").html("");

            $("#reason_modal").modal().one('click', '#btn2', function () {

                reason = document.getElementById("reason").value;

                $("#reason_modal").modal('toggle');

//                console.log(reason);
//                alert(reason);
//                return false;

                var datastring = "feedback=" + feedback + "&faqid=" + faqid + "&reason=" + reason + "&action=updatefeedback";
                $.ajax({
                    type: "POST",
                    async: false,
                    cache: false,
                    url: "<?php echo site_url('ajax/doaction'); ?>",
                    data: datastring,
                    success: function (responsce)
                    {
                        if (responsce == 1)
                        {
                            $(".error").html('<div class="alert alert-success">Your feedback has been submitted successfully.</div>');
                        }
                        else if (responsce == -1)
                        {
                            $(".error").html('<div class="alert alert-danger">You have already submitted your feedback about this question.</div>');
                        }
                    }

                });

            });

        }
        else if (feedback == '1')
        {

            reason = '';

            var datastring = "feedback=" + feedback + "&faqid=" + faqid + "&reason=" + reason + "&action=updatefeedback";
            $.ajax({
                type: "POST",
                async: false,
                cache: false,
                url: "<?php echo site_url('ajax/doaction'); ?>",
                data: datastring,
                success: function (responsce)
                {
                    if (responsce == 1)
                    {
                        $(".error").html('<div class="alert alert-success">Your feedback has been submitted successfully.</div>');
                    }
                    else if (responsce == -1)
                    {
                        $(".error").html('<div class="alert alert-danger">You have already submitted your feedback about this question.</div>');
                    }
                }

            });

        }
    });


    $("#clearold").click(function ()
    {
//        console.log(feedbssssack, faqid);
        delete feedback;
        delete faqid;

    });
</script>
<script type="text/javascript">


//	$("#accordion").accordion({
//		collapsible: true,
////		active: 0,
//          
//  
//                  active: 'all',
////                $(this).tab-content('show');
////		active: "#home<?php // echo $category['category_id'];                   ?>",
////		active: ".tab-content",
//		animate: 250,
//		header: ".panel-heading"
//	});


// var a=document.forms["#faqfeedback"]["reason"].value;
//         if(a!==null)
//         {
//            alert('test123');
//             $("#btn2").prop("disabled", true);
//         }

    $("#faqfeedback").on("valid.bs.validator", function () {
        $("#btn2").prop("disabled", false);
    });

    $(document).ready(function (e) {
        $("#myTab3 li a").first().click();
        $("#btn2").addClass('disabled');

        $("#faqfeedback").on("invalid.bs.validator", function () {

            $("#btn2").prop("disabled", true);

        });

    });
    $("#reason").html("");
    /*$(".tog").on('click', function(e){
     var id = $(this).attr('href');
     $(id).accordion("active" , 0);
     });*/
</script>
</body>
</html>