<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo base_url('fincludes'); ?>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url('fincludes'); ?>/css/form.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url('fincludes'); ?>/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo base_url('fincludes'); ?>/js/megamenu.js"></script>
</head>
<body>
  <div class="header-top">
	 <div class="wrap"> 
		<div class="logo">
			<a href="<?php echo site_url() ;?>index.php"><img src="<?php echo base_url('fincludes'); ?>/images/logo2.png" width="182" height="41" alt=""/></a>
	    </div>
	    <div class="cssmenu">
		   <ul>
			 <li class="active"><a href="<?php echo site_url('signup'); ?>">Sign up</a></li>
			 <li><a href="<?php echo site_url('login'); ?>">Login</a></li> 
			 <li><a href="#">Contact Us</a></li>
		   </ul>
		</div>
		<div class="clear"></div>
 	</div>
   </div>
   <div class="header-bottom">
        <div class="wrap">
            <!-- start header menu -->
            <ul class="megamenu skyblue">
                <li><a class="color1" href="#">Home</a></li>
                <li><a class="color2" href="#">Services</a></li>
                <li><a class="color3" href="#">Support</a></li>
                <li><a class="color4" href="#">About us</a></li>
                <li><a class="color5" href="#">Terms and Conditions</a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
   <div class="register_account">
        <div style="font-size:18px" class="wrap"><?php echo $message; ?></div>
	</div>
    <div class="footer">
     <div class="copy">
       <div class="wrap">
        <p>2014 &copy; All rights reserved</p>
       </div>
     </div>
   </div>
</body>
</html>