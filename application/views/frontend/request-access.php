<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Request for Access | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Request for Access | ARN Fact Book';
}
?>
</title>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>       
        <section class="main-content">        	
        	<section class="container-fluid">
            	 <?php echo $this->load->view('frontend/include/breadcrumb');?>            	
            	<div class="col-md-3 col-sm-12 col-sm-3 login-left-sect">
                	<?php echo $this->common->getmessage();?>
                    <?php echo $this->load->view('frontend/include/loginform');?>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 login-right-sect">                	
                    <div class="row">
                    	<div class="row">
                        	<div class="error">
                            	
                            </div>
                        	<div class="alert alert-info">
                            	Request for Access to Modify Data.
                            </div>
                        </div>
                    	<div class="nav-book-info"> 
                        	<table width="100%">
                            	<tr>
                                	<td></td>
                                </tr>
                            </table>
                            <br/>                       	
                        	<div class="col-md-12 book-img-login padding-left-none">
            <form action="<?php echo site_url('login');?>" method="post" id="accessrequestform" onSubmit='return validate(Array("name", "Email", "comments" ));'>
                			<table width="100%">
                            	<tr>
                                	<td width="48%">
                                    	<div class="form-group">
                                            <label for="usr">Your name:</label>
                                            <input class="form-control" name="name" type="text" id="name" maxlength="75">
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>                                    
                                    <td width="48%">
                                    	<div class="form-group">
                                            <label for="usr">Your Email:</label>
                                            <input class="form-control" name="email" type="text" id="Email" maxlength="100">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                	<td>
                                    	<div class="form-group">
                                            <label for="usr">Name of Company you wish to modify:</label>
                                            <select class="form-control col-sm-4" name="requestcompany" id="requestcompany">
                                            	<option value="0">-- Please Select --</option>
                                                <?php foreach($ListofCompanies as $company): ?>
                                                	<option value="<?php echo $company['cid']; ?>"><?php echo $company['companyname']; ?></option>
												<?php endforeach; ?>
                                            </select>
                                        </div>
                                    </td>    
                                    <td>&nbsp;OR</td>                                
                                    <td>
                                    	<div class="form-group">
                                            <label for="usr">Name of Airport you wish to modify:</label>
                                           <select class="form-control col-sm-4" name="requestIATA" id="requestIATA">
                                            	<option value="0">-- Please Select --</option>
                                                <?php foreach($ListOfAirports as $airport): ?>
                                                	<option value="<?php echo $airport['aid']; ?>"><?php echo $airport['aname']; ?></option>
												<?php endforeach; ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="3">
                                    	<div class="form-group">
                                            <label for="usr">Comments:</label>
                                            <textarea class="form-control" name="comments" id="comments"></textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                	<td>
                                    	<button type="submit" class="btn btn-success">
                                        <i class="fa fa-check"></i>&nbsp;Send Request
                                        </button>&nbsp;<a href="<?php echo site_url(); ?>" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="3">&nbsp;</td>
                                </tr>   
                                <tr>
                                	<td colspan="3">                                    
                                    	<div class="alert alert-warnning">
                                        	This form will be sent to staff at ARN for approval. You will be notified via email regarding this request.
                                        </div>
                                    
                                    </td>
                                </tr>                             
                            </table>
                <div class="clearfix"></div>
            </form>	
                            </div>                                                  
                        </div>
                    </div>                    
                </div>                
            </section>
        </section>
        <?php echo $this->load->view($footer);?>
    </div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
<script type="text/javascript">
	function movetosuccess(msg)
	{
		$(".error").html('<div class="row"><div class="cols-sm-12"><div class="alert alert-success"><i class="fa fa-check fa-1x"></i>'+msg+'</div></div></div>'); 
		$('html, body').animate({ scrollTop: $('.error').offset().top }, 'slow');
	}
	function movetoerror(msg)
	{
		$(".error").html('<div class="row"><div class="cols-sm-12"><div class="alert alert-danger">'+msg+'</div></div></div>'); 
		$('html, body').animate({ scrollTop: $('.error').offset().top }, 'slow');
	}
	function wait()
	{
		$(".error").html('<div class="row"><div class="cols-sm-12"><div class="alert alert-success"><i class="fa fa-spinner fa-spin fa-1x"></i>&nbsp;Wait.......</div></div></div>'); 
		$('html, body').animate({ scrollTop: $('.error').offset().top }, 'slow');
	}
	function validate(listofields)
	{	
        
        		var msg="";
		var valid=1;
		var datastring="";
		var requestcompany=$("#requestcompany").val();
		var requestIATA=$("#requestIATA").val();		
		for(var i=0;i<listofields.length;i++)
		{

            
			if($("#"+listofields[i]).val()=="")
			{
				msg+="(<b>"+listofields[i].toUpperCase()+"</b>) Fields is required.\n";			
				valid=0;				
			}else 
			{
				if(datastring=="")
				 {
					datastring="field"+listofields[i]+"="+$("#"+listofields[i]).val();
				 } else
				 {
					 datastring+="&"+"field"+listofields[i]+"="+$("#"+listofields[i]).val();
				 }
			}
		} //// End for loop
						
		if(valid==0)
		{
			
			//$("#accessrequestform").[0].reset();
			movetoerror(msg);
		} else 
		{
			datastring+="&requestcompany="+requestcompany+"&requestIATA="+requestIATA+"&action=sendrequest";			
			$(".error").html(datastring);

			$.ajax({
				url:"<?php echo site_url('email/sendemail'); ?>",
				data:datastring,
				type:"POST",			
				cache:false,
				async:false,
				success: function(html)
				{
					if(html==1)
					{
					var msl1='<h4 align="center">Your email has been sent to ARN Staff. </h4>';
					var msl2='<h4 align="center">Someone will contact you shortly to respond to your inquiry.</h4';
					var msl3=msl1+"\n"+msl2;
					document.getElementById("accessrequestform").reset();
					movetosuccess(msl3);						
					} else
					{
						movetoerror(html);
					}
				}
		});
		}		
		return false;
	}
</script>    
</body>
</html>
