

<?php
error_reporting(1);

echo $this->load->view($header);
?>

<title>

    <?php
    if ($this->uri->segment(0) != '') {

        echo 'Company Search | ARN Fact Book ' . $this->uri->segment(0);
    } else {

        echo 'Company Search | ARN Fact Book';
    }
    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.bootstrap.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/jquery-ui-1.10.3.full.min.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

</head>

<body>

    <style>

        .ui-state-focus {
            background: #428BCA!important;
            color: #fff!important;
        }

        .input-group-addon {
            background: #fff !important;
            /*border: 1px solid #fff;*/
        }
        input.loading {
/*            background: url(http://www.xiconeditor.com/image/icons/loading.gif) no-repeat right center;*/
            background: url(<?php echo base_url('fassests');?>/images/loading.gif) no-repeat right center;
        }
    </style>


    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>       

        <section class="main-content">   



            <?php /* ?><section class="col-sm-2">



              <?php

              if(count($companycontact)>0) {

              foreach($companysearchadds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach;



              }

              else{



              foreach($company_search_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; } ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>            	            	

                <div class="row">                    	

                    <div class="col-sm-12">

                        <div class="panel panel-primary">

                            <div class="panel-heading">Company Search</div>

                            <div class="panel-body">

                                <div class="alert alert-info">

                                    <strong>How to use the search.</strong><br />

                                    Enter a full or partial name of a company to search for,

                                    click Search then click on the name of desired company. </div>



                                <?php if ($searcherror != "") { ?>

                                    <div class="error">

                                        <small>

                                            <div class="alert alert-danger">

                                                <b>Error!</b> <?php echo $searcherror; ?>

                                            </div>

                                        </small>

                                    </div>

                                <?php } ?>

                                <form action="<?php echo site_url('companysearch'); ?>" method="post" onSubmit="return valid();">

                                    <input type="hidden" name="action" value="sairports">

                                    <div class="form-group">

                                        <label for="exampleInputEmail1">Company Name:</label>

                                        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input name="search" class="form-control ui-autocomplete-input addon" id="search" size="45" placeholder="Company Name" type="text" 

                                                                                                                                 value="<?php
                                                                                                                                 if ($this->input->post("search") != "") {

                                                                                                                                     echo $this->input->post("search");
                                                                                                                                 }

                                                                                                                                 if (isset($clinfo[0]['companyname']) && $clinfo[0]['companyname'] != '') {

                                                                                                                                     echo $clinfo[0]['companyname'];
                                                                                                                                 }
                                                                                                                                 ?>">
                                        <!--<div id="s_spin"></div>-->

                                        <!--<div class="input-group">
                                        <span class="input-group-addon" id="s_spin"></span></div>-->
                                    </div>



									<div class="companysearch-btns">

                                    <button type="reset" class="btn btn-info"><i class="fa fa-undo"></i>&nbsp;Reset</button>

                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>

									</div>

                                </form>

                                <br>

                                <?php if ($searchresult != "") { ?>

                                    <div class="form-group">

                                        <label for="exampleInputEmail1">Search Results:</label>

                                        <?php echo $searchresult;
                                        ?>



                                    </div>



                                    <?php
                                }
                                ?>

                            </div>

                        </div>

                        <?php if (isset($clinfo[0]['companyname']) && $clinfo[0]['companyname'] != '') { ?>



                            <!--                            <div class="panel-group tabs-table" id="accordion" role="tablist" aria-multiselectable="true">-->

                            <div class="panel panel-primary">

                                <div class="panel-heading">

                                    <strong>

                                        <?php echo $clinfo[0]['companyname']; ?><br />            

                                        <?php echo $clinfo[0]['companytype']; ?>                                     

                                    </strong>

                                </div>

                                <div class="panel-body">

                                    <!-- comanpay contact stasrt here -->

                                    <!--<div class="panel-group tabs-table" id="accordion" role="tablist" aria-multiselectable="true">-->

                                    <div class="panel panel-primary">

                                        <div class="panel-heading" role="tab" id="contact">

                                            <h4 class="panel-title">

                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseTwo">

                                                    Contact(s)

                                                </a>

                                            </h4>

                                        </div>

                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contact">



                                            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 

                                                <ul id="myTabs" class="nav nav-tabs" role="tablist"> 

                                                    <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><i class="margin-right-5 fa fa-qrcode">&nbsp;</i>Grid View</a></li> 

                                                    <li role="presentation" class="" ><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="margin-right-5 fa fa-table">&nbsp;</i>Table View</a></li>

                                                </ul> 

                                                <br />
                                                <?php // if (count($companycontact) > 0) { ?>
                                                <div id="myTabContent" class="tab-content"> 

                                                    <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">



                                                        <?php if (count($companycontact) > 0) { ?>

                                                            <div class="panel-body">

                                                                <?php
                                                                foreach ($companycontact as $contact):
                                                                    ?>	



                                                                    <div class="col-sm-6">

                                                                        <div class="panel panel-primary" style="min-height:397px;">

                                                                            <div class="panel-heading">

                                                                                <?php echo $contact['cfname'] . " " . $contact['clname']; ?></div>



                                                                            <div class="panel-body">

                                                                                <div class="table-responsive">



                                                                                    <table class="table-bordered table-hover table-striped" width="100%">

                                                                                        <tbody>

                                                                                            <tr>

                                                                                                <td width="100"><i class="fa fa-phone margin-right-5"></i>&nbsp;Contact</td>

                                                                                                <td><?php echo $contact['cfname'] . " " . $contact['clname']; ?></td>            

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td><i class="margin-right-5 fa fa-book margin-right-5"></i>&nbsp;Title</td>

                                                                                                <td><?php echo $contact['ctitle']; ?></td>            

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td><i class="margin-right-5 fa fa-home"></i>&nbsp;Address</td>

                                                                                                <td><?php echo $contact['caddress1']; ?><br>

                                                                                                    <?php
                                                                                                    if ($contact['caddress2'] != "") {

                                                                                                        echo $contact['caddress2'] . '</br>';
                                                                                                    }
                                                                                                    ?>

                                                                                                    <?php echo $contact['ccity'] . ', ' . $contact['cstate'] . ' ' . $contact['czip'] . ' ' . $contact['ccountry'] ?>

                                                                                                </td>            

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td><i class="margin-right-5 fa fa-phone"></i>&nbsp;Phone</td>

                                                                                                <td><?php echo $contact['cphone']; ?></td>            

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td><i class="margin-right-5 fa fa-fax"></i>&nbsp;Fax</td>

                                                                                                <td><?php echo $contact['cfax']; ?></td>            

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td><i class="margin-right-5 fa fa-envelope-o"></i>&nbsp;E-mail</td>

                                                                                                <td><a href="mailto:<?php echo $contact['cemail']; ?>"><?php echo $contact['cemail']; ?></a></td>            

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td><i class="margin-right-5 fa fa-globe"></i>&nbsp;Web site</td>

                                                                                                <td><a href="<?php echo $this->common->weblink($contact['cwebsite']); ?>" target="_new"><?php echo $contact['cwebsite']; ?></a></td>            

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td><i class="margin-right-5 fa fa-print"></i>&nbsp;To Be Printed</td>

                                                                                                <td><?php echo $contact['printed']; ?></td>            

                                                                                            </tr>

                                                                                        </tbody>

                                                                                    </table>



                                                                                </div>

                                                                            </div>                                   

                                                                            <div class="clearfix"></div>

                                                                        </div>

                                                                        </br>

                                                                    </div>

                                                                    <?php
                                                                endforeach;
                                                                ?>	

                                                            </div>
                                                            <?php
                                                        }

                                                        if (count($companycontact) == 0) {

                                                            echo '<div style="width:95%; text-align: center; margin: auto;" class="alert alert-danger"><strong> No result found !...</strong></div><br/>';
                                                        }
                                                        ?>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">

                                                        <div class="panel-body">

                                                            <div class="table-responsive">

                                                                <table  width="100%" id="table-view-Data" class="table-bordered table-hover table-striped contact2">

                                                                    <thead>

                                                                        <tr  style="background-color:#eee;">

                                                                            <th>Contact</th>

                                                                            <th>Title</th>

                                                                            <th>Address</th>

                                                                            <th>Phone</th>

                                                                            <th>Fax</th>

                                                                            <th>E-mail</th>

                                                                            <th>Web site</th>

                                                                            <th>To Be Printed</th>

                                                                        </tr>

                                                                    </thead>

                                                                    <tbody>

                                                                        <?php
                                                                        foreach ($companycontact as $contact):
                                                                            ?>

                                                                            <tr>

                                                                                <td><?php echo $contact['cfname'] . " " . $contact['clname']; ?></td>

                                                                                <td><?php echo $contact['ctitle']; ?></td>

                                                                                <td><?php echo $contact['caddress1']; ?></br>

                                                                                    <?php
                                                                                    if ($contact['caddress2'] != "") {

                                                                                        echo $contact['caddress2'];
                                                                                    }
                                                                                    ?>

                                                                                    <?php echo $contact['ccity'] . ', ' . $contact['cstate'] . ' ' . $contact['czip'] . ' ' . $contact['ccountry'] ?>

                                                                                </td>

                                                                                <td><?php echo $contact['cphone']; ?></td>

                                                                                <td><?php echo $contact['cfax']; ?></td>

                                                                                <td><a href="mailto:<?php echo $contact['cemail']; ?>"><?php echo $contact['cemail']; ?></a></td>

                                                                                <td><a href="<?php echo $this->common->weblink($contact['cwebsite']); ?>" target="_new"><?php echo $contact['cwebsite']; ?></a></td>

                                                                                <td><?php echo $contact['printed']; ?></td>

                                                                            </tr>

                                                                            <?php
                                                                        endforeach;

//                                                                        }
                                                                        ?>

                                                                    </tbody>

                                                                </table>





                                                            </div>

                                                        </div>



                                                    </div>





                                                </div>
                                                <?php
                                                // } 
//if (count($companycontact) == 0) {
//
////                                        echo '<br /><div  style="width:95%;" class="alert alert-danger"><strong>No result found.</strong></div>';
//                                        echo '<br /><div class="col sm-9 alert alert-danger"><strong>No result found.</strong></div>';
//                                    }
                                                ?>
                                            </div>

                                        </div>

                                    </div>







                                    <!--company infostart here  -->



                                    <div class="panel panel-primary">

                                        <div class="panel-heading" role="tab" id="Company_Info">

                                            <h4 class="panel-title">

                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

                                                    Company Info

                                                </a>

                                            </h4>

                                        </div>



                                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="Company_Info">



                                            <div class="panel-body">
                                                <?php if ($clinfo[0]['companyinfo'] != "") { ?>


                                                    <!--<p align="justify"><?php // echo strip_quotes($companycontact[0]['companyinfo']); ?></p>-->
                                                    <p align="justify"><?php echo strip_quotes($clinfo[0]['companyinfo']); ?></p>
                                                    <?php
                                                } else {
                                                    echo '<div style="width:95%; text-align: center; margin: auto;" class="alert alert-danger"><strong> No result found !...</strong></div>';
                                                }
                                                ?>
                                            </div>

                                        </div>



                                    </div>





                                    <!-- Location start here -->



                                    <!--Location infostart here  -->

                                    <?php
                                    if (count($clinfo) > 0) {
                                        ?>

                                        <div class="panel panel-primary">

                                            <div class="panel-heading" role="tab" id="company_loc">

                                                <h4 class="panel-title">

                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">

                                                        Location

                                                    </a>

                                                </h4>

                                            </div>

                                            <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="company_loc">



                                                <div class="panel-body">







                                                    <div class="table-responsive col-md-12">

                                                        <table class="table-bordered table-hover table-striped datatable" id="sample-table-2" width="100%" >

                                                            <thead>

                                                                <tr>

                                                                    <th>Company name</th>

                                                                </tr>

                                                            </thead>			

                                                            <tbody>

                                                                <?php
                                                                //foreach($clinfo as $ci):
                                                                //if($ci['acity']!='' && $ci['astate']!='' && $ci['aname']!='' ){
                                                                //echo '<tr><td>';
                                                                //echo $ci['acity'].",".$ci['astate']."(".$ci['aname'].")";	
                                                                //echo "</td></tr>";	
                                                                //}
                                                                //endforeach;
                                                                //?>

                                                            </tbody>

                                                        </table>

                                                    </div>

                                                    <?php
//                                                }
                                                    ?>	



                                                </div>

                                            </div>

                                        </div>



                                    <?php } ?>

                                    <!--</div>-->

                                </div>		

                            </div> 

                        <?php } ?>

                    </div>

                </div>

            </section>

        </section>

        <input type="hidden" id="urlid" value="<?php echo $_GET['cid']; ?>">

        <?php echo $this->load->view($footer); ?>

    </div>



<!--    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>   

    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>-->

    <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <!--<script src="<?php echo base_url('assets'); ?>/js/jquery.dataTables.bootstrap.js"></script>-->

    <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>

    <script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.3.full.min.js"></script>

    <script src="http://cdn.datatables.net/plug-ins/1.10.7/type-detection/formatted-num.js"></script>  

    <script src="<?php echo base_url('assets'); ?>/js/typeahead-bs2.min.js"></script>

    <script type="text/javascript">

                                    function valid()

                                    {

                                        var company_id = $("#CompanyName").val();

                                        if (company_id == "")

                                        {

                                            $(".error").removeClass('hide', 1000);

                                            return false;

                                        } else

                                        {

                                            $(".error").addClass("hide", 1000);

                                        }

                                        return true;

                                    }

                                    /*$(document).ready(function()
                                     
                                     {
                                     
                                     $('#cllc').dataTable({ 
                                     
                                     "scrollY": "350px",
                                     
                                     "paging":   true
                                     
                                     
                                     
                                     });
                                     
                                     });*/

                                    $(document).ready(function (e) {

                                        //var datastring='id='+$("#urlid").val();

                                        $('#sample-table-2').dataTable({
                                            responsive: true,
                                            "autoWidth": false,
                                            "processing": true,
                                            "serverSide": true,
                                            "pagingType": "simple_numbers",
                                            "ajax": {
                                                url: "<?php echo site_url('ajax/location_pagi'); ?>",
                                                type: "POST",
                                                // "cache":"false",		        

                                                data: {id: $("#urlid").val()}

                                            },
                                            "dom": 'T<"clear">lfrtip',
                                            "oTableTools": {
                                                "aButtons": [
                                                    "print",
                                                    {
                                                        "sExtends": "collection",
                                                        "sButtonText": "Save",
                                                        "aButtons": ["copy", "csv", "xls", "pdf"]
                                                    }
                                                ],
                                                "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"
                                            },
                                            "oLanguage": {
                                                "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                                            }
                                        });

                                        var dtable = $(".datatable").dataTable().api();

                                        $(".dataTables_filter input")

                                                .unbind()

                                                .bind("input", function (e) {

                                                    if (this.value.length >= 3 || e.keyCode == 13) {



                                                        dtable.search(this.value).draw();

                                                    }



                                                    if (this.value == "") {

                                                        dtable.search("").draw();

                                                    }

                                                    return;

                                                });



                                        $('.contact2').DataTable({
                                            "lengthMenu": [5, 10, 15, 20, 25],
                                            "dom": 'T<"clear">lfrtip',
                                            "oTableTools": {
                                                "aButtons": [
                                                    "print",
                                                    {
                                                        "sExtends": "collection",
                                                        "sButtonText": "Save",
                                                        "aButtons": ["copy", "csv", "xls", "pdf"]
                                                    }
                                                ],
                                                "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"
                                            },
                                            "oLanguage": {
                                                "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                                            }

                                        });
                                        $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
                                            console.log(message);
                                        };
                                    });

                                    $(function () {

                                        function split(val) {

                                            return val.split(/,\s*/);

                                        }

                                        function extractLast(term) {

                                            return split(term).pop();

                                        }

                                        var s = $("#search").val();

                                        data: "search=" + s,
                                                $("#search")

                                                // don't navigate away from the field on tab when selecting an item

                                                .bind("keydown", function (event) {

                                                    if (event.keyCode === $.ui.keyCode.TAB &&
                                                            $(this).autocomplete("instance").menu.active) {

                                                        event.preventDefault();

                                                    }

                                                })

                                                .autocomplete({
                                                    source: function (request, response) {

                                                        $.getJSON("<?php echo site_url('companysearch'); ?>", {
                                                            term: extractLast(request.term)

                                                        }, response);

                                                    },
                                                    search: function () {

                                                        // custom minLength

                                                        var term = extractLast(this.value);


                                                        if (term.length < 2) {
                                                            
                                                            $("#search").removeClass('loading');
                                                            
                                                            return false;

                                                        }

                                                        $("#search").addClass('loading');

                                                    },
                                                    focus: function () {

                                                        // prevent value inserted on focus

                                                        $("#search").removeClass('loading');
                                                        
                                                        return false;

                                                    },
                                                    select: function (event, ui) {

                                                        var terms = split(this.value);

                                                        // remove the current input

                                                        terms.pop();

                                                        // add the selected item

                                                        terms.push(ui.item.value);

                                                        // add placeholder to get the comma-and-space at the end

                                                        terms.push("");

                                                        this.value = terms.join("");
                                                        
                                                        $("#search").removeClass('loading');
                                                        
                                                        return false;

                                                    }

                                                });

                                    });

    </script>





</body>

</html>