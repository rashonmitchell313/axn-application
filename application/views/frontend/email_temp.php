<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login</title>
    </head>

    <body style="margin:0px;">


        <table width="600" cellpadding="0" cellspacing="0" style="width:600px;" align="center">
            <tr>
                <td>
                    <table  align="center" cellspacing="0" cellpadding="0" style="background-color:#E60000; width:100%;">
                        <tr>
                            <td height="15" colspan="3"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="90%">
                                <table align="center" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td width="30%">
                                            <a href="#"><img title="logo" src="http://www.arnfactbook.com/2016/fassests/images/logo.png" style="display:block; width:90%;"></a>
                                        </td>
                                        <td align="right" width="70%">
                                            <img src="http://www.arnfactbook.com/2016/fassests/images/banar_icon.png" style="width:90%;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="5%"></td>
                        </tr>
                        <tr>
                            <td height="15" colspan="3"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="15"></td>
            </tr>
            <tr>
                <td style="height:20px;" class="3">
                    <table style="width:100%; margin:auto;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td> 

                                    <?php echo $message; ?>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="15"></td>
            </tr>
            <tr>
                <td colspan="3" style="background:url(http://www.arnfactbook.com/2016/fassests/images/footer-background-image_e.png); height:62px; background-size:100% auto; background-position:top center;">
                </td>
            </tr>
            <tr>
                <td style="background-color:#ADB6B9;" colspan="3">
                    <table align="center" cellspacing="0" cellpadding="0" width="100%" >
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td style="vertical-align:top;" width="45%">
                                <table cellspacing="0" cellpadding="0" width="98%">
                                    <tr><td style="height:40px;" height="40"></td></tr>
                                    <tr>
                                        <td><h2 style="color:#5c6a6e; font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bolder; margin:0px;">CONTACT US</h2></td>
                                    </tr>
                                    <tr><td style="height:20px;"></td></tr>
                                    <tr>
                                        <td><p style="color:#242f32; margin:0; line-height:15px; font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal;">You can Get in touch with us by <a style="text-transform:uppercase; text-decoration:none; color:#e60000;" href="https://www.airportrevenuenews.com/contact-us">email</a>. Alternatively you can give us a call or fax us any time:</p>
                                        </td>
                                    </tr>
                                    <tr><td style="height:15px;"></td></tr>
                                    <tr>
                                        <td><p style="color:#242f32; font-size:11px; font-family:Arial, Helvetica, sans-serif; line-height:15px; margin:0">Tel: 561-257-1024</p></td>
                                    </tr>
                                    <tr><td style="height:2px;"></td></tr>
                                    <tr>
                                        <td> <p style="color:#242f32; font-size:11px; font-family:Arial, Helvetica, sans-serif; line-height:15px; margin:0">Fax: 561.228.0882</p></td>
                                    </tr>
                                    <tr><td style="height:15px;"></td></tr>
                                    <tr>
                                        <td><p style="color:#242f32; font-size:11px; font-family:Arial, Helvetica, sans-serif; line-height:15px; margin:0"> 3200 North Military Trail, Suite 110 Boca Raton, FL 33431</p></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="vertical-align:top;" width="45%">
                                <table cellspacing="0" cellpadding="0" width="98%">
                                    <tr><td style="height:40px;" height="40"></td></tr>
                                    <tr>
                                        <td><h2 style="color:#5c6a6e; font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bolder; margin:0px;">GLOSSARY</h2></td>
                                    </tr>
                                    <tr><td style="height:20px;"></td></tr>
                                    <tr>
                                        <td ><p style="color:#242f32; font-size:11px; font-family:Arial, Helvetica, sans-serif; line-height:15px; margin:0">Airport Listings: Each airport included in ARN’s OFB 2014 has entered their information directly into this database. This year more than 90 airports submitted their concessions data to ARN for the purposes of this service.</p></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="5%">&nbsp;</td>
                        </tr>
                        <tr><td colspan="3" style="height:40px;" height="40"></td></tr>
                    </table>
                </td>
            </tr>
        </table>



    </body>

</html>