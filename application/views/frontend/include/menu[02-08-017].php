<header class="header">

    <section class="container">

        <div class="header-upper-sect">
            <?php
            if (in_array($this->session->userdata('accesslevel'), array(0, 1, 2, 4)) && $this->session->userdata('user_id') > 0) {
                ?>

                <div class="search-area">

                    <form method="post" >

                        <input id="searchlinks" class="form-control ui-autocomplete-input addon" type="text" value="" placeholder="Search" autocomplete="off"/>

                        <ul id="mySelect" class="slinks" name="mySelect"></ul>

                        <input value="" type="submit">

                    </form>

                </div>

                <?php
            }
            ?>


            <div class="clearfix"></div>

        </div>

        <div class="header-second-sect">

            <div class="logo col-md-3 col-sm-3 col-xs-12 padding-right-none  padding-left-none ">

                <h1>

                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('fassests'); ?>/images/logo.png" title="logo" /></a>

                </h1>

            </div>

            <div class="register-sect col-md-9 col-sm-8  col-xs-12 padding-right-none padding-left-none pull-right">

                        <!--<a href="#"><img src="<?php // echo base_url('fassests');                              ?>/images/register.jpg" /></a>-->
                <a href="javascript:void(0)"><img src="<?php echo base_url('fassests'); ?>/images/banar_icon.png" /></a>

            </div>

            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>

        <div class="header-lower-sect">                

            <button class="navbar-toggle collapsed pull-left" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">

                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

            </button>



            <div class="clearfix"></div>

        </div>





        <div class="clearfix"></div>

    </section> 

    <nav class="nav-sect navbar-collapse bs-navbar-collapse collapse">

        <ul class="nav navbar-nav">

            <li><a href="<?php echo site_url('frontend'); ?>">HOME</a></li>

            <?php
            if (in_array($this->session->userdata('accesslevel'), array(0, 1, 2, 4)) && $this->session->userdata('user_id') > 0) {

                $test = explode(",", $this->session->userdata('user_access'));
                ?>

                <li><a  href="javascript:void(0)">About Us</a>
                    <ul>

                        <li><a href="<?php echo site_url('glossary'); ?>">GLOSSARY</a></li>

                        <li><a href="<?php echo site_url('contact-us'); ?>">CONTACT US</a></li>

                        <li><a href="<?php echo site_url('faq-view'); ?>">FAQS</a></li>

                    </ul>
                </li>

                <?php
//                if (in_array($this->session->userdata('user_access'), array("Airports"))) {
                for ($p = 0; $p < count($test); $p++) {
                    if ("airports" == strtolower(str_replace(" ", "", $test[$p]))) {
                        ?>

                        <li><a href="<?php echo site_url('airportsearch'); ?>">AIRPORTS</a></li>

                        <?php
                    }
                }
                ?>

                <li><a href="<?php echo site_url('lease_expiration'); ?>">LEASE EXP</a></li>

                                                                                                                                                    <!--<li><a href="<?php // echo site_url('contacts');                              ?>">CONTACTS</a></li>-->

                <?php
                for ($p = 0; $p < count($test); $p++) {
                    if ("contacts" == strtolower(str_replace(" ", "", $test[$p]))) {
                        ?>
                        <li><a href="<?php echo site_url('contacts/company/a'); ?>">CONTACTS</a></li>

                        <?php
                    }
                }
                for ($p = 0; $p < count($test); $p++) {
                    if ("companies" == strtolower(str_replace(" ", "", $test[$p]))) {
                        ?>
                        <li><a href="<?php echo site_url('companysearch'); ?>">COMPANIES</a></li>
                        <?php
                    }
                }

                for ($p = 0; $p < count($test); $p++) {
                    if ("tenants" == strtolower(str_replace(" ", "", $test[$p]))) {
                        ?>

                        <li><a href="<?php echo site_url('tenants'); ?>">TENANTS</a></li>

                        <?php
                    }
                }
                for ($p = 0; $p < count($test); $p++) {
                    if ("reports" == strtolower(str_replace(" ", "", $test[$p]))) {
                        ?>

                        <li><a href="<?php echo site_url('reports'); ?>">REPORTS</a></li>

                        <?php
                    }
                }
                ?>




            <?php }
            ?>

            <?php
            if ($this->session->userdata('accesslevel') == 4 && $this->session->userdata('user_id') > 0) {
                ?>

                <li><a href="<?php echo site_url('affiliations'); ?>">Affiliations</a></li>

                <?php
                for ($p = 0; $p < count($test); $p++) {
                    if ("modifydata" == strtolower(str_replace(" ", "", $test[$p]))) {
                        ?>

                        <li><a href="<?php echo site_url('wizard/airport'); ?>">Modify Data</a></li>

                        <?php
                    }
                }
                ?>

                <li><a href="<?php echo site_url('admin'); ?>" target="_blank">Admin</a></li>

                <?php
            }

            if (in_array($this->session->userdata('accesslevel'), array(0, 2)) && $this->session->userdata('user_id') > 0) {

                for ($p = 0; $p < count($test); $p++) {
                    if ("modifydata" == strtolower(str_replace(" ", "", $test[$p]))) {
                        ?>

                        <li><a href="<?php echo site_url('wizard/airport'); ?>">Modify Data</a></li>

                        <?php
                    }
                }
            }
            ?>

            <?php if ($this->session->userdata('user_id')) { ?>

                <li><a href="<?php echo site_url('frontend/setting'); ?>">Setting</a></li>      

                <!--<li><a href="<?php // echo site_url('login/logout'); ?>">Log out</a></li>-->
                
                <li>
                    <!--<a href="<?php // echo site_url('login/logout'); ?>">Log out</a>-->
                    <form method="post" action="<?php echo site_url('login/logout'); ?>">
                        
                        <input type="hidden" name="last_url" value="<?php echo current_url(); ?>" > 
                        
                        <input type="submit" value="Log out" style="">
                        
                    </form>

                </li>
                
                 <?php if ($this->session->userdata('user_id') == '2452') { ?>
                
                <li><a href="<?php echo site_url('UserManual'); ?>">User Manual</a></li>                            

                 <?php } } else { ?>

                <li><a href="<?php echo site_url('login'); ?>">Login</a></li>                            

            <?php } ?>

            <div class="clearfix"></div>

        </ul>

    </nav> 

    <div class="clearfix"></div>

</header>