<?php
// $this->outputData['FooterContent'] = 

$FooterContent = $this->common->GetAllRowOrderBy("tbl_footer_content", "id", "asc");
?>

<a href="#" class="scrollup btn btn-danger"><i class="fa fa-arrow-up"></i> Go on Top</a>

<div id="forgot_password" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <table width="100%">
                    <tr>
                        <td width="15%">
                            <img src="<?php echo base_url('fassests') ?>/images/ticket.png" alt="create a ticket">              </td>
                        <td width="85%" align="left">                  
                            <h4  class="block-header">CREATE A TICKET</h4>
                            <p class="block-header-subtext">Let us know your issues</p>
                        </td>
                    </tr>
                </table>                         
            </div>

            <form method="post"  class="form-vertical" action="<?php echo site_url('frontend/support'); ?>">   

                <div class="modal-body">
                    <br />
                    <div class="form-group">
                        <p>Subject<p>
                            <input tabindex="1" type="text" id="subject" placeholder="Subject" required class="form-control" name="subject"/>
                            <input tabindex="1" type="hidden" id="email"  value="<?php echo $this->session->userdata('email'); ?>"/>
                    </div>
                    <div class="form-group">
                        <p>Message<p>
                            <textarea tabindex="1" id="message" placeholder="Type your Message" required class="form-control" name="message"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" id="submit" class="btn btn-primary pull-left test">Create</button>
                </div>
            </form>
            <div class="success">
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="alert_model" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><div class="alert alert-danger" role="alert"><?php echo $this->common->GetCurrentUserInfo('first_name'); ?>&nbsp;<?php echo $this->common->GetCurrentUserInfo('last_name'); ?> &nbsp;<small>Please Re-Enter Password due to security reasons.</small></div></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="<?php echo site_url('login'); ?>" method="post">
                        <!--                    <div class="row">-->

                        <div class="form-group col-sm-7">
                            <input type="hidden" class="form-control" name="return_url" value="<?php echo current_url(); ?>">
                            <!--<input type="hidden" class="form-control" name="last_url" value="1">-->
                            <input type="hidden" class="form-control" name="username" value="<?php echo $this->common->GetCurrentUserInfo('email'); ?>">
                            <input type="password" class="form-control" name="userpassword" placeholder="Password" id="pwd" required="required"  autocomplete="off">
<!--                            <script>
    if (!("autofocus" in document.createElement("input"))) {
      document.getElementById("pwd").focus();
    }
  </script>-->
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i>&nbsp; Login</button>&nbsp;&nbsp;&nbsp;&nbsp;


                        </div>
                        <!--</div>-->
                    </form>

                    <form method="post" action="<?php echo site_url('login/logout'); ?>">
                        <input type="hidden" name="last_url" value="<?php echo current_url(); ?>" >
                        <!--<a href="<?php // echo site_url('login/logout');   ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i>&nbsp; Logout</a>-->
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-danger"><i class="fa fa-sign-out"></i>&nbsp; Logout</button>
                        </div>
                    </form>
                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="clearfix"></div>
<footer class="footer">
    <section class="container">
        <section class="footer-sect1 footer-sect col-md-4 col-sm-4 col-xs-10">
            <h2>SITE MAP</h2>
            <div class="footer-text-widget">
<?php if ($this->session->userdata('user_id')) { ?>
                    <ul>
                        <li><a href="<?php echo site_url('frontend'); ?>">HOME</a></li>

                        <?php
                        $test = explode(",", $this->session->userdata('user_access'));

                        for ($p = 0; $p < count($test); $p++) {
                            if ("airports" == strtolower(str_replace(" ", "", $test[$p]))) {
                                ?>
                                <li><a href="<?php echo site_url('airportsearch'); ?>">AIRPORTS</a></li>
                                <?php
                            }
                        }
                        ?>
                        <li><a href="<?php echo site_url('lease_expiration'); ?>">LEASE EXP</a></li>
                        <?php
                        for ($p = 0; $p < count($test); $p++) {
                            if ("contacts" == strtolower(str_replace(" ", "", $test[$p]))) {
                                ?>
                                <li><a href="<?php echo site_url('contacts'); ?>">CONTACTS</a></li>
                                <?php
                            }
                        }
                        for ($p = 0; $p < count($test); $p++) {
                            if ("companies" == strtolower(str_replace(" ", "", $test[$p]))) {
                                ?>
                                <li><a href="<?php echo site_url('companysearch'); ?>">COMPANIES</a></li>
                                <?php
                            }
                        }
                        for ($p = 0; $p < count($test); $p++) {
                            if ("tenants" == strtolower(str_replace(" ", "", $test[$p]))) {
                                ?>
                                <li><a href="<?php echo site_url('tenants'); ?>">TENANTS</a></li>
                                <?php
                            }
                        }
                        for ($p = 0; $p < count($test); $p++) {
                            if ("reports" == strtolower(str_replace(" ", "", $test[$p]))) {
                                ?>
                                <li><a href="<?php echo site_url('reports'); ?>">REPORTS</a></li>
                                <?php
                            }
                        }
                        ?>
                        <li><a href="<?php echo site_url('glossary'); ?>">GLOSSARY</a></li>
                        <li><a href="<?php echo site_url('contact-us'); ?>">CONTACT US</a></li>
                        <li><a href="#forgot_password" data-toggle="modal" class="text-primary test">SUPPORT</a></li>
                    </ul>
                    <?php
                } else {
                    ?>
                    <span>Please <a href="<?php echo site_url(''); ?>">Login</a> first.</span>
                    <?php
                }
                ?>
                <div class="join-email">
                    <a href="https://www.airportrevenuenews.com/email-newsflash-signup/" target="_blank">Join Our Email List</a>
                </div>
            </div>
        </section>
        <section class="footer-sect2 footer-sect col-md-4 col-sm-4 col-xs-10">
            <h2>CONTACT US</h2>
            <div class="footer-text-widget">
                <p class="email-contact" ><?php
                    if ($this->session->userdata('user_id')) {
                        echo site_url('contact-us');

                        $mail = "<a href='" . site_url('contact-us') . "'>email</a>";
                    } else {
                        $mail = '<a href="http://www.airportrevenuenews.com/contact-us">email</a>';
                    }
                    $contactUs = str_replace("#EMAIL", $mail, $FooterContent[0]['content']);

//                echo $FooterContent[0]['content']; 
                    echo $contactUs;
                    ?></p>


<!--                <p class="email-contact" >You can Get in touch with us by <a href="<?php
//                    if ($this->session->userdata('user_id')) {
//                        echo site_url('contact-us');
//                    } else {
//                        echo 'https://www.airportrevenuenews.com/contact-us';
//                    }
                ?>">email</a>. Alternatively you can give us a call or fax us any time:</p>
<address>
    <span>Tel: 561.477.3417</span>
    <span>Tel: 561-257-1024</span>
    <span>Fax: 561.228.0882</span>
</address>
<p class="email-contact">3200 North Military Trail, Suite 110 Boca Raton, FL 33431</p>-->
            </div>
        </section>
        <section class="footer-sect3 footer-sect col-md-4 col-sm-4 col-xs-10">
            <h2>GLOSSARY</h2>
            <div class="footer-text-widget">
                <!--<p>Airport Listings: Each airport included in ARN’s OFB 2014 has entered their information directly into this database. This year more than 90 airports submitted their concessions data to ARN for the purposes of this service.</p>-->
                <p><?php echo $FooterContent[1]['content']; ?></p>
                <!--<p><?php // echo $this->common->GetSessionKey('glossary')   ?></p>-->
                <ul class="bill-authorize">
                    <li><a href="https://www.bill.com" target="_blank"><img src="<?php echo base_url('fassests'); ?>/images/bill.png" alt="bill" /></a></li>
                    <li><a href="https://www.authorize.net" target="_blank"><img src="<?php echo base_url('fassests'); ?>/images/autorize.png" alt="authorize3" /> </a></li>
                </ul>
                <div class="paymentmthdz"><p>We accept the following credit cards:</p><p><img width="183" height="29" alt="" src="<?php echo base_url('fassests'); ?>/images/billing-opt.png"></p>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
</footer>
<div class="clearfix"></div>

<script src="<?php echo base_url('fassests'); ?>/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js" type="text/javascript"></script>



<!--for datatable buttons--> 

<!--<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script> 

<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script> 

<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>

<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>

<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>-->




<script type="text/javascript">

    var t;
    var timeout;
    $(document).ready(function () {

        timeout = "<?php echo $this->common->GetSessionKey('login_time_out') ?>";

        if ("<?php echo $this->common->GetSessionKey('user_status') ?>" == "away") {
//            alert(timeout);

            away();
        }
        $(window).scroll(function () {
            if ($(this).scrollTop() > 300) {
                $('.scrollup').fadeIn('slow');
            } else {
                $('.scrollup').fadeOut('slow');
            }
        });

        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 300);
            return false;
        });


        /*$('.test').click (function(){
         var check=$("#email").val();
         //if(check == ""){
         
         alert(check);
         //return false;
         //}
         
         });*/

        var userid = "<?php echo $this->common->GetSessionKey('user_id') ?>";
        if (userid > '0')
        {
            window.onload = resetTimer;
            window.onmousemove = resetTimer;
            window.onmousedown = resetTimer; // catches touchscreen presses
            window.onclick = resetTimer;     // catches touchpad clicks
            window.onscroll = resetTimer;    // catches scrolling with arrow keys
            window.onkeypress = resetTimer;
        }
        $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
            console.log(message);
        };


    });


    $('#searchlinks').bind('input', function () {
        var link = $(this).val(); // get the current value of the input field.

        if (link == '') {
            var select = document.getElementById('mySelect');

            $(select).html('');
        }

        $.ajax({
            dataType: "json",
            url: "<?php echo site_url('ajax/search_links'); ?>",
            type: "POST",
            cache: false,
            data: "slink=" + link,
            success: function (response) {

                var select = document.getElementById('mySelect');

                $(select).html('');

                for (var i in response) {
//        $(select).append('<option value=' + response[i] + '>' + response[i] + '</option>');
//        $(select).append('<option >' + response[i] + '</option>');
                    $(select).append('<li >' + response[i] + '</li>');

                }

            }
        });

    });


//  onload = function () {
//      alert('onload');
//       var e = document.getElementById('searchlinks');
//       e.oninput = myHandler;
//       e.onpropertychange = e.oninput; // for IE8
//       // e.onchange = e.oninput; // FF needs this in <select><option>...
//       // other things for onload()
//    };


    function away() {
//        $('#alert_model').modal('show');
        $.ajax({
            url: '<?php echo site_url('login/user_away') ?>',
            type: 'GET',
            success: function (results) {
                if (results == 1) {

                    $('#alert_model').modal({
                        //                  autoFocus : true,
                        //                        'show', 


                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#alert_model').on('shown.bs.modal', function () {
                        setTimeout(function () {
                            $('#pwd').focus();
                            $('#pwd').attr('autocomplete', 'off');
                        }, 1);
                    });
                }

            }
        });

    }

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(away, 60000 * timeout);  // time is in milliseconds
    }



</script>        