<?php

$userinfo = $this->session->all_userdata();

if ($this->common->GetCurrentUserInfo('user_id') == "") {


    ?>



    <div class="login">

        <form action="<?php echo site_url('login'); ?>" method="post">


                
            <div class="form-group">

                <label for="usr">Username:</label>

                <input type="text" class="form-control" name="username" placeholder="Email" required="required">

            </div>

            <div class="form-group">

                <label for="pwd">Password:</label>

                <input type="password" class="form-control" name="userpassword" placeholder="Password" id="pwd" required="required">

            </div>

            <div class="form-group">

                <input type="submit" name="Login" class="btn btn-success" value="login"> 

                <a href="javascript:void(0);" id="reset" onclick="show()"> <i class="fa fa-key fa-fw"> </i>Forget Password</a>        

            </div>

            <p class="error"></p>

            <div class="clearfix"></div>

        </form>	

    </div>



<?php } else { ?>

    <div class="panel panel-primary">        		

        <div class="panel-heading">

            <form method="post" action="<?php echo site_url('login/logout'); ?>">

                <strong>

                    <?php echo $this->common->GetCurrentUserInfo('first_name') ?>&nbsp;

                    <?php echo $this->common->GetCurrentUserInfo('last_name') ?></strong>&nbsp;:  



                <input type="hidden" name="last_url" value="<?php echo current_url(); ?>" > 



                <button type="submit" class="btnout123" value="" ><i class="fa fa-sign-out">LogOut</i></button>



            </form>

    <!--                <a   href="<?php echo site_url('login/logout'); ?>" style="color:white;">

        <i class="fa fa-sign-out"></i>LogOut</a>                -->

        </div>

    </div>

<?php } ?>



<div class="forget hide">

    <form method="post" id="form" action="" onSubmit="return sendpassword();">

        <div>		

            <label>Reset password</label>



            <div class="form-group">

                <label for="email"> Email</label>

                <input class="form-control" type="text" id="email" name="email" />

            </div>

            <div class="form-group">

                <button type="submit" class="btn btn-primary"><i class="fa fa-key fa-fw"></i> &nbsp;&nbsp;Reset Password</button>

            </div>

            <p class="error"></p>

            <?php if (isset($info)): ?>

                <div class="alert alert-success">

                    <?php echo($info) ?>

                </div>

            <?php elseif (isset($error)): ?>

                <div class="alert alert-error">

                    <?php echo($error) ?>

                </div>

            <?php endif; ?>



        </div>

    </form>

</div>

