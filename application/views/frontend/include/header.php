<?php
$this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');

$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');

$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);

$this->output->set_header('Pragma: no-cache');
?>

<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1">
            
<!--            <meta http-equiv="X-UA-Compatible" content="IE=edge, IE=EmulateIE8, IE=8, IE=EmulateIE9, IE=9"></meta>-->
            <meta http-equiv="X-UA-Compatible" content="IE=8" />

            <link rel="shortcut icon" href="<?php echo base_url('assets/img'); ?>/favicon4.ico" type="image/x-icon" />

            <link href="<?php echo base_url('fassests'); ?>/css/style.css" rel="stylesheet" type="text/css" />

            <link href="<?php echo base_url('fassests'); ?>/css/bootstrap.css" rel="stylesheet" type="text/css" />

            <link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests/font-awesome-4.3.0/css'); ?>/font-awesome.css">

                <link href="<?php echo base_url('fassests'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />

                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/jquery-ui-1.10.3.full.min.css" />

                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/jquery-ui.css">


                    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">-->

                    <!--[if lt IE 9]>
                      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
                      <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
                    <![endif]-->


                    <style>

                        .slinks{
                            list-style-type:none;
                            position: absolute;
                            z-index: 999999999999;
                            width: 176px;
                            background-color: #ffffff;
                            border-radius: 10px;
                        }

                        .slinks li {
                            background-color: #e60000;
                            margin: 1px;
                            border-radius: 12px;
                        }
                        
                        .slinks li a {
                            padding: 0 15px;
                            color: #ffffff;
                            display: inline-block;
                            font-family: "miso";
                            font-size: 17px;
                            text-decoration: none;
                            text-transform: uppercase;
                        }

                        .slinks li:hover {
                            background-color: #000000;
                            color: #000;
                            padding: 0 15px;
                        }

                    </style>