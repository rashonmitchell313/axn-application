<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Reset Password | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Reset Password | ARN Fact Book';
}
?>
</title>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>
        <section class="main-content">
        	<section class="container-fluid">            	
            	<div class="col-md-3 col-sm-12 col-sm-3 login-left-sect">
                	<?php echo $this->common->getmessage();?>
					
                   					 <div id="login_area" class="comments-area">
                     	<h4 class="title">Reset Password</h4>
						<form method="post" action="" onSubmit="return checkpasswords();">
								<div class="form-group">
								<label>Password</label>
								<span>*</span>
								<input type="password" class="form-control" name="pass" id="pass" value="">
								</div>
								<div class="form-group">
								<label>Confirm Password</label>
								<span>*</span>
								<input type="password" class="form-control" name="cpass" id="cpass" value="">
                                <input type="hidden" name="key" id="key" value="<?php echo $this->uri->segment(3); ?>">
							</div>
							 	
								<div class="form-group">
								<input type="submit" class="btn btn-primary" value="Update">                                
							</div>
						</form>
					</div>
					<p class="error"></p>
                    <div id="success_message" class="comments-area" style="display:none;">
                     	<div class="alert alert-success>"Password has been updated successfully.<br>
                     	Please click here to <a href="<?php echo site_url('login')?>">login</a></div>
					</div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 login-right-sect">
                	<div class="row">
                    	<div class="video-box">
                        	<!--<iframe width="640" height="360" src="//www.youtube.com/embed/jSdglW1utg8?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>-->                        	<div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="nav-book-info">
                        	<div class="col-md-4 col-xs-12 col-sm-4  book-img-login padding-left-none">
                            	<img src="<?php echo base_url('fassests');?>/images/book_pic.png" />
                            </div>
                            <div class="col-md-8 col-xs-12 col-sm-8 nav-book-list padding-right-none">
                            	<ul>
                                	<li>More than 100 participating North American airports</li>
                                    <li>More than $8 billion of total sales data</li>
                                    <li>Contact information for key concessions leaders</li>
                                    <li>Lease expirations through 2040</li>
                                    <li>Terminal-by-terminal tenant details</li>
                                    <li>And much more...</li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                    	<div class="text-bottom-login">
                        	<p>Welcome to the ARN Online Fact Book, the premier resource for revenue data on airports in North America. This online service, like its hard copy version, contains comprehensive data on the food and beverage, specialty retail, news and gifts, duty-free, advertising, parking and car rental concessions in major airports. Plus, it provides detailed information on all of the key companies doing business in airports. All data is based on previous calendar-year information.
</p>						
							<p>For those unfamiliar with the figures contained within the OFB, please review the glossary page to understand precisely the meaning of the various data points. Also, please navigate all of the menu options for a variety of valuable and useful reports, such as the top-performing airports by sales per enplanement; leases due to expire within the next 18 months; and a snapshot of important aspects of every airport. </p>
                            <p>Each of the participating airports included in the OFB have provided its information by manually entering it into ARN’s customized OFB program. Each participant has the ability to review the data before submitting it for publication; likewise, each concessions company has directly entered its own information. All participants in the OFB have been informed of the high priority placed on the accuracy of this information, and ARN is not responsible for any discrepancies that may occur.</p>
                            <p>ARN is grateful to all of the participants who took the time to enter their information, making this service the most valuable, one-of-a-kind resource in the airport revenue industry. </p>
                            <p>As always, ARN is interested to know what you think of the new OFB. Please give us a call at 561.477.3417; send a fax to 561.228.0882; or email us at <a href="mailto:info@airportrevenuenews.com">info@airportrevenuenews.com</a>.</p>
                            <p>The ARN Fact Book and Online Fact Book is published by Airport Revenue News, which also publishes the Airport Revenue News magazine and hosts the annual Airport Revenue Conference &amp; Exhibition. For more information, please call 561.477.3417 or fax your questions to 561.228.0882. Or, visit our Web site at <a href="www.airportrevenuenews.com.">www.airportrevenuenews.com</a>.</p>
                            <div class="padding-top-20 padding-bottom-10">
                                <p class="text-center titled-font-bottom">If you are not already a subscriber and are interested in gaining access,</p>
                                <p class="text-center titled-font-bottom">you can view subscription pricing and purchase access at</p>
                                <p class="text-center titled-font-bottom"><a href="http://www.airportrevenuenews.com/store/">www.airportrevenuenews.com/store/</a></p>
                            </div>
                        </div>
                    </div>
                </div>                
            </section>
        </section>
        <?php echo $this->load->view($footer);?>
    </div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script> 
        <script type="text/javascript">
			function checkpasswords()
			{
		
				var user_password		=	$("#pass").val();
				var user_cpassword	=	$("#cpass").val();						
				if(user_password=="" || user_cpassword=="")
				{
					$(".error").html('<div class="alert alert-danger">Invalid Password.</div>');
				}else if(user_cpassword.length<6)
				{
					$(".error").html('<div class="alert alert-danger">Password length must be at least 6.</div>');			
				} else if(user_password!=user_cpassword)
				{
					$(".error").html('<div class="alert alert-danger"> Confirm Password must be match with password.</div>');
				} else
				{
					$(".error").html('<i class="fa fa-spinner fa-spin fx"><i>');
					var key = $("#key").val();
					var datastring = "user_encrypt_key="+key+"&pass="+user_password+"&action=update_password";
					$.ajax({
							url:"<?php echo site_url('reset_password');?>",
							async:false,
							type:"POST",
							cache:false,
							data:datastring,
							success: function(html)
							{
								if(html!=1)
								{
									$(".error").empty();
									$(".error").html(html);
								} else 
								{
									$(".error").empty();
									$("#success_message").show("slow");
								}
							}
					});
					return false;
				}
				return false;
			}
		</script>
<script type="text/javascript">
	function show(){
	$(".login").addClass("hide");
	$(".forget").removeClass("hide");
	}
</script>
</body>
</html>
