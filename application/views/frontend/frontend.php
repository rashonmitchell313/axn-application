<?php echo $this->load->view($header); ?>

<title>

    <?php
    
    // if ($this->uri->segment(0) != '') {

    //     echo 'Welcome to ARN Fact Book | Login ' . $this->uri->segment(0);
    // } else {

        echo 'Welcome to ARN Fact Book | Login';
    // }
    ?>

</title>

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">

            <section class="container-fluid">             

                <div class="col-md-3 col-sm-12 col-sm-3 login-left-sect">

                    <?php
                    echo $this->common->getmessage();

                    if (isset($message)) {
                        ?>

                        <div style="font-size:18px" class="wrap"><?php echo $message; ?></div>

                        <?php
                    }
                    ?>

                    <?php echo $this->load->view('frontend/include/loginform'); ?>

                </div>

                <div class="col-md-9 col-sm-9 col-xs-12 login-right-sect ">

                    <div class="row">

                        <div class="video-box ">

                            <div class="col-md-1 col-sm-1 col-xs-1"></div>
                            <?php // if ($this->session->userdata('user_id') > 0) { ?>
                                <!--<iframe width="800" height="636" src="https://www.youtube.com/embed/jSdglW1utg8?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>-->
                            <?php // } ?>
                            <div class="clearfix"></div>

                        </div>

                    </div>

                    <br/>

                    <br/>                  

                    <div class="row">

                        <div class="nav-book-info">

                            <div class="col-md-6 col-xs-12 col-sm-6 nav-book-list padding-right-none">

                                <ul>

                                    <li>More than 100 participating North American airports</li>

                                    <li>More than $8 billion of total sales data</li>

                                    <li>Contact information for key concessions leaders</li>

                                </ul>

                            </div> 

                            <div class="col-md-1 col-xs-12 col-sm-1  book-img-login padding-left-none">

                            </div>

                            <div class="col-md-5 col-xs-12 col-sm-5 nav-book-list padding-right-none">

                                <ul>

                                    <li>Lease expirations through 2040</li>

                                    <li>Terminal-by-terminal tenant details</li>

                                    <li>And much more...</li>

                                </ul>

                            </div> 

                        </div>

                    </div>

                    <div class="row">

                        <div class="text-bottom-login">

                            <?php echo $content; ?>

                        </div>

                    </div>

                </div>                

            </section>

        </section>

        <?php echo $this->load->view($footer); ?>

    </div>

    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        

    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script> 

    <script type="text/javascript">

        function show() {

            $(".login").addClass("hide");

            $(".forget").removeClass("hide");

        }

    </script>

    <script type="text/javascript">

        function sendpassword()

        {

            var EmailID = $("#email").val();

            if (EmailID == "")

            {

                $(".error").html('<div class="alert alert-danger">Invalid Email Address.</div>');

                return false;

            } else

            {

                var datastring = "email=" + EmailID + "&action=send_password_rest_link";

                //  $(".error").html('<img src="<?php echo base_url('assets'); ?>/images/loading.gif">');

                $.ajax({
                    url: "<?php echo site_url('reset_password'); ?>",
                    async: false,
                    type: "POST",
                    cache: false,
                    data: datastring,
                    success: function (html)

                    {

                        if (html != 1)

                        {

                            $(".error").empty();

                            $(".error").html(html);

                        } else

                        {

                            $(".error").empty();

                            $(".error").html('<div class="alert alert-danger">An email has been sent to reset your password. Please read your email and set new password.</div>');

                        }

                    }

                });

                return false;

            }

        }

    </script>   

</body>

</html>

