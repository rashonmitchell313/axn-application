<?php echo $this->load->view($header); ?>

<title>

    <?php
//    if ($this->uri->segment(0) != '') {
//
//        echo 'Modify Data | ARN Fact Book ' . $this->uri->segment(0);
//    } else {
//
//        echo 'MODIFY DATA | ARN Fact Book';
//    }
    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables2.bootstrap.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/chosen.css">

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">

            <?php /* ?><section class="col-sm-2">

              <?php foreach($modify_airport_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>

                <div class="row">

                    <div class="col-sm-12">

                        <div class="tab-pane active" role="tabpanel">

                            <h1 class="default-title">You are editing data for: <?php echo $main_data[0]['IATA']; ?></h1>

                            <p class="default-text">Please review this screen carefully, as this is exactly how the information will be displayed upon approval.<br>You may click on the above links to modify any data needed.<br>You will find a link to Publish your information to the OFB at the bottom of this page when you are satisfied with your entered data.</p>

                            <h4><p class="text-default"><strong><?php echo $main_data[0]['aname'] . ' (' . $main_data[0]['IATA'] . ')'; ?></strong></p>

                                <p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-12" style="font-family:verdana;">
                        <div class="arport-btns-holder">

                            <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                <div class="btn-group" role="group">

                                    <?php $aid = $this->uri->segment(3); ?>

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $aid); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "1") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 1</u><br />CONTACTS 

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $aid); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "2") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 2</u><br /> AIRPORT INFO

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $aid); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "3") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 3</u><br /> TERMINALS

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $aid); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(5) == "4") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 4</u><br /> RENTAL & PARKING

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $aid); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "5") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 5</u><br /> TENANTS

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $aid); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "6") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 6</u><br /> LOCATION

                                    </a>                                

                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                </br

                ></br>

                <div class="row">
                    <div class="error_e col-sm-12">
                        <?php $this->common->getmessage(); ?>
                    </div>
                </div>

                </br>
                <?php // var_dump($airport_contact); exit; ?>
                <!--<div class="panel-group tabs-table" id="accordion" role="tablist" aria-multiselectable="true">-->
                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="contact" data-toggle="tooltip" title="Click on Text to Close / Open" >

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">

                                Existing Contact(s)

                            </a>

                        </h4>

                    </div>

                    <!--<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contact">-->

                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contact">

                        <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 

                            <ul id="myTabs" class="nav nav-tabs" role="tablist"> 

                                <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><i class="margin-right-5 fa fa-qrcode">&nbsp;</i>Grid View</a></li> 

                                <li role="presentation" class="" ><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="margin-right-5 fa fa-table">&nbsp;</i>Table View</a></li>

                            </ul> 

                            <br />

                            <div id="myTabContent" class="tab-content"> 

                                <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">





                                    <!--<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contact">-->

                                    <div class="pannel-body">

                                        <div class="table-responsive">
                                            <table class="table  table-bordered table-hover table-striped" id="table2" >

                                                <thead>

                                                    <?php $i = 2; ?>



                                                    <?php
                                                    foreach ($airport_contact as $airport_contacts) {
                                                        ?>

                                                        <?php
                                                        if ($i % 2 == 0) {

                                                            echo "<tr>";
                                                        }
                                                        ?>

                                                    <td valign="top">



                                                        <table class="table  table-bordered table-hover table-striped">



                                                            <tbody><tr>



                                                                    <td colspan="2" align="left" height="18"><h1 class="default-title"><?php echo $airport_contacts['mgtresponsibilityname']; ?></h1></td>



                                                                </tr>



                                                                <tr>



                                                                    <td colspan="2" align="left" height="18">

                                                                                                                                                                                                        <!--<a href="<?php // echo site_url('wizard/airport/airport_edit_step_1');                                  ?>/<?php // echo $aid;                                  ?>/<?php // echo $airport_contacts['acid'];                                  ?>"  class="btn btn-primary" > <i class="fa fa-edit"></i>&nbsp;Edit</a>-->

                                                                        <a href= "javascript:void(0);" onClick="editme('<?php echo $airport_contacts['acid']; ?>')" class="btn btn-primary" > <i class="fa fa-edit"></i>&nbsp;Edit</a>

                                                                        <a href="javascript:void(0);" onClick="delme('<?php echo $airport_contacts['acid']; ?>')"  class="btn btn-danger"> 

                                                                            <i class="fa fa-trash"></i>&nbsp;Delete</a>	

                                                                    </td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Contact</td>



                                                                    <td align="left"><?php echo $airport_contacts['afname'] . " " . $airport_contacts['alname']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Title</td>



                                                                    <td align="left"><?php echo $airport_contacts['atitle']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Company</td>



                                                                    <td align="left"><?php echo $airport_contacts['acompany']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Address</td>



                                                                    <td align="left"><?php echo $airport_contacts['aaddress1'] . ' ' . $airport_contacts['aaddress2']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left"></td>



                                                                    <td align="left"><?php echo $airport_contacts['accity'] . "," . $airport_contacts['acstate'] . " " . $airport_contacts['aczip'] . " " . $airport_contacts['accountry']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Phone</td>



                                                                    <td align="left"><?php echo $airport_contacts['aphone']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Fax</td>



                                                                    <td align="left"><?php echo $airport_contacts['afax']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">E-mail</td>



                                                                    <td align="left"><a href="mailto:<?php echo $airport_contacts['aemail']; ?>" class="BodyLink"><?php echo $airport_contacts['aemail']; ?></a></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Web site</td>



                                                                    <td align="left"><a href="<?php echo $this->common->weblink($airport_contacts['awebsite']); ?>" target="new"><?php echo $airport_contacts['awebsite']; ?></a></td>



                                                                </tr>



                                                            </tbody></table>



                                                    </td>

                                                    <?php
                                                    if ($i % 2 != 0) {

                                                        echo "</tr>";
                                                    }

                                                    $i = $i + 1;
                                                    ?>

                                                <?php } ?>



                                                <tr></tr>

                                                </thead>

                                            </table>
                                        </div>
                                    </div>


                                    <?php // echo 'test1'; exit; ?>
                                    <!--</div>-->

                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">

                                    <div class="panel-body">

                                        <div class="table-responsive">

                                            <table  width="100%" id="table-view-Data" class="table-bordered table-hover table-striped contact2">

                                                <thead>

                                                    <tr  style="background-color:#eee;">                                                        

                                                        <th>Category</th>

                                                        <th>Contact</th>

                                                        <th>Title</th>

                                                        <th>Company</th>

                                                        <th>Address</th>

                                                        <th>Phone</th>

                                                        <th>Fax</th>

                                                        <th>E-mail</th>

                                                        <th>Web site</th>

                                                        <th>Action</th>

                                                    </tr>

                                                </thead>

                                                <tbody>



                                                    <?php $i = 2; ?>

                                                    <?php
                                                    foreach ($airport_contact as $airport_contact1):
                                                        ?>



                                                        <tr>                                                            

                                                            <td><?php echo $airport_contact1['mgtresponsibilityname']; ?></td>

                                                            <td><?php echo $airport_contact1['afname'] . " " . $airport_contact1['alname']; ?></td>

                                                            <td><?php echo $airport_contact1['atitle']; ?></td>

                                                            <td><?php echo $airport_contact1['acompany']; ?></td>

                                                            <td><?php echo $airport_contact1['aaddress1'] . ' ' . $airport_contact1['aaddress2'] . "," . $airport_contact1['accity'] . "," . $airport_contact1['acstate'] . " " . $airport_contact1['aczip'] . " " . $airport_contact1['accountry']; ?></td>

                                                            <td><?php echo $airport_contact1['aphone']; ?></td>

                                                            <td><?php echo $airport_contact1['afax']; ?></td>

                                                            <td><a href="mailto:<?php echo $airport_contact1['aemail']; ?>" class="BodyLink"><?php echo $airport_contact1['aemail']; ?></a></td>

                                                            <td><a href="<?php echo $this->common->weblink($airport_contact1['awebsite']); ?>" target="new"><?php echo $airport_contact1['awebsite']; ?></a></td>

                                                            <td>

                                                                                                                                                                                                    <!--<a href="<?php // echo site_url('wizard/airport/airport_edit_step_1');                                  ?>/<?php echo $aid; ?>/<?php // echo $airport_contact1['acid'];                                  ?>"  class="btn btn-primary btn-xs" > <i class="fa fa-edit"></i>&nbsp;Edit</a>-->

                                                                <a href= "javascript:void(0);" onClick="editme('<?php echo $airport_contacts['acid']; ?>')" class="btn btn-primary btn-xs" > <i class="fa fa-edit"></i>&nbsp;Edit</a>

                                                                <a href="javascript:void(0);" onClick="delme('<?php echo $airport_contact1['acid']; ?>')"  class="btn btn-danger btn-xs"> 

                                                                    <i class="fa fa-trash"></i>&nbsp;Delete</a>	

                                                            </td>

                                                        </tr>



                                                    <?php endforeach; ?>

                                                </tbody>

                                            </table>





                                        </div>

                                    </div>

                                </div>



                            </div>

                        </div>

                    </div>

                </div>





                <!--<br>-->

                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="airportwide" data-toggle="tooltip" title="Click on Text to Close / Open">

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

                                Airportwide Information

                            </a>

                        </h4>

                    </div>

                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="airportwide">						

                        <div class="pannel-body">
                            <div class="table-responsive">
                                <table class="table  table-bordered table-hover table-striped" >

                                    <tbody>

                                        <tr>

                                            <td valign="top">

                                                <table class="table  table-bordered table-hover table-striped" ><tbody>

                                                        <tr>

                                                            <th colspan="2" align="left"><h1 class="default-title">Airport Info</h1></th>

                                        </tr>



                                        <tr>

                                        </tr>



                                        <tr>



                                            <td align="left" valign="top" width="200">Airport Configuration 
                                                <i class="fa fa-question-circle" data-original-title="Describes the shape of your airport. For example, T-shaped, U-shaped, Fingers, etc." 
                                                   data-container="body"data-toggle="tooltip"></i>
                                            </td>



                                            <td width="200"><?php echo $wideInfo_traffic[0]['configuration']; ?></td>



                                        </tr>



                                        <tr>



                                            <td align="left" valign="top">Concessions Mgt. Type</td>



                                            <td align="left" valign="top"><?php echo $wideInfo_traffic[0]['mgtstructure']; ?></td>



                                        </tr>


                                        <?php
                                        $texpansionplanned = $wideInfo_traffic[0]['texpansionplanned'];

                                        $completedexpdate = $wideInfo_traffic[0]['completedexpdate'];

                                        $aaddsqft = $wideInfo_traffic[0]['addsqft'];

                                        $al_texpansionplanned = explode("#", $texpansionplanned);

                                        $al_completedexpdate = explode("#", $completedexpdate);

                                        $al_aaddsqft = explode("#", $aaddsqft);


                                        for ($t = 0; $t < count($al_texpansionplanned); $t++) {
                                            ?>


                                            <tr>



                                                <td align="left" valign="top">Expansion Planned
                                                    <i class="fa fa-question-circle" data-original-title="A reference as to whether the airport is expanding its concessions and/or other space in any given terminal or zone in its facility, how much additional space is planned and when the expansion is expected to be complete." 
                                                       data-container="body"data-toggle="tooltip"></i>
                                                </td>



                                                                                                                                                    <!--<td align="left" valign="top"><?php // echo $wideInfo_traffic[0]['texpansionplanned'];                            ?></td>-->
                                                <td align="left" valign="top"><?php echo $al_texpansionplanned[$t]; ?></td>



                                            </tr>



                                            <tr>



                                                <td align="left" valign="top">Addl Sq. Ft.</td>



                                                                                                                                                    <!--<td align="left" valign="top"><?php // echo $wideInfo_traffic[0]['addsqft'];                            ?></td>-->
                                                <td align="left" valign="top"><?php echo $al_aaddsqft[$t]; ?></td>



                                            </tr>



                                            <tr>



                                                <td align="left" valign="top">Complete Date</td>



                                                                                                                                                    <!--<td align="left" valign="top"><?php // echo $wideInfo_traffic[0]['completedexpdate'];                            ?></td>-->
                                                <td align="left" valign="top"><?php echo $al_completedexpdate[$t]; ?></td>



                                            </tr>

                                            <tr><td colspan="2"></td></tr>
                                        <?php } ?>

                                    <!--<tr><td></td><td></td></tr>-->

                                    </tbody>

                                </table>

                                <br><br>

                                </td>

                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                <td valign="top">

                                    <table class="table table-bordered table-hover table-striped">

                                        <tbody>

                                            <tr>



                                                <th align="left" valign="bottom"><h1 class="default-title">Terminals/Concourses</h1></th>



                                        <th align="left" valign="bottom"><h1 class="default-title">Abbreviation</h1></th>



                                        </tr>

                                        <?php foreach ($wideInfo_traffic as $newVal) { ?>



                                            <tr>



                                                <td align="left"><?php echo $newVal['terminalname']; ?></td>



                                                <td align="left"><?php echo $newVal['terminalabbr']; ?></td>



                                            </tr>

                                        <?php } ?>

                                        </tbody>

                                    </table>

                                </td>

                                </tr>

                                </tbody>

                                </table>
                            </div>   

                        </div>

                    </div>             

                </div>



                <!--<br>-->

                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="passenger" >

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

                                Passenger Traffic 

                            </a>

                            <i class="fa fa-question-circle" data-original-title="A breakdown of passenger traffic by terminal or concourse, by enplaning (the number of passengers leaving the airport on departing flights) and deplaning (the number of passengers arriving at the airport on arriving flights), by international (outside the U.S.) and domestic (inside the U.S.)." 
                               data-container="body"data-toggle="tooltip"></i>

                        </h4>

                    </div>

                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="passenger">

                        <div class="pannel-body">

                            <div class="table-responsive col-md-12"  style="overflow:auto;">

                                <br>

                                <table class="table table-bordered table-hover table-striped">

                                    <thead><tr>



                                            <td align="left" ><strong>Terminal</strong></th>



                                            <td align="right"><strong>Total</strong></th>



                                            <td align="right"><strong>+/- %</strong> <i class="fa fa-question-circle" data-original-title="A comparison of numbers from the current Fact Book year to the previous. Currently <?php echo date("Y"); ?> numbers compared to <?php echo date("Y") - 1; ?>. All symbols should be typed included. i.g. +2.4 % or -3.1 %." 
                                                                                        data-container="body"data-toggle="tooltip"></i></th>



                                            <td align="right"><strong>Deplaning</strong> <i class="fa fa-question-circle" data-original-title="The number of people disembarking an airline and arriving through the airport to baggage claim or ground transportation." 
                                                                                            data-container="body"data-toggle="tooltip"></i></th>



                                            <td align="right"><strong>Enplaning</strong> <i class="fa fa-question-circle" data-original-title="Passengers departing the airport on airlines leaving the airport to their predetermined destinations." 
                                                                                            data-container="body"data-toggle="tooltip"></i></th>



                                            <td align="right" ><strong>EP Domestic</strong></th>



                                            <td align="right" ><strong>EP Int'l</strong></th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                        <?php
                                        $TP = 0;

                                        $ttpasstraffic = 0;

                                        $ttdeplaning = 0;

                                        $ttenplaning = 0;

                                        $ttepdomestic = 0;

                                        $ttepintl = 0;

                                        foreach ($wideInfo_traffic as $wideInfo_traffic) {



                                            $TP+=$wideInfo_traffic['tpasstrafficcompare'];

                                            $ttpasstraffic+=$wideInfo_traffic['tpasstraffic'];

                                            $ttdeplaning+=$wideInfo_traffic['tdeplaning'];

                                            $ttenplaning+=$wideInfo_traffic['tenplaning'];

                                            $ttepdomestic+=$wideInfo_traffic['tepdomestic'];

                                            $ttepintl+=$wideInfo_traffic['tepintl'];
                                            ?>





                                            <tr>



                                                <td align="left"><?php echo $wideInfo_traffic['terminalabbr']; ?></td>

                                                <td align="right"><?php
                                                    if ($wideInfo_traffic['tpasstraffic'] != '') {

                                                        echo number_format($wideInfo_traffic['tpasstraffic']);
                                                    }
                                                    ?></td>

                                                <td align="right"><?php echo $wideInfo_traffic['tpasstrafficcompare']; ?></td>



                                                <td align="right"><?php
                                                    if ($wideInfo_traffic['tdeplaning'] != '') {

                                                        echo number_format($wideInfo_traffic['tdeplaning']);
                                                    }
                                                    ?></td>



                                                <td align="right"><?php
                                                    if ($wideInfo_traffic['tenplaning'] != '') {

                                                        echo number_format($wideInfo_traffic['tenplaning']);
                                                    }
                                                    ?></td>



                                                <td align="right"><?php
                                                    if ($wideInfo_traffic['tepdomestic'] != '') {

                                                        echo number_format($wideInfo_traffic['tepdomestic']);
                                                    }
                                                    ?></td>



                                                <td align="right"><?php
                                                    if ($wideInfo_traffic['tepintl'] != '') {

                                                        echo number_format($wideInfo_traffic['tepintl']);
                                                    }
                                                    ?></td>

                                            </tr>

                                        <?php } ?>

                                        <tr scope="row">

                                            <td><h1 class="default-title" style="font-size:1.3em;">Total</h1></td>

                                            <td class="text-right"><?php echo number_format($ttpasstraffic); ?></td>

                                            <!--<td class="text-right"><?php // echo number_format($TP, 2);                            ?>%</td>-->
                                            <!--<td class="text-right"><?php // echo number_format($main_data[0]['apasstrafficcompare'], 2);                         ?>%</td>-->
                                            <td class="text-right"><?php echo $main_data[0]['apasstrafficcompare']; ?></td>

                                            <td class="text-right"><?php echo number_format($ttdeplaning); ?></td>

                                            <td class="text-right"><?php echo number_format($ttenplaning); ?></td>

                                            <td class="text-right"><?php echo number_format($ttepdomestic); ?></td> 

                                            <td class="text-right"><?php echo number_format($ttepintl); ?></td>                    

                                        </tr>



                                    </tbody>

                                </table>

                            </div>

                            <br>

                            <br>


                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" style="width: 97.5% !important; margin-left: 15px;"> 

                                    <tbody>

                                        <tr>

                                            <th align="left"><h1 class="default-title">Passenger Traffic Comment</h1></th>



                                    </tr>

                                    <tr>

                                        <td colspan="4" align="left"><?php
                                            if ($main_data[0]['apasscomment'] == '') {

                                                echo '<div class="alert alert-danger">No Comment Available... </div>';
                                            } else {

                                                echo '<div class="alert alert-success">' . $main_data[0]['apasscomment'] . '</div>';
                                            }
                                            ?></td>

                                    </tr>

                                    </tbody>

                                </table>
                            </div>



                        </div>

                    </div>

                </div>

                <!--<br>-->

                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="airport_specifics" data-toggle="tooltip" title="Click on Text to Close / Open">

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">

                                Airport Specifics

                            </a>

                        </h4>

                    </div>

                    <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="airport_specifics">

                        <div class="pannel-body">

                            <br>

                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" style="width: 97.5% !important; margin-left: 15px;">

                                    <tbody>

                                        <tr>

                                            <td align="left" width="50%">Pre/Post Security % of SqFt: <i class="fa fa-question-circle" data-original-title="The percentage of concessions space that is located pre- versus post-security, i.e. 20/80 would translate to 20% of the concession space is located before security and 80% is located beyond security." 
                                                                                                         data-container="body"data-toggle="tooltip"></i></td>

                                            <td align="right">Pre: <?php echo $main_data[0]['presecurity']; ?>%</td>

                                            <td align="right">Post: <?php echo $main_data[0]['postsecurity']; ?>%</td>

                                            <td align="right">&nbsp;</td>

                                        </tr>

                                        <tr>

                                            <td align="left">Ratio of Business to Leisure: <i class="fa fa-question-circle" data-original-title="The percentage of total enplaning passengers that are traveling on business versus the percentage traveling for leisure or pleasure, i.e. 75/25 would translate to 75% of the people departing the airport are taking a business trip and 25% are on a vacation trip." 
                                                                                              data-container="body"data-toggle="tooltip"></i></td>

                                            <td align="right"> <?php echo $main_data[0]['ratiobusleisure']; ?> </td>

                                            <td align="right">&nbsp;</td>

                                            <td align="right">&nbsp;</td>

                                        </tr>

                                        <tr>

                                            <td align="left">O &amp; D To Transfer: <i class="fa fa-question-circle" data-original-title="This is ratio denoting the percentage of origination and destination passengers relative to the percentage of passengers transferring from one flight to another in the airport." 
                                                                                       data-container="body"data-toggle="tooltip"></i></td>

                                            <td align="right"><?php echo $main_data[0]['ondtransfer']; ?></td>

                                            <td align="right">&nbsp;</td>

                                            <td align="right">&nbsp;</td>

                                        </tr>

                                        <tr>

                                            <td align="left">Average Dwell Time: <i class="fa fa-question-circle" data-original-title="The average amount of time a departing passenger spends within the airport." 
                                                                                    data-container="body"data-toggle="tooltip"></i></td>

                                            <td align="right"><?php echo $main_data[0]['avgdwelltime']; ?> min.</td>

                                            <td align="right">&nbsp;</td>

                                            <td align="right">&nbsp;</td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>



                <!--<br>-->

                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="gross_sales" data-toggle="tooltip" title="Click on Text to Close / Open">

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">

                                Gross Sales

                            </a>

                        </h4>

                    </div>

                    <div id="collapseFive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="gross_sales">

                        <div class="pannel-body">

                            <br>

                            <div class="table-responsive">                                           	

                                <?php
                                $ttenplaning = 0;

                                foreach ($airport_grossSales as $ags):

                                    $tfbcurrsqft+=$ags['fbcurrsqft'];

                                    $tfbgrosssales+=$ags['fbgrosssales'];

                                    $tfbsalesep+=$ags['fbsalesep'];

                                    $tfbrentrev+=$ags['fbrentrev'];



                                    $tsrcurrsqft+=$ags['srcurrsqft'];

                                    $tsrgrosssales+=$ags['srgrosssales'];

                                    $tsrsalesep+=$ags['srsalesep'];

                                    $tsrrentrev+=$ags['srrentrev'];



                                    $tngcurrsqft+=$ags['ngcurrsqft'];

                                    $tnggrosssales+=$ags['nggrosssales'];

                                    $tngsalesep+=$ags['ngsalesep'];

                                    $tngrentrev+=$ags['ngrentrev'];



                                    $tdfcurrsqft+=$ags['dfcurrsqft'];

                                    $tdfgrosssales+=$ags['dfgrosssales'];

                                    $tdfsalesep+=$ags['dfsalesep'];

                                    $tdfrentrev+=$ags['dfrentrev'];

                                    $ttenplaning+=$ags['tenplaning'];



                                    //print_r($ags);
                                    ?>

                                    <table class="table table-striped table-hover table-bordered"  style="width:99% !important; margin-left: 6px;">                                             				<tbody>

                                        <thead>

                                            <tr class="info">

                                                <th scope="row"><b>Terminal - <?php echo $ags['terminalabbr']; ?></b></th>

                                                <th class="text-right"><b>Current Sq. Ft.</b></th>

                                                <th class="text-right"><b>Gross Sales</b></th>

                                                <th class="text-right"><b>Sales/EP</b></th>

                                                <th class="text-right"><b>Gross Rentals</b></th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                                <th scope="row" align="right">Food & Beverage</th>

                                                <td align="right"><?php echo number_format($ags['fbcurrsqft']); ?></td>

                                                <td align="right">$<?php echo number_format($ags['fbgrosssales'], 2); ?></td>

                                                <td align="right">$<?php echo number_format($ags['fbgrosssales'] / $ags['tenplaning'], 2); ?></td>

                                                <td align="right">$<?php echo number_format($ags['fbrentrev'], 2); ?></td>                                                </tr>

                                            <tr>

                                                <th scope="row">Specialty Retail</th>

                                                <td align="right"><?php echo number_format($ags['srcurrsqft']); ?></td>

                                                <td align="right">$<?php echo number_format($ags['srgrosssales'], 2); ?></td>

                                                <td align="right">$<?php echo number_format($ags['srgrosssales'] / $ags['tenplaning'], 2); ?></td>

                                                <td align="right">$<?php echo number_format($ags['srrentrev'], 2); ?></td>                                                </tr>

                                            <tr>

                                                <th align="right">News & Gifts:</th>

                                                <td align="right"><?php echo number_format($ags['ngcurrsqft']); ?></td>

                                                <td align="right">$<?php echo number_format($ags['nggrosssales'], 2); ?></td>

                                                <td align="right">$<?php echo number_format($ags['nggrosssales'] / $ags['tenplaning'], 2); ?></td>

                                                <td align="right">$<?php echo number_format($ags['ngrentrev'], 2); ?></td>

                                            </tr>

                                            <tr>

                                                <th align="right">Duty Free:</th>

                                                <td align="right"><?php echo number_format($ags['dfcurrsqft']); ?></td>

                                                <td align="right">$<?php echo number_format($ags['dfgrosssales'], 2); ?></td>

                                                                                                                                                            <!--<td align="right">$<?php // echo number_format($ags['dfgrosssales'] / $ags['tenplaning'], 2);                            ?></td>-->
                                                                                                                                                            <!--<td align="right">$<?php // echo number_format($ags['dfgrosssales'] / $ags['tepintl'], 2);                            ?></td>-->
                                                <td align="right">$<?php echo number_format($ags['dfsalesep'], 2); ?></td>

                                                <td align="right">$<?php echo number_format($ags['dfrentrev'], 2); ?></td> 

                                            </tr>

                                            <tr>

                                                <td colspan="6">&nbsp;</td>

                                            </tr>

                                            <tr>

                                                <th>Enplanement</th>

                                                <td colspan="5">

                                                    <?php echo number_format($ags['tenplaning']); ?>

                                                </td>

                                            </tr>

                                            <tr>

                                                <th>Deplanement</th>

                                                <td colspan="5">

                                                    <?php echo number_format($ags['tdeplaning']); ?>

                                                </td>

                                            </tr>

                                            <tr>

                                                <th>Dominant Airline <i class="fa fa-question-circle" data-original-title="Name of the airline that occupies the most gates in the respective terminal." 
                                                                        data-container="body"data-toggle="tooltip"></i></th>

                                                <td colspan="5">

                                                    <?php echo strtoupper($ags['tdominantair']); ?>

                                                </td>

                                            </tr>

                                        </tbody>

                                    </table>

                                    <br/>

                                    <?php
                                endforeach;
                                ?>

                                <br/>

                                <table class="table table-striped table-hover table-bordered"  style="width:99% !important; margin-left: 6px;">                                             	<thead>

                                        <tr class="success">

                                            <th scope="row"><b>Terminal - Total</b></th>

                                            <th align="right"><b>Current Sq. Ft.</b></th>

                                            <th align="right"><b>Gross Sales</b></th>

                                            <th align="right"><b>Sales/EP</b></th>

                                            <th align="right"><b>Gross Rentals</b></th>

                                            <th align="right"><b>Rent/EP</b></th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                        <tr>                                                    

                                            <td>Food & Beverage</td>

                                            <td><?php echo number_format($tfbcurrsqft); ?></td>

                                            <td>$<?php echo number_format($tfbgrosssales, 2); ?></td>

                                            <td>$<?php echo number_format($tfbgrosssales / $ttenplaning, 2); ?></td>

                                            <td>$<?php echo number_format($tfbrentrev, 2); ?></td>

                                            <td>$<?php echo number_format($tfbrentrev / $ttenplaning, 2); ?></td>

                                        </tr>

                                        <tr>                                                    

                                            <td>Specialty Retail</td>

                                            <td><?php echo number_format($tsrcurrsqft); ?></td>

                                            <td>$<?php echo number_format($tsrgrosssales, 2); ?></td>

                                            <td>$<?php echo number_format($tsrgrosssales / $ttenplaning, 2); ?></td>

                                            <td>$<?php echo number_format($tsrrentrev, 2); ?></td>

                                            <td>$<?php echo number_format($tsrrentrev / $ttenplaning, 2); ?></td>

                                        </tr>

                                        <tr>                                                    

                                            <td>News & Gifts</td>

                                            <td><?php echo number_format($tngcurrsqft); ?></td>

                                            <td>$<?php echo number_format($tnggrosssales, 2); ?></td>

                                            <td>$<?php echo number_format($tnggrosssales / $ttenplaning, 2); ?></td>

                                            <td>$<?php echo number_format($tngrentrev, 2); ?></td>

                                            <td>$<?php echo number_format($tngrentrev / $ttenplaning, 2); ?></td>

                                        </tr>

                                        <tr>                                                    

                                            <td>Duty Free:</td>

                                            <td><?php echo number_format($tdfcurrsqft); ?></td>

                                            <td>$<?php echo number_format($tdfgrosssales, 2); ?></td>

                                            <!--<td>$<?php // echo number_format($tdfgrosssales / $ttenplaning, 2);                            ?></td>-->
                                            <!--<td>$<?php // echo number_format($main_data[0]['adfsalesep'], 2);                        ?></td>-->
                                            <td>$<?php
                                                if ($main_data[0]['adfsalesep'] = '-') {
                                                    echo '0.00';
                                                } else {
                                                    echo number_format($main_data[0]['adfsalesep'], 2);
                                                }
                                                ?></td>

                                            <td>$<?php echo number_format($tdfrentrev, 2); ?></td>

                                            <!--<td>$<?php // echo number_format($tdfrentrev / $ttenplaning, 2);                            ?></td>-->
                                            <td>$<?php echo number_format($main_data[0]['adfrentep'], 2); ?></td>

                                        </tr>

                                        <tr class="info">

                                            <td>Airport Totals:<br/><b><i>(excludes Duty Free)</i></b></td>

                                            <?php // echo number_format($tfbcurrsqft + $tsrcurrsqft + $tngcurrsqft + $tdfcurrsqft);    ?>
                                            <td><?php echo number_format($tfbcurrsqft + $tsrcurrsqft + $tngcurrsqft); ?>

                                            </td>

                                            <td>$<?php echo number_format($tfbgrosssales + $tsrgrosssales + $tnggrosssales, 2); ?>

                                            </td>

                                            <td>$<?php echo number_format(($tnggrosssales / $ttenplaning) + ($tsrgrosssales / $ttenplaning) + ($tfbgrosssales / $ttenplaning), 2); ?>

                                            </td>

                                            <td>$<?php echo number_format($tfbrentrev + $tsrrentrev + $tngrentrev, 2); ?>

                                            </td>

                                            <td>$<?php echo number_format(($tfbrentrev / $ttenplaning) + ($tngrentrev / $ttenplaning) + ($tsrrentrev / $ttenplaning), 2); ?>	

                                            </td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>



                            <br><br>

                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" style="width: 99% !important; margin-left: 6px;"> 

                                    <tbody>

                                        <tr>

                                            <th align="left"><h1 class="default-title">Gross Sales Comment</h1></th>



                                    </tr>

                                    <tr>

                                        <td colspan="4" align="left"><?php
                                            if ($main_data[0]['grosscomment'] == '') {

                                                echo '<br/><div class="alert alert-danger">No Comment Available... </div>';
                                            } else {

                                                echo '<br/><div class="alert alert-success">' . $main_data[0]['grosscomment'] . '</div>';
                                            }
                                            ?></td>

                                    </tr>

                                    </tbody>

                                </table>
                            </div>


                        </div>


                    </div>

                </div>  

                <!--<br>-->

                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="airportwide_revenue" data-toggle="tooltip" title="Click on Text to Close / Open">

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">

                                Airportwide Revenue 

                            </a>

                        </h4>

                    </div>

                    <div id="collapseSix" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="airportwide_revenue">

                        <div class="pannel-body">

                            <br>

                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" style="width:99% !important; margin-left: 6px;">

                                    <tbody>

                                        <tr class="info">

                                            <td align="left" valign="top">&nbsp;</td>

                                            <td align="right"><strong>Revenue</strong></td>

                                            <td align="right"><strong>Revenue to Airport</strong></td>

                                        </tr>

                                        <tr>

                                            <td align="left" valign="top">Passenger Services
                                                <i class="fa fa-question-circle" data-original-title="Refers to all non-retail or non-food services provided by the airport such as massage, ATMs, banking, business centers, barber shops, etc." 
                                                   data-container="body"data-toggle="tooltip"></i>
                                            </td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['passservicesrev'] != '') {

                                                    echo number_format($main_data[0]['passservicesrev'], 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['passservicesrevtoair'] != '') {

                                                    echo number_format($main_data[0]['passservicesrevtoair'], 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                        </tr>

                                        <tr>

                                            <td align="left" valign="top">Advertising
                                                <i class="fa fa-question-circle" data-original-title="Gross revenue and rent to the airport, where available, are provided. This category includes all revenue generated from advertising space at the airport that has been bought by companies paying for said space for the purpose of marketing their brand, including dioramas, wall space, banners, etc." 
                                                   data-container="body"data-toggle="tooltip"></i>
                                            </td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['advertisingrev'] != '') {

                                                    echo number_format($main_data[0]['advertisingrev'], 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['advertisingrevtoair'] != '') {

                                                    echo number_format($main_data[0]['advertisingrevtoair'], 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                        </tr>

                                        <tr>

                                            <td align="left" valign="top">Currency Exchange</td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['currencyexrev'] != '') {

                                                    echo number_format($main_data[0]['currencyexrev'], 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['currencyexrevtoair'] != '') {

                                                    echo number_format($main_data[0]['currencyexrevtoair'], 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                        </tr>

                                    </tbody>

                                </table> 

                            </div>

                            <br>

                            <br>
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" style="width:99% !important; margin-left: 6px;">

                                    <tbody>

                                        <tr class="info"> 

                                            <td align="left"><strong>Car Rentals</strong>
                                                <i class="fa fa-question-circle" data-original-title="Shows the number of car rental companies at the airport, on-site and off-site, the gross revenue generated by the rental companies and the total amount of rent paid to the airport." 
                                                   data-container="body"data-toggle="tooltip"></i>
                                            </td>

                                            <td align="right"><strong>Agencies</strong></td>

                                            <td align="right"><strong>Gross Revenue</strong></td>

                                            <td align="right"><strong>Gross Rentals</strong></td>

                                        </tr>

                                        <tr>

                                            <td align="left">Car Rental On Site:</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['carrentalagenciesonsite'] != '') {

                                                    echo number_format($main_data[0]['carrentalagenciesonsite']);
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['carrentalrevonsite'] != '') {

                                                    $g_rev_os = $this->common->parseNumber($main_data[0]['carrentalrevonsite']);

                                                    echo number_format($g_rev_os, 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['carrentalrevtoaironsite'] != '') {

                                                    $g_ren_os = $this->common->parseNumber($main_data[0]['carrentalrevtoaironsite']);

                                                    echo number_format($g_ren_os, 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                        </tr>

                                        <tr>

                                            <td align="left">Car Rental Off Site:</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['carrentalagenciesoffsite'] != '') {

                                                    echo number_format($main_data[0]['carrentalagenciesoffsite']);
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['carrentalrevoffsite'] != '') {

                                                    $g_rev_ofs = $this->common->parseNumber($main_data[0]['carrentalrevoffsite']);

                                                    echo number_format($g_rev_ofs, 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['carrentalrevtoairoffsite'] != '') {

                                                    $g_ren_ofs = $this->common->parseNumber($main_data[0]['carrentalrevtoairoffsite']);

                                                    echo number_format($g_ren_ofs, 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                        </tr>

                                        <tr>

                                            <td align="left">Total Cars Rented:</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['totalcarsrented'] != '') {

                                                    echo number_format($main_data[0]['totalcarsrented']);
                                                }
                                                ?></td>

                                            <td align="right">&nbsp;</td>

                                            <td align="right">&nbsp;</td>

                                        </tr>

                                        <tr>

                                            <td align="left">Car Rental Sq. Ft.</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['carrentalsqft'] != '') {

                                                    echo number_format($main_data[0]['carrentalsqft']);
                                                }
                                                ?></td>

                                            <td align="right">&nbsp;</td>

                                            <td align="right">&nbsp;</td>

                                        </tr>

                                    </tbody>

                                </table>
                            </div>

                            <br>

                            <br>
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" style="width:99% !important; margin-left: 6px;">

                                    <tbody>

                                        <tr class="info">

                                            <td align="left"><strong>Parking</strong>
                                                <i class="fa fa-question-circle" data-original-title="Shows all parking rates; gross revenue from parking; the number of concessionaires are located on- or off-airport; daily rate concessionaire(s) charge to parkers; concession fee paid to airport (if applicable) and total rent to the airport from parking." 
                                                   data-container="body"data-toggle="tooltip"></i>
                                            </td>

                                            <td align="left">&nbsp;</td>

                                            <td align="right">&nbsp;</td>

                                            <td align="right">&nbsp;</td>

                                        </tr>

                                        <tr>

                                            <td align="left">Parking</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['parkingspaces'] != '') {

                                                    echo number_format($main_data[0]['parkingspaces']);
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['parkingrev'] != '') {



                                                    echo number_format($this->common->parseNumber($main_data[0]['parkingrev']), 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                            <td align="right">$<?php
                                                if ($main_data[0]['parkingrevtoair'] != '') {

                                                    echo number_format($this->common->parseNumber($main_data[0]['parkingrevtoair']), 2);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>

                                        </tr>

                                    </tbody>

                                </table>
                            </div>

                            <br>

                            <br>
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" style="width:99% !important; margin-left: 6px;">

                                    <tbody>

                                        <tr class="info">

                                            <td align="left"><strong>Parking Rates</strong></td>

                                            <td align="right"><strong>Short</strong></td>

                                            <td align="right"><strong>Long</strong></td>

                                            <td align="right"><strong>Economy</strong></td>

                                            <td align="right"><strong>Valet</strong></td>

                                        </tr>

                                        <tr>

                                            <td>Hourly</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['hourlyshort'] != '') {

                                                    echo '$' . $main_data[0]['hourlyshort'];
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['hourlylong'] != '') {

                                                    echo '$' . $main_data[0]['hourlylong'];
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['hourlyeconomy'] != '') {

                                                    echo '$' . $main_data[0]['hourlyeconomy'];
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['hourlyvalet'] != '') {

                                                    echo '$' . $main_data[0]['hourlyvalet'];
                                                }
                                                ?></td>

                                        </tr>

                                        <tr>

                                            <td>Daily</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['dailyshort'] != '') {

                                                    echo '$' . $main_data[0]['dailyshort'];
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['dailylong'] != '') {

                                                    echo '$' . $main_data[0]['dailylong'];
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['dailyeconomy'] != '') {

                                                    echo '$' . $main_data[0]['dailyeconomy'];
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['dailyvalet'] != '') {

                                                    echo '$' . $main_data[0]['dailyvalet'];
                                                }
                                                ?></td>

                                        </tr>

                                        <tr>

                                            <td># Spaces</td>

                                            <td align="right"><?php
                                                if ($main_data[0]['spacesshort'] != '') {

                                                    echo number_format($main_data[0]['spacesshort']);
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['spaceslong'] != '') {

                                                    echo number_format($main_data[0]['spaceslong']);
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['spaceseconomy'] != '') {

                                                    echo number_format($main_data[0]['spaceseconomy']);
                                                }
                                                ?></td>

                                            <td align="right"><?php
                                                if ($main_data[0]['spacesvalet'] != '') {

                                                    echo number_format($main_data[0]['spacesvalet']);
                                                }
                                                ?></td>

                                        </tr>

                                    </tbody>

                                </table>
                                <div class="table-responsive">

                                    <br>

                                    <table class="table table-bordered table-hover table-striped" style="width:99% !important; margin-left: 6px;">

                                        <tbody>

                                            <tr>

                                                <th align="left"><h1 class="default-title">Airportwide Comment</h1></th>



                                        </tr>

                                        <tr>

                                            <td colspan="4" align="left"><?php
                                                if ($main_data[0]['awrevcomment'] == '') {

                                                    echo '<br/><div class="alert alert-danger">No Comment Available... </div>';
                                                } else {

                                                    echo '<br/><div class="alert alert-success">' . $main_data[0]['awrevcomment'] . '</div>';
                                                }
                                                ?></td>

                                        </tr>

                                        </tbody>

                                    </table>

                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <!--<br>-->

                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="concession" data-toggle="tooltip" title="Click on Text to Close / Open">

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">

                                Concession Tenant Details

                            </a>

                        </h4>

                    </div>

                    <div id="collapseSeven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="concession">

                        <div class="panel-body">

                            <h1 class="default-title">Concession Tenant Details</h1><br />        

                            <div class="table-responsive">



                                <?php
//                                if (count($airporcontactinfo) > 0) {
//                                    foreach ($airporcontactinfo as $contact):
                                ?>



                                <!--<br/>-->                                                            

                                <?php
//                                endforeach;
//                                } else {
                                ?>

                                <!--                                    <div class="alert alert-danger">
                                
                                                                        No Result found...
                                
                                                                    </div>-->

                                <?php
//                                }
                                ?>

                                <!--                                </div>
                                
                                                            </div>
                                
                                                        </div>-->



                                <!--                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="concession">
            
                                                        <div class="pannel-body">
            
                                                        </div>
            
                                                    </div>-->

                                <!--</div>-->

                                <!--                <br>
                
                                                <br>-->

                                <?php $cntr = 0; ?>

                                <?php foreach ($categories as $categoryName) { ?>

                                    <div class="panel panel-primary">

                                                                                        <!--<div class="panel-heading"role="tab" id="dynamicconcession<?php // echo $cntr;                ?>" data-toggle="tooltip" title="Click on Text to Close / Open"-->
                                        <div class="panel-heading"role="tab" id="dynamicconcession<?php echo $cntr; ?>">

                                            <h4 class="panel-title">

                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight<?php echo $cntr; ?>" aria-expanded="false" aria-controls="collapseEight<?php echo $cntr; ?>">

                                                    <?php echo $get_category_name[$cntr]['category']; ?>

                                                </a>

                                                <i class="fa fa-question-circle" data-original-title=" 
                                                <?php
                                                if ($get_category_name[$cntr]['category'] == 'Advertising') {
                                                    echo 'Gross revenue and rent to the airport, where available, are provided. This category includes all revenue generated from advertising space at the airport that has been bought by companies paying for said space for the purpose of marketing their brand, including dioramas, wall space, banners, etc.';
                                                } elseif ($get_category_name[$cntr]['category'] == 'Duty Free') {
                                                    echo 'Retail shops that do not apply local or national taxes and duties to products sold.';
                                                } elseif ($get_category_name[$cntr]['category'] == 'Food/Beverage') {
                                                    echo 'Any store within an airport that utilizes more than 50% of its space to offer food and beverage products for sale to the public.';
                                                } elseif ($get_category_name[$cntr]['category'] == 'News/Gifts') {
                                                    echo 'Any store within an airport that utilizes the majority of its space for the sale of news publications and gift items.';
                                                } elseif ($get_category_name[$cntr]['category'] == 'Passenger Services') {
                                                    echo 'Refers to all non-retail or non-food services provided by the airport such as massage, ATMs, banking, business centers, barber shops, etc.';
                                                } elseif ($get_category_name[$cntr]['category'] == 'Specialty Retail') {
                                                    echo 'A store that utilizes a majority of its space to sell non-news, non-food, retail products such as apparel, souvenirs, gift items, art products, jewelry, etc.';
                                                }
                                                ?>
                                                   "
                                                   data-container="body"data-toggle="tooltip"
                                                   ></i>

                                            </h4>

                                        </div>

                                        <div id="collapseEight<?php echo $cntr; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="dynamicconcession<?php echo $cntr; ?>">



                                            <div class="pannel-body">

                                                <div class="table-responsive" style="overflow:auto;">

                                                    <div class="col-md-12">

                                                        <br>

                                                        <table class="table table0 table-bordered table-hover table-striped" class="display col-md-12" width="100%" cellspacing="0">

                                                            <?php $cntr++; ?>

                                                            <thead>

                                                                <tr>



                                                                    <th style="width: 400px" >Tenant Name (Company)</th>



                                                                    <th style="width: 400px">Company Name</th>



                                                                    <th style="width: 400px">Product Description</th>



                                                                    <th style="width: 400px">Terminal</th>



                                                                    <th style="width: 400px">#</th>



                                                                    <th style="width: 400px">Sq Ft <i class="fa fa-question-circle" data-original-title="Stands for square footage, or the amount of space occupied by the associated category." 
                                                                                                      data-container="body"data-toggle="tooltip"></i></th>



                                                                    <th style="width: 400px">Expires <i class="fa fa-question-circle" data-original-title="Month, date and year in which the contract with the associated concept and operator expires." 
                                                                                                        data-container="body"data-toggle="tooltip"></i></th>



                                                                </tr>

                                                            </thead>

                                                            <tbody>

                                                                <?php foreach ($categoryName as $categoryOptions) { ?>

                                                                    <tr>



                                                                        <td style="width: 400px"><?php echo $categoryOptions['outletname']; ?></td>



                                                                        <td style="width: 400px"><?php echo $categoryOptions['companyname']; ?></td>



                                                                        <td style="width: 400px"><?php echo $categoryOptions['productdescription']; ?></td>



                                                                        <td style="width: 400px"><?php echo $categoryOptions['termlocation']; ?></td>



                                                                        <td style="width: 400px"><?php echo $categoryOptions['numlocations']; ?></td>



                                                                        <td style="width: 400px"><?php echo number_format($categoryOptions['sqft']); ?></td>



                                                                                                                                                                                                                                                                                <!--<td style="width: 400px"><?php // echo $this->common->get_formatted_datetime($categoryOptions['exp']);                           ?></td>-->
                                                                        <td style="width: 400px"><?php
                                                                            if ($categoryOptions['exp_reason'] != '') {

//                                                                echo $categoryOptions['exp_reason'];
//                                                                $reasons = str_replace(array('M-T-M', 'TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $categoryOptions['exp_reason']);
                                                                                $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $categoryOptions['exp_reason']);
//                                                                $reasons = $categoryOptions['exp_reason'];

                                                                                if ($categoryOptions['exp_date'] != '0000-00-00 00:00:00' && $categoryOptions['exp_date'] != '' & $categoryOptions['exp_reason'] != 'M-T-M') {
                                                                                    echo $reasons . '<br>' . date("M d,Y H:i:s", strtotime($categoryOptions['exp_date']));
                                                                                } else if (($categoryOptions['exp_date'] == '0000-00-00 00:00:00' || $categoryOptions['exp_date'] == '') && $categoryOptions['exp_reason'] != 'M-T-M') {
                                                                                    echo $reasons;
                                                                                } else {
                                                                                    echo $reasons;
                                                                                }
                                                                            } else {

                                                                                if ($categoryOptions['exp'] != '') {
                                                                                    echo $this->common->get_formatted_datetime($categoryOptions['exp']);
                                                                                } else {
                                                                                    echo '';
                                                                                }
                                                                            }
                                                                            ?></td>



                                                                    </tr>

                                                                <?php } ?>

                                                            </tbody>

                                                        </table>

                                                        <br>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                <?php } ?>

                            </div>

                        </div>

                    </div>

                </div>
                <br>
                <div class="row">

                    <div class="form-btn-approved">

                        <?php
                        $user = $this->common->GetSessionKey('email');

                        $staff = '@airportrevenuenews.com';

                        if (stripos($user, $staff) !== FALSE) {
                            ?>
                            <form class="" action="<?php echo site_url('wizard/airports/' . $this->uri->segment(3)); ?>" method="post">

                                <div class="form-group">                  

                                    <input type="hidden" name="aid" value="<?php echo $this->uri->segment(3); ?>">

                                    <input type="hidden" name="action" value="approve">

                                    <input type="hidden" name="actiontype" value="publish">

                                    <button type="submit" class="btn btn-success" title="ARN Staff are the only ones to see this option."><i class="fa fa-check"></i>&nbsp;ARN Staff Approved</button>

                                                                                                                                                    <!--&nbsp;&nbsp;&nbsp;<a href="<?php // echo site_url('wizard/company/' . $this->uri->segment(3) . "/publish");                                  ?>" class="btn btn-success"><i class="fa fa-cloud-upload"></i>&nbsp;Final Step Publish</u></a>-->	

                                </div>

                            </form>

                        <?php } ?>

                        <div class="pub">
                            <a href="<?php echo site_url('wizard/airports/' . $this->uri->segment(3) . "/publish"); ?>" class="btn btn-success"><i class="fa fa-cloud-upload"></i>&nbsp;Final Step Publish</u></a>	

                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 text-danger">

                        Once all data is entered and you are satisfied with your results, click on Final Step

                    </div>

                </div>

            </section>

        </section>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">

                        <input type="hidden" id="recdelid" value="">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                        </button>

                        <h4 class="modal-title" id="myModalLabel">Conifm Delete</h4>

                    </div>

                    <div class="modal-body">

                        Are you sure want to delete this Airport contact?

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-danger" data-dismiss="modal">

                            <i class="fa fa-times">&nbsp;</i>Cancel</button>        

                        <button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete

                        </button>        

                    </div>

                </div>

            </div>

        </div>

    </div>


    <div class="modal fade bs-example-modal-lg" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModaleditLabel" aria-hidden="true">

        <div class="modal-dialog modal-lg">

            <div class="modal-content">

                <div class="modal-header">

                    <input type="hidden" id="receditid" value="">

                        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->

                    <h3 class="modal-title" id="myModaleditLabel">Edit Contact</h3>

                </div>

                <div class="modal-body">



                        <!--<form method="post"  class="form-horizontal"  name="contactform_edit" id="contactform_edit" onSubmit="return checkval_e();" role="form"  action="<?php // echo site_url('wizard/airport/airport_edit_step_1') . '/' . $step . '/' . $aid;                                     ?>">-->   
                    <!--<form method="post"  class="form-horizontal"  name="contactform_edit" onSubmit="return checkval_e();" role="form"  action="<?php echo site_url('wizard/airport/airport_edit_step_1/1') . '/' . $aid; ?>">-->   
                    <form method="post"  class="form-horizontal"  name="contactform_edit" id="editcontact" data-toggle="validator" role="form"  action="<?php echo site_url('wizard/airport/airport_edit_step_1/1') . '/' . $aid; ?>">   
                    <!--<form method="post"  class="form-horizontal"  name="contactform_edit" id="contactform_edit"  role="form"  action="<?php // echo site_url('wizard/airport/airport_edit_step_1') . '/' . $step . '/' . $aid;                                     ?>">-->   


                        <!--<form method="post"  class="form-horizontal"  name="contactform_edit" onSubmit="return checkval_e();" role="form"  action="">-->   

                        <div class="row">

                            <div class="error_e col-sm-12">
                                <?php echo $this->common->getmessage(); ?>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="inputEmail3" class="col-sm-3 control-label">&nbsp;</label>
                                    <div class="col-sm-8">
                                        <div class="text-danger">* Indicates Required Information </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-sm-12">


                                <div class="form-group ">
                                    <!--<label for="requestcompany" class="control-label col-sm-3" >&nbsp;</label>-->
                                    <label for="requestcompany_e" class="control-label col-sm-5">Select Management Responsibility:</label>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-5" style="margin-left: 110px !important;">
                                        <select name="mgtresponsibility" id="requestcompany_e" class="chosen form-control" tabindex="1">
                                            <option></option>
                                            <option value=""> </option>
                                            <option value="1">Airport Concessions</option>
                                            <option value="2">Food and Beverage</option>
                                            <option value="3">Retail, News and Gifts</option>
                                            <option value="4">Leasing</option>
                                            <option value="5">Passenger Services</option>
                                            <option value="6">Advertising</option>
                                        </select>
                                    </div>
                                </div>


                            </div>

                            <div class="col-sm-6">
                                <!--
                                <a href="<?php // echo site_url('frontend/airport_add_steps_1');                                        ?>" name="addmorecontact" class="addmorecontact btn btn-primary" value="" id="">+ Add additional Contact</a>  -->

<!--<input type="hidden" name="airport_contfact_id" value="<?php // echo $this->uri->segment(5);                                        ?>"  >-->
                                <!--</br>-->           
                                <input type="hidden" name="airport_contact_id" id="airport_contact_id" />

                                <!--                                    <div class="form-group">
                                                                        <label for="requestcompany" class="control-label col-sm-3" ><span class="text-danger">*</span>Select Management Responsibility:</label>
                                                                        <div class="col-sm-8">
                                                                            <select name="mgtresponsibility" id="requestcompany_e" class="form-control" tabindex="1">
                                                                                <option></option>
                                                                                <option value="1">Airport Concessions</option>
                                                                                <option value="2">Food and Beverage</option>
                                                                                <option value="3">Retail, News and Gifts</option>
                                                                                <option value="4">Leasing</option>
                                                                                <option value="5">Passenger Services</option>
                                                                                <option value="6">Advertising</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>				  -->

                                <!--                                <div class="form-group">
                                                                    <div class="col-sm-3">&nbsp;</div>
                                                                    <label for="requestcompany" class="control-label col-sm-8"><span class="text-danger">*</span>Select Management Responsibility:</label>-->
                                <!--<div class="col-sm-8">-->
                                <!--&nbsp;-->
<!--                                            <select name="mgtresponsibility" id="requestcompany_e" class="form-control" tabindex="1">
                                    <option></option>
                                    <option value="1">Airport Concessions</option>
                                    <option value="2">Food and Beverage</option>
                                    <option value="3">Retail, News and Gifts</option>
                                    <option value="4">Leasing</option>
                                    <option value="5">Passenger Services</option>
                                    <option value="6">Advertising</option>
                                </select>-->
                                <!--</div>-->
                                <!--</div>-->


                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="atitle">Title:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="atitle" class="form-control" id="atitle_e" placeholder="Enter Title" value="" tabindex="2" >
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="afname">First Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="afname" class="form-control" id="afname_e" placeholder="Enter First Name" value="" tabindex="4" >
                                    </div>
                                </div>


                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="aaddress1">Address 1:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="aaddress1" class="form-control" id="aaddress1_e" placeholder="Enter Address 1" value="" tabindex="6" >
                                    </div>
                                </div>



                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="accity">City:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="accity" class="form-control" id="accity_e" placeholder="Enter City" value="" tabindex="8" >
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="aczip">Zip:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="aczip" class="form-control" id="aczip_e" placeholder="Enter Zip" value="" tabindex="10" >
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="aphone1">Phone:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="aphone1" class="form-control" id="aphone1_e" placeholder="Enter Phone" value="" tabindex="12" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="aemail">E-mail:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="aemail" class="form-control" id="aemail_e" placeholder="Enter E-mail" value="" tabindex="14" >
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="apriority">Placement Number:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="priority" class="form-control" id="apriority_e" placeholder="Enter Contact's Placement Number"  tabindex="16" >
                                        <input type="hidden" name="priority_old"  id="apriority_old_e" >
                                        <input type="hidden" name="aid" value="<?php echo $this->uri->segment(3); ?>">
                                    </div>
                                </div>


                                <input type="hidden" name="MM_update" value="form1">
                                <label class="control-label col-sm-3"><span class="text-danger"></span></label>
                                <!--                                <div class="form-group">
                                                                    <div class="col-sm-8">
                                                                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('frontend/airport_edit_step_1');                                        ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>
                                                                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('wizard/airport/'.$step);                                        ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>
                                                                                              javascript:history.go(-1)      <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('wizard/airport/airport_edit_steps/1/'.$step);                                        ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>
                                                                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>
                                                                    </div>
                                                                </div>-->
                            </div>
                            <div class="col-sm-6">
                                <!--</br>--> 
                                <!--                                    <div class="form-group">
                                                                        <label class="control-label col-sm-3" for="atitle"><span class="text-danger">*</span>Title:</label>
                                                                        <div class="col-sm-8">
                                                                            <input type="text" name="atitle" class="form-control" id="atitle_e" placeholder="Enter Title" value="" tabindex="2">
                                                                        </div>
                                                                    </div>-->

                                <!--                                <div class="form-group">
                                                                    <label for="requestcompany" class="control-label col-sm-3" >&nbsp;</label>
                                                                    <div class="col-sm-11">
                                                                        <select name="mgtresponsibility" id="requestcompany_e" class="chosen form-control" tabindex="1">
                                                                            <option></option>
                                                                            <option value="1">Airport Concessions</option>
                                                                            <option value="2">Food and Beverage</option>
                                                                            <option value="3">Retail, News and Gifts</option>
                                                                            <option value="4">Leasing</option>
                                                                            <option value="5">Passenger Services</option>
                                                                            <option value="6">Advertising</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                                </div>-->
                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="acompany">Company:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="acompany" class="form-control" id="acompany_e" placeholder="Enter Company" value="" tabindex="3" >
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="alname">Last Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="alname" class="form-control" id="alname_e" placeholder="Enter Last Name" value="" tabindex="5" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="aaddress2">Address 2:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="aaddress2" class="form-control" id="aaddress2_e" placeholder="Enter Address 2" value="" tabindex="7">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="acstate">State:</label>
                                    <div class="col-sm-8">
                                        <!--<input type="text" name="acstate" class="form-control" id="acstate_e" placeholder="Enter State" value="" tabindex="9">-->
                                        <select class="chosen form-control col-xs-10 col-sm-10" id="acstate_e" name="acstate" data-placeholder="Select State" >

                                            <?php
                                            foreach ($rsStates as $key => $state):
                                                ?>

                                                <option value="<?php echo $state['state_code']; ?>">

                                                    <?php echo $state['state_name']; ?>

                                                </option>

                                                <?php
                                            endforeach;
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-sm-3" for="accountry">Country:</label>
                                    <div class="col-sm-8">
                                        <!--<input type="text" name="accountry" class="form-control" id="accountry_e" placeholder="Enter Country" value="" tabindex="11">-->
                                        <select name="accountry" id="accountry_e"  class="chosen col-sm-10 form-control" >
                                            <?php
                                            foreach ($row_rsCountries as $country):
                                                ?>
                                                <option value="<?php echo strtoupper($country['countries_name']); ?>">
                                                    <?php echo $country['countries_name']; ?>
                                                </option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="afax1">Fax:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="afax1" class="form-control" id="afax1_e" placeholder="Enter Fax" value="" tabindex="13">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="awebsite">Web site:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="awebsite" class="form-control" id="awebsite_e" placeholder="Enter Web site" value="" tabindex="15">
                                    </div>
                                </div>	

                            </div>
                        </div>

                        <div class="modal-footer">

                            <!--                                                <button type="button" onClick="empty_message();" class="btn btn-danger" id="cancel" data-dismiss="modal">
                            
                                                                                <i class="fa fa-times">&nbsp;</i>Cancel</button>        
                            
                                                                            <button type="submit"  class="btn btn-success sureedit"><i class="fa fa-check">&nbsp;</i>Save Changes
                            
                                                                            </button>     -->

                            <button type="submit" id="update_cont" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;
                            <button type="button" onClick="empty_message();" class="btn btn-danger" id="cancel" data-dismiss="modal">

                                <i class="fa fa-times">&nbsp;</i>Cancel</button> 
            <!--<a href="" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>-->

                        </div>
                    </form> 	

                </div>

            </div>

        </div>     

    </div>


    <div class="clearfix"></div>       

    <?php echo $this->load->view($footer); ?>

    <div class="clearfix"></div>

    <!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>-->

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/chosen.jquery.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>



    <script>

                                $.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="submit"], button):enabled';


                                $("#requestcompany_e").prepend("<option value='' >Please Select&nbsp;</option>").trigger('chosen:updated');
                                $("#acstate_e").prepend("<option value='' >Please Select&nbsp;</option>").trigger('chosen:updated');

                                $('#editcontact').validator();



                                jQuery(document).ready(function () {
                                    jQuery(".chosen").chosen();

                                    $('[data-toggle="tooltip"]').tooltip({
                                        animated: 'fade',
                                        placement: 'top',
                                    });
                                });


                                $('div[data-toggle="tooltip"]').tooltip({
                                    animated: 'fade',
                                    placement: 'top',
                                });

                                var seg = "<?php echo $this->uri->segment(0) ?>";

                                if (seg != '') {
                                    $('title').html("Modify Data | ARN Fact Book " + seg);
                                }
                                else {
                                    $('title').html("Modify Data | ARN Fact Book");
                                }
                                $(document).ready(function ()

                                {

                                    $('#table123').dataTable({
                                        "paging": true

                                    });

                                    $('.table0').dataTable({
                                        "paging": true,
                                        "pagingType": "simple_numbers",
                                        "dom": 'T<"clear">lfrtip',
                                        "oTableTools": {
                                            "aButtons": [
                                                "print",
                                                {
                                                    "sExtends": "collection",
                                                    "sButtonText": "Save",
                                                    "aButtons": ["copy", "csv", "xls", "pdf"]
                                                }
                                            ],
                                            "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"
                                        },
                                        "oLanguage": {
                                            "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                                        }

                                    });

                                    $('.contact2').DataTable({
                                        "lengthMenu": [5, 10, 15, 20, 25],
                                        "columnDefs": [
                                            {"width": "95px", "targets": 9},
                                            {"aTargets": [9], "bSortable": false}

                                        ],
                                        "dom": 'T<"clear">lfrtip',
                                        "oTableTools": {
                                            "aButtons": [
                                                "print",
                                                {
                                                    "sExtends": "collection",
                                                    "sButtonText": "Save",
                                                    "aButtons": ["copy", "csv", "xls", "pdf"]
                                                }
                                            ],
                                            "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"
                                        },
                                        "oLanguage": {
                                            "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                                        }

                                    });
                                    $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
                                        console.log(message);
                                    };
                                });

                                function showmodal(bit)

                                {

                                    if (bit == 1)

                                    {

                                        $("#myModal").modal("show");

                                    } else

                                    {

                                        $("#myModal").modal("hide");

                                    }

                                }

                                function delme(idd)

                                {

                                    $("#recdelid").attr("value", idd);

                                    showmodal(1);

                                }

                                $(".suredel").click(function ()

                                {

                                    var delid = $("#recdelid").val();
                                    
                                    var aid = "<?php echo $this->uri->segment(3); ?>";

                                    $(this).html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Deleteing...');

                                    $.ajax({
                                        url: "<?php echo site_url('frontend/airport_delete_step_1'); ?>",
                                        type: "POST",
                                        cache: false,
                                        data: "acid=" + delid + "&aid=" + aid + "&action=delstep1",
                                        success: function (html)

                                        {

                                            if (html == 1)

                                            {

                                                showmodal(0);

//                                                window.location = "<?php // echo current_url(); ?>/?aid=<?php // echo $_REQUEST['aid']; ?>";
                                                window.location = "<?php echo current_url(); ?>";

                                                                } else

                                                                {

                                                                    $(".modal-body").html(html);

                                                                }

                                                            }

                                                        });



                                                    });



                                                    function showmodaledit(bit)
                                                    {
                                                        if (bit == 1)
                                                        {
                                                            $("#myEditModal").modal({backdrop: 'static', keyboard: false});
                                                        } else
                                                        {
                                                            $("#myEditModal").modal("hide");
                                                        }
                                                    }



                                                    function editme(acid)
                                                    {

//            var aid1 = aid;
                                                        var acid1 = acid;
//alert(acid1);
                                                        $.ajax({
                                                            dataType: "json",
                                                            url: "<?php echo site_url('ajax/edit_contact'); ?>",
                                                            type: "POST",
                                                            cache: false,
                                                            data: "&acid=" + acid,
                                                            success: function (response) {


                                                                var $response = $(response);
//alert($response);
                                                                var trHTML1 = '';
                                                                var trHTML2 = '';
                                                                var trHTML3 = '';
                                                                var trHTML4 = '';
                                                                var trHTML5 = '';
                                                                var trHTML6 = '';
                                                                var trHTML7 = '';
                                                                var trHTML8 = '';
                                                                var trHTML9 = '';
                                                                var trHTML10 = '';
                                                                var trHTML11 = '';
                                                                var trHTML12 = '';
                                                                var trHTML13 = '';
                                                                var trHTML14 = '';
                                                                var trHTML15 = '';
                                                                var trHTML16 = '';

//                                        var ide =
//                                                $('#outlet_name').val('');
//                                        $('#company_name').val('');
//                                        //  $('#categoryide option:selected').val('');
//                                        $('#numlocation').val('');
////                                        $('#termlocatione option:selected').val('');
//                                        $('#termlocationee ').val('');
//                                        $('#sqft').val('');
//                                        $('#exp').val('');


                                                                $('#requestcompany_e').val('');
                                                                $('#afname_e').val('');
                                                                $('#aaddress1_e').val('');
                                                                $('#accity_e').val('');
                                                                $('#aczip_e').val('');
                                                                $('#aphone1_e').val('');
                                                                $('#aemail_e').val('');
                                                                $('#apriority_e').val('');
                                                                $('#apriority_old_e').val('');
                                                                $('#atitle_e').val('');
                                                                $('#acompany_e').val('');
                                                                $('#alname_e').val('');
                                                                $('#aaddress2_e').val('');
                                                                $('#acstate_e').val('');
                                                                $('#accountry_e').val('');
                                                                $('#afax1_e').val('');
                                                                $('#awebsite_e').val('');

//                                        $('#airport_contact_id').val(acid1);


                                                                $.each(response, function (i, value) {

                                                                    trHTML1 += $response[0][0];
                                                                    trHTML2 += $response[0][1];
                                                                    trHTML3 += $response[0][2];
                                                                    trHTML4 += $response[0][3];
                                                                    trHTML5 += $response[0][4];
                                                                    trHTML6 += $response[0][5];
                                                                    trHTML7 += $response[0][6];
                                                                    trHTML8 += $response[0][7];
                                                                    trHTML9 += $response[0][8];
                                                                    trHTML10 += $response[0][9];
                                                                    trHTML11 += $response[0][10];
                                                                    trHTML12 += $response[0][11];
                                                                    trHTML13 += $response[0][12];
                                                                    trHTML14 += $response[0][13];
                                                                    trHTML15 += $response[0][14];
                                                                    trHTML16 += $response[0][15];
//alert(trHTML1 + "/" + trHTML2 + "/" + trHTML3); 
                                                                });


                                                                $('#requestcompany_e').val(trHTML1);
                                                                $('#requestcompany_e ').trigger('chosen:updated');
                                                                $('#afname_e').val(trHTML2);
                                                                $('#aaddress1_e').val(trHTML3);
                                                                $('#accity_e').val(trHTML4);
                                                                $('#aczip_e').val(trHTML5);
                                                                $('#aphone1_e').val(trHTML6);
                                                                $('#aemail_e').val(trHTML7);
                                                                $('#atitle_e').val(trHTML8);
                                                                $('#acompany_e').val(trHTML9);
                                                                $('#alname_e').val(trHTML10);
                                                                $('#aaddress2_e').val(trHTML11);
                                                                $('#acstate_e').val(trHTML12);
                                                                $('#acstate_e').trigger('chosen:updated');
                                                                $('#accountry_e').val(trHTML13);
                                                                $('#accountry_e ').trigger('chosen:updated');
                                                                $('#afax1_e').val(trHTML14);
                                                                $('#awebsite_e').val(trHTML15);
                                                                $('#apriority_e').val(trHTML16);
                                                                $('#apriority_old_e').val(trHTML16);

                                                                $('#airport_contact_id').val(acid1);


                                                                showmodaledit(1);
                                                            }

                                                        });



                                                    }


                                                    function checkval_e()
                                                    {

                                                        var rfield = new Array("mgtresponsibility", "afname", "alname", "atitle", "acompany", "aaddress1", "accity", "aczip", "accountry", "acstate", 'aphone1', 'afax1', 'aemail', 'awebsite');
                                                        var msg = new Array('Select Management Responsibility', 'First Name', 'Last Name', 'Title', 'Company', 'Address', 'City', 'Zip', 'Country', 'State', 'Phone', 'Fax', 'Email', 'Website');
                                                        var errmsg = "";


                                                        for (i = 0; i < rfield.length; i++)
                                                        {
                                                            var val = document.forms["contactform_edit"][rfield[i]].value;
                                                            if (val == "" || val.replace(" ", "") == "")
                                                            {
                                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";
                                                            }
                                                        }
                                                        if (errmsg != "")
                                                        {
                                                            $(".error_e").html("<div class='alert alert-danger'>" + errmsg + "</div>");
                                                            $('html, body, modal').animate({scrollTop: $('.error_e').offset().top}, 'slow');
                                                            return false;
                                                        }
                                                        return true;
                                                    }

                                                    function empty_message() {

                                                        $(".error_e").html("");

                                                    }

    </script>



</body>

</html>