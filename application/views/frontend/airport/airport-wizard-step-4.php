<?php
echo $this->load->view($header);
?>

<title>

    <?php
    if ($this->uri->segment(0) != '') {

        echo 'Modify Data Step 4 | ARN Fact Book ' . $this->uri->segment(0);
    } else {

        echo 'Modify Data Step 4 | ARN Fact Book';
    }
    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">

            <?php /* ?><section class="col-sm-2">

              <?php foreach($modify_airport_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>

                <div class="error">

                    <?php echo $this->common->getmessage(); ?>

                </div>

                <div class="row">

                    <div class="col-sm-12" style="font-family:verdana;">
                        <div class="arport-btns-holder">

                            <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "1") {
                                        echo " btn-warning";
                                    } else {
                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 1</u><br />CONTACTS 

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "2") {
                                        echo " btn-warning";
                                    } else {
                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 2</u><br /> AIRPORT INFO

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                        if ($this->uri->segment(4) == "3") {
                                            echo " btn-warning";
                                        } else {
                                            echo " btn-primary";
                                        }
                                    ?>">

                                        <u>Step 3</u><br /> TERMINALS

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                        if ($this->uri->segment(4) == "4") {
                                            echo " btn-warning";
                                        } else {
                                            echo " btn-primary";
                                        }
                                    ?>">

                                        <u>Step 4</u><br /> RENTAL & PARKING

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                        if ($this->uri->segment(4) == "5") {
                                            echo " btn-warning";
                                        } else {
                                            echo " btn-primary";
                                        }
                                    ?>">

                                        <u>Step 5</u><br /> TENANTS

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                        if ($this->uri->segment(4) == "6") {
                                            echo " btn-warning";
                                        } else {
                                            echo " btn-primary";
                                        }
                                    ?>">

                                        <u>Step 6</u><br /> LOCATION

                                    </a>                                

                                </div>

                            </div>

                        </div></div>

                </div>

                </br>

                </br>

                <div class="row">

                    <div class="col-sm-8">

                        <p><strong>VERY IMPORTANT:</strong> <span class="text-danger">Do not use symbols like $ or , when entering numbers. All formatting will be applied automatically on the review page.</span></p>

                        <h3><p style="margin-left: 15px;"><b>Examples:</b></p></h3>

                        <div class="col-sm-4">

                            <p>Currency fields</p>

                            <p>Quantity fields</p>

                            <p>Comment fields</p>

                        </div>

                        <div class="col-sm-4">

                            <p>4.56 will appear as $4.56</p>

                            <p>1234567 will appear as 1,234,567</p>

                            <p>Type any symbols needed</p>

                        </div>

                        <div class="define-yyc-intro">


                            <h4><p class="text-default"><strong><?php echo $main_data[0]['aname'] . ' (' . $main_data[0]['IATA'] . ')'; ?></strong></p>

                                <p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>

                        </div>
                    </div>

                </div>



                </br>



                <div class="row">

                    <div class="col-sm-12">

<?php
$step = $this->uri->segment(4);

$aid = $this->uri->segment(5);
?>

                        <form method="post"  class="form-horizontal" role="form" name="contactform" action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>">

                            <div class="panel panel-primary">

                                <div class="panel-heading">

                                    <h4>Airport Car Rental 
                                        <i class="fa fa-question-circle" data-original-title="Shows the number of car rental companies at the airport, on-site and off-site, the gross revenue generated by the rental companies and the total amount of rent paid to the airport." 
                                           data-container="body"data-toggle="tooltip"></i>
                                    </h4>

                                </div>

                                <div class="panel-body">  

                                    <div class="table-responsive">

                                        <table class="table table-hover" >

                                            <tr style="border-top: none !important;">

                                                <td  style="border-top: none !important;">&nbsp;</td>

                                                <td align="center" style="border-top: none !important;"><b>Agencies</b></td>

                                                <td align="center" style="border-top: none !important;"><b>Gross Revenue</b></td> 

                                                <td align="center" style="border-top: none !important;"><b>* Gross Rentals</b></td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Car Rental On Site:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="carrentalagenciesonsite" value="<?php echo $main_data[0]['carrentalagenciesonsite']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="carrentalrevonsite" value="<?php echo $main_data[0]['carrentalrevonsite']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="carrentalrevtoaironsite" value="<?php echo $main_data[0]['carrentalrevtoaironsite']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Car Rental Off Site:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="carrentalagenciesoffsite" value="<?php echo $main_data[0]['carrentalagenciesoffsite']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="carrentalrevoffsite" value="<?php echo $main_data[0]['carrentalrevoffsite']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="carrentalrevtoairoffsite" value="<?php echo $main_data[0]['carrentalrevtoairoffsite']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Total Cars Rented:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="totalcarsrented" value="<?php echo $main_data[0]['totalcarsrented']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">&nbsp;

                                                </td>

                                                <td align="left" style="border-top: none !important;">&nbsp;

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Car Rental Sq. Ft.</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="carrentalsqft" value="<?php echo $main_data[0]['carrentalsqft']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">&nbsp;

                                                </td>

                                                <td align="left" style="border-top: none !important;">&nbsp;

                                                </td>

                                            </tr>

                                        </table>

                                        <p> <strong class="text-danger">* Gross rental amount from all car rental activities.</strong></p>

                                    </div>

                                </div>

                            </div>



                            <div class="panel panel-primary">

                                <div class="panel-heading">

                                    <h4>Airport Parking <i class="fa fa-question-circle" data-original-title="Shows all parking rates; gross revenue from parking; the number of concessionaires are located on- or off-airport; daily rate concessionaire(s) charge to parkers; concession fee paid to airport (if applicable) and total rent to the airport from parking." 
                                                           data-container="body"data-toggle="tooltip"></i></h4>

                                </div>

                                <div class="panel-body">

                                    <div class="table-responsive">

                                        <table class="table table-hover" >

                                            <tr style="border-top: none !important;">

                                                <td style="border-top: none !important;">&nbsp;</td>

                                                <td align="center" style="border-top: none !important;"><b>Spaces</b></td>

                                                <td align="center" style="border-top: none !important;"><b>Gross Revenue</b></td> 

                                                <td align="center" style="border-top: none !important;"><b>* Gross Parking</b></td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Parking:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="parkingspaces" value="<?php echo $main_data[0]['parkingspaces']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="parkingrev" value="<?php echo $main_data[0]['parkingrev']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="parkingrevtoair" value="<?php echo $main_data[0]['parkingrevtoair']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                            </tr>





                                            <tr style="border-top: none !important;">

                                                <td  style="border-top: none !important;">&nbsp;</td>

                                                <td align="center" style="border-top: none !important;"><b>Short</b></td>

                                                <td align="center" style="border-top: none !important;"><b>Long</b></td> 

                                                <td align="center" style="border-top: none !important;"><b>Economy</b></td>

                                                <td align="center" style="border-top: none !important;"><b>Valet</b></td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Hourly:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="hourlyshort" value="<?php echo $main_data[0]['hourlyshort']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="hourlylong" value="<?php echo $main_data[0]['hourlylong']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="hourlyeconomy" value="<?php echo $main_data[0]['hourlyeconomy']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="hourlyvalet" value="<?php echo $main_data[0]['hourlyvalet']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Daily:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="dailyshort" value="<?php echo $main_data[0]['dailyshort']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="dailylong" value="<?php echo $main_data[0]['dailylong']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="dailyeconomy" value="<?php echo $main_data[0]['dailyeconomy']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="dailyvalet" value="<?php echo $main_data[0]['dailyvalet']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">#Spaces</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="spacesshort" value="<?php echo $main_data[0]['spacesshort']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="spaceslong" value="<?php echo $main_data[0]['spaceslong']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="spaceseconomy" value="<?php echo $main_data[0]['spaceseconomy']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="spacesvalet" value="<?php echo $main_data[0]['spacesvalet']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                            </tr>

                                        </table>

                                        <p> <strong class="text-danger">* Gross rental amount from all car parking  activities.</strong></p>

                                    </div>

                                </div>

                            </div>



                            <div class="panel panel-primary">

                                <div class="panel-heading">

                                    <h4>Comment</h4>

                                </div>

                                <div class="panel-body"> 

                                    <p> <strong class="text-danger">Airportwide Comment</strong></p>

                                    <div class="form-group col-md-12" style="padding-right:0px;">

                                        <textarea class="form-control" name="awrevcomment" rows="2" id=""><?php echo $main_data[0]['awrevcomment']; ?></textarea>

                                    </div>

                                    <p> <strong class="text-danger">Comment field is limited to 150 characters, approximately 30 words.</strong></p>

                                </div>

                            </div>

                            <input type="hidden" name="step4_airport" value="form1">

                            <div class="form-group col-sm-12">

                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Save Change</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $this->uri->segment(5)); ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                            </div>

                        </form>

                    </div>

                </div>

            </section>

            <div class="clearfix">

                <div class="col-sm-12" style="font-family:verdana;">

                    <div class="btn-group btn-group-justified" role="group" aria-label="...">

                        <div class="btn-group" role="group">



                            <div class="col-sm-2 pull-left" style="padding-left: 0px;">

                                <label class="control-label">Next, click on </label>

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "5") {
    echo " btn-warning";
} else {
    echo " btn-primary";
}
?>">

                                    <u>Step 5</u><br /> TENANTS

                                </a>            

                            </div>

                        </div>  

                    </div>

                </div>
            </div>

        </section>



        <div class="clearfix"></div> 

<?php echo $this->load->view($footer); ?>

        <div class="clearfix"></div>

        <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        

        <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url('assets'); ?>/js/jquery.dataTables.bootstrap.js"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function () {

                $('[data-toggle="tooltip"]').tooltip({
                    animated: 'fade',
                    placement: 'top',
                });

            });
        </script>

        <script type="text/javascript">



            function checkval()

            {

                var rfield = new Array('carrentalagenciesonsite', 'carrentalrevonsite', 'carrentalrevtoaironsite', 'carrentalrevoffsite', 'carrentalrevtoairoffsite', 'carrentalagenciesoffsite', 'totalcarsrented', 'carrentalsqft', 'parkingspaces', 'parkingrev', 'parkingrevtoair', 'hourlyshort', 'hourlylong', 'hourlyeconomy', 'hourlyvalet', 'dailyshort', 'dailylong', 'dailyeconomy', 'dailyvalet', 'spacesshort', 'spaceslong', 'spaceseconomy', 'spacesvalet', 'awrevcomment');

                var msg = new Array('Car rental agencies on site', 'Car rental rev on site', 'Car rental rev to air on site', 'Car rental rev off site', 'Car rental rev to air off site', 'Car rental agencies off site', 'Total cars rented', 'Car rental Sq ft', 'Parking spaces', 'Parking rev', 'Parking rev to air', 'Hourly short', 'Hourly long', 'Hourly economy', 'Hourly Valet', 'Daily short', 'Daily long', 'Daily economy', 'Daily valet', 'Spaces short', 'Spaces long', 'Spaces economy', 'Spaces valet', 'Airportwide Comment');

                var errmsg = "";

                for (i = 0; i < rfield.length; i++)

                {

                    //alert(rfield[i]);

                    var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                    if (val == "" || val.replace(" ", "") == "")

                    {

                        errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

                    }

                }

                if (errmsg != "")

                {

                    $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                    $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                    return false;

                }

                return true;

            }

        </script>

</body>