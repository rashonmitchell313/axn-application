<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
	echo 'Airport Details | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
	echo 'Airport Details | ARN Fact Book';
}
error_reporting(0);
?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/dataTables.tableTools.css">
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>
        <section class="main-content">					     	
        	<section class="container-fluid">      
		<?php 
		
        foreach($ap as $ay):
			extract($ay);
			
			echo '<div class="panel panel-primary">
              		<div class="panel-heading"><i class="fa fa-calendar">&nbsp;</i>Year '.$ayear.'</div>
                	<div class="panel-body">';
			foreach($airport_listing as $aid=>$objQuery):
				$k1 = key($airportinfo[$aid]); ?>    
              <div class="panel panel-primary">
              	<div class="panel-heading"><i class="fa fa-plane">&nbsp;</i><?php echo $objQuery['aname'];?></div>
                <div class="panel-body">
                	<div class="page-header text-center" style="margin:0px;">
                    	<h3><?php echo $objQuery['acity'].",".$objQuery['astate']."<br/>".$objQuery['aname']."<br/>".$objQuery['IATA'];?></h3>
                    </div> 
				<?php
                if(array_key_exists('contacts', $filter))
                {
                    echo '<div class="panel panel-primary">
							<div class="panel-heading"><i class="fa fa-arrow-right">&nbsp;</i>Contact</div>
							<div class="panel-body">';		
                    foreach($contactlist[$aid] as $contact):
					if($contact['mgtresponsibilityname']=='' && $contact['alname']=='' && $contact['afname']=='' 
					&&  $contact['atitle']=='' && $contact['aaddress1']==''){
						
					echo '<p class="alert alert-danger">No record found </p> ';	
						
						} 
             	 else{
			   	 ?>	
                    	<div class="col-sm-6">
                           <div class="table-responsive">                            
                               <table class="table-bordered table-hover table-striped" width="100%">
                                  <tbody>
                                     <tr>
                                     <td colspan="2"><strong><?php echo $contact['mgtresponsibilityname'];?></strong></td>  
                                     </tr>
                                     <tr>
                                        <td width="100"><i class="fa fa-phone margin-right-5"></i>&nbsp;Contact</td>
                                        <td><?php echo $contact['alname']."&nbsp;".$contact['afname'];?></td>
                                     </tr>
                                     <tr>
                                        <td><i class="margin-right-5 fa fa-book margin-right-5"></i>&nbsp;Title</td>
                                        <td><?php echo $contact['atitle'];?></td>
                                     </tr>
                                     <tr>
                                        <td><i class="margin-right-5 fa fa-home"></i>&nbsp;Address</td>
                                        <td><?php echo $contact['aaddress1'];?></td>
                                     </tr>
                                     <tr>
                                        <td><i class="margin-right-5 fa fa-phone"></i>&nbsp;Phone</td>
                                        <td><?php echo $contact['aphone'];?></td>
                                     </tr>
                                     <tr>
                                        <td><i class="margin-right-5 fa fa-fax"></i>&nbsp;Fax</td>
                                        <td><?php echo $contact['afax'];?></td>
                                     </tr>
                                     <tr>
                                        <td><i class="margin-right-5 fa fa-envelope-o"></i>&nbsp;E-mail</td>
                                        <td><a href="mailto:<?php echo $contact['aemail'];?>"><?php echo $contact['aemail'];?></a></td>
                                     </tr>
                                     <tr>
                                        <td><i class="margin-right-5 fa fa-globe"></i>&nbsp;Web site</td>
                                        <td><a href="<?php echo $this->common->weblink($contact['awebsite']);?>" target="_new"><?php echo $contact['awebsite'];?></a></td>
                                     </tr>
                                  </tbody>
                               </table>                           
                           </div>                       
                          <div class="clearfix"></div>                 
						</div>	
					<?php } endforeach;
                    echo '</div>
					</div>';
                }
				echo '<div class="clearfix"></div>';
                if(array_key_exists('terminal', $filter))
                {?>
					<div class="panel panel-primary">
                    	<div class="panel-heading"><i class="fa fa-arrow-right">&nbsp;</i>Airport Info</div>
                    	<div class="panel-body">
                        <?php if($airportinfo[$aid][$k1]['configuration']==''){
						
					echo '<p class="alert alert-danger">No record found </p> ';	
						
						} else{ ?>
                            <div class="col-sm-6">
                               <div class="table-responsive">
                                   <table class="table table-hover table-bordered table-striped" width="100%">
                                      <tbody>
                                         <tr>
                                            <td colspan="2"><strong>Airport Info</strong></td>                                        
                                         </tr>
                                         <tr>
                                            <td width="180"><b>Airport Configuration</b></td>
                                            <td><?php echo $airportinfo[$aid][$k1]['configuration'];?></td>
                                         </tr>
                                         <tr>
                                            <td><b>Concessions Mgt. Type</b></td>
                                            <td><?php echo $airportinfo[$aid][$k1]['mgtstructure'];?></td>
                                         </tr>
                                         <tr>
                                            <td><b>Expansion Planned</b></td>
                                            <td><?php echo $airportinfo[$aid][$k1]['texpansionplanned'];?></td>
                                         </tr>
                                         <tr>
                                            <td><b>Add'l Sq. Ft</b></td>
                                            <td><?php echo number_format($airportinfo[$aid][$k1]['addsqft']);?></td>
                                         </tr>
                                         <tr>
                                            <td><b>Completed Date</b></td>
                                            <td><?php echo $airportinfo[$aid][$k1]['completedexpdate'];?></td>
                                         </tr>                                                                          
                                      </tbody>
                                   </table>                           
                               </div>                       
                              <div class="clearfix"></div>
                            </div>
                            <div class="col-sm-6">
                               <div class="table-responsive">
                                   <table class="table table-hover table-bordered table-striped" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="33%">Terminals/Conc.</th>
                                                <th width="33%">Abbr.</th>
                                                <th width="33%">Dominant Airline</th>
                                            </tr>
                                        </thead>
                                      <tbody>
                                      <?php foreach($terminal[$aid] as $tinfo): ?>
                                         <tr>
                                            <td><?php echo $tinfo['terminalname'];?></td>
                                            <td><?php echo $tinfo['terminalabbr'];?></td>
                                            <td><?php echo $tinfo['tdominantair'];?></td>                                        
                                         </tr>                                 
                                        <?php endforeach; ?>                                                                            
                                      </tbody>
                                   </table>
                               </div>                       
                               <div class="clearfix"></div>                    
							</div>
                            
                            <?php } ?>
                    	</div>
                    </div>
				<?php }
				echo '<div class="clearfix"></div>';
                if(array_key_exists('passengertraffic', $filter))
                {?>
                	<div class="panel panel-primary">
                    	<div class="panel-heading"><i class="fa fa-plane fa-spin">&nbsp;</i>Passenger Traffic</div>
                    	<div class="panel-body">
                          <div class="table-responsive">                            
                          <table class="table-bordered table-hover table-striped" width="100%">
                          	<thead>
                                <tr>
                                    <th>Terminal</th>
                                    <th>Total</th>
                                    <th>+ / - %</th>
                                    <th>Deplaning</th>
                                    <th>Enplaning</th>
                                    <th>EP Domestic</th>
                                    <th>EP Int l</th>	
                                </tr>
                            </thead>
                            <tbody>
                             <?php foreach($terminal[$aid] as $airportlist): ?>
                            	<tr>
                                	<td><?php echo $airportlist["terminalabbr"]; ?></td>
                                    <td><?php echo number_format($airportlist["tpasstraffic"]); ?></td>
                                    <td><?php echo $airportlist["tpasstrafficcompare"]; ?></td>
                                    <td><?php echo number_format($airportlist["tdeplaning"]); ?></td>
                                    <td><?php echo number_format($airportlist["tenplaning"]);  ?></td>
                                    <td><?php echo number_format($airportlist["tepdomestic"]);  ?></td>
                                    <td><?php echo number_format($airportlist["tepintl"]); ?></td>
                                </tr>
                              <?php endforeach; ?>  
                            </tbody>
                            <tfoot>
                            	<tr>
                                	<th>Total</th>
                                    <th><?php echo number_format($objQuery["apasstraffic"]); ?></th>
                                    <th><?php echo $objQuery["apasstrafficcompare"]; ?></th>
                                    <th><?php echo number_format($objQuery["adeplaning"]); ?></th>
                                    <th><?php echo number_format($objQuery["aenplaning"]);  ?></th>
                                    <th><?php echo number_format($objQuery["aepdomestic"]);  ?></th>
                                    <th><?php echo number_format($objQuery["aepintl"]); ?></th>
                                </tr>
                            </tfoot>
                          </table>
                          </div>
                          <?php
                          if(count($ptgdata[$aid]) > 0)
						  {
						  ?>
                          <div class="row">
                            <div class="col-sm-12">
                                <br />
                                 <div id="tdeplaning<?php echo $ayear.$aid; ?>"></div>
                            </div>
                            <div class="clearfix"></div>
                             <div class="col-sm-12">
                                <br />
                                 <div id="tenplaning<?php echo $ayear.$aid; ?>"></div>
                            </div>
                            <div class="clearfix"></div>
                             <div class="col-sm-12">
                                <br />
                                 <div id="tepdomestic<?php echo $ayear.$aid; ?>"></div>
                            </div>
                          </div>
                          <?php
						  }
                        if($airportinfo[$aid][$k1]["apasscomment"]!='')
							echo '<div class="well">Comments&nbsp;:&nbsp;'.$airportinfo[$aid][$k1]["apasscomment"].'</div>';
						?>
                        </div>
                    </div>
				<?php }
				echo '<div class="clearfix"></div>';
                if(array_key_exists('airportpercentages', $filter))
                {?>     
                	<div class="panel panel-primary">
                    	<div class="panel-heading"><i class="fa fa-plane fa-spin">&nbsp;</i>Airport Percentages</div>
                    	<div class="panel-body">
                        <?php if($objQuery["presecurity"]=='' && $objQuery["postsecurity"]=='' ){
						
					echo '<p class="alert alert-danger">No record found </p> ';	
						
						} else{ ?>
                        
                        	<div class="table-responsive">                            
                              <table class="table-bordered table-hover table-striped" width="100%">
                                <thead>
                                    <tr>
                                        <th>Pre/Post Security</th>
                                        <th>Business to Leisure Ratio </th>
                                        <th>OD Transfer</th>
                                        <th>Average Dwell Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $objQuery["presecurity"].'/'.$objQuery["postsecurity"]; ?></td>
                                        <td><?php echo $objQuery["ratiobusleisure"]; ?></td>
                                        <td><?php echo $objQuery["ondtransfer"]; ?></td>
                                        <td><?php echo $objQuery["avgdwelltime"].' minutes'; ?></td>
                                    </tr>
                                </tbody>
                              </table>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
				<?php }
				echo '<div class="clearfix"></div>';
                if(array_key_exists('airportwideinfo', $filter))
                {?>
                	<div class="panel panel-primary">
                    	<div class="panel-heading"><i class="fa fa-info-circle">&nbsp;</i>Airportwide Info</div>
                    	<div class="panel-body">
                        	<div class="col-md-12 media-list padding-right-none">
                                <div class="table-responsive">                                                
                                      <table class="table-bordered table-hover table-striped" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Parking</th>
                                                <th>Short</th>
                                                <th>Long</th>
                                                <th>Economy</th>
                                                <th>Valet</th>
                                                <th>Car Rentals</th>
                                                <th>Agencies</th>
                                                <th>Gross Rev</th>
                                                <th>Gross Rentals</th>
                                            </tr>
                                        </thead>
                                        <tbody>                             
                                            <tr>
                                                <td>Hourly</td>
                                                <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["hourlyshort"]) && $airportinfo[$aid][$k1]["hourlyshort"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["hourlyshort"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                   <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["hourlylong"]) && $airportinfo[$aid][$k1]["hourlylong"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["hourlylong"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td> 
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["hourlyeconomy"]) && $airportinfo[$aid][$k1]["hourlyeconomy"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["hourlyeconomy"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>   
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["hourlyvalet"]) && $airportinfo[$aid][$k1]["hourlyvalet"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["hourlyvalet"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                  <td>
                                                 Car Rental On Site
                                                 </td> 
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["carrentalagenciesonsite"]) && $airportinfo[$aid][$k1]["carrentalagenciesonsite"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["carrentalagenciesonsite"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                  <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["carrentalrevonsite"]) && $airportinfo[$aid][$k1]["carrentalrevonsite"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["carrentalrevonsite"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>  
                                                  <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["carrentalrevtoaironsite"]) && $airportinfo[$aid][$k1]["carrentalrevtoaironsite"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["carrentalrevtoaironsite"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>                                     
                                            </tr>
                                            <tr>
                                                <td>Daily</td>
                                                <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["dailyshort"]) && $airportinfo[$aid][$k1]["dailyshort"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["dailyshort"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                   <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["dailylong"]) && $airportinfo[$aid][$k1]["dailylong"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["dailylong"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td> 
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["dailyeconomy"]) && $airportinfo[$aid][$k1]["dailyeconomy"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["dailyeconomy"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>   
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["dailyvalet"]) && $airportinfo[$aid][$k1]["dailyvalet"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["dailyvalet"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                  <td>
                                                 Car Rental Off Site
                                                 </td> 
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["carrentalagenciesoffsite"]) && $airportinfo[$aid][$k1]["carrentalagenciesoffsite"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["carrentalagenciesoffsite"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                  <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["carrentalrevoffsite"]) && $airportinfo[$aid][$k1]["carrentalrevoffsite"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["carrentalrevoffsite"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>  
                                                  <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["carrentalrevtoairoffsite"]) && $airportinfo[$aid][$k1]["carrentalrevtoairoffsite"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["carrentalrevtoairoffsite"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>                                     
                                            </tr>                                 
                                            <tr>
                                                <td># Spaces</td>
                                                <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["spacesshort"]) && $airportinfo[$aid][$k1]["spacesshort"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["spacesshort"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                   <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["spaceslong"]) && $airportinfo[$aid][$k1]["spaceslong"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["spaceslong"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td> 
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["spaceseconomy"]) && $airportinfo[$aid][$k1]["spaceseconomy"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["spaceseconomy"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>   
                                                 <td>
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["spacesvalet"]) && $airportinfo[$aid][$k1]["spacesvalet"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["spacesvalet"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                                  <td>
                                                    <b>Total Cars Rented</b>
                                                 </td> 
                                                 <td colspan="3">
                                                    <?php  
                                                    if(is_numeric($airportinfo[$aid][$k1]["totalcarsrented"]) && $airportinfo[$aid][$k1]["totalcarsrented"]>0) 
                                                    { 
                                                        echo '$'.number_format($airportinfo[$aid][$k1]["totalcarsrented"], 2, '.', '');		
                                                    } else  
                                                    {
                                                         echo "$0.00";
                                                    } 
                                                    ?>
                                                 </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">&nbsp;</td>
                                                <td><strong>Car Rental Sq. Ft.</strong></td>
                                                <td colspan="3"><?php echo number_format($airportinfo[$aid][$k1]["carrentalsqft"]); ?></td>
                                            </tr>  
                                            <tr>
                                                <td colspan="2">Parking Revenue</td>
                                                <td><?php echo '$'.number_format($airportinfo[$aid][$k1]['parkingrev']); ?></td>
                                                <td>&nbsp;</td><td>&nbsp;</td>                                
                                                <td>Total Spaces</td>
                                                <td colspan="3"><?php echo number_format($airportinfo[$aid][$k1]['parkingspaces']); ?></td>
                                            </tr>                           
                                        </tbody>
                                      </table><br />
                                      <table class="table-bordered table-hover table-striped" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="33%">&nbsp;</th>
                                                <th>Revenue</th>
                                                <th>Rev. to Airport</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                                <td><strong>Passenger Services</strong></td>
                                                <td><?php echo '$'.number_format($airportinfo[$aid][$k1]["passservicesrev"]); ?></td>
                                                <td><?php echo '$'.number_format($airportinfo[$aid][$k1]["passservicesrevtoair"]); ?></td>                                    
                                            </tr>
                                            <tr>
                                                <td><strong>Advertising</strong></td>                                   
                                                <td><?php echo '$'.number_format($airportinfo[$aid][$k1]["advertisingrev"]); ?></td>
                                                <td><?php echo '$'.number_format($airportinfo[$aid][$k1]["advertisingrevtoair"]); ?></td>                                     
                                            </tr>
                                            <tr>
                                                <td><strong>Currency Exchange</strong></td>
                                                <td><?php echo '$'.number_format($airportinfo[$aid][$k1]["currencyexrev"]); ?></td>
                                                <td><?php echo '$'.number_format($airportinfo[$aid][$k1]["currencyexrevtoair"]); ?></td>                                    
                                            </tr>
                                        </tbody>
                                      </table>          
                                </div>
                    		</div>
					<?php
                    if($airportinfo[$aid][$k1]["awrevcomment"]!='')
                        echo '<div class="well">Comments&nbsp;:&nbsp;'.$airportinfo[$k1]["awrevcomment"].'</div>';
                    ?> 
                        </div>
                    </div>
				<?php }
				echo '<div class="clearfix"></div>';
				if(array_key_exists('concessiontenantdetails', $filter))
                {?>
                	<div class="panel panel-primary">
                    	<div class="panel-heading"><i class="fa fa-arrow-right">&nbsp;</i>Concession Totals - Terminal Breakdowns (Food/Beverage, Specialty Retail, News/Gifts Only)</div>
                    	<div class="panel-body">
                            <div class="col-sm-12 padding-left-none padding-right-none">
                               <div class="table-responsive">                                                     
                                   <table class="table table-hover table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="17%">Terminals</th>
                                                <th width="15%">Gross Sales</th>
                                                <th width="15%">Sales EP</th>
                                                <th width="17%">Total Rent To Airport</th>
                                                <th width="17%">Rent EP</th>
                                                <th width="17%">Current Sq. Ft</th>
                                            </tr>
                                        </thead>
                                      <tbody>
                                      <?php 
                                        $totgrall=0;$totsalesepall=0;$totrentrevall=0;$totcurrsqftall=0;$totrentepall=0;
                                        foreach($terminal[$aid] as $totairportList):
                                        $totgr=$totairportList["fbgrosssales"]+$totairportList["srgrosssales"]+$totairportList["nggrosssales"]."";
                                        $totsalesep=$totgr/$totairportList["tenplaning"];
                                        $totrentrev=$totairportList["fbrentrev"]+$totairportList["srrentrev"]+$totairportList["ngrentrev"];
                                        $totrentep=$totrentrev/$totairportList["tenplaning"];
                                        $totcurrsqft=$totairportList["fbcurrsqft"]+$totairportList["srcurrsqft"]+$totairportList["ngcurrsqft"];
                                       ?>
                                         <tr>
                                            <td><?php echo $totairportList['terminalabbr'];?></td>
                                            <td><?php echo '$'.number_format($totgr);?></td>
                                            <td><?php echo '$'.number_format($totsalesep, 2, '.', '');?></td>
                                            <td><?php echo '$'.number_format($totrentrev);?></td>
                                            <td><?php echo '$'.number_format($totrentep, 2, '.', '');?></td>
                                            <td><?php echo number_format($totcurrsqft);?></td>                                        
                                         </tr>                                 
                                        <?php
                                         $totgrall += $totgr;
                                         $totsalesepall+= $totgr/$totairportList["aenplaning"];									
                                         $totrentrevall += $totrentrev;									
                                         $totrentepall += $totrentrev/$totairportList["aenplaning"];									
                                         $totcurrsqftall += $totcurrsqft;
                                         endforeach; ?>                                                                            
                                      </tbody>
                                      <tfoot>
                                        <tr class="info">
                                            <td><strong>Total</strong></td>
                                            <td><?php echo '$'.number_format($totgrall);?></td>
                                            <td><?php echo '$'.number_format($totsalesepall, 2, '.', '');?></td>
                                            <td><?php echo '$'.number_format($totrentrevall);?></td>
                                            <td><?php echo '$'.number_format($totrentepall, 2, '.', '');?></td>
                                            <td><?php echo  number_format($totcurrsqftall);?></td>
                                        </tr>
                                      </tfoot>
                                   </table>                           
                               </div>  
                               <?php
                                if($airportinfo[$aid][$k1]["grosscomment"]!='') {  ?>
                                    <div class="well"><?php  echo $airportinfo[$aid][$k1]["grosscomment"]; ?></div>
                                <?php } ?>                     
                              <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                 <div class="col-sm-12">
                                    <br />
                                     <div id="ct<?php echo $ayear.$aid; ?>"></div>
                                 </div>
                            </div> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-primary">
                    	<div class="panel-heading"><i class="fa fa-arrow-right">&nbsp;</i>Concession Program Details - Category Breakdowns</div>
                    	<div class="panel-body">
                			<?php 
							if (count($allcat[$aid]) > 0){
							foreach($allcat[$aid] as $cat):?>
                        	<div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="5"><?php echo $cat['category']; ?></th>
                                        </tr>
                                        <tr>
                                            <th>Terminal</th>
                                            <th>Gross Sales</th>
                                            <th>Sales/EP</th>
                                            <th>Total Rent To Airport</th>
                                            <th>Total Rent To Airport</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $allfbsales = 0;
                                    $allfbsalesep = 0;
                                    $allfbrentrev = 0;
                                    $allfbcurrsqft = 0;
                                    $abbr = 0;
                                    foreach($terminal[$aid] as $t):
                                        if($cat['category']=='Food/Beverage')
                                        {
                                            $abbr = "fb";
                                        }elseif($cat['category'] == 'Specialty Retail')
                                        {
                                            $abbr = "sr";
                                        }elseif($cat['category'] == 'News/Gifts')
                                        {
                                            $abbr = "ng";
                                        }elseif($cat['category'] == 'Duty Free')
                                        {
                                            $abbr = "df";
                                        }	 
                                        if($abbr != "df")
                                        {																		
                                            $fb_salesep = $t[$abbr."grosssales"]/$t["tenplaning"];
                                            $allfbsalesep += $t[$abbr."grosssales"]/$t["aenplaning"];
                                        } else
                                        {
                                            $fb_salesep = $t[$abbr."grosssales"]/$t["tepintl"];
                                            $allfbsalesep += $t[$abbr."grosssales"]/$t["aepintl"];										
                                        }									
                                        $allfbsales += $t[$abbr."grosssales"];									
                                        $allfbrentrev += $t[$abbr."rentrev"];
                                        $allfbcurrsqft += $t[$abbr."currsqft"];
                                    ?>
                                        <tr>
                                            <td><?php echo $t['terminalabbr'];?></td>
                                            <td>$<?php echo number_format($t[$abbr.'grosssales']);?></td>
                                            <td>$<?php echo number_format($fb_salesep, 2, '.', '');?></td>
                                            <td>$<?php echo number_format($t[$abbr."rentrev"]);?></td>
                                            <td><?php echo number_format($t[$abbr."currsqft"]);?></td>
                                        </tr>
                                     <?php endforeach; ?>    
                                    </tbody>
                                    <tfoot>
                                        <tr class="info">
                                            <td><strong>Total</strong></td>
                                            <td>$<?php echo number_format($allfbsales);?></td>
                                            <td>$<?php echo number_format($allfbsalesep, 2, '.', '');?></td>
                                            <td>$<?php echo number_format($allfbrentrev);?></td>
                                            <td><?php echo number_format($allfbcurrsqft);?></td>
                                        </tr>
                                    </tfoot>
                              </table>
                        	</div> 
                       		<?php endforeach; 
							}
							else{
								echo '<p class="alert alert-danger">No record found </p> ';	
								}
							?>  
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-primary">
                    	<div class="panel-heading"><i class="fa fa-info-circle">&nbsp;</i>Concession Tenant Details</div>
                    	<div class="panel-body">
                        <?php 
						if (count($allcat[$aid])){
						
						foreach($allcat[$aid] as $cat): ?>
					   		<div class="table-responsive">
							  <table id="term_<?php echo $ayear.$objQuery['aid']; echo str_ireplace('/', '_', $cat['category']); ?>" aid="<?php echo $objQuery['aid']; ?>" cat="<?php echo $cat['category']; ?>" class="table table-bordered table-striped table-hover table-me">
                              	<thead>
                                	<tr class="info">
	                                    <th width="30%"><?php echo $cat['category'];?>(Company)</th>
										<th width="20%">Product Description</th>
										<th width="14%">Terminal </th>
										<th width="8%"># locations</th>
										<th width="17%">Sq. Ft.</th>
										<th width="21%">Expires</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>   
                              </table>
                            </div>
                         <?php endforeach;
						}
						else{
							echo '<p class="alert alert-danger">No record found </p> ';	
							}
				  echo '</div>
				  
					</div>';	
				} ?>
                </div>
              </div>
				<script>
                google.load("visualization", "1", {packages:['corechart','line']});
                google.setOnLoadCallback(drawChart<?php echo $ayear.$aid; ?>);
                function drawChart<?php echo $ayear.$aid; ?>()
                {
					
					var data = google.visualization.arrayToDataTable(<?php echo json_encode($ptgdata[$aid]); ?>);
					var options = {
						isStacked: true,		
						title: 'Terminal Passenger Traffic',
						height: 400,
  		  				width: 1100,
						isStacked: 'percent',
						hAxis: {title: 'Deplaning',  titleTextStyle: {color: '#333'}},         
						pointSize: 20		  	 
					};
		var chart = new google.visualization.AreaChart(document.getElementById('tdeplaning<?php echo $ayear.$aid; ?>'));
		chart.draw(data, options); 	
			var data = google.visualization.arrayToDataTable(<?php echo json_encode($dtgdata[$aid]); ?>);
					var options = {
						isStacked: true,		
						title: 'Terminal Passenger Traffic',
						height: 400,
  		  				width: 1100,
						isStacked: 'percent',
						hAxis: {title: 'Enplaning',  titleTextStyle: {color: '#333'}},         
						pointSize: 20		  	 
					};
		var chart = new google.visualization.AreaChart(document.getElementById('tenplaning<?php echo $ayear.$aid; ?>'));
		chart.draw(data, options); 	
			var data = google.visualization.arrayToDataTable(<?php echo json_encode($ftgdata[$aid]); ?>);
					var options = {
						isStacked: true,		
						title: 'Terminal Passenger Traffic',
						height: 400,
  		  				width: 1100,
						isStacked: 'percent',
						hAxis: {title: 'EP Domestic',  titleTextStyle: {color: '#333'}},         
						pointSize: 20		  	 
					};
		var chart = new google.visualization.AreaChart(document.getElementById('tepdomestic<?php echo $ayear.$aid; ?>'));
					
					chart.draw(data, options); 
					var data = google.visualization.arrayToDataTable(<?php echo json_encode($ctdata[$aid]);?>);
					var options = {
						isStacked: true,		
						title: 'Concession Totals - Terminal Breakdowns (Food/Beverage, Specialty Retail, News/Gifts Only)',
						height: 400,
  		  				width: 1100,
						isStacked: 'percent',
						hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},         
						pointSize: 20		  	 
					};
			var chart = new google.visualization.AreaChart(document.getElementById('ct<?php echo $ayear.$aid; ?>'));
					chart.draw(data, options);  
                }	
                </script>
            <?php endforeach;
			echo '</div>
				</div>';
		endforeach; ?>
            </section>
        </section>
        <div class="clearfix"></div>       
         <?php echo $this->load->view($footer);?>
        <div class="clearfix"></div>
    </div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url('fassests');?>/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function()
{
	//$(".table-me").dataTable({ "pageLength": 10  });
	$.each($('.table-me'), function(i, value){
		var id = $(value).attr('id');
		var aid = $(value).attr('aid');
		var cat = $(value).attr('cat');
		$("#"+id).dataTable({
			"bProcessing": true,
			"bServerSide": true,
			"sPaginationType": "full_numbers",
			"ajax": {
				"url":"<?php echo site_url('get_result'); ?>",
				"type":"POST",
				"data":{
					"cate" : cat,
					"aid": aid
				}
			},
			"aoColumns": [ 
				{ 
					"bSortable": false, 
					"bSearchable": false, 
					"mData": "outletname", 
					"sDefaultContent": "-" 
				}, 
				{ 
					"bSortable": false, 
					"bSearchable": false, 
					"mData": "productdescription", 
					"sDefaultContent": "-" 
				}, 
				{ 
					"bSortable": false, 
					"bSearchable": false, 
					"mData": "termlocation", 
					"sDefaultContent": "-" 
				}, 
				{ 
					"bSortable": false, 
					"bSearchable": false, 
					"mData": "numlocations", 
					"sDefaultContent": "-" 
				}, 
				{ 
					"bSortable": false, 
					"bSearchable": false, 
					"mData": "sqft", 
					"sDefaultContent": "-" 
				}, 
				{ 
					"bSortable": false, 
					"bSearchable": false, 
					"mData": "exp", 
					"sDefaultContent": "-" 
				}
			]
		});
	});
});
	  /*google.load("visualization", "1", {packages:['corechart','line']});
	  google.setOnLoadCallback(drawChart);
	  function drawChart()
	  {
		 var data = google.visualization.arrayToDataTable(<?php //echo json_encode($ptgdata); ?>);
		var options = {
		 isStacked: false,		
          title: 'Terminal Passenger Traffic',
		  isStacked: 'percent',
          hAxis: {title: 'Airport Terminal',  titleTextStyle: {color: '#333'}},         
		  pointSize: 20		  	 
        };
        var chart = new google.visualization.ColumnChart(document.getElementById('pt'));
       
	    chart.draw(data, options); 
		 var data = google.visualization.arrayToDataTable(<?php //echo json_encode($ctdata);?>);
		var options = {
		 isStacked: false,		
          title: 'Concession Totals - Terminal Breakdowns (Food/Beverage, Specialty Retail, News/Gifts Only)',
		  isStacked: 'percent',
          hAxis: {title: 'Airport Terminal',  titleTextStyle: {color: '#333'}},         
		  pointSize: 20		  	 
        };
        var chart = new google.visualization.AreaChart(document.getElementById('ct'));
        chart.draw(data, options);  
	  }	*/

	  //google.load("visualization", "1", {packages:['corechart','line']});
//      google.setOnLoadCallback(drawChart);	  
//      function drawChart() 
//	  {
//        var data = google.visualization.arrayToDataTable([
//          ['Director (Year)','C-A','C-B','C-D','C-E','C-F'],
//          ['Deplaning',48.4,38.9,30.9,21.9,17.9],
//          ['Enplaning',6.5,4.9,13.9,11.9,17.9],
//          ['EP Domestic',14.9,1.9,13.9,1.9,26.4],
//          ['EP Int',24.9,13.9,11.9,17.9,26.2]
//        ]);
//        var options = {
//          title: 'Passenger Traffic By Terminal',
//          vAxis: {title: 'Passenger Traffic'},
//		  connectSteps: false,
//		  tooltip: {isHtml: true, trigger: 'both',},
//          isStacked: true
//        }; 
//       var chart = new google.visualization.SteppedAreaChart(document.getElementById('chart_div'));
//        chart.draw(data, options);
//		
//		var data = new google.visualization.DataTable();
//        data.addColumn('number', 'Airport Percentages');
//        data.addColumn('number', 'Pre/Post Security');
//        data.addColumn('number', 'Business to Leisure Ratio');
//        data.addColumn('number', 'OD Transfer');
//        data.addRows([
//			[1,  37.8, 80.8, 41.8],
//			[2,  30.9, 69.5, 32.4],
//			[3,  25.4,   57, 25.7],
//			[4,  11.7, 18.8, 10.5],
//			[5,  11.9, 17.6, 10.4],
//			[6,   8.8, 13.6,  7.7],
//			[7,   7.6, 12.3,  9.6],
//			[8,  12.3, 29.2, 10.6],
//			[9,  16.9, 42.9, 14.8],
//			[10, 12.8, 30.9, 11.6],
//			[11,  5.3,  7.9,  4.7],
//			[12,  6.6,  8.4,  5.2],
//			[13,  4.8,  6.3,  3.6],
//			[14,  4.2,  6.2,  3.4]
//		  ]);
//		  var options = {
//			chart: {
//			  title: 'Airport Specifics',
//			  subtitle: 'in millions of dollars (USD)'
//			},
//			width: 900,
//			height: 500
//		  };
//		  var chart = new google.charts.Line(document.getElementById('chart_div2'));
//	      chart.draw(data, options);
//		   var data = google.visualization.arrayToDataTable([
//    ['Month', 'Food/Beverage', 'Specialty Retail', 'News/Gifts', 'Advertising'],
//    ["CKE Restaurants, Inc.",  1650,      938,         522,             998],
//    ["Host International, Inc.",  1305,      1120,        599,             1268]
//  ]);
//
//  var options = {
//    title : 'Concession Tenant Details',
//    vAxis: {title: "#Sq Ft"},
//    hAxis: {title: "Companies"},
//    seriesType: "bars",
//    series: {5: {type: "line"}}
//  };
//
//  var chart = new google.visualization.ComboChart(document.getElementById('chart_div3'));
//  chart.draw(data, options);		
//      }   
	

	     

    </script> 
</body>
</html>
