<?php

echo $this->load->view($header);

?>

<title>

    <?php

    if ($this->uri->segment(0) != '') {

        echo 'Modify Data Step 3 | ARN Fact Book ' . $this->uri->segment(0);

    } else {

        echo 'Modify Data Step 3 | ARN Fact Book';

    }

    ?>

</title>

<!--<link rel="stylesheet" type="text/css" href="<?php // echo base_url('fassests');  ?>/css/jquery.dataTables.css">-->

<!--<link rel="stylesheet" type="text/css" href="<?php // echo base_url('fassests');  ?>/css/dataTables.tableTools.css">-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/bootstrap-tour.min.css">

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">

<!--            <section class="col-sm-2">

            <?php // foreach ($modify_airport_adds as $adds): ?>

                    <a href="<?php // echo $adds['adlink'];  ?>" target="_new"><img src="<?php // echo base_url();  ?><?php // echo $adds['imglink'];  ?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

            <?php // endforeach; ?>				

            </section>		     	-->

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>



<!--<a href="#" class="scrolldown btn btn-danger"><i class="fa fa-arrow-up"></i> Go bottom</a>-->



                <div class="error" id="error">

                    <?php echo $this->common->getmessage(); ?>

                </div>

                <div class="row">

                    <div class="col-sm-12" style="font-family:verdana;">

                        <div class="btn-group btn-group-justified" role="group" aria-label="...">

                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $this->uri->segment(4)); ?>" class="btn text-uppercase btn-primary">

                                    <u>Step 1</u><br />CONTACTS 

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $this->uri->segment(4)); ?>" class="btn text-uppercase btn-primary">

                                    <u>Step 2</u><br /> AIRPORT INFO

                                </a>                                

                            </div>



                            <div class="btn-group" id="starttour" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $this->uri->segment(4)); ?>" class="btn text-uppercase btn-warning">

                                    <u>Step 3</u><br /> TERMINALS

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $this->uri->segment(4)); ?>" class="btn text-uppercase btn-primary">

                                    <u>Step 4</u><br /> RENTAL & PARKING

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $this->uri->segment(4)); ?>" class="btn text-uppercase btn-primary">

                                    <u>Step 5</u><br /> TENANTS

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $this->uri->segment(4)); ?>" class="btn text-uppercase btn-primary">

                                    <u>Step 6</u><br /> LOCATION

                                </a>                                

                            </div>

                        </div>

                    </div>

                </div>		

                </br>

                </br>

                <div class="row">

                    <div class="col-sm-8">

                        <p><strong>VERY IMPORTANT:</strong> <span class="text-danger">Do not use symbols like $ or , when entering numbers. All formatting will be applied automatically on the review page.</span></p>

                        <h3><p style="margin-left: 15px;"><b>Examples:</b></p></h3>

                        <div class="col-sm-4">

                            <p>Currency fields</p>

                            <p>Quantity fields</p>

                            <p>Comment fields</p>

                        </div>

                        <div class="col-sm-4">

                            <p>4.56 will appear as $4.56</p>

                            <p>1234567 will appear as 1,234,567</p>

                            <p>Type any symbols needed</p>

                        </div>

                        </br>



                        </br></br></br>

                        </br></br>

                        <h4><p class="text-default"><strong><?php echo $main_data[0]['aname'] . ' (' . $main_data[0]['IATA'] . ')'; ?></strong></p>

                            <p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>

                    </div>

                </div>



                </br>



                <div class="row">

                    <div class="col-sm-12">

                        <div class="panel panel-primary">

                            <div class="panel-heading">

                                <h4>Airport Info</h4>

                            </div>

                            <div class="panel-body">

                                <?php

                                $cid = $this->uri->segment(4);

                                $tid = $this->uri->segment(5);

                                ?>

                                <form method="post"  class="form-horizontal" role="form" name="step1_step2_step3"   action="<?php echo site_url('wizard/airport/airport_edit_step_3') . '/' . $cid . '/' . $tid; ?>">   

                                    <div class="table-responsive">

                                        <h4><p class="text-danger"> Please follow steps 1, 2 and 3 for each terminal </p></h4>

                                        <p> <strong class="text-danger">1. Select a Terminal to Modify Data:</strong>(Click on the icon <b><i class="fa fa-edit"></i></b> Edit Terminal next to desired terminal)  </p>

                                        <table class="table table-hover" >

                                            <thead>

                                                <tr>

                                                    <td><b>Terminal</b></td>

                                                    <td><b>Total</b></td>

                                                    <td><b>+/- %</b></td>

                                                    <td><b>Deplaning</b></td>

                                                    <td><b>Enplaning</b></td>

                                                    <td><b>EP Domestic</b></td>

                                                    <td><b>EP Int'l</b></td> 

                                                </tr>

                                            </thead>

                                            </tbody>



                                            <?php

                                            $aid = $this->uri->segment(4);

                                            foreach ($wideInfo_traffic as $wideInfo_traffic) {

                                                ?>



                                                <tr>



                                                    <td><a href='<?php echo site_url('wizard/airport/airport_edit_step_3'); ?>/<?php echo $aid; ?>/<?php echo $wideInfo_traffic['tid']; ?>' target="_new" title='Edit "<?php echo $wideInfo_traffic['terminalabbr']; ?>"'><i class="fa fa-edit"></i> &nbsp;Edit</a>



                                                        <?php echo $wideInfo_traffic['terminalabbr']; ?></td>

                                                    <td><?php

                                                        if ($wideInfo_traffic['tpasstraffic'] != '') {

                                                            echo number_format($wideInfo_traffic['tpasstraffic']);

                                                        }

                                                        $tot_tpasstraffic+= $wideInfo_traffic['tpasstraffic'];

                                                        ?></td>



                                                    <td><?php

                                                        echo $wideInfo_traffic['tpasstrafficcompare'];



                                                        $tot_tpasstrafficcompare+= $wideInfo_traffic['tpasstrafficcompare'];

                                                        ?></td>



                                                    <td><?php

                                                        if ($wideInfo_traffic['tdeplaning'] != '') {

                                                            echo number_format($wideInfo_traffic['tdeplaning']);

                                                        }

                                                        $tot_tdeplanin+= $wideInfo_traffic['tdeplaning'];

                                                        ?></td>



                                                    <td ><?php

                                                        if ($wideInfo_traffic['tenplaning'] != '') {

                                                            echo number_format($wideInfo_traffic['tenplaning']);

                                                        }

                                                        $tot_tenplaning+= $wideInfo_traffic['tenplaning'];

                                                        ?></td>



                                                    <td><?php

                                                        if ($wideInfo_traffic['tepdomestic'] != '') {

                                                            echo number_format($wideInfo_traffic['tepdomestic']);

                                                        }

                                                        $tot_tepdomestic+= $wideInfo_traffic['tepdomestic'];

                                                        ?></td>



                                                    <td ><?php

                                                        if ($wideInfo_traffic['tepintl'] != '') {

                                                            echo number_format($wideInfo_traffic['tepintl']);

                                                        }

                                                        $tot_tepintl+= $wideInfo_traffic['tepintl'];

                                                        ?></td>

                                                </tr>

                                            <?php } ?>



                                            <tr class="info">



                                                <td><b>Totals</b></td>



                                                <td><?php echo number_format($tot_tpasstraffic); ?></td>



                                                <!--<td><?php // echo $tot_tpasstrafficcompare;         ?></td>-->



                                                <td><?php echo $wideInfo_traffic['apasstrafficcompare']; ?></td>



                                                <td><?php echo number_format($tot_tdeplanin); ?></td>



                                                <td><?php echo number_format($tot_tenplaning); ?></td>



                                                <td><?php echo number_format($tot_tepdomestic); ?></td>



                                                <td><?php echo number_format($tot_tepintl); ?></td>

                                            </tr>



                                            </tbody>



                                        </table>

                                    </div>



                                    <div class="table-responsive">

                                    <!--<h4><p class="text-danger"> 2. You are modifying data for <?php //  echo $wideInfo_traffic['terminalabbr'].' '.$wideInfo_traffic['terminalname'];                                    ?> Concourse A : </p></h4>-->

                                        <h4><p class="text-danger"> 2. You are modifying data for <b data-toggle="tooltip" title="Terminal Name"><?php echo '(' . $ste1_step2_step3[0]['terminalabbr'] . ') ' . $ste1_step2_step3[0]['terminalname']; ?></b> : </p></h4>



                                        <table class="table" >

                                            <thead>

                                                <tr>

                                                    <td><b>Terminal <?php echo $ste1_step2_step3[0]['terminalabbr'] ?>:</b></td>

                                                    <td><b>Current Sq. Ft.</b></td>

                                                    <td><b>Gross Sales</b></td>

                                                    <td><b>Sales/EP</b></td>

                                                    <td><b>Gross Rentals</b></td>

                                                </tr>

                                            </thead>

                                            </tbody>

                                            <tr>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <label for="" class="col-sm-8 control-label">Food & Beverage:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="fbcurrsqft" value="<?php echo $ste1_step2_step3[0]['fbcurrsqft']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="fbgrosssales" value="<?php echo $ste1_step2_step3[0]['fbgrosssales']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="fbsalesep" value="<?php // echo number_format($ste1_step2_step3[0]['fbgrosssales'] / $ste1_step2_step3[0]['tenplaning'], 2);                              ?>" class="form-control" id="" readonly >-->

                                                        <input type="text" name="fbsalesep" value="<?php echo number_format($ste1_step2_step3[0]['fbsalesep'], 2); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="fbrentrev" value="<?php echo $ste1_step2_step3[0]['fbrentrev']; ?>" class="form-control" id=""  >

                                                    </div>

                                                </td>



                                            </tr>



                                            <tr>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <label for="" class="col-sm-8 control-label">Specialty Retail:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="srcurrsqft" value="<?php echo $ste1_step2_step3[0]['srcurrsqft']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="srgrosssales" value="<?php echo $ste1_step2_step3[0]['srgrosssales']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="srsalesep" value="<?php // echo number_format($ste1_step2_step3[0]['srgrosssales'] / $ste1_step2_step3[0]['tenplaning'], 2);                              ?>" class="form-control" id="" readonly  >-->

                                                        <input type="text" name="srsalesep" value="<?php echo number_format($ste1_step2_step3[0]['srsalesep'], 2); ?>" class="form-control" id="" readonly  >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="srrentrev" value="<?php echo $ste1_step2_step3[0]['srrentrev']; ?>" class="form-control" id=""  >

                                                    </div>

                                                </td>

                                            </tr>



                                            <tr>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <label for="" class="col-sm-8 control-label">News & Gifts:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="ngcurrsqft" value="<?php echo $ste1_step2_step3[0]['ngcurrsqft']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="nggrosssales" value="<?php echo $ste1_step2_step3[0]['nggrosssales']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="ngsalesep" value="<?php // echo number_format($ste1_step2_step3[0]['nggrosssales'] / $ste1_step2_step3[0]['tenplaning'], 2);                              ?> " class="form-control" id="" readonly >-->

                                                        <input type="text" name="ngsalesep" value="<?php echo number_format($ste1_step2_step3[0]['ngsalesep'], 2); ?> " class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="ngrentrev" value="<?php echo $ste1_step2_step3[0]['ngrentrev']; ?>" class="form-control" id=""  >

                                                    </div>

                                                </td>

                                            </tr>



                                            <tr>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <label for="" class="col-sm-8 control-label">Duty Free:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="dfcurrsqft" value="<?php echo $ste1_step2_step3[0]['dfcurrsqft']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="dfgrosssales" value="<?php echo $ste1_step2_step3[0]['dfgrosssales']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="dfsalesep" value="<?php // echo number_format($ste1_step2_step3[0]['dfgrosssales'] / $ste1_step2_step3[0]['tenplaning'], 2);                                ?>" class="form-control" id=""  readonly >-->

                                                        <input type="text" name="dfsalesep" value="<?php echo number_format($ste1_step2_step3[0]['dfsalesep'], 2); ?>" class="form-control" id=""  readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="dfrentrev" value="<?php echo $ste1_step2_step3[0]['dfrentrev']; ?>" class="form-control" id=""  >

                                                    </div>

                                                </td>

                                            </tr>



                                            <tr  style="border-top: none !important;" colspan="6">

                                                <td colspan="6">



                                                </td>

                                            </tr>

                                            <tr  style="border-top: none !important;" colspan="6">

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<label for="" class="col-sm-8 control-label">Duty Free:</label>-->

                                                        <label for="" class="col-sm-8 control-label">Dominant Airline:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;" >

                                                    <div class="col-sm-12">

                                                        <input type="text" name="tdominantair" value="<?php echo $ste1_step2_step3[0]['tdominantair']; ?>" class="form-control" id="" >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;" colspan="3">



                                                </td>

                                            </tr>



                                            </tbody>



                                        </table>

                                    </div>



                                    <div class="table-responsive" >

                                        <h4><p class="text-danger"> 3. Totals for terminal <?php echo '(' . $ste1_step2_step3[0]['terminalabbr'] . ') ' . $ste1_step2_step3[0]['terminalname']; ?> :</p></h4>

                                        <table class="table" >

                                            <thead>

                                                <tr>

                                                    <td><b>Terminal</b></td>

                                                    <td><b>Terminal Passenger Traffic</b></td>

                                                    <td><b>+/- %</b></td>

                                                    <td><b>Deplaning</b></td>

                                                    <td><b>Enplaning</b></td>

                                                    <td><b>EP Domestic</b></td>

                                                    <td><b>EP Int'l</b></td> 

                                                </tr>

                                            </thead>

                                            </tbody>

                                            <tr>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="terminalabbr" value="<?php echo $ste1_step2_step3[0]['terminalabbr']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="tpasstraffic" value="<?php echo number_format($ste1_step2_step3[0]['tpasstraffic']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="tpasstrafficcompare" value="<?php echo $ste1_step2_step3[0]['tpasstrafficcompare']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="tdeplaning" value="<?php echo $ste1_step2_step3[0]['tdeplaning']; ?>" class="form-control" id=""  >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="tenplaning" value="<?php echo number_format($ste1_step2_step3[0]['tenplaning']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left"  style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="tepdomestic" value="<?php echo $ste1_step2_step3[0]['tepdomestic']; ?>" class="form-control" id=""  >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="tepintl" value="<?php echo $ste1_step2_step3[0]['tepintl']; ?>" class="form-control" id=""  >

                                                    </div>

                                                </td>



                                            </tr>

                                            <tr  style="border-top: none !important;" colspan="7">

                                                <td colspan="5">

                                            <center>type in symbols example: +1.45 %</center>

                                            </td>

                                            </tr>



                                            </tbody>



                                        </table>

                                    </div>





                                    <input type="hidden" name="step1_step2_step3" value="form1">

                                    <div class="form-group">

                                        &nbsp;&nbsp;<button type="submit" class="btn btn-success" id="tour-sep2"><i class="fa fa-check"></i>&nbsp;Save Change</button>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="showmodal()" id="helpvideo"><i class="fa fa-question-circle fa-2x" data-toggle="tooltip" title="Please Click this icon & watch video for help about Update data."></i></a>

                                    </div>

                                </form>



                            </div>



                        </div>

                        <form method="post"  class="form-horizontal" role="form" name="contactform"  onSubmit="return checkval();"  action="<?php echo site_url('wizard/airport/airport_edit_step_3') . '/' . $cid . '/' . $tid; ?>">   

                            <div class="panel panel-primary">

                                <div class="panel-heading">

                                    <h4>Airport Totals</h4>

                                </div>

                                <div class="panel-body">

                                    <div class="table-responsive">

                                        <h4><p class="text-danger"> 4. Airport Passenger Traffic Totals</p></h4>



                                        <table class="table" >

                                            <thead>

                                                <tr>

                                                    <td><b>IATA</b></td>

                                                    <td><b>Passenger Traffic</b></td>

                                                    <td><b>+/- %</b> &nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="A comparison of numbers from the current Fact Book year to the previous. Currently 2016 numbers compared to 2015. All symbols should be typed included. i.g. +2.4 % or -3.1 % ."></i></td>

                                                    <td><b>Deplaning</b>

                                                        <i class="fa fa-question-circle" data-original-title="The number of people disembarking an airline and arriving through the airport to baggage claim or ground transportation." 

                                                           data-container="body"data-toggle="tooltip"></i>

                                                    </td>

                                                    <td><b>Enplaning</b>

                                                        <i class="fa fa-question-circle" data-original-title="Passengers departing the airport on airlines leaving the airport to their predetermined destinations." 

                                                           data-container="body"data-toggle="tooltip"></i>

                                                    </td>

                                                    <td><b>EP Domestic</b></td>

                                                    <td><b>EP Int'l</b></td> 

                                                </tr>

                                            </thead>

                                            </tbody>

                                            <tr>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="IATA" value="<?php echo $wideInfo_traffic['IATA']; ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="apasstraffic" value="<?php echo number_format($wideInfo_traffic['apasstraffic']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="apasstrafficcompare" value="<?php echo $wideInfo_traffic['apasstrafficcompare']; ?>" class="form-control" id="">

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="adeplaning" value="<?php echo number_format($wideInfo_traffic['adeplaning']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="aenplaning" value="<?php echo number_format($wideInfo_traffic['aenplaning']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left"  style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="aepdomestic" value="<?php echo number_format($wideInfo_traffic['aepdomestic']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="aepintl" value="<?php echo number_format($wideInfo_traffic['aepintl']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>



                                            </tr>

                                            <tr  style="border-top: none !important;" colspan="7">

                                                <td colspan="5">

                                            <center>type in symbols example: +1.45 %</center>

                                            </td>

                                            </tr>



                                            </tbody>



                                        </table>

                                    </div>



                                    <p> <strong class="text-danger">Passenger Traffic Comment</strong></p>

                                    <div class="form-group">

                                        <div class="col-sm-12">

                                            <textarea class="form-control" name="apasscomment" rows="2"   id=""><?php echo $wideInfo_traffic['apasscomment']; ?></textarea>

                                        </div>

                                    </div>



                                    <h4><p class="text-danger">5. Airport totals from all Terminals combined:</p></h4>

                                    <div class="table-responsive">

                                        <table class="table table-hover" >

                                            <tr style="border-top: none !important;">

                                                <td  style="border-top: none !important;">&nbsp;</td>

                                                <td  style="border-top: none !important;"><b>Current Sq. Ft.</b></td>

                                                <td  style="border-top: none !important;"><b>Gross Sales</b></td> 

                                                <td  style="border-top: none !important;"><b>Sales/EP</b> &nbsp; <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="The estimated amount of money each departing passenger is spending before boarding their flight. This figure is a standard measure of performance in the industry and is derived by dividing the total gross sales figure by the number of enplaning passengers."></i></td> 

                                                <td  style="border-top: none !important;"><b>Gross Rentals</b>&nbsp; <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title=" The amount of revenue received by the airport in the form of rent paid by concessionaires. "></i></td> 

                                                <td  style="border-top: none !important;"><b>Rent/EP</b></td> 

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Food & Beverage:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="afbcurrsqft" value="<?php echo number_format($wideInfo_traffic['afbcurrsqft']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="afbgrosssales" value="<?php echo number_format($wideInfo_traffic['afbgrosssales']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="afbsalesep" value="<?php // echo number_format($wideInfo_traffic['fbgrosssales'] / $wideInfo_traffic['tenplaning'], 2);                                ?>" class="form-control" id="" readonly >-->

                                                        <input type="text" name="afbsalesep" value="<?php echo $wideInfo_traffic['afbsalesep']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="afbrentrev" value="<?php echo number_format($wideInfo_traffic['afbrentrev']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="afbrentep" value="<?php echo $wideInfo_traffic['afbrentep']; ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Specialty Retail:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="asrcurrsqft" value="<?php echo number_format($wideInfo_traffic['asrcurrsqft']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="asrgrosssales"   value="<?php echo number_format($wideInfo_traffic['asrgrosssales']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="asrsalesep"   value="<?php // echo number_format($wideInfo_traffic['asrgrosssales'] / $wideInfo_traffic['tenplaning'], 2);                                ?>" class="form-control" id="" readonly >-->

                                                        <input type="text" name="asrsalesep"   value="<?php echo $wideInfo_traffic['asrsalesep']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="asrrentrev"   value="<?php echo number_format($wideInfo_traffic['asrrentrev']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="asrrentep"   value="<?php echo $wideInfo_traffic['asrrentep']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">News & Gifts:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="angcurrsqft" value="<?php echo number_format($wideInfo_traffic['angcurrsqft']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="anggrosssales" value="<?php echo number_format($wideInfo_traffic['anggrosssales']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="angsalesep" value="<?php // echo number_format($wideInfo_traffic['anggrosssales'] / $wideInfo_traffic['tenplaning'], 2);                                ?>" class="form-control" id="" readonly>-->

                                                        <input type="text" name="angsalesep" value="<?php echo $wideInfo_traffic['angsalesep']; ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="angrentrev" value="<?php echo number_format($wideInfo_traffic['angrentrev']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="angrentep" value="<?php echo $wideInfo_traffic['angrentep']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Duty Free:</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="adfcurrsqft" value="<?php echo number_format($wideInfo_traffic['adfcurrsqft']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="adfgrosssales" value="<?php echo number_format($wideInfo_traffic['adfgrosssales']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="adfsalesep" value="<?php // echo number_format($wideInfo_traffic['adfgrosssales'] / $wideInfo_traffic['tenplaning'], 2);                                ?>" class="form-control" id="" readonly>-->

                                                        <input type="text" name="adfsalesep" value="<?php echo $wideInfo_traffic['adfsalesep']; ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="adfrentrev" value="<?php echo number_format($wideInfo_traffic['adfrentrev']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="adfrentep" value="<?php echo $wideInfo_traffic['adfrentep']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td colspan="7">&nbsp;



                                                </td>

                                            </tr>

                                            <tr>

                                                <td colspan="7">&nbsp;



                                                </td>

                                            </tr>

                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Airport Totals (Excluding Duty Free):</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="acurrsqft" value="<?php echo number_format($wideInfo_traffic['acurrsqft']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="aconcessiongrosssales" value="<?php echo number_format($wideInfo_traffic['aconcessiongrosssales']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <!--<input type="text" name="asalesep" value="<?php // echo ($wideInfo_traffic['adfgrosssales'] / $wideInfo_traffic['tenplaning']) + ($wideInfo_traffic['srgrosssales'] / $wideInfo_traffic['tenplaning']) + ($wideInfo_traffic['nggrosssales'] / $wideInfo_traffic['tenplaning']) + ($wideInfo_traffic['adfgrosssales'] / $wideInfo_traffic['tenplaning']);                                ?>" class="form-control" id="" readonly >-->

                                                        <input type="text" name="asalesep" value="<?php echo $wideInfo_traffic['asalesep']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="arentrev" value="<?php echo number_format($wideInfo_traffic['arentrev']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="arentep" value="<?php echo $wideInfo_traffic['arentep']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                            </tr>



                                            <tr>

                                                <td align="right" style="border-top: none !important;">							

                                                    <div class="form-group">

                                                        <label for="inputEmail3" class="col-sm-12 control-label">Airport Totals (Including Duty Free):</label>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="acurrsqft_incl_df" value="<?php echo number_format($wideInfo_traffic['acurrsqft_incl_df']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="aconcessiongrosssales_incl_df" value="<?php echo number_format($wideInfo_traffic['aconcessiongrosssales_incl_df']); ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="asalesep_incl_df" value="<?php echo $wideInfo_traffic['asalesep_incl_df']; ?>" class="form-control" id="" readonly>

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="arentrev_incl_df" value="<?php echo number_format($wideInfo_traffic['arentrev_incl_df']); ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                                <td align="left" style="border-top: none !important;">

                                                    <div class="col-sm-12">

                                                        <input type="text" name="arentep_incl_df" value="<?php echo $wideInfo_traffic['arentep_incl_df']; ?>" class="form-control" id="" readonly >

                                                    </div>

                                                </td>

                                            </tr>

                                        </table>

                                    </div>

                                    <hr>

                                    <p> <strong class="text-danger">Gross Sales Comment</strong></p>

                                    <div class="form-group">

                                        <div class="col-sm-12">

                                            <textarea class="form-control" name="grosscomment" rows="2"    id=""><?php echo $wideInfo_traffic['grosscomment']; ?></textarea>

                                        </div>

                                    </div>

                                    <br/>

                                    <input type="hidden" name="step4_step5_step6" value="form1">

                                    <div class="form-group col-sm-12">

                                        <!--<button type="submit" class="btn btn-success" id="utotal" data-toggle="tooltip" title="Must Click here to Update Airport's Total data" ><i class="fa fa-check"></i>&nbsp;Update Total</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $cid); ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="showmodal()" id="helpvideo"><i class="fa fa-question-circle fa-2x" data-toggle="tooltip" title="Please Click this icon & watch video for help about Update data."></i></a>-->

                                        <button type="submit" class="btn btn-success" id="utotal" data-toggle="tooltip"  ><i class="fa fa-check"></i>&nbsp;Update Total</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $cid); ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="showmodal()" id="helpvideo"><i class="fa fa-question-circle fa-2x" data-toggle="tooltip" title="Please Click this icon & watch video for help about Update data."></i></a>

                                    </div>



                                </div>	

                            </div>	







                            <div class="space-4"></div> 

                        </form>

                    </div>

                </div>





                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">



                    <div class="modal-dialog modal-lg">



                        <div class="modal-content">



                            <div class="modal-header">



                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>



                                </button>



                                <h4 class="modal-title" id="myModalLabel">Update Airport's Totals (Helping Video)</h4>



                            </div>



                            <div class="modal-body">



                                <iframe width="100%" height="500" src="https://www.youtube.com/embed/I2zAi9LMVO4?rel=0" frameborder="0" allowfullscreen></iframe>

                                <!--<iframe width="870" height="500" src="https://www.youtube.com/embed/jSdglW1utg8" frameborder="0" allowfullscreen></iframe>-->



                                <!--Are you sure want to delete this?-->



                            </div>



                            <!--                                    <div class="modal-footer">

                            

                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">

                            

                                                                        <i class="fa fa-times">&nbsp;</i>Cancel</button>        

                            

                                                                    <button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete

                            

                                                                    </button>        

                            

                                                                </div>-->



                        </div>



                    </div>



                </div> 



            </section>

            <!--<div class="col-sm-12" style="font-family:verdana;">

                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                                                      <div class="btn-group" role="group">

                                                                      

                                                                      <div class="col-sm-2 pull-left" style="padding-left: 0px;">

            <label class="control-label">Next, click on </label>

            <a  href="<?php // echo site_url('wizard/airport/airport_edit_steps/4/'.$this->uri->segment(5));                                    ?>" class="btn text-uppercase<?php // if($this->uri->segment(5)=="4") { echo " btn-warning"; } else { echo " btn-primary"; }                                    ?>">

                                                            <u>Step 4</u><br /> RENTAL & PARKING

                                            </a>               

                                                                            </div>

                                          </div>  

                                        </div>

                </div>-->

        </section>







        <div class="clearfix"></div>       

        <?php echo $this->load->view($footer); ?>

        <div class="clearfix"></div>

        <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        

        <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

        <!--<script src="<?php // echo base_url('fassests');  ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>-->

        <!--<script src="<?php // echo base_url('assets');  ?>/js/jquery.dataTables.bootstrap.js"></script>-->

<!--        <script src="<?php // echo base_url('fassests');  ?>/js/dataTables.tableTools.js" type="text/javascript"></script>-->

        <script src="<?php echo base_url('fassests'); ?>/js/bootstrap-tour.min.js" type="text/javascript"></script>

        <script>





        </script>

        <script type="text/javascript">



















//            $('div[data-toggle="tooltip"]').tooltip({

//                animated: 'fade',

//                placement: 'top',

//            });





//$("#helpvideo").click(function({

//    alert('test');

//    $("#myModal").modal("show");

//}));





            function showmodal() {

//     alert('test');

                $("#myModal").modal("show");

            }





            function checkval()

            {

                var rfield = new Array("mgtresponsibility", "afname", "alname", "atitle", "acompany", "aaddress1", "accity", "aczip", "accountry", "acstate", 'aphone1', 'afax1', 'aemail', 'awebsite');

                var msg = new Array('Select Management Responsibility', 'First Name', 'Last Name', 'Title', 'Company', 'Address', 'City', 'Zip', 'Country', 'State', 'Phone', 'Fax', 'Email', 'Website');

                var errmsg = "";

                for (i = 0; i < rfield.length; i++)

                {

                    //alert(rfield[i]);

                    var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                    if (val == "" || val.replace(" ", "") == "")

                    {

                        errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

                    }

                }

                if (errmsg != "")

                {

                    $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                    $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                    return false;

                }

                return true;

            }

        </script>



        <script type="text/javascript">

            $(document).ready(function () {





                var IATA = "<?php echo $main_data[0]['IATA'] ?>";



                // Instance the tour

                var tour = new Tour({

                    steps: [

                        {

                            element: "#starttour",

                            title: "Welcom to the edit terminal of (<b>" + IATA + "</b>).",

                            content: "Complete this Tour before Update the data of Terminal for accurate results against you inputs. This tour helps you about; <b>“How to update the data of Terminals and also for the Airport (<b>" + IATA + "</b>)?”</b>"

                        },

                        {

                            element: "#tour-sep2",

                            title: "Add records for the terminal.",

                            content: "Fill the data in above all those fields you want to be modify/update and after that click on <b>“Save Changes”</b>."

                        },

                        {

                            element: "#utotal",

                            title: "Must Click here to Update <b>" + IATA + "'s</b> Total data.",

                            content: "After save the terminal’s data must click on the button named <b>“Update Totals”</b> and after click on this button your filled data has been updated for all other sections of <b>" + IATA + "</b>."

                        }

                    ]});



// Initialize the tour

                tour.init();



// Start the tour

                tour.start();







                $('[data-toggle="tooltip"]').tooltip({

                    animated: 'fade',

                    placement: 'top'

                });



//                function hasClass(element, cls) {

//

//                    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;

//

//                }

//

//                var el = document.getElementById('messg');

//

//                if (hasClass(el, 'alert-success')) {

//

//                    $("html, body").animate({scrollTop: $(document).height()}, 5000);

//

//                    $('#utotal').focus()

//

//                }





            });

        </script>

</body>