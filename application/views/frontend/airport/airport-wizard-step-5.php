<?php 

	echo $this->load->view($header);

?>

<title>

<?php 

if($this->uri->segment(0)!='')

{

echo 'Modify Data Step 5 | ARN Fact Book '.$this->uri->segment(0);

}

else 

{

echo 'MODIFY DATA Step 5 | ARN Fact Book';

}

?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/dataTables.tableTools.css">

</head>

<body>

	<div class="main-wrapper">

    	<?php echo $this->load->view($menu);?>

        <div class="clearfix"></div>

        <section class="main-content">

				<?php /*?><section class="col-sm-2">

<?php foreach($modify_airport_adds as $adds): ?>

<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

<?php endforeach; ?>				

				</section><?php */?>		     	

        	<section class="container-fluid">

                    <?php echo $this->load->view('frontend/include/breadcrumb'); ?>

		 <div class="error">

		<?php echo $this->common->getmessage();?>

                             </div>

	   <div class="row">

                 	<div class="col-sm-12" style="font-family:verdana;">

                    	<div class="arport-btns-holder">
                        
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">

                              <div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="1") { echo " btn-primary"; } else { echo " btn-primary"; } ?>">

                                		<u>Step 1</u><br />CONTACTS 

                                </a>                                

                              </div>

							  

							  <div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="2") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">

                                		<u>Step 2</u><br /> AIRPORT INFO

                                </a>                                

                              </div>

							  

							  <div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="3") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">

                                		<u>Step 3</u><br /> TERMINALS

                                </a>                                

                              </div>

							  

							  <div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(5)=="4") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">

                                		<u>Step 4</u><br /> RENTAL & PARKING

                                </a>                                

                              </div>

							  

							  <div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/'.$this->uri->segment(5)); ?>" class="btn text-uppercase btn-warning">

                                		<u>Step 5</u><br /> TENANTS

                                </a>                                

                              </div>

							  

							  <div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="6") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">

                                		<u>Step 6</u><br /> LOCATION

                                </a>                                

                              </div>

                            </div>

                       </div>
                       </div>

                 </div>		

</br>

</br>

<div class="row">

<div class="col-sm-8">

	<h4><p class="text-default"><strong><?php echo $main_data[0]['aname']; ?></strong></p>

	<p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>

</div>

</div>

				

				</br>



				<div class="row">

				<div class="col-sm-12">

		<div class="panel panel-primary">

			<div class="panel-heading">

				<h4>Tenant Information</h4>

			</div>

		<div class="panel-body">

<?php

$step= $this->uri->segment(4);

$aid=$this->uri->segment(5);

?>

<form method="post"  class="form-horizontal" role="form" name="contactform"  onSubmit="return checkval();"  action="<?php echo site_url('wizard/airport/airport_edit_step_5').'/'.$step.'/'.$aid; ?>">   

<div class="table-responsive">

	<table class="table table-hover" >

						<thead>

						<tr>

							<td><span class="text-danger">*</span><b>Tenant Name</b></td>

							<td><b>Company</b></td>

							<td><span class="text-danger">*</span><b>Category</b></td>

							<td><b># Locations</b></td>

							<td><b>Terminal</b></td>

							<td><b>Sq. Ft.</b></td>

							<td><b>Expires</b></td> 

						</tr>

						</thead>

						</tbody>

						

						<tr>

						<td align="left" style="border-top: none !important;">

								<div class="col-sm-12">

								  <input type="text" name="outletname" value="<?php   echo $edittanants[0]['outletname']; ?>" class="form-control" id="" >

								</div>

							</td>

							<td align="left" style="border-top: none !important;">

								<div class="col-sm-12">

								  <input type="text" name="companyname" value="<?php  echo $edittanants[0]['companyname']; ?>" class="form-control" id="">

								</div>

							</td>

							<td align="left" style="border-top: none !important;">	

<div class="col-sm-12">



<select name="categoryid" id="categoryid" class="form-control">

	<option></option>

	<?php  

	foreach ($con_mgt_type as $x) { 

	?>

	<option  value="<?php echo  $x['categoryid'] ?>" <?php if($x['categoryid'] ==  $edittanants[0]['categoryid']) { echo "selected"; }?>  > <?php echo $x['category'] ?> || <?php echo $x['productdescription']; ?></option> <?php echo "\n"; ?>

	<?php 		

	} 

	 ?>

</select>

</div>

							</td>

							<td align="left" style="border-top: none !important;">

								<div class="col-sm-12">

								  <input type="text" name="numlocations" value="<?php  echo $edittanants[0]['numlocations']; ?>" class="form-control" id="">

								</div>

							</td>

							

							<td align="left" style="border-top: none !important;">

								<div class="col-sm-8">

<select name="termlocation" id="termlocation" class="form-control">

	<option   value="0">-- Please Select --</option>

	<?php  

	foreach ($terminals as $x) { 

	?>

	<option  value="<?php echo  $x['terminalabbr']; ?>" <?php if($x['terminalabbr'] ==  $edittanants[0]['termlocation']) { echo "selected"; }?> > <?php echo $x['terminalabbr']; ?> </option> <?php echo "\n"; ?>

	<?php 		

	} 

	 ?>

</select>

</div>



							</td>

							<td align="left"  style="border-top: none !important;">

								<div class="col-sm-12">

								  <input type="text" name="sqft" value="<?php  echo $edittanants[0]['sqft']; ?>" class="form-control" id=""  >

								</div>

							</td>

							<td align="left" style="border-top: none !important;">

								<div class="col-sm-12">

								  <input type="date" name="exp" value="<?php echo $edittanants[0]['exp']; ?>" class="form-control " data-provide="datepicker" id=""  >

								</div>

							</td>

						

						</tr>

						

						</tbody>

						

					</table>

				</div>

			

			

<p><strong><span class="text-danger">*</span>  = required information  </strong></p>

</br>

<input type="hidden" name="step5edit" value="form1">

<div class="form-group">

&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Save Change</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport');?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>

</div>		

				

</form>			

	</div>

</div>

	<div class="panel panel-primary">

			<div class="panel-heading">

				<h4>Existing Tenants</h4>

			</div>

			<div class="panel-body">

			

			

			</div>

	</div>



               <?php /*?> <?php $cntr = 0; ?>

                <?php foreach($categories as $categoryName){?>

                <div class="panel panel-primary">

                	<div class="panel-heading"role="tab" id="dynamicconcession<?php echo $cntr;?>">

					<h4 class="panel-title">

<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight<?php echo $cntr;?>" aria-expanded="false" aria-controls="collapseEight<?php echo $cntr;?>">

					<?php echo $get_category_name[$cntr]['category']; ?>

					</a>

					</h4>

				</div>

					<div id="collapseEight<?php echo $cntr;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dynamicconcession<?php echo $cntr;?>">

                   

					<div class="pannel-body">

					<div class="table-responsive" style="height:450px;overflow:auto;">

                  	<table class="table table0 table-bordered table-hover table-striped " class="display" width="100%" cellspacing="0">

					 <?php $cntr++; ?>

                  <thead>

                  <tr>

            

                    <th style="width: 400px" >Tenant Name (Company)</th>

            

                    <th style="width: 400px">Company Name</th>

            

                    <th style="width: 400px">Product Description</th>

            

                    <th style="width: 400px">Terminal</th>

            

                    <th style="width: 400px">#</th>

            

                    <th style="width: 400px">Sq Ft</th>

            

                    <th style="width: 400px" >Expires</th>

            

                  </tr>

				  </thead>

				  <tbody>

				  

            	<?php 

				$aid= $this->uri->segment(4);

				foreach($categoryName as $categoryOptions) {?>

            	  <tr>

            

<td style="width: 400px"><a href='<?php echo site_url('wizard/airport/airport_edit_step_5');?>/<?php echo $aid; ?>/<?php echo $categoryOptions['oid'];?>' target="_new" title='Edit "<?php echo $categoryOptions['companyname']; ?>"'><i class='fa fa-external-link'></i></a>&nbsp;

<a href="javascript:void(0);" onClick="delme('<?php echo  $categoryOptions['oid']; ?>')" > 

<i class="fa fa-trash"></i></a>



<?php echo $categoryOptions['outletname']; ?></td>

            

                    <td style="width: 400px"><?php echo $categoryOptions['companyname']; ?></td>

            

                    <td style="width: 400px"><?php echo $categoryOptions['productdescription']; ?></td>

            

                    <td style="width: 400px"><?php echo $categoryOptions['termlocation']; ?></td>

            

                    <td style="width: 400px"><?php echo $categoryOptions['numlocations']; ?></td>

            

                    <td style="width: 400px"><?php echo $categoryOptions['sqft']; ?></td>

            

                    <td style="width: 400px"><?php echo $categoryOptions['exp']; ?></td>

            

            	</tr>

            	<?php } ?>

            </tbody>

            </table>

			</div>

                    </div>

					</div>

                </div>

                <?php } ?><?php */?>











 </div>

</section>

<div class="clearfix">

<div class="col-sm-12" style="font-family:verdana;">

                    	<div class="btn-group btn-group-justified" role="group" aria-label="...">

							  <div class="btn-group" role="group">

							  <div class="col-sm-2"></div>

							  <div class="col-sm-2">

<label class="control-label">Next, click on </label>

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="6") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">

                                		<u>Step 6</u><br /> LOCATION

                                </a>                

								</div>

                              </div>  

                            </div>

    </div>
    </div>

</section>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

<div class="modal-dialog">

<div class="modal-content">

<div class="modal-header">

<input type="hidden" id="recdelid" value="">

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

</button>

<h4 class="modal-title" id="myModalLabel">Conifm Delete</h4>

</div>

<div class="modal-body">

Are you sure want to delete this Airport contact?

</div>

<div class="modal-footer">

<button type="button" class="btn btn-danger" data-dismiss="modal">

<i class="fa fa-times">&nbsp;</i>Cancel</button>        

<button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete

</button>        

</div>

</div>

</div>

</div>



<div class="clearfix"></div>       

     <?php echo $this->load->view($footer);?>

<div class="clearfix"></div>

<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        

<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>

<script src="<?php echo base_url('fassests');?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

<script src="<?php echo base_url('assets');?>/js/jquery.dataTables.bootstrap.js"></script>

<script src="<?php echo base_url('fassests');?>/js/dataTables.tableTools.js" type="text/javascript"></script>

<script>





</script>

<script type="text/javascript">

 

 function checkval()

 {

	var rfield = new Array("outletname","categoryid");

	var msg=new Array('Outlet Name ','Category ');

	var errmsg="";

	for(i=0;i<rfield.length;i++)

	{

		//alert(rfield[i]);

		var val=document.getElementsByName(""+rfield[i]+"")[0].value;

	 	if(val=="" || val.replace(" ","")=="")

		{

			errmsg+="<b><i>"+msg[i]+" is Required. </i></b><br/>";

		}

	}	

	if(errmsg!="")

	{

		$(".error").html("<div class='alert alert-danger'>"+errmsg+"</div>");

		$('html, body').animate({scrollTop:$('.error').offset().top},'slow');

		return false;

	}

	 return true;

 }

function showmodal(bit)

{

	if(bit==1)

	{

		$("#myModal").modal("show");

	}else

	{

		$("#myModal").modal("hide");

	}

}

function delme(idd)

{

	$("#recdelid").attr("value",idd);

	showmodal(1);	

}$(".suredel").click(function()

{

	var oid=$("#recdelid").val();

	$(this).html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Deleteing...');

	$.ajax({ 

			url:"<?php echo site_url('frontend/airport_delete_step_1'); ?>",

			type:"POST",

			cache:false,

			data:"oid="+oid+"&action=delstep5",

			success: function(html)

			{

				if(html==1)

				{					

					showmodal(0);

					window.location="<?php echo site_url('wizard/airport/airport_edit_step_5').'/'.$step.'/'.$aid; ?>";

				} else

				{

					$(".modal-body").html(html);

				}

			}

	});

	

});

</script>

</body>