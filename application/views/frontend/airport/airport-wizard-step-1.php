<?php 
	echo $this->load->view($header);
?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Modify Data Step 1 | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Modify Data Step 1 | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/dataTables.tableTools.css">
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>
        <section class="main-content">
	     	
        	<section class="container-fluid">
                    <?php echo $this->load->view('frontend/include/breadcrumb'); ?> 
		 <div class="error">
		<?php echo $this->common->getmessage();?>
         </div>
	</br>
<div class="row">
<div class="col-sm-12">
	<h4><p class="text-default"><strong><?php echo $main_data[0]['aname']; ?></strong></p>
	<p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>
</div>
</div>
				
				</br>
				<div class="row">
				<div class="col-sm-12">
<h3 class="text-info"> Please complete your Contact Information</h3>
</br>
</hr>
<?php
$step= $this->uri->segment(4);
$aid=$this->uri->segment(5);
?>
<form method="post"  class="form-horizontal"  name="contactform"  onSubmit="return checkval();" role="form"  action="<?php echo site_url('wizard/airport/airport_edit_step_1').'/'.$step.'/'.$aid; ?>">   

<input type="hidden" name="airport_contact_id" value="<?php echo $this->uri->segment(5);?>"  >

<div class="col-sm-6">
<!--
<a href="<?php echo site_url('frontend/airport_add_steps_1');?>" name="addmorecontact" class="addmorecontact btn btn-primary" value="" id="">+ Add additional Contact</a>  -->


</br>           

         				  
 <div class="form-group">
 <label for="requestcompany" class="control-label col-sm-3" ><span class="text-danger">*</span>Select Management Responsibility:</label>
 <div class="col-sm-8">
 <select name="mgtresponsibility" id="requestcompany" class="form-control" tabindex="1">
		<option></option>
		<option <?php  if ($airport_contact[0]['mgtresponsibility'] == 1 ) echo 'selected';?> value="1">Airport Concessions</option>
		<option <?php  if ($airport_contact[0]['mgtresponsibility'] == 2 ) echo 'selected';?> value="2">Food and Beverage</option>
		<option <?php  if ($airport_contact[0]['mgtresponsibility'] == 3 ) echo 'selected';?> value="3">Retail, News and Gifts</option>
		<option <?php  if ($airport_contact[0]['mgtresponsibility'] == 4 ) echo 'selected';?> value="4">Leasing</option>
		<option <?php  if ($airport_contact[0]['mgtresponsibility'] == 5 ) echo 'selected';?> value="5">Passenger Services</option>
		<option <?php  if ($airport_contact[0]['mgtresponsibility'] == 6 ) echo 'selected';?> value="6">Advertising</option>
</select>
</div>
</div>				  
				  
		  
	<div class="form-group">
			<label class="control-label col-sm-3" for="afname"><span class="text-danger">*</span>First Name:</label>
			<div class="col-sm-8">
				<input type="text" name="afname" class="form-control" id="afname" placeholder="Enter First Name" value="<?php  echo $airport_contact[0]['afname']; ?>" tabindex="4">
			</div>
		</div>
	
	
	<div class="form-group">
			<label class="control-label col-sm-3" for="aaddress1"><span class="text-danger">*</span>Address 1:</label>
			<div class="col-sm-8">
				<input type="text" name="aaddress1" class="form-control" id="aaddress1" placeholder="Enter Address 1" value="<?php  echo $airport_contact[0]['aaddress1']; ?>" tabindex="6">
			</div>
	</div>
	
	
	
	<div class="form-group">
			<label class="control-label col-sm-3" for="accity"><span class="text-danger">*</span>City:</label>
			<div class="col-sm-8">
				<input type="text" name="accity" class="form-control" id="accity" placeholder="Enter City" value="<?php  echo $airport_contact[0]['accity']; ?>" tabindex="8">
			</div>
		</div>
	
	<div class="form-group">
			<label class="control-label col-sm-3" for="aczip"><span class="text-danger">*</span>Zip:</label>
			<div class="col-sm-8">
				<input type="text" name="aczip" class="form-control" id="aczip" placeholder="Enter Zip" value="<?php  echo $airport_contact[0]['aczip']; ?>" tabindex="10">
			</div>
		</div>
	
	<div class="form-group">
			<label class="control-label col-sm-3" for="aphone1"><span class="text-danger">*</span>Phone:</label>
			<div class="col-sm-8">
				<input type="text" name="aphone1" class="form-control" id="aphone1" placeholder="Enter Phone" value="<?php  echo $airport_contact[0]['aphone']; ?>" tabindex="12">
			</div>
	</div>
	
<div class="form-group">
			<label class="control-label col-sm-3" for="aemail"><span class="text-danger">*</span>E-mail:</label>
			<div class="col-sm-8">
				<input type="text" name="aemail" class="form-control" id="aemail" placeholder="Enter E-mail" value="<?php  echo $airport_contact[0]['aemail']; ?>" tabindex="14">
			</div>
		</div>


<input type="hidden" name="MM_update" value="form1">
<label class="control-label col-sm-3"><span class="text-danger"></span></label>
						<div class="form-group">
                        <div class="col-sm-8">
						<!--<button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('frontend/airport_edit_step_1');?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>-->
						<!--<button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('wizard/airport/'.$step);?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>-->
						<!--<button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('wizard/airport/airport_edit_steps/1/'.$step);?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>-->
						<button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="javascript:history.go(-1)" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>
                        </div>
						</div>
						</div>
<div class="col-sm-6">
</br>
<div class="form-group">
			<label class="control-label col-sm-3" for="atitle"><span class="text-danger">*</span>Title:</label>
			<div class="col-sm-8">
				<input type="text" name="atitle" class="form-control" id="atitle" placeholder="Enter Title" value="<?php  echo $airport_contact[0]['atitle']; ?>" tabindex="2">
			</div>
</div>
<div class="form-group">
			<label class="control-label col-sm-3" for="acompany"><span class="text-danger">*</span>Company:</label>
			<div class="col-sm-8">
				<input type="text" name="acompany" class="form-control" id="acompany" placeholder="Enter Company" value="<?php  echo $airport_contact[0]['acompany']; ?>" tabindex="3">
			</div>
</div>

<div class="form-group">
			<label class="control-label col-sm-3" for="alname"><span class="text-danger">*</span>Last Name:</label>
			<div class="col-sm-8">
				<input type="text" name="alname" class="form-control" id="alname" placeholder="Enter Last Name" value="<?php  echo $airport_contact[0]['alname']; ?>" tabindex="5">
			</div>
</div>
<div class="form-group">
			<label class="control-label col-sm-3" for="aaddress2">Address 2:</label>
			<div class="col-sm-8">
				<input type="text" name="aaddress2" class="form-control" id="aaddress2" placeholder="Enter Address 2" value="<?php  echo $airport_contact[0]['aaddress2']; ?>" tabindex="7">
			</div>
</div>

<div class="form-group">
			<label class="control-label col-sm-3" for="acstate"><span class="text-danger">*</span>State:</label>
			<div class="col-sm-8">
				<input type="text" name="acstate" class="form-control" id="acstate" placeholder="Enter State" value="<?php  echo $airport_contact[0]['acstate']; ?>" tabindex="9">
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="accountry"><span class="text-danger">*</span>Country:</label>
			<div class="col-sm-8">
				<input type="text" name="accountry" class="form-control" id="accountry" placeholder="Enter Country" value="<?php  echo $airport_contact[0]['accountry']; ?>" tabindex="11">
			</div>
	</div>
	
<div class="form-group">
			<label class="control-label col-sm-3" for="afax1"><span class="text-danger">*</span>Fax:</label>
			<div class="col-sm-8">
				<input type="text" name="afax1" class="form-control" id="afax1" placeholder="Enter Fax" value="<?php  echo $airport_contact[0]['afax']; ?>" tabindex="13">
			</div>
	</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="awebsite"><span class="text-danger">*</span>Web site:</label>
	<div class="col-sm-8">
		<input type="text" name="awebsite" class="form-control" id="awebsite" placeholder="Enter Web site" value="<?php  echo $airport_contact[0]['awebsite']; ?>" tabindex="15">
	</div>
</div>	
	
	
	
	




</div>
                        
                        </form> 			
				</div>
				</div>
               
 </div>
            </section>
<!--<div class="col-sm-12" style="font-family:verdana;">
                    	<div class="btn-group btn-group-justified" role="group" aria-label="...">
							  <div class="btn-group" role="group">
						
							  <div class="col-sm-3 pull-left" style="padding-left: 84px;">
<label class="control-label">Next, click on </label>
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="2") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 2</u><br /> AIRPORT INFO
                                </a>                
								</div>
                              </div>  
                            </div>
    </div>-->
        </section>
<div class="clearfix"></div>       
     <?php echo $this->load->view($footer);?>
<div class="clearfix"></div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('fassests');?>/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets');?>/js/jquery.dataTables.bootstrap.js"></script>
<script src="<?php echo base_url('fassests');?>/js/dataTables.tableTools.js" type="text/javascript"></script>
<script>
$(".addmorecontact").click(function()
{
$("#addmorecontacthidden").val(1);		
});

</script>
<script type="text/javascript">
 
 function checkval()
 {
	var rfield = new Array("mgtresponsibility","afname", "alname", "atitle","acompany","aaddress1","accity","aczip","accountry","acstate",'aphone1','afax1','aemail','awebsite');
	var msg=new Array('Select Management Responsibility','First Name','Last Name','Title','Company','Address','City','Zip','Country','State','Phone','Fax','Email','Website');
	var errmsg="";
	for(i=0;i<rfield.length;i++)
	{
		//alert(rfield[i]);
		var val=document.getElementsByName(""+rfield[i]+"")[0].value;
	 	if(val=="" || val.replace(" ","")=="")
		{
			errmsg+="<b><i>"+msg[i]+" is Required. </i></b><br/>";
		}
	}	
	if(errmsg!="")
	{
		$(".error").html("<div class='alert alert-danger'>"+errmsg+"</div>");
		$('html, body').animate({scrollTop:$('.error').offset().top},'slow');
		return false;
	}
	 return true;
 }
</script>
</body>