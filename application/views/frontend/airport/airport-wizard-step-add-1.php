<?php

echo $this->load->view($header);

?>

<title>

    <?php

//    if ($this->uri->segment(0) != '') {

//        echo 'Modify Data Step 1 | ARN Fact Book ' . $this->uri->segment(0);

//    } else {

//        echo 'MODIFY DATA Step 1 | ARN Fact Book';

//    }

    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/chosen.css">

<style>

    .dataTables_wrapper .dataTables_paginate .paginate_button {

        padding : 0px;

        margin-left: 0px;

        display: inline;

        border: 0px;

    }



    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {

        border: 0px;

    }



    .help-block ul li {

        margin-left: 0px ! Important;

    }

</style>

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">

            <?php /* ?><section class="col-sm-2">

              <?php foreach($modify_airport_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>

                <div class="error">

                    <?php echo $this->common->getmessage(); ?>

                </div>

                <div class="row">

                    <div class="col-sm-12" style="font-family:verdana;">
						<div class="arport-btns-holder">
                        
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">

                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "1") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 1</u><br />CONTACTS 

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "2") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 2</u><br /> AIRPORT INFO

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "3") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 3</u><br /> TERMINALS

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "4") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 4</u><br /> RENTAL & PARKING

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "5") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 5</u><br /> TENANTS

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "6") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 6</u><br /> LOCATION

                                </a>                                

                            </div>

                        </div>
                        </div>

                    </div>

                </div>	

                </br>	

                <div class="row">

                    <div class="col-sm-12">



                        <h4><p class="text-default"><strong><?php echo $main_data[0]['aname'] . ' (' . $main_data[0]['IATA'] . ')'; ?></strong></p>

                            <p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>

                    </div>

                </div>	

                </br>

                <div class="row">

                    <div class="col-sm-12">

                        <h3 class="text-info"> Please complete your Contact Information</h3>

                        </br>

                        </hr>

                        <?php

                        $step = $this->uri->segment(4);

                        $aid = $this->uri->segment(5);

                        ?>

                        <!--<form method="post" id="newcontact" class="form-horizontal" role="form" name="contactform"  onSubmit="return checkval();"  action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>">-->   

                        <form method="post" id="newcontact" class="form-horizontal"  role="form" onSubmit="return checkplace();"  name="contactform" action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>">   





                            <div class="col-sm-6">

                                <!--

                                <a href="<?php echo site_url('frontend/airport_add_steps_1') ?>" name="addmorecontact" class="addmorecontact btn btn-primary" value="" id="">+ Add additional Contact</a>

                                -->

                                <input type="hidden" name="addmorecontacthidden" value="1" id="">

                                </br>       				  

                                <div class="form-group control-group ">

                                    <label for="requestcompany" class="control-label col-sm-3" for="requestcompany">Select Management Responsibility:</label>

                                    <div class="col-sm-8">

                                        <select name="mgtresponsibility" id="requestcompany" class="chosen form-control"  data-placeholder='Please Select an Option' tabindex="1" >

                                            <option></option>

                                            <option  value="1">Airport Concessions</option>

                                            <option  value="2">Food and Beverage</option>

                                            <option value="3">Retail, News and Gifts</option>

                                            <option value="4">Leasing</option>

                                            <option value="5">Passenger Services</option>

                                            <option value="6">Advertising</option>

                                        </select>

                                    </div>

                                </div>				  







                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="afname">First Name:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="afname" class="form-control" id="afname" placeholder="Enter First Name" tabindex="4" >

                                    </div>

                                </div>





                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="aaddress1">Address 1:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="aaddress1" class="form-control" id="aaddress1" placeholder="Enter Address 1" tabindex="6" >

                                    </div>

                                </div>







                                <div class="form-group">

                                    <label class="control-label col-sm-3" for="accity">City:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="accity" class="form-control" id="accity" placeholder="Enter City"  tabindex="8" >

                                    </div>

                                </div>



                                <div class="form-group">

                                    <label class="control-label col-sm-3" for="aczip">Zip:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="aczip" class="form-control" id="aczip" placeholder="Enter Zip" tabindex="10" >

                                    </div>

                                </div>



                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="aphone1">Phone:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="aphone1" class="form-control" id="aphone1" placeholder="Enter Phone" tabindex="12" >

                                    </div>

                                </div>



                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="aemail">E-mail:</label>

                                    <div class="col-sm-8">

                                        <!--<input type="text" name="aemail" class="form-control" id="aemail" placeholder="Enter E-mail"  tabindex="14" data-error="The Email address is invalid" required>-->

                                        <input type="email" name="aemail" class="form-control" id="aemail" placeholder="Enter E-mail"  tabindex="14" >

                                    </div>

                                </div>



                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="apriority">Placement Number:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="priority" class="form-control" id="apriority" placeholder="Enter Contact's Placement Number"  tabindex="16" >

                                    </div>

                                </div>





                                <input type="hidden" name="MM_update" value="form1">

                                <div class="form-group">

                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport'); ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                                </div>

                            </div>

                            <div class="col-sm-6">

                                </br> 



                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="atitle">Title:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="atitle" class="form-control" id="atitle" placeholder="Enter Title" tabindex="2" >

                                    </div>

                                </div>

                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="acompany">Company:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="acompany" class="form-control" id="acompany" placeholder="Enter Company" tabindex="3" >

                                    </div>

                                </div>

                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="alname">Last Name:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="alname" class="form-control" id="alname" placeholder="Enter Last Name" tabindex="5" >

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="control-label col-sm-3" for="aaddress2">Address 2:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="aaddress2" class="form-control" id="aaddress2" placeholder="Enter Address 2"  tabindex="7">

                                    </div>

                                </div>

                                <div class="form-group control-group ">

                                    <label class="control-label col-sm-3" for="acstate">State:</label>

                                    <div class="col-sm-8">

                                        <!--<input type="text" name="acstate" class="form-control" id="acstate" placeholder="Enter State"  tabindex="9">-->

                                        <select class="chosen form-control col-xs-10 col-sm-10" id="acstate" name="acstate" data-placeholder="Select State" >



                                            <?php

                                            foreach ($rsStates as $key => $state):

                                                ?>







                                                <option value="<?php echo strtoupper($state['state_code']); ?>">



                                                    <?php echo $state['state_name']; ?>



                                                </option>



                                                <?php

                                            endforeach;

                                            ?>



                                        </select>

                                    </div>

                                </div>



                                <div class="form-group ">

                                    <label class="control-label col-sm-3" for="accountry">Country:</label>

                                    <div class="col-sm-8">

                                        <!--<input type="text" name="accountry" class="form-control" id="accountry" placeholder="Enter Country" tabindex="11" >-->

                                        <!--<select name="accountry" class="chosen-select col-sm-10" >-->

                                        <select id="acountry" name="accountry" class="chosen form-control" tabindex="1" >

                                            <?php

                                            foreach ($row_rsCountries as $country):

                                                ?>

                                                <option value="<?php echo $country['countries_name']; ?>"<?php

                                                if (!(strcmp($country['countries_name'], 'United States'))) {

                                                    echo "selected=\"selected\"";

                                                }

                                                ?>>

                                                            <?php

                                                            echo $country['countries_name'];

                                                            ?>

                                                </option>

                                                <?php

                                            endforeach;

                                            ?>

                                        </select>



                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="control-label col-sm-3" for="afax1">Fax:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="afax1" class="form-control" id="afax1" placeholder="Enter Fax" tabindex="13">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="control-label col-sm-3" for="awebsite">Web site:</label>

                                    <div class="col-sm-8">

                                        <input type="text" name="awebsite" class="form-control" id="awebsite" placeholder="Enter Web site" tabindex="15">

                                    </div>

                                </div>



                                <input type="hidden" id="aaid" value="<?php echo $this->uri->segment(5); ?>">



                            </div>



                        </form> 			

                    </div>

                </div>

                <div class="panel panel-primary">

                    <div class="panel-heading" role="tab" id="contact">

                        <h4 class="panel-title">

                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">

                                Existing Contact(s)

                            </a>

                        </h4>

                    </div>

                    <div class="col-lg-11 contacts-inner-table">

                        <!--<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contact">-->

                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contact">

                            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 

                                <ul id="myTabs" class="nav nav-tabs" role="tablist"> 

                                    <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><i class="margin-right-5 fa fa-qrcode">&nbsp;</i>Grid View</a></li> 

                                    <li role="presentation" class="" ><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false"><i class="margin-right-5 fa fa-table">&nbsp;</i>Table View</a></li>

                                </ul> 

                                <br />

                                <div id="myTabContent" class="tab-content"> 

                                    <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">



                                        <div class="pannel-body">
                                        
                                        <div class="table-responsive">


                                            <table class="table  table-bordered table-hover table-striped" id="table2" >

                                                <thead>

                                                    <?php

                                                    $i = 2;

                                                    $k = 0;

                                                    ?>

                                                    <?php

                                                    foreach ($airport_contact as $airport_contacts) {

                                                        $aid = $this->uri->segment(4);

                                                        $k++;

                                                        ?>

                                                        <?php

                                                        if ($i % 2 == 0) {



                                                            echo "<tr>";

                                                        }

                                                        ?>

                                                    <td valign="top">



                                                        <table class="table  table-bordered table-hover table-striped" style="border-collapse: collapse;">



                                                            <tbody><tr>



                                                                    <td colspan="2" align="left" height="18"><h1 class="default-title"><?php echo $airport_contacts['mgtresponsibilityname']; ?></h1></td>



                                                                </tr>



                                                                <tr>



                                                                    <td colspan="2" align="left" height="18">

                                                                        <!--<a href="<?php // echo site_url('wizard/airport/airport_edit_step_1');                                   ?>/<?php // echo $aid;                                   ?>/<?php // echo $airport_contacts['acid'];                                   ?>"  class="btn btn-primary" > <i class="fa fa-edit"></i>&nbsp;Edit</a>-->

                                                                        <a href= "javascript:void(0);" onClick="editme('<?php echo $airport_contacts['acid']; ?>')" class="btn btn-primary" > <i class="fa fa-edit"></i>&nbsp;Edit</a>

                                                                        <a href="javascript:void(0);" onClick="delme('<?php echo $airport_contacts['acid']; ?>')"  class="btn btn-danger"> 

                                                                            <i class="fa fa-trash"></i>&nbsp;Delete</a>





                                                                    </td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Contact </td>



                                                                    <td align="left"><?php echo $airport_contacts['afname'] . " " . $airport_contacts['alname']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Title</td>



                                                                    <td align="left"><?php echo $airport_contacts['atitle']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Company</td>



                                                                    <td align="left"><?php echo $airport_contacts['acompany']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Address</td>



                                                                    <td align="left"><?php echo $airport_contacts['aaddress1']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">&nbsp;</td>



                                                                    <td align="left"><?php echo $airport_contacts['aaddress2']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left"></td>



                                                                    <td align="left"><?php echo $airport_contacts['accity'] . "," . $airport_contacts['acstate'] . " " . $airport_contacts['aczip'] . " " . $airport_contacts['accountry']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Phone</td>



                                                                    <td align="left"><?php echo $airport_contacts['aphone']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Fax</td>



                                                                    <td align="left"><?php echo $airport_contacts['afax']; ?></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">E-mail</td>



                                                                    <td align="left"><a href="mailto:<?php echo $airport_contacts['aemail']; ?>" class="BodyLink"><?php echo $airport_contacts['aemail']; ?></a></td>



                                                                </tr>



                                                                <tr>



                                                                    <td align="left">Web site</td>



                                                                    <td align="left"><a href="<?php echo $this->common->weblink($airport_contacts['awebsite']); ?>" target="_blank"><?php echo $airport_contacts['awebsite']; ?></a></td>



                                                                </tr>



                                                            </tbody></table>



                                                    </td>



                                                    <?php

                                                    if ($i % 2 != 0) {

                                                        echo "</tr>";



                                                        echo "<tr><td colspan='2' height='50'></td> </tr>";

                                                    }

                                                    $i = $i + 1;

                                                    ?>

                                                    <?php

                                                }

                                                $aid = $this->uri->segment(5);

                                                ?>



                                            <!--<tr></tr>-->

                                                </thead>

                                            </table>
                                            </div>

                                        </div>

                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">

                                        <div class="panel-body">

                                            <div class="table-responsive">

                                                <table  width="100%" id="table-view-Data" class="table-bordered table-hover table-striped contact2">

                                                    <thead>

                                                        <tr  style="background-color:#eee;">                                                        

                                                            <th>Category</th>

                                                            <th>Contact</th>

                                                            <th>Title</th>

                                                            <th>Company</th>

                                                            <th>Address</th>

                                                            <th>Phone</th>

                                                            <th>Fax</th>

                                                            <th>E-mail</th>

                                                            <th>Web site</th>

                                                            <th>Action</th>

                                                        </tr>

                                                    </thead>

                                                    <tbody>



                                                        <?php $i = 2; ?>

                                                        <?php

                                                        foreach ($airport_contact as $airport_contact1):

                                                            ?>



                                                            <tr>                                                            

                                                                <td><?php echo $airport_contact1['mgtresponsibilityname']; ?></td>

                                                                <td><?php echo $airport_contact1['afname'] . " " . $airport_contact1['alname']; ?></td>

                                                                <td><?php echo $airport_contact1['atitle']; ?></td>

                                                                <td><?php echo $airport_contact1['acompany']; ?></td>

                                                                <td><?php echo $airport_contact1['aaddress1'] . ' ' . $airport_contact1['aaddress2'] . "," . $airport_contact1['accity'] . "," . $airport_contact1['acstate'] . " " . $airport_contact1['aczip'] . " " . $airport_contact1['accountry']; ?></td>

                                                                <td><?php echo $airport_contact1['aphone']; ?></td>

                                                                <td><?php echo $airport_contact1['afax']; ?></td>

                                                                <td><a href="mailto:<?php echo $airport_contact1['aemail']; ?>" class="BodyLink"><?php echo $airport_contact1['aemail']; ?></a></td>

                                                                <td><a href="<?php echo $this->common->weblink($airport_contact1['awebsite']); ?>" target="_blank"><?php echo $airport_contact1['awebsite']; ?></a></td>

                                                                <td>

                                                                    <!--<a href="<?php // echo site_url('wizard/airport/airport_edit_step_1');                         ?>/<?php echo $aid; ?>/<?php // echo $airport_contact1['acid'];                         ?>"  class="btn btn-primary btn-xs" > <i class="fa fa-edit"></i>&nbsp;Edit</a>-->



                                                                    <a href= "javascript:void(0);" onClick="editme('<?php echo $airport_contacts['acid']; ?>')" class="btn btn-primary btn-xs" > <i class="fa fa-edit"></i>&nbsp;Edit</a>



                                                                    <a href="javascript:void(0);" onClick="delme('<?php echo $airport_contact1['acid']; ?>')"  class="btn btn-danger btn-xs"> 

                                                                        <i class="fa fa-trash"></i>&nbsp;Delete</a>	

                                                                </td>

                                                            </tr>



                                                        <?php endforeach; ?>

                                                    </tbody>

                                                </table>





                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>    

                </div>

                <!--</div>-->

            </section>

            <div class="col-sm-12" style="font-family:verdana;">

                <div class="btn-group btn-group-justified" role="group" aria-label="...">

                    <div class="btn-group" role="group">



                        <div class="col-sm-2 pull-left" style="padding-left: 0px;">

                            <label class="control-label">Next, click on </label>

                            <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                            if ($this->uri->segment(4) == "2") {

                                echo " btn-warning";

                            } else {

                                echo " btn-primary";

                            }

                            ?>">

                                <u>Step 2</u><br /> AIRPORT INFO

                            </a>                

                        </div>

                    </div>  

                </div>

            </div>



        </section>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">

                        <input type="hidden" id="arecdelid" value="<?php echo $this->uri->segment(5); ?>">

                        <input type="hidden" id="recdelid" value="">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                        </button>

                        <h4 class="modal-title" id="myModalLabel">Conifm Delete</h4>

                    </div>

                    <div class="modal-body">

                        Are you sure want to delete this Airport contact?

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-danger" data-dismiss="modal">

                            <i class="fa fa-times">&nbsp;</i>Cancel</button>        

                        <button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete

                        </button>        

                    </div>

                </div>

            </div>

        </div>





        <div class="modal fade bs-example-modal-lg" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModaleditLabel" aria-hidden="true">



            <div class="modal-dialog modal-lg">



                <div class="modal-content">



                    <div class="modal-header">



                        <input type="hidden" id="receditid" value="">



                        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->



                        <h3 class="modal-title" id="myModaleditLabel">Edit Contact</h3>



                    </div>



                    <div class="modal-body">







                        <!--<form method="post"  class="form-horizontal"  name="contactform_edit" id="contactform_edit" onSubmit="return checkval_e();" role="form"  action="<?php // echo site_url('wizard/airport/airport_edit_step_1') . '/' . $step . '/' . $aid;                            ?>">-->   

                        <!--<form method="post"  class="form-horizontal" id="edit_contactf" name="contactform_edit" onSubmit="return checkval_e();" role="form"  action="<?php // echo site_url('wizard/airport/airport_edit_step_1/1') . '/' . $aid;        ?>">-->   

                        <form method="post"  class="form-horizontal" id="edit_contactf" name="contactform_edit" role="form" action="<?php echo site_url('wizard/airport/airport_edit_step_1/1') . '/' . $aid; ?>">   

                        <!--<form method="post"  class="form-horizontal"  name="contactform_edit" id="contactform_edit"  role="form"  action="<?php // echo site_url('wizard/airport/airport_edit_step_1') . '/' . $step . '/' . $aid;                            ?>">-->   





                            <!--<form method="post"  class="form-horizontal"  name="contactform_edit" onSubmit="return checkval_e();" role="form"  action="">-->   



                            <div class="row">



                                <div class="error_e col-sm-12">

                                    <?php echo $this->common->getmessage(); ?>

                                </div>



                                <div class="form-group">

                                    <div class="col-sm-6">

                                        <label for="inputEmail3" class="col-sm-3 control-label">&nbsp;</label>

                                        <div class="col-sm-8">

                                            <div class="text-danger">* Indicates Required Information </div>

                                        </div>

                                    </div>

                                </div>







                                <div class="col-sm-12">





                                    <div class="form-group ">

                                        <!--<label for="requestcompany" class="control-label col-sm-3" >&nbsp;</label>-->

                                        <label for="requestcompany_e" class="control-label col-sm-5">Select Management Responsibility:</label>

                                        <div class="col-sm-2"></div>

                                        <div class="col-sm-5" style="margin-left: 110px !important;">

                                            <select name="mgtresponsibility" id="requestcompany_e" class="chosen form-control" tabindex="1">

                                                <option></option>

                                                <option value=""> </option>

                                                <option value="1">Airport Concessions</option>

                                                <option value="2">Food and Beverage</option>

                                                <option value="3">Retail, News and Gifts</option>

                                                <option value="4">Leasing</option>

                                                <option value="5">Passenger Services</option>

                                                <option value="6">Advertising</option>

                                            </select>

                                        </div>

                                    </div>





                                </div>

                                <div class="col-sm-6">

                                    <!--

                                    <a href="<?php // echo site_url('frontend/airport_add_steps_1');                               ?>" name="addmorecontact" class="addmorecontact btn btn-primary" value="" id="">+ Add additional Contact</a>  -->



<!--<input type="hidden" name="airport_contfact_id" value="<?php // echo $this->uri->segment(5);                               ?>"  >-->

                                    <!--</br>-->           

                                    <input type="hidden" name="airport_contact_id" id="airport_contact_id" />



                                    <!--                                    <div class="form-group">

                                                                            <label for="requestcompany" class="control-label col-sm-3" ><span class="text-danger">*</span>Select Management Responsibility:</label>

                                                                            <div class="col-sm-8">

                                                                                <select name="mgtresponsibility" id="requestcompany_e" class="form-control" tabindex="1">

                                                                                    <option></option>

                                                                                    <option value="1">Airport Concessions</option>

                                                                                    <option value="2">Food and Beverage</option>

                                                                                    <option value="3">Retail, News and Gifts</option>

                                                                                    <option value="4">Leasing</option>

                                                                                    <option value="5">Passenger Services</option>

                                                                                    <option value="6">Advertising</option>

                                                                                </select>

                                                                            </div>

                                                                        </div>				  -->



                                    <!--                                    <div class="form-group has-feedback">

                                                                            <div class="col-sm-3">&nbsp;</div>

                                                                            <label for="requestcompany_e" class="control-label col-sm-8"><span class="text-danger">*</span>Select Management Responsibility:</label>

                                                                            <div class="col-sm-8">

                                                                            &nbsp;

                                                                                <select name="mgtresponsibility" id="requestcompany_e" class="form-control" tabindex="1">

                                                                                <option></option>

                                                                                <option value="1">Airport Concessions</option>

                                                                                <option value="2">Food and Beverage</option>

                                                                                <option value="3">Retail, News and Gifts</option>

                                                                                <option value="4">Leasing</option>

                                                                                <option value="5">Passenger Services</option>

                                                                                <option value="6">Advertising</option>

                                                                            </select>

                                                                            </div>

                                                                            

                                                                            <div class="col-sm-6 help-block with-errors"></div>

                                                                        </div>-->





                                    <div class="form-group">

                                        <label class="control-label col-sm-3" for="atitle">Title:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="atitle" class="form-control" id="atitle_e" placeholder="Enter Title" value="" tabindex="2" >

                                        </div>

                                    </div>



                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="afname">First Name:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="afname" class="form-control" id="afname_e" placeholder="Enter First Name" value="" tabindex="4" >

                                        </div>

                                    </div>





                                    <div class="form-group">

                                        <label class="control-label col-sm-3" for="aaddress1">Address 1:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="aaddress1" class="form-control" id="aaddress1_e" placeholder="Enter Address 1" value="" tabindex="6" >

                                        </div>

                                    </div>





                                    <input type="hidden" id="aid" value="<?php echo $this->uri->segment(5); ?>">

                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="accity">City:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="accity" class="form-control" id="accity_e" placeholder="Enter City" value="" tabindex="8" >

                                        </div>

                                    </div>



                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="aczip">Zip:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="aczip" class="form-control" id="aczip_e" placeholder="Enter Zip" value="" tabindex="10" >

                                        </div>

                                    </div>



                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="aphone1">Phone:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="aphone1" class="form-control" id="aphone1_e" placeholder="Enter Phone" value="" tabindex="12" >

                                        </div>

                                    </div>



                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="aemail">E-mail:</label>

                                        <div class="col-sm-8">

                                            <!--<input type="text" name="aemail" class="form-control" id="aemail_e" placeholder="Enter E-mail" value="" tabindex="14" data-error="The Email address is invalid" required>-->

                                            <input type="email" name="aemail" class="form-control" id="aemail_e" placeholder="Enter E-mail" value="" tabindex="14" >

                                        </div>

                                    </div>



                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="apriority">Placement Number:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="priority" class="form-control" id="apriority_e" placeholder="Enter Contact's Placement Number"  tabindex="16" >

                                            <input type="hidden" name="priority_old"  id="apriority_old_e" >

                                            <input type="hidden" name="aid" value="<?php echo $this->uri->segment(5); ?>">

                                        </div>

                                    </div>





                                    <input type="hidden" name="MM_update" value="form1">

                                    <label class="control-label col-sm-3"><span class="text-danger"></span></label>

                                    <!--                                <div class="form-group">

                                                                        <div class="col-sm-8">

                                                                                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('frontend/airport_edit_step_1');                               ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                                                                                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('wizard/airport/'.$step);                               ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                                                                                                  javascript:history.go(-1)      <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('wizard/airport/airport_edit_steps/1/'.$step);                               ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                                                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                                                                        </div>

                                                                    </div>-->

                                </div>

                                <div class="col-sm-6">

                                    <!--</br>--> 

                                    <!--                                    <div class="form-group">

                                                                            <label class="control-label col-sm-3" for="atitle"><span class="text-danger">*</span>Title:</label>

                                                                            <div class="col-sm-8">

                                                                                <input type="text" name="atitle" class="form-control" id="atitle_e" placeholder="Enter Title" value="" tabindex="2">

                                                                            </div>

                                                                        </div>-->



                                    <!--                                    <div class="form-group has-feedback">

                                                                            <label for="requestcompany" class="control-label col-sm-3" >&nbsp;</label>

                                                                            <div class="col-sm-11">

                                                                                <select name="mgtresponsibility" id="requestcompany_e" class="chosen form-control" tabindex="1"data-error="Management Responsibility is invalid" required>

                                                                                    <option></option>

                                                                                    <option value=""> </option>

                                                                                    <option value="1">Airport Concessions</option>

                                                                                    <option value="2">Food and Beverage</option>

                                                                                    <option value="3">Retail, News and Gifts</option>

                                                                                    <option value="4">Leasing</option>

                                                                                    <option value="5">Passenger Services</option>

                                                                                    <option value="6">Advertising</option>

                                                                                </select>

                                                                            </div>

                                                                            <div class="col-sm-6 help-block with-errors"></div>

                                                                        </div>-->

                                    <div class="form-group">

                                        <label class="control-label col-sm-3" for="acompany">Company:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="acompany" class="form-control" id="acompany_e" placeholder="Enter Company" value="" tabindex="3" >

                                        </div>

                                    </div>



                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="alname">Last Name:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="alname" class="form-control" id="alname_e" placeholder="Enter Last Name" value="" tabindex="5" >

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-sm-3" for="aaddress2">Address 2:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="aaddress2" class="form-control" id="aaddress2_e" placeholder="Enter Address 2" value="" tabindex="7">

                                        </div>

                                    </div>



                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="acstate">State:</label>

                                        <div class="col-sm-8">

                                            <!--<input type="text" name="acstate" class="form-control" id="acstate_e" placeholder="Enter State" value="" tabindex="9">-->

                                            <select class="chosen form-control col-xs-10 col-sm-10" id="acstate_e" name="acstate" data-placeholder="Select State" >



                                                <?php

                                                foreach ($rsStates as $key => $state):

                                                    ?>



                                                    <option value="<?php echo $state['state_code']; ?>">



                                                        <?php echo $state['state_name']; ?>



                                                    </option>



                                                    <?php

                                                endforeach;

                                                ?>



                                            </select>

                                        </div>

                                    </div>

                                    <div class="form-group ">

                                        <label class="control-label col-sm-3" for="accountry">Country:</label>

                                        <div class="col-sm-8">

                                            <!--<input type="text" name="accountry" class="form-control" id="accountry_e" placeholder="Enter Country" value="" tabindex="11">-->

                                            <select name="accountry" id="accountry_e"  class="chosen col-sm-10 form-control" >

                                                <?php

                                                foreach ($row_rsCountries as $country):

                                                    ?>

                                                    <option value="<?php echo strtoupper($country['countries_name']); ?>">

                                                        <?php echo $country['countries_name']; ?>

                                                    </option>

                                                    <?php

                                                endforeach;

                                                ?>

                                            </select>

                                        </div>

                                    </div>



                                    <div class="form-group">

                                        <label class="control-label col-sm-3" for="afax1">Fax:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="afax1" class="form-control" id="afax1_e" placeholder="Enter Fax" value="" tabindex="13">

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-sm-3" for="awebsite">Web site:</label>

                                        <div class="col-sm-8">

                                            <input type="text" name="awebsite" class="form-control" id="awebsite_e" placeholder="Enter Web site" value="" tabindex="15">

                                        </div>

                                    </div>	



                                </div>

                            </div>



                            <div class="modal-footer ">

                                <div class="form-group ">



                                    <!--                                                <button type="button" onClick="empty_message();" class="btn btn-danger" id="cancel" data-dismiss="modal">

                                    

                                                                                        <i class="fa fa-times">&nbsp;</i>Cancel</button>        

                                    

                                                                                    <button type="submit"  class="btn btn-success sureedit"><i class="fa fa-check">&nbsp;</i>Save Changes

                                    

                                                                                    </button>     -->



                                    <button type="submit" id="update_cont" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Update</button>&nbsp;&nbsp;

                                    <button type="button" onClick="empty_message();" class="btn btn-danger" id="cancel" data-dismiss="modal">



                                        <i class="fa fa-times">&nbsp;</i>Cancel</button> 

                    <!--<a href="" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>-->



                                </div>

                            </div>

                        </form> 	



                    </div>



                </div>



            </div>     



        </div>

        <div class="clearfix"></div>       

        <?php echo $this->load->view($footer); ?>

        <div class="clearfix"></div>

    </div>

<!--    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        

    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url('assets'); ?>/js/jquery.dataTables.bootstrap.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

    -->    





    <script src="<?php echo base_url('fassests'); ?>/js/chosen.jquery.min.js"></script>



<!--<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>-->





    <!--<script src="<?php echo base_url(); ?>assets/js/validator.js"></script>-->



    <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

   <!--<script src="<?php echo base_url('assets'); ?>/js/jquery.dataTables.bootstrap.js"></script>-->

    <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>

    <script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.3.full.min.js"></script>

    <script src="http://cdn.datatables.net/plug-ins/1.10.7/type-detection/formatted-num.js"></script>  

    <script src="<?php echo base_url('assets'); ?>/js/typeahead-bs2.min.js"></script>







    <script>



//                                        $.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="submit"], button):enabled';

//

//                                        $(".addmorecontact").click(function ()

//                                        {

//                                            $("#addmorecontacthidden").val(1);

//                                        });



    </script>

    <script type="text/javascript">



//        $(document).ready(function () {



//            if(".disabled"){

//                $(btnid).attr("disabled","disabled").css("cursor", "default").fadeTo(500,0.2);

//            }



//        $("#requestcompany").append("data-placeholder='Please Select an Option'").trigger('chosen:updated');

        $("#requestcompany").prepend("<option value='' selected='selected'>Please Select&nbsp;</option>").trigger('chosen:updated');

        $("#acstate").prepend("<option value='' selected='selected'>Please Select&nbsp;</option>").trigger('chosen:updated');





        $("#requestcompany_e").prepend("<option value='' >Please Select&nbsp;</option>").trigger('chosen:updated');

        $("#acstate_e").prepend("<option value='' >Please Select&nbsp;</option>").trigger('chosen:updated');





//        $('#newcontact').validator();

//        $('#edit_contactf').validator();











//             $('#newcontact').validator({

//                                                                    rules: {

//                                                                        acstate: {

//                                                                            required: true

//                                                                        }

//                                                                    },

//                                                                    highlight: function (element) {

//                                                                        $(element).closest('.control-group').removeClass('success').addClass('error').trigger('chosen:updated');

////                                                                        $('#categoryide').trigger('chosen:updated');

//                                                                    },

//                                                                    success: function (element) {

//                                                                        element.text('OK!').addClass('valid')

//                                                                                .closest('.control-group').removeClass('error').addClass('success').trigger('chosen:updated');

//                                                                    }

//                                                                });





//            $('#newcontact')

//    .find('.chosen-selects')

//        .chosen({

//            width: '100%',

//            allow_single_deselect : true,

//            no_results_text : 'Oops, No Results Found for - '

//        })

//        // Revalidate your field when it is changed

//        .change(function(e) {

//            $('#newcontact').bootstrapValidator('revalidateField', 'mgtresponsibility');

//        })

//        .end()

//    .bootstrapValidator({

//        excluded: ':disabled', // <=== make sure to use this option

//        feedbackIcons: {

//            valid: 'glyphicon glyphicon-ok',

//            invalid: 'glyphicon glyphicon-remove',

//            validating: 'glyphicon glyphicon-refresh'

//        },

//        fields: {

//            your_field_name: {

//                validators: {

//                    notEmpty: {

//                        message: 'Please choose an option'

//                    }

//                }

//            }

//        }

//    });





//            $("#requestcompany").chosen({

//                allow_single_deselect: true

//            });

        /*

         $('#newcontact')

         .find('[name="mgtresponsibility"]')

         .chosen({

         width: '100%'

         })

         // Revalidate the color when it is changed

         .change(function(e) {

         $('#newcontact').validator('revalidateField', 'mgtresponsibility');

         })

         .end()

         .validator({

         framework: 'bootstrap',

         excluded: ':disabled',

         //            icon: {

         //                valid: 'glyphicon glyphicon-ok',

         //                invalid: 'glyphicon glyphicon-remove',

         //                validating: 'glyphicon glyphicon-refresh'

         //            },

         fields: {

         colors: {

         validators: {

         callback: {

         message: 'Please choose 2-4 color you like most',

         callback: function(value, validator, $field) {

         // Get the selected options

         var options = validator.getFieldElements('mgtresponsibility').val();

         return (options != null && options.length >= 2 && options.length <= 4);

         }

         }

         }

         }

         }

         });

         */



//            $('#newcontact').validator({

//                rules: {

////                requestcompany: {

////                    mgtresponsibility: {

////                        required: true

////                    },

//                    acstate: {

//                        required: true

//                    }

//                },

//                highlight: function (element) {

//                    $(element).closest('.control-group').removeClass('success').addClass('error').trigger('chosen:updated');

////                                                                        $('#categoryide').trigger('chosen:updated');

//                },

//                success: function (element) {

//                    element.text('OK!').addClass('valid')

//                            .closest('.control-group').removeClass('error').addClass('success').trigger('chosen:updated');

//                }

//            });

//        });





        var seg = "<?php echo $this->uri->segment(0) ?>";



        if (seg != '') {

            $('title').html("Modify Data Step 1 | ARN Fact Book " + seg);

        }

        else {

            $('title').html("Modify Data Step 1 | ARN Fact Book");

        }



        $(document).ready(function (e)

        {

            $('.contact2').DataTable({

                "lengthMenu": [5, 10, 15, 20, 25],

                "columnDefs": [

                    {"width": "103px", "targets": 9},

                    {"aTargets": [9], "bSortable": false}

                ],

                "dom": 'T<"clear">lfrtip',

                "oTableTools": {

                    "aButtons": [

                        "print",

                        {

                            "sExtends": "collection",

                            "sButtonText": "Save",

                            "aButtons": ["copy", "csv", "xls", "pdf"]

                        }

                    ],

                    "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"

                },

                "oLanguage": {

                    "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"



                }

            });

        });



        function showmodal(bit)

        {

            if (bit == 1)

            {

                $("#myModal").modal("show");

            } else

            {

                $("#myModal").modal("hide");

            }

        }

        function delme(idd)

        {

            $("#recdelid").attr("value", idd);

            showmodal(1);

        }

        $(".suredel").click(function ()

        {

            var adelid = $("#arecdelid").val();

//            alert(adelid); return false;

            var delid = $("#recdelid").val();

            $(this).html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Deleteing...');

            $.ajax({

                url: "<?php echo site_url('frontend/airport_delete_step_1'); ?>",

                type: "POST",

                cache: false,

                data: "acid=" + delid + "&aid=" + adelid + "&action=delstep1",

                success: function (html)

                {

                    if (html == 1)

                    {

                        showmodal(0);

                        window.location = "<?php echo current_url(); ?>";

                    } else

                    {

                        $(".modal-body").html(html);

                    }

                }

            });



        });







        function showmodaledit(bit)

        {

            if (bit == 1)

            {

                $("#myEditModal").modal({backdrop: 'static', keyboard: false});

            } else

            {

                $("#myEditModal").modal("hide");

            }

        }







        function editme(acid)

        {



//            var aid1 = aid;

            var acid1 = acid;

//alert(acid1);

            $.ajax({

                dataType: "json",

                url: "<?php echo site_url('ajax/edit_contact'); ?>",

                type: "POST",

                cache: false,

                data: "&acid=" + acid,

                success: function (response) {





                    var $response = $(response);

//alert($response);

                    var trHTML1 = '';

                    var trHTML2 = '';

                    var trHTML3 = '';

                    var trHTML4 = '';

                    var trHTML5 = '';

                    var trHTML6 = '';

                    var trHTML7 = '';

                    var trHTML8 = '';

                    var trHTML9 = '';

                    var trHTML10 = '';

                    var trHTML11 = '';

                    var trHTML12 = '';

                    var trHTML13 = '';

                    var trHTML14 = '';

                    var trHTML15 = '';

                    var trHTML16 = '';



//                                        var ide =

//                                                $('#outlet_name').val('');

//                                        $('#company_name').val('');

//                                        //  $('#categoryide option:selected').val('');

//                                        $('#numlocation').val('');

////                                        $('#termlocatione option:selected').val('');

//                                        $('#termlocationee ').val('');

//                                        $('#sqft').val('');

//                                        $('#exp').val('');





                    $('#requestcompany_e').val('');

                    $('#afname_e').val('');

                    $('#aaddress1_e').val('');

                    $('#accity_e').val('');

                    $('#aczip_e').val('');

                    $('#aphone1_e').val('');

                    $('#aemail_e').val('');

                    $('#apriority_e').val('');

                    $('#apriority_old_e').val('');

                    $('#atitle_e').val('');

                    $('#acompany_e').val('');

                    $('#alname_e').val('');

                    $('#aaddress2_e').val('');

                    $('#acstate_e').val('');

                    $('#accountry_e').val('');

                    $('#afax1_e').val('');

                    $('#awebsite_e').val('');



//                                        $('#airport_contact_id').val(acid1);





                    $.each(response, function (i, value) {



                        trHTML1 += $response[0][0];

                        trHTML2 += $response[0][1];

                        trHTML3 += $response[0][2];

                        trHTML4 += $response[0][3];

                        trHTML5 += $response[0][4];

                        trHTML6 += $response[0][5];

                        trHTML7 += $response[0][6];

                        trHTML8 += $response[0][7];

                        trHTML9 += $response[0][8];

                        trHTML10 += $response[0][9];

                        trHTML11 += $response[0][10];

                        trHTML12 += $response[0][11];

                        trHTML13 += $response[0][12];

                        trHTML14 += $response[0][13];

                        trHTML15 += $response[0][14];

                        trHTML16 += $response[0][15];

//alert(trHTML1 + "/" + trHTML2 + "/" + trHTML3); 

                    });





                    $('#requestcompany_e').val(trHTML1);

                    $('#requestcompany_e ').trigger('chosen:updated');

                    $('#afname_e').val(trHTML2);

                    $('#aaddress1_e').val(trHTML3);

                    $('#accity_e').val(trHTML4);

                    $('#aczip_e').val(trHTML5);

                    $('#aphone1_e').val(trHTML6);

                    $('#aemail_e').val(trHTML7);

                    $('#atitle_e').val(trHTML8);

                    $('#acompany_e').val(trHTML9);

                    $('#alname_e').val(trHTML10);

                    $('#aaddress2_e').val(trHTML11);

                    $('#acstate_e').val(trHTML12);

                    $('#acstate_e').trigger('chosen:updated');

                    $('#accountry_e').val(trHTML13);

                    $('#accountry_e ').trigger('chosen:updated');

                    $('#afax1_e').val(trHTML14);

                    $('#awebsite_e').val(trHTML15);

                    $('#apriority_e').val(trHTML16);

                    $('#apriority_old_e').val(trHTML16);



                    $('#airport_contact_id').val(acid1);





                    showmodaledit(1);

                }



            });







        }





    </script> 

    <script type="text/javascript">



        function checkval()

        {

            var rfield = new Array("mgtresponsibility", "afname", "alname", "atitle", "acompany", "aaddress1", "accity", "aczip", "accountry", "acstate", 'aphone1', 'afax1', 'aemail', 'awebsite');

            var msg = new Array('Select Management Responsibility', 'First Name', 'Last Name', 'Title', 'Company', 'Address', 'City', 'Zip', 'Country', 'State', 'Phone', 'Fax', 'Email', 'Website');

            var errmsg = "";

            for (i = 0; i < rfield.length; i++)

            {

                //alert(rfield[i]);

                var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                if (val == "" || val.replace(" ", "") == "")

                {

                    errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

                }

            }

            if (errmsg != "")

            {

                $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                return false;

            }

            return true;

        }



        function checkplace()

        {



            var aid = $("#aaid").val();

            var priority = $("#apriority").val();



            $.ajax({

                url: "<?php echo site_url('frontend/checkplace'); ?>",

                type: "POST",

                cache: false,

                data: "aid=" + aid + "&priority=" + priority,

                success: function (html)

                {



                    if (html == -1)

                    {

                        window.location = "<?php echo current_url(); ?>";

                        return false;

                    } else

                    {

                        return true;

                    }

                }

            });



        }







        function checkval_e()

        {



            var rfield = new Array("mgtresponsibility", "afname", "alname", "atitle", "acompany", "aaddress1", "accity", "aczip", "accountry", "acstate", 'aphone1', 'afax1', 'aemail', 'awebsite');

            var msg = new Array('Select Management Responsibility', 'First Name', 'Last Name', 'Title', 'Company', 'Address', 'City', 'Zip', 'Country', 'State', 'Phone', 'Fax', 'Email', 'Website');

            var errmsg = "";





            for (i = 0; i < rfield.length; i++)

            {

                var val = document.forms["contactform_edit"][rfield[i]].value;

                if (val == "" || val.replace(" ", "") == "")

                {

                    errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

                }

            }

            if (errmsg != "")

            {

                $(".error_e").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                $('html, body, modal').animate({scrollTop: $('.error_e').offset().top}, 'slow');

                return false;

            }

            return true;

        }



        function empty_message() {



            $(".error_e").html("");



        }



        jQuery(document).ready(function () {

            jQuery(".chosen").chosen();

        });















    </script>

</body>