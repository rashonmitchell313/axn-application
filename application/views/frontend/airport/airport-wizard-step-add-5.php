<?php
echo $this->load->view($header);
?>



<title>

<?php
//    if ($this->uri->segment(0) != '') {
//        echo 'Modify Data Step 5 | ARN Fact Book ' . $this->uri->segment(0);
//    } else {
//        echo 'MODIFY DATA Step 5 | ARN Fact Book';
//    }
?>

</title>



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables2.bootstrap.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >



<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/chosen.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/bootstrap-datetimepicker.css">



<!--<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">-->



<style>



.dataTables_wrapper .dataTables_paginate .paginate_button {



padding : 0px;



margin-left: 0px;



display: inline;



border: 0px;



}



.dataTables_wrapper .dataTables_paginate .paginate_button:hover {



border: 0px;



}



</style>



</head>

<body>

<div class="main-wrapper">

<?php echo $this->load->view($menu); ?>

<div class="clearfix"></div>

<section class="main-content">

<?php /* ?><section class="col-sm-2">

<?php foreach($modify_airport_adds as $adds): ?>

<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

<?php endforeach; ?>

</section><?php */ ?>		     	

<section class="container-fluid">

<?php echo $this->load->view('frontend/include/breadcrumb'); ?>

<div class="error">

<?php echo $this->common->getmessage(); ?>

</div>

<div class="row">

<div class="col-sm-12" style="font-family:verdana;">
<div class="arport-btns-holder">

<div class="btn-group btn-group-justified" role="group" aria-label="...">

<div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "1") {

echo " btn-warning";
} else {

echo " btn-primary";
}
?>">

<u>Step 1</u><br />CONTACTS 

</a>                                

</div>



<div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "2") {

echo " btn-warning";
} else {

echo " btn-primary";
}
?>">

<u>Step 2</u><br /> AIRPORT INFO

</a>                                

</div>



<div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "3") {

echo " btn-warning";
} else {

echo " btn-primary";
}
?>">

<u>Step 3</u><br /> TERMINALS

</a>                                

</div>



<div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "4") {

echo " btn-warning";
} else {

echo " btn-primary";
}
?>">

<u>Step 4</u><br /> RENTAL & PARKING

</a>                                

</div>



<div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "5") {

echo " btn-warning";
} else {

echo " btn-primary";
}
?>">

<u>Step 5</u><br /> TENANTS

</a>                                

</div>



<div class="btn-group" role="group">

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "6") {

echo " btn-warning";
} else {

echo " btn-primary";
}
?>">

<u>Step 6</u><br /> LOCATION

</a>                                

</div>

</div>
</div>

</div>

</div>		

</br>

</br>

<div class="row">

<div class="col-sm-8">

<h4><p class="text-default"><strong><?php echo $main_data[0]['aname'] . ' (' . $main_data[0]['IATA'] . ')'; ?></strong></p>

<p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>

</div>

</div>



</br>



<div class="row">

<div class="col-sm-12">

<div class="panel panel-primary">

<div class="panel-heading">

<h4>Tenant Information</h4>

</div>

<div class="panel-body">

<?php
$step = $this->uri->segment(4);

$aid = $this->uri->segment(5);
?>

<!--<form method="post"  class="form-horizontal" role="form" name="contactform"  onSubmit="return checkval();"  action="<?php // echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid;                         ?>">-->   

<form method="post"  class="form-horizontal" role="form" name="contactform" id="addtenant" data-toggle="validator"  action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>">   

<!--<div class="table-responsive addate">-->



<!--<div class="col-sm-12">-->



<div class="col-sm-9 margin-auto">

<div class="col-sm-5">





<div class="form-group has-feedback">

    <label for="exampleInputEmail1" class="col-sm-4 control-label"><span class="text-danger">*</span><b>Tenant Name</b></label>

    <div class="col-sm-8">

        <input type="text" name="outletname" value="" class="form-control" id="" data-error="Tenant Name is invalid" required>

    </div>

    <div class="col-sm-8 help-block with-errors"></div>

</div>

<div class="form-group has-feedback">

    <label for="exampleInputPassword1" class="col-sm-4 control-label"><span class="text-danger">*</span><b>Category</b></label>

    <div class="col-sm-8">

        <select name="categoryid" id="categoryid" class="chosen form-control" data-error="Category is invalid" required>

            <option value="">Please Select</option>

            <?php
            foreach ($con_mgt_type as $x) {
                ?>

                <option  value="<?php echo $x['categoryid'] ?>"  > <?php echo $x['category'] ?> || <?php echo $x['productdescription']; ?></option> <?php echo "\n"; ?>

                <?php
            }
            ?>

        </select>

    </div>

    <div class="col-sm-8 help-block with-errors"></div>

</div>

<div class="form-group">

    <label for="exampleInputPassword1" class="col-sm-4 control-label">#Locations</label>

    <div class="col-sm-8">

        <input type="text" name="numlocations" value="" class="form-control" id="">

    </div>

</div>

<div class="form-group">

    <label for="exampleInputPassword1" class="col-sm-4 control-label">Expires</label>

    <div class="col-sm-8">

        <input type="text" name="exp" value="" class="form-control datetimepicker41">

    </div>

</div>



<div class="form-group" id="exp_dated" style="display:none;">

    <label for="inputEmail3" class="col-sm-4 control-label">Expire Date:</label>

    <div class="col-sm-8">

        <input type="text" name="exp_date" id="exp_date" placeholder="Click for set Date & Time" class="form-control datetimepicker42" />

    </div>

</div>



</div>



<div class="col-sm-1"> </div>



<div class="col-sm-5 pull-left">





<div class="form-group">

    <label for="exampleInputEmail1" class="col-sm-4 control-label">Company</label>

    <div class="col-sm-8">

        <input type="text" name="companyname" value="" class="form-control" id="">

    </div>

</div>

<div class="form-group">

    <label for="exampleInputEmail1" class="col-sm-4 control-label">Terminal</label>

    <div class="col-sm-8">

        <select name="termlocation" id="termlocation" class="chosen form-control">

            <option   value="0">Select</option>

            <?php
            foreach ($terminals as $x) {
                ?>

                <option  value="<?php echo $x['terminalabbr']; ?>"  > <?php echo $x['terminalabbr']; ?> </option> <?php echo "\n"; ?>

                <?php
            }
            ?>

        </select>

    </div>

</div>



<div class="form-group">

    <label for="exampleInputPassword1" class="col-sm-4 control-label">Sq. Ft.</label>

    <div class="col-sm-8">

        <input type="text" name="sqft" value="" class="form-control" id=""  >

    </div>

</div>

<div class="form-group">

    <label for="exampleInputPassword1" class="col-sm-4 control-label">Expire Options&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" title="When you select one of these options no date will be shown."></i></label>

    <div class="col-sm-8">

        <select name="exp_reason" id="exp_reasona" class="chosen form-control">

            <option value=""> &nbsp;</option>

            <option value="M-T-M">M-T-M</option>

            <option value="TBD">TBD</option>

            <option value="Permanently Closed">Permanently Closed</option>

            <option value="Temporarily Closed">Temporarily Closed</option>

            <option value="Holdover">Holdover</option>

            <option value="Temp">Temp</option>

        </select>

    </div>

</div>







</div>



<div class="col-sm-5 ">







<div class="col-sm-8 col-sm-offset-4">

    <div class="form-group">

        <p><strong><span class="text-danger">*</span>  = required information  </strong></p>

    </div>

    </br>

    <input type="hidden" name="step5add" value="form1">

    <div class="form-group">

        <button type="submit" class="btn btn-success "><i class="fa fa-check"></i>&nbsp;Save Change</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport'); ?>" class="btn btn-danger "><i class="fa fa-reply "></i>&nbsp;Cancel</a>

    </div>

</div>

</div>



</div>





</form>			

</div>

</div>

<!--                        <div class="panel panel-primary">

        <div class="panel-heading">

            <h4>Existing Tenants</h4>

        </div>

        

        <div class="panel-body">-->



<?php /* $cntr = 0; ?>

<?php foreach ($categories as $categoryName) { ?>

<!--<div class="panel panel-primary">-->

<div class="panel-heading"role="tab" id="dynamicconcession<?php echo $cntr; ?>">

<h4 class="panel-title">

<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight<?php echo $cntr; ?>" aria-expanded="false" aria-controls="collapseEight<?php echo $cntr; ?>">

<?php echo $get_category_name[$cntr]['category']; ?>

</a>

</h4>

</div>





<div id="collapseEight<?php echo $cntr; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dynamicconcession<?php echo $cntr; ?>">



<div class="pannel-body">









<table width="100%" id="sample-table-2"   class="table-bordered table-hover table-striped datatable">

<thead>



<tr>



<th style="width: 400px" >Tenant Name (Company)</th>



<th style="width: 400px">Company Name</th>



<th style="width: 400px">Product Description</th>



<th style="width: 400px">Terminal</th>



<th style="width: 400px">#</th>



<th style="width: 400px">Sq Ft</th>



<th style="width: 400px" >Expires</th>



<th style="width: 200px" >Action</th>



</tr>



</thead>



<tbody>





<?php

$aid = $this->uri->segment(5);

foreach ($categoryName as $categoryOptions) {

?>





<?php // foreach ($outlets as $o): ?>



<tr scop="row">





<td style="width: 400px">

<!--                                                     <a href='<?php  echo site_url('wizard/airport/airport_edit_step_5'); ?>/<?php echo $aid; ?>/<?php echo $categoryOptions['oid']; ?>' target="_new" title='Edit "<?php echo $categoryOptions['companyname']; ?>"'><i class="fa fa-pencil-square-o"></i></a>&nbsp;

<a href="javascript:void(0);" onClick="delme('<?php echo $categoryOptions['oid']; ?>')" >

<i class="fa fa-trash"></i></a>-->



<?php echo $categoryOptions['outletname']; ?></td>



<td style="width: 400px"><?php echo $categoryOptions['companyname']; ?></td>



<td style="width: 400px"><?php echo $categoryOptions['productdescription']; ?></td>



<td style="width: 400px"><?php echo $categoryOptions['termlocation']; ?></td>



<td style="width: 400px"><?php echo $categoryOptions['numlocations']; ?></td>



<td style="width: 400px"><?php echo $categoryOptions['sqft']; ?></td>



<!--<td style="width: 400px"><?php // echo $categoryOptions['exp']; ?></td>-->







<!--                                        //        <td><?php echo $o['outletname']; ?></td>



//   <td><?php echo $o['companyname']; ?></td>



//  <td><?php echo $o['productdescription']; ?></td>



//  <td><?php echo $o['termlocation']; ?></td>



//  <td><?php echo $o['numlocations']; ?></td>



//  <td><?php //echo number_format($o['sqft']); ?></td>-->



<td>



<?php

//                if (!$this->common->check_date($o['exp'])) {

//

//

//

//                    $date_val = $o['exp'];

//

//                } elseif (strtotime($o['exp']) == '') {

//

//

//

//                    $date_val = "00/00/0000";

//

//                } elseif (is_null($o['exp'])) {

//

//

//

//                    $date_val = "00/00/0000";

//                    echo 'test'.$data_val; exit;

//                } elseif ($o['exp'] == '0000-00-00') {

//

//

//

//                    $date_val = "00/00/0000";

//                } else {

//                                                                                        $date_val = date('m/d/Y', strtotime($o['exp']));



//                                                    $date_val = $this->common->get_formatted_datetime($o['exp']);

$date_val = $this->common->get_formatted_datetime($categoryOptions['exp']);

//                }







echo $date_val;

?>







</td>



<td>







<a href='<?php echo site_url('wizard/airport/airport_edit_step_5'); ?>/<?php echo $aid; ?>/<?php echo $categoryOptions['oid']; ?>' target="_new" class="btn btn-primary btn-sm" title="Edit this Tenant."<?php echo $categoryOptions['companyname']; ?>"'><i  class="fa fa-edit"></i></a>&nbsp;

<a href="javascript:void(0);" onClick="delme('<?php echo $categoryOptions['oid']; ?>')" class="btn btn-danger btn-sm" title="Delete this Tenant." >

<i class="fa fa-trash"></i></a>







<!--                                                    <button type="submit" class="btn btn-danger btn-sm" title="Edit this Tenant.">



<i class="fa fa-edit"></i>&nbsp;



</button>



<button type="submit" class="btn btn-danger btn-sm" title="Delete this Tenant.">



<i class="fa fa-trash"></i>&nbsp;



</button> -->

</td>



</tr>



<?php } ?>



</tbody>



</table>



</div>

</div>



<?php // } */ ?>   



<!--                        </div>

    </div>-->

<!--</div>-->





<div class="panel panel-primary">

<div class="panel-heading">

<h4>Existing Tenants</h4>

</div>



<div class="panel-body">

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">



<?php ?> <?php $cntr = 0; ?>

<?php foreach ($categories as $categoryName) { ?>

<div class="panel panel-primary">

<div class="panel-heading"role="tab" id="dynamicconcession<?php echo $cntr; ?>">

    <h4 class="panel-title">

        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight<?php echo $cntr; ?>" aria-expanded="false" aria-controls="collapseEight<?php echo $cntr; ?>">

            <?php echo $get_category_name[$cntr]['category']; ?>

        </a>

        <i class="fa fa-question-circle" data-original-title=" 
        <?php
        if ($get_category_name[$cntr]['category'] == 'Advertising') {
            echo 'Gross revenue and rent to the airport, where available, are provided. This category includes all revenue generated from advertising space at the airport that has been bought by companies paying for said space for the purpose of marketing their brand, including dioramas, wall space, banners, etc.';
        } elseif ($get_category_name[$cntr]['category'] == 'Duty Free') {
            echo 'Retail shops that do not apply local or national taxes and duties to products sold.';
        } elseif ($get_category_name[$cntr]['category'] == 'Food/Beverage') {
            echo 'Any store within an airport that utilizes more than 50% of its space to offer food and beverage products for sale to the public.';
        } elseif ($get_category_name[$cntr]['category'] == 'News/Gifts') {
            echo 'Any store within an airport that utilizes the majority of its space for the sale of news publications and gift items.';
        } elseif ($get_category_name[$cntr]['category'] == 'Passenger Services') {
            echo 'Refers to all non-retail or non-food services provided by the airport such as massage, ATMs, banking, business centers, barber shops, etc.';
        } elseif ($get_category_name[$cntr]['category'] == 'Specialty Retail') {
            echo 'A store that utilizes a majority of its space to sell non-news, non-food, retail products such as apparel, souvenirs, gift items, art products, jewelry, etc.';
        }
        ?>
           "
           data-container="body"data-toggle="tooltip"
           ></i>

    </h4>

</div>

<div id="collapseEight<?php echo $cntr; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dynamicconcession<?php echo $cntr; ?>">



    <div class="pannel-body">

        <!--<div class="table-responsive" style="height:450px;overflow:auto;">-->



        <h1 class="default-title"><?php echo $get_category_name[$cntr]['category']; ?></h1><br /> 



        <div class="table-responsive " >

                                                            <!--<table class="table table0 table-bordered table-hover table-striped datatable sample-table-2" id="sample-table-2"  class="display" width="100%" cellspacing="0" >-->

            <table class="table  table-bordered table-hover table-striped datatable sample-table-2" id="sample-table-2"  class="display" width="98%" cellspacing="0">

                <?php $cntr++; ?>

                <thead>

                    <tr class="info">



                        <th  >Tenant Name (Company)</th>



                        <th >Company Name</th>



                        <th >Product Description</th>



                        <th >Terminal</th>



                        <th ># Locations</th>



                        <th >Sq Ft</th>



                        <th >Expires Date/Options</th>



                        <th  >Action</th>



                    </tr>

                </thead>

                <tbody>



                    <?php
                    $aid = $this->uri->segment(5);

                    foreach ($categoryName as $categoryOptions) {

                        $aid = $this->uri->segment(5);
                        ?>

                        <tr>



                            <td style="width: 400px">

                                                                <!--                                                                <a href='<?php echo site_url('wizard/airport/airport_edit_step_5'); ?>/<?php echo $aid; ?>/<?php echo $categoryOptions['oid']; ?>' target="_new" title='Edit "<?php echo $categoryOptions['companyname']; ?>"'><i class="fa fa-pencil-square-o"></i></a>&nbsp;

                                                                                                                                <a href="javascript:void(0);" onClick="delme('<?php echo $categoryOptions['oid']; ?>')" >

                                                                                                                                    <i class="fa fa-trash"></i></a>-->



                                <?php echo $categoryOptions['outletname']; ?></td>



                            <td style="width: 400px"><?php echo $categoryOptions['companyname']; ?></td>



                            <td style="width: 400px"><?php echo $categoryOptions['productdescription']; ?></td>



                            <td style="width: 400px"><?php echo $categoryOptions['termlocation']; ?></td>



                            <td style="width: 400px"><?php echo $categoryOptions['numlocations']; ?></td>



                            <td style="width: 400px"><?php echo $categoryOptions['sqft']; ?></td>



                            <td style="width: 400px"><?php
                                if ($categoryOptions['exp_reason'] != '') {



//                                                                                $reasons = str_replace(array('M-T-M', 'TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $categoryOptions['exp_reason']);

                                    // $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $categoryOptions['exp_reason']);
                                    //$reasons = $categoryOptions['exp_reason'];
                                 // $reasons = str_replace(array('TBD', 'Permanently Closed', 'Temporarily Closed'), 'Closed', $categoryOptions['exp_reason']);
                                 $reasons = str_replace(array('Permanently Closed', 'Temporarily Closed'), 'Closed', $categoryOptions['exp_reason']);
                                 // $reasons = $categoryOptions['exp_reason'];



                                    if ($categoryOptions['exp_date'] != '0000-00-00 00:00:00' && $categoryOptions['exp_date'] != '' & $categoryOptions['exp_reason'] != 'M-T-M') {

                                        // echo $reasons . '<br>' . date("M d,Y H:i:s", strtotime($categoryOptions['exp_date']));
                                        echo $reasons . '<br>' . date("M d,Y", strtotime($categoryOptions['exp_date']));
                                    } else if (($categoryOptions['exp_date'] == '0000-00-00 00:00:00' || $categoryOptions['exp_date'] == '') && $categoryOptions['exp_reason'] != 'M-T-M') {

                                        echo $reasons;
                                    } else {

                                        echo $reasons;
                                    }



//                                                                                echo '<b>' . $reasons . '</b><br>' . date("M d,Y H:i:s", strtotime($categoryOptions['exp_date'])); 
                                } else {



                                    if ($categoryOptions['exp'] != '') {

                                        echo $this->common->get_formatted_datetime($categoryOptions['exp']);
                                    } else {

                                        echo '';
                                    }
                                }
                                ?></td>





                            <td style="width: 200px">

                                                                                                                                <!--<a href='<?php /* echo site_url('wizard/airport/airport_edit_step_5'); ?>/<?php echo $aid; ?>/<?php echo $categoryOptions['oid']; ?>' target="_new" class="btn btn-primary btn-sm" title="Edit this Tenant."<?php echo $categoryOptions['companyname'];// */ ?>"'><i  class="fa fa-edit"></i></a>&nbsp;-->

                                <a href="javascript:void(0);" onClick="editme('<?php echo $categoryOptions['oid']; ?>')"  class="btn btn-primary btn-sm" title="Edit the Tenant. ' <?php echo $categoryOptions['companyname']; ?>'"><i  class="fa fa-edit"></i></a>&nbsp;

                                <a href="javascript:void(0);" onClick="delme('<?php echo $categoryOptions['oid']; ?>')" class="btn btn-danger btn-sm" title="Delete the Tenant. ' <?php echo $categoryOptions['companyname']; ?>'" >

                                    <i class="fa fa-trash"></i></a></td>



                        </tr>

                    <?php } ?>

                </tbody>

            </table>

        </div>

    </div>

</div>

</div>

<?php } ?><?php ?>

</div>

</div>

</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">



<div class="modal-dialog">



<div class="modal-content">



<div class="modal-header">



<input type="hidden" id="arecdelid" value="<?php echo $this->uri->segment(5); ?>">



<input type="hidden" id="recdelid" value="">



<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>



</button>



<h4 class="modal-title" id="myModalLabel">Conifm Delete</h4>



</div>



<div class="modal-body">



Are you sure want to delete this?



</div>



<div class="modal-footer">



<button type="button" class="btn btn-danger" data-dismiss="modal">



<i class="fa fa-times">&nbsp;</i>Cancel</button>        



<button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete



</button>        



</div>



</div>



</div>



</div>     











<!--                        <div class="modal fade bs-example-modal-lg" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

        <div class="modal-dialog modal-lg">

                                            <div class="modal-content">

                                                ...

                                            </div>

                                        </div>

                                    </div>

            

-->                                                        
<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModaleditLabel" aria-hidden="true">



<div class="modal-dialog">



<div class="modal-content">



<div class="modal-header">



<input type="hidden" id="receditid" value="">



<!--                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>



</button>-->



<h4 class="modal-title" id="myModaleditLabel">Edit Tenant</h4>



</div>



<div class="modal-body">



<!--<form class="form-horizontal" method="post" onSubmit="return checkval1();" action="<?php // echo site_url('wizard/airport/airport_edit_step_5_') . '/' . $step . '/' . $aid;                      ?>">-->

<form class="form-horizontal" method="post" id="edittenant" data-toggle="validator" role="form" action="<?php echo site_url('wizard/airport/airport_edit_step_5_') . '/' . $step . '/' . $aid; ?>">



<div class="error_e">

    <?php echo $this->common->getmessage(); ?>

</div>

<input type="hidden" name="tenante_id" id="tenante_id" />



<div class="form-group">

    <label for="inputEmail3" class="col-sm-3 control-label">&nbsp;</label>

    <div class="col-sm-8">

        <div class="text-danger">* Indicates Required Information </div>

    </div>

</div>  

<div class="form-group has-feedback">

    <label for="inputEmail3" class="col-sm-3 control-label"><span class="text-danger">*</span> Tenant Name:</label>

    <div class="col-sm-8">

        <input type="text" name="outletnamee" id="outlet_name" value="" class="form-control" data-error="Tenant Name is invalid" required>

    </div>

    <div class="col-sm-8 help-block with-errors"></div>

</div>

<div class="form-group">

    <label for="inputEmail3" class="col-sm-3 control-label">Company:</label>

    <div class="col-sm-8">

        <input type="text" name="companyname" id="company_name"  class="form-control" >

    </div>

</div>



<div class="form-group has-feedback">

    <label for="inputEmail3" class="col-sm-3 control-label"><span class="text-danger">*</span> Category:</label>

    <div class="col-sm-8">

        <select name="categoryide" id="categoryide" class="chosen form-control" data-error="Category is invalid" required>

            <option   value="">-Please Select-</option>

            <?php
            foreach ($con_mgt_type as $x) {
                ?>

                <option  value="<?php echo $x['categoryid'] ?>" > <?php echo $x['category'] ?> || <?php echo $x['productdescription']; ?></option> <?php echo "\n"; ?>

                <?php
            }
            ?>

        </select>

    </div>

    <div class="col-sm-8 help-block with-errors"></div>

</div>



<div class="form-group">

    <label for="inputEmail3" class="col-sm-3 control-label"># Locations:</label>

    <div class="col-sm-8">

        <input type="text" name="numlocations" id="numlocation" class="form-control" >

    </div>

</div>



<div class="form-group">

    <label for="inputEmail3" class="col-sm-3 control-label">Terminal:</label>

    <div class="col-sm-8">

        <select name="termlocation" id="termlocationee" class="chosen form-control">

            <option   value="">-Please Select-</option>



            <?php
            foreach ($terminals as $x) {
                ?>

                <option  value="<?php echo $x['terminalabbr']; ?>"  > <?php echo $x['terminalabbr']; ?> </option> <?php echo "\n"; ?>

                <?php
            }
            ?>

        </select>

    </div>

</div>



<div class="form-group">

    <label for="inputEmail3" class="col-sm-3 control-label">Sq. Ft:</label>

    <div class="col-sm-8">

        <input type="text" name="sqft" id="sqft"  class="form-control" >

    </div>

</div>



<div class="form-group">

    <label for="inputEmail3" class="col-sm-3 control-label">Expires:</label>

    <div class="col-sm-8">

        <!--<input type="date" name="exp" id="exp"  class="form-control " data-provide="datepicker"  >-->

        <input type="text" name="exp" id="exp"  class="form-control datetimepicker4" />

    </div>

</div>

<div class="form-group">

    <label for="inputEmail3" class="col-sm-3 control-label">Expire Options:&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" title="When you select one of these options no date will be shown."></i></label>

    <div class="col-sm-8">

        <select name="exp_reason" id="exp_reason" class="chosen form-control exp_reasone">

            <option value="">&nbsp;</option>

            <option value="M-T-M">M-T-M</option>

            <option value="TBD">TBD</option>

            <option value="Permanently Closed">Permanently Closed</option>

            <option value="Temporarily Closed">Temporarily Closed</option>

            <option value="Holdover">Holdover</option>

            <option value="Temp">Temp</option>

        </select>

    </div>

</div>

<div class="form-group " id="exp_datede" style="display:none;">

    <label for="inputEmail3" class="col-sm-3 control-label">Expiry Date:</label>

    <div class="col-sm-8">

        <input type="text" name="exp_datee" id="exp_datee" placeholder="Click for set Date & Time" class="form-control datetimepicker421"  />

    </div>

</div>



<input type="hidden" name="step5edit" value="form2">

<div class="modal-footer">



    <button type="button" onClick="empty_message();" class="btn btn-danger" id="cancel" data-dismiss="modal">



        <i class="fa fa-times">&nbsp;</i>Cancel</button>        



    <button type="submit"  class="btn btn-success sureedit"><i class="fa fa-check">&nbsp;</i>Save Changes



    </button>        



</div>



</form>



</div>



</div>



</div>     



</div>

</section>

<div class="clearfix">
<div class="col-sm-12" style="font-family:verdana;">

<div class="btn-group btn-group-justified" role="group" aria-label="...">

<div class="btn-group" role="group">



<div class="col-sm-2 pull-left" style="padding-left: 0px;">

<label class="control-label">Next, click on </label>

<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
if ($this->uri->segment(4) == "6") {

    echo " btn-warning";
} else {

    echo " btn-primary";
}
?>">

    <u>Step 6</u><br /> LOCATION

</a>                

</div>

</div>  

</div>

</div>
</div>

</section>



<div class="clearfix"></div>       

<?php echo $this->load->view($footer); ?>

<div class="clearfix"></div>

<!--<script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>-->        

<!--<script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>-->



<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>



<script src="<?php echo base_url('fassests'); ?>/js/moment-with-locales.js" type="text/javascript"></script>

<script src="<?php echo base_url('fassests'); ?>/js/bootstrap-datetimepicker.js" type="text/javascript"></script>



<!--                        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>-->



<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>

<!--<script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>-->

<!--<script src="<?php echo base_url('assets'); ?>/js/jquery.dataTables.bootstrap.js"></script>-->

<script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

<script src="<?php echo base_url('fassests'); ?>/js/chosen.jquery.min.js"></script>



<script src="<?php echo base_url(); ?>assets/js/validator.js"></script>





<script>





</script>

<script type="text/javascript">



$.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="submit"], button):enabled';



$('#addtenant').validator();

$('#edittenant').validator();





var seg = "<?php echo $this->uri->segment(0) ?>";



if (seg != '') {

$('title').html("Modify Data Step 5 | ARN Fact Book " + seg);

}

else {

$('title').html("Modify Data Step 5 | ARN Fact Book");

}



// li with date picker

$('[data-toggle="tooltip"]').tooltip({'placement': 'top'});

$('.dropdown-menu input').click(function (e) {

e.stopPropagation();

});

$('.dropdown-menu li').click(function () {



$('.dropdown-toggle b').remove().appendTo($('.dropdown-toggle').text($(this).text()));

});





$('.exp_select').click(function () {

var val = $(this).attr('id');

$('#selectedexp').val(val);

});









$(function () {

$('.datetimepicker4').datetimepicker({
format: 'MM/DD/YYYY'

});







var d = new Date();

var month = d.getMonth() + 1;

var day = d.getDate();

var startDate = (month < 10 ? '0' : '') + month + '/' +
(day < 10 ? '0' : '') + day + '/' +
d.getFullYear();

//alert(startDate); return false;

$('.datetimepicker41').datetimepicker({
format: 'MM/DD/YYYY',
}).val(startDate);





var currentdate = new Date();

var year = currentdate.getFullYear();

var month = currentdate.getMonth() + 1;

var day = currentdate.getDate();

var hours = currentdate.getHours();

var minutes = currentdate.getMinutes();

var seconds = currentdate.getSeconds();

var datetime = (month < 10 ? '0' : '') + month + "/"

+ (day < 10 ? '0' : '') + day + '/'

+ year + " "

+ (hours < 10 ? '0' : '') + hours + ":"

+ minutes + ":"

+ seconds;



//$('.datetimepicker421').datetimepicker({

//                                    format: 'MM/DD/YYYY H:m:s'

//                                }).val(datetime);

$('.datetimepicker42').datetimepicker({
format: 'MM/DD/YYYY H:m:s'

}).val(datetime);



});





$('#exp_reasona').change(function () {

if ($(this).val() != '')

{

$('#exp_dated').show();

}

else

{

$('#exp_dated').hide();



}

});



$('.exp_reasone').change(function () {

if ($(this).val() != '')

{

$('#exp_datede').show();



}

else

{

$('#exp_datede').hide();



}

});





function checkval()

{

var rfield = new Array("outletname", "categoryid");

var msg = new Array('Tenant Name ', 'Category ');

var errmsg = "";

for (i = 0; i < rfield.length; i++)

{

//alert(rfield[i]);

var val = document.getElementsByName("" + rfield[i] + "")[0].value;

if (val == "" || val.replace(" ", "") == "")

{

errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

}

}

if (errmsg != "")

{

$(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

$('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

return false;

}

return true;

}

function checkval1()

{

var rfield = new Array("outletnamee", "categoryide");

var msg = new Array('Tenant Name ', 'Category ');

var errmsg = "";

for (i = 0; i < rfield.length; i++)

{

//alert(rfield[i]);

var val = document.getElementsByName("" + rfield[i] + "")[0].value;

//                                    alert(val);

if (val == "" || val.replace(" ", "") == "")

{

errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

}

}

if (errmsg != "")

{

$(".error_e").html("<div class='alert alert-danger'>" + errmsg + "</div>");

return false;

}

return true;

}



function empty_message() {



$(".error_e").html("");



}



function showmodal(bit)

{

if (bit == 1)

{

$("#myModal").modal("show");

} else

{

$("#myModal").modal("hide");

}

}

function delme(idd)

{

$("#recdelid").attr("value", idd);

showmodal(1);

}

$(".suredel").click(function ()

{

var aid = $("#arecdelid").val();

var oid = $("#recdelid").val();

$(this).html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Deleteing...');

$.ajax({
url: "<?php echo site_url('frontend/airport_delete_step_1'); ?>",
type: "POST",
cache: false,
data: "oid=" + oid + "&aid=" + aid + "&action=delstep5",
success: function (html)

{

if (html == 1)

{

showmodal(0);

window.location = "<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>";

} else

{

$(".modal-body").html(html);

}

}

});



});







function showmodaledit(bit)

{

if (bit == 1)

{

$("#myModaledit").modal({backdrop: 'static', keyboard: false});

} else

{

$("#myModaledit").modal("hide");

}

}



function editme(ide)

{

var ide1 = ide;

$.ajax({
dataType: "json",
url: "<?php echo site_url('ajax/edit_tanant'); ?>",
type: "POST",
cache: false,
data: "oid=" + ide,
success: function (response) {



var $response = $(response);



var trHTML1 = '';

var trHTML2 = '';

var trHTML3 = '';

var trHTML4 = '';

var trHTML5 = '';

var trHTML6 = '';

var trHTML7 = '';

var trHTML8 = '';

var trHTML9 = '';

//                                        var ide =

$('#outlet_name').val('');

$('#company_name').val('');

//  $('#categoryide option:selected').val('');

$('#numlocation').val('');

//                                        $('#termlocatione option:selected').val('');

$('#termlocationee ').val('');

$('#sqft').val('');

$('#exp').val('');

//                                        $('#exp_reason').val('');





$.each(response, function (i, value) {



trHTML1 += $response[0][0];

trHTML2 += $response[0][1];

trHTML3 += $response[0][2];

//                                          trHTML3 += "<option value=" + $response[0][2] + ">"+ $response[0][2] + "</option>";

trHTML4 += $response[0][3];

trHTML5 += $response[0][4];

trHTML6 += $response[0][5];

trHTML7 += $response[0][6];

trHTML8 += $response[0][7];

trHTML9 += $response[0][8];



});

//option:selected

$('#outlet_name').val(trHTML1);

$('#company_name').val(trHTML2);

$('#categoryide').val(trHTML3);

$('#categoryide').trigger('chosen:updated');

$('#numlocation').val(trHTML4);

$('#termlocationee ').val(trHTML5);

$('#termlocationee ').trigger('chosen:updated');

$('#sqft').val(trHTML6);

$('#exp').val(trHTML7);

$('#exp_reason').val(trHTML8);

$('#exp_reason').trigger('chosen:updated');

//                                        $('#exp_datee').val(trHTML9);

$('#tenante_id').val(ide1);

if ($('#exp_reason').val() != '')

{



$('#exp_datede').show();



if ($('#exp_datee').val() != '0000-00-00 00:00:00' && $('#exp_datee').val() != '')

{

    $('#exp_datee').val(trHTML9);

}

else

{

    var currentdate = new Date();

    var year = currentdate.getFullYear();

    var month = currentdate.getMonth() + 1;

    var day = currentdate.getDate();

    var hours = currentdate.getHours();

    var minutes = currentdate.getMinutes();

    var seconds = currentdate.getSeconds();

    var datetime = (month < 10 ? '0' : '') + month + "/"

            + (day < 10 ? '0' : '') + day + '/'

            + year + " "

            + (hours < 10 ? '0' : '') + hours + ":"

            + minutes + ":"

            + seconds;



    $('.datetimepicker421').datetimepicker({
        format: 'MM/DD/YYYY H:m:s'

    }).val(datetime);

}

}

else

{

var currentdate = new Date();

var year = currentdate.getFullYear();

var month = currentdate.getMonth() + 1;

var day = currentdate.getDate();

var hours = currentdate.getHours();

var minutes = currentdate.getMinutes();

var seconds = currentdate.getSeconds();

var datetime = (month < 10 ? '0' : '') + month + "/"

        + (day < 10 ? '0' : '') + day + '/'

        + year + " "

        + (hours < 10 ? '0' : '') + hours + ":"

        + minutes + ":"

        + seconds;



$('.datetimepicker421').datetimepicker({
    format: 'MM/DD/YYYY H:m:s'

}).val(datetime);

}

showmodaledit(1);

}

});

}











//                                        if ($('#exp_datee').val() == '')

//                                        {

////                                            $('#exp_datede').show();

//                                            $('.datetimepicker42').datetimepicker({

//                                                format: 'MM/DD/YYYY H:m:s'

//                                            });

//                                        }



function cleare_apend()

{

$("#categoryide").html("");

$("#termlocatione").html("");

$("#exp_reason").html("");

}

jQuery(document).ready(function () {

//                jQuery(".chosen").chosen();





$('.sample-table-2').dataTable({
//                                                                "scrollY": "350px",



"paging": true,
"pagingType": "simple_numbers",
"display": "inline",
"dom": 'T<"clear">lfrtip',
"oTableTools": {
"aButtons": [
"print",
{
    "sExtends": "collection",
    "sButtonText": "Save",
    "aButtons": ["copy", "csv", "xls", "pdf"]

}

],
"sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"

},
"oLanguage": {
"sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"



}



});



/*

$('#sample-table-2').dataTable({



responsive: true,



"processing": true,



"serverSide": true,



//                aoColumnDefs: [

//

//                    {"aTargets": [8], "bSortable": false}

//

//                ],



"ajax": "<?php // echo site_url('ajax/list_of_airport_tenants_pagi');                                                                     ?>",



"dom": 'T<"clear">lfrtip',



"tableTools": {



"sSwfPath": "<?php // echo base_url('assets/tabletools');                                                                     ?>/swf/copy_csv_xls_pdf.swf"



},



"oLanguage": {



"sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"



}



});*/

$.fn.dataTable.ext.errMode = function (settings, helpPage, message) {

console.log(message);

};



jQuery(".chosen").chosen();

});

</script>

</body>