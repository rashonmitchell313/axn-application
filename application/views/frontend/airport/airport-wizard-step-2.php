<?php 
	echo $this->load->view($header);
?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Modify Data Step 2 | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'MODIFY DATA Step 2 | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/dataTables.tableTools.css">
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>
        <section class="main-content">
				<?php /*?><section class="col-sm-2">
<?php foreach($modify_airport_adds as $adds): ?>
<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
<?php endforeach; ?>				
				</section><?php */?>		     	
        	<section class="container-fluid">
                    <?php echo $this->load->view('frontend/include/breadcrumb'); ?>
		 <div class="error">
		<?php echo $this->common->getmessage();?>
         </div>
</br>
</br>	
<div class="row">
<div class="col-sm-8">
	<p><strong>VERY IMPORTANT:</strong> <span class="text-danger">Do not use symbols like $ or , when entering numbers. All formatting will be applied automatically on the review page.</span></p>
	<h4><p style="margin-left: 140px;">Examples:</p></h4>
	<div class="col-sm-4">
	<p>Currency fields</p>
	<p>Quantity fields</p>
	<p>Comment fields</p>
	</div>
	<div class="col-sm-4">
	<p>4.56 will appear as $4.56</p>
	<p>1234567 will appear as 1,234,567</p>
	<p>Type any symbols needed</p>
	</div>
	</br>

	</br></br></br>
	</br></br>
	<h4><p class="text-default"><strong><?php echo $main_data[0]['aname']; ?></strong></p>
	<p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>
</div>
</div>
				</br>
				</br>
				<div class="row">
				<div class="col-sm-12">
				
<div class="panel panel-default">
    <div class="panel-body default-title">Airport Info</div>
</div>
</hr>

<form method="post"  class="form-horizontal"  name="contactform"  onSubmit="return checkval();" role="form"  action="<?php echo current_url(); ?>">   

<input type="hidden" name="airport_contact_id" value="<?php echo $this->uri->segment(5);?>"  >

<div class="col-sm-6">
<!--
<a href="<?php echo site_url('frontend/airport_add_steps_1');?>" name="addmorecontact" class="addmorecontact btn btn-primary" value="" id="">+ Add additional Contact</a>  -->


</br> 
</br>     
</br>            

         				  
 <div class="form-group">
 <label for="requestcompany" class="control-label col-sm-3" for="requestcompany"><span class="text-danger">*</span>Concessions Mgt. Type:</label>
 <div class="col-sm-8">
 <select name="mgtresponsibility" id="requestcompany" class="form-control">
		<option></option>
		<option <?php  if ($airport_contact[0]['mgtresponsibility'] == $x['lkpmgttype']  ) echo 'selected';?> value="<?php echo  $x['lkpmgttype'] ?>"><?php echo  $x['lkpmgttype'] ?></option>
		
</select>
</div>
</div>				  
				  
		  
	<div class="form-group">
			<label class="control-label col-sm-3" for="afname"><span class="text-danger">*</span>First Name:</label>
			<div class="col-sm-8">
				<input type="text" name="afname" class="form-control" id="afname" placeholder="Enter First Name" value="<?php  echo $airport_contact[0]['afname']; ?>">
			</div>
		</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="alname"><span class="text-danger">*</span>Last Name:</label>
			<div class="col-sm-8">
				<input type="text" name="alname" class="form-control" id="alname" placeholder="Enter Last Name" value="<?php  echo $airport_contact[0]['alname']; ?>">
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="atitle"><span class="text-danger">*</span>Title:</label>
			<div class="col-sm-8">
				<input type="text" name="atitle" class="form-control" id="atitle" placeholder="Enter Title" value="<?php  echo $airport_contact[0]['atitle']; ?>" >
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="acompany"><span class="text-danger">*</span>Company:</label>
			<div class="col-sm-8">
				<input type="text" name="acompany" class="form-control" id="acompany" placeholder="Enter Company" value="<?php  echo $airport_contact[0]['acompany']; ?>">
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="aaddress1"><span class="text-danger">*</span>Address 1:</label>
			<div class="col-sm-8">
				<input type="text" name="aaddress1" class="form-control" id="aaddress1" placeholder="Enter Address 1" value="<?php  echo $airport_contact[0]['aaddress1']; ?>">
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="aaddress2"><span class="text-danger">*</span>Address 2:</label>
			<div class="col-sm-8">
				<input type="text" name="aaddress2" class="form-control" id="aaddress2" placeholder="Enter Address 2" value="<?php  echo $airport_contact[0]['aaddress2']; ?>">
			</div>
	</div>
	
	
	<div class="form-group">
			<label class="control-label col-sm-3" for="accity"><span class="text-danger">*</span>City:</label>
			<div class="col-sm-8">
				<input type="text" name="accity" class="form-control" id="accity" placeholder="Enter City" value="<?php  echo $airport_contact[0]['accity']; ?>" >
			</div>
		</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="acstate"><span class="text-danger">*</span>State:</label>
			<div class="col-sm-8">
				<input type="text" name="acstate" class="form-control" id="acstate" placeholder="Enter State" value="<?php  echo $airport_contact[0]['acstate']; ?>">
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="aczip"><span class="text-danger">*</span>Zip:</label>
			<div class="col-sm-8">
				<input type="text" name="aczip" class="form-control" id="aczip" placeholder="Enter Zip" value="<?php  echo $airport_contact[0]['aczip']; ?>">
			</div>
		</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="accountry"><span class="text-danger">*</span>Country:</label>
			<div class="col-sm-8">
				<input type="text" name="accountry" class="form-control" id="accountry" placeholder="Enter Country" value="<?php  echo $airport_contact[0]['accountry']; ?>">
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="aphone1"><span class="text-danger">*</span>Phone:</label>
			<div class="col-sm-8">
				<input type="text" name="aphone1" class="form-control" id="aphone1" placeholder="Enter Phone" value="<?php  echo $airport_contact[0]['aphone']; ?>">
			</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-3" for="afax1"><span class="text-danger">*</span>Fax:</label>
			<div class="col-sm-8">
				<input type="text" name="afax1" class="form-control" id="afax1" placeholder="Enter Fax" value="<?php  echo $airport_contact[0]['afax']; ?>">
			</div>
	</div>
<div class="form-group">
			<label class="control-label col-sm-3" for="aemail"><span class="text-danger">*</span>E-mail:</label>
			<div class="col-sm-8">
				<input type="text" name="aemail" class="form-control" id="aemail" placeholder="Enter E-mail" value="<?php  echo $airport_contact[0]['aemail']; ?>">
			</div>
		</div>
<div class="form-group">
	<label class="control-label col-sm-3" for="awebsite"><span class="text-danger">*</span>Web site:</label>
	<div class="col-sm-8">
		<input type="text" name="awebsite" class="form-control" id="awebsite" placeholder="Enter Web site" value="<?php  echo $airport_contact[0]['awebsite']; ?>">
	</div>
</div>

<input type="hidden" name="MM_update" value="form1">
						<div class="form-group">
						<button type="submit" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php echo site_url('frontend/airport_edit_step_1');?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
						</div>
						</div>
                        <div class="space-4"></div> 
                        </form> 			
				</div>
				</div>
               
 </div>
            </section>
<div class="col-sm-12" style="font-family:verdana;">
                    	<div class="btn-group btn-group-justified" role="group" aria-label="...">
							  <div class="btn-group" role="group">
							  <div class="col-sm-2"></div>
							  <div class="col-sm-2">
<label class="control-label">Next, click on </label>
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="3") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 3</u><br /> TERMINALS
                                </a>               
								</div>
                              </div>  
                            </div>
    </div>
        </section>
<div class="clearfix"></div>       
     <?php echo $this->load->view($footer);?>
<div class="clearfix"></div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('fassests');?>/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets');?>/js/jquery.dataTables.bootstrap.js"></script>
<script src="<?php echo base_url('fassests');?>/js/dataTables.tableTools.js" type="text/javascript"></script>
<script>
$(".addmorecontact").click(function()
{
$("#addmorecontacthidden").val(1);		
});

</script>
<script type="text/javascript">
 
 function checkval()
 {
	var rfield = new Array("configuration","mgtstructure","texpansionplanned[]", "addsqft[]", "completedexpdate[]");
	var msg=new Array('Airport Configuration','Concessions Mgt. Type','Expansion Planned','Addl Sq. Ft.','Complete Date');
	var errmsg="";
	for(i=0;i<rfield.length;i++)
	{
		//alert(rfield[i]);
		var val=document.getElementsByName(""+rfield[i]+"")[0].value;
	 	if(val=="" || val.replace(" ","")=="")
		{
			errmsg+="<b><i>"+msg[i]+" is Required. </i></b><br/>";
		}
	}	
	if(errmsg!="")
	{
		$(".error").html("<div class='alert alert-danger'>"+errmsg+"</div>");
		$('html, body').animate({scrollTop:$('.error').offset().top},'slow');
		return false;
	}
	 return true;
 }
</script>
</body>