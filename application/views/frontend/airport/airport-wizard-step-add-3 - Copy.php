<?php
echo $this->load->view($header);
?>

<title>

    <?php
    if ($this->uri->segment(0) != '') {

        echo 'Modify Data Step 3 | ARN Fact Book ' . $this->uri->segment(0);
    } else {

        echo 'Modify Data Step 3 | ARN Fact Book';
    }
    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">

            <?php /* ?><section class="col-sm-2">

              <?php foreach($modify_airport_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>

                <div class="error">

                    <?php echo $this->common->getmessage(); ?>

                </div>

                <div class="row">

                    <div class="col-sm-12" style="font-family:verdana;">
                        <div class="arport-btns-holder">

                            <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "1") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 1</u><br />CONTACTS 

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "2") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 2</u><br /> AIRPORT INFO

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "3") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 3</u><br /> TERMINALS

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "4") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 4</u><br /> RENTAL & PARKING

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "5") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 5</u><br /> TENANTS

                                    </a>                                

                                </div>



                                <div class="btn-group" role="group">

                                    <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                                    if ($this->uri->segment(4) == "6") {

                                        echo " btn-warning";
                                    } else {

                                        echo " btn-primary";
                                    }
                                    ?>">

                                        <u>Step 6</u><br /> LOCATION

                                    </a>                                

                                </div>

                            </div>
                        </div>

                    </div>

                </div>		

                </br>

                </br>

                <div class="row">

                    <div class="col-sm-8">

                        <p><strong>VERY IMPORTANT:</strong> <span class="text-danger">Do not use symbols like $ or , when entering numbers. All formatting will be applied automatically on the review page.</span></p>

                        <h3><p style="margin-left: 15px;"><b>Examples:</b></p></h3>

                        <div class="col-sm-4">

                            <p>Currency fields</p>

                            <p>Quantity fields</p>

                            <p>Comment fields</p>

                        </div>

                        <div class="col-sm-4">

                            <p>4.56 will appear as $4.56</p>

                            <p>1234567 will appear as 1,234,567</p>

                            <p>Type any symbols needed</p>

                        </div>

                        <div class="define-yyc-intro">
                            <h4><p class="text-default"><strong><?php echo $main_data[0]['aname'] . ' (' . $main_data[0]['IATA'] . ')'; ?></strong></p>

                                <p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>
                        </div>

                    </div>

                </div>



                </br>



                <div class="row">

                    <div class="col-sm-12">

                        <div class="panel panel-primary">

                            <div class="panel-heading">

                                <h4>Airport Info</h4>

                            </div>

                            <div class="panel-body">

                                <?php
                                $step = $this->uri->segment(4);

                                $aid = $this->uri->segment(5);
                                ?>

                                <form method="post"  class="form-horizontal" role="form" name="contactform"  onSubmit="return checkval();"  action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>">   

                                    <div class="table-responsive">

                                        <h4><p class="text-danger"> Please follow steps 1, 2 and 3 for each terminal </p></h4>

                                        <p> <strong class="text-danger">1. Select a Terminal to Modify Data:</strong>(Click on the icon <b><i class="fa fa-edit"></i></b> Edit Terminal next to desired terminal)  </p>

                                        <table class="table table-hover" >

                                            <thead>

                                                <tr>

                                                    <td><b>Terminal</b></td>

                                                    <td><b>Total</b></td>

                                                    <td><b>+/- %</b></td>

                                                    <td><b>Deplaning</b></td>

                                                    <td><b>Enplaning</b></td>

                                                    <td><b>EP Domestic</b></td>

                                                    <td><b>EP Int'l</b></td> 

                                                </tr>

                                            </thead>

                                            </tbody>



                                            <?php
                                            $aid = $this->uri->segment(5);

                                            foreach ($wideInfo_traffic as $wideInfo_traffic) {
                                                ?>

                                                      <tr>



                                                    <td><a href='<?php echo site_url('wizard/airport/airport_edit_step_3'); ?>/<?php echo $aid; ?>/<?php echo $wideInfo_traffic['tid']; ?>'  title='Edit "<?php echo $wideInfo_traffic['terminalabbr']; ?>"'><i class="fa fa-edit"></i> &nbsp;Edit</a>



                                                        <?php echo $wideInfo_traffic['terminalabbr']; ?></td>

                                                    <td><?php
                                                        if ($wideInfo_traffic['tpasstraffic'] != '') {

                                                            echo number_format($wideInfo_traffic['tpasstraffic']);
                                                        }

                                                        $tot_tpasstraffic+= $wideInfo_traffic['tpasstraffic'];
                                                        ?></td>

                                                    <td ><?php
                                                        echo $wideInfo_traffic['tpasstrafficcompare'];

                                                        $tot_tpasstrafficcompare+= $wideInfo_traffic['tpasstrafficcompare'];
                                                        ?></td>



                                                    <td ><?php
                                                        if ($wideInfo_traffic['tdeplaning'] != '') {

                                                            echo number_format($wideInfo_traffic['tdeplaning']);
                                                        }

                                                        $tot_tdeplanin+= $wideInfo_traffic['tdeplaning'];
                                                        ?></td>



                                                    <td ><?php
                                                        if ($wideInfo_traffic['tenplaning'] != '') {

                                                            echo number_format($wideInfo_traffic['tenplaning']);
                                                        }

                                                        $tot_tenplaning+= $wideInfo_traffic['tenplaning'];
                                                        ?></td>



                                                    <td><?php
                                                        if ($wideInfo_traffic['tepdomestic'] != '') {

                                                            echo number_format($wideInfo_traffic['tepdomestic']);
                                                        }

                                                        $tot_tepdomestic+=$wideInfo_traffic['tepdomestic'];
                                                        ?></td>



                                                    <td ><?php
                                                        if ($wideInfo_traffic['tepintl'] != '') {

                                                            echo number_format($wideInfo_traffic['tepintl']);
                                                        }

                                                        $tot_tepintl+= $wideInfo_traffic['tepintl'];
                                                        ?></td>

                                                </tr>

                                            <?php } ?>



                                            <tr class="info">



                                                <td><b>Totals</b></td>



                                                <td><b><?php echo number_format($tot_tpasstraffic); ?></b></td>



                                                <!--<td><b><?php // echo $tot_tpasstrafficcompare;  ?></b></td>-->



                                                <td><b><?php echo $wideInfo_traffic['apasstrafficcompare']; ?></b></td>



                                                <td><b><?php echo number_format($tot_tdeplanin); ?></b></td>



                                                <td><b><?php echo number_format($tot_tenplaning); ?></b></td>



                                                <td><b><?php echo number_format($tot_tepdomestic); ?></b></td>



                                                <td><b><?php echo number_format($tot_tepintl); ?></b></td>

                                            </tr>



                                            </tbody>



                                        </table>

                                    </div>

                                    <form method="post"  class="form-horizontal" role="form" name="contactform"  onSubmit="return checkval();"  action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>"> 

                                        <p> <strong class="text-danger">2. You are modifying data for (N/A) NO TERMINAL LOADED :</strong></p>			



                                        </div>

                                        </div>

                                        <div class="panel panel-primary">

                                            <div class="panel-heading">

                                                <h4>Airport Totals</h4>

                                            </div>

                                            <div class="panel-body">

                                                <div class="table-responsive">

                                                    <h4><p class="text-danger"> 4. Airport Passenger Traffic Totals</p></h4>



                                                    <table class="table" >

                                                        <thead>

                                                            <tr>

                                                                <td><b>IATA</b></td>

                                                                <td><b>Passenger Traffic</b></td>

                                                                <td><b>+/- %</b> &nbsp; <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="A comparison of numbers from the current Fact Book year to the previous. Currently 2016 numbers compared to 2015. All symbols should be typed included. i.g. +2.4 % or -3.1 % ."></i></td>

                                                                <td><b>Deplaning</b> 
                                                                    <i class="fa fa-question-circle" data-original-title="The number of people disembarking an airline and arriving through the airport to baggage claim or ground transportation." 
                                                                       data-container="body"data-toggle="tooltip"></i>
                                                                </td>

                                                                <td><b>Enplaning</b>
                                                                    <i class="fa fa-question-circle" data-original-title="Passengers departing the airport on airlines leaving the airport to their predetermined destinations." 
                                                                       data-container="body"data-toggle="tooltip"></i>
                                                                </td>

                                                                <td><b>EP Domestic</b></td>

                                                                <td><b>EP Int'l</b></td> 

                                                            </tr>

                                                        </thead>

                                                        </tbody>

                                                        <tr>

                                                            <td align="left" style="border-top: none !important;">

                                                                <div class="col-sm-12">

                                                                    <input type="text" name="IATA" value="<?php echo $wideInfo_traffic['IATA']; ?>" class="form-control" id="" readonly>

                                                                </div>

                                                            </td>

                                                            <td align="left" style="border-top: none !important;">

                                                                <div class="col-sm-12">

                                                                    <input type="text" name="apasstraffic" value="<?php echo number_format($wideInfo_traffic['apasstraffic']); ?>" class="form-control" id="" readonly>

                                                                </div>

                                                            </td>

                                                            <td align="left" style="border-top: none !important;">

                                                                <div class="col-sm-12">

                                                                    <input type="text" name="apasstrafficcompare" value="<?php echo $wideInfo_traffic['apasstrafficcompare']; ?>" class="form-control" id="">

                                                                </div>

                                                            </td>

                                                            <td align="left" style="border-top: none !important;">

                                                                <div class="col-sm-12">

                                                                    <input type="text" name="adeplaning" value="<?php echo number_format($wideInfo_traffic['adeplaning']); ?>" class="form-control" id="" readonly >

                                                                </div>

                                                            </td>

                                                            <td align="left" style="border-top: none !important;">

                                                                <div class="col-sm-12">

                                                                    <input type="text" name="aenplaning" value="<?php echo number_format($wideInfo_traffic['aenplaning']); ?>" class="form-control" id="" readonly >

                                                                </div>

                                                            </td>

                                                            <td align="left"  style="border-top: none !important;">

                                                                <div class="col-sm-12">

                                                                    <input type="text" name="aepdomestic" value="<?php echo number_format($wideInfo_traffic['aepdomestic']); ?>" class="form-control" id="" readonly >

                                                                </div>

                                                            </td>

                                                            <td align="left" style="border-top: none !important;">

                                                                <div class="col-sm-12">

                                                                    <input type="text" name="aepintl" value="<?php echo number_format($wideInfo_traffic['aepintl']); ?>" class="form-control" id="" readonly >

                                                                </div>

                                                            </td>



                                                        </tr>

                                                        <tr  style="border-top: none !important;" colspan="7">

                                                            <td colspan="5">

                                                        <center>type in symbols example: +1.45 %</center>

                                                        </td>

                                                        </tr>



                                                        </tbody>



                                                    </table>

                                                    <p> <strong class="text-danger">Passenger Traffic Comment</strong></p>

                                                    <div class="form-group">

                                                        <textarea class="form-control" name="apasscomment" rows="2" id=""><?php echo $wideInfo_traffic['apasscomment']; ?></textarea>

                                                    </div>



                                                    <p> <strong class="text-danger">5. Airport totals from all Terminals combined:</strong></p>

                                                    <div class="table-responsive">

                                                        <table class="table table-hover" >

                                                            <tr style="border-top: none !important;">

                                                                <td  style="border-top: none !important;">&nbsp;</td>

                                                                <td  style="border-top: none !important;"><b>Current Sq. Ft.</b></td>

                                                                <td  style="border-top: none !important;"><b>Gross Sales</b></td> 

                                                                <td  style="border-top: none !important;"><b>Sales/EP</b> &nbsp; <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="The estimated amount of money each departing passenger is spending before boarding their flight. This figure is a standard measure of performance in the industry and is derived by dividing the total gross sales figure by the number of enplaning passengers."></i></td> 

                                                                <td  style="border-top: none !important;"><b>Gross Rentals</b>  &nbsp; <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title=" The amount of revenue received by the airport in the form of rent paid by concessionaires. "></i></td> 

                                                                <td  style="border-top: none !important;"><b>Rent/EP</b></td> 

                                                            </tr>

                                                            <tr>

                                                                <td align="right" style="border-top: none !important;">							

                                                                    <div class="form-group">

                                                                        <label for="inputEmail3" class="col-sm-12 control-label">Food & Beverage:</label>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="afbcurrsqft" value="<?php echo number_format($wideInfo_traffic['afbcurrsqft']); ?>" class="form-control" id="" readonly>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="afbgrosssales" value="<?php echo number_format($wideInfo_traffic['afbgrosssales']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="afbsalesep" value="<?php echo $wideInfo_traffic['afbsalesep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="afbrentrev" value="<?php echo number_format($wideInfo_traffic['afbrentrev']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="afbrentep" value="<?php echo $wideInfo_traffic['afbrentep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td align="right" style="border-top: none !important;">							

                                                                    <div class="form-group">

                                                                        <label for="inputEmail3" class="col-sm-12 control-label">Specialty Retail:</label>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="asrcurrsqft" value="<?php echo number_format($wideInfo_traffic['asrcurrsqft']); ?>" class="form-control" id="" readonly>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="asrgrosssales"   value="<?php echo number_format($wideInfo_traffic['asrgrosssales']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="asrsalesep"   value="<?php echo $wideInfo_traffic['asrsalesep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="asrrentrev"   value="<?php echo number_format($wideInfo_traffic['asrrentrev']); ?>" class="form-control" id=""  readonly="readonly">

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="asrrentep"   value="<?php echo $wideInfo_traffic['asrrentep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td align="right" style="border-top: none !important;">							

                                                                    <div class="form-group">

                                                                        <label for="inputEmail3" class="col-sm-12 control-label">News & Gifts:</label>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="angcurrsqft" value="<?php echo number_format($wideInfo_traffic['angcurrsqft']); ?>" class="form-control" id="" readonly>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="anggrosssales" value="<?php echo number_format($wideInfo_traffic['anggrosssales']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="angsalesep" value="<?php echo $wideInfo_traffic['angsalesep']; ?>" class="form-control" id=""  readonly="readonly">

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="angrentrev" value="<?php echo number_format($wideInfo_traffic['angrentrev']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="angrentep" value="<?php echo $wideInfo_traffic['angrentep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td align="right" style="border-top: none !important;">							

                                                                    <div class="form-group">

                                                                        <label for="inputEmail3" class="col-sm-12 control-label">Duty Free:</label>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="adfcurrsqft" value="<?php echo number_format($wideInfo_traffic['adfcurrsqft']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="adfgrosssales" value="<?php echo number_format($wideInfo_traffic['adfgrosssales']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="adfsalesep" value="<?php echo $wideInfo_traffic['adfsalesep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="adfrentrev" value="<?php echo number_format($wideInfo_traffic['adfrentrev']); ?>" class="form-control" id="" readonly>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="adfrentep" value="<?php echo $wideInfo_traffic['adfrentep']; ?>" class="form-control" id=""  readonly="readonly">

                                                                    </div>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td colspan="7">&nbsp;



                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td colspan="7">&nbsp;



                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td align="right" style="border-top: none !important;">							

                                                                    <div class="form-group">

                                                                        <label for="inputEmail3" class="col-sm-12 control-label">Airport Totals (Excluding Duty Free):</label>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="acurrsqft" value="<?php echo number_format($wideInfo_traffic['acurrsqft']); ?>" class="form-control" id="" readonly>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="aconcessiongrosssales" value="<?php echo number_format($wideInfo_traffic['aconcessiongrosssales']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="asalesep" value="<?php echo $wideInfo_traffic['asalesep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="arentrev" value="<?php echo number_format($wideInfo_traffic['arentrev']); ?>" class="form-control" id=""  readonly="readonly">

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="arentep" value="<?php echo $wideInfo_traffic['arentep']; ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                            </tr>



                                                            <tr>

                                                                <td align="right" style="border-top: none !important;">							

                                                                    <div class="form-group">

                                                                        <label for="inputEmail3" class="col-sm-12 control-label">Airport Totals (Including Duty Free):</label>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="acurrsqft_incl_df" value="<?php echo number_format($wideInfo_traffic['acurrsqft_incl_df']); ?>" class="form-control" id=""readonly="readonly" >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="aconcessiongrosssales_incl_df" value="<?php echo number_format($wideInfo_traffic['aconcessiongrosssales_incl_df']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="asalesep_incl_df" value="<?php echo $wideInfo_traffic['asalesep_incl_df']; ?>" class="form-control" id="" readonly>

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="arentrev_incl_df" value="<?php echo number_format($wideInfo_traffic['arentrev_incl_df']); ?>" class="form-control" id="" readonly >

                                                                    </div>

                                                                </td>

                                                                <td align="left" style="border-top: none !important;">

                                                                    <div class="col-sm-12">

                                                                        <input type="text" name="arentep_incl_df" value="<?php echo $wideInfo_traffic['arentep_incl_df']; ?>" class="form-control" id=""  readonly="readonly">

                                                                    </div>

                                                                </td>

                                                            </tr>

                                                        </table>

                                                        <hr>

                                                        <p> <strong class="text-danger">Gross Sales Comment</strong></p>

                                                        <div class="form-group">

                                                            <textarea class="form-control" name="grosscomment" rows="2"  id=""><?php echo $wideInfo_traffic['grosscomment']; ?></textarea>

                                                        </div>

                                                    </div>	

                                                </div>	

                                                </br>

                                                <input type="hidden" name="step4_step5_step6" value="form1">

                                                <div class="form-group">

                                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Save Change</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport'); ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                                                </div>

                                            </div>

                                        </div>

                                        <!-- hih there -->

                                        </br>



                                        </div>

                                        <div class="space-4"></div> 

                                    </form>

                            </div>

                        </div>

                    </div>

            </section>

            <div style="clear: both;"></div>

            <div class="col-sm-12" style="font-family:verdana;">

                <div class="btn-group btn-group-justified" role="group" aria-label="...">

                    <div class="btn-group" role="group">



                        <div class="col-sm-2 pull-left" style="padding-left: 0px;">

                            <label class="control-label">Next, click on </label>

                            <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php
                            if ($this->uri->segment(4) == "4") {

                                echo " btn-warning";
                            } else {

                                echo " btn-primary";
                            }
                            ?>">

                                <u>Step 4</u><br /> RENTAL & PARKING

                            </a>               

                        </div>

                    </div>  

                </div>

            </div>

        </section>







        <div class="clearfix"></div>       

        <?php echo $this->load->view($footer); ?>

        <div class="clearfix"></div>

        <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        

        <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url('assets'); ?>/js/jquery.dataTables.bootstrap.js"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

        <script>





        </script>

        <script type="text/javascript">



            function checkval()

            {

                var rfield = new Array("mgtresponsibility", "afname", "alname", "atitle", "acompany", "aaddress1", "accity", "aczip", "accountry", "acstate", 'aphone1', 'afax1', 'aemail', 'awebsite');

                var msg = new Array('Select Management Responsibility', 'First Name', 'Last Name', 'Title', 'Company', 'Address', 'City', 'Zip', 'Country', 'State', 'Phone', 'Fax', 'Email', 'Website');

                var errmsg = "";

                for (i = 0; i < rfield.length; i++)

                {

                    //alert(rfield[i]);

                    var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                    if (val == "" || val.replace(" ", "") == "")

                    {

                        errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

                    }

                }

                if (errmsg != "")

                {

                    $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                    $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                    return false;

                }

                return true;

            }

        </script>

        <script type="text/javascript">

            $(document).ready(function () {

                $('[data-toggle="tooltip"]').tooltip({
                    placement: 'top'

                });

            });

        </script>

</body>