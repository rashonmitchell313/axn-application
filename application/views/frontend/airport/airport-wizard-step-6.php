<?php 
	echo $this->load->view($header);
?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Modify Data Step 6 | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Modify Data Step 6 | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/dataTables.tableTools.css">
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSvlFP82dGOe_U4Yy9owatgxzSpk7Cpp0&sensor=false"></script>
<script type="text/javascript">
//function initialize(lat,longg)
//{
//var latt=lat;var longg=longg;var myCenter=new google.maps.LatLng(latt,longg);var mapProp={center:myCenter,zoom:14,mapTypeId:google.maps.MapTypeId.ROADMAP};var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);var marker=new google.maps.Marker({draggable:true,animation:google.maps.Animation.DROP,position:myCenter});marker.setMap(map);
//}
//google.maps.event.addDomListener(window,'load',initialize);
</script>
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>
        <section class="main-content">
				<?php /*?><section class="col-sm-2">
<?php foreach($modify_airport_adds as $adds): ?>
<a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>
<?php endforeach; ?>				
				</section><?php */?>		     	
        	<section class="container-fluid">
                    <?php echo $this->load->view('frontend/include/breadcrumb'); ?>
		 <div class="error">
		<?php echo $this->common->getmessage();?>
                             </div>
	   <div class="row">
                 	<div class="col-sm-12" style="font-family:verdana;">
                    	<div class="arport-btns-holder">
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                              <div class="btn-group" role="group">
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="1") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 1</u><br />CONTACTS 
                                </a>                                
                              </div>
							  
							  <div class="btn-group" role="group">
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="2") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 2</u><br /> AIRPORT INFO
                                </a>                                
                              </div>
							  
							  <div class="btn-group" role="group">
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="3") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 3</u><br /> TERMINALS
                                </a>                                
                              </div>
							  
							  <div class="btn-group" role="group">
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="4") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 4</u><br /> RENTAL & PARKING
                                </a>                                
                              </div>
							  
							  <div class="btn-group" role="group">
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="5") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 5</u><br /> TENANTS
                                </a>                                
                              </div>
							  
							  <div class="btn-group" role="group">
<a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="6") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Step 6</u><br /> LOCATION
                                </a>                                
                              </div>
                            </div>
                       </div>
                       </div>
                 </div>		
</br>
</br>
<div class="row">
<div class="col-sm-8">
	<h4><p class="text-default"><strong><?php echo $main_data[0]['aname'].' ('.$main_data[0]['IATA'].')'; ?></strong></p>
	<p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>
</div>
</div>
				
				</br>

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h4>Find us on google map </h4>
			</div>
			<div class="panel-body">
			<script>
									<?php
										$latitude=number_format($ainfo[0]['Latitude'],6, '.', '');
										$Longitude=number_format($ainfo[0]['Longitude'],6, '.', '');
									?>
window.onload=function(){initialize('<?php echo $latitude;  ?>','<?php echo $Longitude; ?>');};
                                    </script> 
                                    <div id="googleMap" style="width:100%;min-height:600px;"></div> 
			</div>
		</div>
	 </div>
</div>
</section>
            <div class="col-sm-12" style="font-family:verdana;">
                    	<div class="btn-group btn-group-justified" role="group" aria-label="...">
							  <div class="btn-group" role="group">
				
							  <div class="col-sm-2 pull-left" style="padding-left: 0px;">
<label class="control-label">Next, click on </label>
<a  href="<?php echo site_url('wizard/airport/'.$this->uri->segment(5)); ?>" class="btn text-uppercase<?php if($this->uri->segment(4)=="3") { echo " btn-warning"; } else { echo " btn-primary"; } ?>">
                                		<u>Final Step</u><br /> REVIEW
                                </a>               
								</div>
                              </div>  
                            </div>
 </div>
</section>


<div class="clearfix"></div>       
<?php echo $this->load->view($footer);?>
<div class="clearfix"></div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('fassests');?>/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets');?>/js/jquery.dataTables.bootstrap.js"></script>
<script src="<?php echo base_url('fassests');?>/js/dataTables.tableTools.js" type="text/javascript"></script>

<script type="text/javascript">
jQuery(document).ready(function () {

                                                                latt = "<?php echo $latitude; ?>";

                                                                longg = "<?php echo $Longitude; ?>";

                                                                var centerLatlng = new google.maps.LatLng(latt, longg);

                                                                var mapOptions = {center: centerLatlng, zoom: 14, mapTypeId: google.maps.MapTypeId.ROADMAP};

                                                                map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
                                                                // Display default marker
                                                                marker = new google.maps.Marker({
                                                                    draggable: true, animation: google.maps.Animation.DROP,
                                                                    map: map,
                                                                    position: centerLatlng
                                                                });

                                                                google.maps.event.addDomListener(marker, 'load');

                                                            });

    </script>

</body>