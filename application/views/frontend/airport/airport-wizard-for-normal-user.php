<?php echo $this->load->view($header); ?>



<title>



    <?php

    if ($this->uri->segment(0) != '') {



        echo 'Modify Data | ARN Fact Book ' . $this->uri->segment(0);

    } else {



        echo ucwords('Modify Data | ARN Fact Book');

    }

    ?>



</title>



<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.bootstrap.css">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/jquery-ui-1.10.3.full.min.css" >



</head>



<body>



    <div class="main-wrapper">



        <?php echo $this->load->view($menu); ?>



        <div class="clearfix"></div>



        <section class="main-content">



            <section class="container-fluid">            



                <section class="bottom-area-tabs">



                    <div role="tabpanel" class="bottom-area-tabs-body">                    



                        <!-- Nav tabs -->



                        <ul class="nav nav-tabs" role="tablist">



                            <li <?php if ($this->uri->segment(2) != "company") { ?>class="active"<?php } ?> role="presentation"><a href="#Airport-Instructions2" aria-controls="home" role="tab" data-toggle="tab">Airport Instructions</a></li>



                            <li role="presentation"><a href="#Airport-Instructions"  role="tab" data-toggle="tab">Reset Password</a></li>



                            <li role="presentation"><a href="#Category-Airport-Listing-A" onClick="loaddata('A', 'AI')"  role="tab" data-toggle="tab">Category A Airport Listing</a></li>



                            <li role="presentation"><a href="#Category-Airport-Listing-B"  role="tab" data-toggle="tab" onClick="loaddata('B', 'AI')">Category B Airport Listing</a></li>



                            <li role="presentation"><a href="#Category-Airport-Listing-C"  role="tab" data-toggle="tab" onClick="loaddata('C', 'AI')">Category C Airport Listing</a></li>



                            <li role="presentation"><a href="#Show-All"  role="tab" data-toggle="tab" onClick="loaddata('Show-All', 'AI')">Show All Airport Listing</a></li>



                            <li <?php if ($this->uri->segment(2) == "company") { ?>class="active"<?php } ?> role="presentation"><a href="#Company-Listing"  role="tab" data-toggle="tab"  onClick="loaddata('Company-Listing', 'CI')">Company Listing</a></li>



                        </ul>



                        <!-- Tab panes -->



                        <div class="tab-content">  



                            <div role="tabpanel" class="tab-pane <?php if ($this->uri->segment(2) != "company") { ?>active<?php } ?>" id="Airport-Instructions2">



                                <div class="tab-reset-pass">                                                        



                                    <div role="tabpanel" class="tab-pane" id="home">

                                        <div class="row">
                                        <div class="col-sm-8">

                                        <div class="table-responsive">



                                            <h1 class="default-title">You are affiliated with the following facilities:</h1>



                                            <p class="default-text">If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", if you are a company click "Company Listing".</p>



                                            <p class="default-text">Please consider using <a href="http://www.chrome.com/">Google Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> as you will have less problems. </p>



                                            <!--<p class="default-text">Please make note that all dollar figures should be quoted in US Dollars. Therefore, all Canadian airports should convert revenue and sales data by using the exchange rate of $0.97 Canadian = $1.00 US Dollars according to BankofCanada.ca yearly average rate of 2013.</p>-->



<!--                                            <p class="default-text">It is requested that the average Canadian dollar exchange rate to be used for Canadian airports reporting Gross Revenue, <br>

                                            Gross Sales and/or Total Rent to Airport amounts in US Dollars, be provided using the “Bank of Canada Databank Statistics Lookup” stated rate for <br>

                                            Canadian (CDN) dollar conversion to US (USD) dollar conversion for calendar year 2015 at $1 CDN = $0.7831 USD.</p>

                                            -->

<!--                                            <p class="default-text">Based on the 5 year Bank of Canada average exchange rate (from 2012 to 2016), the rate for converting CA$ to US$ is 0.8715.<br>

                                            The rate is $0.8715 US$ to $1 CA$.</p>-->

                                            <!-- <p class="default-text">Based on the 5 year Bank of Canada average exchange rate (from 2012 to 2016), the rate for converting CA$ to US$ is $0.8715 US$ to $1 CA$.</p> -->
                                            <p class="default-text">Based on the 5 year Bank of Canada average exchange rate (from 2013 to 2017), the rate for converting CA$ to US$ is $0.8272 US$ to $1 CA$.</p>
                                            

                                            <p class="default-text">To add additional airports/companies to your list, please click on the <a href="<?php echo site_url('requestaccess'); ?>">Request to Modify Data</a> link at bottom. </p>







                                        </div>

                                 </div>
                                 <!-- Currency Rate API  !-->
                                 <!--  <div class="col-sm-2" style="border:1px solid #d4d4d4;">
                                <p><b>Live Currency Rate</b></p>
                               <p>Candian Dollor to US Dollor</p>
                               <form method="post" id="currency-form">
                                <input type="hidden" name="from_currency" value="CAD" id="from_currency"> 
                                <input type="hidden"  name="amount" id="amount" value="1" />
                                <input type="hidden"  name="to_currency" id="to_currency" value="USD" />
                                             
                            </form> 
                            
                            <div class="form-group" id="converted_rate">
                                  $0.00 CDN = $0.00 USD
                            </div>  
                            <div id="converted_amount"></div>
                               </div> -->
                      
                                 </div>

                                    </div>                                 



                                </div>    



                            </div>



                            <div role="tabpane2" class="tab-pane" id="Airport-Instructions">



                                <div class="tab-reset-pass form-horizontal clearfix">



                                    <div class="error">







                                    </div>   



                                    <div class="col-sm-8 margin-auto">







                                        <form class="" action="" method="post" onSubmit="return !!(passwordcheck() & checkval());">



                                            <div class="form-group">



                                                <label class="col-sm-5 control-label">Original Password:



                                                    &nbsp;<span class="red">*</span></label>                                            



                                                <div class="col-sm-7">



                                                    <input type="password" class="form-control" id="opassword" name="opassword" placeholder="Original Password">



                                                </div>



                                            </div>



                                            <div class="form-group">



                                                <label class="col-sm-5 control-label">New Password:&nbsp;<span class="has-warning">*</span></label>



                                                <div class="col-sm-7">



                                                    <input type="password" class="form-control" id="nopassword" name="nopassword" placeholder="New Password">



                                                </div>



                                            </div>



                                            <div class="form-group">



                                                <label class="col-sm-5 control-label">Type New Password again:<span class="red">*</span></label>



                                                <div class="col-sm-7">



                                                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password">



                                                </div>



                                            </div>



                                            <div class="form-group">



                                                <div class="col-sm-offset-5 col-sm-10">



                                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>

                                                    <!--                                                    <button type="submit" onClick="MM_validateForm('opassword', '', 'R', 'nopassword', '', 'R', 'cpassword', '', 'R');

                                                                                                                return document.MM_returnValue" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>-->



                                                </div>



                                            </div>



                                        </form>



                                    </div>



                                </div>



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-A">



                                <div class="tab-reset-pass">                                                        



                                    <table width="100%" id="Category-Airport-Listing-A-Data" class="table-bordered table-hover table-hover text-center">



                                        <thead>



                                            <tr>



                                                <th>Edit</th>



                                                <th>IATA</th>



                                                <th>Airport Name</th>



                                                <th>Last Modified</th>



                                                <th>Published</th>



                                                <th>Approved</th>



                                                <th>Changed Last By</th>



                                            </tr>



                                        </thead>



                                    </table>                                 



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-B">



                                <div class="tab-reset-pass">



                                    <table width="100%" id="Category-Airport-Listing-B-Data" class="table-bordered table-hover table-hover text-center">



                                        <thead>



                                            <tr>



                                                <th>Edit</th>



                                                <th>IATA</th>



                                                <th>Airport Name</th>



                                                <th>Last Modified</th>



                                                <th>Published</th>



                                                <th>Approved</th>



                                                <th>Changed Last By</th>



                                            </tr>



                                        </thead>											  



                                    </table>                                                              



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-C">



                                <div class="tab-reset-pass">







                                    <table id="Category-Airport-Listing-C-Data" width="100%" class="table-bordered table-hover table-hover text-center">



                                        <thead>



                                            <tr>



                                                <th>Edit</th>



                                                <th>IATA</th>



                                                <th>Airport Name</th>



                                                <th>Last Modified</th>



                                                <th>Published</th>



                                                <th>Approved</th>



                                                <th>Changed Last By</th>



                                            </tr>



                                        </thead>											



                                    </table>                        



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Show-All">



                                <div class="tab-reset-pass"> 



                                    <table width="100%" id="Category-Airport-Listing-Show-All-Data" class="table-bordered table-hover table-hover text-center">



                                        <thead>



                                            <tr>



                                                <th>Edit</th>



                                                <th>IATA</th>



                                                <th>Airport Name</th>



                                                <th>Last Modified</th>



                                                <th>Published</th>



                                                <th>Approved</th>



                                                <th>Changed Last By</th>



                                            </tr>



                                        </thead>											



                                    </table>                                                             



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane <?php if ($this->uri->segment(2) == "company") { ?>active<?php } ?>" id="Company-Listing">



                                <div class="tab-reset-pass">



                                    <table width="100%" id="Category-Airport-Listing-Company-Listing-Data" class="table-bordered table-hover table-hover text-center">



                                        <thead>



                                            <tr>



                                                <th>Edit</th>                                                



                                                <th>Company Name</th>



                                                <th>Last Modified</th>



                                                <th>Published</th>



                                                <th>Approved</th>



                                                <th>Changed Last By</th>



                                            </tr>



                                        </thead>



                                    </table> 



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-A">



                                <div class="tab-reset-pass">                                                        



                                    <div role="tabpanel" class="tab-pane" id="home">



                                        <div class="table-responsive">



                                            <h1 class="default-title">You are affiliated with the following facilities:</h1>



                                            <p class="default-text">If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", if you are a company click "Company Listing".</p>



                                            <p class="default-text">Please consider using <a href="http://www.chrome.com/">Google Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> as you will have less problems. </p>



                                            <p class="default-text">Please make note that all dollar figures should be quoted in US Dollars. Therefore, all Canadian airports should convert revenue and sales data by using the exchange rate of $0.97 Canadian = $1.00 US Dollars according to BankofCanada.ca yearly average rate of 2013.</p>



                                            <p class="default-text">To add additional airports/companies to your list, please click on the <a href="<?php echo site_url('requestaccess'); ?>">Request to Modify Data</a> link at bottom. </p>







                                        </div>



                                    </div>                                 



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-B">



                                <div class="tab-reset-pass">



                                    <div role="tabpanel" class="tab-pane">



                                        <div class="table-responsive">



                                            <h1 class="default-title">You are affiliated with the following facilities:</h1>



                                            <p class="default-text">If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", if you are a company click "Company Listing".</p>



                                            <p class="default-text">Please consider using <a href="http://www.chrome.com/">Google Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> as you will have less problems. </p>



                                            <p class="default-text">Please make note that all dollar figures should be quoted in US Dollars. Therefore, all Canadian airports should convert revenue and sales data by using the exchange rate of $0.97 Canadian = $1.00 US Dollars according to BankofCanada.ca yearly average rate of 2013.</p>



                                            <p class="default-text">To add additional airports/companies to your list, please click on the 



                                                <a href="<?php echo site_url('requestaccess'); ?>">Request to Modify Data</a> link at bottom. </p>







                                        </div>



                                    </div>                                                              



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-C">



                                <div class="tab-reset-pass">







                                    <div role="tabpanel" class="tab-pane">



                                        <div class="table-responsive">



                                            <h1 class="default-title">You are affiliated with the following facilities:</h1>



                                            <p class="default-text">If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", if you are a company click "Company Listing".</p>



                                            <p class="default-text">Please consider using <a href="http://www.chrome.com/">Google Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> as you will have less problems. </p>



                                            <p class="default-text">Please make note that all dollar figures should be quoted in US Dollars. Therefore, all Canadian airports should convert revenue and sales data by using the exchange rate of $0.97 Canadian = $1.00 US Dollars according to BankofCanada.ca yearly average rate of 2013.</p>



                                            <p class="default-text">To add additional airports/companies to your list, please click on the <a href="<?php echo site_url('requestaccess'); ?>">Request to Modify Data</a> link at bottom. </p>







                                        </div>



                                    </div>                        



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane" id="Show-All">



                                <div class="tab-reset-pass"> 



                                    <div role="tabpanel" class="tab-pane">



                                        <div class="table-responsive">



                                            <h1 class="default-title">You are affiliated with the following facilities:</h1>



                                            <p class="default-text">If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", if you are a company click "Company Listing".</p>



                                            <p class="default-text">Please consider using <a href="http://www.chrome.com/">Google Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> as you will have less problems. </p>



                                            <p class="default-text">Please make note that all dollar figures should be quoted in US Dollars. Therefore, all Canadian airports should convert revenue and sales data by using the exchange rate of $0.97 Canadian = $1.00 US Dollars according to BankofCanada.ca yearly average rate of 2013.</p>



                                            <p class="default-text">To add additional airports/companies to your list, please click on the <a href="<?php echo site_url('requestaccess'); ?>">Request to Modify Data</a> link at bottom. </p>







                                        </div>



                                    </div>                                                             



                                </div>    



                            </div>



                            <div role="tabpanel" class="tab-pane <?php if ($this->uri->segment(2) == "company") { ?>active<?php } ?>" id="Company-Listing">



                                <div class="tab-reset-pass"> 



                                    <div class="table-responsive">



                                        <h1 class="default-title">You are affiliated with the following facilities:</h1>



                                        <p class="default-text">If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", if you are a company click "Company Listing".</p>



                                        <p class="default-text">Please consider using <a href="http://www.chrome.com/">Google Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> as you will have less problems. </p>



                                        <p class="default-text">Please make note that all dollar figures should be quoted in US Dollars. Therefore, all Canadian airports should convert revenue and sales data by using the exchange rate of $0.97 Canadian = $1.00 US Dollars according to BankofCanada.ca yearly average rate of 2013.</p>



                                        <p class="default-text">To add additional airports/companies to your list, please click on the <a href="<?php echo site_url('requestaccess'); ?>">Request to Modify Data</a> link at bottom. </p>







                                    </div>



                                </div>        



                            </div>



                        </div>



                    </div>



                </section>



            </section>



        </section>



        <div class="clearfix"></div>       



        <?php echo $this->load->view($footer); ?>



        <div class="clearfix"></div>



    </div>



    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>     



    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>



    <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>



    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.bootstrap.js"></script>



    <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>



    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>



    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>



    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.full.min.js"></script>



    <script src="http://cdn.datatables.net/plug-ins/1.10.7/type-detection/formatted-num.js"></script>  

<!-- Currency RAte API  !-->
<!-- <script>
$(function() {
    var anys = setInterval(anyname,3000);
});
function anyname() {
var fcurrency = $("#from_currency").val();
var tcurrency = $("#to_currency").val();
var amount = $("#amount").val();
var texthtml ="";
 $.ajax({
            type: 'post',
            url: '<?php //echo site_url('ajax/currencyy'); ?>',
            data: { from_currency:fcurrency, to_currency:tcurrency, amount:amount },
            success: function (response) {
                $( "#converted_rate" ).empty();
            $('#converted_rate').append("$1 CDN = $" + response + "  USD");
               
            }
          });

//});
}

</script> -->

    <script type="text/javascript" class="init">



                                            function checkval()



                                            {



                                                var rfield = new Array("opassword", "nopassword", "cpassword");



                                                var msg = new Array('Original Password', 'New Password', 'Confirm Password');



                                                var errmsg = "";



                                                for (i = 0; i < rfield.length; i++)



                                                {



                                                    //alert(rfield[i]);



                                                    var val = document.getElementsByName("" + rfield[i] + "")[0].value;



                                                    if (val == "" || val.replace(" ", "") == "")



                                                    {



//                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

                                                        errmsg += "" + msg[i] + " is Required. <br/>";





                                                    }



                                                }



                                                if (errmsg != "")



                                                {



                                                    $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");



                                                    $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');



                                                    return false;



                                                }



                                                return true;



                                            }



//                                                        {

//alert('test');

//                                                            var rfield = new Array("opassword", "nopassword", "cpassword");

//

//                                                            var msg = new Array('Original Password', 'New Password', 'Confirm Password');

//

//                                                            var errmsg = "";

//

//                                                            for (i = 0; i < rfield.length; i++)

//

//                                                            {

//

//                                                                //alert(rfield[i]);

//

//                                                                var val = document.getElementsByName("" + rfield[i] + "")[0].value;

//

//                                                                if (val == "" || val.replace(" ", "") == "")

//

//                                                                {

//

////                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

//                                                                    errmsg += "" + msg[i] + " is Required. <br/>";

//

//

//                                                                }

//

//                                                            }

//

//                                                            if (errmsg != "")

//

//                                                            {

//

//                                                                $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

//

//                                                                $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

//

//                                                                return false;

//

//                                                            }

//

//                                                            return true;

//

//                                                        }





                                            $(".chosen-select").chosen({width: "83.4%"});









        <?php if ($this->uri->segment(2) == "company") { ?>



                                                loaddata('Company-Listing', 'CI');



        <?php } ?>



                                            function loaddata(div, type)



                                            {



                                                var newdiv = "Category-Airport-Listing-" + div + "-Data";



                                                var trlength = $("#" + newdiv + " tr").length;



                                                if (trlength <= 1)



                                                {



                                                    getdata(newdiv, div, type);



                                                }



                                            }



                                            function getdata(newdiv, offset, type)



                                            {



                                                if (offset == 'Company-Listing')



                                                {



                                                    offset = 'K';// K for company



                                                }



                                                if (offset == 'Show-All')



                                                {



                                                    offset = 'S';// S Show-All



                                                }



                                                var datastring = "C" + offset + "T" + type + "";







                                                $('#' + newdiv + '').dataTable({

                                                    responsive: true,

                                                    "processing": true,

                                                    "serverSide": true,

                                                    "ajax": {

                                                        "url": "<?php echo site_url('ajax/GetRecord'); ?>",

                                                        "type": "POST",

                                                        "cache": "false",

                                                        "data": datastring,

                                                    },

                                                    "pagingType": "simple_numbers",

                                                    //"bProcessing": true,



                                                    "oLanguage": {

                                                        "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"



                                                    }



                                                });

                                                $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {

                                                    console.log(message);

                                                };

                                            }



                                            function movetoerror(msg)



                                            {



                                                $(".error").html(msg);



                                                $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');



                                            }



                                            function passwordcheck()



                                            {



                                                var opassword = $("#opassword").val();



                                                var cpassword = $("#cpassword").val();



                                                var npassword = $("#nopassword").val();



                                                if (opassword == "" || cpassword == "" || npassword == "")



                                                {



                                                    //var set="<?php // echo $this->common->setmessage('(*) asterisk is indicates field is required.', '-1'); ?>";	



//                                                                movetoerror("<div class='row'><div class=''><div class='alert alert-danger'>(*) asterisk is indicates field is required.</div></div></div>");



                                                } else if (cpassword != npassword)



                                                {



                                                    movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-danger'>Confirm Password must be match with password.</div></div></div>");



                                                } else



                                                {



                                                    movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-success'><i class='fa fa-spinner fa-spin fa-1x'></i>&nbsp; Updateing password.</div></div></div>");



                                                    $.ajax({

                                                        url: "<?php echo site_url('ajax/doaction'); ?>",

                                                        data: "opassword=" + opassword + "&cpassword=" + cpassword + "&npassword=" + npassword + "&action=changepassword",

                                                        type: "POST",

                                                        cache: false,

                                                        async: false,

                                                        success: function (html)



                                                        {



                                                            if (html == 1)



                                                            {



                                                                movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-success'><i class='fa fa-check fa-1x'></i>&nbsp;Password has been update Successfully.</div></div></div>");



                                                            } else



                                                            {



                                                                movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-danger'>" + html + "</div></div></div>");



                                                            }



                                                        }



                                                    });



                                                }



                                                return false;



                                            }



    </script>



</body>



</html>



