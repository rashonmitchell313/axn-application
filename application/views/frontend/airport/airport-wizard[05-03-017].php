<?php echo $this->load->view($header); ?>

<title>

    <?php
//    if ($this->uri->segment(0) != '') {
//
//        echo 'Modify Data | ARN Fact Book ' . $this->uri->segment(0);
//    } else {
//
//        echo ucwords('Modify Datat | ARN Fact Book');
//    }
    ?>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.bootstrap.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/jquery-ui-1.10.3.full.min.css" >

<style>
    .help-block ul li {
        margin-left: 0px ! Important;
    }
</style>

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">

            <?php /* ?><section class="col-sm-2">

              <?php foreach($modify_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">   

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>       

                <section class="bottom-area-tabs">

                    <div role="tabpanel" class="bottom-area-tabs-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <div role="tabpanel" class="tab-pane active">

                                    <div class="table-responsive padding-table">

                                        <h1 class="default-title">You are affiliated with the following facilities:</h1>

                                        <p class="default-text">If you would like to modify your data, and you're an Airport please Click "Show All Airport Listing", if you are a company click "Company Listing".</p>

                                        <p class="default-text">Please consider using <a href="https://www.google.com/chrome/" target="_blank">Google Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/?v=1b" target="_blank">Firefox</a> as you will have less problems. </p>

                                        <!--<p class="default-text">Please make note that all dollar figures should be quoted in US Dollars. Therefore, all Canadian airports should convert revenue and sales data by using the exchange rate of $0.97 Canadian = $1.00 US Dollars according to BankofCanada.ca yearly average rate of 2013.</p>-->

                                        <p class="default-text">It is requested that the average Canadian dollar exchange rate to be used for Canadian airports reporting Gross Revenue, <br>
                                            Gross Sales and/or Total Rent to Airport amounts in US Dollars, be provided using the “Bank of Canada Databank Statistics Lookup” stated rate for <br>
                                            Canadian (CDN) dollar conversion to US (USD) dollar conversion for calendar year 2015 at $1 CDN = $0.7831 USD.</p>

                                        <p class="default-text">To add additional airports/companies to your list, please click on the <a href="<?php echo site_url('requestaccess'); ?>">Request to Modify Data</a> link at bottom. </p>

                                        <p class="default-text">Read me: <a href="<?php echo site_url('privacy'); ?>">Privacy</a>



                                    </div>

                                </div>

                            </div>

                        </div>

                        <h2 class="default-title margin-bottom-ten">Click Any</h2>

                        <!-- Nav tabs -->

                        <ul class="nav nav-tabs" role="tablist">

                            <li <?php if ($this->uri->segment(2) != "company") { ?>class="active"<?php } ?> role="presentation"><a href="#Airport-Instructions2" aria-controls="home" role="tab" data-toggle="tab">Airport Instructions</a></li>

                            <!--<li role="presentation"><a href="#Airport-Instructions"  role="tab" data-toggle="tab">Reset Password</a></li>-->

                            <li role="presentation"><a href="#Category-Airport-Listing-A" onClick="loaddata('A', 'AI')"  role="tab" data-toggle="tab">Category A Airport Listing</a></li>

                            <li role="presentation"><a href="#Category-Airport-Listing-B"  role="tab" data-toggle="tab" onClick="loaddata('B', 'AI')">Category B Airport Listing</a></li>

                            <li role="presentation"><a href="#Category-Airport-Listing-C"  role="tab" data-toggle="tab" onClick="loaddata('C', 'AI')">Category C Airport Listing</a></li>

                            <li role="presentation"><a href="#Show-All"  role="tab" data-toggle="tab" onClick="loaddata('Show-All', 'AI')">Show All Airport Listing</a></li>

                            <li <?php if ($this->uri->segment(2) == "company") { ?>class="active"<?php } ?> role="presentation"><a href="#Company-Listing"  role="tab" data-toggle="tab"  onClick="loaddata('Company-Listing', 'CI')">Company Listing</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content tenants-data">

                            <div role="tabpanel" class="tab-pane <?php if ($this->uri->segment(2) != "company") { ?>active<?php } ?>" id="Airport-Instructions2">

                                <div class="row">

                                    <div class="col-sm-12">

                                        <div class="alert alert-info">

                                            <a href="http://arnfactbook.com/2015/FBOInstructions.pdf" target="_new">

                                                <i class="fa fa-download"></i>&nbsp;Click Here to download Airport Instructions.</a>



                                        </div>

                                    </div>

                                </div>

                            </div>



                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-A">

                                <div class="tab-reset-pass">                                                        

                                    <table  width="100%" id="Category-Airport-Listing-A-Data" class="table-bordered table-hover table-hover text-center table-responsive">

                                        <thead>

                                            <tr>

                                                <th>Edit</th>

                                                <th>IATA</th>

                                                <th>Airport Name</th>

                                                <th>Last Modified</th>

                                                <th>Published</th>

                                                <th>Approved</th>

                                                <th>Changed Last By</th>

                                            </tr>

                                        </thead>

                                    </table>                                 

                                </div>    

                            </div>

                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-B">

                                <div class="tab-reset-pass">

                                    <table width="100%" id="Category-Airport-Listing-B-Data" class="table-bordered table-hover table-hover text-center table-responsive">

                                        <thead>

                                            <tr>

                                                <th>Edit</th>

                                                <th>IATA</th>

                                                <th>Airport Name</th>

                                                <th>Last Modified</th>

                                                <th>Published</th>

                                                <th>Approved</th>

                                                <th>Changed Last By</th>

                                            </tr>

                                        </thead>											  

                                    </table>                                                              

                                </div>    

                            </div>

                            <div role="tabpanel" class="tab-pane" id="Category-Airport-Listing-C">

                                <div class="tab-reset-pass">



                                    <table id="Category-Airport-Listing-C-Data" width="100%" class="table-bordered table-hover table-hover text-center table-responsive">

                                        <thead>

                                            <tr>

                                                <th>Edit</th>

                                                <th>IATA</th>

                                                <th>Airport Name</th>

                                                <th>Last Modified</th>

                                                <th>Published</th>

                                                <th>Approved</th>

                                                <th>Changed Last By</th>

                                            </tr>

                                        </thead>											

                                    </table>                        

                                </div>    

                            </div>

                            <div role="tabpanel" class="tab-pane" id="Show-All">

                                <div class="tab-reset-pass"> 

                                    <table width="100%" id="Category-Airport-Listing-Show-All-Data" class="table-bordered table-hover table-hover text-center table-responsive">

                                        <thead>

                                            <tr>

                                                <th>Edit</th>

                                                <th>IATA</th>

                                                <th>Airport Name</th>

                                                <th>Last Modified</th>

                                                <th>Published</th>

                                                <th>Approved</th>

                                                <th>Changed Last By</th>

                                            </tr>

                                        </thead>											

                                    </table>                                                             

                                </div>    

                            </div>

                            <div role="tabpanel" class="tab-pane <?php if ($this->uri->segment(2) == "company") { ?>active<?php } ?>" id="Company-Listing">

                                <div class="tab-reset-pass">

                                    <table width="100%" id="Category-Airport-Listing-Company-Listing-Data" class="table-bordered table-hover table-hover text-center table-responsive">

                                        <thead>

                                            <tr>

                                                <th>Edit</th>

                                                <th>Company Name</th>

                                                <th>Last Modified</th>

                                                <th>Published</th>

                                                <th>Approved</th>

                                                <th>Changed Last By</th>

                                            </tr>

                                        </thead>

                                    </table> 

                                </div>    

                            </div>

                        </div>

                    </div>

                </section>

            </section>

        </section>

        <div class="clearfix"></div>       

        <?php echo $this->load->view($footer); ?>

        <div class="clearfix"></div>

    </div>

    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>     

    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>

    <script src="http://cdn.datatables.net/plug-ins/1.10.7/type-detection/formatted-num.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.bootstrap.js"></script>

    <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.full.min.js"></script>



    <script type="text/javascript" class="init">


                                $('#form1').validator();



                                var seg = "<?php echo $this->uri->segment(0) ?>";

                                if (seg != '') {
                                    $('title').html("Modify Data | ARN Fact Book " + seg);
                                }
                                else {
                                    $('title').html("Modify Data | ARN Fact Book");
                                }



//                                $(".chosen-select").chosen({width: "83.4%"});




        <?php if ($this->uri->segment(2) == "company") { ?>

                                    loaddata('Company-Listing', 'CI');

        <?php } ?>

                                function loaddata(div, type)

                                {

                                    var newdiv = "Category-Airport-Listing-" + div + "-Data";

                                    var trlength = $("#" + newdiv + " tr").length;

                                    if (trlength <= 1)

                                    {

                                        getdata(newdiv, div, type);

                                    }

                                }

                                function getdata(newdiv, offset, type)

                                {

                                    if (offset == 'Company-Listing')

                                    {

                                        offset = 'K';// K for company

                                    }

                                    if (offset == 'Show-All')

                                    {

                                        offset = 'S';// S Show-All

                                    }

                                    var datastring = "C" + offset + "T" + type + "";

                                    $('#' + newdiv + '').dataTable({
                                        responsive: true,
                                        "processing": true,
                                        "serverSide": true,
                                        aoColumnDefs: [
                                            {"aTargets": [0], "bSortable": false}

                                        ],
                                        /*"aoColumnDefs": [
                                         
                                         {
                                         
                                         "mData": null,
                                         
                                         "sDefaultContent": "jamie@airportrevenuenews.com",
                                         
                                         "aTargets": [ -1 ]
                                         
                                         }
                                         
                                         ],	*/

                                        "ajax": {
                                            "url": "<?php echo site_url('ajax/GetRecord'); ?>",
                                            "type": "POST",
                                            "cache": "false",
                                            "data": datastring,
                                        },
                                        "pagingType": "simple_numbers",
                                        "dom": 'T<"clear">lfrtip',
                                        "tableTools": {
                                            "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"

                                        },
                                        "oLanguage": {
                                            "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                                        }

                                    });
                                    $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
                                        console.log(message);
                                    };
                                }

                                function movetoerror(msg)

                                {

                                    $(".error").html(msg);

                                    $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                                }

    </script>

</body>

</html>

