<?php

echo $this->load->view($header);

?>

<title>

    <?php

    if ($this->uri->segment(0) != '') {

        echo 'Modify Data Step 2 | ARN Fact Book ' . $this->uri->segment(0);

    } else {

        echo 'Modify Data Step 2 | ARN Fact Book';

    }

    ?>

</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/jquery.dataTables.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/css/choseen.css">

</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content" style="overflow:hidden;">

            <?php /* ?><section class="col-sm-2">

              <?php foreach($modify_airport_adds as $adds): ?>

              <a href="<?php echo $adds['adlink'];?>" target="_new"><img src="<?php echo base_url();?><?php echo $adds['imglink'];?>" alt="ad" width="140" height="200" border="0"></a></br></br></br>

              <?php endforeach; ?>

              </section><?php */ ?>		     	

            <section class="container-fluid">

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>

                <div class="error">

                    <?php echo $this->common->getmessage(); ?>

                </div>

                <div class="row">

                    <div class="col-sm-12" style="font-family:verdana;">
                    <div class="arport-btns-holder">

                        <div class="btn-group btn-group-justified" role="group" aria-label="...">

                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/1/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "1") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 1</u><br />CONTACTS 

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/2/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "2") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 2</u><br /> AIRPORT INFO

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "3") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 3</u><br /> TERMINALS

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/4/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "4") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 4</u><br /> RENTAL & PARKING

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/5/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "5") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 5</u><br /> TENANTS

                                </a>                                

                            </div>



                            <div class="btn-group" role="group">

                                <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/6/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                                if ($this->uri->segment(4) == "6") {

                                    echo " btn-warning";

                                } else {

                                    echo " btn-primary";

                                }

                                ?>">

                                    <u>Step 6</u><br /> LOCATION

                                </a>                                

                            </div>

                        </div>
                        </div>

                    </div>

                </div>		

                </br>

                </br>

                <div class="row">

                    <div class="col-sm-8">

                        <p><strong>VERY IMPORTANT:</strong> <span class="text-danger">Do not use symbols like $ or , when entering numbers. All formatting will be applied automatically on the review page.</span></p>

                        <h3><p style="margin-left: 15px;"><b>Examples:</b></p></h3>

                        <div class="col-sm-4">

                            <p>Currency fields</p>

                            <p>Quantity fields</p>

                            <p>Comment fields</p>

                        </div>

                        <div class="col-sm-4">

                            <p>4.56 will appear as $4.56</p>

                            <p>1234567 will appear as 1,234,567</p>

                            <p>Type any symbols needed</p>

                        </div>
						
                        <div class="define-yyc-intro">

                        <h4><p class="text-default"><strong><?php echo $main_data[0]['aname'] . ' (' . $main_data[0]['IATA'] . ')'; ?></strong></p>

                            <p class="text-default"><strong><?php echo $main_data[0]['acity']; ?>, <?php echo $main_data[0]['astate']; ?></strong></p></h4>
                        </div>    

                    </div>

                </div>



                </br>

                <?php

                $step = $this->uri->segment(4);

                $aid = $this->uri->segment(5);

                ?>

                <div class="row">

                    <form method="post"  class="form-horizontal" role="form" name="contactform" id="a_info" data-toggle="validator" action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>">   



                        <div class="col-sm-12">

                            <div class="panel panel-primary">

                                <div class="panel-heading">

                                    <h4>Airport Info</h4>

                                </div>

                                <div class="panel-body">



<!--<form method="post"  class="form-horizontal" role="form" name="contactform"  onSubmit="return checkval();"  action="<?php // echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid;      ?>">-->   

<!--<form method="post"  class="form-horizontal" role="form" name="contactform"  id="a_info"  data-toggle="validator" action="<?php echo site_url('wizard/airport/airport_edit_steps') . '/' . $step . '/' . $aid; ?>">-->   





                                    <!--<div class="col-sm-12">-->

                                    <!--

                                    <a href="<?php echo site_url('frontend/airport_add_steps_1'); ?>" name="addmorecontact" class="addmorecontact btn btn-primary" value="" id="">+ Add additional Contact</a>

                                    -->   	

                                    <div class="col-sm-12">

                                        <div class="col-sm-6">



                                            <div class="form-group">

                                             
                                                
                                                <label class="control-label col-sm-4" for="configuration">Airport Configuration: </label>

                                                <div class="col-sm-6">

                                                    <input type="text" name="configuration" class="form-control" value="<?php echo $wideInfo_traffic[0]['configuration']; ?>" id="configuration" placeholder="Enter Airport Configuration">

                                                </div>

                                                <div class="col-sm-2">

                                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="Describes the shape of your airport. For example, T-shaped, U-shaped, Fingers, etc."></i>

                                                </div>





                                            </div>



                                            <?php

                                            $texpansionplanned = $wideInfo_traffic[0]['texpansionplanned'];



                                            $completedexpdate = $wideInfo_traffic[0]['completedexpdate'];



                                            $al_texpansionplanned = explode("#", $texpansionplanned);



                                            $al_completedexpdate = explode("#", $completedexpdate);





                                            for ($t = 0; $t < count($al_texpansionplanned); $t++) {

                                                ?>

                                                <div class="form-group" id="texpan<?php echo $t;?>">

                                                    <label class="control-label col-sm-4" for="texpansionplanned">Expansion Planned:</label>

                                                    <!--<div class="col-sm-8">-->

                                                    <div class="col-xs-6">

                                                        <!--<input type="text" name="texpansionplanned[]" value="<?php // echo $wideInfo_traffic[0]['texpansionplanned'];        ?>" class="form-control" id="texpansionplanned" placeholder="Enter Expansion Planned" >-->

                                                        <input type="text" name="texpansionplanned[]" value="<?php echo $al_texpansionplanned[$t]; ?>" class="form-control expp" id="texpansionplanned" placeholder="Enter Expansion Planned" >

                                                    </div>

                                                    <div class="col-sm-2">

                                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="A reference as to whether the airport is expanding its concessions and/or other space in any given terminal or zone in its facility, how much additional space is planned and when the expansion is expected to be complete."></i>

                                                    </div>





                                                </div>



                                            <div class="form-group" id="compl<?php echo $t;?>">

                                                    <label class="control-label col-sm-4" for="completedexpdate">Complete Date:</label>

                                                    <div class="col-sm-6">

                                                        <!--<input type="text" name="completedexpdate[]" value="<?php // echo $wideInfo_traffic[0]['completedexpdate'];        ?>" class="form-control" id="completedexpdate" placeholder="Enter Complete Date" >-->

                                                        <input type="text" name="completedexpdate[]" value="<?php echo $al_completedexpdate[$t]; ?>" class="form-control cdate" id="completedexpdate[<?php echo $t;?>]" placeholder="Enter Complete Date" >

                                                    </div>

                                                </div>

                                                <?php

                                            }

//echo $t; exit;

                                            ?>





                                        </div>



                                        <div class="col-sm-6">





                                            <div class="form-group">

                                                <label for="mgtstructure" class="control-label col-sm-4" >Concessions Mgt. Type:</label>

                                                <div class="col-sm-6">



                                                    <select name="mgtstructure" id="mgtstructure" class="chosen form-control">

                                                        <option></option>

                                                        <?php

                                                        foreach ($con_mgt_type as $x) {

                                                            ?>

                                                            <option  value="<?php echo $x['lkpmgttype'] ?>" <?php

                                                            if ($x['lkpmgttype'] == $wideInfo_traffic[0]['mgtstructure']) {

                                                                echo "selected";

                                                            }

                                                            ?>   > <?php echo $x['lkpmgttype'] ?></option> <?php echo "\n"; ?>

                                                                     <?php

                                                                 }

                                                                 ?>

                                                    </select>

                                                </div>

                                            </div>				  



                                            <?php

                                            $texpansionplanned = $wideInfo_traffic[0]['texpansionplanned'];



                                            $aaddsqft = $wideInfo_traffic[0]['addsqft'];



                                            $al_texpansionplanned = explode("#", $texpansionplanned);



                                            $al_aaddsqft = explode("#", $aaddsqft);





//                                            print_r($al_aaddsqft);

//print_r($al_texpansionplanned); exit;



                                            for ($t = 0; $t < count($al_texpansionplanned); $t++) {

                                                ?>



                                            <div class="form-group" id="sqft<?php echo $t?>">

                                                    <label class="control-label col-sm-4" for="addsqft">Addl Sq. Ft,s.:</label>

                                                    <div class="col-sm-6">

                                                        <input type="text" name="addsqft[]" value="<?php echo $al_aaddsqft[$t]; ?>" class="form-control sqftt" id="addsqft" placeholder="Enter Addl Sq. Ft,s">

                                                    </div>

                                                </div>







                                            <div class="form-group" id="btnn<?php echo $t?>">

                                                    <label class="control-label col-sm-4" for="addsqft">&nbsp;</label>

                                                    <div class="col-xs-6">



                                                        <!--<button type="button" id="removeterminal" onclick="removeterminal(<?php echo $t;?>)" title="Remove this Terminal Option" class="col-xs-6 col-sm-5 btn btn-xs btn-danger"><i class="fa fa-check"></i>&nbsp;Remove</button>-->

                                                        <a href="javascript:void(0)" id="removeterminal" onClick="removeterminal(<?php echo $t;?>)" title="Remove this Terminal Option" class="col-xs-12 col-sm-5 btn btn-xs btn-danger"><i class="fa fa-check"></i>&nbsp;Remove</a>

                                                        <!--<button type="button" id="addmoreterminal" title="Add more Terminal Option" class="col-xs-6 col-sm-5 btn btn-xs btn-success"><i class="fa fa-check"></i>&nbsp;Add</button>-->

                                                    </div>   

                                                </div>   



                                                <?php

                                            }

                                            ?>



                                            <div class="form-group">

                                                <label class="control-label col-sm-4" for="addsqft">&nbsp;</label>

                                                <div class="col-xs-6">

                                                    <button type="button" id="addmoreterminal" title="Add more Terminal Option" class="col-xs-12 col-sm-5 btn btn-xs btn-success"><i class="fa fa-check"></i>&nbsp;Add</button>

                                                </div>   

                                            </div>   









                                        </div>

                                        <div class="terminla"></div>

                                    </div>

                                    <!--</div>-->

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="panel panel-primary">

                                        <div class="panel-heading">

                                            <h4>Airport Revenue</h4>

                                        </div>

                                        <div class="panel-body">

                                            <div class="table-responsive">

                                                <table class="table" border="0" cellspacing="0" cellpadding="0">

                                                    <tr style="border-top: none !important;">

                                                        <td width="25%" style="border-top: none !important;">&nbsp;</td>

                                                        <td width="33%" style="border-top: none !important;"><b>Revenue</b></td>

                                                        <td width="33%" style="border-top: none !important;"><b>Revenue to Airport</b></td> 

                                                    </tr>

                                                    <tr>

                                                        <td align="right" style="border-top: none !important;">							

                                                            <div class="form-group">

                                                                <label for="inputEmail3" class="col-sm-12 control-label">Passenger Services</label>

                                                            </div>
                                                            
                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="col-sm-12">

                                                                <input type="text" name="passservicesrev" value="<?php echo $wideInfo_traffic[0]['passservicesrev']; ?>" class="form-control" id="" placeholder="Passenger Services">

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="col-sm-11">

                                                                <input type="text" name="passservicesrevtoair" value="<?php echo $wideInfo_traffic[0]['passservicesrevtoair']; ?>" class="form-control" id="" placeholder="Revenue to Airport">

                                                            </div>
                                                            <i class="fa fa-question-circle" data-original-title="Refers to all non-retail or non-food services provided by the airport such as massage, ATMs, banking, business centers, barber shops, etc." 
                                                   data-container="body"data-toggle="tooltip"></i>

                                                        </td>

                                                    </tr>

                                                    <tr>

                                                        <td align="right" style="border-top: none !important;">							

                                                            <div class="form-group">

                                                                <label for="inputEmail3" class="col-sm-12 control-label">Advertising</label>

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="col-sm-12">

                                                                <input type="text" name="advertisingrev" value="<?php echo $wideInfo_traffic[0]['advertisingrev']; ?>" class="form-control" id="" placeholder="Revenue">

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="col-sm-11">

                                                                <input type="text" name="advertisingrevtoair"   value="<?php echo $wideInfo_traffic[0]['advertisingrevtoair']; ?>" class="form-control" id="" placeholder="Revenue to Airport">

                                                            </div>
                                                            
                                                            <i class="fa fa-question-circle" data-original-title="Gross revenue and rent to the airport, where available, are provided. This category includes all revenue generated from advertising space at the airport that has been bought by companies paying for said space for the purpose of marketing their brand, including dioramas, wall space, banners, etc." 
                                                   data-container="body"data-toggle="tooltip"></i>

                                                        </td>

                                                    </tr>

                                                    <tr>

                                                        <td align="right" style="border-top: none !important;">							

                                                            <div class="form-group">

                                                                <label for="inputEmail3" class="col-sm-12 control-label">Currency Exchange</label>

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="col-sm-12">

                                                                <input type="text" name="currencyexrev" value="<?php echo $wideInfo_traffic[0]['currencyexrev']; ?>" class="form-control" id="" placeholder="Revenue">

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="col-sm-11">

                                                                <input type="text" name="currencyexrevtoair" value="<?php echo $wideInfo_traffic[0]['currencyexrevtoair']; ?>" class="form-control" id="" placeholder="Revenue to Airport">

                                                            </div>

                                                        </td>

                                                    </tr>

                                                </table>

                                            </div>	

                                        </div>

                                    </div>

                                </div>

                            </div>



                            <!-- hih there -->

                            </br>

                            </br>



                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="panel panel-primary">

                                        <div class="panel-heading">

                                            <h4>Airport Ratios</h4>

                                        </div>

                                        <div class="panel-body">

                                            <div class="table-responsive">

                                                <table class="table" border="0" cellspacing="0" cellpadding="0" style="width: 99% !important;">

                                                    <tr>

                                                        <td align="left" style="border-top: none !important;">							

                                                            <div class="form-group">

                                                                <label for="" class="col-sm-12 control-label">Pre/Post Security % of SqFt:</label>

                                                            </div>

                                                        </td>

                                                        <td align="right" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-4" for="">Pre:</label>

                                                                <div class="col-sm-8">

                                                                    <input type="text" name="presecurity" value="<?php echo $wideInfo_traffic[0]['presecurity']; ?>" class="form-control" id="" placeholder="ex. 0" >

                                                                </div>

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-2" for="">% Post:</label>

                                                                <div class="col-sm-7 pull-left">

                                                                    <input type="text" name="postsecurity" value="<?php echo $wideInfo_traffic[0]['postsecurity']; ?>" class="form-control" id="" placeholder="ex. 0" >

                                                                </div>

                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="The percentage of concessions space that is located pre- versus post-security, i.e. 20/80 would translate to 20% of the concession space is located before security and 80% is located beyond security."></i>

                                                            </div>

                                                        </td>

                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="border-top: none !important;">							

                                                            <div class="form-group">

                                                                <label for="" class="col-sm-12 control-label">Ratio of Business to Leisure:</label>

                                                            </div>

                                                        </td>

                                                        <td align="right" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-4" for="">Business/Leisure:</label>

                                                                <div class="col-sm-8">

                                                                    <input type="text" name="ratiobusleisure" value="<?php echo $wideInfo_traffic[0]['ratiobusleisure']; ?>" class="form-control" id="" placeholder="ex. 15/85" >

                                                                </div>

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-4" for="">% (ex. 15/85)</label>





                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title=" The percentage of total enplaning passengers that are traveling on business versus the percentage traveling for leisure or pleasure, i.e. 75/25 would translate to 75% of the people departing the airport are taking a business trip and 25% are on a vacation trip. "></i>





                                                            </div>

                                                        </td>

                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="border-top: none !important;">							

                                                            <div class="form-group">

                                                                <label for="" class="col-sm-12 control-label">O & D To Transfer:</label>

                                                            </div>

                                                        </td>

                                                        <td align="right" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-4" for="">O&D:</label>

                                                                <div class="col-sm-8">

                                                                    <input type="text" name="ondtransfer" value="<?php echo $wideInfo_traffic[0]['ondtransfer']; ?>" class="form-control" id="" placeholder="ex. 15/85" >

                                                                </div>

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-4" for="">% (ex. 15/85)</label>



                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="This is ratio denoting the percentage of origination and destination passengers relative to the percentage of passengers transferring from one flight to another in the airport."></i>



                                                            </div>

                                                        </td>

                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="border-top: none !important;">							

                                                            <div class="form-group">

                                                                <label for="inputEmail3" class="col-sm-12 control-label">Average Dwell Time:</label>

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-4" for=""> </label>

                                                                <div class="col-sm-8">

                                                                    <input type="text" name="avgdwelltime" value="<?php echo $wideInfo_traffic[0]['avgdwelltime']; ?>"  class="form-control" id="" placeholder="ex. 0" >

                                                                </div>

                                                            </div>

                                                        </td>

                                                        <td align="left" style="border-top: none !important;">

                                                            <div class="form-group">

                                                                <label class="control-label col-sm-5" for="">min. (number of minutes)</label>



                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-original-title="The average amount of time a departing passenger spends within the airport."></i>





                                                            </div>

                                                        </td>

                                                    </tr>

                                                </table>

                                            </div>	

                                        </div>

                                    </div>

                                </div>

                            </div>

                            </br>

                            </br>

                            <input type="hidden" name="step2_airport" value="form1">

                            <div class="form-group" style="margin-left: 0 !important;">

                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Save Change</button>&nbsp;&nbsp;<a href="<?php echo site_url('wizard/airport'); ?>" class="btn btn-danger"><i class="fa fa-reply"></i>&nbsp;Cancel</a>

                            </div>

                        </div>

                        <div class="space-4"></div> 

                    </form> 			



                    <div class="row">

                        <div class="col-md-12" style="width: 99%; padding-left: 29px;">

                            <div class="panel panel-primary">

                                <div class="panel-heading">

                                    <h4>Existing Terminals for <?php echo $main_data[0]['IATA'] ?>:</h4>

                                </div>

                                <div class="panel-body">

                                    <div class="table-responsive">

                                        <table class="table table-hover">

                                            <thead>

                                                <tr style="border-top: none !important;">

                                                    <td><b>Terminals/Concourses</b></td>

                                                    <td><b>Abbreviation</b></td> 

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php

                                                if (count($wideInfo_traffic) > 0) {



//                                                    echo count($wideInfo_traffic); exit;

                                                    foreach ($wideInfo_traffic as $newVal) {

                                                        ?>



                                                        <tr>

                                                            <td align="left"><?php echo $newVal['terminalname']; ?></td>



                                                            <td align="left"><?php echo $newVal['terminalabbr']; ?></td>

                                                        </tr>

                                                        <?php

                                                    }

                                                } else {

                                                    echo "No results!!";

                                                }

                                                ?>	



                                            </tbody>

                                        </table>

                                    </div>	

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                </div>

                </div>

            </section>
            
            <div style="clear: both;"></div>

            <div class="col-sm-12" style="font-family:verdana;">

                <div class="btn-group btn-group-justified" role="group" aria-label="...">

                    <div class="btn-group" role="group">



                        <div class="col-sm-2 pull-left" style="padding-left: 0px;">

                            <label class="control-label">Next, click on </label>

                            <a  href="<?php echo site_url('wizard/airport/airport_edit_steps/3/' . $this->uri->segment(5)); ?>" class="btn text-uppercase<?php

                            if ($this->uri->segment(4) == "3") {

                                echo " btn-warning";

                            } else {

                                echo " btn-primary";

                            }

                            ?>">

                                <u>Step 3</u><br /> TERMINALS

                            </a>               

                        </div>

                    </div>  

                </div>

            </div>

        </section>



        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">

                        <input type="hidden" id="recdelid" value="">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                        </button>

                        <h4 class="modal-title" id="myModalLabel">Conifm Delete</h4>

                    </div>

                    <div class="modal-body">

                        Are you sure want to delete this Airport contact?

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-danger" data-dismiss="modal">

                            <i class="fa fa-times">&nbsp;</i>Cancel</button>        

                        <button type="submit" class="btn btn-success suredel"><i class="fa fa-check">&nbsp;</i>Delete

                        </button>        

                    </div>

                </div>

            </div>

        </div>

        <div class="mclone hide">  

            <!--<div class="col-sm-12">-->

            <div class="col-sm-6">

                <div class="form-group">

                    <label class="control-label col-sm-4" for="texpansionplanned">Expansion Planned:</label>

                    <!--<div class="col-sm-8">-->

                    <div class="col-xs-6">

                        <!--<input type="text" name="arr_texpansionplanned[]" class="form-control" id="texpansionplanned" placeholder="Enter Expansion Planned" >-->

                        <input type="text" name="texpansionplanned[]" class="form-control" id="texpansionplanned" placeholder="Enter Expansion Planned" >

                    </div>

                    <!--                <div class="col-xs-2">

                                        &nbsp;<button type="button" id="addmoreterminal" title="Add more Terminal Option" class="col-xs-12 col-sm-10 btn btn-xs btn-success"><i class="icon-plus"></i>Add</button>

                                    </div>-->

                    <!--</div>-->

                </div>



                <div class="form-group">

                    <label class="control-label col-sm-4" for="completedexpdate">Complete Date:</label>

                    <div class="col-sm-6">

                        <!--<input type="text" name="arr_completedexpdate[]" class="form-control" id="completedexpdate" placeholder="Enter Title" >-->

                        <input type="text" name="completedexpdate[]" class="form-control" id="completedexpdate" placeholder="Enter Title" >

                    </div>

                </div>

            </div>



            <div class="col-sm-6">

                <div class="form-group">

                    <label class="control-label col-sm-4" for="addsqft">Addl Sq. Ft.:</label>

                    <div class="col-sm-6">

                        <!--<input type="text" name="arr_addsqft[]" class="form-control" id="addsqft" placeholder="Enter Addl Sq. Ft">-->

                        <input type="text" name="addsqft[]" class="form-control" id="addsqft" placeholder="Enter Addl Sq. Ft">

                    </div>

                </div>

                <div class="form-group">

                    <label class="control-label col-sm-4" for="addsqft">&nbsp;</label>

                    <div class="col-sm-6">

                        &nbsp;

                    </div>

                </div>



            </div>

            <!--</div>-->

        </div>

        <div class="clearfix"></div>       

        <?php echo $this->load->view($footer); ?>

        <div class="clearfix"></div>

        <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>        

        <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url('assets'); ?>/js/jquery.dataTables.bootstrap.js"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/chosen.jquery.min.js"></script>



        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>

        <script>



            $.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="submit"], button):enabled';





            $("#mgtstructure").prepend("<option value='' >Please Select&nbsp;</option>").trigger('chosen:updated');

//$("#mgtstructure").prepend("<option value='' selected='selected'>&nbsp;</option>").trigger('chosen:updated');





            $('#a_info').validator();





            $("#addmoreterminal").click(function ()

            {

                var clone = $(".mclone").html();

                $(".terminla").append(clone);



            });

            $(".addmorecontact").click(function ()

            {

                $("#addmorecontacthidden").val(1);

            });



        </script>

        <script type="text/javascript">

            function showmodal(bit)

            {

                if (bit == 1)

                {

                    $("#myModal").modal("show");

                } else

                {

                    $("#myModal").modal("hide");

                }

            }

            function delme(idd)

            {

                $("#recdelid").attr("value", idd);

                showmodal(1);

            }

            $(".suredel").click(function ()

            {

                var delid = $("#recdelid").val();

                $(this).html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Deleteing...');

                $.ajax({

                    url: "<?php echo site_url('frontend/airport_delete_step_1'); ?>",

                    type: "POST",

                    cache: false,

                    data: "acid=" + delid + "&action=delstep1",

                    success: function (html)

                    {

                        if (html == 1)

                        {

                            showmodal(0);

                            window.location = "<?php echo current_url(); ?>";

                        } else

                        {

                            $(".modal-body").html(html);

                        }

                    }

                });



            });





            function removeterminal(tt)

            {



                $("#compl"+ tt).html('');

                $("#texpan"+ tt).html('');

                $("#sqft"+ tt).html('');

                $("#btnn"+ tt).html('');

                

            }





        </script> 

        <script type="text/javascript">



            function checkval()

            {



//                    var rfield = new Array("configuration", "mgtstructure", "texpansionplanned[]", "addsqft[]", "completedexpdate[]");

                var rfield = new Array("configuration", "mgtstructure");

//                    var msg = new Array('Airport Configuration', 'Concessions Mgt. Type', 'Expansion Planned', 'Addl Sq. Ft.', 'Complete Date');

                var msg = new Array('Airport Configuration', 'Concessions Mgt. Type');

                var errmsg = "";

                for (i = 0; i < rfield.length; i++)

                {

                    //alert(rfield[i]);

                    var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                    if (val == "" || val.replace(" ", "") == "")

                    {

                        errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";

                    }

                }

                if (errmsg != "")

                {

                    $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                    $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                    return false;

                }

                return true;

            }

        </script>

        <script type="text/javascript">

            $(document).ready(function () {

                $('[data-toggle="tooltip"]').tooltip({

                    placement: 'top'

                });

            });



            jQuery(document).ready(function () {

                jQuery(".chosen").chosen();

            });

        </script>

</body>