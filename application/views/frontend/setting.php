<?php echo $this->load->view($header); ?>

<title>

</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/jquery-ui-1.10.3.full.min.css" >

<style>
    .help-block ul li {
        margin-left: 0px ! Important;
    }
    
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    cursor: not-allowed;
    background-color: #fff ! Important;
    opacity: 1;
}
</style>
</head>

<body>

    <div class="main-wrapper">

        <?php echo $this->load->view($menu); ?>

        <div class="clearfix"></div>

        <section class="main-content">		     	

            <section class="container-fluid">   

                <?php echo $this->load->view('frontend/include/breadcrumb'); ?>       

                <section class="bottom-area-tabs">

                    <div role="tabpanel" class="bottom-area-tabs-body">


                        <h2 class="default-title margin-bottom-ten">Change Password</h2>

                        <div class="tab-reset-pass form-horizontal">

                            <div class="clearfix"></div>   
                            <div class="error">

                            </div>   

                            <div class="col-sm-8 margin-auto">

                                <!--<form class="" action="" method="post" onSubmit="return !!(passwordcheck() & checkval());">-->
                                <form class="form-horizontal" id="form1" data-toggle="validator" role="form" action="" method="post" onSubmit="return !!(passwordcheck());">

                                    <div class="form-group has-feedback">

                                        <label class="col-sm-5 control-label">Original Password:

                                            &nbsp;<span style="color: #d9534f;" class="danger">*</span></label>                                            

                                        <div class="col-sm-7">

                                            <input type="password" class="form-control" id="opassword" name="opassword" placeholder="Original Password"placeholder="Original Password" data-error="Password is invalid" required autocomplete="off" style="cursor:text" readonly style="background: #fff ! Important;" onfocus="this.removeAttribute('readonly');" >
                                            <!--<input type="password" class="form-control" id="opassword" name="opassword" placeholder="Original Password"  data-error="Password is invalid" required autocomplete="off">-->

                                        </div>

                                        <div class="col-sm-7 pull-right help-block with-errors"></div>

                                    </div>

                                    <div class="form-group has-feedback">

                                        <label class="col-sm-5 control-label">New Password:&nbsp;<span style="color: #d9534f;" class="has-warning">*</span></label>

                                        <div class="col-sm-7">

                                            <input type="password" class="form-control" id="nopassword" name="nopassword" placeholder="New Password" placeholder="New Password" data-error="New Password is invalid" required autocomplete="off" style="cursor:text" readonly onfocus="this.removeAttribute('readonly');" style="background: #fff ! important;">
                                            <!--<input type="password" class="form-control" id="nopassword" name="nopassword" placeholder="New Password"  data-error="New Password is invalid" required autocomplete="off">-->

                                        </div>

                                        <div class="col-sm-7 pull-right help-block with-errors"></div>

                                    </div>

                                    <div class="form-group has-feedback">

                                        <label class="col-sm-5 control-label">Type New Password again:<span style="color: #d9534f;" class="red">*</span></label>

                                        <div class="col-sm-7">

                                            <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password" data-match="#nopassword" data-match-error="Whoops, Password don't match" required readonly style="cursor:text" onfocus="this.removeAttribute('readonly');" style="background: #fff ! important;">
                                            <!--<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password" data-match="#nopassword" data-match-error="Whoops, Password don't match" required >-->

                                        </div>

                                        <div class="col-sm-7 pull-right help-block with-errors"></div>

                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-offset-5 col-sm-10">

                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>

                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </section>

            </section>

        </section>

        <div class="clearfix"></div>       

        <?php echo $this->load->view($footer); ?>

        <div class="clearfix"></div>

    </div>

    <script src="<?php echo base_url('fassests'); ?>/js/jquery.min.js"></script>     

    <script src="<?php echo base_url('fassests'); ?>/js/bootstrap.min.js"></script>

    <script src="http://cdn.datatables.net/plug-ins/1.10.7/type-detection/formatted-num.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.full.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>

    <script type="text/javascript" class="init">


        $('#form1').validator();

        var seg = "<?php echo $this->uri->segment(0) ?>";

        if (seg != '') {
            $('title').html("Setting | ARN Fact Book " + seg);
        }
        else {
            $('title').html("Setting | ARN Fact Book");
        }

        function checkval()

        {

            var rfield = new Array("opassword", "nopassword", "cpassword");

            var msg = new Array('Original Password', 'New Password', 'Confirm Password');

            var errmsg = "";

            for (i = 0; i < rfield.length; i++)

            {
                var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                if (val == "" || val.replace(" ", "") == "")

                {

//                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";
                    errmsg += "" + msg[i] + " is Required. <br/>";

                }

            }

            if (errmsg != "")

            {

                $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                return false;

            }

            return true;

        }

//        $(".chosen-select").chosen({width: "83.4%"});


        function movetoerror(msg)

        {

            $(".error").html(msg);

            $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

        }

        function passwordcheck()

        {

            var opassword = $("#opassword").val();

            var cpassword = $("#cpassword").val();

            var npassword = $("#nopassword").val();

            if (opassword == "" || cpassword == "" || npassword == "")

            {

                //var set="<?php // echo $this->common->setmessage('(*) asterisk is indicates field is required.', '-1'); ?>";	

//                                                    movetoerror("<div class='row'><div class=''><div class='alert alert-danger'>(*) asterisk is indicates field is required.</div></div></div>");

            } else if (cpassword != npassword)

            {

                movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-danger'>Confirm Password must be match with password.</div></div></div>");

            } else

            {

                movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-success'><i class='fa fa-spinner fa-spin fa-1x'></i>&nbsp; Updateing password.</div></div></div>");

                $.ajax({
                    url: "<?php echo site_url('ajax/doaction'); ?>",
                    data: "opassword=" + opassword + "&cpassword=" + cpassword + "&npassword=" + npassword + "&action=changepassword",
                    type: "POST",
                    cache: false,
                    async: false,
                    success: function (html)

                    {

                        if (html == 1)

                        {

                            movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-success'><i class='fa fa-check fa-1x'></i>&nbsp;Password has been update Successfully.</div></div></div>");

                        } else

                        {

                            movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-danger'>" + html + "</div></div></div>");

                        }

                    }

                });

            }

            return false;

        }

    </script>

</body>

</html>

