<?php echo $this->load->view($header);?>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Privacy | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Privacy | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests');?>/css/jquery.dataTables.css">
</head>
<body>
	<div class="main-wrapper">
    	<?php echo $this->load->view($menu);?>
        <div class="clearfix"></div>       
        <section class="main-content">  
				<?php /*?><section class="col-sm-2">
				 <img src="<?php echo base_url('ads');?>/adsample5.jpg" alt="ad" width="140" height="200" border="0">
							
				</section><?php */?>
        	<section class="col-sm-12">
            	 <?php echo $this->load->view('frontend/include/breadcrumb');?>            	            	
                <div class="col-md-12 col-sm-9 col-xs-12 login-right-sect">                	
                   <div class="row">
				   <div class="col-sm-12">
					<h3><p class="text-danger">What information do we collect? </p></h3>	
					
					<p>We collect information from you when you place an order.</p>
					
					<p>When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address, mailing address, phone number or credit card information.</p>

					<h3><p class="text-danger">What do we use your information for?</p></h3>	
					
					<p>Any of the information we collect from you may be used in one of the following ways:</p>	
					<ul>
						<li>To personalize your experience (your information helps us to better respond to your individual needs)</li>
						<li>To improve our website (we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>
						<li>To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</li>
						<li>To process transactions</li>
						<li>Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.</li>
						<li> To administer a contest, promotion, survey or other site feature</li>
						<li>To send periodic emails
						<li>The email address you provide for order processing, will only be used  to send you information and updates pertaining to your order.</li>
						</li>
				  </ul>
					
					<h3><p class="text-danger">How do we protect your information?</p></h3>	
					
					<p>We implement a variety of security measures to maintain the safety of your personal information when you place an order.</p>
					
					<p>We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Database to be only accessed by those authorized with special access rights to our systems, and are required to?keep the information confidential.</p>
					<p>After a transaction, your private information (credit cards, social security numbers, financials, etc.) will not be kept on file for more than 60 days.</p>
					 
					
					<h3><p class="text-danger">Do we use cookies?</p></h3>	
					<p>Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information.</p>
					<p>If you prefer, you can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies via your browser settings. Like most websites, if you turn your cookies off, some of our services may not function properly. However, you can still place orders over the telephone.</p>
					
					
					
					<h3><p class="text-danger">Do we disclose any information to outside parties?</p></h3>	
					
					<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>  
					
					<h3><p class="text-danger">Third party links</p></h3>	
					<p> Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
					 
					<h3><p class="text-danger">California Online Privacy Protection Act Compliance</p></h3>	
					<p>Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.</p>  

					<h3><p class="text-danger">Online Privacy Policy Only</p></h3>	
					<p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>  
					
					
					<h3><p class="text-danger">Terms and Conditions</p></h3>	
					<p>Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website at <a href="http://www.airportrevenuenews.com/terms-of-use/">http://www.airportrevenuenews.com/terms-of-use/</a></p>  
					
					<h3><p class="text-danger">Your Consent</p></h3>	
					<p>By using our site, you consent to our online privacy policy.</p>  
					<p>This policy was last modified on March 25, 2014</p>
					
					<h3><p class="text-danger">Contacting Us</p></h3>	
					<p>If there are any questions regarding this privacy policy you may contact us using the information below. </p>  
					<p>3200 North Military Trail #110</p>  
					<p>Boca Raton, Florida 33431</p>  
					<p>USA</p>
					<a href="mailto:info@airportrevenuenews.com">info@airportrevenuenews.com</a>
					
					
					  </div>
                    </div>                     
                </div>                
            </section>
        </section>
        <?php echo $this->load->view($footer);?>
    </div>
<script src="<?php echo base_url('fassests');?>/js/jquery.min.js"></script>        
<script src="<?php echo base_url('fassests');?>/js/bootstrap.min.js"></script>
</body>
</html>
