<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="<?php echo base_url('fassests');?>/css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div class="main-wrapper">
    	<header class="header">
        	<section class="inner-wrapper">
            	<div class="header-upper-sect">
                	<div class="search-area">
                        <form>
                            <input type="text" value="" placeholder="Search" />
                            <input value="" type="submit">
                        </form>
                	</div>
                	<div class="clearfix"></div>
                </div>
                <div class="header-second-sect">
                    <div class="logo">
                        <h1>
                            <a href="#"><img src="<?php echo base_url('fassests');?>/images/logo.png" title="logo" /></a>
                        </h1>
                    </div>
                    <div class="register-sect"><img src="<?php echo base_url('fassests');?>/images/register.jpg" /></div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="header-lower-sect">
                    <nav class="nav-sect">
                        <ul>
                            <li><a href="#">HOME</a></li>
                            <li><a href="#">AIRPORTS</a></li>
                            <li><a href="#">LEASE EXP</a></li>
                            <li><a href="#">CONTACTS</a></li>
                            <li><a href="#">COMPANIES</a></li>
                            <li><a href="#">TENANTS</a></li>
                            <li><a href="#">REPORTS</a></li>
                            <li><a href="#">GLOSSARY</a></li>
                            <li><a href="#">CONTACT US</a></li>
                            <li><a href="#">LOG OUT</a></li>
                        </ul>
                    </nav>
               		<div class="clearfix"></div>
                </div>
                    
                
                <div class="clearfix"></div>
            </section>
            <div class="clearfix"></div>
        </header>
        <div class="clearfix"></div>
        <section class="main-content">
        	<section class="inner-wrapper">
            	<h1 class="default-title">Airport Listings</h1>
                <p class="default-text margin-top-ten">Airport Listings: Each airport included in ARN’s OFB 2014 has entered their information directly into this database. This year more than 90 airports submitted their concessions data to ARN for the purposes of this service.  The majority of airports are reporting their information on a terminal-by-terminal basis (all information is based on 2012 data). Where an airport was unable to provide the information broken down by concourse or terminal, the data is shown in the aggregate for the airport’s entire program. The book indicates where “airportwide information only” is shown. </p>
            </section>
        </section>
        <div class="clearfix"></div>
        <footer class="footer">
        	<section class="inner-wrapper">
            	<section class="footer-sect1 footer-sect">
                	<h2>SITE MAP</h2>
                    <div class="footer-text-widget">
                        <ul>
                            <li><a href="#">HOME</a></li>
                            <li><a href="#">AIRPORTS</a></li>
                            <li><a href="#">LEASE EXP</a></li>
                            <li><a href="#">CONTACTS</a></li>
                            <li><a href="#">COMPANIES</a></li>
                            <li><a href="#">TENANTS</a></li>
                            <li><a href="#">REPORTS</a></li>
                            <li><a href="#">GLOSSARY</a></li>
                            <li><a href="#">CONTACT US</a></li>
                            <li><a href="#">LOG OUT</a></li>
                        </ul>                        
                    </div>
                </section>
                <section class="footer-sect2 footer-sect">
                	<h2>CONTACT US</h2>
                    <div class="footer-text-widget">
                    	<p class="email-contact" >You can Get in touch with us by <a href="#">email</a>. Alternatively you can give us a call or fax us any time:</p>
                        <address>
                            <span>Tel: 561.477.3417</span>
                            <span>Fax: 561.228.0882</span>
                        </address>
                        <p class="email-contact">3200 North Military Trail, Suite 110 Boca Raton, FL 33431</p>
					</div>
                </section>
                <section class="footer-sect3 footer-sect">
                	<h2>GLOSSARY</h2>
                    <div class="footer-text-widget">
                    	<p>Airport Listings: Each airport included in ARN’s OFB 2014 has entered their information directly into this database. This year more than 90 airports submitted their concessions data to ARN for the purposes of this service.</p>
                        
                        
                    </div>
                </section>
            	<div class="clearfix"></div>
            </section>
        	<div class="clearfix"></div>
        </footer>
        <div class="clearfix"></div>
    </div>
</body>
</html>
