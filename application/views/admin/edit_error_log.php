<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Modify Tickets | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'Modify Tickets | ARN Fact Book';
            }
            ?>
        </title>
        <?php echo $this->load->view($header); ?>
    </head>
    <body>
        <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>                    
                    <div class="page-content">                   
                        <div class="row">                        
                            <div class="col-xs-12">
                                <div class="error"><?php echo $this->common->getmessage(); ?></div>
                                <?php
                                $id = $this->uri->segment(3);
                                ?>
                                <form method="post" name="form1" class="form-horizontal" id="form1" data-toggle="validator" role="form" action="<?php echo site_url('admin/edit_error_log') . '/' . $id; ?>">                    <table width="100%" border="0" cellpadding="0" cellspacing="0"> 


                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Error Log Subject:&nbsp;<span class="red">*</span>  
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="col-xs-10 col-sm-10" name="error_log_subject" type="text" id="subject" size="50" maxlength="50" value="<?php echo htmlentities($log['error_log_subject'], ENT_COMPAT, 'iso-8859-1'); ?>" data-error="Subject is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Error Log Message:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <textarea rows="8" class="col-xs-10 col-sm-10" name="error_log_message" id="message" data-error="Message is invalid" required><?php echo strip_tags($log['error_log_message']); ?></textarea>
                                                    </div>
                                                    <div class="col-sm-8 pull-right help-block with-errors"></div>
                                                </div>
                                            </td>                            
                                        </tr> 
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Status: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="checkbox" name="error_log_status" value="1" <?php
                                                        if (!(strcmp(htmlentities($log['error_log_status'], ENT_COMPAT, 'iso-8859-1'), 1))) {
                                                            echo "checked=\"checked\"";
                                                        }
                                                        ?>>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="MM_update" value="form1">
                                                <input type="hidden" name="user_id" value="<?php echo $log['error_log_id']; ?>">
                                            </td>
                                        </tr>
                                        <tr>                        	
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        &nbsp; 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <button type="submit" onClick="MM_validateForm('subject', '', 'R', 'message', '', 'R');
                                                                return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Update</button>
                                                        <a href="<?php echo site_url('admin/errors_log'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>&nbsp;

                                            </td>                           
                                        </tr>
                                    </table>	
                                </form>                       	   

                            </div>			
                        </div>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
        <?php echo $this->load->view($footer); ?> 

        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>

        <script type="text/javascript">

                                                            $('#form1').validator();

        </script>

    </body>
</html>
