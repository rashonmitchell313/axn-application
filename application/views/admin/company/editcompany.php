<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Modify Company | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'Modify Company | ARN Fact Book';
            }
            ?>
        </title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/choseen.css" />
        <?php echo $this->load->view($header); ?>
    </head>
    <body>
        <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>                    
                    <div class="page-content">                   
                        <div class="row">                        
                            <div class="col-xs-12">
                                <div class="error">
                                    <?php echo $this->common->getmessage(); ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-warning alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                Please use this page to Edit a company.</div>
                                        </div>
                                    </div>   
                                </div>
                                <?php
                                $id = $this->uri->segment(4);
                                ?>
                                <form method="post" name="form1" class="form-horizontal" id="form1" data-toggle="validator" role="form" action="<?php echo site_url('admin/company/edit_company') . '/' . $id; ?>">                    
                                    <input type="hidden" value="<?php echo $company_info['cid']; ?> " name="cid">	
                                    <div class="form-group has-feedback">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Company Name:&nbsp;<span class="red">*</span> 
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['companyname']; ?>" type="text" class="col-xs-10 col-sm-5" name="companyname"  id="companyname" data-error="Company Name is invalid" required>
                                        </div>
                                        <div class="col-sm-6 help-block with-errors"></div>
                                    </div>
                                    <div class="space-4"></div>   
                                    <div class="form-group has-feedback">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Company Type:&nbsp;<span class="red">*</span> 
                                        </label>
                                        <div class="col-sm-8">
                                            <!--<input value="<?php // echo $company_info['companytype'];   ?>"  class="col-xs-10 col-sm-5"  name="companytype" type="text" id="companytype"  size="50" maxlength="50">-->

                                            <select class="chosen-select col-xs-10 col-sm-10" name="companytype" data-placeholder="Select an Option"  data-error="Company Name is invalid" required>

                                                <?php
                                                foreach ($CompanyCategory as $Category):
                                                    ?>

                                                    <option value="<?php echo $Category['category_name']; ?>" <?php if ($company_info['companytype'] == $Category['category_name']) { ?> selected <?php } ?>>

                                                        <?php echo $Category['category_name']; ?>

                                                    </option>

                                                    <?php
                                                endforeach;
                                                ?>

                                            </select>
                                        </div>
                                        <div class="col-sm-6 help-block with-errors"></div>
                                    </div>                        
                                    <div class="space-4"></div> 
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Address1:&nbsp; 
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['address1']; ?>" class="col-xs-10 col-sm-8"  name="address1" type="text" id="address1" size="60" maxlength="150">
                                        </div>
                                        
                                    </div>     
                                    <div class="space-4"></div> 
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Address2: 
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['address2']; ?>" class="col-xs-12 col-sm-8"  name="address2" type="text" id="address2" size="60" maxlength="150">
                                        </div>
                                    </div>  
                                    <div class="space-4"></div> 
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            City: 
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['city']; ?>" class="col-xs-10 col-sm-5"  name="city" type="text" id="City" size="60" maxlength="150">
                                        </div>
                                    </div>                        
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            State:&nbsp; 
                                        </label>
                                        <div class="col-sm-8">                            	
                                            <select  name="state" class="chosen-select">
                                                <?php
                                                foreach ($rsStates as $key => $state):
                                                    ?>

                                                    <option value="<?php echo $state['state_code']; ?>" 
                                                            <?php if ($company_info['state'] == $state['state_code']) { ?> selected <?php } ?>>
                                                                <?php echo $state['state_name']; ?>
                                                    </option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="space-4"></div> 
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Country:&nbsp;
                                        </label>
                                        <div class="col-sm-8">                            	
                                            <select name="country" size="1" id="acountry" class="chosen-select">
                                                <?php
                                                foreach ($row_rsCountries as $country):
                                                    ?>

                                                    <option value="<?php echo strtoupper($country['countries_name']); ?>" <?php if ($company_info['country'] == strtoupper($country['countries_name'])) { ?> selected <?php } ?> >
                                                        <?php echo strtoupper($country['countries_name']); ?>
                                                    </option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="space-4"></div> 
                                    <div class="form-group has-feedback">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Zip:&nbsp;<span class="red">*</span> 
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['zip']; ?>" class="col-xs-10 col-sm-5"  name="zip" type="text" id="zip" size="20" maxlength="20" data-error="Zip is invalid" required>
                                        </div>
                                        <div class="col-sm-6 help-block with-errors"></div>
                                    </div>     
                                    <div class="space-4"></div> 
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Website:
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['website']; ?>" class="col-xs-10 col-sm-5"  name="website" type="text" id="website" size="20" maxlength="20">
                                        </div>
                                    </div>
                                    <div class="space-4"></div> 
                                    <div class="form-group has-feedback">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Phone:&nbsp;<span class="red">*</span> 
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['phone']; ?>" class="col-xs-10 col-sm-5" name="phone" type="text" id="phone" value="" size="10" maxlength="20" data-error="Phone is invalid" required>
                                        </div>
                                        <div class="col-sm-6 help-block with-errors"></div>
                                    </div>
                                    <div class="space-4"></div> 
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Fax:
                                        </label>
                                        <div class="col-sm-8">
                                            <input value="<?php echo $company_info['fax']; ?>" class="col-xs-10 col-sm-5" name="fax" type="text" id="fax" value="" size="10" maxlength="10">
                                        </div>
                                    </div>
                                    <div class="space-4"></div> 
                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Company Info: 
                                        </label>
                                        <div class="col-sm-8">
                                            <textarea class="col-xs-10 col-sm-5"  name="companyinfo" id="companyinfo" cols="45" rows="5"><?php echo $company_info['companyinfo']; ?></textarea>
                                        </div>
                                    </div>                         
                                    <input type="hidden" name="MM_update" value="form1">

                                    <div class="form-group">                          
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Published&nbsp;:
                                        </label>                       
                                        <div class="col-sm-8">
                                            <input type="checkbox" class="ace ace-switch ace-switch-7" name="published" <?php
                                            if (strcmp(htmlentities($company_info['published'], ENT_COMPAT, 'iso-8859-1'), 0)) {
                                                echo "checked=\"checked\"";
                                            }
                                            ?>>
                                            <span class="lbl"></span>
                                            <span class="red">(Checking this set the Company is Published or Not.)</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            &nbsp; 
                                        </label>
                                        <div class="col-sm-8">
                                            <button type="submit" name="update_comp" onClick="MM_validateForm('companyname', '', 'R', 'companytype', '', 'R', 'address1', '', 'R', 'city', '', 'R', 'zip', '', 'R', 'phone', '', 'R');
                                                    return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php echo site_url('admin/company'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                        </div>
                                    </div>
                                    <div class="space-4"></div> 
                                </form> 


                            </div>			
                        </div>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
        <?php echo $this->load->view($footer); ?>  
        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>
        <script type="text/javascript">

                                                $('#form1').validator();
                                                $('#form1').validator({
                                                    rules: {
                                                        state: {
                                                            required: true
                                                        }
                                                    },
                                                    highlight: function (element) {
                                                        $(element).closest('.control-group').removeClass('success,valid').addClass('error').trigger('chosen:updated');
//                                                                        $('#categoryide').trigger('chosen:updated');
                                                    },
                                                    success: function (element) {
                                                        element.text('OK!').addClass('valid')
                                                                .closest('.control-group').removeClass('error').addClass('success');
                                                    }
                                                });

                                                $(".chosen-select").chosen({width: "41.7%",
                                                    search_contains: true
                                                });

                                                function MM_validateForm() { //v4.0

                                                    if (document.getElementById) {

                                                        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;

                                                        for (i = 0; i < (args.length - 2); i += 3) {
                                                            test = args[i + 2];
                                                            val = document.getElementById(args[i]);

                                                            if (val) {
                                                                nm = val.name;
                                                                if ((val = val.value) != "") {

                                                                    if (test.indexOf('isEmail') != -1) {
                                                                        p = val.indexOf('@');

                                                                        if (p < 1 || p == (val.length - 1))
                                                                            errors += '- ' + nm + ' must contain an e-mail address.\n';

                                                                    } else if (test != 'R') {
                                                                        num = parseFloat(val);

                                                                        if (isNaN(val))
                                                                            errors += '- ' + nm + ' must contain a number.\n';

                                                                        if (test.indexOf('inRange') != -1) {
                                                                            p = test.indexOf(':');

                                                                            min = test.substring(8, p);
                                                                            max = test.substring(p + 1);

                                                                            if (num < min || max < num)
                                                                                errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';

                                                                        }
                                                                    }
                                                                } else if (test.charAt(0) == 'R')
                                                                    errors += '- ' + nm + ' is required.\n';
                                                            }

                                                        }
                                                        if (errors) {
                                                            $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:\n' + errors.replace("_", " ").toUpperCase() + "</div></div></div>");
                                                            $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');
                                                        }

                                                        document.MM_returnValue = (errors == '');

                                                    }
                                                }
                                                $("#addmoreterminal").click(function ()
                                                {
                                                    var clone = $(".mclone").html();
                                                    $(".terminla").append(clone);

                                                });
        </script>  

    </body>
</html>
