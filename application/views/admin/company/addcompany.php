<!DOCTYPE html>

<html lang="en">

    <head>

        <title>

            <?php
            if ($this->uri->segment(0) != '') {

                echo 'Add Company | ARN Fact Book ' . $this->uri->segment(0);
            } else {

                echo 'Add Company | ARN Fact Book';
            }
            ?>

        </title>

        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/choseen.css" />

        <?php echo $this->load->view($header); ?>

    </head>

    <body>

        <?php echo $this->load->view($topnav); ?>

        <div class="main-container" id="main-container">

            <script type="text/javascript">

                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }

            </script>

            <div class="main-container-inner">

                <a class="menu-toggler" id="menu-toggler" href="javascript:void(0)">

                    <span class="menu-text"></span>

                </a>

                <?php echo $this->load->view($leftnav); ?>	

                <div class="main-content">

                    <?php echo $this->load->view($breadcrumb); ?>                    

                    <div class="page-content">                   

                        <div class="row">                        

                            <div class="col-xs-12">

                                <div class="error">

                                    <?php echo $this->common->getmessage(); ?>

                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="alert alert-warning alert-dismissible" role="alert">

                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">

                                                    <span aria-hidden="true">×</span></button>

                                                Please use this page to add a new company.</div>

                                        </div>

                                    </div>   

                                </div>

                                <!--<form method="post" name="form1" class="form-horizontal" onSubmit="return checkval();" action="<?php // echo site_url('admin/company/add_company'); ?>">-->
                                <form method="post" name="form1" class="form-horizontal" id="form1" data-toggle="validator" role="form" action="<?php echo site_url('admin/company/add_company'); ?>">

                                    <table width="100%">

                                        <tr>

                                            <td>

                                                <div class="form-group has-feedback">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Company Name:&nbsp;<span class="red">*</span> 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input type="text" class="col-xs-10 col-sm-10" name="companyname"  id="companyname" data-error="Company Name is invalid" required>

                                                    </div>
                                                    
                                                    <div class="col-sm-6 help-block with-errors"></div>

                                                </div>

                                            </td>

                                            <td>

                                                <div class="form-group control-group has-feedback">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Company Type:&nbsp;<span class="red">*</span> 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <!--<input class="col-xs-10 col-sm-10"  name="companytype" type="text" id="companytype"  size="50" maxlength="50">-->

                                                        <select class="chosen-select col-xs-10 col-sm-10" id="companytype" name="companytype" data-placeholder="Select Company Category" data-error="Company Name is invalid" required>

                                                            <?php
                                                            foreach ($CompanyCategory as $Category):
                                                                ?>

                                                                <option value=""></option>    
                                                                <option value="<?php echo $Category['category_name']; ?>">

                                                                    <?php echo $Category['category_name']; ?>

                                                                </option>

                                                                <?php
                                                            endforeach;
                                                            ?>

                                                        </select>

                                                    </div>
                                                    
                                                    <div class="col-sm-8 pull-right help-block with-errors"></div>

                                                </div>

                                            </td>

                                        </tr>

                                        <tr>

                                            <td>

                                                <div class="form-group ">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Address1:&nbsp;

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input class="col-xs-10 col-sm-10"  name="address1" type="text" id="address1" size="60" maxlength="150">

                                                    </div>

                                                </div>

                                            </td>

                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Address2: 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input class="col-xs-12 col-sm-10"  name="address2" type="text" id="address2" size="60" maxlength="150">

                                                    </div>

                                                </div>

                                            </td>

                                        </tr>

                                        <tr>

                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        City: 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input class="col-xs-10 col-sm-10"  name="city" type="text" id="city" size="60" maxlength="150">

                                                    </div>

                                                </div>

                                            </td>

                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        State:&nbsp; 

                                                    </label>

                                                    <div class="col-sm-8">                            	

                                                        <select class="chosen-select col-xs-10 col-sm-10" name="state" data-placeholder="Select State">

                                                            <?php
                                                            foreach ($rsStates as $key => $state):
                                                                ?>



                                                                <option value="<?php echo $state['state_code']; ?>">

                                                                    <?php echo $state['state_name']; ?>

                                                                </option>

                                                                <?php
                                                            endforeach;
                                                            ?>

                                                        </select>

                                                    </div>

                                                </div>

                                            </td>

                                        </tr>

                                        <tr>

                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Country:&nbsp;

                                                    </label>

                                                    <div class="col-sm-8">                            	

                                                        <select class="chosen-select col-xs-10 col-sm-10" name="country" size="1" id="acountry">

                                                            <?php
                                                            foreach ($row_rsCountries as $country):
                                                                ?>



                                                                <option value="<?php echo strtoupper($country['countries_name']); ?>"<?php
                                                                if (!(strcmp(strtoupper($country['countries_name']), 'UNITED STATES'))) {
                                                                    echo "selected=\"selected\"";
                                                                }
                                                                ?>>

                                                                    <?php echo strtoupper($country['countries_name']); ?>

                                                                </option>

                                                                <?php
                                                            endforeach;
                                                            ?>

                                                        </select>

                                                    </div>

                                                </div>

                                            </td>

                                            <td>

                                                <div class="form-group has-feedback">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Zip:&nbsp;<span class="red">*</span> 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input class="col-xs-10 col-sm-10"  name="zip" type="text" id="zip" size="20" maxlength="20" data-error="Zip is invalid" required>

                                                    </div>
                                                    
                                                    <div class="col-sm-6 help-block with-errors"></div>

                                                </div>

                                            </td>

                                        </tr>

                                        <tr>

                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Website:

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input class="col-xs-10 col-sm-10"  name="website" type="text" id="website" size="20" maxlength="20">

                                                    </div>

                                                </div>

                                            </td>

                                            <td>

                                                <div class="form-group has-feedback">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Phone:&nbsp;<span class="red">*</span> 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input class="col-xs-10 col-sm-10" name="phone" type="text" id="phone" value="" size="10" maxlength="20" data-error="Phone is invalid" required>

                                                    </div>
                                                    
                                                    <div class="col-sm-6 help-block with-errors"></div>

                                                </div>

                                            </td>

                                        </tr>

                                        <tr>

                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Fax:

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <input class="col-xs-10 col-sm-10" name="fax" type="text" id="fax" value="" size="10" maxlength="10">

                                                    </div>

                                                </div>

                                                <div class="form-group">                          
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        &nbsp;
                                                    </label>                       
                                                    <div class="col-sm-8">

                                                    </div>
                                                </div>
                                                <!--                                                <div class="form-group">                          
                                                                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                                                        Published&nbsp;:
                                                                                                    </label>                       
                                                                                                    <div class="col-sm-8">
                                                                                                        <input type="checkbox" class="ace ace-switch ace-switch-7" name="published" >
                                                                                                        <span class="lbl"></span>
                                                                                                        <span class="red">(Checking this set the Company is Published or Not.)</span>									
                                                                                                    </div>
                                                                                                </div>-->

                                            </td>

                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        Company Info: 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <textarea class="col-xs-10 col-sm-10"  name="companyinfo" id="companyinfo" cols="45" rows="5"></textarea>

                                                    </div>

                                                </div>

                                                <input type="hidden" name="MM_update" value="form1">

                                            </td>
                                        </tr>

                                        <tr>
                                            <td>

                                                <div class="form-group">

                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 

                                                        &nbsp; 

                                                    </label>

                                                    <div class="col-sm-8">

                                                        <!--<button type="submit" onClick="MM_validateForm('companyname', '', 'R', 'companytype', '', 'R', 'address1', '', 'R', 'city', '', 'R', 'zip', '', 'R', 'phone', '', 'R'); return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php echo site_url('admin/company'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>-->
                                                        <button type="submit" name="add_comp" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php echo site_url('admin/company'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>

                                                    </div>

                                                </div>

                                            </td>

                                        </tr>

                                    </table>  							

                                   

                                </form> 





                            </div>			

                        </div>

                    </div><!-- /.page-content -->

                </div><!-- /.main-content -->

            </div><!-- /.main-container-inner -->

            <a href="javascript:void(0)" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">

                <i class="icon-double-angle-up icon-only bigger-110"></i>

            </a>

        </div><!-- /.main-container -->

        <?php echo $this->load->view($footer); ?>  

        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>

        <script type="text/javascript">

                                    $("#companytype").prepend("<option value='' selected='selected'>&nbsp;</option>").trigger('chosen:updated');

                                    $('#form1').validator();
                                    $('#form1').validator({
                                        rules: {
                                            state: {
                                                required: true
                                            }
                                        },
                                        highlight: function (element) {
                                            $(element).closest('.control-group').removeClass('success,valid').addClass('error').trigger('chosen:updated');
//                                                                        $('#categoryide').trigger('chosen:updated');
                                        },
                                        success: function (element) {
                                            element.text('OK!').addClass('valid')
                                                    .closest('.control-group').removeClass('error').addClass('success');
                                        }
                                    });


                                    function checkval()

                                    {

                                        var rfield = new Array("companyname", "companytype", "address1", "zip", "phone");

                                        var msg = new Array('Company Name', 'Company Type', 'Address', 'ZIP', 'Phone');

                                        var errmsg = "";

                                        for (i = 0; i < rfield.length; i++)

                                        {

                                            //alert(rfield[i]);

                                            var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                                            if (val == "" || val.replace(" ", "") == "")

                                            {

//                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";
                                                errmsg += "" + msg[i] + " is Required. <br/>";


                                            }

                                        }

                                        if (errmsg != "")

                                        {

                                            $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                                            $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                                            return false;

                                        }

                                        return true;

                                    }


//                                                            function titleCase(nm) {
//                                                                var titleStr = nm.split(' ');
//                                                                for (var j = 0; j < titleStr.length; j++) {
//                                                                    titleStr[j] = titleStr[j].charAt(0).toUpperCase() + titleStr[j].slice(1).toLowerCase();
//                                                                }
//                                                                return titleStr.join(' ');
//                                                            }

//                                                            function toTitleCase(nm)
//                                                            {
//                                                                // \u00C0-\u00ff for a happy Latin-1
//                                                                return nm.toLowerCase().replace(/_/g, ' ').replace(/\b([a-z\u00C0-\u00ff])/g, function (_, initial) {
//                                                                    return initial.toUpperCase();
//                                                                }).replace(/(\s(?:de|a|o|e|da|do|em|ou|[\u00C0-\u00ff]))\b/ig, function (_, match) {
//                                                                    return match.toLowerCase();
//                                                                });
//                                                            }


//                                                var str = 'helkjasdfd lkjasdfkk jkjasdfkj';  

//        function ConvertToTitleCase(nm)  
//        {  
//            return nm.replace(/\w\S*/g, function (txt)  
//            {  
//                return txt.charAt(0)  
//                    .toUpperCase() + txt.substr(1)  
//                    .toLowerCase();  
//            });  
//        }
//        alert(ConvertToTitleCase(str));


//String.prototype.toTitleCase = function(){
//  var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
//
//  return this.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title){
//    if (index > 0 && index + match.length !== title.length &&
//      match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
//      (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
//      title.charAt(index - 1).search(/[^\s-]/) < 0) {
//      return match.toLowerCase();
//    }
//
//    if (match.substr(1).search(/[A-Z]|\../) > -1) {
//      return match;
//    }
//
//    return match.charAt(0).toUpperCase() + match.substr(1);
//  });
//};



                                    $(".chosen-select").chosen({width: "83.4%"});

//                                     $('.chosen-select').chosen({
//      placeholder_text_single: "Select Project/Initiative...",
//      no_results_text: "Oops, nothing found!"
//    });

                                    /*   function MM_validateForm() { //v4.0
                                     
                                     
                                     
                                     if (document.getElementById) {
                                     
                                     
                                     
                                     var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
                                     
                                     
                                     
                                     for (i = 0; i < (args.length - 2); i += 3) {
                                     test = args[i + 2];
                                     val = document.getElementById(args[i]);
                                     
                                     
                                     
                                     if (val) {
                                     nm = val.name;
                                     if ((val = val.value) != "") {
                                     
                                     
                                     
                                     if (test.indexOf('isEmail') != -1) {
                                     p = val.indexOf('@');
                                     
                                     
                                     
                                     if (p < 1 || p == (val.length - 1))
                                     errors += '- ' + nm + ' must contain an e-mail address.\n';
                                     
                                     
                                     
                                     } else if (test != 'R') {
                                     num = parseFloat(val);
                                     
                                     
                                     
                                     if (isNaN(val))
                                     errors += '- ' + nm + ' must contain a number.\n';
                                     
                                     
                                     
                                     if (test.indexOf('inRange') != -1) {
                                     p = test.indexOf(':');
                                     
                                     
                                     
                                     min = test.substring(8, p);
                                     max = test.substring(p + 1);
                                     
                                     
                                     
                                     if (num < min || max < num)
                                     errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                                     
                                     
                                     
                                     }
                                     }
                                     } else if (test.charAt(0) == 'R')
                                     //                                                                                var test1 = titleCase(nm.replace("_", " "))
                                     var test1 = ConvertToTitleCase(nm.replace("_", " "))
                                     //                                                                           console.log(test1);
                                     errors += '' + test1 + ' is required.<br />';
                                     }
                                     
                                     
                                     
                                     }
                                     if (errors) {
                                     $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors + "</div></div></div>");
                                     
                                     $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');
                                     
                                     }
                                     
                                     
                                     
                                     document.MM_returnValue = (errors == '');
                                     
                                     
                                     
                                     }
                                     } */

                                    $("#addmoreterminal").click(function ()

                                    {

                                        var clone = $(".mclone").html();

                                        $(".terminla").append(clone);



                                    });

        </script>  



    </body>

</html>

