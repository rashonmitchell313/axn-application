<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset='utf-8' />
        <?php echo $this->load->view($header); ?>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Welcome to ARN Fact Book | Admin ' . $this->uri->segment(0);
            } else {
                echo 'Welcome to ARN Fact Book | Admin';
            }
            ?>
        </title>

        <link href="<?php echo base_url(); ?>assets/css/fullcalendar/fullcalendar.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/fullcalendar/fullcalendar.print.css" rel="stylesheet" media='print' />
        <link href='<?php echo base_url(); ?>assets/css/jquery-ui.min.css' rel='stylesheet' />

        <style>

            #calendar .fc-content {
                clear: inherit;
            }

            #calendar .fc-toolbar .fc-clear {
                clear: inherit !important; 
            }

        </style>
    </head>
    <body>
        <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>
                    <div class="page-content">
                        <div class="row">

                            <div id='calendar' class="col-sm-12"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->load->view($footer); ?>

        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery-ui.js'></script>

        <script>


                $(document).ready(function () {

                    var action = "<?php echo $action = $this->uri->segment(2); ?>";

                    $('#calendar').fullCalendar({
                        eventLimit: true, // for all non-agenda views
                        theme: true,
                        buttonText: {
                            prev: '<',
                            next: '>'
                        },
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },
                        defaultDate: new Date(),
                        editable: true,
                        allDayDefault: false,
                        events: {
                            url: "<?php echo site_url('ajax/' . $action); ?>",
                        },
                        eventClick: function (event, jsEvent, view) {
//                            alert('testing....');
//   var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });

                        }
                    });
                });

        </script>
    </body>
</html>