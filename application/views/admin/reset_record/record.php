<!DOCTYPE html>



<html lang="en">



    <head>



        <title>



            <?php

            if ($this->uri->segment(0) != '') {



                echo 'Reset Records | Admin ' . $this->uri->segment(0);

            } else {



                echo 'Reset Records | Admin';

            }

            ?>



        </title>



        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">



        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.full.min.css" />



        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.css" />



        <?php echo $this->load->view($header); ?>



    </head>



    <body>



        <?php echo $this->load->view($topnav); ?>



        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <a class="menu-toggler" id="menu-toggler" href="#">



                    <span class="menu-text"></span>



                </a>



                <?php echo $this->load->view($leftnav); ?>	



                <div class="main-content">



                    <?php echo $this->load->view($breadcrumb); ?>







                    <div class="page-content">

                      <div class="col-sm-12 getmsg">    

            <div class="row" style="display:none;" id="reset_div_alert">

                <div class="col-md-12">

                    <div id="messg" class="alert alert-success alert-dismissible" role="alert">

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">

                            <span aria-hidden="true">×</span></button>

                       <p id ="reset_message"></p>
                    </div>

                </div>

            </div>

            </div>

                      

                        <div class="message"> </div>



                        <div class="row">



                            <div class="col-xs-12 col-sm-12">



                                <div class="row">



                                    <!--<div class="col-xs-12">-->

                                    <div class="widget-box">



                                        <div class="widget-header header-color-blue2">

                                            <h4 class="lighter smaller">Reset Records for year <?php echo date('Y')+1 ?></h4>

                                        </div>



                                        <div class="error"></div>


                                        <div class="widget-body">



                                            <div class="widget-main">



                                                <form  action="" method="post" id="reset-form" class="form-horizontal">
                                                       <div class="wysiwyg-editor editor1" id="login_content2" style="padding-left: 15px;overflow-y: hidden;">
                                                              
                                                            <font size="5"><b>
                                                          Kindly follow below Instructions before reset records</b>.</font>
                                                          <div><br></div><div>Step1:</div><div><ul>
                                                          <li><span style="line-height: 1.5;">Make a copy of current axn site in folder <?php echo date('Y'); ?> and attach the _<?php echo date('Y'); ?> with .com/<?php echo date('Y'); ?>.</span><br></li>                                                          
                                                         </ul>
                                                        </div>         
                                                        <div>Step2:</div><div><ul>
                                                          <li><span style="line-height: 1.5;">Make a new database for year (<?php echo date('Y')+1; ?>) and attach with current site (<a href="https://axnfactbook.com">https://axnfactbook.com</a>).</span><br></li>                                                          
                                                         </ul>
                                                        </div>  

                                                         <div>Remember:</div><div><ul>
                                                         <li><span style="line-height: 1.5;">Now currently working database is <b><?php echo $this->db->database;?>.</b></span><br></li>                                                         
                                                         </ul>
                                                        </div>     

                                                         <div id="id-message-item-navbar">                                   

                                                                <span class="inline btn-send-message">

                                                                    <button type="button" id="reset_button" class="btn btn-sm btn-primary no-border btn btn-xs btn-success" onClick="reset_form_data()">

                                                                        <span class="bigger-110">Reset</span>

                                                                        <i class="icon-arrow-right icon-on-right"></i>

                                                                    </button>

                                                                    
                                                                </span>    

                                                            </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        

                                                           </div>
                                                  

                                                </form>



                                            </div>

                                        </div>

                                    </div>



                                    <div class="vspace-sm"></div>



                                    <!-- /span -->



                                </div><!-- /row -->



                                <!-- PAGE CONTENT ENDS -->



                            </div><!-- /.col -->



                        </div><!-- /.row -->	



                    </div><!-- /.page-content -->



                </div><!-- /.main-content -->



            </div><!-- /.main-container-inner -->



            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">



                <i class="icon-double-angle-up icon-only bigger-110"></i>



            </a>



        </div><!-- /.main-container -->


        <?php echo $this->load->view($footer); ?>       	

        <script src="<?php echo base_url('assets'); ?>/js/bootstrap-wysiwyg.min.js"></script>

        <script src="<?php echo base_url('assets'); ?>/js/jquery.hotkeys.min.js"></script>

        <script src="<?php echo base_url('assets'); ?>/js/chosen.jquery.min.js"></script>

   <script>
  function reset_form_data(){
     if (confirm('Are you sure you want to delete records?')) {
          $.ajax({
           type: "POST",
            url: "<?php echo site_url('admin/reset_data'); ?>",
             data: [],
             success: function (response)
                 {
                $('#reset_div_alert').css('display','block');
               $('#reset_message').html("Records has been reset successfully.");
               $('#reset_button').prop('disabled','true');
                   }
                });
          }
    
        }

   </script>

    </body>

</html>