<!DOCTYPE html>
<html lang="en">
	<head>
		<?php echo $this->load->view($header); ?>
<title>
<?php 
	if($this->uri->segment(0)!='')
	{
	echo 'Welcome to ARN Fact Book | Admin '.$this->uri->segment(0);
	
	}
	else 
	{
	echo 'Welcome to ARN Fact Book | Admin';
	}
?>
</title>
	</head>
	<body>
		<?php echo $this->load->view($topnav); ?>
		<div class="main-container" id="main-container">
			
			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>
				<?php echo $this->load->view($leftnav); ?>	
				<div class="main-content">
					<?php echo $this->load->view($breadcrumb); ?>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
                            <div class="error"><?php echo $this->common->getmessage();?></div>
								<div class="row">								
									<div class="col-sm-12">
										<div class="tabbable">
											<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
												<?php $g=0; foreach($logsetting as $logset):  ?>
                                                <li class="<?php if($g==0) { echo "active"; } $g++;?>">
													<a data-toggle="tab" href="#log<?php echo $logset['error_log_setting_id']; ?>"><?php echo $logset['error_log_setting_title'];?></a>
												</li>
                                                <?php endforeach; ?>
											</ul>
											<div class="tab-content">
                                            	<?php $t=0; foreach($logsetting as $logset):?>
												<div id="log<?php echo $logset['error_log_setting_id']; ?>" class="tab-pane<?php if($t==0) { echo " active"; } $t++;?>">
													<form class="form-horizontal" role="form" action="<?php echo current_url();?>" method="post" onSubmit="return validate_form()">
                                                    <input type="hidden" name="settingid" value="<?php echo $logset['error_log_setting_id'];?>" />
                                                    <input type="hidden" name="settingupdate" value="1">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"><strong>Email Subject : </strong></label>                
                                                        <div class="col-sm-9">
                                                            <input type="text" id="emailsubject" name="emailsubject" placeholder="Error email subject" value="<?php echo $logset['error_receive_email_subject']; ?>" class="col-xs-10 col-sm-5">
                                                        </div>
                                                    </div>                                                    
													<div class="space-4"></div>
                                                     <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"><strong>Email Send To : </strong></label>                
                                                        <div class="col-sm-9">
                                                            <input type="text" id="sendto" name="sendto" placeholder=">Email Send To" value="<?php echo $logset['error_attention_to']; ?>" class="col-xs-10 col-sm-5">
                                                        </div>
                                                    </div> 
                                                     <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"><strong>Email Send To CC : </strong></label>                
                                                        <div class="col-sm-9">
                                                            <input type="text" id="sendtocc" name="sendtocc" placeholder=">Email Send To CC" value="<?php echo $logset['error_attention_to_cc']; ?>" class="col-xs-10 col-sm-5">
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"><strong>Email Send To BCC : </strong></label>                
                                                        <div class="col-sm-9">
                                                            <input type="text" id="sendtobcc" name="sendtobcc" placeholder=">Email Send To BCC" value="<?php echo $logset['error_attention_to_bcc']; ?>" class="col-xs-10 col-sm-5">
                                                        </div>
                                                    </div>                                                   
													<div class="space-4"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"><strong>Send Active : </strong></label>                
                                                        <div class="col-sm-9">
                                                            <input type="checkbox" name="isactive" <?php if($logset['error_log_setting_is_active']==1) { echo 'checked="checked"'; } ?>>
                                                        </div>
                                                    </div>                                                    
													<div class="space-4"></div>
                                                    <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button class="btn btn-info" type="submit">
                                                            <i class="icon-ok bigger-110"></i>
                                                            Update
                                                        </button>            
                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                            </div>									
	                                    	</form>
												</div>
                                                <?php endforeach; ?>                                                
											</div>
										</div>
									</div>									 
								</div><!-- /row -->
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->	
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
       <?php echo $this->load->view($footer);?>
		 <script src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
         <script type="text/javascript" src="https://www.google.com/jsapi"></script>
         <script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
    <script type="text/javascript">
	function validate_form()
	{
		var emailsubject = $("#emailsubject").val();
		var sendto = $("#sendto").val();
		var sendtocc = $("#sendtocc").val();
		var sendtobcc = $("#sendtobcc").val();
		var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
		if (emailsubject == "" ||  sendto == "" || sendtocc == "" || sendtobcc == "")
		{
			movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>You cannot send empty message or subject.</div></div></div>");
			 return false;
		}
		else if (!(/^([a-z0-9.,_\-\s])+$/i).test(emailsubject))  
		{
			movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>The Email subject field may only contain alpha-numeric characters.</div></div></div>");
			 return false;
		}
		else if (pattern.test(sendto) == false || pattern.test(sendtocc) == false || pattern.test(sendtobcc) == false)
		{
			movetoerror("<div class='row'><div class='col-sm-12'><div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>Email Address is invalid.</div></div></div>");
			 return false;
		}
		
	return true;
	}
	function movetoerror(msg)
	{
		$(".error").html(msg); 
		$('html, body').animate({ scrollTop: $('.error').offset().top }, 'slow');
	}
    	
    </script>		
	</body>
</html>
