
<!DOCTYPE html>
<html lang="en">
    <head>
        <!--         <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
        <meta http-equiv="pragma" content="no-cache" />-->
        <?php
//        header("Location: /admin/create_user/index.php?r=".mt_rand(0, 9999999));
//        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
//        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
//        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
//        header("Cache-Control: post-check=0, pre-check=0", false);
//        header("Pragma: no-cache");
//        header("Content-Type: application/json");
        header("Expires: on, 01 Jan 1970 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        ?>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Create Users | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'Create Users | ARN Fact Book';
            }
            ?>
        </title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/choseen.css" />
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/font/glyphicons-halflings-regular.ttf" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/font/glyphicons-halflings-regular.woff" />-->

        <?php echo $this->load->view($header); ?>
    </head>
    <body>
        <?php
        echo $this->load->view($topnav);
        clearstatcache();
        ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <?php ?>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="javascript:void(0)">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>                    
                    <div class="page-content">                   
                        <div class="row">                        
                            <div class="col-xs-12">
                                <div class="error"><?php echo $this->common->getmessage(); ?></div>
                                <!--<form method="post" name="form1" id="form1" class="form-horizontal" onSubmit="return checkval();" action="<?php // echo site_url('admin/create_user');      ?>" autocomplete="off">                     <table width="100%" border="0" cellpadding="0" cellspacing="0">-->                      	
                                <form method="post" name="form1" id="form1" class="form-horizontal" data-toggle="validator" role="form" action="<?php echo site_url('admin/create_user'); ?>" autocomplete="off">                     <table width="100%" border="0" cellpadding="0" cellspacing="0">                      	
                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="first_name" class="col-sm-4 control-label no-padding-right"> 
                                                        First Name:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="col-xs-10 col-sm-10" name="first_name" type="text" id="first_name" size="50" maxlength="50" data-error="First Name is invalid" required>
                                                    <!--<span class="glyphicon form-control-feedback" aria-hidden="true"></span>-->
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="last_name" class="col-sm-4 control-label no-padding-right"> 
                                                        Last Name:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="col-xs-10 col-sm-10" name="last_name" type="text" id="last_name" size="50" maxlength="50"  data-error="Last Name is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Company:&nbsp;
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="company" type="text" size="40" maxlength="100">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="address1" class="col-sm-4 control-label no-padding-right"> 
                                                        Address: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="address1" type="text" id="address1" size="60" maxlength="150" >
                                                    </div>
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Address2:&nbsp; 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="address2" type="text" size="60" maxlength="150">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        City:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="city" type="text" id="city" size="40" maxlength="50" >
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group control-group ">
                                                    <label for="state" class="col-sm-4 control-label no-padding-right"> 
                                                        State: 
                                                    </label>
                                                    <div class="col-sm-8">                            	
                                                        <select id="state" name="state" class="chosen-select col-sm-10" >
                                                        <!--<select id="state" name="state" class="col-sm-10" data-error="State is invalid" required>-->
                                                            <option value=""> </option>
                                                            <option value=""></option>
                                                            <?php
                                                            foreach ($rsStates as $key => $state):
                                                                ?>

                                                                <option value="<?php echo $state['states_id']; ?>">
                                                                    <?php echo $state['state_name']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Country:&nbsp;
                                                    </label>
                                                    <div class="col-sm-8">                            	
                                                        <select name="country" id="country" class="chosen-select col-sm-10">
                                                            <?php
                                                            foreach ($row_rsCountries as $country):
                                                                ?>
                                                                <option value="<?php echo $country['countries_id']; ?>"<?php
                                                                if (!(strcmp($country['countries_id'], '223'))) {
                                                                    echo "selected=\"selected\"";
                                                                }
                                                                ?>>
                                                                            <?php
                                                                            echo $country['countries_name'];
                                                                            ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group ">
                                                    <label for="zip" class="col-sm-4 control-label no-padding-right"> 
                                                        Zip: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="zip" type="text" id="zip" size="20" maxlength="20" >
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Work Phone: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="work_phone" type="text" id="work_phone" size="20" maxlength="20" >
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Work Phone Ext:&nbsp;
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="work_phone_ext" type="text" size="10" maxlength="10">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Home Phone : 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="home_phone" type="text" size="20" maxlength="20">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Cell Phone: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="cell_phone" type="text" size="20" maxlength="20">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Fax: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="col-xs-10 col-sm-10" name="fax" size="20" maxlength="20">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="email" class="col-sm-4 control-label no-padding-right"> 
                                                        Email: <span class="red">*</span>
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <!--<input class="col-xs-10 col-sm-10" name="email" type="text" id="email" size="50" maxlength="100" data-error="Bruh, that email address is invalid" required>-->
                                                        <input class="col-xs-10 col-sm-10" name="email" type="email" id="email" size="50" maxlength="100" data-error="The Email address is invalid" required>
                                                        <!--<span class="fa form-control-feedback" aria-hidden="true"></span>-->
                                                    </div>
                                                    <!--<span class="glyphicon form-control-feedback" aria-hidden="true"></span>-->
                                                    <!--<div class="help-block with-errors">Hey look, this one has feedback icons!</div>-->
                                                    <div class="help-block with-errors col-sm-8"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Published Email: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="published_email" type="text" id="published_email" placeholder="" size="50" maxlength="100" autocomplete="off">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Password:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="password" type="password" id="password" value="" size="15" maxlength="20" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" style="background: #fff !important;" />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Name On Badge:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="name_on_badge" type="text" id="name_on_badge"  size="20" maxlength="20">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Golf Handicap:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="golf_handicap" type="text" id="golf_handicap" size="20" maxlength="20">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Job Title:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="job_title" type="text"  size="35" maxlength="100">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Job Category:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <select name="job_category" id="job_category" class="chosen-select col-sm-10">

                                                            <option value="none">-- Please Select --</option>

                                                            <option value="Airport Director">Airport Director</option>

                                                            <option value="Airport Staff">Airport Staff</option>

                                                            <option value="Concessionaire">Concessionaire</option>

                                                            <option value="Other">Other</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Emergency Name: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="emergency_name" type="text" size="50" maxlength="100">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Emergency Phone:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="emergency_phone" type="text" size="50" maxlength="100">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Emergency Cell:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="emergency_cell" type="text" size="50" maxlength="100">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Emergency Email:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="emergency_email" type="text" size="50" maxlength="100">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Year:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="year" type="text" size="50" maxlength="50">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Access Level:
                                                    </label>
                                                    <div class="col-sm-8">                            	
                                                        <select id="access_level" name="access_level" class="chosen-select col-sm-10">
                                                            <?php
                                                            foreach ($AccessLevel as $key => $alevel):
                                                                ?>

                                                                <option value="<?php echo $alevel['levelid']; ?>">
                                                                    <?php echo $alevel['level']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Active: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="checkbox" name="is_active" value="1" checked>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">                          
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Admin&nbsp;:
                                                    </label>                       
                                                    <div class="col-sm-8">
                                                        <input type="checkbox" class="ace ace-switch ace-switch-7" name="is_admin" >
                                                        <span class="lbl"></span>
                                                        <span class="red">(Checking this gives the user access to the admin area.)</span>									
                                                    </div>
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Active: 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <?php echo $this->common->GetCreatedTime(); ?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <input type="hidden" name="MM_update" value="form1">
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        &nbsp; 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <!--<button type="submit" onClick="MM_validateForm('first_name', '', 'R', 'last_name', '', 'R', 'address1', '', 'R', 'city', '', 'R', 'state', '', 'R', 'zip', '', 'R', 'work_phone', '', 'R', 'email', '', 'R','RisEmail');-->
                                                        <!--<button type="submit" onClick="MM_validateForm('first_name', '', 'R', 'last_name', '', 'R', 'address1', '', 'R', 'city', '', 'R', 'state', '', 'R', 'zip', '', 'R', 'work_phone', '', 'R', 'email', '', 'R'); return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php echo site_url('admin/list_of_users'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>-->
                                                        <button type="submit" name="add_user" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button> &nbsp;&nbsp;<a href="<?php echo site_url('admin/list_of_users'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <!--<div class="form-group">                          
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Admin&nbsp;:
                                          </label>                       
                                              <div class="col-sm-8">
                                                      <input type="checkbox" class="ace ace-switch ace-switch-7" name="is_admin" >
                                              <span class="lbl"></span>
                                              <span class="red">(Checking this gives the user access to the admin area.)</span>									
                                              </div>
                                      </div>
                                       <div class="space-4"></div>                 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              First Name:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input type="text" class="col-xs-10 col-sm-5" name="first_name" type="text" id="last_name" size="50" maxlength="50" >
                                          </div>
                                      </div>                        
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Last Name:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="last_name" type="text" id="last_name" size="50" maxlength="50" >
                                          </div>
                                      </div>
                                      <div class="space-4"></div>    
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Company:&nbsp;
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="company" type="text" size="40" maxlength="100">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Address:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="address1" type="text" id="address1" size="60" maxlength="150">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Address2:&nbsp; 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="address2" type="text" size="60" maxlength="150">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              City:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="city" type="text" id="city" size="40" maxlength="50">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>  
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                               State:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">                            	
                                              <select name="state">
                                    <?php
//                                    foreach ($rsStates as $key => $state):
                                    ?>
                                                                                  
                                                                                      <option value="<?php // echo $state['states_id'];                ?>">
                                    <?php // echo $state['state_name'];    ?>
                                                                                      </option>
                                    <?php
//                                    endforeach;
                                    ?>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                               Country:&nbsp;
                                          </label>
                                          <div class="col-sm-8">                            	
                                              <select name="country">
                                    <?php
//                                    foreach ($row_rsCountries as $country):
                                    ?>
                                                                                  
                                                                                      <option value="<?php // echo $country['countries_id'];                ?>">
                                    <?php // echo $country['countries_name'];    ?>
                                                                                      </option>
                                    <?php
//                                    endforeach;
                                    ?>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                       <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Zip:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="zip" type="text" id="zip" size="20" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Work Phone:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="work_phone" type="text" id="work_phone" size="20" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>  
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Work Phone Ext:&nbsp;
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="work_phone_ext" type="text" size="10" maxlength="10">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Home Phone : 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="home_phone" type="text" size="20" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>  
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Cell Phone: 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="home_phone" type="text" size="20" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>  
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Fax: 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="fax" size="20" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                       <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Email :<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="email" type="text" id="email" size="50" maxlength="100">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Published Email: 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="published_email" type="text" id="published_email"  size="50" maxlength="100">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Password:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5"  name="password" type="password" size="15" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>   
                                       <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Name On Badge:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5"  name="name_on_badge" type="text" id="name_on_badge"  size="20" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Golf Handicap:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5" name="golf_handicap" type="text" id="golf_handicap" size="20" maxlength="20">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Job Title:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5" name="job_title" type="text"  size="35" maxlength="100">
                                          </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Job Category:
                                          </label>
                                          <div class="col-sm-8">
                                           <select name="job_category" id="job_category">
              
                                                                <option value="none">-- Please Select --</option>
              
                                                                <option value="Airport Director">Airport Director</option>
              
                                                                <option value="Airport Staff">Airport Staff</option>
              
                                                                <option value="Concessionaire">Concessionaire</option>
              
                                                                <option value="Other">Other</option>
              
                                                                                                </select>
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Emergency Name: 
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5"  name="emergency_name" type="text" size="50" maxlength="100">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Emergency Phone:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5"  name="emergency_phone" type="text" size="50" maxlength="100">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                       <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Emergency Cell:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5"  name="emergency_cell" type="text" size="50" maxlength="100">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Emergency Email:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5"  name="emergency_email" type="text" size="50" maxlength="100">
                                          </div>
                                      </div>                        
                                      <div class="space-4"></div>  
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Active: 
                                          </label>
                                          <div class="col-sm-8">
                                          <input type="checkbox" name="is_active" value="1" checked>
                                          </div>
                                      </div>
                                      <div class="space-4"></div>  
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Active: 
                                          </label>
                                          <div class="col-sm-8">
                                    <?php echo $this->common->GetCreatedTime(); ?>
                                          </div>
                                      </div>
                                      <div class="space-4"></div>  
                                      <input type="hidden" name="MM_update" value="form1">
                                     <div class="form-group">
                                              <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                  &nbsp; 
                                              </label>
                                              <div class="col-sm-8">
                                              <button type="submit" onClick="MM_validateForm('first_name','','R','last_name','','R','address1','','R','city','','R','zip','','R','work_phone','','R','email','','RisEmail');return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php echo site_url('admin/list_of_users'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                              </div>
                                      </div>
                                      <div class="space-4"></div> -->
                                </form> 


                            </div>			
                        </div>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="javascript:void(0)" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
        <?php echo $this->load->view($footer); ?>  
        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>

        <script type="text/javascript">

                                                            $(".chosen-select").chosen({width: "83.4%",
                                                                search_contains: true});


                                                            $("#state").prepend("<option value='' selected='selected'>&nbsp;</option>").trigger('chosen:updated');

                                                            $(document).ready(function () {
                                                                $('input').attr('autofill', 'false');
//                                                                $(".chosen-select").chosen({width: "83.4%"});


                                                                $('#form1').validator();
                                                                $('#form1').validator({
                                                                    rules: {
                                                                        state: {
                                                                            required: true
                                                                        }
                                                                    },
                                                                    highlight: function (element) {
                                                                        $(element).closest('.control-group').removeClass('success').addClass('error').trigger('chosen:updated');
//                                                                        $('#categoryide').trigger('chosen:updated');
                                                                    },
                                                                    success: function (element) {
                                                                        element.text('OK!').addClass('valid')
                                                                                .closest('.control-group').removeClass('error').addClass('success').trigger('chosen:updated');
                                                                    }
                                                                });
                                                            });






//                                    $(document).ready(function () {
//                                        $.ajaxSetup({cache: false});
//                                    });

//$(document).ready(function(){
//    $(':input').live('focus',function(){
//        $(this).attr('autocomplete', 'off');
//    });
//});


//                                    $("#password").attr({cache: false});


//var idx = 0;
//$("input:password")
//    
//    .eq(idx++).autocomplete('');


//$(document).ready(function(){
//    $(':input').live('focus',function(){
//        $(this).attr('autocomplete', 'off');
//    });
//});

//                                    $("#password").attr("autofill", "off");

//$("#form1").setAttribute( "autocomplete", "off" ); 

//if (document.getElementsByTagName) {
//
//var inputElements = document.getElementsByTagName("input");
//
//for (i=0; inputElements[i]; i++) {
//
////if (inputElements[i].className && (inputElements[i].className.indexOf("disableAutoComplete") != -1)) {
//
//inputElements[i].setAttribute("autocomplete","off");
////alert('test');
////}
//
//}
//
//}

                                                            function checkval()

                                                            {

                                                                var rfield = new Array("first_name", "last_name", "email");

                                                                var msg = new Array('First Name', 'Last Name', 'Email');

                                                                var errmsg = "";

                                                                for (i = 0; i < rfield.length; i++)

                                                                {

                                                                    //alert(rfield[i]);

                                                                    var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                                                                    if (val == "" || val.replace(" ", "") == "")

                                                                    {

//                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";
                                                                        errmsg += "" + msg[i] + " is Required. <br/>";


                                                                    }

                                                                }

                                                                if (errmsg != "")

                                                                {

                                                                    $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                                                                    $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                                                                    return false;

                                                                }

                                                                return true;

                                                            }



//                                    function titleCase(nm) {
//                                        var titleStr = nm.split(' ');
//                                        for (var j = 0; j < titleStr.length; j++) {
//                                            titleStr[j] = titleStr[j].charAt(0).toUpperCase() + titleStr[j].slice(1).toLowerCase();
//                                        }
//                                        return titleStr.join(' ');
//                                    }

//                                                            $(".chosen-select").chosen({width: "83.4%"});
                                                            /*function MM_validateForm() {
                                                             
                                                             
                                                             if (document.getElementById) {
                                                             
                                                             var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
                                                             //console.log(args);
                                                             //alert(args);
                                                             for (i = 0; i < (args.length - 2); i += 3) {
                                                             test = args[i + 2];
                                                             val = document.getElementById(args[i]);
                                                             
                                                             if (val) {
                                                             nm = val.name;
                                                             if ((val = val.value) != "") {
                                                             
                                                             if (test.indexOf('isEmail') != -1) {
                                                             p = val.indexOf('@');
                                                             
                                                             if (p < 1 || p == (val.length - 1))
                                                             errors += '- ' + nm + ' must contain an e-mail address.\n';
                                                             
                                                             } else if (test != 'R') {
                                                             num = parseFloat(val);
                                                             
                                                             if (isNaN(val))
                                                             errors += '- ' + nm + ' must contain a number.\n';
                                                             
                                                             if (test.indexOf('inRange') != -1) {
                                                             p = test.indexOf(':');
                                                             
                                                             min = test.substring(8, p);
                                                             max = test.substring(p + 1);
                                                             
                                                             if (num < min || max < num)
                                                             //                                                                     errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                                                             errors += nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                                                             }
                                                             }
                                                             } else if (test.charAt(0) == 'R')
                                                             //                                                                            {
                                                             //                                                         errors += '- ' + nm + ' is required.<br />';
                                                             //console.log(nm);
                                                             var test1 = titleCase(nm.replace("_", " "));
                                                             errors += test1 + ' is required.<br />';
                                                             //                                                                            }
                                                             }
                                                             
                                                             }
                                                             if (errors) {
                                                             //                                                 $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors.replace("_", " ").toUpperCase() + "</div></div></div>");
                                                             //                                                                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors.replace("_", " ").charAt(0).toUpperCase() + errors.substr(1).toLowerCase() + "</div></div></div>");
                                                             $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors + "</div></div></div>");
                                                             $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');
                                                             }
                                                             
                                                             document.MM_returnValue = (errors == '');
                                                             
                                                             }
                                                             } */
        </script>     
    </body>
</html>
