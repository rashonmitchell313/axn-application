<!DOCTYPE html>
<html lang="en">
   <head>
      <title>
			<?php 
            if($this->uri->segment(0)!='')
            {
            	echo 'Modify Tickets | ARN Fact Book '.$this->uri->segment(0);
            }
            else 
            {
            	echo 'Modify Tickets | ARN Fact Book';
            }
            ?>
      </title>
      <?php echo $this->load->view($header); ?>
   </head>
   <body>
      <?php echo $this->load->view($topnav); ?>
      <div class="main-container" id="main-container">
         <div class="main-container-inner">
            <a class="menu-toggler" id="menu-toggler" href="javascript:void(0)">
            <span class="menu-text"></span>
            </a>
            <?php echo $this->load->view($leftnav); ?>	
            <div class="main-content">
               <?php echo $this->load->view($breadcrumb); ?>                    
               <div class="page-content">
                  <div class="row">
                     <div class="col-xs-12">
                     <div class="msg alert alert-danger" style="display:none;"><a href="javascript:void(0)" class="close" data-dismiss="alert">&times;</a>Please enter a message first...</div>  
                        <div class="error"><?php echo $this->common->getmessage();?></div>
                        <div class="widget-box">
                           <div class="widget-header">
                              <h4 class="lighter smaller">
                                 <i class="icon-comment blue"></i>
                                 Conversation &nbsp; <b><?php echo $receivemessage[0]['subject'];?></b>
                              </h4>
                           </div>
                           <div class="widget-body">
                              <div class="widget-main no-padding">
                                 <div class="dialogs">
                                    <?php foreach($receivemessage as $r): ?>
                                    <div class="itemdiv dialogdiv">
                                       <div class="user">
                                          <img alt="Alexa's Avatar" src="<?php echo base_url(); ?>assets/avatars/avatar1.png" />
                                       </div>
                                       <div class="body">
                                          <div class="time">
                                             <i class="icon-time"></i>
                                             <span class="green"><?php echo $this->commonmodel->get_formatted_datetime($r['fr_ticket_created_on']); ?></span>
                                          </div>
                                          <div class="name">
                                             <a href="javascript:void(0)"><?php echo ($r['sender_bit'] != 1)?'Support':$r['first_name'].' '.$r['last_name']; ?></a>
                                          </div>
                                          <div class="text"><?php echo $r['fr_ticket_conversion_message']; ?>
                                          </div>
                                       </div>
                                       <br>
                                    </div>
                                    <?php endforeach; ?>
                                 </div>
                                 <form method="post" action="<?php echo site_url('admin/view_single_ticket/'.$r['support_id']);?>">
                                    <div class="form-actions">
                                       <div class="input-group">
                                          <input id="message" placeholder="Type your message here ..." type="text" class="form-control" name="message"  />
                                          <span class="input-group-btn">
                                          <button id="send-button" value="1" name="submitted" class="btn btn-sm btn-info no-radius" type="submit">
                                          <i class="icon-share-alt"></i>
                                          Send
                                          </button>
                                          </span>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <!-- /widget-main -->
                           </div>
                           <!-- /widget-body -->
                        </div>
                        <!-- /widget-box -->
                     </div>
                  </div>
               </div>
               <!-- /.page-content -->
            </div>
            <!-- /.main-content -->
         </div>
         <!-- /.main-container-inner -->
         <a href="javascript:void(0)" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
         <i class="icon-double-angle-up icon-only bigger-110"></i>
         </a>
      </div>
      <!-- /.main-container -->
      <?php echo $this->load->view($footer);?> 
      <script src="<?php echo base_url();?>assets/js/bootstrap-wysiwyg.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/jquery.hotkeys.min.js"></script>
      <script src="<?php echo base_url('assets');?>/js/jquery.slimscroll.min.js"></script>
      <script type="text/javascript">
         try{ace.settings.check('main-container' , 'fixed')}catch(e){}
      </script>
      <script type="text/javascript">
			$('.dialogs,.comments').slimScroll({
				height: '260px'
			});
         	   
      </script>   
      <script type="text/javascript">
$("#send-button").on('click', function(){
	
	
var message = $("#message").val();

if(message == "")
{
			$('.msg').show();
			return false;
}
});
</script>
   </body>
</html>