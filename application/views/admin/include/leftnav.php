<div class="sidebar" id="sidebar">

    <script type="text/javascript">

        try {

            ace.settings.check('sidebar', 'fixed')

        } catch (e) {

        }

    </script>



    <div class="sidebar-shortcuts" id="sidebar-shortcuts">

        <!--<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">

                <button class="btn btn-success">

                        <i class="icon-signal"></i>

                </button>

                <button class="btn btn-info">

                        <i class="icon-pencil"></i>

                </button>

                <button class="btn btn-warning">

                        <i class="icon-group"></i>

                </button>

                <button class="btn btn-danger">

                        <i class="icon-cogs"></i>

                </button>

        </div>-->

        <!--<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">

                <span class="btn btn-success"></span>

                <span class="btn btn-info"></span>

                <span class="btn btn-warning"></span>

                <span class="btn btn-danger"></span>

        </div>-->

    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">

        <?php $dashboard = array(''); ?>	

        <li <?php $this->common->leftnavactive($dashboard); ?>>

            <a href="<?php echo site_url('admin'); ?>">

                <i class="icon-dashboard"></i>

                <span class="menu-text"> Dashboard </span>

            </a>

        </li>

        <?php $userarray = array('list_of_users', 'create_user', 'edit_user', 'logged_in_report', 'list_of_del_users', 'delete_logged_report', 'view_user_activity_log', 'list_of_user_activities'); ?>					

        <li <?php $this->common->leftnavactive($userarray); ?>>

            <a href="#" class="dropdown-toggle">

                <i class="icon-user"></i>

                <span class="menu-text">Users</span>

                <b class="arrow icon-angle-down"></b>

            </a>                            

            <ul class="submenu">

                <li <?php $this->common->LeftSubNavActive('list_of_users', 2); ?>>

                    <a href="<?php echo site_url('admin/list_of_users'); ?>" title="List All Users">

                        <i class="icon-double-angle-right"></i>

                        List All Users

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('list_of_del_users', 2); ?>>

                    <a href="<?php echo site_url('admin/list_of_del_users'); ?>" title="List Deleted Users">

                        <i class="icon-double-angle-right"></i>

                        List Deleted Users

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('create_user', 2); ?> >

                    <a href="<?php echo site_url('admin/create_user'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Create User

                    </a>

                </li>

                <?php /* ?><li>

                  <a href="<?php echo site_url('admin/list_of_users');?>">

                  <i class="icon-double-angle-right"></i>

                  Delete User

                  </a>

                  </li><?php */ ?>

                <li <?php $this->common->LeftSubNavActive('logged_in_report', 2); ?> >

                    <a href="<?php echo site_url('admin/logged_in_report'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Logged In Report

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('delete_logged_report', 2); ?> >

                    <a href="<?php echo site_url('admin/delete_logged_report'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Deleted Logged In Report

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('view_user_activity_log', 2); ?> >

                    <a href="<?php echo site_url('admin/view_user_activity_log'); ?>">

                        <i class="icon-double-angle-right"></i>

                        View User's Activity Log

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('list_of_user_activities', 2); ?> >

                    <a href="<?php echo site_url('admin/list_of_user_activities'); ?>">

                        <i class="icon-double-angle-right"></i>

                        List of all User's Activities

                    </a>

                </li>



            </ul>

        </li>

        <?php $airportarray = array('airport', 'list_of_airport_not_published', 'add_main_airport', 'add_airport', 'view_aiorport_activity_log'); ?>					

        <li <?php $this->common->leftnavactive($airportarray); ?>>

            <a href="" class="dropdown-toggle">

                <i class="icon-plane"></i>

                <span class="menu-text">Airports</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <li <?php if ($this->uri->segment(2) == "airport" && $this->uri->segment(3) == "") echo 'class="active"'; //$this->common->LeftSubNavActive('airport',2);   ?> >

                    <a href="<?php echo site_url('admin/airport'); ?>">

                        <i class="icon-double-angle-right"></i>

                        List All Airports

                    </a>

                </li>

                <li <?php if ($this->uri->segment(3) == "list_of_airport_not_published") echo 'class="active"'; //$this->common->LeftSubNavActive('list_of_airport_not_published',3);    ?>>

                    <a href="<?php echo site_url('admin/airport/list_of_airport_not_published'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Not Published

                    </a>

                </li>

                <li <?php if ($this->uri->segment(3) == "list_of_del_airport") echo 'class="active"'; //$this->common->LeftSubNavActive('list_of_airport_not_published',3);    ?>>

                    <a href="<?php echo site_url('admin/airport/list_of_del_airport'); ?>">

                        <i class="icon-double-angle-right"></i>

                        List Deleted Airports

                    </a>

                </li>

                <li>

                    <a href="<?php echo site_url('wizard/airport'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Airport Wizard

                    </a>

                </li>

                <li <?php if ($this->uri->segment(3) == "add_main_airport") echo 'class="active"'; //$this->common->LeftSubNavActive('add_main_airport',2);   ?> >

                    <a href="<?php echo site_url('admin/airport/add_main_airport'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Add Main Airports

                    </a>

                </li>

                <li <?php if ($this->uri->segment(3) == "add_airport") echo 'class="active"'; ?>>

                    <a href="<?php echo site_url('admin/airport/add_airport', 2); ?>">

                        <i class="icon-double-angle-right"></i>

                        Add an Airport

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('view_aiorport_activity_log', 2); ?>>

                    <a href="<?php echo site_url('admin/view_aiorport_activity_log', 2); ?>">

                        <i class="icon-double-angle-right"></i>

                        View Airports Activity Log

                    </a>

                </li>

                <?php /* ?><li>

                  <a href="<?php echo site_url('admin/airport');?>">

                  <i class="icon-double-angle-right"></i>

                  Delete an Airport

                  </a>

                  </li><?php */ ?>								

            </ul>

        </li>

        <?php $airportarray = array('company', 'view_company_activity_log'); ?>					

        <li  <?php $this->common->leftnavactive($airportarray); ?>>

            <a href="" class="dropdown-toggle">

                <i class="icon-asterisk"></i>

                <span class="menu-text">Companies</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <li  <?php if ($this->uri->segment(2) == "company" && $this->uri->segment(3) == "") echo 'class="active"'; //$this->common->LeftSubNavActive('company',2);   ?> >

                    <a href="<?php echo site_url('admin/company'); ?>">

                        <i class="icon-double-angle-right"></i>

                        List All Companies

                    </a>

                </li>

                <li  <?php $this->common->LeftSubNavActive('company_not_published', 3); ?>>

                    <a href="<?php echo site_url('admin/company/company_not_published'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Not Published

                    </a>

                </li>

                <li  <?php $this->common->LeftSubNavActive('company_deleted', 3); ?>>

                    <a href="<?php echo site_url('admin/company/company_deleted'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Deleted Companies

                    </a>

                </li>

                <li>

                    <a href="<?php echo site_url('wizard/company'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Company Wizard

                    </a>

                </li>

                <li  <?php $this->common->LeftSubNavActive('add_company', 3); ?>>

                    <a href="<?php echo site_url('admin/company/add_company'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Add Company

                    </a>

                </li>

                <li  <?php $this->common->LeftSubNavActive('view_company_activity_log', 2); ?>>

                    <a href="<?php echo site_url('admin/view_company_activity_log'); ?>">

                        <i class="icon-double-angle-right"></i>

                        View Companies Activity Log

                    </a>

                </li>

                <?php /* ?><li >

                  <a href="<?php echo site_url('admin/company');?>">

                  <i class="icon-double-angle-right"></i>

                  Delete Company

                  </a>

                  </li><?php */ ?> 

                <li <?php $this->common->LeftSubNavActive('companies_by_category', 3); ?>>

                    <a href="<?php echo site_url('admin/company/companies_by_category'); ?>" title="Companies By Category">

                        <i class="icon-double-angle-right"></i>

                        Companies by Category

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('add_airport_lookup', 3); ?>>

                    <a href="<?php echo site_url('admin/company/add_airport_lookup'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Add Airport Lookup Lists

                    </a>

                </li>		

                <li <?php $this->common->LeftSubNavActive('lookup_list_for_locations', 3); ?>>

                    <a href="<?php echo site_url('admin/company/lookup_list_for_locations'); ?>">

                        <i class="icon-double-angle-right"></i>

                        Lookup List for Locations

                    </a>

                </li>								

            </ul>

        </li>

        <?php $addsarray = array('adds'); ?>	

        <li <?php $this->common->leftnavactive($addsarray); ?>>

            <a href="#" class="dropdown-toggle">

                <i class="icon-desktop"></i>

                <span class="menu-text">Ads</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <li <?php $this->common->LeftSubNavActive('adds', 2); ?>>

                    <a href="<?php echo site_url('admin/adds'); ?>">

                        <i class="icon-double-angle-right"></i>

                        List All Ads

                    </a>

                </li>

                <?php /* ?>   <li>

                  <a href="<?php echo site_url('admin/adds'); ?>">

                  <i class="icon-double-angle-right"></i>

                  Create/View Ads

                  </a>

                  </li>

                  <li>

                  <a href="#">

                  <i class="icon-double-angle-right"></i>

                  View Assignments

                  </a>

                  </li>		<?php */ ?>						

            </ul>

        </li>

        <?php $editsarray = array('edit_content', 'multi_language', 'edit_content_text'); ?>	

        <li <?php $this->common->leftnavactive($editsarray); ?>>

            <a href="#" class="dropdown-toggle">

                <i class="icon-edit"></i>

                <span class="menu-text">Edit Content</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <li <?php $this->common->LeftSubNavActive('edit_content', 2); ?>>

                    <a href="<?php echo site_url('admin/edit_content'); ?>">

                        <i class="icon-edit"></i>

                        <span class="menu-text"> Edit Content</span>

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('multi_language', 2); ?>>

                    <a href="<?php echo site_url('admin/multi_language'); ?>">

                        <i class="icon-edit"></i>

                        <span class="menu-text"> Multi Language</span>

                    </a>

                </li>

                <?php /* ?><li <?php $this->common->LeftSubNavActive('edit_content_text',2); ?>>

                  <a href="<?php echo site_url('admin/edit_content_text'); ?>">

                  <i class="icon-edit"></i>

                  <span class="menu-text"> Edit Content Text</span>

                  </a>

                  </li><?php */ ?>

            </ul>

        </li>

        <?php /* ?><li>

          <a href="#" class="dropdown-toggle">

          <i class="icon-certificate"></i>

          <span class="menu-text">Affiliations</span>

          <b class="arrow icon-angle-down"></b>

          </a>

          <ul class="submenu">

          <li>

          <a href="#">

          <i class="icon-edit"></i>

          <span class="menu-text"> View/Change Affiliations </span>

          </a>

          </li>

          </ul>

          </li><?php */ ?>

        <?php $editsarray = array('view_tickets', 'view_del_tickets'); ?>

        <li <?php $this->common->leftnavactive($editsarray); ?>>

            <a href="#" class="dropdown-toggle">

                <i class="icon-flag"></i>

                <span class="menu-text">Support</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <?php /* ?>   <li>

                  <a href="#">

                  <i class="icon-double-angle-right"></i>

                  <span class="menu-text">Submit a Change Request</span>

                  </a>

                  </li>

                  <li>

                  <a href="#">

                  <i class="icon-double-angle-right"></i>

                  <span class="menu-text">Call Support</span>

                  </a>

                  </li><?php */ ?>

                <li  <?php $this->common->LeftSubNavActive('view_tickets', 2); ?>>

                    <a href="<?php echo site_url('admin/view_tickets'); ?>">

                        <i class="icon-double-angle-right"></i>						

                        <span class="menu-text">Support Question</span>

                    </a>

                </li>

                <li  <?php $this->common->LeftSubNavActive('view_del_tickets', 2); ?>>

                    <a href="<?php echo site_url('admin/view_del_tickets'); ?>">

                        <i class="icon-double-angle-right"></i>						

                        <span class="menu-text">Deleted Support Question</span>

                    </a>

                </li>

            </ul>

        </li>

        <?php $editsarray = array('errors_log', 'del_errors_log', 'error_log_setting', 'view_error_activity_log'); ?>	

        <li <?php $this->common->leftnavactive($editsarray); ?>>

            <a href="#" class="dropdown-toggle">

                <i class="icon-eye-open"></i>

                <span class="menu-text">Error Log</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <li <?php $this->common->LeftSubNavActive('errors_log', 2); ?>>

                    <a href="<?php echo site_url('admin/errors_log'); ?>">

                        <i class="icon-double-angle-right"></i>									

                        <span class="menu-text">View All Error Log</span>

                    </a>

                </li>    

                <li <?php $this->common->LeftSubNavActive('del_errors_log', 2); ?>>

                    <a href="<?php echo site_url('admin/del_errors_log'); ?>">

                        <i class="icon-double-angle-right"></i>									

                        <span class="menu-text">View Deleted Error Log</span>

                    </a>

                </li>    

                <li <?php $this->common->LeftSubNavActive('error_log_setting', 2); ?>>

                    <a href="<?php echo site_url('admin/error_log_setting'); ?>">	

                        <i class="icon-double-angle-right"></i>								

                        <span class="menu-text">Error Log Setting</span>

                    </a>

                </li>                                

                <li <?php $this->common->LeftSubNavActive('view_error_activity_log', 2); ?>>

                    <a href="<?php echo site_url('admin/view_error_activity_log'); ?>">	

                        <i class="icon-double-angle-right"></i>								

                        <span class="menu-text">View Errors Activity Log</span>

                    </a>

                </li>                                

            </ul>

        </li>   

        <?php $editsarray = array('create-faq', 'view-faq', 'view_faq_feedback', 'view_deleted_faq_feedback', 'view_activity_log'); ?>    

        <li <?php $this->common->leftnavactive($editsarray); ?>>

            <a href="#" class="dropdown-toggle">

                <i class="icon-question"></i>

                <span class="menu-text">FAQs</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <li <?php $this->common->LeftSubNavActive('create-faq', 2); ?>>

                    <a href="<?php echo site_url('admin/create-faq'); ?>">

                        <i class="icon-double-angle-right"></i>

                        <span class="menu-text">  Create FAQ's </span>

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('view-faq', 2); ?>>

                    <a href="<?php echo site_url('admin/view-faq'); ?>">

                        <i class="icon-double-angle-right"></i>

                        <span class="menu-text"> View FAQ's </span>

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('view_faq_feedback', 2); ?>>

                    <a href="<?php echo site_url('admin/view_faq_feedback'); ?>">

                        <i class="icon-double-angle-right"></i>

                        <span class="menu-text"> View FAQ's Feedback </span>

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('view_deleted_faq_feedback', 2); ?>>

                    <a href="<?php echo site_url('admin/view_deleted_faq_feedback'); ?>">

                        <i class="icon-double-angle-right"></i>

                        <span class="menu-text"> View Deleted FAQ's Feedback </span>

                    </a>

                </li>

                <li <?php $this->common->LeftSubNavActive('view_activity_log', 2); ?>>

                    <a href="<?php echo site_url('admin/view_activity_log'); ?>">

                        <i class="icon-double-angle-right"></i>

                        <span class="menu-text"> View Activity Log </span>

                    </a>

                </li>

                <?php /* ?><li <?php $this->common->LeftSubNavActive('faq_feedback_chart',2); ?>>

                  <a href="<?php echo site_url('admin/faq_feedback_chart'); ?>">

                  <i class="icon-double-angle-right"></i>

                  <span class="menu-text"> Feedback Chart </span>

                  </a>

                  </li><?php */ ?>

            </ul>

        </li>

        <?php $editsarray = array('compose', 'email_content'); ?>	

        <li <?php $this->common->leftnavactive($editsarray); ?>>

            <a href="javascript:void(0)" class="dropdown-toggle">

                <i class="icon-envelope"></i>

                <span class="menu-text">Email</span>

                <b class="arrow icon-angle-down"></b>

            </a>

            <ul class="submenu">

                <li  <?php if ($this->uri->segment(2) == 'compose') echo 'class="active"' ?>>

                    <a href="<?php echo site_url('admin/compose'); ?>">

                        <i class="icon-double-angle-right"></i>

                        <span class="menu-text"> Send Email </span>

                    </a>

                </li> 

                <li  <?php if ($this->uri->segment(2) == 'email_content') echo 'class="active"' ?>>

                    <a href="<?php echo site_url('admin/email_content'); ?>">

                        <i class="icon-double-angle-right"></i>

                        <span class="menu-text"> Email Content Setting </span>

                    </a>

                </li> 

            </ul>

        </li>
        
     <?php  
      $email = $this->session->userdata('email');
      $accesslevel = $this->session->userdata('accesslevel');
      if($email=="donna@airportrevenuenews.com" &&  $accesslevel==4){
     ?> 
         <li>

            <a href="<?php  echo site_url('admin/reset_records');  ?>">

                        <i class="icon-undo bigger-120"></i>
                     
                        <span class="menu-text"> Reset Records </span>

                    </a>

        </li>

        <?php } ?>

        <?php $dashboard = array(''); ?>	

<!--        <li <?php $this->common->leftnavactive($dashboard); ?>>

            <a href="<?php echo site_url('admin'); ?>">

                <i class="icon-dashboard"></i>

                <span class="menu-text"> Dashboard </span>

            </a>

        </li>-->

        <?php // $editview_activity_logsarray = array('view_activity_log'); ?>    

<!--        <li <?php // $this->common->leftnavactive($editsarray);  ?>>

            <a href="<?php // echo site_url('admin/view_activity_log');  ?>">

                        <i class="icon-calendar"></i>

                        <span class="menu-text"> View Activity Log </span>

                    </a>

        </li>-->

    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">

        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>

    </div>

    <script type="text/javascript">

        try {

            ace.settings.check('sidebar', 'collapsed')

        } catch (e) {

        }

    </script>

</div>