<div class="navbar navbar-default" id="navbar">
    
    <style>
    .btnadm{
        display: block;
        padding: 3px 72px 3px 3px;
        clear: both;
        font-weight: normal;
        line-height: 1.428571429;
        color: #333;
        white-space: nowrap;
	font-size: 13px;
        margin-bottom: 1px;
        margin-top: 1px;
        background: white;
        border: none;
    }

    .btnadm:hover
    {
        background: #fee188;
        color: #444;
    }
    </style>
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left">
            <a href="<?php echo site_url('admin') ?>" class="navbar-brand">
                <small>
                    <i class="icon-leaf"></i>
                    ArnfactBook Admin
                </small>
            </a><!-- /.brand -->
        </div><!-- /.navbar-header -->
        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="grey">
                    <a href="<?php echo base_url('') ?>" title="Go Back To Live Site">
                        <i class="icon-arrow-left icon-on-left"></i>Live Site								
                    </a>
                </li>
                <li class="grey">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)">
                        <i class="icon-plane"></i>								
                    </a>
                    <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                        <li class="dropdown-header">
                            <i class="icon-ok"></i>
                            <?php $this->common->topnav('totalAirport'); ?>
                        </li>

                        <li>
                            <a href="javascript:void(0)" title="<?php $this->common->topnav('num_rowmod'); ?>%">
                                <div class="clearfix">
                                    <span class="pull-left">Modified:	</span>
                                    <span class="pull-right"><?php $this->common->topnav('totalmodified'); ?></span>
                                </div>

                                <div class="progress progress-mini ">
                                    <div style="width:<?php $this->common->topnav('num_rowmod'); ?>%" class="progress-bar "></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0)" title="<?php $this->common->topnav('num_rowpub'); ?>%">
                                <div class="clearfix">
                                    <span class="pull-left">Published</span>
                                    <span class="pull-right"><?php $this->common->topnav('totalpublished'); ?></span>
                                </div>

                                <div class="progress progress-mini ">
                                    <div style="width:<?php $this->common->topnav('num_rowpub'); ?>%" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0)" title="<?php $this->common->topnav('num_rowapp'); ?>%">
                                <div class="clearfix">
                                    <span class="pull-left">Approved</span>
                                    <span class="pull-right"><?php $this->common->topnav('totalapproved'); ?></span>
                                </div>

                                <div class="progress progress-mini ">
                                    <div style="width:<?php $this->common->topnav('num_rowapp'); ?>%"  class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <!--								<li>
                                                                                                <a href="javascript:void(0)">
                                                                                                        See tasks with details
                                                                                                        <i class="icon-arrow-right"></i>
                                                                                                </a>
                                                                                        </li>-->
                    </ul>
                </li>

                <!--<li class="purple">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="icon-user icon-animated-bell"></i>
                                <span class="badge badge-important">8</span>
                        </a>

                        <ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
                                <li class="dropdown-header">
                                        <i class="icon-warning-sign"></i>
                                        8 Notifications
                                </li>

                                <li>
                                        <a href="#">
                                                <div class="clearfix">
                                                        <span class="pull-left">
                                                                <i class="btn btn-xs no-hover btn-pink icon-comment"></i>
                                                                New Comments
                                                        </span>
                                                        <span class="pull-right badge badge-info">+12</span>
                                                </div>
                                        </a>
                                </li>

                                <li>
                                        <a href="#">
                                                <i class="btn btn-xs btn-primary icon-user"></i>
                                                Bob just signed up as an editor ...
                                        </a>
                                </li>

                                <li>
                                        <a href="#">
                                                <div class="clearfix">
                                                        <span class="pull-left">
                                                                <i class="btn btn-xs no-hover btn-success icon-shopping-cart"></i>
                                                                New Orders
                                                        </span>
                                                        <span class="pull-right badge badge-success">+8</span>
                                                </div>
                                        </a>
                                </li>

                                <li>
                                        <a href="#">
                                                <div class="clearfix">
                                                        <span class="pull-left">
                                                                <i class="btn btn-xs no-hover btn-info icon-twitter"></i>
                                                                Followers
                                                        </span>
                                                        <span class="pull-right badge badge-info">+11</span>
                                                </div>
                                        </a>
                                </li>

                                <li>
                                        <a href="#">
                                                See all notifications
                                                <i class="icon-arrow-right"></i>
                                        </a>
                                </li>
                        </ul>
                </li>-->

                <!--<li class="green">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="icon-envelope icon-animated-vertical"></i>
                                <span class="badge badge-success">5</span>
                        </a>

                        <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                                <li class="dropdown-header">
                                        <i class="icon-envelope-alt"></i>
                                        5 Messages
                                </li>

                                <li>
                                        <a href="#">
                                                <img src="<?php echo base_url(); ?>assets/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar" />
                                                <span class="msg-body">
                                                        <span class="msg-title">
                                                                <span class="blue">Alex:</span>
                                                                Ciao sociis natoque penatibus et auctor ...
                                                        </span>

                                                        <span class="msg-time">
                                                                <i class="icon-time"></i>
                                                                <span>a moment ago</span>
                                                        </span>
                                                </span>
                                        </a>
                                </li>

                                <li>
                                        <a href="#">
                                                <img src="<?php echo base_url(); ?>assets/avatars/avatar3.png" class="msg-photo" alt="Susan's Avatar" />
                                                <span class="msg-body">
                                                        <span class="msg-title">
                                                                <span class="blue">Susan:</span>
                                                                Vestibulum id ligula porta felis euismod ...
                                                        </span>

                                                        <span class="msg-time">
                                                                <i class="icon-time"></i>
                                                                <span>20 minutes ago</span>
                                                        </span>
                                                </span>
                                        </a>
                                </li>

                                <li>
                                        <a href="#">
                                                <img src="<?php echo base_url(); ?>assets/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar" />
                                                <span class="msg-body">
                                                        <span class="msg-title">
                                                                <span class="blue">Bob:</span>
                                                                Nullam quis risus eget urna mollis ornare ...
                                                        </span>

                                                        <span class="msg-time">
                                                                <i class="icon-time"></i>
                                                                <span>3:15 pm</span>
                                                        </span>
                                                </span>
                                        </a>
                                </li>

                                <li>
                                        <a href="">
                                                See all messages
                                                <i class="icon-arrow-right"></i>
                                        </a>
                                </li>
                        </ul>
                </li>-->

                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
<!--								<img class="nav-user-photo" src="<?php // echo base_url(); ?>assets/avatars/user.jpg" alt="Jason's Photo" />-->
                        <span class="user-info">
                            <small>Welcome,</small>
                            <?php echo $this->common->GetCurrentUserInfo('first_name'); ?>&nbsp;<?php echo $this->common->GetCurrentUserInfo('last_name'); ?>
                        </span>

                        <i class="icon-caret-down"></i>
                    </a>

                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="<?php echo site_url('admin/access_config'); ?>">
                                <i class="icon-cog"></i>
                                Settings
                            </a>
                        </li>

                        <!--	<li>
                                        <a href="">
                                                <i class="icon-user"></i>
                                                Profile
                                        </a>
                                </li>
                        -->
                        <li class="divider"></li>

                        <li>
<!--				<a href="<?php // echo site_url('login/logout'); ?>">
                                        <i class="icon-off"></i>
                                        Logout
                                </a>-->

                            <form method="post" action="<?php echo site_url('login/logout'); ?>">
                                <input type="hidden" name="last_url" value="<?php echo current_url(); ?>" >
                                <div class="col-sm-2">
                                    <button type="submit" class="btnadm"><i class="icon-off"></i>&nbsp; Logout</button>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul><!-- /.ace-nav -->
        </div><!-- /.navbar-header -->
    </div><!-- /.container -->
</div>