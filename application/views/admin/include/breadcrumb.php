<div class="breadcrumbs" id="breadcrumbs">
    <script type="text/javascript">
        try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
    </script>
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="<?php echo site_url('admin');?>">Home</a>
        </li>
        <?php
			$count=0;
			$segs = $this->uri->segment_array();					
			foreach ($segs as $segment)
			{	
				if($segment!="admin")
				{
				?>
                	<li <?php if($segs[count($segs)]==$segment) { ?> class="active" <?php } ?>>
						<a href="<?php $this->common->breadcurmburl($segment,$segs);?>">
							<?php echo $this->common->SegmentToName($segment);?>
                        </a>   
                   </li>
                <?php							
				}
			}
		?>   		
    </ul>
    <?php /*?><div class="nav-search" id="nav-search">
        <form class="form-search" method="post" action="<?php echo $searchaction; ?>">
            <span class="input-icon">
          <input type="text" placeholder="Search ..." name="search" class="col-sm-12" required="required" autocomplete="off"/>
                <i class="icon-search nav-search-icon"></i>
            </span>
        </form>
    </div><?php */?><!-- #nav-search -->
</div>