<?php
$this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
$this->output->set_header('Pragma: no-cache');
?>

<meta charset="utf-8" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!--favicon-->
<link rel="shortcut icon" href="<?php echo base_url('assets/img'); ?>/favicon4.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/jquery-ui-1.10.3.full.min.css" >
<!-- basic styles -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/font/font-awesome.min.css" />
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
<!--[if IE 7]>
  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
<![endif]-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-fonts.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-rtl.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-skins.min.css" />
<!--[if lte IE 8]>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-ie.min.css" />
<![endif]-->
<script src="<?php echo base_url();?>assets/js/ace-extra.min.js"></script>
<!--[if lt IE 9]>
<script src="<?php echo base_url();?>assets/js/html5shiv.js"></script>
<script src="<?php echo base_url();?>assets/js/respond.min.js"></script>
<![endif]-->