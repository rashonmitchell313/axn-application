<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Multi Language | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'Multi Language | ARN Fact Book';
            }
            ?>
        </title>

        <?php echo $this->load->view($header); ?>
    </head>
    <body>
        <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>                    
                    <div class="page-content">
                        <div class="row">             																											
                            <div class="col-xs-12">
                                <?php $this->common->getmessage(); ?>
                                <div class="tabbable">
                                    <ul id="myTab" class="nav nav-tabs padding-18 tab-size-bigger">
                                        <li class="active">
                                            <a href="#faq-tab-1" data-toggle="tab">
                                                <i class="blue icon-signin bigger-120"></i>
                                                Login Page
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#faq-tab-2" data-toggle="tab">
                                                <i class="green icon-credit-card bigger-120"></i>
                                                Home Page Section 1
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#faq-tab-3" data-toggle="tab">
                                                <i class="orange icon-credit-card bigger-120"></i>
                                                Home Page Section 2
                                            </a>
                                        </li>
                                        <!-- /.dropdown -->
                                    </ul>   
                                    
                                    
                                    <div class="tab-content no-border padding-24">
                                        <div class="tab-pane fade active in" id="faq-tab-1">
                                            <h4 class="blue">
                                                <i class="icon-signin bigger-110"></i>
                                                Login Page Content
                                            </h4>
                                            <div class="space-8"></div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <!-- ------------------------>
                                                    <div class="panel-group accordion-style1 accordion-style2" id="faq-list-1">
                                                        <?php foreach ($text_multilang as $text): ?>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#faq-list-1" href="#faq-1-<?php echo $text['id'] ?>">
                                                                        <i data-icon-show="icon-chevron-left" data-icon-hide="icon-chevron-down" class="icon-chevron-left pull-right"></i>

                                                                        <i class="icon-signin bigger-130"></i>
                                                                        &nbsp; Login Page Content In <strong><?php echo $text['text_language_name']; ?></strong>
                                                                    </a>
                                                                </div>
                                                                <div id="faq-1-<?php echo $text['id'] ?>" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <?php echo $text['login_content']; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                    <!-- ------------------ -->
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->					
                                        </div>
                                        </div>

                                        <div class="tab-pane fade" id="faq-tab-2">
                                            <h4 class="blue">
                                                <i class="icon-user bigger-110"></i>
                                                Home page Content
                                            </h4>
                                            <div class="space-8"></div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="panel-group accordion-style1 accordion-style2" id="faq-list-2">
                                                        <?php foreach ($text_multilang as $text): ?>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#faq-list-2" href="#faq-2-<?php echo $text['id'] ?>">
                                                                        <i data-icon-show="icon-chevron-left" data-icon-hide="icon-chevron-down" class="icon-chevron-left pull-right"></i>

                                                                        <i class="icon-credit-card bigger-130"></i>
                                                                        &nbsp; Home Page Content 1 In <strong><?php echo $text['text_language_name']; ?></strong>
                                                                    </a>
                                                                </div>
                                                                <div id="faq-2-<?php echo $text['id'] ?>" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <?php echo $text['home_content']; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div><!-- /.col -->
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="faq-tab-3">
                                            <h4 class="blue">
                                                <i class="icon-credit-card bigger-110"></i>
                                                Home Content 2
                                            </h4>                                                     <div class="space-8"></div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="panel-group accordion-style1 accordion-style2" id="faq-list-3">
                                                        <?php foreach ($text_multilang as $text): ?>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#faq-list-3" href="#faq-3-<?php echo $text['id'] ?>">
                                                                        <i data-icon-show="icon-chevron-left" data-icon-hide="icon-chevron-down" class="icon-chevron-left pull-right"></i>

                                                                        <i class="icon-credit-card bigger-130"></i>
                                                                        &nbsp; Home Page Content 2 In <strong><?php echo $text['text_language_name']; ?></strong>
                                                                    </a>
                                                                </div>
                                                                <div id="faq-3-<?php echo $text['id'] ?>" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <?php echo $text['home_content2']; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div><!-- /.col -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- PAGE CONTENT ENDS -->
                            </div>																			
                        </div>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->        
        <?php echo $this->load->view($footer); ?>
        
        
<!--        <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#myTab').tab();
    });
</script> -->
    </body>

</body>
</html>
