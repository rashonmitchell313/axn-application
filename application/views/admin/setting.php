<!DOCTYPE html>

<html lang="en">

    <head>

        

        <title>

            <?php
            if ($this->uri->segment(0) != '') {

                echo 'Welcome to ARN Fact Book | Admin ' . $this->uri->segment(0);
            } else {

                echo 'Welcome to ARN Fact Book | Admin';
            }
            ?>

        </title>
        
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.full.min.css" />

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.css" />
        
        <?php echo $this->load->view($header); ?>

    </head>

    <body>

        <?php echo $this->load->view($topnav); ?>

        <div class="main-container" id="main-container">

<!--            <script type="text/javascript">

                try {

                    ace.settings.check('main-container', 'fixed')

                } catch (e) {

                }

            </script>-->

            <div class="main-container-inner">

                <a class="menu-toggler" id="menu-toggler" href="#">

                    <span class="menu-text"></span>

                </a>

                <?php echo $this->load->view($leftnav); ?>	

                <div class="main-content">

                    <?php echo $this->load->view($breadcrumb); ?>

                    <div class="page-content">

                        <div class="row">

                            <div class="col-xs-12">

                                <div class="row">

                                    <div class="space-6"></div>



                                    <!--                                    <div class="col-sm-8 margin-auto">

                                                                            <div class="error">

                                    

                                                                            </div>                           

                                                                            <form  action="" class="form-horizontal" method="post" onSubmit="return passwordcheck();">

                                                                                <div class="form-group">

                                                                                    <label class="col-sm-5 control-label">Original Password:

                                                                                        &nbsp;<span class="red">*</span></label>                                            

                                                                                    <div class="col-sm-7">

                                                                                        <input type="password" class="form-control" id="opassword" placeholder="Original Password">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label class="col-sm-5 control-label">New Password:&nbsp;<span class="has-warning">*</span></label>

                                                                                    <div class="col-sm-7">

                                                                                        <input type="password" class="form-control" id="nopassword" placeholder="New Password">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <label class="col-sm-5 control-label">Type New Password again:<span class="red">*</span></label>

                                                                                    <div class="col-sm-7">

                                                                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="form-group">

                                                                                    <div class="col-sm-offset-5 col-sm-10">

                                                                                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>

                                                                                    </div>

                                                                                </div>

                                                                            </form>

                                                                        </div> -->

                                    <div class="vspace-sm"></div>

                                    <?php echo $this->common->getmessage(); ?>

                                    <div class="tabbable">

                                        <ul id="myTab" class="nav nav-tabs padding-18 tab-size-bigger">

                                             <li class="active">

                                                <a href="#faq-tab-4" data-toggle="tab">

                                                    <i class="green icon-cog bigger-120"></i>

                                                    Footer Contact 

                                                </a>

                                            </li>
                                            
                                            <li class="">

                                                <a href="#faq-tab-1" data-toggle="tab">

                                                    <i class="green icon-credit-card bigger-120"></i>

                                                    Change Password

                                                </a>

                                            </li>

                                            <li class="">

                                                <a href="#faq-tab-2" data-toggle="tab">

                                                    <i class="blue icon-signin bigger-120"></i>

                                                    Re-Login Time

                                                </a>

                                            </li>

                                            <li class="">

                                                <a href="#faq-tab-3" data-toggle="tab">

                                                    <i class="green icon-cogs bigger-120"></i>

                                                    Buttons Configuration 

                                                </a>

                                            </li>

<!--                                            <li class="">

                                                <a href="#faq-tab-4" data-toggle="tab">

                                                    <i class="green icon-cog bigger-120"></i>

                                                    Footer Contact 

                                                </a>

                                            </li>-->

                                        </ul>            

                                        <div class="tab-content no-border padding-24">

                                            <div class="tab-pane fade  " id="faq-tab-1">

                                                <h4 class="blue">

                                                    <i class="green icon-user bigger-110"></i>

                                                    Change Password

                                                </h4>

                                                <div class="space-8"></div>

                                                <div class="row">

                                                    <div class="col-sm-8 margin-auto">

                                                        <div class="error"></div>                           

                                                        <form  action="" class="form-horizontal" method="post" id="form1" data-toggle="validator" role="form" onSubmit="return !!(passwordcheck() & checkval());">

                                                            <div class="form-group has-feedback">

                                                                <label class="col-sm-5 control-label">Original Password:

                                                                    &nbsp;<span class="red">*</span></label>                                            

                                                                <div class="col-sm-7">

<!--<input type="hidden" value="<?php ?>" id="opass" >-->

                                                                    <input type="password" class="form-control" id="opassword" name="opassword" placeholder="Original Password" data-error="Password is invalid" required autocomplete="off" style="cursor:text" readonly onfocus="this.removeAttribute('readonly');" style="background: #fff !important;">

                                                                </div>

                                                                <div class="col-sm-7 pull-right help-block with-errors"></div>

                                                            </div>

                                                            <div class="form-group has-feedback">

                                                                <label class="col-sm-5 control-label">New Password:&nbsp;<span class="has-warning red">*</span></label>

                                                                <div class="col-sm-7">

                                                                    <input type="password" class="form-control" id="nopassword" name="nopassword" placeholder="New Password" data-error="New Password is invalid" required autocomplete="off" style="cursor:text" readonly onfocus="this.removeAttribute('readonly');" style="background: #fff !important;">

                                                                </div>

                                                                <div class="col-sm-7 pull-right help-block with-errors"></div>

                                                            </div>

                                                            <div class="form-group has-feedback">

                                                                <label class="col-sm-5 control-label">Type New Password again:<span class="red">*</span></label>

                                                                <div class="col-sm-7">

                                                                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password"  data-match="#nopassword" data-match-error="Whoops, Password don't match" required readonly style="cursor:text" onfocus="this.removeAttribute('readonly');" style="background: #fff !important;">

                                                                </div>

                                                                <div class="col-sm-7 pull-right help-block with-errors"></div>

                                                            </div>

                                                            <!--                                                            <div class="form-group">
                                                            
                                                                                                                            <div class="col-sm-offset-5 col-sm-10">
                                                            
                                                                                                                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>
                                                            
                                                                                                                            </div>
                                                            
                                                                                                                        </div>-->
                                                            <div class="form-group">

                                                                <div class="col-sm-offset-5 col-sm-10">

<!--<button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>-->

                                                                    <!--<button type="submit" onClick="MM_validateForm('opassword', '', 'R', 'nopassword', '', 'R', 'cpassword', '', 'R'); return document.MM_returnValue" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>-->
                                                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Set New Password</button>

                                                                </div>

                                                            </div>

                                                        </form>

                                                    </div>

                                                </div><!-- /.row -->					

                                            </div>

                                            <div class="tab-pane fade" id="faq-tab-2">

                                                <h4 class="blue">

                                                    <i class="icon-signin bigger-110"></i>

                                                    Re-Login Time

                                                </h4>

                                                <div class="space-8"></div>

                                                <div class="row">

                                                    <div class="col-sm-8 margin-auto">

                                                        <div class="error2"></div>                           

                                                        <form  action="" class="form-horizontal" method="post" id="form11" data-toggle="validator" role="form" onSubmit="return relogintime();">

                                                            <div class="form-group has-feedback">

                                                                <label class="col-sm-5 control-label">New Re-Login Time:&nbsp;</label>                                            

                                                                <div class="col-sm-7">

                                                                    <input type="text" class="form-control" id="relogin_time" placeholder="Re-Login Time" value="<?php echo $this->common->GetSessionKey('login_time_out') ?>"  data-error="Re-Login Time is invalid" required>

                                                                </div>

                                                                <div class="col-sm-7 pull-right help-block with-errors"></div>

                                                            </div>

                                                            <div class="form-group">

                                                                <div class="col-sm-offset-5 col-sm-10">

                                                                    <button type="submit" class="btn btn-success"><i class="icon-cloud-upload"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php // echo site_url('admin');                       ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>

                                                                </div>

                                                            </div>

                                                        </form>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="tab-pane fade" id="faq-tab-3">

                                                <h4 class="blue">

                                                    <i class="green icon-cogs bigger-110"></i>

                                                    Buttons Access Configuration

                                                </h4>



                                                <div class="row">

                                                    <div class="col-sm-8 margin-auto">



                                                        <div class="col-sm-6 margin-auto">

                                                            <table class="table table-striped">

                                                                <thead>
                                                                    <tr>
                                                                        <td>
                                                                            Access
                                                                        </td>
                                                                        <td>
                                                                            Level
                                                                        </td>
                                                                        <td>
                                                                            Active
                                                                        </td>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    <?php foreach ($access_config_level as $btn) { ?>

                                                                        <tr>
                                                                            <td>
                                                                                <?php echo $btn['access_name']; ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php echo $btn['level']; ?>
                                                                            </td>
                                                                            <td>
                                                                                <!--<i class="green icon-user bigger-110"></i>-->
                                                                                <?php if ($btn['is_active'] == '0') { ?>

                                                                                    <i class="red icon-lock bigger-200" title="Not Allowed"></i>

                                                                                    <?php
                                                                                } else {
                                                                                    ?>

                                                                                    <i class="green icon-unlock bigger-200" title="Allowed"></i>

                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>


                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>

                                                        </div>

                                                        <div class="col-sm-6 margin-auto">

                                                            <!--<div class="error3"></div>-->
                                                            <div class="error"></div>

                                                            <!--<form action="" class="form-horizontal" method="post" onSubmit="return setuseraccess();">-->
                                                            <form action="<?php echo site_url('admin/access_config') ?>" class="form-horizontal" method="post">

                                                                <div class="form-group">
                                                                    <select id="access_name" name="access_name" class="form-control">
                                                                        <!--<option value="none">-- Please Select User Access --</option>-->
                                                                        <?php
                                                                        foreach ($access_config as $key => $access):
                                                                            ?>

                                                                            <option value="<?php echo $access['id']; ?>">
                                                                                <?php echo $access['access_name']; ?>
                                                                            </option>
                                                                            <?php
                                                                        endforeach;
                                                                        ?>
                                                                    </select>
                                                                </div>


                                                                <div class="form-group">
                                                                    <select class="form-control" id="access_level" name="access_level">
                                                                        <!--<option value="none">-- Please Select User Level --</option>-->
                                                                        <?php
                                                                        foreach ($AccessLevel as $key => $alevel):
                                                                            ?>

                                                                            <option value="<?php echo $alevel['levelid']; ?>">
                                                                                <?php echo $alevel['level']; ?>
                                                                            </option>
                                                                            <?php
                                                                        endforeach;
                                                                        ?>
                                                                    </select>
                                                                </div>

                                                                <br/>
                                                                <div class="form-group">                          
                                                                    <label for="form-field-1" class="col-sm-2 control-label no-padding-right"> 
                                                                        Active:
                                                                    </label>                       

                                                                    <input type="checkbox" class="ace ace-switch ace-switch-7" name="is_active_access" id="is_active_access" checked="checked">
                                                                    <span class="lbl"></span>
                                                                    <span class="red">(Check this set for user access active/not.)</span>									

                                                                </div>

                                                                <button type="submit" class="btn btn-success"><i class="icon-cloud-upload"></i>&nbsp;Update</button>
                                                            </form>


                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                            <div class="tab-pane fade active in" id="faq-tab-4">

<!--                                                <h4 class="blue">

                                                    <i class="icon-cog bigger-110"></i>

                                                    Footer Contact Configuration

                                                </h4>-->

                                                <div class="space-8"></div>

                                                <div class="row">

                                                    <!--<div class="col-sm-8 margin-auto">-->
                                                    
                                                    <div class="widget-box">
                                                        
                                                         <div class="widget-header header-color-blue2">

                                                            <h4 class="lighter smaller"><i class="icon-cog bigger-110"></i>Footer Contact Configuration</h4>

                                                        </div>

                                                        <div class="error4"></div>                           

                                                        <div class="widget-body">

                                                            <div class="widget-main">

                                                                <form onSubmit="//return validate_form()" action="<?php // echo site_url('admin/email_content/' . $this->uri->segment(3)); ?>" method="post" id="id-message-form" class="form-horizontal">

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-recipient">Footer Section:</label>
                                                                        <div class="col-sm-6">
                                                                            <select  id="content_id"  class="chosen-select col-sm-12" placeholder="Select an Option">
                                                                                <option value="">Select an Option</option>
                                                                                <?php // echo 'test'; print_r($FooterContent);  ?>
                                                                                <?php foreach ($FooterContent as $user):?>
                                                                               
                                                                                    <option value="<?php echo $user['id']; ?>" <?php if ($this->uri->segment(3) == $user['id']) { ?> selected="selected" <?php } ?>><?php echo ucwords($user['content_name']); ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-subject">Required Words:</label>
                                                                        <div class="col-sm-6 col-xs-6">
                                                                            <div class="input-icon block col-xs-12 no-padding">
                                                                                <input id="nrwords" maxlength="100" type="text" class="col-xs-12" value="<?php
                                                                                if ($this->uri->segment(3) != '') {
                                                                                    echo $FooterContent_t['n_remove_words'];
                                                                                }
                                                                                ?>"> 
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Message: <span class="red">*</span></label>                                    
                                                                        <div class="col-sm-9">
                                                                            
                                                                            <textarea style="height: 250px;" id="message" name="message" class="col-xs-12 col-sm-10"><?php
                                                                                            if ($this->uri->segment(3) != '') {
                                                                                                echo $FooterContent_t['content'];
                                                                                            }
                                                                                            ?></textarea>
                                                                            
<!--                                                                            <div class="widget-box">
                                                                                <div class="widget-header widget-header-small  header-color-blue">
                                                                                    <div class="widget-toolbar">
                                                                                        <a href="javascript:void(0)" data-action="collapse">
                                                                                            <i class="icon-chevron-up"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="widget-body">
                                                                                    <div class="widget-main no-padding">
                                                                                        <div class="wysiwyg-editor" id="editor"><?php
//                                                                                            if ($this->uri->segment(3) != '') {
//                                                                                                echo $FooterContent_t['content'];
//                                                                                            }
                                                                                            ?></div>
                                                                                        <textarea style="display:none;" id="message" name="message" class="col-xs-10 col-sm-5"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>-->
                                                                        </div>
                                                                    </div>

                                                                    <div class="space"></div>
                                                                    <div class="clearfix form-actions">
                                                                        <div class="col-md-offset-3 col-md-6"> 
                                                                            <div id="id-message-item-navbar">
                                                                                <div class="message-bar"></div>
                                                                                <span class="inline btn-send-message">
                                                                                    <button type="submit" class="btn btn-sm btn-success no-border" id="save" data-toggle="tooltip" title='Do Not Remove those words which have "#" sign. Further not show exact data.' >
                                                                                        <span class="bigger-110">Save Changes</span>

                                                                                        <i class="icon-ok icon-on-right"></i>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--</div>-->
                                                                </form>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <!--</div>-->

                                        <!-- /span -->

                                    </div><!-- /row -->

                                    <!-- PAGE CONTENT ENDS -->

                                </div><!-- /.col -->

                            </div><!-- /.col -->

                        </div><!-- /.row -->	

                    </div><!-- /.page-content -->

                </div><!-- /.main-content -->

            </div><!-- /.main-container-inner -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">

                <i class="icon-double-angle-up icon-only bigger-110"></i>

            </a>

        </div><!-- /.main-container -->

        <script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/jquery-2.0.3.min.js"></script>
        
        <?php echo $this->load->view($footer); ?>
        
        <script src="<?php echo base_url('assets'); ?>/js/bootstrap-wysiwyg.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>

        <script src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.custom.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>

        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>



        <script type="text/javascript">


                                                    $('#form1').validator();
                                                    $('#form11').validator();

                                                    $(".alert-dismissible").fadeTo(3000, 500).slideUp(500, function () {
                                                        $(".alert-dismissible").alert('close');
                                                    });
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                     $('button[data-toggle="tooltip"]').tooltip({
                                                        animated: 'fade',
                                                        placement: 'top',
                                                    });
                                                    
                                                    $('#nrwords').prop('readonly', true);

//                                                    $(".chosen-select").chosen({width: "41.5%"});


                                                    $('#content_id').change(function () {

                                                        var ecid = $('#content_id option:selected').val();

                                                        window.location = "<?php echo site_url('admin/access_config'); ?>" + '/' + ecid;

                                                    });

//                                                    $('#editor').ace_wysiwyg({
//                                                        toolbar_place: function (toolbar) {
//                                                            return $(this).closest('.widget-box').find('.widget-header').prepend(toolbar).children(0).addClass('inline');
//                                                        },
//                                                        toolbar:
//                                                                [
//                                                                    'bold',
//                                                                    {name: 'italic', title: 'Change Title!', icon: 'icon-leaf'},
//                                                                    'strikethrough',
//                                                                    null,
//                                                                    'insertunorderedlist',
//                                                                    'insertorderedlist',
//                                                                    null,
//                                                                    'justifyleft',
//                                                                    'justifycenter',
//                                                                    'justifyright'
//                                                                ],
//                                                        speech_button: false
//                                                    }).prev().addClass('wysiwyg-style2');
//$('#textarea').ace_wysiwyg();

//                                                    $('#myform').on('reset', function () {
//                                                        $('#editor').empty();
//                                                    });

                                                    function validate_form()
                                                    {
                                                        var message = $("#editor").html();
                                                        if (message == "")
                                                        {
                                                            $(".message").html('<div class="alert alert-danger">You cannot Update empty Content.</div>');
                                                            $(".message").children().attr('tabindex', 0).delay(3000).fadeOut();
                                                            return false;
                                                        }

                                                        $("#save").html('<i class="fa fa-spinner fa-spin"></i> Saving');
                                                        $("#message").val(message);
                                                        return true;
                                                    }

                                                    $('.getmsg').children().attr('tabindex', 0).delay(3000).fadeOut();
                                                    
                                                    
                                                    

                                                    function checkval()

                                                    {

                                                        var rfield = new Array("opassword", "nopassword", "cpassword");

                                                        var msg = new Array('Original Password', 'New Password', 'Confirm Password');

                                                        var errmsg = "";

                                                        for (i = 0; i < rfield.length; i++)

                                                        {

                                                            //alert(rfield[i]);

                                                            var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                                                            if (val == "" || val.replace(" ", "") == "")

                                                            {

//                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";
                                                                errmsg += "" + msg[i] + " is Required. <br/>";


                                                            }

                                                        }

                                                        if (errmsg != "")

                                                        {

                                                            $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                                                            $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                                                            return false;

                                                        }

                                                        return true;

                                                    }

//    function titleCase(nm) {
//                                                                var titleStr = nm.split(' ');
//                                                                for (var i = 0; i < titleStr.length; i++) {
//                                                                    titleStr[i] = titleStr[i].charAt(0).toUpperCase() + titleStr[i].slice(1).toLowerCase();
//                                                                }
//                                                                return titleStr.join(' ');
//                                                            }

                                                    $(".chosen-select").chosen({width: "83.4%"});

                                                    /*   function MM_validateForm() { //v4.0
                                                     
                                                     
                                                     
                                                     if (document.getElementById) {
                                                     
                                                     
                                                     
                                                     var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
                                                     
                                                     
                                                     
                                                     for (i = 0; i < (args.length - 2); i += 3) {
                                                     test = args[i + 2];
                                                     val = document.getElementById(args[i]);
                                                     
                                                     
                                                     
                                                     if (val) {
                                                     nm = val.name;
                                                     if ((val = val.value) != "") {
                                                     
                                                     
                                                     
                                                     if (test.indexOf('isEmail') != -1) {
                                                     p = val.indexOf('@');
                                                     
                                                     
                                                     
                                                     if (p < 1 || p == (val.length - 1))
                                                     errors += '- ' + nm + ' must contain an e-mail address.\n';
                                                     
                                                     
                                                     
                                                     } else if (test != 'R') {
                                                     num = parseFloat(val);
                                                     
                                                     
                                                     
                                                     if (isNaN(val))
                                                     errors += '- ' + nm + ' must contain a number.\n';
                                                     
                                                     
                                                     
                                                     if (test.indexOf('inRange') != -1) {
                                                     p = test.indexOf(':');
                                                     
                                                     
                                                     
                                                     min = test.substring(8, p);
                                                     max = test.substring(p + 1);
                                                     
                                                     
                                                     
                                                     if (num < min || max < num)
                                                     errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                                                     
                                                     
                                                     
                                                     }
                                                     }
                                                     } else if (test.charAt(0) == 'R')
                                                     var test1 = titleCase(nm.replace("_", " "))
                                                     
                                                     var mapObj = {
                                                     Opassword: "Original Password",
                                                     Nopassword: "New Password",
                                                     Cpassword: "Confirm Password"
                                                     
                                                     };//console.log(nm);
                                                     var re = new RegExp(Object.keys(mapObj).join("|"), "gi");
                                                     test1 = test1.replace(re, function (matched) {
                                                     return mapObj[matched];
                                                     });
                                                     
                                                     errors += test1 + ' is required.<br />';
                                                     //                                                                                errors += '' + nm + ' is required.<br />';
                                                     }
                                                     }
                                                     if (errors) {
                                                     //                                                                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors.replace("_", " ").toUpperCase() + "</div></div></div>");
                                                     $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors + "</div></div></div>");
                                                     
                                                     $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');
                                                     
                                                     }
                                                     
                                                     
                                                     
                                                     document.MM_returnValue = (errors == '');
                                                     
                                                     
                                                     
                                                     }
                                                     } */







                                                    function movetoerror(msg)

                                                    {

                                                        $(".error").html(msg);

                                                        $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                                                    }

                                                    function movetoerror2(msg)

                                                    {

                                                        $(".error2").html(msg);

                                                        $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                                                    }

                                                    function movetoerror3(msg)

                                                    {

                                                        $(".error3").html(msg);

                                                        $('html, body').animate({scrollTop: $('.error3').offset().top}, 'slow');

                                                    }

                                                    function passwordcheck() {

                                                        var opassword = $("#opassword").val();

                                                        var cpassword = $("#cpassword").val();

                                                        var npassword = $("#nopassword").val();

                                                        if (opassword == "" || cpassword == "" || npassword == "")

                                                        {



                                                            //var set="<?php // echo $this->common->setmessage('(*) asterisk is indicates field is required.', '-1');                       ?>";	

//                                                                    movetoerror("<div class='row'><div class='col-sm-8'><div class='alert alert-danger'>(*) asterisk is indicates field is required.</div></div></div>");

                                                        } else if (cpassword != npassword)

                                                        {

                                                            movetoerror("<div class='row'><div class='col-sm-8'><div class='alert alert-danger'>Confirm Password must be match with password.</div></div></div>");

                                                        } else

                                                        {

                                                            movetoerror("<div class='row'><div class='col-sm-8'><div class='alert alert-success'><i class='fa fa-spinner fa-spin fa-1x'></i>&nbsp; Updateing password.</div></div></div>");

                                                            $.ajax({
                                                                url: "<?php echo site_url('ajax/doaction'); ?>",
                                                                data: "opassword=" + opassword + "&cpassword=" + cpassword + "&npassword=" + npassword + "&action=changepassword",
                                                                type: "POST",
                                                                cache: false,
                                                                async: false,
                                                                success: function (html)

                                                                {

                                                                    if (html == 1)

                                                                    {

                                                                        movetoerror("<div class='row'><div class='col-sm-8'><div class='alert alert-success'><i class='fa fa-check fa-1x'></i>&nbsp;Password has been update Successfully.</div></div></div>");

                                                                    } else

                                                                    {

                                                                        movetoerror("<div class='row'><div class='col-sm-8'><div class='alert alert-danger'>" + html + "</div></div></div>");

                                                                    }

                                                                }

                                                            });

                                                        }

                                                        return false;

                                                    }

                                                    function relogintime() {

                                                        var relogin_time = $("#relogin_time").val();

                                                        if (relogin_time == "" || relogin_time < 1)

                                                        {

                                                            movetoerror2("<div class='row'><div class='col-sm-7 pull-right'><div class='alert alert-danger'>Enter the Time in Minutes.</div></div></div>");

                                                        } else

                                                        {

                                                            movetoerror2("<div class='row'><div class='col-sm-7 pull-right'><div class='alert alert-success'><i class='fa fa-spinner fa-spin fa-1x'></i>&nbsp; Updateing Re-Login Time.</div></div></div>");

                                                            $.ajax({
                                                                url: "<?php echo site_url('ajax/doaction'); ?>",
                                                                data: "relogin_time=" + relogin_time + "&action=changerelogin_time",
                                                                type: "POST",
                                                                cache: false,
                                                                async: false,
                                                                success: function (html)

                                                                {

                                                                    if (html == 1)

                                                                    {

                                                                        movetoerror2("<div class='row'><div class='col-sm-7 pull-right'><div class='alert alert-success'><i class='fa fa-check fa-1x'></i>&nbsp;Re-Login Time has been update Successfully.</div></div></div>");

                                                                    } else

                                                                    {

                                                                        movetoerror2("<div class='row'><div class='col-sm-7 pull-right'><div class='alert alert-danger'>" + html + "</div></div></div>");

                                                                    }

                                                                }

                                                            });

                                                        }

                                                        return false;

                                                    }


                                                    function setuseraccess() {

                                                        var access_name = $("#access_name").val();

                                                        var access_level = $("#access_level").val();

                                                        var is_active_access = $("#is_active_access").val();

//                                                                alert(is_active_access); 
//                                                                alert(access_name); 
//                                                                alert(access_level); 
//                                                                return false;

                                                        if ((access_name == "" || access_name < 1) && (access_level == "" || access_level < 1))

                                                        {

                                                            movetoerror3("<div class='row'><div class='col-sm-12 pull-right'><div class='alert alert-danger'>Please select the both (User's Access & Access Level).</div></div></div>");

                                                        } else

                                                        {

                                                            movetoerror3("<div class='row'><div class='col-sm-12 pull-right'><div class='alert alert-success'><i class='fa fa-spinner fa-spin fa-1x'></i>&nbsp; setteing New Access.</div></div></div>");

                                                            $.ajax({
                                                                url: "<?php echo site_url('ajax/doaction'); ?>",
                                                                data: "access_name=" + access_name + "access_level=" + access_level + "is_active_access=" + is_active_access + "&action=changeuseraccess",
                                                                type: "POST",
                                                                cache: false,
                                                                async: false,
                                                                success: function (html)

                                                                {

                                                                    if (html == 1)

                                                                    {

                                                                        movetoerror3("<div class='row'><div class='col-sm-12 pull-right'><div class='alert alert-success'><i class='fa fa-check fa-1x'></i>&nbsp;User Access has been updated Successfully.</div></div></div>");

                                                                    } else

                                                                    {

                                                                        movetoerror3("<div class='row'><div class='col-sm-12 pull-right'><div class='alert alert-danger'>" + html + "</div></div></div>");

                                                                    }

                                                                }

                                                            });

                                                        }

                                                        return false;

                                                    }
                                                    
                                                    
                                                    

        </script>		

    </body>

</html>

