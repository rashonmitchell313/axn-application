<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Edit Content | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'Edit Content | ARN Fact Book';
            }
            ?>
        </title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/choseen.css" />
        <?php echo $this->load->view($header); ?>
    </head>
    <body>
        <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>                    
                    <div class="page-content">
                        <div class="row">             																											
                            <div class="col-xs-12">
                                <?php $this->common->getmessage(); ?>
                                <div class="tabbable">
                                    <ul id="myTab" class="nav nav-tabs padding-18 tab-size-bigger">
                                        <li class="active">
                                            <a href="#faq-tab-1" data-toggle="tab">
                                                <i class="blue icon-signin bigger-120"></i>
                                                Login Page
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#faq-tab-2" data-toggle="tab">
                                                <i class="green icon-credit-card bigger-120"></i>
                                                Home Page Section 1
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#faq-tab-3" data-toggle="tab">
                                                <i class="orange icon-credit-card bigger-120"></i>
                                                Home Page Section 2
                                            </a>
                                        </li>
                                        <!-- /.dropdown -->
                                    </ul>            
                                    <div class="tab-content no-border padding-24">
                                        <div class="tab-pane fade active in" id="faq-tab-1">
                                            <h4 class="blue">
                                                <i class="icon-signin bigger-110"></i>
                                                Login Page Content
                                            </h4>
                                            <div class="space-8"></div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <form method="post" onSubmit="return text('login_content')" action="<?php echo site_url('google/translate'); ?>">
                                                        <input type="hidden" name="login_content" id="login_content" value="">
                                                        <input type="hidden" name="content_type" value="login_content">
                                                        <div class="wysiwyg-editor editor1" id="login_content2">
                                                            <?php echo $page_content[0]['login_content']; ?>
                                                        </div>
                                                        <div class="space-12"></div>
                                                        <div class="form-group">
                                                            <select class="chosen-select col-sm-6" name="lang_id">
                                                                <?php foreach ($text_language as $lang): ?>
                                                                    <option value="<?php echo $lang['text_language_code']; ?>"><?php echo $lang['text_language_name']; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                        <div class="space-12"></div>
                                                        <br/>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-success"><i class="icon-cloud-upload"></i>&nbsp;Update</button>
                                                            &nbsp;&nbsp;<a href="<?php echo site_url('admin'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                        </div>
                                                    </form>
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->					
                                        </div>
                                        <div class="tab-pane fade" id="faq-tab-2">
                                            <h4 class="blue">
                                                <i class="green icon-user bigger-110"></i>
                                                Home page Content
                                            </h4>
                                            <div class="space-8"></div>
                                            <form method="post" onSubmit="return text('home_content2')" action="<?php echo site_url('google/translate'); ?>">
                                                <input type="hidden" name="home_content" id="home_content2" >
                                                <input type="hidden" name="content_type" value="home_content">
                                                <div class="wysiwyg-editor editor1" id="home_content22">
                                                    <?php echo $page_content[0]['home_content']; ?>
                                                </div>
                                                <div class="space-12"></div>
                                                <div class="form-group">
                                                    <select class="chosen-select col-sm-6" name="lang_id">
                                                        <?php foreach ($text_language as $lang): ?>
                                                            <option value="<?php echo $lang['text_language_code']; ?>"><?php echo $lang['text_language_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="space-12"></div>

                                                <br/>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success"><i class="icon-cloud-upload"></i>&nbsp;Update</button>
                                                    &nbsp;&nbsp;<a href="<?php echo site_url('admin'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="faq-tab-3">
                                            <h4 class="blue">
                                                <i class="orange icon-credit-card bigger-110"></i>
                                                Home Content 2
                                            </h4>
                                            <form method="post" onSubmit="return text('home_content222')" action="<?php echo site_url('google/translate'); ?>">
                                                <input type="hidden" name="home_content2" id="home_content222" >
                                                <input type="hidden" name="content_type" value="home_content2">
                                                <div class="wysiwyg-editor editor1" id="home_content2222">
                                                    <?php echo $page_content[0]['home_content2']; ?>
                                                </div>
                                                <div class="space-12"></div>
                                                <div class="form-group">
                                                    <select class="chosen-select col-sm-6" name="lang_id">
                                                        <?php foreach ($text_language as $lang): ?>
                                                            <option value="<?php echo $lang['text_language_code']; ?>"><?php echo $lang['text_language_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="space-12"></div>
                                                <br/>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success"><i class="icon-cloud-upload"></i>&nbsp;Update</button>
                                                    &nbsp;&nbsp;<a href="<?php echo site_url('admin'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                </div>
                                            </form>
                                            <div class="space-8"></div>

                                        </div>
                                    </div>
                                </div>


                                <!-- PAGE CONTENT ENDS -->
                            </div>																			
                        </div>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->        
        <?php echo $this->load->view($footer); ?>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
        <script type="text/javascript">
                                                $(".chosen-select").chosen({width: "50%"});
                                                $(".bootbox-confirm").click(function ()
                                                {
                                                    $('#mymodal').modal('show')
                                                });
                                                function text(idd)
                                                {
                                                    //var text=$("#login_content2").html();
                                                    //alert(idd);
                                                    var text = $("#" + idd + "2").html();
                                                    $("#" + idd).val(text);
                                                    return true;
                                                }
        </script>
        <script type="text/javascript">
            jQuery(function ($) {

                function showErrorAlert(reason, detail) {
                    var msg = '';
                    if (reason === 'unsupported-file-type') {
                        msg = "Unsupported format " + detail;
                    }
                    else {
                        console.log("error uploading file", reason, detail);
                    }
                    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                }

                //$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

                //but we want to change a few buttons colors for the third style
                $('.editor1').ace_wysiwyg({
                    toolbar:
                            [
                                'font',
                                null,
                                'fontSize',
                                null,
                                {name: 'bold', className: 'btn-info'},
                                {name: 'italic', className: 'btn-info'},
                                {name: 'strikethrough', className: 'btn-info'},
                                {name: 'underline', className: 'btn-info'},
                                null,
                                {name: 'insertunorderedlist', className: 'btn-success'},
                                {name: 'insertorderedlist', className: 'btn-success'},
                                {name: 'outdent', className: 'btn-purple'},
                                {name: 'indent', className: 'btn-purple'},
                                null,
                                {name: 'justifyleft', className: 'btn-primary'},
                                {name: 'justifycenter', className: 'btn-primary'},
                                {name: 'justifyright', className: 'btn-primary'},
                                {name: 'justifyfull', className: 'btn-inverse'},
                                null,
                                {name: 'createLink', className: 'btn-pink'},
                                {name: 'unlink', className: 'btn-pink'},
                                null,
                                {name: 'insertImage', className: 'btn-success'},
                                null,
                                'foreColor',
                                null,
                                {name: 'undo', className: 'btn-grey'},
                                {name: 'redo', className: 'btn-grey'}
                            ],
                    'wysiwyg': {
                        fileUploadError: showErrorAlert
                    }
                }).prev().addClass('wysiwyg-style2');



                $('#editor2').css({'height': '200px'}).ace_wysiwyg({
                    toolbar_place: function (toolbar) {
                        return $(this).closest('.widget-box').find('.widget-header').prepend(toolbar).children(0).addClass('inline');
                    },
                    toolbar:
                            [
                                'bold',
                                {name: 'italic', title: 'Change Title!', icon: 'icon-leaf'},
                                'strikethrough',
                                null,
                                'insertunorderedlist',
                                'insertorderedlist',
                                null,
                                'justifyleft',
                                'justifycenter',
                                'justifyright'
                            ],
                    speech_button: false
                });


                $('[data-toggle="buttons"] .btn').on('click', function (e) {
                    var target = $(this).find('input[type=radio]');
                    var which = parseInt(target.val());
                    var toolbar = $('#editor1').prev().get(0);
                    if (which == 1 || which == 2 || which == 3) {
                        toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
                        if (which == 1)
                            $(toolbar).addClass('wysiwyg-style1');
                        else if (which == 2)
                            $(toolbar).addClass('wysiwyg-style2');
                    }
                });




                //Add Image Resize Functionality to Chrome and Safari
                //webkit browsers don't have image resize functionality when content is editable
                //so let's add something using jQuery UI resizable
                //another option would be opening a dialog for user to enter dimensions.
                if (typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase())) {

                    var lastResizableImg = null;
                    function destroyResizable() {
                        if (lastResizableImg == null)
                            return;
                        lastResizableImg.resizable("destroy");
                        lastResizableImg.removeData('resizable');
                        lastResizableImg = null;
                    }

                    var enableImageResize = function () {
                        $('.wysiwyg-editor')
                                .on('mousedown', function (e) {
                                    var target = $(e.target);
                                    if (e.target instanceof HTMLImageElement) {
                                        if (!target.data('resizable')) {
                                            target.resizable({
                                                aspectRatio: e.target.width / e.target.height,
                                            });
                                            target.data('resizable', true);

                                            if (lastResizableImg != null) {//disable previous resizable image
                                                lastResizableImg.resizable("destroy");
                                                lastResizableImg.removeData('resizable');
                                            }
                                            lastResizableImg = target;
                                        }
                                    }
                                })
                                .on('click', function (e) {
                                    if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                                        destroyResizable();
                                    }
                                })
                                .on('keydown', function () {
                                    destroyResizable();
                                });
                    }

                    enableImageResize();

                    /**
                     //or we can load the jQuery UI dynamically only if needed
                     if (typeof jQuery.ui !== 'undefined') enableImageResize();
                     else {//load jQuery UI if not loaded
                     $.getScript($path_assets+"/js/jquery-ui-1.10.3.custom.min.js", function(data, textStatus, jqxhr) {
                     if('ontouchend' in document) {//also load touch-punch for touch devices
                     $.getScript($path_assets+"/js/jquery.ui.touch-punch.min.js", function(data, textStatus, jqxhr) {
                     enableImageResize();
                     });
                     } else	enableImageResize();
                     });
                     }
                     */
                }


            });
        </script>
    </body>

</body>
</html>
