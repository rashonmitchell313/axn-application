<!DOCTYPE html>

<html lang="en">

    <head>

        <title>

            <?php
            if ($this->uri->segment(0) != '') {

                echo 'Edit Email Contact ARN Fact Book | Admin ' . $this->uri->segment(0);
            } else {

                echo 'Edit Email Contact ARN Fact Book | Admin';
            }
            ?>

        </title>

        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.full.min.css" />

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.css" />

        <?php echo $this->load->view($header); ?>

    </head>

    <body>

        <?php echo $this->load->view($topnav); ?>

        <div class="main-container" id="main-container">

            <div class="main-container-inner">

                <a class="menu-toggler" id="menu-toggler" href="#">

                    <span class="menu-text"></span>

                </a>

                <?php echo $this->load->view($leftnav); ?>	

                <div class="main-content">

                    <?php echo $this->load->view($breadcrumb); ?>

                    <div class="page-content">

                        <div class="col-sm-12 getmsg"><?php $this->common->getmessage(); ?></div>
                        <div class="message"> </div>

                        <div class="row">

                            <div class="col-xs-12 col-sm-12">

                                <div class="row">

                                    <div class="widget-box">

                                        <div class="widget-header header-color-blue2">

                                            <h4 class="lighter smaller">Edit Email Content</h4>

                                        </div>

                                        <div class="error"><?php echo $this->common->getmessage(); ?></div>

                                        <div class="widget-body">

                                            <div class="widget-main">

                                                <form onSubmit="return validate_form()" action="<?php echo site_url('admin/email_content/' . $this->uri->segment(3)); ?>" method="post" id="id-message-form" class="form-horizontal">

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-recipient">Email Section:</label>
                                                        <div class="col-sm-6">
                                                            <select  id="content_id"  class="chosen-select col-sm-12" placeholder="Select an Option">
                                                                <option value="">Select an Option</option>
                                                                <?php foreach ($email_temp as $user): ?>
                                                                    <option value="<?php echo $user[0]; ?>" <?php if ($this->uri->segment(3) == $user[0]) { ?> selected="selected" <?php } ?>><?php echo ucwords($user[1]); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-subject">Required Words:</label>
                                                        <div class="col-sm-6 col-xs-6">
                                                            <div class="input-icon block col-xs-12 no-padding">
                                                                <input id="nrwords" maxlength="100" type="text" class="col-xs-12" value="<?php
                                                                            if ($this->uri->segment(3) != '') {
                                                                                echo $email_temp_t['n_remove_words'];
                                                                            }
                                                                            ?>"> 
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Message: <span class="red">*</span></label>                                    
                                                        <div class="col-sm-9">
                                                            <div class="widget-box">
                                                                <div class="widget-header widget-header-small  header-color-blue">
                                                                    <div class="widget-toolbar">
                                                                        <a href="javascript:void(0)" data-action="collapse">
                                                                            <i class="icon-chevron-up"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-body">
                                                                    <div class="widget-main no-padding">
                                                                        <div class="wysiwyg-editor" id="editor"><?php
                                                                            if ($this->uri->segment(3) != '') {
                                                                                echo $email_temp_t['email_temp_text'];
                                                                            }
                                                                            ?></div>
                                                                        <textarea style="display:none;" id="message" name="message" class="col-xs-10 col-sm-5"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space"></div>
                                                    <div class="clearfix form-actions">
                                                        <div class="col-md-offset-3 col-md-6"> 
                                                            <div id="id-message-item-navbar">
                                                                <div class="message-bar"></div>
                                                                <span class="inline btn-send-message">
                                                                    <button type="submit" class="btn btn-sm btn-success no-border" id="save" data-toggle="tooltip" title='Do Not Remove those words which have "#" sign. Further Email not containe exact data.' >
                                                                        <span class="bigger-110">Save Changes</span>

                                                                        <i class="icon-ok icon-on-right"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--</div>-->
                                                </form>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="vspace-sm"></div>

                                    <!-- /span -->

                                </div><!-- /row -->

                                <!-- PAGE CONTENT ENDS -->

                            </div><!-- /.col -->

                        </div><!-- /.row -->	

                    </div><!-- /.page-content -->

                </div><!-- /.main-content -->

            </div><!-- /.main-container-inner -->

            <a href="javascript:void(0)" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">

                <i class="icon-double-angle-up icon-only bigger-110"></i>

            </a>

        </div><!-- /.main-container -->

        <script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/jquery-2.0.3.min.js"></script>
        <?php echo $this->load->view($footer); ?>       	

        <script src="<?php echo base_url('assets'); ?>/js/bootstrap-wysiwyg.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/jquery.hotkeys.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/chosen.jquery.min.js"></script>

        <script type="text/javascript">


                                                    $('button[data-toggle="tooltip"]').tooltip({
                                                        animated: 'fade',
                                                        placement: 'top',
                                                    });
                                                    
                                                    $('#nrwords').prop('readonly', true);

                                                    $(".chosen-select").chosen({width: "41.5%"});


                                                    $('#content_id').change(function () {

                                                        var ecid = $('#content_id option:selected').val();

                                                        window.location = "<?php echo site_url('admin/email_content'); ?>" + '/' + ecid;

                                                    });

                                                    $('#editor').ace_wysiwyg({
                                                        toolbar_place: function (toolbar) {
                                                            return $(this).closest('.widget-box').find('.widget-header').prepend(toolbar).children(0).addClass('inline');
                                                        },
                                                        toolbar:
                                                                [
                                                                    'bold',
                                                                    {name: 'italic', title: 'Change Title!', icon: 'icon-leaf'},
                                                                    'strikethrough',
                                                                    null,
                                                                    'insertunorderedlist',
                                                                    'insertorderedlist',
                                                                    null,
                                                                    'justifyleft',
                                                                    'justifycenter',
                                                                    'justifyright'
                                                                ],
                                                        speech_button: false
                                                    }).prev().addClass('wysiwyg-style2');


                                                    $('#myform').on('reset', function () {
                                                        $('#editor').empty();
                                                    });

                                                    function validate_form()
                                                    {
                                                        var message = $("#editor").html();
                                                        if (message == "")
                                                        {
                                                            $(".message").html('<div class="alert alert-danger">You cannot send empty message.</div>');
                                                            $(".message").children().attr('tabindex', 0).delay(3000).fadeOut();
                                                            return false;
                                                        }

                                                        $("#save").html('<i class="fa fa-spinner fa-spin"></i> Saving');
                                                        $("#message").val(message);
                                                        return true;
                                                    }

                                                    $('.getmsg').children().attr('tabindex', 0).delay(3000).fadeOut();

        </script>  

    </body>
</html>