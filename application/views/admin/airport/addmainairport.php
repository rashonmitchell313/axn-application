<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Add Main Airport | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'Add Main Airport | ARN Fact Book';
            }
            ?>
        </title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/choseen.css" />
        <?php echo $this->load->view($header); ?>
    </head>
    <body>
        <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="javascript:void(0)">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>                    
                    <div class="page-content">                   
                        <div class="row">                        
                            <div class="col-xs-12">
                                <div class="error">
                                    <?php echo $this->common->getmessage(); ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-warning alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                Please use this page to add a new Airport.</div>
                                        </div>
                                    </div>   
                                </div>
                                <!--<form method="post" name="form1" class="form-horizontal" onSubmit="return checkval();" action="<?php // echo site_url('admin/airport/add_main_airport');   ?>">-->                    	
                                <form method="post" name="form1" class="form-horizontal" id="form1" data-toggle="validator" role="form" action="<?php echo site_url('admin/airport/add_main_airport'); ?>">                    	

                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        IATA:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="col-xs-10 col-sm-10" name="IATA" type="text" id="IATA" size="50" maxlength="50" data-error="IATA is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Airport Name:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="col-xs-10 col-sm-10"  name="aname" id="aname"  size="50" maxlength="50" data-error="Airport Name is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Category:(Eg:A , B, C):&nbsp;<span class="red">*</span>
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="category" type="text" id="category" size="40" maxlength="10" data-error="Category is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        City:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="acity" type="text" id="acity" size="40" maxlength="50" data-error="City is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group control-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        State:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">                            	
                                                        <select  name="astate" id="astate" class="chosen-select col-xs-10 col-sm-10" data-error="State is invalid" required>
                                                            <option value="">Select an Option</option>
                                                            <?php
                                                            foreach ($rsStates as $key => $state):
                                                                ?>

                                                                <option value="<?php echo $state['state_code']; ?>">
                                                                    <?php echo $state['state_name']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-8 pull-right help-block with-errors"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Country:&nbsp;
                                                    </label>
                                                    <div class="col-sm-8">                            	
                                                        <select name="acountry" size="1" id="acountry" class="chosen-select col-xs-10 col-sm-10">
                                                            <?php
                                                            foreach ($row_rsCountries as $country):
                                                                ?>

                                                                <option value="<?php echo strtoupper($country['countries_name']); ?>"<?php
                                                                if (!(strcmp($country['countries_name'], 'United States'))) {
                                                                    echo "selected=\"selected\"";
                                                                }
                                                                ?>>
                                                                            <?php echo $country['countries_name']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Website:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="awebsite" type="text" id="awebsite" size="40" maxlength="50">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Configuration:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="configuration" type="text" id="configuration" size="40" maxlength="50">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Mgtstructure:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"   name="mgtstructure" type="text" id="mgtstructure" size="40" maxlength="50">
                                                    </div>
                                                </div>
                                            </td>	
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="mclone">
                                                    <div class="space-4"></div> 
                                                    <div class="form-group">
                                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                            Terminal Name:
                                                        </label>
                                                        <div class="col-sm-8">
                                                            <input class="col-xs-10 col-sm-10"  name="terminalname[]" type="text" size="50" maxlength="100">
                                                            &nbsp;<button type="button" id="addmoreterminal" title="Add more Terminal Option" class="btn btn-xs btn-success"><i class="icon-plus"></i>Add</button> 
                                                        </div>
                                                    </div>
                                                    <div class="space-4"></div> 
                                                    <div class="form-group">
                                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                            Terminal Abbr:
                                                        </label>
                                                        <div class="col-sm-8">
                                                            <input class="col-xs-10 col-sm-10"  name="terminalabbr[]"  type="text" size="50" maxlength="100">                          </div>
                                                    </div> 
                                                </div>
                                                <div class="terminla"> 
                                                </div> 
                                            </td>
                                        </tr>
<!--                                        <tr>
                                            <td>
                                                <div class="form-group">                          
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Published&nbsp;:
                                                    </label>                       
                                                    <div class="col-sm-8">
                                                        <input type="checkbox" class="ace ace-switch ace-switch-7" name="published" >
                                                        <span class="lbl"></span>
                                                        <span class="red">(Checking this to set the Airport is Published or Not.)</span>									
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">                          
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Approved&nbsp;:
                                                    </label>                       
                                                    <div class="col-sm-8">
                                                        <input type="checkbox" class="ace ace-switch ace-switch-7" name="approved" >
                                                        <span class="lbl"></span>
                                                        <span class="red">(Checking this to set the Airport is Approved or Not.)</span>									
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>-->
                                        <tr>
                                            <td>
                                                <input type="hidden" name="MM_update" value="form1">
                                                <!--                       <div class="form-group">
                                                                                <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                                    &nbsp; 
                                                                                </label>
                                                                                <div class="col-sm-8">
                                                       <button type="submit" class="btn btn-success" id="checkValidation"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php echo site_url('admin/list_of_users'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                                                </div>
                                                                        </div>-->


                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-3 control-label no-padding-right"> 
                                                        &nbsp; 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <!--<button type="submit" onClick="MM_validateForm('IATA', '', 'R', 'aname', '', 'R', 'category', '', 'R', 'acity', '', 'R', 'astate', '', 'R'); return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php //echo site_url('admin/list_of_users');    ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>-->
                                                        <button type="submit" name="add_airport" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php //echo site_url('admin/list_of_users');    ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--<div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              IATA:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input type="text" class="col-xs-10 col-sm-5" name="IATA" type="text" id="IATA" size="50" maxlength="50" >
                                          </div>
                                      </div>
                                      
                                      <div class="space-4"></div>   
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Airport Name:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5"  name="aname" type="text" id="aname"  size="50" maxlength="50">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Category:(Eg:A , B, C):&nbsp;<span class="red">*</span>
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="category" type="text" id="category" size="40" maxlength="10">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> 
                                      <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              City:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input class="col-xs-10 col-sm-5" name="acity" type="text" id="acity" size="40" maxlength="50">
                                          </div>
                                      </div>                 -->           
                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             State:&nbsp;<span class="red">*</span> 
                                        </label>
                                        <div class="col-sm-8">                            	
                                            <select  name="astate">
                                    <?php
//foreach($rsStates  as $key =>  $state):											
                                    ?>
                                                        
                                                            <option value="<?php //echo $state['states_id'];          ?>">
                                    <?php //echo $state['state_name']; ?>
                                                            </option>
                                    <?php
                                    //	endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="space-4"></div>  -->
                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             Country:&nbsp;
                                        </label>
                                        <div class="col-sm-8">                            	
                                            <select name="acountry" size="1" id="acountry">
                                    <?php
//foreach($row_rsCountries as $country):											
                                    ?>
                                                        
                                                            <option value="<?php ////echo $country['countries_id'];          ?>">
                                    <?php ///echo $country['countries_name']; ?>
                                                            </option>
                                    <?php
                                    //endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>-->
                                    <!--<div class="space-4"></div> 
                                 <div class="form-group">
                                      <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                          Website:
                                      </label>
                                      <div class="col-sm-8">
                                       <input class="col-xs-10 col-sm-5"  name="awebsite" type="text" id="awebsite" size="40" maxlength="50">
                                      </div>
                                  </div>
                                  <div class="space-4"></div> -->
                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Configuration:
                                        </label>
                                        <div class="col-sm-8">
                                         <input class="col-xs-10 col-sm-5" name="configuration" type="text" id="configuration" size="40" maxlength="50">
                                        </div>
                                    </div>
                                    <div class="space-4"></div> -->
                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Mgtstructure:
                                        </label>
                                        <div class="col-sm-8">
                                         <input class="col-xs-10 col-sm-5"   name="mgtstructure" type="text" id="mgtstructure" size="40" maxlength="50">
                                        </div>
                                    </div>-->
                                    <!--<div class="mclone">
                                        <div class="space-4"></div> 
                                        <div class="form-group">
                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                Terminal Name:
                                            </label>
                                            <div class="col-sm-8">
                                             <input class="col-xs-10 col-sm-5"  name="terminalname[]" type="text" size="50" maxlength="100">
                                              &nbsp;<button type="button" id="addmoreterminal" title="Add more Terminal Option" class="btn btn-xs btn-success"><i class="icon-plus"></i>Add</button>
                                            </div>
                                        </div>
                                        <div class="space-4"></div> 
                                        <div class="form-group">
                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                Terminal Abbr:
                                            </label>
                                            <div class="col-sm-8">
                                             <input class="col-xs-10 col-sm-5"  name="terminalabbr[]"  type="text" size="50" maxlength="100">                          </div>
                                        </div> 
                                    </div>-->

<!--                       <input type="hidden" name="MM_update" value="form1">
<div class="form-group">
<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> 
&nbsp; 
</label>
<div class="col-sm-8">
<button type="submit" onClick="MM_validateForm('IATA','','R','aname','','R','acountry','','R','city','','R','category','','R');return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Add</button>&nbsp;&nbsp;<a href="<?php //echo site_url('admin/list_of_users');          ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
</div>
</div>-->
                                    <div class="space-4"></div> 
                                </form> 


                            </div>			
                        </div>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="javascript:void(0)" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
        <?php echo $this->load->view($footer); ?> 
        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>
        <script type="text/javascript">


                $("#astate").prepend("<option value='' selected='selected'>&nbsp;</option>").trigger('chosen:updated');

                $('#form1').validator();
                $('#form1').validator({
                    rules: {
                        astate: {
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.control-group').removeClass('success').addClass('error').trigger('chosen:updated');
//                                                                        $('#categoryide').trigger('chosen:updated');
                    },
                    success: function (element) {
                        element.text('OK!').addClass('valid')
                                .closest('.control-group').removeClass('error').addClass('success').trigger('chosen:updated');
                    }
                });




                function checkval()

                {

                    var rfield = new Array("IATA", "aname", "category", "acity", "astate");

                    var msg = new Array('IATA', 'Airport Name', 'Airport Category', 'Airport City', 'Airport State');

                    var errmsg = "";

                    for (i = 0; i < rfield.length; i++)

                    {

                        //alert(rfield[i]);

                        var val = document.getElementsByName("" + rfield[i] + "")[0].value;

                        if (val == "" || val.replace(" ", "") == "")

                        {

//                                                errmsg += "<b><i>" + msg[i] + " is Required. </i></b><br/>";
                            errmsg += "" + msg[i] + " is Required. <br/>";


                        }

                    }

                    if (errmsg != "")

                    {

                        $(".error").html("<div class='alert alert-danger'>" + errmsg + "</div>");

                        $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

                        return false;

                    }

                    return true;

                }


//                                                            function titleCase(nm) {
//                                                                var titleStr = nm.split(' ');
//                                                                for (var i = 0; i < titleStr.length; i++) {
//                                                                    titleStr[i] = titleStr[i].charAt(0).toUpperCase() + titleStr[i].slice(1).toLowerCase();
//                                                                }
//                                                                return titleStr.join(' ');
//                                                            }

                $(".chosen-select").chosen({width: "83.4%",
                    search_contains: true});
                /* function MM_validateForm() { //v4.0
                 
                 if (document.getElementById) {
                 
                 var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
                 
                 for (i = 0; i < (args.length - 2); i += 3) {
                 test = args[i + 2];
                 val = document.getElementById(args[i]);
                 
                 if (val) {
                 nm = val.name;
                 if ((val = val.value) != "") {
                 
                 if (test.indexOf('isEmail') != -1) {
                 p = val.indexOf('@');
                 
                 if (p < 1 || p == (val.length - 1))
                 errors += '- ' + nm + ' must contain an e-mail address.\n';
                 
                 } else if (test != 'R') {
                 num = parseFloat(val);
                 
                 if (isNaN(val))
                 errors += '- ' + nm + ' must contain a number.\n';
                 
                 if (test.indexOf('inRange') != -1) {
                 p = test.indexOf(':');
                 
                 min = test.substring(8, p);
                 max = test.substring(p + 1);
                 
                 if (num < min || max < num)
                 errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                 
                 }
                 }
                 } else if (test.charAt(0) == 'R')
                 //                                                                                errors += '- ' + nm + ' is required.<br />';
                 
                 var test1 = titleCase(nm.replace("_", " "))
                 
                 var mapObj = {
                 Aname: "Airport Name",
                 Acity: "Airport City",
                 Astate: "Airport State"
                 
                 };
                 var re = new RegExp(Object.keys(mapObj).join("|"), "gi");
                 test1 = test1.replace(re, function (matched) {
                 return mapObj[matched];
                 });
                 
                 errors += test1 + ' is required.<br />  ';
                 }
                 
                 }
                 
                 if (errors) {
                 //                                                                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors.replace("_", " ").toUpperCase() + "</div></div></div>");
                 //                                                                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors.replace("_", " ").charAt(0).toUpperCase() + errors.substr(1).toLowerCase() + "</div></div></div>");
                 $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:<br />' + errors + "</div></div></div>");
                 console.log(MM_validateForm('.errors'));
                 $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');
                 }
                 
                 
                 document.MM_returnValue = (errors == '');
                 
                 }
                 } */
                /*$("#addmoreterminal").click(function()
                 {
                 $("#remove").show();
                 var clone=$(".mclone").html();				
                 $(".terminla").append(clone);
                 
                 });
                 $("#remove").click(function()
                 {
                 
                 $( "div" ).remove( ".mclone" );
                 
                 });*/
                var x = 1;
                var max_fields = 50;
                var wrapper = $(".mclone");
                var add_button = $("#addmoreterminal");
                $(add_button).click(function (e) {
                    e.preventDefault();
                    if (x < max_fields) {
                        x++;
                        $(wrapper).append('<div><div class="form-group"><label for="form-field-1" class="col-sm-4 control-label no-padding-right">Terminal Name:</label><div class="col-sm-8"><input type="text" maxlength="100" class="col-xs-10 col-sm-10" name="terminalname[]" size="50" /></div></div><div class="form-group"><label for="form-field-1" class="col-sm-4 control-label no-padding-right">Terminal Abbr:</label><div class="col-sm-8"><input type="text" maxlength="100" size="50" class="col-xs-10 col-sm-10"  name="terminalabbr[]" /></div></div><button style="margin:-46px -21px 0px 0px;" class="remove_field btn btn-xs btn-danger pull-right col-md-offset-1"><i class="icon-remove"></i> Remove</button></div>');
                    }
                });
                $(wrapper).on("click", ".remove_field", function (e) {
                    e.preventDefault();
                    $(this).parent('div').remove();
                    x--;
                });
                $("#checkValidation").click(function () {
                    var IATA = $("#IATA").val();
                    var category = $("#category").val();
                    var aname = $("#aname").val();
                    var acity = $("#acity").val();
                    var astate = $("#astate").val();
                    //				 if(IATA == '' && category == '' && aname == '' && acity == '' && astate == '')
                    //				 {
                    //					 $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">(*) Indicated that field is required.</div></div></div>');
                    //					 return false;
                    //				 }
                    if (IATA == '')
                    {
                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">IATA is required.</div></div></div>');
                        return false;
                    }
                    if (category == '')
                    {
                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">Category is required.</div></div></div>');
                        return false;
                    }
                    if (aname == '')
                    {
                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">Airport Name is required.</div></div></div>');
                        return false;
                    }
                    if (acity == '')
                    {
                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">City is required.</div></div></div>');
                        return false;
                    }
                    if (astate == '')
                    {
                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">State is required.</div></div></div>');
                        return false;
                    }
                    return true;

                });
        </script>  

    </body>
</html>
