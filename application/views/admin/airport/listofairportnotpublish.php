<!DOCTYPE html>

<html lang="en">

    <head>

        <title>

            <?php
//            if ($this->uri->segment(0) != '') {
//
//                echo 'List of not published Airports | ARN Fact Book ' . $this->uri->segment(0);
//            } else {
//
//                echo 'List of not published Airports | ARN Fact Book';
//            }
            ?>

        </title>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('fassests'); ?>/css/dataTables.tableTools.css">

        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.responsive.css" >

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.bootstrap.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/jquery-ui-1.10.3.full.min.css" >

<?php echo $this->load->view($header); ?>



    </head>

    <body>

<?php echo $this->load->view($topnav); ?>

        <div class="main-container" id="main-container">



            <div class="main-container-inner">

                <a class="menu-toggler" id="menu-toggler" href="javascript:void(0)">

                    <span class="menu-text"></span>

                </a>

<?php echo $this->load->view($leftnav); ?>  

                <div class="main-content">

                <?php echo $this->load->view($breadcrumb); ?>                    

                    <div class="page-content">

                        <div class="row">                       

                            <div class="col-xs-12">                                     

<?php if ($this->common->GetSessionKey('airportnotpublishsearch') != "") { ?>

                                    <div class="alert alert-success">

                                        <strong>(<?php echo $total ?>)</strong> Airport has been found.

                                        <form action="<?php echo site_url('airport/clearsearchnotpub'); ?>" method="post">

                                            <button  class="cleansearch btn btn-danger btn-sm" name="submit" >

                                                <i class="fa fa-trash"></i>&nbsp;Clear Search</button>

                                        </form>

                                    </div>

<?php } ?>

                                <div class="error"><?php echo $this->common->getmessage(); ?></div>

                                <div class="table-header">

                                    List of Non Published Airports

                                </div>

                                <div class="table-responsive">

                                    <table width="100%" id="sample-table-2" class="table table-striped table-bordered table-hover datatable">   <thead>

                                            <tr>                                    

                                                <th><input type="checkbox"  name="bla" id="selecctall"/></th>

                                                <th>IATA</th>

                                                <th>Airport Name</th>

                                                <th>Last Modified</th>                                    

                                                <th>Published</th>

                                                <th>Approved</th>

                                                <th>AID</th>
                                                
                                                <th>Category</th>

                                                <th>PDF Export</th>                                    

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody>

<?php /* ?>   <?php  foreach($listofairport as $airport): ?>

  <tr>



  <td><?php echo $airport['IATA'];?></td>

  <td><?php echo $airport['aname'];?></td>

  <td><?php echo $airport['lastmodified'];?></td>                                    <td><?php echo $airport['published'];?></td>

  <td><?php echo $airport['approved'];?></td>

  <td><?php echo $airport['aid']; ?></td>

  <td><input type="checkbox" name="aaid[]" value="<?php echo $airport['aid']; ?>"></td>

  <td>

  <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">



  <a class="green" href="<?php echo site_url('admin/airport/edit_airport/'.$airport['aid']);?>">

  <i class="icon-pencil bigger-130"></i>

  </a>



  <a class="bootbox-confirm id-btn-dialog2" href="" aid="<?php echo $airport['aid'];?>">

  <i class="icon-trash bigger-130"></i>

  </a>

  </div>



  <div class="visible-xs visible-sm hidden-md hidden-lg">

  <div class="inline position-relative">

  <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">

  <i class="icon-caret-down icon-only bigger-120"></i>

  </button>



  <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">





  <li>

  <a href="<?php echo site_url('admin/airport/edit_airport/'.$airport['aid']);?>" class="tooltip-success" data-rel="tooltip" title="Edit">

  <span class="green">

  <i class="icon-edit bigger-120"></i>

  </span>

  </a>

  </li>



  <li>

  <a href="#" class="tooltip-error bootbox-confirm id-btn-dialog2" data-rel="tooltip" title="Delete" aid="<?php echo $airport['aid'];?>">

  <span class="red">

  <i class="icon-trash bigger-120"></i>

  </span>

  </a>

  </li>

  </ul>

  </div>

  </div>

  </td>

  </tr>

  <?php endforeach; ?><?php */ ?>

                                        </tbody>

                                    </table>

                                    <br>

                                    <div class="row">

                                        <div class="col-sm-1">

                                            <a href="javascript:void(0)" class="btn btn-danger btn-sm" id="getcheck"><i class="icon-trash bigger-120"></i> Delete</a>

                                        </div>

                                        <div class="col-sm-6" style="display: none;">

                                            <button type="button" class="btn btn-success pull-left export-to-pdf">

                                                <i class="icon-download"></i>Export pdf Of Airports                                     

                                            </button></div> 

                                        <div class="col-sm-6">

                                            <div class="pull-right">

<?php /* ?><?php echo $this->common->getpagination(site_url('admin/'.$this->uri->segment(2)),$total);?><?php */ ?>                                  

                                            </div><!-- /span -->

                                        </div>

                                    </div>

<?php /* ?><div class="row">

  <div class="col-sm-6">&nbsp;</div>

  <div class="col-sm-6">

  <div class="pull-right">

  <?php echo $this->common->getpagination(site_url('admin/airport/list_of_airport_not_published'),$total);?>                                    </div>

  </div><!-- /span -->

  </div><?php */ ?>

                                </div>

                            </div>

                        </div>

                    </div><!-- /.page-content -->

                </div><!-- /.main-content -->

                <div id="dialog-confirm" class="hide" >

                    <div class="alert alert-info bigger-110" style="height: auto; width: auto; left: 603px; display: block; top: 250px;">

                        These items will be permanently deleted and cannot be recovered.

                    </div>    

                    <div class="space-6"></div>    

                    <p class="bigger-110 bolder center grey">

                        <i class="icon-hand-right blue bigger-120"></i>

                        Are you sure?

                    </p>

                </div>

                <div id="dialog-select" class="hide" >

                    <div class="alert alert-info bigger-110" style="height: auto; width: auto; left: 603px; display: block; top: 250px;">

                        Please check at least one checkbox.



                    </div>

                </div> 

            </div><!-- /.main-container-inner -->

            <a href="javascript:void(0)" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">

                <i class="icon-double-angle-up icon-only bigger-110"></i>

            </a>

        </div><!-- /.main-container -->        

<?php echo $this->load->view($footer); ?>

        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTablees.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.bootstrap.js"></script>

        <script src="<?php echo base_url('fassests'); ?>/js/dataTables.tableTools.js" type="text/javascript"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.full.min.js"></script>

        <script type="text/javascript">

            var seg = "<?php echo $this->uri->segment(0) ?>";

            if (seg != '') {
                $('title').html("List of not published Airports | ARN Fact Book " + seg);
            }
            else {
                $('title').html("List of not published Airports | ARN Fact Book");
            }

            $(document).ready(function (e) {

                $('#sample-table-2').dataTable({
                    responsive: true,
                    "processing": true,
                    "serverSide": true,
                    aoColumnDefs: [
                        {"aTargets": [0], "bSortable": false},
                        {"aTargets": [8], "bSortable": false},
                        {"aTargets": [9], "bSortable": false}

                    ],
                    "ajax": "<?php echo site_url('ajax/list_of_not_published_airports_pagination'); ?>",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets/tabletools'); ?>/swf/copy_csv_xls_pdf.swf"

                    },
                    //"bProcessing": true,

                    "oLanguage": {
                        "sProcessing": "<i class='fa fa-spinner fa-3x fa-spin'></i>"

                    }



                });

                var dtable = $(".datatable").dataTable().api();

                $(".dataTables_filter input")

                        .unbind()

                        .bind("input", function (e) {

//                            if (this.value.length >= 3 || e.keyCode == 13) {
                            if (this.value.length >= 1 || e.keyCode == 13) {



                                dtable.search(this.value).draw();

                            }



                            if (this.value == "") {

                                dtable.search("").draw();

                            }

                            return;

                        });

                $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
                    console.log(message);
                };

                $('#selecctall').click(function (event) {

                    if (this.checked) {

                        $('.checkboxsingle').each(function () {

                            this.checked = true;

                        });

                    } else {

                        $('.checkboxsingle').each(function () {

                            this.checked = false;

                        });

                    }

                });

            });





            $('#getcheck').click(function () {



                var myCheckboxes = new Array();

                $("input:checked").each(function () {

                    myCheckboxes.push($(this).val());

                });

                datastring = "myCheckboxes=" + myCheckboxes;

                if ($('.checkboxsingle:checked').length > 0) {

                    $("#dialog-confirm").removeClass('hide').dialog({
                        resizable: false,
                        modal: true,
                        title: "Are you sure?",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
                                "class": "btn btn-danger btn-xs",
                                click: function ()

                                {

                                    $(this).dialog("close");

                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo site_url('admin/deleteallnotpublishairports'); ?>",
                                        data: datastring,
                                        success: function (response)

                                        {

                                            if (response == 1)

                                            {

                                                var dtable = $(".datatable").dataTable().api();

                                                dtable.ajax.reload();

                                            }

                                            else

                                            {

                                                showerror(response);

                                            }

                                        }

                                    });

                                }

                            },
                            {
                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
                                "class": "btn btn-xs",
                                click: function () {

                                    $(this).dialog("close");

                                    return false;

                                }

                            }

                        ]



                    });

                }

                else {

                    $("#dialog-select").removeClass('hide').dialog({
                        resizable: false,
                        modal: true,
                        title: "Confirmation",
                        title_html: true,
                        buttons: [
                            {
                                html: "OK",
                                "class": "btn btn-info btn-xs",
                                click: function ()

                                {

                                    $(this).dialog("close");

                                    return false;

                                }

                            }

                        ]

                    });

                }

            });

        </script>

        <script type="text/javascript">

            function showerror(error)

            {

                var h = '<div class="row"><div class="col-sm-12"><div class="alert alert-danger">' + error + '</div></div></div>';

                $(".error").html(h);

                $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');

            }

            $(".upsub").click(function ()

            {

                var user_id = $(this).attr("user_id");

                var subscription = $(this).attr("subscription");

                var datastring = "user_id=" + user_id + "&subscription=" + subscription + "&action=updatesubscription";

                if (subscription == 1 || subscription == 0 && user_id > 0)

                {

                    $.ajax({
                        url: "<?php echo site_url('ajax/doaction'); ?>",
                        type: "POST",
                        cache: false,
                        async: false,
                        data: datastring,
                        success: function (html)

                        {

                            if (html == 1)

                            {

                                window.location = "<?php echo site_url('admin/airport/list_of_airport_not_published'); ?>";

                            }

                        }

                    });

                } else

                {

                    showerror("Action has been failed.");

                }

                return false;

            });

            $(".export-to-pdf").on("click", function ()

            {

                updateselectlist();

                $(this).html("Wait.....");

                //var datastring="listOfAirport="+allVals+"&action=generatepdf";

                window.location = "<?php echo site_url('pdf/GenPdf'); ?>";

                /*$.ajax({
                 
                 url:"<?php //echo site_url('pdf/GenPdf');  ?>",
                 
                 cache:false,
                 
                 type:"POST",
                 
                 async:false,
                 
                 data:datastring,
                 
                 success: function(responsce)
                 
                 {
                 
                 if(responsce!=1)
                 
                 {
                 
                 showerror(responsce);  
                 
                 }
                 
                 }
                 
                 });*/

            });

        </script>   



        <script type="text/javascript">

            /*jQuery(function($)
             
             {
             
             $( ".id-btn-dialog2" ).on('click', function(e) 
             
             {
             
             var aid=$(this).attr('aid');
             
             var datastring="aid="+aid+"&action=deleteairport";             
             
             e.preventDefault();                    
             
             $( "#dialog-confirm" ).removeClass('hide').dialog({
             
             resizable: false,
             
             modal: true,
             
             title: "Are you sure?",
             
             title_html: true,
             
             buttons: [
             
             {
             
             html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
             
             "class" : "btn btn-danger btn-xs",
             
             click: function() 
             
             {
             
             $( this ).dialog( "close" );
             
             $.ajax({
             
             type:"POST",
             
             async:false,
             
             cache:false,
             
             url:"<?php //echo site_url('ajax/doaction');   ?>",
             
             data:datastring,
             
             success: function(responsce)
             
             {
             
             if(responsce==1)
             
             {
             
             window.location="<?php //echo site_url('admin/airport/list_of_airport_not_published');  ?>";
             
             } else 
             
             {
             
             showerror(responsce);  
             
             }
             
             }                                                                                  
             
             });                                
             
             }
             
             }
             
             ,
             
             {
             
             html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
             
             "class" : "btn btn-xs",
             
             click: function() {
             
             $( this ).dialog( "close" );
             
             return false;
             
             }
             
             }
             
             ]
             
             });
             
             return false;
             
             });
             
             
             
             
             
             });*/

        </script>   

        <script type="text/javascript">

            try {

                ace.settings.check('main-container', 'fixed')

            } catch (e) {

            }

        </script> 

        <script type="text/javascript">

            try {

                ace.settings.check('main-container', 'fixed')

            } catch (e) {

            }

        </script> 

        <script type="text/javascript">

            function myFunction(val) {



                var aid = val;

                var datastring = "aid=" + aid + "&action=deletenotpublishairport";

                var cateid = $(this).attr("value");

                $("#dialog-confirm").removeClass('hide').dialog({
                    resizable: false,
                    modal: true,
                    title: "Are you sure?",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
                            "id": "confo_del",
                            "class": "btn btn-danger btn-xs",
                            click: function ()

                            {

                                $(this).dialog("close");

                                $.ajax({
                                    type: "POST",
                                    async: false,
                                    cache: false,
                                    url: "<?php echo site_url('ajax/doaction'); ?>",
                                    data: datastring,
                                    success: function (responsce)

                                    {

                                        if (responsce == 1)

                                        {

                                            window.location = "<?php echo site_url('admin/airport/list_of_airport_not_published'); ?>";

                                        } else

                                        {

                                            showerror(responsce);

                                        }

                                    }

                                });

                            }

                        }

                        ,
                        {
                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
                            "class": "btn btn-xs",
                            click: function () {

                                $(this).dialog("close");

                                return false;

                            }

                        }

                    ]

                });

                return false;



            }</script>   

    </body>

</html>

