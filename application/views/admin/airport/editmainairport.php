<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Modify Airport | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'Modify Airport | ARN Fact Book';
            }
            ?>
        </title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/choseen.css" />
        <?php echo $this->load->view($header); ?>
    </head>
    <body>
        <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
                <?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
                    <?php echo $this->load->view($breadcrumb); ?>                    
                    <div class="page-content">                   
                        <div class="row">                        
                            <div class="col-xs-12">
                                <div class="error">
                                    <?php echo $this->common->getmessage(); ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-warning alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>Please use this page to Edit an Airport.				 </div>
                                        </div>
                                    </div>   
                                </div>
                                <?php
                                $id = $this->uri->segment(4);
                                ?>
                                <form method="post" name="form1" class="form-horizontal" id="form1" data-toggle="validator" role="form" action="<?php echo site_url('admin/airport/edit_airport') . '/' . $id; ?>">  				
                                    <input type="hidden" name="aid" value="<?php echo $airport_info[0]['aid']; ?>"> 
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        IATA:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <?php // var_dump($airport_info);exit;  ?>
                                                        <input type="text" class="col-xs-10 col-sm-10" name="IATA" type="text" id="IATA" size="50" maxlength="50" value="<?php echo $airport_info[0]['IATA']; ?>"  data-error="IATA is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Airport Name:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="aname" type="text" id="aname"  size="50" maxlength="50" value="<?php echo $airport_info[0]['aname']; ?>" data-error="Airport Name is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Category:(Eg:A , B, C):&nbsp;<span class="red">*</span>
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="category" type="text" id="category" size="40" maxlength="10" value="<?php echo $airport_info[0]['category']; ?>" data-error="Category is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        City:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input  class="col-xs-10 col-sm-10" name="acity" type="text" id="acity" size="40" maxlength="50" value="<?php echo $airport_info[0]['acity']; ?>" data-error="City is invalid" required>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        State:&nbsp;<span class="red">*</span> 
                                                    </label>
                                                    <div class="col-sm-8">                            	
                                                        <select class="chosen-select col-xs-10 col-sm-10" id="astate" name="astate" data-error="State is invalid" required>
                                                            <?php
                                                            foreach ($rsStates as $key => $state):
                                                                ?>

                                                                <option value="<?php echo $state['state_code']; ?>" <?php if ($airport_info[0]['astate'] == $state['state_code']) { ?> selected <?php } ?> >
                                                                    <?php echo $state['state_name']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6 help-block with-errors"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Country:&nbsp;
                                                    </label>
                                                    <div class="col-sm-8">                            	
                                                        <select class="chosen-select col-xs-10 col-sm-10" name="acountry" size="1" id="acountry">
                                                            <?php
                                                            foreach ($row_rsCountries as $country):
                                                                ?>

                                                                <option value="<?php echo strtoupper($country['countries_name']); ?>" <?php if ($airport_info[0]['acountry'] == strtoupper($country['countries_name'])) { ?> selected <?php } ?>>
                                                                    <?php echo $country['countries_name']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Website:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="awebsite" type="text" id="awebsite" size="40" maxlength="50" value="<?php echo $airport_info[0]['awebsite']; ?>">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Configuration:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10" name="configuration" type="text" id="configuration" size="40" maxlength="50" value="<?php echo $airport_info[0]['configuration']; ?>">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Mgtstructure:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"   name="mgtstructure" type="text" id="mgtstructure" size="40" maxlength="50" value="<?php echo $airport_info[0]['mgtstructure']; ?>">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Latitude:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="Latitude" type="text" id="Latitude" size="40" maxlength="50" value="<?php echo $airport_info[0]['Latitude']; ?>">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Longitude:
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <input class="col-xs-10 col-sm-10"  name="Longitude" type="text" id="Longitude" size="40" maxlength="50" value="<?php echo $airport_info[0]['Longitude']; ?>">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <?php
                                                if (count($termairportdet) > 0) {
                                                    foreach ($termairportdet as $term):
                                                        ?>
                                                        <div class="space-4"></div> 
                                                        <div class="form-group">
                                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                Terminal Name:
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <input class="col-xs-10 col-sm-10"  name="terminalname[]" type="text" size="50" maxlength="100" value="<?php echo $term['terminalname']; ?>">&nbsp;<button type="button" title="Remove Terminal" class="btn btn-xs btn-danger removethis pull-right" style="margin: 0px -17px 0px 0px;" tid="<?php echo $term['tid']; ?>"><i class="icon-trash"></i>Remove</button>
                                                                <input type="hidden" name="termid[]" value="<?php echo $term['tid']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="space-4"></div> 
                                                        <div class="form-group">
                                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                Terminal Abbr:
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <input class="col-xs-10 col-sm-10"  name="terminalabbr[]"  type="text" size="50" maxlength="100" value="<?php echo $term['terminalabbr']; ?>">                          </div>
                                                        </div>								
                                                    <?php endforeach; ?>
                                                    <div class="mclone">                        
                                                        <div class="space-4"></div> 
                                                        <div class="form-group">
                                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                Terminal Name:
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <input class="col-xs-10 col-sm-10"  name="terminalname[]" type="text" size="50" maxlength="100" >
                                                                &nbsp;<button type="button" id="addmoreterminal" title="Remove this Terminal Option" class="btn btn-xs btn-success"><i class="icon-plus"></i>Add</button>
                                                            </div>
                                                        </div>
                                                        <div class="space-4"></div> 
                                                        <div class="form-group">
                                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                Terminal Abbr:
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <input class="col-xs-10 col-sm-10"  name="terminalabbr[]"  type="text" size="50" maxlength="100">                   </div>
                                                        </div>                        
                                                    </div>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <div class="mclone">                        
                                                        <div class="space-4"></div> 
                                                        <div class="form-group">
                                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                Terminal Name:
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <input class="col-xs-10 col-sm-10"  name="terminalname[]" type="text" size="50" maxlength="100">
                                                                &nbsp;<button type="button" id="addmoreterminal" title="Add more Terminal Option" class="btn btn-xs btn-success"><i class="icon-plus"></i>Add</button>
                                                            </div>
                                                        </div>
                                                        <div class="space-4"></div> 
                                                        <div class="form-group">
                                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                                Terminal Abbr:
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <input class="col-xs-10 col-sm-10"  name="terminalabbr[]"  type="text" size="50" maxlength="100">                          </div>
                                                        </div>                        

                                                    </div>
                                                <?php }
                                                ?>
                                                <div class="terminla"> 
                                                </div>
                                                <input type="hidden" name="MM_update" value="form1">


                                                <div class="form-group">                          
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Published&nbsp;:
                                                    </label>                       
                                                    <div class="col-sm-8">
                                                        <input type="checkbox" class="ace ace-switch ace-switch-7" name="published" <?php
                                                        if (strcmp(htmlentities($airport_info[0]['published'], ENT_COMPAT, 'iso-8859-1'), 0)) {
                                                            echo "checked=\"checked\"";
                                                        }
                                                        ?>>
                                                        <span class="lbl"></span>									
                                                    </div>
                                                </div>

                                                <div class="form-group">                          
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        Approved&nbsp;:
                                                    </label>                       
                                                    <div class="col-sm-8">
                                                        <input type="checkbox" class="ace ace-switch ace-switch-7" name="approved" <?php
                                                        if (strcmp(htmlentities($airport_info[0]['approved'], ENT_COMPAT, 'iso-8859-1'), 0)) {
                                                            echo "checked=\"checked\"";
                                                        }
                                                        ?>>
                                                        <span class="lbl"></span>									
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                        &nbsp; 
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <button type="submit" id="update" onClick="MM_validateForm('IATA', '', 'R', 'aname', '', 'R', 'acountry', '', 'R', 'city', '', 'R');
                                                                return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php echo site_url('admin/airport'); ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--<div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              IATA:&nbsp;<span class="red">*</span> 
                                          </label>
                                          <div class="col-sm-8">
                                              <input type="text" class="col-xs-10 col-sm-5" name="IATA" type="text" id="IATA" size="50" maxlength="50" value="<?php //echo $airport_info[0]['IATA'];    ?>" >
                                          </div>
                                      </div>
                                      <div class="space-4"></div> -->  
                                    <!-- <div class="form-group">
                                         <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             Airport Name:&nbsp;<span class="red">*</span> 
                                         </label>
                                         <div class="col-sm-8">
                                             <input class="col-xs-10 col-sm-5"  name="aname" type="text" id="aname"  size="50" maxlength="50" value="<?php echo $airport_info[0]['aname']; ?>" >
                                         </div>
                                     </div>
                                     <div class="space-4"></div>  -->  
                                    <!-- <div class="form-group">
                                         <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             Category:(Eg:A , B, C):&nbsp;<span class="red">*</span>
                                         </label>
                                         <div class="col-sm-8">
                                             <input class="col-xs-10 col-sm-5" name="category" type="text" id="category" size="40" maxlength="10" value="<?php echo $airport_info[0]['category']; ?>">
                                         </div>
                                     </div>
                                     <div class="space-4"></div> -->
                                    <!-- <div class="form-group">
                                         <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             City:&nbsp;<span class="red">*</span> 
                                         </label>
                                         <div class="col-sm-8">
                                             <input class="col-xs-10 col-sm-5" name="acity" type="text" id="acity" size="40" maxlength="50" value="<?php //echo $airport_info[0]['acity'];    ?>">
                                         </div>
                                     </div>-->                        
                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             State:&nbsp;<span class="red">*</span> 
                                        </label>
                                        <div class="col-sm-8">                            	
                                            <select  name="astate">
                                    <?php
//foreach($rsStates  as $key =>  $state):											
                                    ?>
                                                        
                                                            <option value="<?php //echo $state['states_id'];     ?>" <?php //if($airport_info[0]['astate']==$state['states_id']) {    ?> selected <?php //}     ?> >
                                    <?php //echo $state['state_name'];  ?>
                                                            </option>
                                    <?php
                                    //endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="space-4"></div>--> 
                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             Country:&nbsp;
                                        </label>
                                        <div class="col-sm-8">                            	
                                            <select name="acountry" size="1" id="acountry">
                                    <?php
//foreach($row_rsCountries as $country):											
                                    ?>
                                                        
                                                            <option value="<?php //echo $country['countries_id'];     ?>" <?php //if($airport_info[0]['acountry']==$country['countries_id']) {    ?> selected <?php //}     ?>>
                                    <?php //echo $country['countries_name'];  ?>
                                                            </option>
                                    <?php
                                    //endforeach;
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="space-4"></div>--> 
                                    <!--  <div class="form-group">
                                         <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             Website:
                                         </label>
                                         <div class="col-sm-8">
                                          <input class="col-xs-10 col-sm-5"  name="awebsite" type="text" id="awebsite" size="40" maxlength="50" value="<?php echo $airport_info[0]['awebsite']; ?>">
                                         </div>
                                     </div>
                                     <div class="space-4"></div> -->
                                    <!--  <div class="form-group">
                                          <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                              Configuration:
                                          </label>
                                          <div class="col-sm-8">
                                           <input class="col-xs-10 col-sm-5" name="configuration" type="text" id="configuration" size="40" maxlength="50" value="<?php echo $airport_info[0]['configuration']; ?>">
                                          </div>
                                      </div>
                                      <div class="space-4"></div> -->
                                    <!-- <div class="form-group">
                                         <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                             Mgtstructure:
                                         </label>
                                         <div class="col-sm-8">
                                          <input class="col-xs-10 col-sm-5"   name="mgtstructure" type="text" id="mgtstructure" size="40" maxlength="50" value="<?php //echo $airport_info[0]['mgtstructure'];    ?>">
                                         </div>
                                     </div>-->
                                    <?php /* ?> <?php 
                                      if(count($termairportdet)>0)
                                      {
                                      foreach($termairportdet as $term):
                                      ?>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                      <label for="form-field-1" class="col-sm-4 control-label no-padding-right">
                                      Terminal Name:
                                      </label>
                                      <div class="col-sm-8">
                                      <input class="col-xs-10 col-sm-5"  name="terminalname[]" type="text" size="50" maxlength="100" value="<?php echo $term['terminalname']; ?>">&nbsp;<button type="button" title="Add more Terminal Option" class="btn btn-xs btn-danger removethis" tid="<?php echo $term['tid']; ?>"><i class="icon-trash"></i>Remove</button>
                                      <input type="hidden" name="termid[]" value="<?php echo $term['tid']; ?>">
                                      </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                      <label for="form-field-1" class="col-sm-4 control-label no-padding-right">
                                      Terminal Abbr:
                                      </label>
                                      <div class="col-sm-8">
                                      <input class="col-xs-10 col-sm-5"  name="terminalabbr[]"  type="text" size="50" maxlength="100" value="<?php echo $term['terminalabbr']; ?>">                          </div>
                                      </div>
                                      <?php
                                      endforeach; ?>
                                      <div class="mclone">
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                      <label for="form-field-1" class="col-sm-4 control-label no-padding-right">
                                      Terminal Name:
                                      </label>
                                      <div class="col-sm-8">
                                      <input class="col-xs-10 col-sm-5"  name="terminalname[]" type="text" size="50" maxlength="100" >
                                      &nbsp;<button type="button" id="addmoreterminal" title="Remove this Terminal Option" class="btn btn-xs btn-success"><i class="icon-plus"></i>Add</button>
                                      </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                      <label for="form-field-1" class="col-sm-4 control-label no-padding-right">
                                      Terminal Abbr:
                                      </label>
                                      <div class="col-sm-8">
                                      <input class="col-xs-10 col-sm-5"  name="terminalabbr[]"  type="text" size="50" maxlength="100">                          </div>
                                      </div>

                                      </div>
                                      <?php
                                      } else  {
                                      ?>
                                      <div class="mclone">
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                      <label for="form-field-1" class="col-sm-4 control-label no-padding-right">
                                      Terminal Name:
                                      </label>
                                      <div class="col-sm-8">
                                      <input class="col-xs-10 col-sm-5"  name="terminalname[]" type="text" size="50" maxlength="100">
                                      &nbsp;<button type="button" id="addmoreterminal" title="Add more Terminal Option" class="btn btn-xs btn-success"><i class="icon-plus"></i>Add</button>
                                      </div>
                                      </div>
                                      <div class="space-4"></div>
                                      <div class="form-group">
                                      <label for="form-field-1" class="col-sm-4 control-label no-padding-right">
                                      Terminal Abbr:
                                      </label>
                                      <div class="col-sm-8">
                                      <input class="col-xs-10 col-sm-5"  name="terminalabbr[]"  type="text" size="50" maxlength="100">                          </div>
                                      </div>

                                      </div>
                                      <?php }
                                      ?><?php */ ?>

                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Latitude:
                                        </label>
                                        <div class="col-sm-8">
                                         <input class="col-xs-10 col-sm-5"  name="Latitude" type="text" id="Latitude" size="40" maxlength="50" value="<?php //echo $airport_info[0]['Latitude'];   ?>">
                                        </div>
                                    </div>
                                    <div class="space-4"></div>--> 
                                    <!--<div class="form-group">
                                        <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                            Longitude:
                                        </label>
                                        <div class="col-sm-8">
                                         <input class="col-xs-10 col-sm-5"  name="Longitude" type="text" id="Longitude" size="40" maxlength="50" value="<?php //echo $airport_info[0]['Longitude'];    ?>">
                                        </div>
                                    </div>
                                    <div class="space-4"></div>-->                      
                                   <!-- <input type="hidden" name="MM_update" value="form1">
                                   <div class="form-group">
                                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                                &nbsp; 
                                            </label>
                                            <div class="col-sm-8">
                   <button type="submit" onClick="MM_validateForm('IATA','','R','aname','','R','acountry','','R','city','','R');return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Update</button>&nbsp;&nbsp;<a href="<?php //echo site_url('admin/airport');    ?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                            </div>
                                    </div>
                                    <div class="space-4"></div> -->
                                </form> 


                            </div>			
                        </div>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
        <?php echo $this->load->view($footer); ?>
        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script> 

        <script src="<?php echo base_url(); ?>assets/js/validator.js"></script>

        <script type="text/javascript">

                                                            $('#form1').validator();

                                                            $('#form1').validator({
                                                                rules: {
                                                                    astate: {
                                                                        required: true
                                                                    }
                                                                },
                                                                highlight: function (element) {
                                                                    $(element).closest('.control-group').removeClass('success').addClass('error').trigger('chosen:updated');
//                                                                        $('#categoryide').trigger('chosen:updated');
                                                                },
                                                                success: function (element) {
                                                                    element.text('OK!').addClass('valid')
                                                                            .closest('.control-group').removeClass('error').addClass('success').trigger('chosen:updated');
                                                                }
                                                            });


                                                            $(".chosen-select").chosen({width: "83.4%"});
                                                            function MM_validateForm() { //v4.0

                                                                if (document.getElementById) {

                                                                    var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;

                                                                    for (i = 0; i < (args.length - 2); i += 3) {
                                                                        test = args[i + 2];
                                                                        val = document.getElementById(args[i]);

                                                                        if (val) {
                                                                            nm = val.name;
                                                                            if ((val = val.value) != "") {

                                                                                if (test.indexOf('isEmail') != -1) {
                                                                                    p = val.indexOf('@');

                                                                                    if (p < 1 || p == (val.length - 1))
                                                                                        errors += '- ' + nm + ' must contain an e-mail address.\n';

                                                                                } else if (test != 'R') {
                                                                                    num = parseFloat(val);

                                                                                    if (isNaN(val))
                                                                                        errors += '- ' + nm + ' must contain a number.\n';

                                                                                    if (test.indexOf('inRange') != -1) {
                                                                                        p = test.indexOf(':');

                                                                                        min = test.substring(8, p);
                                                                                        max = test.substring(p + 1);

                                                                                        if (num < min || max < num)
                                                                                            errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';

                                                                                    }
                                                                                }
                                                                            } else if (test.charAt(0) == 'R')
                                                                                errors += '- ' + nm + ' is required.\n';
                                                                        }

                                                                    }
                                                                    if (errors) {
                                                                        $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:\n' + errors.replace("_", " ").toUpperCase() + "</div></div></div>");
                                                                        $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');
                                                                    }

                                                                    document.MM_returnValue = (errors == '');

                                                                }
                                                            }
                                                            /*$("#addmoreterminal").click(function()
                                                             {
                                                             var clone=$(".mclone").html();				
                                                             $(".terminla").append(clone);
                                                             
                                                             });*/
                                                            var x = 1;
                                                            var max_fields = 50;
                                                            var wrapper = $(".mclone");
                                                            var add_button = $("#addmoreterminal");
                                                            $(add_button).click(function (e) {
                                                                e.preventDefault();
                                                                if (x < max_fields) {
                                                                    x++;
                                                                    $(wrapper).append('<div><div class="form-group"><label for="form-field-1" class="col-sm-4 control-label no-padding-right">Terminal Name:</label><div class="col-sm-8"><input type="text" maxlength="100" class="col-xs-10 col-sm-10" name="terminalname[]" size="50" /></div></div><div class="form-group"><label for="form-field-1" class="col-sm-4 control-label no-padding-right">Terminal Abbr:</label><div class="col-sm-8"><input type="text" maxlength="100" size="50" class="col-xs-10 col-sm-10"  name="terminalabbr[]" /></div></div><button style="margin:-46px -21px 0px 0px;" class="remove_field btn btn-xs btn-danger pull-right col-md-offset-1"><i class="icon-remove"></i> Remove</button></div>');
                                                                }
                                                            });
                                                            $(wrapper).on("click", ".remove_field", function (e) {
                                                                e.preventDefault();
                                                                $(this).parent('div').remove();
                                                                x--;
                                                            });

        </script>  
        <script type="text/javascript">
            function showerror(error)
            {
                var h = '<div class="row"><div class="col-sm-12"><div class="alert alert-danger">' + error + '</div></div></div>';
                $(".error").html(h);
                $('html, body').animate({scrollTop: $('.error').offset().top}, 'slow');
            }
            $(".removethis").on("click", function ()
            {
                //var datastring=$(this).attr("tid"); 
                var datastring = "tid=" + $(this).attr("tid") + "&action=delterminalname";
                $.ajax({
                    type: "POST",
                    async: false,
                    cache: false,
                    url: "<?php echo site_url('ajax/doaction'); ?>",
                    data: datastring,
                    success: function (responsce)
                    {
                        if (responsce == 1)
                        {
                            window.location = "<?php echo current_url(); ?>";
                        } else
                        {
                            showerror(responsce);
                        }
                    }
                });
            });
        </script>
    </body>
</html>
