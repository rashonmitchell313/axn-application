<!DOCTYPE html>
<html lang="en">
	<head>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Modify Category | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Modify Category | ARN Fact Book';
}
?>
</title>
		<?php echo $this->load->view($header); ?>
	</head>
	<body>
		<?php echo $this->load->view($topnav); ?>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>
				<?php echo $this->load->view($leftnav); ?>	
				<div class="main-content">
					<?php echo $this->load->view($breadcrumb); ?>                    
					<div class="page-content">                   
						<div class="row">                        
							<div class="col-xs-12">
                            <div class="error"><?php echo $this->common->getmessage();?></div>
<?php
$id= $this->uri->segment(3);
?>
                      <form method="post" name="form1" class="form-horizontal" action="<?php echo site_url('admin/edit_category').'/'.$id; ?>">                    <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
                     	<tr>
                        	<tr>
                            	<td>
                                	<div class="form-group">
                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                            	Category ID:&nbsp;<span class="red">*</span> 
                            </label>
                            <div class="col-sm-8">
                             <input type="text" value="<?php echo $row_rsUser['category_id'];?>" class="col-xs-10 col-sm-10" readonly>
                              </div>
                        </div>
                                </td>                                
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                            <td>
                            	<div class="form-group">
                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                            	Category Name:&nbsp;<span class="red">*</span> 
                            </label>
                            <div class="col-sm-8">
                             <input type="text" class="col-xs-10 col-sm-10" name="category_name" type="text" id="category_name" size="50" maxlength="50" value="<?php echo htmlentities($row_rsUser['category_name'], ENT_COMPAT, 'iso-8859-1'); ?>" >
                              </div>
                        </div>
                            </td>
                            </tr>
                            <tr>
                            <td>
                            <div class="form-group">
                            <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                            	Active: 
                            </label>
                            <div class="col-sm-8">
                            <input type="checkbox" name="is_active" value="1" <?php if (!(strcmp(htmlentities($row_rsUser['is_active'],ENT_COMPAT, 'iso-8859-1'),1))) {echo "checked=\"checked\"";} ?>>
                            </div>
                        </div>
                        <input type="hidden" name="MM_update" value="form1">
                        <input type="hidden" name="category_id" value="<?php echo $row_rsUser['category_id']; ?>">
                            </td>
                        </tr>
                        
                        <tr>                        	
                            <td>
                            <div class="form-group">
                                <label for="form-field-1" class="col-sm-4 control-label no-padding-right"> 
                                    &nbsp; 
                                </label>
                                <div class="col-sm-8">
                                <button type="submit" onClick="MM_validateForm('category_name','','R');return document.MM_returnValue" class="btn btn-success"><i class="icon-ok"></i>&nbsp;Update</button>
                                <a href="<?php echo site_url('admin/all_faq_categories');?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                </div>
                        </div>
                            </td>
                            <td>&nbsp;
                            
                            </td>                           
                        </tr>
                     </table>	
                      </form>                       	   
		
                            </div>			
						</div>
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
       <?php echo $this->load->view($footer);?>  
       <script type="text/javascript">
	   	function MM_validateForm() { //v4.0

  if (document.getElementById){

    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;

    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);

      if (val) { nm=val.name; if ((val=val.value)!="") {

        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');

          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';

        } else if (test!='R') { num = parseFloat(val);

          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';

          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');

            min=test.substring(8,p); max=test.substring(p+1);

            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';

      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }

    } if(errors) { $(".error").html('<div class="row"><div class="col-sm-12"><div class="alert alert-danger">The following error(s) occurred:\n'+errors.replace("_"," ").toUpperCase()+"</div></div></div>"); 
	$('html, body').animate({ scrollTop: $('.error').offset().top }, 'slow');
	}

    document.MM_returnValue = (errors == '');

} }
	   </script>     
	</body>
</html>
