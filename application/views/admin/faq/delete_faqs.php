<!DOCTYPE html>
<html lang="en">
	<head>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Delete Category | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Delete Category | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/dataTables.responsive.css" >
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/jquery-ui-1.10.3.full.min.css" >
		<?php echo $this->load->view($header); ?>
	</head>
	<body>
    
		<?php echo $this->load->view($topnav); ?>
		<div class="main-container" id="main-container">
			  <script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>
				<?php echo $this->load->view($leftnav); ?>	
				<div class="main-content">
					<?php echo $this->load->view($breadcrumb); ?>                    
					<div class="page-content">
						<div class="row">    
                                           
									<div class="col-xs-12">	
                                    
                                    <?php 
										if($this->common->GetSessionKey('airportsearch')!="")
										{ ?>
                                        	<div class="alert alert-success">
                                            	<strong>(<?php echo $total; ?>)</strong> Airports has been founds.
									<form action="<?php echo site_url('airport/clearsearch');?>" method="post">
                                    <button  class="cleansearch btn btn-danger btn-xs" name="submit" >
                                    <i class="fa fa-trash"></i>&nbsp;Clear Search</button>
									</form>
                                            </div>
											<?php
										}
									?>									
                                     <div class="error"><?php echo $this->common->getmessage();?></div>
										<div class="table-header">
											List of all Categories
										</div>
										<div class="table-responsive">
                                        
                        <table width="100%" id="sample-table-2" class="table table-striped table-bordered table-hover datatable"> 	<thead>
                                <tr>                                    
                                    <th>Category ID</th>
                                    <th>Category Name</th>  
                                    <th>Status</th>                         
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            <?php  // foreach($listofairport as $airport): ?>
                               
                                <?php /*?><!--<tr>                                   
                                                                                                        
                                    <td><?php //echo $airport['IATA'];?></td>
                                    <td><?php //echo $airport['aname'];?></td>
                                    <td><?php //echo $airport['lastmodified'];?></td>
                                    <td><?php //echo $airport['modifiedby'];?></td>
                                    <td><?php //echo $airport['published'];?></td>
                                    <td><?php //echo $airport['approved'];?></td>
                                    <td><?php //echo $airport['aid']; ?></td>
                                    <td><input type="checkbox" name="" value="<?php echo $airport['aid']; ?>" <?php $this->common->AirportPdfArray($airport['aid']);?> ></td>									                                  
                                    <td>
                                        <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                           
                                            <a class="green" href="<?php echo site_url('admin/airport/edit_airport/'.$airport['aid']);?>">
                                                <i class="icon-pencil bigger-130"></i>
                                            </a>

                                            <a class="bootbox-confirm id-btn-dialog2" href="" aid="<?php echo $airport['aid'];?>">
                                                <i class="icon-trash bigger-130"></i>
                                            </a>
                                        </div>

                                        <div class="visible-xs visible-sm hidden-md hidden-lg">
                                            <div class="inline position-relative">
                                                <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-caret-down icon-only bigger-120"></i>
                                                </button>

                                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                   

                                                    <li>
                                                        <a href="<?php echo site_url('admin/airport/edit_airport/'.$airport['aid']);?>" class="tooltip-success" data-rel="tooltip" title="Edit">
                                                            <span class="green">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#" class="tooltip-error bootbox-confirm id-btn-dialog2"  data-rel="tooltip" title="Delete" aid="<?php echo $airport['aid'];?>">
                                                            <span class="red">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>--><?php */?>
                                
                                <?php //endforeach; ?>
                            </tbody>
                        </table>
                        
                        <br>
                        	
									</div>
								</div>
					</div><!-- /.page-content -->
                    <div id="dialog-confirm" class="hide">
                    <div class="alert alert-info bigger-110">
                        These items will be permanently deleted and cannot be recovered.
                    </div>    
                    <div class="space-6"></div>    
                    <p class="bigger-110 bolder center grey">
                        <i class="icon-hand-right blue bigger-120"></i>
                        Are you sure?
                    </p>
                </div>  
				</div><!-- /.main-content -->
                
			</div><!-- /.main-container-inner -->
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->  
               
       <?php echo $this->load->view($footer);?>
     
      
     
       <script type="text/javascript">
	   
	   function showerror(error)
	   {
		 var h='<div class="row"><div class="col-sm-12"><div class="alert alert-danger">'+error+'</div></div></div>';
		  $(".error").html(h);
		  $('html, body').animate({ scrollTop: $('.error').offset().top }, 'slow');
	   }
	   /*	$(".upsub").click(function()
		{
				var user_id=$(this).attr("user_id");
				var subscription=$(this).attr("subscription");
				var datastring="user_id="+user_id+"&subscription="+subscription+"&action=updatesubscription";
				if(subscription==1 || subscription==0 && user_id>0)
				{
					$.ajax({
							url:"<?php //echo site_url('ajax/doaction'); ?>",
							type:"POST",
							cache:false,
							async:false,
							data:datastring,
							success: function(html)
							{
								if(html==1)
								{
									window.location="<?php //echo site_url('admin/airport'); ?>";
								}
							}
					});
				} else 
				{
					showerror("Action has been failed.");
				}
				return false;
		});*/
	   </script>   
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.dataTablees.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.responsive.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.full.min.js"></script>

<script type="text/javascript">
$(document).ready(function(e) {
    $('#sample-table-2').dataTable({
		 responsive: true,
		"processing": true,
		"serverSide": true,	
		 aoColumnDefs: [
        { "aTargets": [ 2 ], "bSortable": false },
		 { "aTargets": [ 3 ], "bSortable": false }
    ],			
		"ajax": "<?php echo site_url('ajax/list_of_all_faq_category');?>"
	});	
	var dtable = $(".datatable").dataTable().api();
	$(".dataTables_filter input")
    .unbind() 
    .bind("input", function(e) {
        if(this.value.length >= 3 || e.keyCode == 13) {
           
            dtable.search(this.value).draw();
        }
       
        if(this.value == "") {
            dtable.search("").draw();
        }
        return;
    });
});
jQuery(function($)
{
	function updateselectlist()
	{
		 var allVals = [];
			 $(':checked').each(function() 
			 {
			   allVals.push($(this).val());
			 });			
			 var datastring="listOfAirport="+allVals+"&action=listofairport";			
			 $.ajax({
						url:"<?php echo site_url('ajax/doaction');?>",
						cache:false,
						type:"POST",
						async:false,
						data:datastring,
						success: function(responsce)
						{
							if(responsce!=1)
							{
								showerror(responsce);	
							}
						}
			 });
	}	
	$(".export-to-pdf").on("click",function()
	{
		updateselectlist();
		$(this).html("Wait.....");		
		 //var datastring="listOfAirport="+allVals+"&action=generatepdf";
		 window.location="<?php echo site_url('pdf/GenPdf');?>";
		 $.ajax({
						url:"<?php echo site_url('pdf/GenPdf');?>",
						cache:false,
						type:"POST",
						async:false,
						data:datastring,
						success: function(responsce)
						{
							if(responsce!=1)
							{
								showerror(responsce);	
							}
						}
			 });
	});
	
	$(".pagination li a").on("click",function()
	{
			 updateselectlist();			 
			 return true;						 
	});
	
/*$( ".id-btn-dialog2" ).on('click', function(e) 
{
					//var aid=$(this).attr('aid');
					var datastring="aid="+aid+"&action=deleteairport";				
					e.preventDefault();
					var cateid=$(this).attr("value");
					$( "#dialog-confirm" ).removeClass('hide').dialog({
						resizable: false,
						modal: true,
						title: "Are you sure?",
						title_html: true,
						buttons: [
							{
								html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
								"class" : "btn btn-danger btn-xs",
								click: function() 
								{
									$( this ).dialog( "close" );
									$.ajax({
										type:"POST",
										async:false,
										cache:false,
										url:"<?php //echo site_url('ajax/doaction'); ?>",
										data:datastring,
										success: function(responsce)
										{
											if(responsce==1)
											{
												window.location="<?php //echo site_url('admin/airport');?>";
											} else 
											{
												showerror(responsce);	
											}
										}																					
									});								
								}
							}
							,
							{
								html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
								"class" : "btn btn-xs",
								click: function() {
									$( this ).dialog( "close" );
									return false;
								}
							}
						]
					});
				return false;
				});*/

				
});
</script>   
 <script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script> 
            <script type="text/javascript">
            function myFunction(val) {
				
			var category_id=val;
			var datastring="category_id="+category_id+"&action=deletefaqcategory";				
			var cateid=$(this).attr("value");
			$( "#dialog-confirm" ).removeClass('hide').dialog({
				resizable: false,
				modal: true,
				title: "Are you sure?",
				title_html: true,
				buttons: [
				{
					html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
					"class" : "btn btn-danger btn-xs",
					click: function() 
					{
						$( this ).dialog( "close" );
						$.ajax({
						type:"POST",
						async:false,
						cache:false,
						url:"<?php echo site_url('ajax/doaction'); ?>",
						data:datastring,
						success: function(responsce)
						{
						if(responsce==1)
						{
							window.location="<?php echo site_url('admin/all_faq_categories');?>";
							} else 
							{
								showerror(responsce);	
							}
						}																					
						});								
					}
				}
				,
				{
				html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
				"class" : "btn btn-xs",
				click: function() {
				$( this ).dialog( "close" );
				return false;
				}
				}
				]
			});
			return false;
			
}</script>   

	</body>
</html>

