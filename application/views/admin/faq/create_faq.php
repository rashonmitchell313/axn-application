<!DOCTYPE html>
<html lang="en">
	<head>
<title>
<?php 
if($this->uri->segment(0)!='')
{
echo 'Create FAQ | ARN Fact Book '.$this->uri->segment(0);
}
else 
{
echo 'Create FAQ | ARN Fact Book';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/jquery-ui-1.10.3.full.min.css" >
<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/choseen.css" />
<?php echo $this->load->view($header); ?>
	</head>

	<body>
		<div class="navbar navbar-default" id="navbar">

			<?php echo $this->load->view($topnav); ?>
		</div>

		<div class="main-container" id="main-container">

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<?php echo $this->load->view($leftnav); ?>	

				<div class="main-content">
					<?php echo $this->load->view($breadcrumb); ?>

					<div class="page-content">
                    	<!-- /.page-header -->
						<?php
						if($this->uri->segment(2)=='edit-faq')
						{
							foreach($edit_faq as $faq)
							{
								$category_id = $faq['category_id'];
								$faq_question = $faq['faq_question'];
								$faq_answer = $faq['faq_answer'];
								//$faq_category = $faq['category_name'];
								$faq_is_active = $faq['faq_is_active'];
								
							}
						}
						else
						{		//$faq_category='';
								$category_id = '';
								$faq_question = '';
								$faq_answer = '';
								$faq_is_active = 1;
						}
						?>
                        <div class="message"><?php // echo $this->commonmodel->get_admin_message(); ?></div><br />
						<div class="row">
							<div class="col-xs-12">
                            <div class="alert alert-danger" id="field" style="display:none;">Please select Category or Add a new category.</div>
								<!-- PAGE CONTENT BEGINS -->
								<div class="widget-box">
                                    <div class="widget-header header-color-blue2">
                                            <h4 class="lighter smaller"><?php if($this->uri->segment(2)=='edit-faq'){echo "Edit"; }else{echo 'Create new';}?> FAQ</h4>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <form class="form-horizontal" role="form" onSubmit="return validate_faq();" method="post" action="<?php echo site_url('admin/create-faq');?>">
                                            	<div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Question <span class="red">*</span></label>                                    
                                                    <div class="col-sm-9">
                                                    	<textarea placeholder="Enter the Question.." id="question" name="question" class="col-xs-10 col-sm-5"><?php echo $faq_question;?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Select Category <span class="red">*</span></label>                                    
                                                    <div class="col-sm-9">
                                                    <select class="chosen-select col-xs-10 col-sm-5" name="scategory" id="scategory" >
                                                  	<option value="">Select Category</option>';
													
													 <?php  foreach($faqcategory as $c)
													{
														$selected = ($c['category_id'] == $category_id)?' selected':'';
														echo '<option'.$selected.' value="'.$c['category_id'].'">'.$c['category_name'].'</option>';
													} ?>
                                                    <option value="0">Add New</option>
                                                     </select>
                                                    &nbsp; <a href="<?php echo site_url('admin/all_faq_categories');?>" class="btn btn-xs btn-danger"><i class="icon-trash bigger-120"></i> Delete Category</a>
                                                    	
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group" id="addnew" style="display:none;">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Add new Category <span class="red">*</span></label>                                    
                                                    <div class="col-sm-9">
                                                    <input type="text" class="col-xs-10 col-sm-5" name="category" id="category" size="50" maxlength="50">
                                                   
                                                    	
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Is Active <span class="red">*</span></label>
                                                    <label class="col-sm-3 no-padding-right">
                                                        <input class="ace ace-switch ace-switch-2" name="is_active" type="checkbox" <?php if($faq_is_active == 1){echo "checked";}?>>
                                                        <span class="lbl"></span>
                                                    </label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Answer <span class="red">*</span></label>                                    
                                                    <div class="col-sm-9">
                                                    	<div class="widget-box">
                                                            <div class="widget-header widget-header-small  header-color-blue">
                                                                <div class="widget-toolbar">
                                                                    <a href="#" data-action="collapse">
                                                                        <i class="icon-chevron-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="widget-body">
                                                                <div class="widget-main no-padding">
                                                                    <div class="wysiwyg-editor" id="editor"><?php echo $faq_answer;?></div>
                                                                    <textarea style="display:none;" id="answer" name="answer" class="col-xs-10 col-sm-5"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <?php if($this->uri->segment(2)=='edit-faq'){?>
                                                	<input type="hidden" name="faq_id" value="<?php echo $this->uri->segment(3);?>">
                                                <?php }?>
                                                
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-6">
                                                        <button class="btn btn-success" type="submit" id="update">
                                                            <i class="icon-ok bigger-110"></i>
                                                            <?php if($this->uri->segment(2)=='edit-faq'){echo "Edit"; }else{echo 'Add';}?> FAQ
                                                        </button>
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                        <a href="<?php echo site_url('admin/view-faq');?>" class="btn btn-danger"><i class="icon-reply icon-only"></i>&nbsp;Cancel</a>
                                                    </div>
                                                </div>                                                                            
                                            </form>                                                                                                                   	
                                        </div>                                                       		
                                    </div>
                                </div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
                <div id="dialog-confirm" class="hide">
                    <div class="alert alert-info bigger-110">
                        These items will be permanently deleted and cannot be recovered.
                    </div>    
                    <div class="space-6"></div>    
                    <p class="bigger-110 bolder center grey">
                        <i class="icon-hand-right blue bigger-120"></i>
                        Are you sure?
                    </p>
                </div>

			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<?php echo $this->load->view($footer);?>  
		<!-- page specific plugin scripts -->
		<script src="<?php echo base_url('assets');?>/js/bootstrap-wysiwyg.min.js"></script>
		<script src="<?php echo base_url('assets');?>/js/jquery.hotkeys.min.js"></script>
          <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
		<!-- inline scripts related to this page -->
        <script type="text/javascript">
		$(".chosen-select").chosen({width: "41.5%"}); 
			$('#editor').ace_wysiwyg({
				toolbar_place: function(toolbar) {
					return $(this).closest('.widget-box').find('.widget-header').prepend(toolbar).children(0).addClass('inline');
				},
				toolbar:
					[
						'bold',
						{name:'italic' , title:'Change Title!', icon: 'icon-leaf'},
						'strikethrough',
						null,
						'insertunorderedlist',
						'insertorderedlist',
						null,
						'justifyleft',
						'justifycenter',
						'justifyright'
					],
				speech_button:false
			}).prev().addClass('wysiwyg-style2');
			
			$('#myform').on('reset', function() {
				$('#editor').empty();
			});
			function validate_faq()
			{
				var question = $("#question").val();
				var ans = $('#editor').html().trim();
				$("#answer").val(ans);
				var answer = $("#answer").val();
				if(question == "" || answer == "")
				{
					$(".message").html('<div class="alert alert-danger">(*) is indicated field is required.<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></div>');
					$('html, body').animate({ scrollTop: $(".page-header").offset().top }, 100);
					$(".message").children().delay(3000).fadeOut();
					return false;
				}
				else
				{
					$("#update").children('i').attr('class', 'fa fa-spinner fa-spin');
					return true;
				}
			}	
			
				
				 $("#scategory").on('change', function(e){	  
         		   var other = $("#scategory option:selected").text().trim();
         	 		if(other == 'Add New')
         			{
         			$('#addnew').show('slow');
         			}
         			else if(other !== 'Add New')
         			{
         				$('#addnew').hide('slow');
         			}			
         		});
				$("#update").on('click', function(){
				 var select_option = $("#scategory option:selected").val();
				 var hidden_field = $("#category").val();
				  if(select_option == "" && hidden_field=="")
				  {
					 $("#field").show();
					 return false;
				    
				  }
				 
				  else
				  {
					  return true;
				  }
				});
			
		</script>
	</body>
</html>