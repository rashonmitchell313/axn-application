<?php //var_dump ($this->session->all_userdata());exit; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>

            <?php
            if ($this->uri->segment(0) != '') {
                echo 'View FAQ | ARN Fact Book ' . $this->uri->segment(0);
            } else {
                echo 'View FAQ | ARN Fact Book';
            }
            ?>
        </title>
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/jquery-ui-1.10.3.full.min.css" >
<?php echo $this->load->view($header); ?>
    </head>


    <body>
        <div class="navbar navbar-default" id="navbar">

<?php echo $this->load->view($topnav); ?>
        </div>

        <div class="main-container" id="main-container">

            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>

<?php echo $this->load->view($leftnav); ?>	

                <div class="main-content">
                <?php echo $this->load->view($breadcrumb); ?>	
                    <div class="page-content">

                        <div class="message"><?php //echo $this->commonmodel->get_admin_message(); ?></div><br />
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->
                                <div class="error"><?php echo $this->common->getmessage(); ?></div>
                                <div class="widget-box">
                                    <div class="widget-header header-color-blue">
                                        <h4>FAQ's</h4>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div id="accordion" class="accordion-style2">
<?php foreach ($faqs as $faq): ?>
                                                    <div class="group">
                                                        <h3 class="accordion-header" id="<?php echo $faq['faq_id']; ?>"><?php echo $faq['faq_question']; ?></h3>
                                                        <div>
                                                            <p><?php echo $faq['faq_answer']; ?></p><?php if ($faq['faq_is_active'] == 1) { ?>
                                                                <span class="label label-success arrowed">Active</span>
    <?php } else if ($faq['faq_is_active'] == 0) { ?>
                                                                <span class="label label-danger arrowed-in">Deleted</span>
                                                            <?php } ?><a id="faq-edit<?php echo $faq['faq_id']; ?>" href="<?php echo site_url('admin/edit-faq/' . $faq['faq_id']); ?>"><i class="icon-edit"></i></a>
                                                            &nbsp;<a class="bootbox-confirm id-btn-dialog2 faq-del<?php echo $faq['faq_id']; ?>" id="id-btn-dialog2" href="javascript:void(0)" faq_id="<?php echo $faq['faq_id']; ?>" faq_status="<?php echo $faq['faq_is_active']; ?>" data-toggle="tooltip" title="Click here to delete & If Status is 'Deleted' then click to delete Permanently." >
                                                                <i class="icon-trash"></i>
                                                            </a>

    <?php /* ?><img src="<?php echo $this->common->get_small_pic_name('faq', $faq['faq_img']);?>"  width="100px" height="100px" /><?php */ ?>
                                                        </div>
                                                    </div>
                                                        <?php endforeach; ?>
                                            </div><!-- #accordion -->
                                                <?php /* ?><div class="table-responsive">
                                                  <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                                  <thead>
                                                  <tr>
                                                  <th class="center">Sr #</th>
                                                  <th>Title</th>

                                                  <th>Question</th>
                                                  <th>Answer</th>
                                                  <th>Created Date</th>
                                                  <th>Last Modified Date</th>
                                                  <th class="hidden-480">Status</th>
                                                  <th></th>
                                                  </tr>
                                                  </thead>

                                                  <tbody>
                                                  <?php
                                                  foreach($faqs as $faq) : ?>
                                                  <tr class="">
                                                  <td><?php echo $faq['faq_id']; ?></td>
                                                  <td><?php echo $faq['faq_title']; ?></td>
                                                  <td><?php echo $faq['faq_question']; ?></td>
                                                  <td><?php echo $faq['faq_answer']; ?></td>
                                                  <td><?php echo date("d/m/Y H:i:s",strtotime($faq['faq_created_date'])); ?></td>

                                                  <td class="hidden-480"><?php if($faq['faq_is_active'] == 1) { ?>
                                                  <span class="label label-success arrowed">Active</span>
                                                  <?php } else if($faq['faq_is_active'] == 0) { ?>
                                                  <span class="label label-danger arrowed-in">Deleted</span>
                                                  <?php } ?>
                                                  </td>
                                                  <td>
                                                  <a href="<?php echo site_url('admin/edit-faq/'.$faq['faq_id']); ?>"><i class="icon-edit"></i></a>
                                                  &nbsp;<a href="<?php echo site_url('admin/delete-faq/'.$faq['faq_id']); ?>" onClick="return confirm('Are you sure want to delete this FAQ?')"><i class="icon-trash"></i></a>
                                                  </td>
                                                  </tr>
                                                  <?php endforeach; ?>
                                                  <tfoot>
                                                  </tfoot>
                                                  </tbody>
                                                  </table>
                                                  </div><?php */ ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->

                <div id="dialog-confirm21" class="hide">
                    <div class="alert alert-info bigger-110">
<?php // if($faq['faq_is_active'] == 1) {  ?>
                        <!--This FAQ will be not active more, But not Deleted permanently.-->
                        <?php // } else if($faq['faq_is_active'] == 0) {  ?>
                        This FAQ will be permanently deleted and cannot be recovered.
                        <?php // } ?>
                    </div>    
                    <div class="space-6"></div>    
                    <p class="bigger-110 bolder center grey">
                        <i class="icon-hand-right blue bigger-120"></i>
                        Are you sure?
                    </p>
                </div>

                <div id="dialog-confirm" class="hide">
                    <div class="alert alert-info bigger-110">
<?php // if($faq['faq_is_active'] == 1) {  ?>
                        This FAQ will be not active more, But not Deleted permanently.
                        <?php // } else if($faq['faq_is_active'] == 0) {  ?>
                        <!--This FAQ will be permanently deleted and cannot be recovered.-->
                        <?php // } ?>
                    </div>    
                    <div class="space-6"></div>    
                    <p class="bigger-110 bolder center grey">
                        <i class="icon-hand-right blue bigger-120"></i>
                        Are you sure?
                    </p>
                </div>
            </div><!-- /.main-container-inner -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

<?php echo $this->load->view($footer); ?> 
        <!-- page specific plugin scripts -->
        <script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/bootbox.min.js"></script>
        <!-- inline scripts related to this page -->
        <script type="text/javascript">

            $('a[data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'top',
            });

            $("#accordion").accordion({
                collapsible: true,
                heightStyle: "content",
                animate: 250,
                header: ".accordion-header"
            });

            $(".delete_faq").on('click', function (e) {
                e.preventDefault();
                var id = $(this).attr("value");
                var active = ($(this).attr("active") == 0) ? " <strong>permanently</strong>" : "";
                bootbox.confirm("Are you sure you wants to" + active + " delete this faq?", function (result) {
                    if (result)
                        window.location.href = "<?php echo site_url('admin/delete-faq'); ?>/" + id;
                });
            });

        </script>
        <script type="text/javascript">
//			jQuery(function($)
//			{
            $(".id-btn-dialog2").on('click', function (e)
//			$("#id-btn-dialog2").on('click', function(e) 
            {//alert('test123'); return false;
                var faq_id = $(this).attr('faq_id');
                var faq_status = $(this).attr('faq_status');
                var datastring = "faq_id=" + faq_id + "&faq_status=" + faq_status + "&action=deletefaq";
                e.preventDefault();
                var cateid = $(this).attr("value");

                if (faq_status == 1) {
//                                    alert('test');
                    $("#dialog-confirm").removeClass('hide').dialog({
                        resizable: false,
                        modal: true,
                        title: "Are you sure?",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
                                "class": "btn btn-danger btn-xs",
                                click: function ()
                                {
                                    $(this).dialog("close");
                                    $.ajax({
                                        type: "POST",
                                        async: false,
                                        cache: false,
                                        url: "<?php echo site_url('ajax/doaction'); ?>",
                                        data: datastring,
                                        success: function (responsce)
                                        {
                                            if (responsce == 1)
                                            {
                                                window.location = "<?php echo site_url('admin/view-faq'); ?>";
                                            } else
                                            {alert(responsce);
                                                showerror(responsce);
                                            }
                                        }
                                    });
                                }
                            }
                            ,
                            {
                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
                                "class": "btn btn-xs",
                                click: function () {
                                    $(this).dialog("close");
                                    return false;
                                }
                            }
                        ]
                    });
                }
                else if (faq_status == 0)
                {
//                                    alert('testig');
                    $("#dialog-confirm21").removeClass('hide').dialog({
                        resizable: false,
                        modal: true,
                        title: "Are you sure?",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
                                "class": "btn btn-danger btn-xs",
                                click: function ()
                                {
                                    $(this).dialog("close");
                                    $.ajax({
                                        type: "POST",
                                        async: false,
                                        cache: false,
                                        url: "<?php echo site_url('ajax/doaction'); ?>",
                                        data: datastring,
                                        success: function (responsce)
                                        {
                                            if (responsce == 1)
                                            {
                                                window.location = "<?php echo site_url('admin/view-faq'); ?>";
                                            } else
                                            {
                                                alert(responsce);
                                                showerror(responsce);
                                            }
                                        }
                                    });
                                }
                            }
                            ,
                            {
                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
                                "class": "btn btn-xs",
                                click: function () {
                                    $(this).dialog("close");
                                    return false;
                                }
                            }
                        ]
                    });
                }

//				$( "#dialog-confirm" ).removeClass('hide').dialog
//                                ({
//					resizable: false,
//					modal: true,
//					title: "Are you sure?",
//					title_html: true,
//					buttons: [
//						{
//							html: "<i class='icon-trash bigger-110'></i>&nbsp; Delete",
//							"class" : "btn btn-danger btn-xs",
//							click: function() 
//							{
//								$( this ).dialog( "close" );
//								$.ajax({
//									type:"POST",
//									async:false,
//									cache:false,
//									url:"<?php echo site_url('ajax/doaction'); ?>",
//									data:datastring,
//									success: function(responsce)
//									{
//										if(responsce==1)
//										{
//											window.location="<?php echo site_url('admin/view-faq'); ?>";
//										} else 
//										{
//											showerror(responsce);	
//										}
//									}																					
//								});								
//							}
//						}
//						,
//						{
//							html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
//							"class" : "btn btn-xs",
//							click: function() {
//								$( this ).dialog( "close" );
//								return false;
//							}
//						}
//					]
//				});
                return false;
            });

//			});
        </script> 
    </body>
</html>