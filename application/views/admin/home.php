<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->load->view($header); ?>
        <title>
            <?php
            if ($this->uri->segment(0) != '') {
                echo 'Welcome to ARN Fact Book | Admin ' . $this->uri->segment(0);
            } else {
                echo 'Welcome to ARN Fact Book | Admin';
            }
            ?>
        </title>
    </head>
    <body>
            <?php echo $this->load->view($topnav); ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
<?php echo $this->load->view($leftnav); ?>	
                <div class="main-content">
<?php echo $this->load->view($breadcrumb); ?>
                    <div class="page-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="alert alert-block alert-success">
                                    <button type="button" class="close" data-dismiss="alert">
                                        <i class="icon-remove"></i>
                                    </button>
                                    <i class="icon-ok green"></i>
                                    Welcome to
                                    <strong class="green">
                                        Arnfact Book										
                                    </strong>
                                </div>
                                <div class="row">
                                    <div class="space-6"></div>
                                    <div class="col-sm-6">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-asterisk"></i>
                                                    Companies | Total Companies:<strong> <?php echo $total_companies; ?></strong>
                                                </h5>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div id="companychart"></div>
                                                    <div class="hr hr8 hr-double"></div>													
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div>
                                    </div>
                                    <div class="vspace-sm"></div>
                                    <div class="col-sm-6">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-asterisk"></i>
                                                    Company Comparison Company Type </strong>
                                                </h5>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div id="piechart"></div>
                                                    <div class="hr hr8 hr-double"></div>													
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                    </div><!-- /span -->
                                </div><!-- /row -->
                                <div class="row">
                                    <div class="space-6"></div>
                                    <div class="col-sm-6">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-user"></i>
                                                    User | Total Register User:<strong> <?php echo $TotalUsers; ?></strong>
                                                </h5>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div id="UserChart"></div>
                                                    <div class="hr hr8 hr-double"></div>													
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div>
                                    </div>
                                    <div class="vspace-sm"></div>
                                    <div class="col-sm-6">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-user"></i>
                                                    Top 10 Most Login User </strong>
                                                </h5>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div id="LoggingHistory"></div>
                                                    <div class="hr hr8 hr-double"></div>													
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                    </div><!-- /span -->
                                </div>
                                <div class="row">
                                    <div class="space-6"></div>
                                    <div class="col-sm-6">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-plane"></i>
                                                    Airports | Total Airports:<strong> <?php echo $total_airport; ?></strong>
                                                </h5>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div id="airportchart"></div>
                                                    <div class="hr hr8 hr-double"></div>													
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div>
                                    </div>
                                    <div class="vspace-sm"></div>									
                                </div>
                                <div class="hr hr32 hr-dotted"></div>
                                <div class="row">
                                    <div class="col-sm-5">									
                                    </div>
                                </div>
                                <div class="hr hr32 hr-dotted"></div>
                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
<?php echo $this->load->view($footer); ?>
        <script src="<?php echo base_url(); ?>/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
                                google.load("visualization", "1", {packages: ["corechart"]});
                                google.setOnLoadCallback(drawChart);
                                function drawChart() {
                                    var data = google.visualization.arrayToDataTable([
                                        ['Company Type', '# Of Company'],
                                        ['<?php echo $companycompersion[0]['companytype']; ?>',<?php echo $companycompersion[0]['total']; ?>],
                                        ['<?php echo $companycompersion[1]['companytype']; ?>',<?php echo $companycompersion[1]['total']; ?>],
                                        ['<?php echo $companycompersion[2]['companytype']; ?>',<?php echo $companycompersion[2]['total']; ?>],
                                        ['<?php echo $companycompersion[3]['companytype']; ?>',<?php echo $companycompersion[3]['total']; ?>],
                                        ['<?php echo $companycompersion[4]['companytype']; ?>',<?php echo $companycompersion[4]['total']; ?>],
                                        ['<?php echo $companycompersion[5]['companytype']; ?>',<?php echo $companycompersion[5]['total']; ?>],
                                        ['<?php echo $companycompersion[6]['companytype']; ?>',<?php echo $companycompersion[6]['total']; ?>],
                                        ['<?php echo $companycompersion[7]['companytype']; ?>',<?php echo $companycompersion[7]['total']; ?>],
                                        ['<?php echo $companycompersion[8]['companytype']; ?>',<?php echo $companycompersion[8]['total']; ?>],
                                        ['<?php echo $companycompersion[9]['companytype']; ?>',<?php echo $companycompersion[9]['total']; ?>],
                                        ['<?php echo $companycompersion[10]['companytype']; ?>',<?php echo $companycompersion[10]['total']; ?>]]);
                                    var options = {
                                        title: 'Company Comparison Company Type',
                                        is3D: true,
                                    };
                                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                                    chart.draw(data, options);
                                    var data = google.visualization.arrayToDataTable(
                                            [
                                                ['Task', 'Hours per Day'],
                                                ['Modified', <?php echo $companies_mod; ?>],
                                                ['Published', <?php echo $companies_pub; ?>],
                                                ['Approved', <?php echo $companies_app; ?>]
                                            ]);
                                    var options = {
                                        title: 'Companies Information',
                                        is3D: true,
                                        colors: ['#e0440e', '#1271B0', '#87b87f'],
                                    };
                                    var chart = new google.visualization.PieChart(document.getElementById('companychart'));
                                    chart.draw(data, options);
                                    var options = {
                                        title: 'Top 10 Most Login',
                                        is3D: true,
                                        colors: ['#e0440e', '#1271B0', '#87b87f', '#e7711b'],
                                    };
                                    var data = google.visualization.arrayToDataTable(
                                            [
                                                ['Task', 'Hours per Day'],
<?php
foreach ($LoggingHistory as $h):
    echo "['" . $h['Username'] . "'," . $h['total_number_of_login'] . "],";
endforeach;
?>
                                            ]);
                                    var chart = new google.visualization.PieChart(document.getElementById('LoggingHistory'));

                                    chart.draw(data, options);
                                    var options = {
                                        title: 'Users',
                                        is3D: true,
                                        colors: ['#e0440e', '#1271B0', '#87b87f'],
                                    };
                                    var data = google.visualization.arrayToDataTable(
                                            [
                                                ['Task', 'Hours per Day'],
                                                ['Read Only',<?php echo $TotalUsersReadOnly; ?>],
                                                ['Modify Company/Airport',<?php echo $TotalUsersModifyCompany; ?>],
//                                                ['Modify Airport',<?php // echo $TotalUsersModifyAirport; ?>],
                                                ['Admin',<?php echo $TotalUsersAdmin; ?>],
                                            ]);
                                    var chart = new google.visualization.PieChart(document.getElementById('UserChart'));
                                    chart.draw(data, options);
                                    var options = {
                                        title: 'Airports',
                                        is3D: true,
                                        colors: ['#e0440e', '#1271B0', '#87b87f'],
                                    };
                                    var data = google.visualization.arrayToDataTable(
                                            [
                                                ['Task', 'Hours per Day'],
                                                ['Total',<?php echo $total_airport; ?>],
                                                ['Modified',<?php echo $totalmodified; ?>],
                                                ['Published',<?php echo $totalpublished; ?>],
                                                ['Approved',<?php echo $totalapproved; ?>],
                                            ]);
                                    var chart = new google.visualization.PieChart(document.getElementById('airportchart'));
                                    chart.draw(data, options);
                                    //airportchart		 
                                }
        </script>		
    </body>
</html>
